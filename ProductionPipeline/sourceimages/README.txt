The Sourceimages folder is where you store all images that are brought into maya.
You could also use this folder to store reference images and like.

The Texture_Database is the FINAL version of each texture.  This is also saved to the GOOGLE Drive.
NOTE there should only be one version of each texture.
You may wish to create a Texture folder for each prop to avoid duplication and possible overwriting of images.
This is NOT the backup of all textures, that would be on the local drive of each person working on the project.  
NOTE you can always create a mirrored backup.
THIS FOLDER NEEDS A GATEKEEPER.
There must be some kind of system setup that will allow artist to "check out" and lock the model from other departments, such as Modeling, Rigging,