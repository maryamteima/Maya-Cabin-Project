This Folder is the common folder that is used to create the Environment File.
The Environment file is used by the Lighting, Layout and Animation teams.
There should only be ONE copy of each of the props in this folder.  
This is NOT the backup of all the props.  
NOTE you can always create a mirrored backup.
THIS FOLDER NEEDS A GATEKEEPER.
There must be some kind of system setup that will allow artist to "check out" and lock the model from other departments, such as Modeling, Rigging, Texturing and sometimes Lighting.  Lamps may require a light setup.
If a Lamp has a light it should be in the actual prop