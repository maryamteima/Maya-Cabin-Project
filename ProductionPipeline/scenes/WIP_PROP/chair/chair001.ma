//Maya ASCII 2023 scene
//Name: chair001.ma
//Last modified: Fri, Oct 20, 2023 05:47:48 PM
//Codeset: 1252
requires maya "2023";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "5.3.3.3";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202202161415-df43006fd3";
fileInfo "osv" "Windows 11 Home v2009 (Build: 22621)";
fileInfo "UUID" "A269BD71-476F-FA3F-6FA9-A9A6013E34CD";
fileInfo "license" "education";
createNode transform -n "POS";
	rename -uid "5ABA93AC-4C93-7A89-720F-78920DEA923B";
createNode transform -n "MOV" -p "POS";
	rename -uid "3850B1DC-427A-5D50-3002-ACBBB4529DDC";
createNode transform -n "ADJ" -p "MOV";
	rename -uid "DC991A7F-45F1-10B1-8C8D-999D2CFA9357";
createNode transform -s -n "persp";
	rename -uid "33B9B25E-4E26-3500-848C-94B024F14EE7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.1141416495964638 3.9956341330267993 -5.4421509724196664 ;
	setAttr ".r" -type "double3" -25.538352662879234 13153.800000000067 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "DC6F9A2B-4C74-DF8A-53A4-90884A899215";
	setAttr -k off ".v" no;
	setAttr ".pze" yes;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 6.5973984888362232;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.0504989425887972 1.5622737463547449 0.74213020060870671 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "77C5D01E-49E5-CF23-441C-F99F04796743";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "10195A0F-48C9-D1DD-A4EA-EBA2FFACB2FF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 3.0175905130014331;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "78CCCB98-4E3B-A0D3-2662-7E8A10128F55";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.38290324907975065 0.88771739809072292 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "C36C2A9E-4625-AD17-ECD5-E48CEDC414AD";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 2.6528498647005154;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "1A77B2DA-49CF-C289-B59B-6EA2DE9026A8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "6A521042-420E-134E-90AE-21BA732E2C92";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 354.66645270021354;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "ChairBase_geo";
	rename -uid "7B9B586B-40C5-1FE6-0B4C-B0A5BB8125A9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 2.6427477469125233 0 ;
	setAttr ".s" -type "double3" 5.3495440598531037 0.45296313956898321 6.1639835913336682 ;
createNode mesh -n "ChairBase_geoShape" -p "ChairBase_geo";
	rename -uid "7E423457-48EB-D0FC-02BE-33AD628C726D";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.43598750233650208 0.43788748979568481 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "chair_grp_geo";
	rename -uid "D579791F-4EB7-6FEC-F395-F29396B78F75";
	setAttr ".s" -type "double3" 1 0.94143140255103697 1 ;
createNode transform -n "BackTopArch_geo" -p "chair_grp_geo";
	rename -uid "7319DD38-4151-2CCB-3932-95B0A360BBCC";
	setAttr ".t" -type "double3" 0 3.1662713009973751 0.6965260422864028 ;
	setAttr ".s" -type "double3" 1.0422983623511961 0.16524697957621876 0.06163633794191678 ;
createNode mesh -n "BackTopArch_geoShape" -p "BackTopArch_geo";
	rename -uid "1BD41133-4679-8C32-8EC4-39A0376DC5C6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 6 "f[2]" "f[8]" "f[12]" "f[16]" "f[19:22]" "f[42:45]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 6 "f[3]" "f[9]" "f[13]" "f[17]" "f[30:33]" "f[62:65]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 6 "f[0]" "f[6]" "f[10]" "f[14]" "f[24:27]" "f[48:51]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 5 "f[5]" "f[18]" "f[28:29]" "f[40:41]" "f[52:54]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 5 "f[4]" "f[23]" "f[34:35]" "f[46:47]" "f[59:61]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 6 "f[1]" "f[7]" "f[11]" "f[15]" "f[36:39]" "f[55:58]";
	setAttr ".pv" -type "double2" 0.51249998807907104 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 89 ".uvst[0].uvsp[0:88]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75 0.51249999 0
		 0.51249999 1 0.51249999 0.25 0.51249999 0.5 0.51249999 0.74999994 0.48750001 0 0.48750001
		 1 0.48750001 0.25 0.48750001 0.5 0.48750001 0.75 0.375 0.241163 0.125 0.24116288
		 0.375 0.50883716 0.48750001 0.50883716 0.5 0.50883716 0.51249999 0.50883716 0.625
		 0.50883716 0.875 0.24116288 0.625 0.241163 0.51249999 0.241163 0.5 0.241163 0.48750001
		 0.241163 0.35769519 0.25 0.375 0.26730484 0.35769519 0.241163 0.35769528 0 0.375
		 0.98269522 0.48750001 0.98269522 0.5 0.98269522 0.51249999 0.98269522 0.625 0.98269522
		 0.64230472 0 0.64230472 0.241163 0.625 0.26730484 0.64230484 0.25 0.51249999 0.26730484
		 0.5 0.26730484 0.48750001 0.26730484 0.375 0.017447637 0.35769525 0.01744766 0.125
		 0.017447652 0.375 0.73255241 0.48750001 0.73255241 0.5 0.73255241 0.51249999 0.73255235
		 0.625 0.73255241 0.875 0.017447652 0.64230472 0.017447637 0.62500006 0.017447637
		 0.51249999 0.017447637 0.5 0.017447637 0.48750001 0.017447637 0.13858061 0 0.375
		 0.76358062 0.13858056 0.017447652 0.13858056 0.2411629 0.13858056 0.25 0.375 0.48641944
		 0.48750001 0.48641944 0.5 0.48641944 0.51249999 0.48641944 0.625 0.48641944 0.86141944
		 0.25 0.86141938 0.2411629 0.86141938 0.01744765 0.625 0.76358062 0.86141938 0 0.51249999
		 0.76358056 0.5 0.76358062 0.48750001 0.76358062;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".vt[0:67]"  -0.5 -0.95547771 0.5 0.52890182 -0.95547795 0.5
		 -0.5 0.044522256 0.5 0.52890182 0.044522017 0.5 -0.5 0.044522256 -0.5 0.52890182 0.044522017 -0.5
		 -0.5 -0.95547771 -0.5 0.52890182 -0.95547795 -0.5 0 -0.5 0.5 0 0.5 0.5 0 0.5 -0.5
		 0 -0.5 -0.5 0.14566766 -0.5 0.5 0.14566766 0.5 0.5 0.14566766 0.5 -0.5 0.14566766 -0.5 -0.5
		 -0.13165325 -0.5 0.5 -0.13165325 0.5 0.5 -0.13165325 0.5 -0.5 -0.13165325 -0.5 -0.5
		 -0.5 0.0091742612 0.5 -0.5 0.0091737583 -0.5 -0.13165326 0.46465153 -0.5 0 0.46465153 -0.5
		 0.14566767 0.46465153 -0.5 0.52890182 0.0091735162 -0.5 0.52890182 0.0091740191 0.5
		 0.14566766 0.464652 0.5 0 0.464652 0.5 -0.13165325 0.464652 0.5 -0.5 0.044522256 0.43078071
		 -0.5 0.0091742259 0.43078071 -0.5 -0.95547771 0.43078101 -0.13165325 -0.5 0.43078101
		 0 -0.5 0.43078101 0.14566766 -0.5 0.43078101 0.52890182 -0.95547795 0.43078101 0.52890182 0.0091739837 0.43078101
		 0.52890182 0.044522017 0.43078071 0.14566767 0.5 0.43078071 0 0.5 0.43078071 -0.13165325 0.5 0.43078071
		 -0.5 -0.88568717 0.5 -0.5 -0.88568705 0.43078101 -0.5 -0.88568711 -0.5 -0.13165325 -0.4302094 -0.5
		 0 -0.4302094 -0.5 0.14566767 -0.4302094 -0.5 0.52890182 -0.88568735 -0.5 0.52890182 -0.88568747 0.43078101
		 0.52890182 -0.88568747 0.5 0.14566767 -0.43020946 0.5 0 -0.43020946 0.5 -0.13165325 -0.43020946 0.5
		 -0.5 -0.95547771 -0.44567758 -0.5 -0.88568711 -0.44567776 -0.5 0.0091737863 -0.44567776
		 -0.5 0.044522256 -0.44567776 -0.13165325 0.5 -0.44567776 0 0.5 -0.44567776 0.14566766 0.5 -0.44567776
		 0.52890182 0.044522017 -0.44567776 0.52890182 0.0091735432 -0.44567758 0.52890182 -0.88568741 -0.44567758
		 0.52890182 -0.95547795 -0.44567758 0.14566766 -0.5 -0.44567758 0 -0.5 -0.44567758
		 -0.13165325 -0.5 -0.44567758;
	setAttr -s 132 ".ed[0:131]"  0 16 0 2 17 0 4 18 0 6 19 0 0 42 0 1 50 0
		 2 30 0 3 38 0 4 21 0 5 25 0 6 54 0 7 64 0 8 12 0 9 13 0 10 14 0 11 15 0 8 52 1 9 40 1
		 10 23 1 11 66 1 12 1 0 13 3 0 14 5 0 15 7 0 12 51 1 13 39 1 14 24 1 15 65 1 16 8 0
		 17 9 0 18 10 0 19 11 0 16 53 1 17 41 1 18 22 1 19 67 1 20 2 0 21 44 0 22 45 1 23 46 1
		 24 47 1 25 48 0 26 3 0 27 13 1 28 9 1 29 17 1 20 31 1 21 22 1 22 23 1 23 24 1 24 25 1
		 25 62 1 26 27 1 27 28 1 28 29 1 29 20 1 30 57 0 31 56 1 32 0 0 33 16 1 34 8 1 35 12 1
		 36 1 0 37 26 1 38 61 0 39 60 1 40 59 1 41 58 1 30 31 1 31 43 1 32 33 1 33 34 1 34 35 1
		 35 36 1 36 49 1 37 38 1 38 39 1 39 40 1 40 41 1 41 30 1 42 20 0 43 32 1 44 6 0 45 19 1
		 46 11 1 47 15 1 48 7 0 49 37 1 50 26 0 51 27 1 52 28 1 53 29 1 42 43 1 43 55 1 44 45 1
		 45 46 1 46 47 1 47 48 1 48 63 1 49 50 1 50 51 1 51 52 1 52 53 1 53 42 1 54 32 0 55 44 1
		 56 21 1 57 4 0 58 18 1 59 10 1 60 14 1 61 5 0 62 37 1 63 49 1 64 36 0 65 35 1 66 34 1
		 67 33 1 54 55 1 55 56 1 56 57 1 57 58 1 58 59 1 59 60 1 60 61 1 61 62 1 62 63 1 63 64 1
		 64 65 1 65 66 1 66 67 1 67 54 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 32 103 -5
		mu 0 4 0 24 70 57
		f 4 1 33 79 -7
		mu 0 4 2 26 56 42
		f 4 94 83 -4 -83
		mu 0 4 60 61 28 6
		f 4 70 59 -1 -59
		mu 0 4 45 46 25 8
		f 4 -63 74 99 -6
		mu 0 4 1 50 66 67
		f 4 92 81 58 4
		mu 0 4 57 58 44 0
		f 4 101 -17 12 24
		mu 0 4 68 69 14 19
		f 4 77 -18 13 25
		mu 0 4 54 55 16 21
		f 4 -85 96 85 -16
		mu 0 4 18 62 63 23
		f 4 -61 72 61 -13
		mu 0 4 15 47 48 20
		f 4 100 -25 20 5
		mu 0 4 67 68 19 1
		f 4 76 -26 21 7
		mu 0 4 52 54 21 3
		f 4 -86 97 86 -24
		mu 0 4 23 63 64 7
		f 4 -62 73 62 -21
		mu 0 4 20 48 49 9
		f 4 102 -33 28 16
		mu 0 4 69 70 24 14
		f 4 78 -34 29 17
		mu 0 4 55 56 26 16
		f 4 -84 95 84 -32
		mu 0 4 28 61 62 18
		f 4 -60 71 60 -29
		mu 0 4 25 46 47 15
		f 4 68 -47 36 6
		mu 0 4 41 43 29 2
		f 4 2 34 -48 -9
		mu 0 4 4 27 32 31
		f 4 -49 -35 30 18
		mu 0 4 33 32 27 17
		f 4 -50 -19 14 26
		mu 0 4 34 33 17 22
		f 4 -51 -27 22 9
		mu 0 4 35 34 22 5
		f 4 -64 75 -8 -43
		mu 0 4 37 51 53 3
		f 4 -44 -53 42 -22
		mu 0 4 21 38 37 3
		f 4 -45 -54 43 -14
		mu 0 4 16 39 38 21
		f 4 -46 -55 44 -30
		mu 0 4 26 40 39 16
		f 4 -56 45 -2 -37
		mu 0 4 29 40 26 2
		f 4 -107 120 107 8
		mu 0 4 30 74 75 13
		f 4 10 118 105 82
		mu 0 4 12 71 73 59
		f 4 3 35 131 -11
		mu 0 4 6 28 88 72
		f 4 130 -36 31 19
		mu 0 4 87 88 28 18
		f 4 129 -20 15 27
		mu 0 4 86 87 18 23
		f 4 128 -28 23 11
		mu 0 4 84 86 23 7
		f 4 98 127 -12 -87
		mu 0 4 65 83 85 10
		f 4 125 -52 -10 -112
		mu 0 4 81 82 36 11
		f 4 -111 124 111 -23
		mu 0 4 22 79 80 5
		f 4 -110 123 110 -15
		mu 0 4 17 78 79 22
		f 4 -109 122 109 -31
		mu 0 4 27 77 78 17
		f 4 121 108 -3 -108
		mu 0 4 76 77 27 4
		f 4 69 -93 80 46
		mu 0 4 43 58 57 29
		f 4 -106 119 106 37
		mu 0 4 59 73 74 30
		f 4 47 38 -95 -38
		mu 0 4 31 32 61 60
		f 4 -96 -39 48 39
		mu 0 4 62 61 32 33
		f 4 -97 -40 49 40
		mu 0 4 63 62 33 34
		f 4 -98 -41 50 41
		mu 0 4 64 63 34 35
		f 4 126 -99 -42 51
		mu 0 4 82 83 65 36
		f 4 -100 87 63 -89
		mu 0 4 67 66 51 37
		f 4 52 -90 -101 88
		mu 0 4 37 38 68 67
		f 4 53 -91 -102 89
		mu 0 4 38 39 69 68
		f 4 54 -92 -103 90
		mu 0 4 39 40 70 69
		f 4 -104 91 55 -81
		mu 0 4 57 70 40 29
		f 4 -119 104 -82 93
		mu 0 4 73 71 44 58
		f 4 -120 -94 -70 57
		mu 0 4 74 73 58 43
		f 4 -121 -58 -69 56
		mu 0 4 75 74 43 41
		f 4 -80 67 -122 -57
		mu 0 4 42 56 77 76
		f 4 -123 -68 -79 66
		mu 0 4 78 77 56 55
		f 4 -124 -67 -78 65
		mu 0 4 79 78 55 54
		f 4 -125 -66 -77 64
		mu 0 4 80 79 54 52
		f 4 -76 -113 -126 -65
		mu 0 4 53 51 82 81
		f 4 -88 -114 -127 112
		mu 0 4 51 66 83 82
		f 4 -128 113 -75 -115
		mu 0 4 85 83 66 50
		f 4 -74 -116 -129 114
		mu 0 4 49 48 86 84
		f 4 -73 -117 -130 115
		mu 0 4 48 47 87 86
		f 4 -72 -118 -131 116
		mu 0 4 47 46 88 87
		f 4 -132 117 -71 -105
		mu 0 4 72 88 46 45;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "BackMiddleArch_geo" -p "chair_grp_geo";
	rename -uid "0F5BFCCB-4612-86F8-BCCD-5BB59174D011";
	setAttr ".t" -type "double3" 0 2.6497793616862642 0.66229037073836494 ;
	setAttr ".s" -type "double3" 1.0422983623511961 0.098479022943911046 0.06163633794191678 ;
createNode mesh -n "BackMiddleArch_geoShape" -p "BackMiddleArch_geo";
	rename -uid "68A094B9-4693-23CD-4196-57BE9542E592";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 6 "f[2]" "f[8]" "f[12]" "f[16]" "f[19:22]" "f[42:45]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 6 "f[3]" "f[9]" "f[13]" "f[17]" "f[30:33]" "f[62:65]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 6 "f[0]" "f[6]" "f[10]" "f[14]" "f[24:27]" "f[48:51]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 5 "f[5]" "f[18]" "f[28:29]" "f[40:41]" "f[52:54]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 5 "f[4]" "f[23]" "f[34:35]" "f[46:47]" "f[59:61]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 6 "f[1]" "f[7]" "f[11]" "f[15]" "f[36:39]" "f[55:58]";
	setAttr ".pv" -type "double2" 0.51249998807907104 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 89 ".uvst[0].uvsp[0:88]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75 0.51249999 0
		 0.51249999 1 0.51249999 0.25 0.51249999 0.5 0.51249999 0.74999994 0.48750001 0 0.48750001
		 1 0.48750001 0.25 0.48750001 0.5 0.48750001 0.75 0.375 0.241163 0.125 0.24116288
		 0.375 0.50883716 0.48750001 0.50883716 0.5 0.50883716 0.51249999 0.50883716 0.625
		 0.50883716 0.875 0.24116288 0.625 0.241163 0.51249999 0.241163 0.5 0.241163 0.48750001
		 0.241163 0.35769519 0.25 0.375 0.26730484 0.35769519 0.241163 0.35769528 0 0.375
		 0.98269522 0.48750001 0.98269522 0.5 0.98269522 0.51249999 0.98269522 0.625 0.98269522
		 0.64230472 0 0.64230472 0.241163 0.625 0.26730484 0.64230484 0.25 0.51249999 0.26730484
		 0.5 0.26730484 0.48750001 0.26730484 0.375 0.017447637 0.35769525 0.01744766 0.125
		 0.017447652 0.375 0.73255241 0.48750001 0.73255241 0.5 0.73255241 0.51249999 0.73255235
		 0.625 0.73255241 0.875 0.017447652 0.64230472 0.017447637 0.62500006 0.017447637
		 0.51249999 0.017447637 0.5 0.017447637 0.48750001 0.017447637 0.13858061 0 0.375
		 0.76358062 0.13858056 0.017447652 0.13858056 0.2411629 0.13858056 0.25 0.375 0.48641944
		 0.48750001 0.48641944 0.5 0.48641944 0.51249999 0.48641944 0.625 0.48641944 0.86141944
		 0.25 0.86141938 0.2411629 0.86141938 0.01744765 0.625 0.76358062 0.86141938 0 0.51249999
		 0.76358056 0.5 0.76358062 0.48750001 0.76358062;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".vt[0:67]"  -0.5 -0.95547771 0.5 0.52890182 -0.95547795 0.5
		 -0.5 0.044522256 0.5 0.52890182 0.044522017 0.5 -0.5 0.044522256 -0.5 0.52890182 0.044522017 -0.5
		 -0.5 -0.95547771 -0.5 0.52890182 -0.95547795 -0.5 0 -0.5 0.5 0 0.5 0.5 0 0.5 -0.5
		 0 -0.5 -0.5 0.14566766 -0.5 0.5 0.14566766 0.5 0.5 0.14566766 0.5 -0.5 0.14566766 -0.5 -0.5
		 -0.13165325 -0.5 0.5 -0.13165325 0.5 0.5 -0.13165325 0.5 -0.5 -0.13165325 -0.5 -0.5
		 -0.5 0.0091742612 0.5 -0.5 0.0091737583 -0.5 -0.13165326 0.46465153 -0.5 0 0.46465153 -0.5
		 0.14566767 0.46465153 -0.5 0.52890182 0.0091735162 -0.5 0.52890182 0.0091740191 0.5
		 0.14566766 0.464652 0.5 0 0.464652 0.5 -0.13165325 0.464652 0.5 -0.5 0.044522256 0.43078071
		 -0.5 0.0091742259 0.43078071 -0.5 -0.95547771 0.43078101 -0.13165325 -0.5 0.43078101
		 0 -0.5 0.43078101 0.14566766 -0.5 0.43078101 0.52890182 -0.95547795 0.43078101 0.52890182 0.0091739837 0.43078101
		 0.52890182 0.044522017 0.43078071 0.14566767 0.5 0.43078071 0 0.5 0.43078071 -0.13165325 0.5 0.43078071
		 -0.5 -0.88568717 0.5 -0.5 -0.88568705 0.43078101 -0.5 -0.88568711 -0.5 -0.13165325 -0.4302094 -0.5
		 0 -0.4302094 -0.5 0.14566767 -0.4302094 -0.5 0.52890182 -0.88568735 -0.5 0.52890182 -0.88568747 0.43078101
		 0.52890182 -0.88568747 0.5 0.14566767 -0.43020946 0.5 0 -0.43020946 0.5 -0.13165325 -0.43020946 0.5
		 -0.5 -0.95547771 -0.44567758 -0.5 -0.88568711 -0.44567776 -0.5 0.0091737863 -0.44567776
		 -0.5 0.044522256 -0.44567776 -0.13165325 0.5 -0.44567776 0 0.5 -0.44567776 0.14566766 0.5 -0.44567776
		 0.52890182 0.044522017 -0.44567776 0.52890182 0.0091735432 -0.44567758 0.52890182 -0.88568741 -0.44567758
		 0.52890182 -0.95547795 -0.44567758 0.14566766 -0.5 -0.44567758 0 -0.5 -0.44567758
		 -0.13165325 -0.5 -0.44567758;
	setAttr -s 132 ".ed[0:131]"  0 16 0 2 17 0 4 18 0 6 19 0 0 42 0 1 50 0
		 2 30 0 3 38 0 4 21 0 5 25 0 6 54 0 7 64 0 8 12 0 9 13 0 10 14 0 11 15 0 8 52 1 9 40 1
		 10 23 1 11 66 1 12 1 0 13 3 0 14 5 0 15 7 0 12 51 1 13 39 1 14 24 1 15 65 1 16 8 0
		 17 9 0 18 10 0 19 11 0 16 53 1 17 41 1 18 22 1 19 67 1 20 2 0 21 44 0 22 45 1 23 46 1
		 24 47 1 25 48 0 26 3 0 27 13 1 28 9 1 29 17 1 20 31 1 21 22 1 22 23 1 23 24 1 24 25 1
		 25 62 1 26 27 1 27 28 1 28 29 1 29 20 1 30 57 0 31 56 1 32 0 0 33 16 1 34 8 1 35 12 1
		 36 1 0 37 26 1 38 61 0 39 60 1 40 59 1 41 58 1 30 31 1 31 43 1 32 33 1 33 34 1 34 35 1
		 35 36 1 36 49 1 37 38 1 38 39 1 39 40 1 40 41 1 41 30 1 42 20 0 43 32 1 44 6 0 45 19 1
		 46 11 1 47 15 1 48 7 0 49 37 1 50 26 0 51 27 1 52 28 1 53 29 1 42 43 1 43 55 1 44 45 1
		 45 46 1 46 47 1 47 48 1 48 63 1 49 50 1 50 51 1 51 52 1 52 53 1 53 42 1 54 32 0 55 44 1
		 56 21 1 57 4 0 58 18 1 59 10 1 60 14 1 61 5 0 62 37 1 63 49 1 64 36 0 65 35 1 66 34 1
		 67 33 1 54 55 1 55 56 1 56 57 1 57 58 1 58 59 1 59 60 1 60 61 1 61 62 1 62 63 1 63 64 1
		 64 65 1 65 66 1 66 67 1 67 54 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 32 103 -5
		mu 0 4 0 24 70 57
		f 4 1 33 79 -7
		mu 0 4 2 26 56 42
		f 4 94 83 -4 -83
		mu 0 4 60 61 28 6
		f 4 70 59 -1 -59
		mu 0 4 45 46 25 8
		f 4 -63 74 99 -6
		mu 0 4 1 50 66 67
		f 4 92 81 58 4
		mu 0 4 57 58 44 0
		f 4 101 -17 12 24
		mu 0 4 68 69 14 19
		f 4 77 -18 13 25
		mu 0 4 54 55 16 21
		f 4 -85 96 85 -16
		mu 0 4 18 62 63 23
		f 4 -61 72 61 -13
		mu 0 4 15 47 48 20
		f 4 100 -25 20 5
		mu 0 4 67 68 19 1
		f 4 76 -26 21 7
		mu 0 4 52 54 21 3
		f 4 -86 97 86 -24
		mu 0 4 23 63 64 7
		f 4 -62 73 62 -21
		mu 0 4 20 48 49 9
		f 4 102 -33 28 16
		mu 0 4 69 70 24 14
		f 4 78 -34 29 17
		mu 0 4 55 56 26 16
		f 4 -84 95 84 -32
		mu 0 4 28 61 62 18
		f 4 -60 71 60 -29
		mu 0 4 25 46 47 15
		f 4 68 -47 36 6
		mu 0 4 41 43 29 2
		f 4 2 34 -48 -9
		mu 0 4 4 27 32 31
		f 4 -49 -35 30 18
		mu 0 4 33 32 27 17
		f 4 -50 -19 14 26
		mu 0 4 34 33 17 22
		f 4 -51 -27 22 9
		mu 0 4 35 34 22 5
		f 4 -64 75 -8 -43
		mu 0 4 37 51 53 3
		f 4 -44 -53 42 -22
		mu 0 4 21 38 37 3
		f 4 -45 -54 43 -14
		mu 0 4 16 39 38 21
		f 4 -46 -55 44 -30
		mu 0 4 26 40 39 16
		f 4 -56 45 -2 -37
		mu 0 4 29 40 26 2
		f 4 -107 120 107 8
		mu 0 4 30 74 75 13
		f 4 10 118 105 82
		mu 0 4 12 71 73 59
		f 4 3 35 131 -11
		mu 0 4 6 28 88 72
		f 4 130 -36 31 19
		mu 0 4 87 88 28 18
		f 4 129 -20 15 27
		mu 0 4 86 87 18 23
		f 4 128 -28 23 11
		mu 0 4 84 86 23 7
		f 4 98 127 -12 -87
		mu 0 4 65 83 85 10
		f 4 125 -52 -10 -112
		mu 0 4 81 82 36 11
		f 4 -111 124 111 -23
		mu 0 4 22 79 80 5
		f 4 -110 123 110 -15
		mu 0 4 17 78 79 22
		f 4 -109 122 109 -31
		mu 0 4 27 77 78 17
		f 4 121 108 -3 -108
		mu 0 4 76 77 27 4
		f 4 69 -93 80 46
		mu 0 4 43 58 57 29
		f 4 -106 119 106 37
		mu 0 4 59 73 74 30
		f 4 47 38 -95 -38
		mu 0 4 31 32 61 60
		f 4 -96 -39 48 39
		mu 0 4 62 61 32 33
		f 4 -97 -40 49 40
		mu 0 4 63 62 33 34
		f 4 -98 -41 50 41
		mu 0 4 64 63 34 35
		f 4 126 -99 -42 51
		mu 0 4 82 83 65 36
		f 4 -100 87 63 -89
		mu 0 4 67 66 51 37
		f 4 52 -90 -101 88
		mu 0 4 37 38 68 67
		f 4 53 -91 -102 89
		mu 0 4 38 39 69 68
		f 4 54 -92 -103 90
		mu 0 4 39 40 70 69
		f 4 -104 91 55 -81
		mu 0 4 57 70 40 29
		f 4 -119 104 -82 93
		mu 0 4 73 71 44 58
		f 4 -120 -94 -70 57
		mu 0 4 74 73 58 43
		f 4 -121 -58 -69 56
		mu 0 4 75 74 43 41
		f 4 -80 67 -122 -57
		mu 0 4 42 56 77 76
		f 4 -123 -68 -79 66
		mu 0 4 78 77 56 55
		f 4 -124 -67 -78 65
		mu 0 4 79 78 55 54
		f 4 -125 -66 -77 64
		mu 0 4 80 79 54 52
		f 4 -76 -113 -126 -65
		mu 0 4 53 51 82 81
		f 4 -88 -114 -127 112
		mu 0 4 51 66 83 82
		f 4 -128 113 -75 -115
		mu 0 4 85 83 66 50
		f 4 -74 -116 -129 114
		mu 0 4 49 48 86 84
		f 4 -73 -117 -130 115
		mu 0 4 48 47 87 86
		f 4 -72 -118 -131 116
		mu 0 4 47 46 88 87
		f 4 -132 117 -71 -105
		mu 0 4 72 88 46 45;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "BackBottomArch_geo" -p "chair_grp_geo";
	rename -uid "BC4BD4F0-42B0-BCE3-3B67-448A9C598E84";
	setAttr ".t" -type "double3" 0 2.216205580440251 0.62699031348185885 ;
	setAttr ".s" -type "double3" 0.91251533944527596 0.097070490090640046 0.06163633794191678 ;
createNode mesh -n "BackBottomArch_geoShape" -p "BackBottomArch_geo";
	rename -uid "6071EE26-47E8-58C0-5C97-5386EDB97694";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.51249998807907104 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "RightTopAjoin_geo1" -p "chair_grp_geo";
	rename -uid "B5BF5E41-4361-4B34-5355-7FB0198D0DFE";
	setAttr ".t" -type "double3" -0.55600432255147736 1.3845961021151023 0.076153788928603958 ;
	setAttr ".r" -type "double3" 0 2.2148179403146866 0 ;
	setAttr ".s" -type "double3" 0.012921427309455509 0.09585987147475436 1.1221484966738502 ;
createNode mesh -n "RightTopAjoin_geo1Shape" -p "RightTopAjoin_geo1";
	rename -uid "5B142AD7-4457-E9D3-9CA7-EF942B673192";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 5 "f[2]" "f[7]" "f[11]" "f[18:20]" "f[26:28]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 3 "f[3]" "f[21]" "f[29]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 4 "f[0]" "f[9]" "f[13:16]" "f[22:24]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 2 "f[5:6]" "f[10]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 3 "f[4]" "f[8]" "f[12]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 3 "f[1]" "f[17]" "f[25]";
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0.23448375 0.125 0.23448387 0.375 0.5155161 0.62499994
		 0.5155161 0.875 0.23448387 0.625 0.23448375 0.375 0.022498902 0.125 0.022498967 0.375
		 0.72750103 0.625 0.72750103 0.875 0.022498967 0.625 0.022498902 0.6107223 0 0.6107223
		 1 0.6107223 0.022498902 0.6107223 0.23448375 0.6107223 0.25 0.6107223 0.5 0.61072218
		 0.5155161 0.6107223 0.72750103 0.6107223 0.75 0.38566718 0 0.38566718 1 0.38566715
		 0.022498902 0.38566715 0.23448375 0.38566718 0.25 0.38566718 0.5 0.38566718 0.5155161
		 0.38566718 0.72750098 0.38566718 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[0]" -type "float3" 9.5367432e-07 0 0 ;
	setAttr ".pt[6]" -type "float3" 9.5367432e-07 0 0 ;
	setAttr -s 32 ".vt[0:31]"  -0.49999905 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5
		 0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.49999905 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.43793499 0.5
		 -0.5 0.43793547 -0.5 0.5 0.43793547 -0.5 0.5 0.43793499 0.5 -0.5 -0.41000441 0.5
		 -0.5 -0.41000414 -0.5 0.5 -0.41000414 -0.5 0.5 -0.41000441 0.5 0.44288898 -0.5 0.5
		 0.44288909 -0.41000441 0.5 0.44288909 0.43793499 0.5 0.44288898 0.5 0.5 0.44288898 0.5 -0.5
		 0.44288898 0.43793547 -0.5 0.44288898 -0.41000414 -0.5 0.44288898 -0.5 -0.5 -0.45733124 -0.5 0.5
		 -0.45733145 -0.41000441 0.5 -0.45733145 0.43793499 0.5 -0.45733124 0.5 0.5 -0.45733124 0.5 -0.5
		 -0.45733124 0.43793547 -0.5 -0.45733124 -0.41000414 -0.5 -0.45733124 -0.5 -0.5;
	setAttr -s 60 ".ed[0:59]"  0 24 0 2 27 0 4 28 0 6 31 0 0 12 0 1 15 0
		 2 4 0 3 5 0 4 9 0 5 10 0 6 0 0 7 1 0 8 2 0 9 13 0 10 14 0 11 3 0 8 9 1 9 29 1 10 11 1
		 11 18 1 12 8 0 13 6 0 14 7 0 15 11 0 12 13 1 13 30 1 14 15 1 15 17 1 16 1 0 17 25 1
		 18 26 1 19 3 0 20 5 0 21 10 1 22 14 1 23 7 0 16 17 1 17 18 1 18 19 1 19 20 1 20 21 1
		 21 22 1 22 23 1 23 16 1 24 16 0 25 12 1 26 8 1 27 19 0 28 20 0 29 21 1 30 22 1 31 23 0
		 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1 29 30 1 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 0 52 45 -5
		mu 0 4 0 35 37 20
		f 4 1 55 -3 -7
		mu 0 4 2 39 40 4
		f 4 25 58 -4 -22
		mu 0 4 22 42 43 6
		f 4 3 59 -1 -11
		mu 0 4 6 43 36 8
		f 4 -12 -23 26 -6
		mu 0 4 1 10 24 25
		f 4 10 4 24 21
		mu 0 4 12 0 20 21
		f 4 -17 12 6 8
		mu 0 4 15 14 2 13
		f 4 2 56 -18 -9
		mu 0 4 4 40 41 16
		f 4 -19 -10 -8 -16
		mu 0 4 19 18 11 3
		f 4 -47 54 -2 -13
		mu 0 4 14 38 39 2
		f 4 -25 20 16 13
		mu 0 4 21 20 14 15
		f 4 17 57 -26 -14
		mu 0 4 16 41 42 22
		f 4 -27 -15 18 -24
		mu 0 4 25 24 18 19
		f 4 -46 53 46 -21
		mu 0 4 20 37 38 14
		f 4 -37 28 5 27
		mu 0 4 28 26 1 25
		f 4 -38 -28 23 19
		mu 0 4 29 28 25 19
		f 4 -39 -20 15 -32
		mu 0 4 30 29 19 3
		f 4 -40 31 7 -33
		mu 0 4 31 30 3 5
		f 4 -41 32 9 -34
		mu 0 4 32 31 5 17
		f 4 -42 33 14 -35
		mu 0 4 33 32 17 23
		f 4 -43 34 22 -36
		mu 0 4 34 33 23 7
		f 4 -44 35 11 -29
		mu 0 4 27 34 7 9
		f 4 -53 44 36 29
		mu 0 4 37 35 26 28
		f 4 -54 -30 37 30
		mu 0 4 38 37 28 29
		f 4 -55 -31 38 -48
		mu 0 4 39 38 29 30
		f 4 -56 47 39 -49
		mu 0 4 40 39 30 31
		f 4 -57 48 40 -50
		mu 0 4 41 40 31 32
		f 4 -58 49 41 -51
		mu 0 4 42 41 32 33
		f 4 -59 50 42 -52
		mu 0 4 43 42 33 34
		f 4 -60 51 43 -45
		mu 0 4 36 43 34 27;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "LeftTopAjoin_geo" -p "chair_grp_geo";
	rename -uid "BEF90749-40D0-AB4C-66E5-E0B7CFA0BBAC";
	setAttr ".t" -type "double3" 0.52730359152962936 1.3845961021151023 0.10268522805375652 ;
	setAttr ".r" -type "double3" 0 -4.6973908172676158 0 ;
	setAttr ".s" -type "double3" 0.012921427309455509 0.09585987147475436 1.0754070588287594 ;
createNode mesh -n "LeftTopAjoin_geoShape" -p "LeftTopAjoin_geo";
	rename -uid "6430AD7A-4AC4-3CD1-A8BC-ECAD9AF76991";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[0]" -type "float3" 9.5367432e-07 0 0 ;
	setAttr ".pt[6]" -type "float3" 9.5367432e-07 0 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "BackLegPillar_geo" -p "chair_grp_geo";
	rename -uid "E66F2682-45FF-0BB2-1BF7-35AA7700C75A";
	setAttr ".t" -type "double3" -0.0078010153670731164 0.8930796092174923 0.70025184203087687 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.031853839319324569 0.48711690518873674 0.032362635719265419 ;
createNode mesh -n "BackLegPillar_geoShape" -p "BackLegPillar_geo";
	rename -uid "980A6F65-49CD-14D6-3B00-72A2C8EFE87F";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape6" -p "BackLegPillar_geo";
	rename -uid "48C3DE4C-4BA0-DDFC-AE74-7EBD1F5DA415";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[0:7]" "e[16:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:23]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:15]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[16:23]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 27 ".uvst[0].uvsp[0:26]" -type "float2" 0.375 0.3125 0.40625
		 0.3125 0.4375 0.3125 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375
		 0.3125 0.625 0.3125 0.375 0.5 0.40625 0.5 0.4375 0.5 0.46875 0.5 0.5 0.5 0.53125
		 0.5 0.5625 0.5 0.59375 0.5 0.625 0.5 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875
		 0.6875 0.5 0.6875 0.53125 0.6875 0.5625 0.6875 0.59375 0.6875 0.625 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 0 -0.70710671 0 0 -0.99999988 -0.70710671 0 -0.70710671
		 -0.99999988 0 0 -0.70710671 0 0.70710671 0 0 0.99999994 0.70710677 0 0.70710677 1 0 0
		 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671 -0.99999988 1 0
		 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0
		 5 13 0 6 14 0 7 15 0 8 16 0 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0;
	setAttr -s 16 -ch 64 ".fc[0:15]" -type "polyFaces" 
		f 4 0 25 -9 -25
		mu 0 4 0 1 10 9
		f 4 1 26 -10 -26
		mu 0 4 1 2 11 10
		f 4 2 27 -11 -27
		mu 0 4 2 3 12 11
		f 4 3 28 -12 -28
		mu 0 4 3 4 13 12
		f 4 4 29 -13 -29
		mu 0 4 4 5 14 13
		f 4 5 30 -14 -30
		mu 0 4 5 6 15 14
		f 4 6 31 -15 -31
		mu 0 4 6 7 16 15
		f 4 7 24 -16 -32
		mu 0 4 7 8 17 16
		f 4 8 33 -17 -33
		mu 0 4 9 10 19 18
		f 4 9 34 -18 -34
		mu 0 4 10 11 20 19
		f 4 10 35 -19 -35
		mu 0 4 11 12 21 20
		f 4 11 36 -20 -36
		mu 0 4 12 13 22 21
		f 4 12 37 -21 -37
		mu 0 4 13 14 23 22
		f 4 13 38 -22 -38
		mu 0 4 14 15 24 23
		f 4 14 39 -23 -39
		mu 0 4 15 16 25 24
		f 4 15 32 -24 -40
		mu 0 4 16 17 26 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "RightSideBottomPillar_geo" -p "chair_grp_geo";
	rename -uid "40000143-4F2E-5973-31E7-EA89B6FCE4C9";
	setAttr ".t" -type "double3" -0.53719925759438614 0.72813372572290025 0.062167227281510834 ;
	setAttr ".r" -type "double3" 90 0 90 ;
	setAttr ".s" -type "double3" 0.033546535386596556 0.60421070761739315 0.034082368956424233 ;
createNode mesh -n "RightSideBottomPillar_geoShape" -p "RightSideBottomPillar_geo";
	rename -uid "964548C5-474B-1D9E-621B-829C92BF043A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.6875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 17 ".pt[0:16]" -type "float3"  -7.1054274e-15 0 -0.81175792 
		-7.030786e-15 0 -0.81175792 -7.1054274e-15 0 -0.81175792 -7.1054274e-15 0 -0.81175792 
		-7.1054274e-15 0 -0.81175792 -7.030786e-15 0 -0.81175792 -7.1054274e-15 0 -0.81175792 
		-7.1054274e-15 0 -0.81175792 0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 
		0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 0 0 0;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape3" -p "RightSideBottomPillar_geo";
	rename -uid "DDA8C55D-4B36-DF23-51B7-A1A8B96F6DE8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[0:7]" "e[16:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:23]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:15]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[16:23]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 27 ".uvst[0].uvsp[0:26]" -type "float2" 0.375 0.3125 0.40625
		 0.3125 0.4375 0.3125 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375
		 0.3125 0.625 0.3125 0.375 0.5 0.40625 0.5 0.4375 0.5 0.46875 0.5 0.5 0.5 0.53125
		 0.5 0.5625 0.5 0.59375 0.5 0.625 0.5 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875
		 0.6875 0.5 0.6875 0.53125 0.6875 0.5625 0.6875 0.59375 0.6875 0.625 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 0 -0.70710671 0 0 -0.99999988 -0.70710671 0 -0.70710671
		 -0.99999988 0 0 -0.70710671 0 0.70710671 0 0 0.99999994 0.70710677 0 0.70710677 1 0 0
		 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671 -0.99999988 1 0
		 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0
		 5 13 0 6 14 0 7 15 0 8 16 0 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0;
	setAttr -s 16 -ch 64 ".fc[0:15]" -type "polyFaces" 
		f 4 0 25 -9 -25
		mu 0 4 0 1 10 9
		f 4 1 26 -10 -26
		mu 0 4 1 2 11 10
		f 4 2 27 -11 -27
		mu 0 4 2 3 12 11
		f 4 3 28 -12 -28
		mu 0 4 3 4 13 12
		f 4 4 29 -13 -29
		mu 0 4 4 5 14 13
		f 4 5 30 -14 -30
		mu 0 4 5 6 15 14
		f 4 6 31 -15 -31
		mu 0 4 6 7 16 15
		f 4 7 24 -16 -32
		mu 0 4 7 8 17 16
		f 4 8 33 -17 -33
		mu 0 4 9 10 19 18
		f 4 9 34 -18 -34
		mu 0 4 10 11 20 19
		f 4 10 35 -19 -35
		mu 0 4 11 12 21 20
		f 4 11 36 -20 -36
		mu 0 4 12 13 22 21
		f 4 12 37 -21 -37
		mu 0 4 13 14 23 22
		f 4 13 38 -22 -38
		mu 0 4 14 15 24 23
		f 4 14 39 -23 -39
		mu 0 4 15 16 25 24
		f 4 15 32 -24 -40
		mu 0 4 16 17 26 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "RightSideTopPillar_geo" -p "chair_grp_geo";
	rename -uid "CECBD4DB-4FA3-28C9-9E25-209D0E510D19";
	setAttr ".t" -type "double3" -0.53719925759438614 1.0181472262399718 0.062167227281510834 ;
	setAttr ".r" -type "double3" 90 0 90 ;
	setAttr ".s" -type "double3" 0.033546535386596556 0.57796049315921239 0.034082368956424233 ;
createNode mesh -n "RightSideTopPillar_geoShape" -p "RightSideTopPillar_geo";
	rename -uid "A346CBE0-490B-4A45-8CD6-D8BE295500DF";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 17 ".pt[0:16]" -type "float3"  -7.1054274e-15 0 -0.81175792 
		-7.030786e-15 0 -0.81175792 -7.1054274e-15 0 -0.81175792 -7.1054274e-15 0 -0.81175792 
		-7.1054274e-15 0 -0.81175792 -7.030786e-15 0 -0.81175792 -7.1054274e-15 0 -0.81175792 
		-7.1054274e-15 0 -0.81175792 0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 
		0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 0 0 1.6623755 0 0 0;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape4" -p "RightSideTopPillar_geo";
	rename -uid "61FD914F-455B-C4AE-AF9F-FEA46573073A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[0:7]" "e[16:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:23]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:15]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[16:23]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 27 ".uvst[0].uvsp[0:26]" -type "float2" 0.375 0.3125 0.40625
		 0.3125 0.4375 0.3125 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375
		 0.3125 0.625 0.3125 0.375 0.5 0.40625 0.5 0.4375 0.5 0.46875 0.5 0.5 0.5 0.53125
		 0.5 0.5625 0.5 0.59375 0.5 0.625 0.5 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875
		 0.6875 0.5 0.6875 0.53125 0.6875 0.5625 0.6875 0.59375 0.6875 0.625 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 0 -0.70710671 0 0 -0.99999988 -0.70710671 0 -0.70710671
		 -0.99999988 0 0 -0.70710671 0 0.70710671 0 0 0.99999994 0.70710677 0 0.70710677 1 0 0
		 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671 -0.99999988 1 0
		 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0
		 5 13 0 6 14 0 7 15 0 8 16 0 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0;
	setAttr -s 16 -ch 64 ".fc[0:15]" -type "polyFaces" 
		f 4 0 25 -9 -25
		mu 0 4 0 1 10 9
		f 4 1 26 -10 -26
		mu 0 4 1 2 11 10
		f 4 2 27 -11 -27
		mu 0 4 2 3 12 11
		f 4 3 28 -12 -28
		mu 0 4 3 4 13 12
		f 4 4 29 -13 -29
		mu 0 4 4 5 14 13
		f 4 5 30 -14 -30
		mu 0 4 5 6 15 14
		f 4 6 31 -15 -31
		mu 0 4 6 7 16 15
		f 4 7 24 -16 -32
		mu 0 4 7 8 17 16
		f 4 8 33 -17 -33
		mu 0 4 9 10 19 18
		f 4 9 34 -18 -34
		mu 0 4 10 11 20 19
		f 4 10 35 -19 -35
		mu 0 4 11 12 21 20
		f 4 11 36 -20 -36
		mu 0 4 12 13 22 21
		f 4 12 37 -21 -37
		mu 0 4 13 14 23 22
		f 4 13 38 -22 -38
		mu 0 4 14 15 24 23
		f 4 14 39 -23 -39
		mu 0 4 15 16 25 24
		f 4 15 32 -24 -40
		mu 0 4 16 17 26 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "LeftSideBottomPillar_geo" -p "chair_grp_geo";
	rename -uid "ECEE7FA0-4971-38F5-9473-8E8D63625F8B";
	setAttr ".t" -type "double3" 0.54403371429385394 0.72813372572290025 0.062167227281510834 ;
	setAttr ".r" -type "double3" 90 0 90 ;
	setAttr ".s" -type "double3" 0.033546535386596556 0.60421070761739315 0.034082368956424233 ;
createNode mesh -n "LeftSideBottomPillar_geoShape" -p "LeftSideBottomPillar_geo";
	rename -uid "B9130918-4F04-0295-689F-A8803156BAA5";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape5" -p "LeftSideBottomPillar_geo";
	rename -uid "A75F9FB4-493A-090F-A597-C88614243345";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "e[0:7]" "e[16:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:23]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[16:23]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:15]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[16:23]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 27 ".uvst[0].uvsp[0:26]" -type "float2" 0.375 0.3125 0.40625
		 0.3125 0.4375 0.3125 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375
		 0.3125 0.625 0.3125 0.375 0.5 0.40625 0.5 0.4375 0.5 0.46875 0.5 0.5 0.5 0.53125
		 0.5 0.5625 0.5 0.59375 0.5 0.625 0.5 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875
		 0.6875 0.5 0.6875 0.53125 0.6875 0.5625 0.6875 0.59375 0.6875 0.625 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 0 -0.70710671 0 0 -0.99999988 -0.70710671 0 -0.70710671
		 -0.99999988 0 0 -0.70710671 0 0.70710671 0 0 0.99999994 0.70710677 0 0.70710677 1 0 0
		 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671 -0.99999988 1 0
		 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0
		 5 13 0 6 14 0 7 15 0 8 16 0 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0;
	setAttr -s 16 -ch 64 ".fc[0:15]" -type "polyFaces" 
		f 4 0 25 -9 -25
		mu 0 4 0 1 10 9
		f 4 1 26 -10 -26
		mu 0 4 1 2 11 10
		f 4 2 27 -11 -27
		mu 0 4 2 3 12 11
		f 4 3 28 -12 -28
		mu 0 4 3 4 13 12
		f 4 4 29 -13 -29
		mu 0 4 4 5 14 13
		f 4 5 30 -14 -30
		mu 0 4 5 6 15 14
		f 4 6 31 -15 -31
		mu 0 4 6 7 16 15
		f 4 7 24 -16 -32
		mu 0 4 7 8 17 16
		f 4 8 33 -17 -33
		mu 0 4 9 10 19 18
		f 4 9 34 -18 -34
		mu 0 4 10 11 20 19
		f 4 10 35 -19 -35
		mu 0 4 11 12 21 20
		f 4 11 36 -20 -36
		mu 0 4 12 13 22 21
		f 4 12 37 -21 -37
		mu 0 4 13 14 23 22
		f 4 13 38 -22 -38
		mu 0 4 14 15 24 23
		f 4 14 39 -23 -39
		mu 0 4 15 16 25 24
		f 4 15 32 -24 -40
		mu 0 4 16 17 26 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "LeftSideTopPillar_geo" -p "chair_grp_geo";
	rename -uid "EA29E92E-4FE5-9A16-2A3B-76ABBD0C84E3";
	setAttr ".t" -type "double3" 0.54403371429385394 1.0179960558555465 0.062167227281510834 ;
	setAttr ".r" -type "double3" 90 0 90 ;
	setAttr ".s" -type "double3" 0.033546535386596556 0.60421070761739315 0.034082368956424233 ;
createNode mesh -n "LeftSideTopPillar_geoShape" -p "LeftSideTopPillar_geo";
	rename -uid "4BF7D1F4-471B-3E01-714B-32AE9CE59A34";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "ChairLegBL_geo" -p "chair_grp_geo";
	rename -uid "503F0B82-4744-AB11-94A8-0CA0B8998915";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.54256828275900715 1.7033632805259917 0.66194863622917399 ;
	setAttr ".s" -type "double3" 0.12297111295365905 2.9904573375819372 0.13481991745119387 ;
createNode mesh -n "ChairLegBL_geoShape" -p "ChairLegBL_geo";
	rename -uid "2FFE9720-4B9F-2012-9F1D-D79C2D0F5017";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 88 ".pt";
	setAttr ".pt[0]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[1]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[6]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[7]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[8]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[9]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[10]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[11]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[12]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[13]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[14]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[15]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[28]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[29]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[30]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[39]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[40]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[41]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[42]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[43]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[44]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[45]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[46]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[55]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[60]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[61]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[62]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[63]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[64]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[65]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[66]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[67]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[74]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[75]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[76]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[87]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[88]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[89]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[90]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[91]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[92]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[93]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[94]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[95]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[96]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[97]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[98]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[99]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[100]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[101]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[102]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[103]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[104]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[105]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[106]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[107]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[108]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[109]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[110]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[111]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[112]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[113]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[114]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[115]" -type "float3" -0.16008992 0 0 ;
	setAttr ".pt[116]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[117]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[118]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[119]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[120]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[121]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[122]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[123]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[124]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[125]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[126]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[127]" -type "float3" -0.096317202 0 0 ;
	setAttr ".pt[152]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[153]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[154]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[155]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[156]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[157]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[158]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[159]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[160]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[161]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[162]" -type "float3" -0.09743993 0 0 ;
	setAttr ".pt[163]" -type "float3" -0.09743993 0 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape1" -p "ChairLegBL_geo";
	rename -uid "39F2BF7F-4EBB-C8CB-86FA-7CB99E4A051E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 9 "f[2]" "f[7]" "f[13]" "f[17]" "f[21]" "f[25]" "f[33:38]" "f[42:47]" "f[93:95]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 5 "f[3]" "f[39]" "f[41]" "f[60:62]" "f[87:89]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 10 "f[0]" "f[9]" "f[11]" "f[15]" "f[19]" "f[23]" "f[26:31]" "f[40]" "f[49:53]" "f[99:101]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 8 "f[5:6]" "f[10]" "f[14]" "f[18]" "f[22]" "f[54:59]" "f[72:77]" "f[90:92]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 9 "f[4]" "f[8]" "f[12]" "f[16]" "f[20]" "f[24]" "f[63:68]" "f[81:86]" "f[96:98]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 5 "f[1]" "f[32]" "f[48]" "f[69:71]" "f[78:80]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 132 ".uvst[0].uvsp[0:131]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0.044264 0.125 0.044264004 0.375 0.70573604 0.625
		 0.70573604 0.875 0.044264004 0.625 0.044264 0.125 0.077012233 0.375 0.67298782 0.375
		 0.077012241 0.625 0.077012241 0.625 0.67298782 0.875 0.077012233 0.125 0.22516587
		 0.375 0.52483416 0.375 0.22516587 0.625 0.22516587 0.625 0.52483416 0.875 0.22516587
		 0.125 0.24060583 0.375 0.50939417 0.375 0.24060583 0.625 0.24060583 0.625 0.50939417
		 0.875 0.24060583 0.125 0.24959019 0.375 0.50040978 0.375 0.24959019 0.625 0.24959019
		 0.625 0.50040978 0.875 0.24959019 0.38080713 0 0.38080713 1 0.38080698 0.044264 0.38080713
		 0.077012233 0.38080713 0.22516586 0.38080713 0.24060583 0.38080713 0.24959019 0.38080713
		 0.25 0.38080713 0.5 0.38080698 0.50040978 0.38080698 0.50939417 0.38080698 0.52483416
		 0.38080698 0.67298782 0.38080713 0.70573604 0.38080713 0.75 0.61768234 0.044264 0.61768228
		 0 0.61768228 1 0.61768228 0.75 0.61768228 0.70573604 0.61768234 0.67298782 0.61768234
		 0.52483416 0.61768234 0.50939417 0.61768234 0.50040978 0.61768228 0.5 0.61768228
		 0.25 0.61768228 0.24959019 0.61768228 0.24060583 0.61768228 0.22516587 0.61768228
		 0.077012241 0.36668921 0.25 0.375 0.25831079 0.36668923 0.24959019 0.36668923 0.24060583
		 0.36668923 0.22516587 0.36668923 0.077012241 0.36668921 0.044264004 0.36668923 0
		 0.375 0.99168926 0.38080713 0.99168926 0.61768228 0.99168921 0.625 0.99168926 0.63331074
		 0 0.63331074 0.044264 0.63331079 0.077012241 0.63331079 0.22516589 0.63331079 0.24060585
		 0.63331079 0.24959019 0.625 0.25831079 0.63331079 0.25 0.61768228 0.25831074 0.38080713
		 0.25831079 0.13624567 0 0.375 0.76124567 0.1362458 0.044264004 0.13624567 0.077012233
		 0.13624567 0.22516587 0.13624567 0.24060583 0.13624567 0.24959019 0.1362458 0.25
		 0.375 0.48875421 0.38080713 0.48875421 0.61768228 0.4887543 0.625 0.48875421 0.86375415
		 0.25 0.86375415 0.24959019 0.86375415 0.24060583 0.86375415 0.22516587 0.86375415
		 0.077012233 0.86375433 0.044264004 0.62499994 0.76124567 0.86375433 0 0.61768228
		 0.76124579 0.3808071 0.76124567 0.375 0.0005250817 0.36668923 0.00052505871 0.13624567
		 0.0005250817 0.125 0.00052505871 0.375 0.74947494 0.38080713 0.74947494 0.61768228
		 0.74947488 0.625 0.74947494 0.875 0.00052505871 0.86375433 0.00052505871 0.63331068
		 0.0005250817 0.62499994 0.0005250817 0.61768228 0.00052505865 0.38080713 0.0005250817;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 52 ".pt";
	setAttr ".pt[0]" -type "float3" 3.3306691e-16 5.5511151e-16 1.1275291 ;
	setAttr ".pt[1]" -type "float3" -3.7252903e-09 4.6566184e-10 1.1275291 ;
	setAttr ".pt[6]" -type "float3" 3.3306691e-16 5.5511151e-16 1.1275294 ;
	setAttr ".pt[7]" -type "float3" 3.3306691e-16 5.5511151e-16 1.1275294 ;
	setAttr ".pt[8]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[9]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[10]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[11]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[12]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[13]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[14]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[15]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[28]" -type "float3" 0.066556238 5.5511151e-16 1.0577301 ;
	setAttr ".pt[29]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[30]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[39]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[40]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[41]" -type "float3" 0.066556238 5.5511151e-16 1.1973283 ;
	setAttr ".pt[42]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[43]" -type "float3" -0.065712795 4.6566184e-10 1.0577301 ;
	setAttr ".pt[44]" -type "float3" -0.065712869 5.5511151e-16 1.1973283 ;
	setAttr ".pt[45]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[46]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[55]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[60]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[61]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[62]" -type "float3" 0.069799006 5.5511151e-16 1.0623703 ;
	setAttr ".pt[63]" -type "float3" 0.06655632 5.5511151e-16 1.0623707 ;
	setAttr ".pt[64]" -type "float3" -0.065712869 5.5511151e-16 1.0623705 ;
	setAttr ".pt[65]" -type "float3" 3.3306691e-16 5.5511151e-16 1.1275288 ;
	setAttr ".pt[66]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[67]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[74]" -type "float3" 0.069799006 5.5511151e-16 1.191049 ;
	setAttr ".pt[75]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[76]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[87]" -type "float3" 1.110223e-16 2.220446e-16 -0.1418716 ;
	setAttr ".pt[88]" -type "float3" 3.3306691e-16 2.220446e-16 0.1620083 ;
	setAttr ".pt[89]" -type "float3" -0.069799006 5.5511151e-16 1.191049 ;
	setAttr ".pt[90]" -type "float3" -0.065712869 5.5511151e-16 1.1910485 ;
	setAttr ".pt[91]" -type "float3" 0.06655632 5.5511151e-16 1.1910486 ;
	setAttr ".pt[92]" -type "float3" 3.7252903e-09 1.110223e-16 1.1275291 ;
	setAttr ".pt[93]" -type "float3" 3.7252903e-09 9.9920072e-16 1.127529 ;
	setAttr ".pt[94]" -type "float3" 3.7252903e-09 1.110223e-16 1.1275291 ;
	setAttr ".pt[95]" -type "float3" 3.7252903e-09 9.9920072e-16 1.127529 ;
	setAttr ".pt[96]" -type "float3" 5.5879354e-09 9.9920072e-16 1.127529 ;
	setAttr ".pt[97]" -type "float3" -1.8626448e-09 1.110223e-16 1.127529 ;
	setAttr ".pt[98]" -type "float3" -3.7252903e-09 9.9920072e-16 1.127529 ;
	setAttr ".pt[99]" -type "float3" -3.7252903e-09 9.9920072e-16 1.1275291 ;
	setAttr ".pt[100]" -type "float3" -3.7252903e-09 1.110223e-16 1.127529 ;
	setAttr ".pt[101]" -type "float3" -3.7252903e-09 1.110223e-16 1.1275291 ;
	setAttr ".pt[102]" -type "float3" -1.8626448e-09 9.9920072e-16 1.1275291 ;
	setAttr ".pt[103]" -type "float3" 3.7252903e-09 1.110223e-16 1.1275291 ;
	setAttr -s 104 ".vt[0:103]"  -0.45144573 -0.49999997 -0.05187881 0.45144573 -0.49999997 -0.05187881
		 -0.5 0.49910232 0.49999997 0.5 0.49910232 0.49999997 -0.5 0.49910232 -0.059761293
		 0.5 0.49910232 -0.059761293 -0.45144573 -0.49999997 -0.9547708 0.45144573 -0.49999997 -0.9547708
		 -0.5 -0.32294399 0.41244549 -0.5 -0.32294399 -0.58755457 0.5 -0.32294399 -0.58755457
		 0.5 -0.32294399 0.41244555 -0.5 -0.19195107 -0.49999997 -0.5 -0.19195107 0.49999994
		 0.5 -0.19195107 0.5 0.5 -0.19195107 -0.5 -0.5 0.35460174 -0.49999997 -0.5 0.35460177 0.5
		 0.5 0.35460177 0.5 0.5 0.35460177 -0.5 -0.49999997 0.43846977 -0.37809029 -0.5 0.43846977 0.49999997
		 0.49999997 0.43846977 0.5 0.49999997 0.43846977 -0.37809029 -0.52716219 0.49449024 -0.089229576
		 -0.52716219 0.49449024 0.51558149 0.52716219 0.49449024 0.51558149 0.52716219 0.49449024 -0.089229576
		 -0.43047282 -0.49999997 -0.05187881 -0.47677201 -0.32294399 0.41244549 -0.47677141 -0.19195107 0.49999994
		 -0.47677147 0.35460177 0.5 -0.47677147 0.43846977 0.49999997 -0.50267178 0.49449024 0.51558149
		 -0.47677147 0.49910232 0.49999997 -0.47677147 0.49910232 -0.059761293 -0.50267237 0.49449024 -0.089229576
		 -0.47677198 0.43846977 -0.37809029 -0.47677201 0.35460177 -0.5 -0.47677201 -0.19195107 -0.49999997
		 -0.47677147 -0.32294399 -0.58755457 -0.43047282 -0.49999997 -0.9547708 0.47072929 -0.32294399 0.41244555
		 0.42501727 -0.49999997 -0.05187881 0.42501727 -0.49999997 -0.9547708 0.47072908 -0.32294399 -0.58755457
		 0.47072929 -0.19195107 -0.5 0.47072929 0.3546018 -0.5 0.47072926 0.43846977 -0.37809029
		 0.49630135 0.49449024 -0.089229576 0.47072908 0.49910232 -0.059761293 0.47072908 0.49910232 0.49999997
		 0.49630114 0.49449024 0.51558149 0.47072905 0.43846977 0.5 0.47072908 0.35460177 0.5
		 0.47072908 -0.19195107 0.5 -0.5 0.49910232 0.48139173 -0.52716219 0.49449024 0.49547577
		 -0.5 0.43846977 0.47080961 -0.5 0.35460177 0.466757 -0.5 -0.19195107 0.466757 -0.5 -0.32294399 0.37920231
		 -0.45144573 -0.49999997 -0.081893653 -0.43047282 -0.49999997 -0.081893653 0.42501727 -0.49999997 -0.081893831
		 0.45144573 -0.49999997 -0.081893653 0.5 -0.32294399 0.37920254 0.5 -0.19195107 0.46675682
		 0.5 0.3546018 0.46675682 0.49999997 0.43846977 0.47080949 0.52716219 0.49449027 0.49547568
		 0.5 0.49910232 0.48139173 0.47072908 0.5 0.48139182 -0.47677147 0.5 0.48139173 -0.45144573 -0.49999997 -0.91415632
		 -0.5 -0.32294399 -0.54257143 -0.5 -0.19195104 -0.45501727 -0.5 0.35460171 -0.4550173
		 -0.49999994 0.43846977 -0.3385914 -0.52716219 0.49449021 -0.062023524 -0.5 0.49910232 -0.03458143
		 -0.47677147 0.5 -0.03458143 0.47072908 0.5 -0.034581698 0.5 0.49910232 -0.03458143
		 0.52716219 0.49449024 -0.062023237 0.49999997 0.43846977 -0.33859098 0.5 0.35460177 -0.45501679
		 0.5 -0.19195107 -0.45501679 0.5 -0.32294399 -0.54257184 0.45144573 -0.49999997 -0.91415632
		 0.42501727 -0.49999997 -0.91415584 -0.43047282 -0.49999997 -0.91415632 -0.45202172 -0.49789962 -0.04637076
		 -0.45202169 -0.49789974 -0.076424144 -0.45202172 -0.49789962 -0.90974838 -0.45202169 -0.49789974 -0.9504149
		 -0.43102202 -0.49789974 -0.9504149 0.42555952 -0.49789962 -0.95041472 0.45202169 -0.49789974 -0.9504149
		 0.45202169 -0.49789974 -0.90974861 0.45202172 -0.49789962 -0.076423898 0.45202172 -0.49789962 -0.04637076
		 0.42555952 -0.49789974 -0.046371005 -0.43102205 -0.49789962 -0.04637076;
	setAttr -s 204 ".ed";
	setAttr ".ed[0:165]"  0 28 0 2 34 0 4 35 0 6 41 0 0 92 0 1 101 0 2 56 0 3 71 0
		 4 24 0 5 27 0 6 74 0 7 89 0 8 13 0 9 95 0 10 98 0 11 14 0 8 61 1 9 40 1 10 88 1 11 42 1
		 12 9 0 13 17 0 14 18 0 15 10 0 12 76 1 13 30 1 14 67 1 15 46 1 16 12 0 17 21 0 18 22 0
		 19 15 0 16 77 1 17 31 1 18 68 1 19 47 1 20 16 0 21 25 0 22 26 0 23 19 0 20 78 1 21 32 1
		 22 69 1 23 48 1 24 20 0 25 2 0 26 3 0 27 23 0 24 79 1 25 33 1 26 70 1 27 49 1 28 43 0
		 29 8 1 30 55 1 31 54 1 32 53 1 33 52 1 34 51 0 35 50 0 36 24 1 37 20 1 38 16 1 39 12 1
		 40 45 1 41 44 0 28 103 1 29 30 1 30 31 1 31 32 1 32 33 1 33 34 1 34 73 1 35 36 1
		 36 37 1 37 38 1 38 39 1 39 40 1 40 96 1 41 91 1 42 29 1 43 1 0 44 7 0 45 10 1 46 39 1
		 47 38 1 48 37 1 49 36 1 50 5 0 51 3 0 52 26 1 53 22 1 54 18 1 55 14 1 42 102 1 43 64 1
		 44 97 1 45 46 1 46 47 1 47 48 1 48 49 1 49 50 1 50 82 1 51 52 1 52 53 1 53 54 1 54 55 1
		 55 42 1 56 80 0 57 25 1 58 21 1 59 17 1 60 13 1 61 75 1 62 0 0 63 28 1 64 90 1 65 1 0
		 66 11 1 67 87 1 68 86 1 69 85 1 70 84 1 71 83 0 72 51 1 73 81 1 56 57 1 57 58 1 58 59 1
		 59 60 1 60 61 1 61 93 1 62 63 1 63 64 1 64 65 1 65 100 1 66 67 1 67 68 1 68 69 1
		 69 70 1 70 71 1 71 72 1 72 73 1 73 56 1 74 62 0 75 9 1 76 60 1 77 59 1 78 58 1 79 57 1
		 80 4 0 81 35 1 82 72 1 83 5 0 84 27 1 85 23 1 86 19 1 87 15 1 88 66 1 89 65 0 90 44 1
		 91 63 1 74 94 1 75 76 1 76 77 1 77 78 1;
	setAttr ".ed[166:203]" 78 79 1 79 80 1 80 81 1 81 82 1 82 83 1 83 84 1 84 85 1
		 85 86 1 86 87 1 87 88 1 88 99 1 89 90 1 90 91 1 91 74 1 92 8 0 93 62 1 94 75 1 95 6 0
		 96 41 1 97 45 1 98 7 0 99 89 1 100 66 1 101 11 0 102 43 1 103 29 1 92 93 1 93 94 1
		 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 100 1 100 101 1 101 102 1 102 103 1 103 92 1;
	setAttr -s 102 -ch 408 ".fc[0:101]" -type "polyFaces" 
		f 4 0 66 203 -5
		mu 0 4 0 44 131 118
		f 4 1 72 143 -7
		mu 0 4 2 51 95 75
		f 4 195 184 -4 -184
		mu 0 4 122 123 58 6
		f 4 132 115 -1 -115
		mu 0 4 82 83 45 8
		f 4 -118 135 200 -6
		mu 0 4 1 86 128 129
		f 4 192 181 114 4
		mu 0 4 118 119 81 0
		f 4 126 109 45 6
		mu 0 4 74 76 40 2
		f 4 2 73 60 -9
		mu 0 4 4 52 53 39
		f 4 50 140 -8 -47
		mu 0 4 41 91 93 3
		f 4 49 71 -2 -46
		mu 0 4 40 50 51 2
		f 4 130 -17 12 -113
		mu 0 4 79 80 14 22
		f 4 -54 67 -26 -13
		mu 0 4 14 46 47 22
		f 4 -119 136 -27 -16
		mu 0 4 19 87 88 23
		f 4 -64 77 -18 -21
		mu 0 4 21 56 57 16
		f 4 129 112 21 -112
		mu 0 4 78 79 22 28
		f 4 25 68 -34 -22
		mu 0 4 22 47 48 28
		f 4 26 137 -35 -23
		mu 0 4 23 88 89 29
		f 4 -63 76 63 -29
		mu 0 4 27 55 56 21
		f 4 128 111 29 -111
		mu 0 4 77 78 28 34
		f 4 33 69 -42 -30
		mu 0 4 28 48 49 34
		f 4 34 138 -43 -31
		mu 0 4 29 89 90 35
		f 4 -62 75 62 -37
		mu 0 4 33 54 55 27
		f 4 127 110 37 -110
		mu 0 4 76 77 34 40
		f 4 41 70 -50 -38
		mu 0 4 34 49 50 40
		f 4 42 139 -51 -39
		mu 0 4 35 90 91 41
		f 4 -61 74 61 -45
		mu 0 4 39 53 54 33
		f 4 201 190 81 5
		mu 0 4 129 130 60 1
		f 4 107 -20 15 -94
		mu 0 4 73 59 19 23
		f 4 106 93 22 -93
		mu 0 4 72 73 23 29
		f 4 105 92 30 -92
		mu 0 4 71 72 29 35
		f 4 104 91 38 -91
		mu 0 4 70 71 35 41
		f 4 103 90 46 -90
		mu 0 4 69 70 41 3
		f 4 141 124 89 7
		mu 0 4 92 94 69 3
		f 4 101 88 9 51
		mu 0 4 67 68 5 42
		f 4 100 -52 47 43
		mu 0 4 66 67 42 36
		f 4 99 -44 39 35
		mu 0 4 65 66 36 30
		f 4 98 -36 31 27
		mu 0 4 64 65 30 24
		f 4 97 -28 23 -84
		mu 0 4 63 64 24 17
		f 4 96 197 186 -83
		mu 0 4 62 124 125 7
		f 4 95 134 117 -82
		mu 0 4 61 84 85 9
		f 4 202 -67 52 -191
		mu 0 4 130 131 44 60
		f 4 -116 133 -96 -53
		mu 0 4 45 83 84 61
		f 4 -185 196 -97 -66
		mu 0 4 58 123 124 62
		f 4 -78 -85 -98 -65
		mu 0 4 57 56 64 63
		f 4 -77 -86 -99 84
		mu 0 4 56 55 65 64
		f 4 -76 -87 -100 85
		mu 0 4 55 54 66 65
		f 4 -75 -88 -101 86
		mu 0 4 54 53 67 66
		f 4 -74 59 -102 87
		mu 0 4 53 52 68 67
		f 4 142 -73 58 -125
		mu 0 4 94 95 51 69
		f 4 -72 57 -104 -59
		mu 0 4 51 50 70 69
		f 4 -71 56 -105 -58
		mu 0 4 50 49 71 70
		f 4 -70 55 -106 -57
		mu 0 4 49 48 72 71
		f 4 -69 54 -107 -56
		mu 0 4 48 47 73 72
		f 4 -68 -81 -108 -55
		mu 0 4 47 46 59 73
		f 4 48 167 150 8
		mu 0 4 38 102 103 13
		f 4 40 166 -49 44
		mu 0 4 32 101 102 38
		f 4 32 165 -41 36
		mu 0 4 26 100 101 32
		f 4 24 164 -33 28
		mu 0 4 20 99 100 26
		f 4 -146 163 -25 20
		mu 0 4 15 98 99 20
		f 4 10 162 194 183
		mu 0 4 12 96 120 121
		f 4 3 79 179 -11
		mu 0 4 6 58 117 97
		f 4 178 -80 65 -161
		mu 0 4 116 117 58 62
		f 4 177 160 82 11
		mu 0 4 114 116 62 7
		f 4 198 187 -12 -187
		mu 0 4 126 127 115 10
		f 4 175 -19 -24 -158
		mu 0 4 112 113 18 25
		f 4 174 157 -32 -157
		mu 0 4 111 112 25 31
		f 4 173 156 -40 -156
		mu 0 4 110 111 31 37
		f 4 172 155 -48 -155
		mu 0 4 109 110 37 43
		f 4 171 154 -10 -154
		mu 0 4 108 109 43 11
		f 4 102 170 153 -89
		mu 0 4 68 106 107 5
		f 4 -152 169 -103 -60
		mu 0 4 52 105 106 68
		f 4 168 151 -3 -151
		mu 0 4 104 105 52 4
		f 4 193 -163 144 -182
		mu 0 4 119 120 96 81
		f 4 -164 -114 -131 -147
		mu 0 4 99 98 80 79
		f 4 -165 146 -130 -148
		mu 0 4 100 99 79 78
		f 4 -166 147 -129 -149
		mu 0 4 101 100 78 77
		f 4 -167 148 -128 -150
		mu 0 4 102 101 77 76
		f 4 -168 149 -127 108
		mu 0 4 103 102 76 74
		f 4 -144 125 -169 -109
		mu 0 4 75 95 105 104
		f 4 -170 -126 -143 -153
		mu 0 4 106 105 95 94
		f 4 -171 152 -142 123
		mu 0 4 107 106 94 92
		f 4 -141 122 -172 -124
		mu 0 4 93 91 109 108
		f 4 -140 121 -173 -123
		mu 0 4 91 90 110 109
		f 4 -139 120 -174 -122
		mu 0 4 90 89 111 110
		f 4 -138 119 -175 -121
		mu 0 4 89 88 112 111
		f 4 -137 -159 -176 -120
		mu 0 4 88 87 113 112
		f 4 199 -136 -160 -188
		mu 0 4 127 128 86 115
		f 4 -135 116 -178 159
		mu 0 4 85 84 116 114
		f 4 -134 -162 -179 -117
		mu 0 4 84 83 117 116
		f 4 -180 161 -133 -145
		mu 0 4 97 117 83 82
		f 4 131 -193 180 16
		mu 0 4 80 119 118 14
		f 4 -183 -194 -132 113
		mu 0 4 98 120 119 80
		f 4 -195 182 145 13
		mu 0 4 121 120 98 15
		f 4 17 78 -196 -14
		mu 0 4 16 57 123 122
		f 4 -197 -79 64 -186
		mu 0 4 124 123 57 63
		f 4 -198 185 83 14
		mu 0 4 125 124 63 17
		f 4 176 -199 -15 18
		mu 0 4 113 127 126 18
		f 4 -189 -200 -177 158
		mu 0 4 87 128 127 113
		f 4 -201 188 118 -190
		mu 0 4 129 128 87 19
		f 4 94 -202 189 19
		mu 0 4 59 130 129 19
		f 4 -192 -203 -95 80
		mu 0 4 46 131 130 59
		f 4 -204 191 53 -181
		mu 0 4 118 131 46 14;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "FrontBottomAjoin_geo" -p "chair_grp_geo";
	rename -uid "87E2EDC0-4D35-8D5B-5A0A-899D12D5307B";
	setAttr ".t" -type "double3" 0 0.90722979612801513 -0.49769992960198089 ;
	setAttr ".s" -type "double3" 0.97079427114698358 0.10850627479800959 0.024067106731412824 ;
createNode mesh -n "FrontBottomAjoin_geoShape" -p "FrontBottomAjoin_geo";
	rename -uid "A489EA5F-4C11-10BD-A88F-0C80CD8AD418";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 3 "f[2]" "f[7]" "f[13]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 3 "f[3]" "f[17]" "f[29]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 3 "f[0]" "f[9]" "f[11]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 4 "f[5:6]" "f[10]" "f[14:16]" "f[22:24]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 5 "f[4]" "f[8]" "f[12]" "f[18:20]" "f[26:28]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 3 "f[1]" "f[21]" "f[25]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 50 ".uvst[0].uvsp[0:49]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0.0029867 0.125 0.002986744 0.375 0.74701321
		 0.625 0.74701321 0.875 0.002986744 0.625 0.0029867 0.125 0.2398745 0.375 0.51012546
		 0.375 0.23987442 0.625 0.23987442 0.625 0.51012546 0.875 0.2398745 0.36848226 0.25
		 0.375 0.25651774 0.36848226 0.23987442 0.36848226 0.0029867012 0.36848226 0 0.375
		 0.99348223 0.625 0.99348223 0.63151777 0 0.63151777 0.0029867012 0.63151777 0.23987444
		 0.625 0.25651774 0.63151777 0.25 0.15016584 0 0.375 0.7751658 0.15016583 0.0029867396
		 0.15016584 0.23987448 0.15016583 0.25 0.375 0.47483417 0.625 0.47483417 0.84983414
		 0.25 0.84983414 0.2398745 0.84983414 0.0029867394 0.62499994 0.7751658 0.84983414
		 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 -0.4880532 0.5 -0.5 -0.48805302 -0.5
		 0.5 -0.48805302 -0.5 0.5 -0.4880532 0.5 -0.5 0.45949802 -0.5 -0.5 0.45949769 0.5
		 0.5 0.45949769 0.5 0.5 0.45949802 -0.5 -0.5 0.5 0.47392911 -0.5 0.45949769 0.47392899
		 -0.5 -0.4880532 0.47392911 -0.5 -0.5 0.47392899 0.5 -0.5 0.47392899 0.5 -0.4880532 0.47392899
		 0.5 0.45949769 0.47392911 0.5 0.5 0.47392911 -0.5 -0.5 -0.39933664 -0.5 -0.48805305 -0.39933667
		 -0.5 0.45949799 -0.39933664 -0.5 0.5 -0.39933667 0.5 0.5 -0.39933667 0.5 0.45949799 -0.39933667
		 0.5 -0.48805305 -0.39933664 0.5 -0.5 -0.39933664;
	setAttr -s 60 ".ed[0:59]"  0 1 0 2 3 0 4 5 0 6 7 0 0 8 0 1 11 0 2 16 0
		 3 23 0 4 12 0 5 15 0 6 24 0 7 31 0 8 13 0 9 6 0 10 7 0 11 14 0 8 18 1 9 10 1 10 30 1
		 11 8 1 12 9 0 13 2 0 14 3 0 15 10 0 12 26 1 13 14 1 14 22 1 15 12 1 16 27 0 17 13 1
		 18 25 1 19 0 0 20 1 0 21 11 1 22 29 1 23 28 0 16 17 1 17 18 1 18 19 1 19 20 1 20 21 1
		 21 22 1 22 23 1 23 16 1 24 19 0 25 9 1 26 17 1 27 4 0 28 5 0 29 15 1 30 21 1 31 20 0
		 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1 29 30 1 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 0 5 19 -5
		mu 0 4 0 1 19 14
		f 4 1 7 43 -7
		mu 0 4 2 3 36 27
		f 4 17 14 -4 -14
		mu 0 4 16 17 7 6
		f 4 39 32 -1 -32
		mu 0 4 31 32 9 8
		f 4 -33 40 33 -6
		mu 0 4 1 33 34 19
		f 4 38 31 4 16
		mu 0 4 29 30 0 14
		f 4 36 29 21 6
		mu 0 4 26 28 22 2
		f 4 2 9 27 -9
		mu 0 4 4 5 24 21
		f 4 26 42 -8 -23
		mu 0 4 23 35 37 3
		f 4 25 22 -2 -22
		mu 0 4 22 23 3 2
		f 4 37 -17 12 -30
		mu 0 4 28 29 14 22
		f 4 -20 15 -26 -13
		mu 0 4 14 19 23 22
		f 4 -34 41 -27 -16
		mu 0 4 19 34 35 23
		f 4 -28 23 -18 -21
		mu 0 4 21 24 17 16
		f 4 24 54 47 8
		mu 0 4 20 41 42 13
		f 4 -46 53 -25 20
		mu 0 4 15 40 41 20
		f 4 10 52 45 13
		mu 0 4 12 38 40 15
		f 4 3 11 59 -11
		mu 0 4 6 7 48 39
		f 4 58 -12 -15 18
		mu 0 4 47 49 10 18
		f 4 57 -19 -24 -50
		mu 0 4 46 47 18 25
		f 4 56 49 -10 -49
		mu 0 4 45 46 25 11
		f 4 55 48 -3 -48
		mu 0 4 43 44 5 4
		f 4 -53 44 -39 30
		mu 0 4 40 38 30 29
		f 4 -54 -31 -38 -47
		mu 0 4 41 40 29 28
		f 4 -55 46 -37 28
		mu 0 4 42 41 28 26
		f 4 -44 35 -56 -29
		mu 0 4 27 36 44 43
		f 4 -43 34 -57 -36
		mu 0 4 37 35 46 45
		f 4 -42 -51 -58 -35
		mu 0 4 35 34 47 46
		f 4 -41 -52 -59 50
		mu 0 4 34 33 49 47
		f 4 -60 51 -40 -45
		mu 0 4 39 48 32 31;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "FrontTopAjoin_geo" -p "chair_grp_geo";
	rename -uid "B5452E66-4036-4139-1CF0-30B166C89698";
	setAttr ".t" -type "double3" 0 1.385 -0.43476085453932783 ;
	setAttr ".s" -type "double3" 0.97079427114698358 0.10850627479800959 0.024067106731412824 ;
createNode mesh -n "FrontTopAjoin_geoShape" -p "FrontTopAjoin_geo";
	rename -uid "E5861EF8-427A-B11E-B5F7-7BAC9ED7C3FE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "ChairLegFR_geo" -p "chair_grp_geo";
	rename -uid "7803592E-44E9-E6F7-FC1B-EF891A7DD420";
	setAttr ".rp" -type "double3" -0.55706424284135747 -1.1792925343437001e-16 -0.4876772645825912 ;
	setAttr ".sp" -type "double3" -0.55706424284135747 2.8303020824248802e-15 -0.4876772645825912 ;
createNode mesh -n "ChairLegFR_geoShape" -p "ChairLegFR_geo";
	rename -uid "C6191170-4ED8-7B8B-DD77-C2B684C2BAAE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 15 "f[3]" "f[5]" "f[9]" "f[15:16]" "f[27]" "f[30]" "f[35]" "f[40]" "f[48]" "f[50]" "f[56]" "f[58]" "f[61]" "f[63]" "f[89:91]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 16 "f[1]" "f[12]" "f[18]" "f[21]" "f[23:24]" "f[28:29]" "f[36]" "f[42]" "f[44]" "f[47]" "f[53:54]" "f[65]" "f[68]" "f[78]" "f[81]" "f[83:85]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 18 "f[0]" "f[2]" "f[6]" "f[14]" "f[20]" "f[26]" "f[31]" "f[37]" "f[39]" "f[45:46]" "f[49]" "f[57]" "f[59]" "f[64]" "f[66]" "f[77]" "f[82]" "f[92:93]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 15 "f[8]" "f[11]" "f[13]" "f[17]" "f[25]" "f[32:34]" "f[41]" "f[51:52]" "f[55]" "f[60]" "f[62]" "f[69]" "f[71]" "f[79]" "f[86:88]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 0;
	setAttr ".pv" -type "double2" 0.5 0.22516585886478424 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 115 ".uvst[0].uvsp[0:114]" -type "float2" 0.38703677 0.70648128
		 0.38674906 0.037726711 0.61391932 0.035723533 0.38620126 0.23758426 0.61379862 0.23758425
		 0.13690287 0.076971926 0.36316109 0.15108906 0.61379862 0.67298782 0.38620126 0.59891099
		 0.61379862 0.077012219 0.38619298 0.044264 0.38620126 0.077012219 0.61379862 0.15108906
		 0.61376649 0.70573604 0.38620132 0.67298782 0.13718042 0.04409752 0.36316112 0.077061288
		 0.86309713 0.076971926 0.63689393 0.044398244 0.63683891 0.077061281 0.86316109 0.15108906
		 0.36310604 0.044398244 0.38622415 0.70573604 0.86280978 0.044097349 0.61380696 0.044264
		 0.81872332 0.23758525 0.63683891 0.22516584 0.61379856 0.52483416 0.32341939 0.41867793
		 0.38620126 0.22516584 0.36943954 0.25153166 0.13683893 0.22416066 0.61085373 0.49618545
		 0.3806408 0.2639474 0.61085373 0.26733533 0.36316109 0.22516586 0.13683891 0.15108906
		 0.38620126 0.52483416 0.61379862 0.59891099 0.86316109 0.22416063 0.63683891 0.15108906
		 0.61379862 0.22516586 0.38620126 0.15108906 0.83622336 0.03691493 0.27343029 0.32003218
		 0.64615387 0.23759028 0.375 0.044264 0.375 0.077012241 0.125 0.044264004 0.375 0.70573604
		 0.39929354 0.71308565 0.38714677 0.74947494 0.13895366 0.00052505999 0.375 0.7076726
		 0.875 0.044264004 0.625 0.70573604 0.86097401 0.005103583 0.61279458 0.74947494 0.8505891
		 0.037090138 0.625 0.044264 0.625 0.077012241 0.125 0.077012233 0.375 0.67298782 0.375
		 0.15108906 0.625 0.15108906 0.625 0.67298782 0.875 0.077012233 0.125 0.22516587 0.375
		 0.52483416 0.375 0.59891099 0.125 0.15108906 0.625 0.52483416 0.875 0.22516587 0.875
		 0.15108906 0.625 0.59891099 0.38731262 0.0085392622 0.375 0.039034069 0.36214566
		 0.00052508054 0.61329222 0.0081484532 0.63781172 0.004937666 0.375 0.22516587 0.37016019
		 0.2520009 0.37502864 0.23764789 0.38572323 0.23991224 0.30927309 0.41486335 0.625
		 0.22516587 0.625 0.27114981 0.64614981 0.25 0.61354536 0.24020338 0.6250425 0.23758429
		 0.625 0.5 0.875 0.25 0.375 0.74947494 0.125 0.00052505871 0.375 0.74947494 0.875
		 0.00052505871 0.625 0.74947494 0.375 0.0005250817 0.62499994 0.0005250817 0.375 0.25
		 0.625 0.25 0.36217394 0.0018169699 0.375 0.0018130182 0.38727966 0.0095912134 0.61330736
		 0.0092119118 0.62499994 0.0018130396 0.63778472 0.0060996423 0.86102808 0.0062517938
		 0.875 0.0018130175 0.625 0.74818701 0.61282319 0.74818701 0.38711959 0.74818701 0.37500003
		 0.74818701 0.125 0.0018130175 0.13890144 0.0018080948;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 96 ".pt[0:95]" -type "float3"  -0.15515031 0.69116235 -0.74501741 
		-0.11562257 0.69116235 -0.74501741 -0.11562257 0.69157892 -0.70347106 -0.15517977 
		0.87389791 -0.93227154 -0.11562257 0.87389791 -0.93227154 -0.11562257 0.87402296 
		-0.89016354 -0.15526102 0.69116235 0.14417356 -0.11562257 0.69116235 0.14417356 -0.11578338 
		0.69080436 0.10240287 -0.19676195 0.49595451 0.41816005 -0.19641085 0.49595451 0.37179163 
		-0.15798147 0.49595451 0.37329248 -0.95883483 0.69116235 0.14417356 -0.99850506 0.69116235 
		0.14417356 -0.99834365 0.690804 0.1023698 -0.95614576 0.49595451 0.37305972 -0.91736484 
		0.49595451 0.37136933 -0.91717792 0.49595451 0.41816005 -0.99850506 0.69157892 -0.70347106 
		-0.99850506 0.69116235 -0.74501741 -0.95897669 0.69116235 -0.74501741 -0.95894724 
		0.87389791 -0.93227154 -0.99850506 0.87389791 -0.93227154 -0.99850506 0.87402296 
		-0.89016354 -0.15517977 0.87389791 -0.043081742 -0.11562257 0.87389791 -0.043081742 
		-0.11562257 0.87377286 -0.085189924 -0.11562257 0.88047242 -0.89016354 -0.11562257 
		0.88047242 -0.93227154 -0.15517977 0.88047242 -0.93227154 -0.99850506 0.88047242 
		-0.89016354 -0.99850506 0.88047242 -0.93227154 -0.95894724 0.88047242 -0.93227154 
		-0.99850506 0.87377286 -0.085189924 -0.99850506 0.87389791 -0.043081742 -0.95894724 
		0.87389791 -0.043081742 -0.15517977 1.0238887 -0.043081742 -0.11562257 1.0238887 
		-0.043081742 -0.11562257 1.0219426 -0.085189924 -0.11562257 0.88047236 -0.085189924 
		-0.11562257 0.88047236 -0.043081742 -0.15517977 0.88047236 -0.043081742 -0.99850506 
		1.0219426 -0.085189924 -0.99850506 1.0238887 -0.043081742 -0.95894724 1.0238887 -0.043081742 
		-0.95894724 0.88047242 -0.043081742 -0.99850506 0.88047242 -0.043081742 -0.99850506 
		0.88047242 -0.085189924 -0.19653799 0.4959546 -0.38570714 -0.15798147 0.49595457 
		-0.34437445 -0.19549181 0.49595457 -0.34599957 -0.91759914 0.4959546 -0.38570714 
		-0.91863459 0.49595457 -0.34599957 -0.95614576 0.49595457 -0.34451312 -0.15517977 
		1.0179893 -0.93227154 -0.11562257 1.0179893 -0.93227154 -0.11562257 1.0179893 -0.89016354 
		-0.11562257 1.0287082 -0.89016354 -0.11562257 1.0262989 -0.89016354 -0.11562257 1.0262989 
		-0.93227154 -0.15517977 1.0262989 -0.93227154 -0.15517977 1.0287082 -0.93227154 -0.15517977 
		1.0287082 -0.89016354 -0.15517977 1.0287082 -0.45365846 -0.11562257 1.0287082 -0.45365846 
		-0.11562257 1.0262989 -0.45813733 -0.99850506 1.0179893 -0.89016354 -0.99850506 1.0179893 
		-0.93227154 -0.95894724 1.0179893 -0.93227154 -0.99850506 1.0287082 -0.89016354 -0.95894724 
		1.0287082 -0.89016354 -0.95894724 1.0287082 -0.93227154 -0.95894724 1.0262989 -0.93227154 
		-0.99850506 1.0262989 -0.93227154 -0.99850506 1.0262989 -0.89016354 -0.99850506 1.0262989 
		-0.45813733 -0.99850506 1.0287082 -0.45365846 -0.95894724 1.0287082 -0.45365846 -0.15798147 
		0.49595451 0.41816005 -0.95614576 0.49595451 0.41816005 -0.15798147 0.4959546 -0.38570714 
		-0.95614576 0.4959546 -0.38570714 -0.11562257 1.0287082 -0.93227154 -0.99850506 1.0287082 
		-0.93227154 -0.15673418 0.49873644 -0.35522842 -0.15673418 0.49872419 -0.3965674 
		-0.19531929 0.49872419 -0.3965674 -0.91881752 0.49872419 -0.3965674 -0.95739305 0.49872425 
		-0.39656764 -0.95739305 0.4987365 -0.35536319 -0.95738828 0.49871355 0.36480895 -0.95739305 
		0.49872416 0.40981206 -0.91840458 0.49872416 0.40981206 -0.19553992 0.4987241 0.40981218 
		-0.15673415 0.49872416 0.40981206 -0.15673892 0.49871355 0.36503586;
	setAttr -s 96 ".vt[0:95]"  -0.45522881 -0.32294402 0.29213667 -0.5 -0.32294402 0.29213667
		 -0.5 -0.32216918 0.24541283 -0.45519543 -0.13392943 0.49999905 -0.5 -0.13392943 0.49999905
		 -0.5 -0.13369679 0.45264363 -0.4551034 -0.32294402 -0.70786428 -0.5 -0.32294402 -0.70786428
		 -0.49981785 -0.32361001 -0.6608882 -0.40809727 -0.4978998 -1.070772648 -0.40849495 -0.4978998 -1.018875599
		 -0.45202208 -0.4978998 -1.02031374 0.45506644 -0.32294402 -0.70786428 0.49999905 -0.32294402 -0.70786428
		 0.49981618 -0.32361057 -0.660851 0.45202065 -0.4978998 -1.020051956 0.40809536 -0.4978998 -1.018400669
		 0.40788364 -0.4978998 -1.070772648 0.49999905 -0.32216918 0.24541283 0.49999905 -0.32294402 0.29213667
		 0.45522714 -0.32294402 0.29213667 0.45519376 -0.13392943 0.49999905 0.49999905 -0.13392943 0.49999905
		 0.49999905 -0.13369679 0.45264363 -0.45519543 -0.13392943 -0.50000048 -0.5 -0.13392943 -0.50000048
		 -0.5 -0.13416201 -0.45264482 -0.5 0.21734369 0.45264363 -0.5 0.21734369 0.49999905
		 -0.45519543 0.21734369 0.49999905 0.49999905 0.21734369 0.45264363 0.49999905 0.21734369 0.49999905
		 0.45519376 0.21734369 0.49999905 0.49999905 -0.13416201 -0.45264482 0.49999905 -0.13392943 -0.50000048
		 0.45519376 -0.13392943 -0.50000048 -0.45519543 0.4841131 -0.50000048 -0.5 0.4841131 -0.50000048
		 -0.5 0.48049307 -0.45264482 -0.5 0.21734363 -0.45264482 -0.5 0.21734363 -0.50000048
		 -0.45519543 0.21734363 -0.50000048 0.49999905 0.48049307 -0.45264482 0.49999905 0.4841131 -0.50000048
		 0.45519376 0.4841131 -0.50000048 0.45519376 0.21734369 -0.50000048 0.49999905 0.21734369 -0.50000048
		 0.49999905 0.21734369 -0.45264482 -0.40835094 -0.49789962 -0.1667285 -0.45202208 -0.49789965 -0.21321201
		 -0.40953588 -0.49789965 -0.21163416 0.40836072 -0.49789962 -0.1667285 0.4095335 -0.49789965 -0.21163416
		 0.45202065 -0.49789965 -0.21305609 -0.45519543 0.4841131 0.49999905 -0.5 0.4841131 0.49999905
		 -0.5 0.4841131 0.45264363 -0.5 0.49307787 0.45264363 -0.5 0.48859608 0.45264363 -0.5 0.48859608 0.49999905
		 -0.45519543 0.48859608 0.49999905 -0.45519543 0.49307787 0.49999905 -0.45519543 0.49307787 0.45264363
		 -0.45519543 0.49307787 -0.059762239 -0.5 0.49307787 -0.059762239 -0.5 0.48859608 -0.05472517
		 0.49999905 0.4841131 0.45264363 0.49999905 0.4841131 0.49999905 0.45519376 0.4841131 0.49999905
		 0.49999905 0.49307787 0.45264363 0.45519376 0.49307787 0.45264363 0.45519376 0.49307787 0.49999905
		 0.45519376 0.48859608 0.49999905 0.49999905 0.48859608 0.49999905 0.49999905 0.48859608 0.45264363
		 0.49999905 0.48859608 -0.05472517 0.49999905 0.49307787 -0.059762239 0.45519376 0.49307787 -0.059762239
		 -0.45202208 -0.4978998 -1.070772648 0.45202065 -0.4978998 -1.070772648 -0.45202208 -0.49789962 -0.1667285
		 0.45202065 -0.49789962 -0.1667285 -0.5 0.49307787 0.49999905 0.49999905 0.49307787 0.49999905
		 -0.45343482 -0.49272507 -0.19970734 -0.45343482 -0.49274787 -0.15321675 -0.4097313 -0.49274787 -0.15321675
		 0.40974075 -0.49274787 -0.15321675 0.45343342 -0.49274778 -0.15321653 0.45343342 -0.49272501 -0.1995558
		 0.45342803 -0.49276769 -1.0094748735 0.45343342 -0.49274796 -1.06008625 0.409273 -0.49274796 -1.06008625
		 -0.40948141 -0.49274805 -1.06008637 -0.45343485 -0.49274796 -1.06008625 -0.45342946 -0.49276766 -1.0097301006;
	setAttr -s 188 ".ed";
	setAttr ".ed[0:165]"  2 84 1 2 1 1 5 2 1 1 0 1 0 3 1 5 4 1 4 28 0 28 27 1
		 27 5 1 4 3 1 3 29 1 29 28 1 25 24 1 24 6 1 8 26 1 26 25 1 8 7 1 7 6 1 10 9 1 9 17 0
		 17 16 1 16 10 1 11 49 0 11 10 1 10 50 1 50 49 1 17 92 1 14 13 1 13 34 0 34 33 1 33 14 1
		 13 12 1 12 35 1 35 34 1 16 15 1 15 53 0 53 52 1 52 16 1 53 89 1 20 19 1 19 22 0 22 21 1
		 21 20 1 19 18 1 18 23 1 23 22 1 32 21 1 23 30 1 41 24 1 26 39 1 56 27 1 29 54 1 32 31 1
		 68 32 1 31 30 1 30 66 1 47 33 1 35 45 1 64 63 1 63 36 1 38 65 1 65 64 1 38 37 1 37 40 0
		 40 39 1 39 38 1 37 36 0 36 41 1 41 40 1 76 75 1 75 42 1 44 77 1 77 76 1 44 43 0 43 46 0
		 46 45 1 45 44 1 43 42 1 42 47 1 47 46 1 48 51 0 48 50 1 50 52 1 52 51 1 56 55 1 55 59 0
		 59 58 1 58 56 1 55 54 1 54 60 1 60 59 1 58 57 1 65 58 1 57 62 1 62 63 1 62 61 1 61 71 0
		 71 70 1 70 62 1 61 60 1 60 72 1 72 71 1 68 67 1 67 73 0 73 72 1 72 68 1 67 66 1 66 74 1
		 74 73 1 70 69 1 77 70 1 69 74 1 74 75 1 26 5 1 27 39 1 35 24 1 41 45 1 21 3 1 0 20 1
		 32 29 1 12 6 1 8 2 1 33 23 1 18 14 1 47 30 1 66 42 1 44 36 0 63 77 1 54 68 1 38 56 1
		 1 4 0 7 25 0 22 31 0 37 64 0 25 40 0 43 76 0 34 46 0 28 55 0 57 64 0 31 67 0 69 76 0
		 9 78 0 78 94 0 11 78 0 15 79 0 79 91 0 17 79 0 48 80 0 80 49 0 51 81 0 81 88 0 53 81 0
		 57 82 0 82 61 0 59 82 0 69 83 0 83 73 0 71 83 0 1 85 0 0 86 1 20 87 1 14 90 1 6 93 1
		 8 95 1 84 49 1 85 80 0;
	setAttr ".ed[166:187]" 86 48 1 87 51 1 88 19 0 89 18 1 90 15 1 91 13 0 92 12 1
		 93 9 1 94 7 0 95 11 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1 90 91 1 91 92 1
		 92 93 1 93 94 1 94 95 1 95 84 1;
	setAttr -s 94 -ch 376 ".fc[0:93]" -type "polyFaces" 
		f 4 5 6 7 8
		mu 0 4 16 47 63 6
		f 4 9 10 11 -7
		mu 0 4 47 11 42 63
		f 4 16 -175 186 -164
		mu 0 4 15 48 113 114
		f 4 17 162 185 174
		mu 0 4 49 22 111 112
		f 4 18 19 20 21
		mu 0 4 0 50 58 43
		f 4 -121 -173 184 -163
		mu 0 4 22 13 110 111
		f 4 187 -1 -122 163
		mu 0 4 114 101 21 15
		f 4 23 24 25 -23
		mu 0 4 53 0 1 76
		f 4 27 28 29 30
		mu 0 4 23 54 66 17
		f 4 31 32 33 -29
		mu 0 4 55 13 7 65
		f 4 34 35 36 37
		mu 0 4 43 56 79 2
		f 4 -124 -170 181 -162
		mu 0 4 23 18 106 107
		f 4 39 40 41 42
		mu 0 4 24 59 60 9
		f 4 43 44 45 -41
		mu 0 4 59 18 19 60
		f 4 62 63 64 65
		mu 0 4 31 67 70 36
		f 4 66 67 68 -64
		mu 0 4 68 37 8 69
		f 4 73 74 75 76
		mu 0 4 27 71 74 38
		f 4 77 78 79 -75
		mu 0 4 72 39 20 73
		f 4 -119 159 178 -161
		mu 0 4 24 10 103 104
		f 4 81 82 83 -81
		mu 0 4 75 1 2 78
		f 4 84 85 86 87
		mu 0 4 35 80 82 30
		f 4 88 89 90 -86
		mu 0 4 80 29 3 82
		f 4 95 96 97 98
		mu 0 4 33 83 88 34
		f 4 99 100 101 -97
		mu 0 4 83 3 4 88
		f 4 102 103 104 105
		mu 0 4 41 85 89 4
		f 4 106 107 108 -104
		mu 0 4 85 26 45 89
		f 4 113 -9 114 -50
		mu 0 4 5 16 6 36
		f 4 115 -49 116 -58
		mu 0 4 7 14 8 38
		f 4 -43 117 -5 118
		mu 0 4 24 9 11 10
		f 4 -118 -47 119 -11
		mu 0 4 11 9 12 42
		f 4 -33 120 -14 -116
		mu 0 4 7 13 22 14
		f 4 -15 121 -3 -114
		mu 0 4 5 15 21 16
		f 4 -31 122 -45 123
		mu 0 4 23 17 19 18
		f 4 -123 -57 124 -48
		mu 0 4 19 17 20 40
		f 4 -113 -108 125 -71
		mu 0 4 25 45 26 39
		f 4 -72 126 -60 127
		mu 0 4 32 27 37 28
		f 4 -101 -90 128 -106
		mu 0 4 4 3 29 41
		f 4 -88 -93 -61 129
		mu 0 4 35 30 44 31
		f 4 -128 -95 -99 -111
		mu 0 4 32 28 33 34
		f 4 -130 -66 -115 -51
		mu 0 4 35 31 36 6
		f 4 -127 -77 -117 -68
		mu 0 4 37 27 38 8
		f 4 -126 -56 -125 -79
		mu 0 4 39 26 40 20
		f 4 -129 -52 -120 -54
		mu 0 4 41 29 42 12
		f 4 -25 -22 -38 -83
		mu 0 4 1 0 43 2
		f 4 -4 158 177 -160
		mu 0 4 10 46 102 103
		f 4 -2 0 176 -159
		mu 0 4 46 21 101 102
		f 4 1 130 -6 2
		mu 0 4 21 46 47 16
		f 4 3 4 -10 -131
		mu 0 4 46 10 11 47
		f 4 -18 131 12 13
		mu 0 4 22 49 62 14
		f 4 -17 14 15 -132
		mu 0 4 48 15 5 61
		f 4 -32 -172 183 172
		mu 0 4 13 55 109 110
		f 4 -28 161 182 171
		mu 0 4 54 23 107 108
		f 4 -44 -169 180 169
		mu 0 4 18 59 105 106
		f 4 -40 160 179 168
		mu 0 4 59 24 104 105
		f 4 -42 132 -53 46
		mu 0 4 9 60 64 12
		f 4 -46 47 -55 -133
		mu 0 4 60 19 40 64
		f 4 -67 133 58 59
		mu 0 4 37 68 84 28
		f 4 -63 60 61 -134
		mu 0 4 67 31 44 84
		f 4 -13 134 -69 48
		mu 0 4 14 62 69 8
		f 4 -16 49 -65 -135
		mu 0 4 61 5 36 70
		f 4 -78 135 69 70
		mu 0 4 39 72 91 25
		f 4 -74 71 72 -136
		mu 0 4 71 27 32 90
		f 4 -30 136 -80 56
		mu 0 4 17 66 73 20
		f 4 -34 57 -76 -137
		mu 0 4 65 7 38 74
		f 4 -8 137 -85 50
		mu 0 4 6 63 80 35
		f 4 -12 51 -89 -138
		mu 0 4 63 42 29 80
		f 4 91 138 -62 92
		mu 0 4 30 81 84 44
		f 4 93 94 -59 -139
		mu 0 4 81 33 28 84
		f 4 52 139 -103 53
		mu 0 4 12 64 85 41
		f 4 54 55 -107 -140
		mu 0 4 64 40 26 85
		f 4 109 140 -73 110
		mu 0 4 34 86 90 32
		f 4 111 112 -70 -141
		mu 0 4 87 45 25 91
		f 4 -19 -24 143 -142
		mu 0 4 50 0 53 92
		f 4 -35 -21 146 -145
		mu 0 4 56 43 58 95
		f 4 -26 -82 147 148
		mu 0 4 76 1 75 97
		f 4 -84 -37 151 -150
		mu 0 4 78 2 79 98
		f 4 -96 -94 152 153
		mu 0 4 83 33 81 99
		f 4 -92 -87 154 -153
		mu 0 4 81 30 82 99
		f 4 -91 -100 -154 -155
		mu 0 4 82 3 83 99
		f 4 -109 -112 155 156
		mu 0 4 89 45 87 100
		f 4 -110 -98 157 -156
		mu 0 4 86 34 88 100
		f 4 -102 -105 -157 -158
		mu 0 4 88 4 89 100
		f 4 -177 164 -149 -166
		mu 0 4 102 101 77 97
		f 4 -178 165 -148 -167
		mu 0 4 103 102 97 75
		f 4 -179 166 80 -168
		mu 0 4 104 103 75 78
		f 4 -180 167 149 150
		mu 0 4 105 104 78 98
		f 4 -181 -151 -152 38
		mu 0 4 106 105 98 79
		f 4 -182 -39 -36 -171
		mu 0 4 107 106 79 56
		f 4 -183 170 144 145
		mu 0 4 108 107 56 95
		f 4 -184 -146 -147 26
		mu 0 4 110 109 96 57
		f 4 -185 -27 -20 -174
		mu 0 4 111 110 57 51
		f 4 -186 173 141 142
		mu 0 4 112 111 51 94
		f 4 -187 -143 -144 -176
		mu 0 4 114 113 93 52
		f 4 -165 -188 175 22
		mu 0 4 77 101 114 52;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "ChairLegFL_geo" -p "chair_grp_geo";
	rename -uid "DAC6D066-48BE-B28D-5007-F3B233162085";
	setAttr ".t" -type "double3" 0.53069512874999869 0.91670652431544375 -0.4876772645825912 ;
	setAttr ".s" -type "double3" 0.13202207034874919 1.4019313346035325 0.12491256347063069 ;
	setAttr ".rp" -type "double3" -1.0755547013784699 -0.6609554774237143 -0.085960490180752663 ;
	setAttr ".sp" -type "double3" -8.1467795387337709 -0.47146066366412476 -0.68682184123667944 ;
	setAttr ".spt" -type "double3" 7.071224837355377 -0.18949481375958988 0.6008613510559252 ;
createNode mesh -n "ChairLegFL_geoShape" -p "ChairLegFL_geo";
	rename -uid "D635B499-47E1-351B-A051-7A87A5A98DEE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999997019767761 0.875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 0.10819442 ;
	setAttr ".pt[1]" -type "float3" -3.7252903e-09 4.6566129e-10 0.10819441 ;
	setAttr ".pt[6]" -type "float3" 0 0 0.1081946 ;
	setAttr ".pt[7]" -type "float3" 0 0 0.1081946 ;
	setAttr ".pt[28]" -type "float3" 0.066556238 0 0.038395327 ;
	setAttr ".pt[41]" -type "float3" 0.066556238 0 0.17799358 ;
	setAttr ".pt[43]" -type "float3" -0.065712795 4.6566129e-10 0.038395327 ;
	setAttr ".pt[44]" -type "float3" -0.065712869 0 0.17799358 ;
	setAttr ".pt[62]" -type "float3" 0.069799006 0 0.043035641 ;
	setAttr ".pt[63]" -type "float3" 0.06655632 0 0.043035887 ;
	setAttr ".pt[64]" -type "float3" -0.065712869 0 0.043035842 ;
	setAttr ".pt[65]" -type "float3" 0 0 0.10819399 ;
	setAttr ".pt[74]" -type "float3" 0.069799006 0 0.17171417 ;
	setAttr ".pt[89]" -type "float3" -0.069799006 0 0.17171417 ;
	setAttr ".pt[90]" -type "float3" -0.065712869 0 0.17171371 ;
	setAttr ".pt[91]" -type "float3" 0.06655632 0 0.1717139 ;
	setAttr ".pt[92]" -type "float3" 3.7252903e-09 -4.4408921e-16 0.10819437 ;
	setAttr ".pt[93]" -type "float3" 3.7252903e-09 4.4408921e-16 0.10819427 ;
	setAttr ".pt[94]" -type "float3" 3.7252903e-09 -4.4408921e-16 0.10819436 ;
	setAttr ".pt[95]" -type "float3" 3.7252903e-09 4.4408921e-16 0.10819428 ;
	setAttr ".pt[96]" -type "float3" 5.5879354e-09 4.4408921e-16 0.10819428 ;
	setAttr ".pt[97]" -type "float3" -1.8626451e-09 -4.4408921e-16 0.10819429 ;
	setAttr ".pt[98]" -type "float3" -3.7252903e-09 4.4408921e-16 0.10819428 ;
	setAttr ".pt[99]" -type "float3" -3.7252903e-09 4.4408921e-16 0.10819434 ;
	setAttr ".pt[100]" -type "float3" -3.7252903e-09 -4.4408921e-16 0.10819428 ;
	setAttr ".pt[101]" -type "float3" -3.7252903e-09 -4.4408921e-16 0.10819437 ;
	setAttr ".pt[102]" -type "float3" -1.8626451e-09 4.4408921e-16 0.10819437 ;
	setAttr ".pt[103]" -type "float3" 3.7252903e-09 -4.4408921e-16 0.10819437 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "left";
	rename -uid "83411C78-4A83-237F-AB70-3394B41CA095";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1000.1 0.17418244608894021 -0.52696753685231623 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
createNode camera -n "leftShape" -p "left";
	rename -uid "A2BB921A-45EF-9D2A-48F2-0ABB3BA254B3";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 4.1001883379085555;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube2";
	rename -uid "AE5C178A-4FCA-B71F-FB68-CBAFF83BCCDB";
	setAttr ".rp" -type "double3" 0 1.3735155397207299 -0.56162011623382235 ;
	setAttr ".sp" -type "double3" 0 1.3735155397207299 -0.56162011623381924 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "FBA16ACB-42F9-1660-C318-3F99A1FA71BC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 7 "f[11:12]" "f[14]" "f[18]" "f[21]" "f[25:26]" "f[28:29]" "f[60:62]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 7 "f[0]" "f[5]" "f[10]" "f[38:40]" "f[50:52]" "f[63:65]" "f[75:78]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 7 "f[2:3]" "f[6]" "f[9]" "f[20]" "f[22:24]" "f[27]" "f[54:56]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 8 "f[1]" "f[7]" "f[13]" "f[30:31]" "f[41:43]" "f[53]" "f[66:67]" "f[79]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 6 "f[4]" "f[17]" "f[19]" "f[35:37]" "f[47:49]" "f[72:74]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 6 "f[8]" "f[15:16]" "f[32:34]" "f[44:46]" "f[57:59]" "f[68:71]";
	setAttr ".pv" -type "double2" 0.5 0.4375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 107 ".uvst[0].uvsp[0:106]" -type "float2" 0.38348806 0.10846689
		 0.38320482 0.14515325 0.61651194 0.14153358 0.61767817 0.64153314 0.61679518 0.10484724
		 0.38232183 0.60846645 0.61740667 0.61248851 0.38232183 0.64153308 0.38391727 0 0.38391727
		 1 0.3754991 0.1085327 0.62450957 0.10659342 0.61704779 1 0.61704779 0 0.37549046
		 0.14340709 0.38332978 0.24937777 0.38198608 0.74934131 0.375 0.64153308 0.125 0.10846689
		 0.38267535 0.50065762 0.125 0.14159605 0.375 0.60840398 0.61645514 0.24935849 0.62450093
		 0.14146778 0.625 0.61054254 0.875 0.13945743 0.61735851 0.50067008 0.61783898 0.74935597
		 0.875 0.10846689 0.625 0.64153308 0.375 1 0.375 0 0.625 0 0.625 1 0.375 0.25 0.375
		 0.75 0.125 0 0.375 0.5 0.125 0.25 0.625 0.25 0.875 0.25 0.625 0.5 0.625 0.75 0.875
		 0 0.15004991 0.10847347 0.15004905 0.14177716 0.15000001 0.25 0.375 0.47499999 0.3827408
		 0.47552964 0.61726815 0.47553891 0.625 0.47499999 0.84999996 0.25 0.84995008 0.13965847
		 0.84995091 0.10827954 0.84999996 0 0.625 0.77499998 0.61775988 0.77442038 0.3821792
		 0.77440715 0.375 0.77499998 0.15000001 0 0.61712694 0.97493565 0.64999998 0 0.625
		 0.97500002 0.6495586 0.10678077 0.64955086 0.14126675 0.625 0.27500001 0.64999998
		 0.25 0.6165455 0.27448964 0.38326436 0.27450573 0.35000002 0.25 0.375 0.27500001
		 0.3504414 0.14322598 0.35044917 0.10852613 0.375 0.97500002 0.35000002 0 0.38372415
		 0.97493416 0.50048256 0 0.50048256 1 0.50014162 0.10665707 0.49985838 0.14334342
		 0.49989247 0.24936813 0.49990493 0.27449769 0.50000447 0.47553426 0.50001693 0.50066388
		 0.49986425 0.61047745 0.5 0.64153314 0.49991253 0.74934864 0.49996954 0.77441376
		 0.50042558 0.97493494 0.25024953 0.1084998 0.25024521 0.14250156 0.25 0.25 0.375
		 0.375 0.38300258 0.3750177 0.4999547 0.37501597 0.61690682 0.37501428 0.625 0.375
		 0.75 0.25 0.74975049 0.14046261 0.74975479 0.10753015 0.75 0 0.625 0.875 0.61744344
		 0.87467802 0.50019753 0.87467432 0.38295168 0.87467062 0.375 0.875 0.25 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 82 ".pt[0:81]" -type "float3"  -0.0040344484 1.8243146 0.41021496 
		-0.015435778 1.5880649 0.42744723 -0.018344643 1.5872108 0.4242208 0.018344643 1.6123775 
		0.4242208 0.015490435 1.635038 0.42744723 0.0040725004 1.8243146 0.41021496 -0.018344643 
		1.1346444 0.4242208 -0.015473329 1.1119839 0.42744723 -0.0040680077 0.92271078 0.41021496 
		-0.036693748 1.8243146 0.37692052 -0.059976682 1.5880649 0.36291468 -0.050975613 
		1.5880649 0.35968825 -0.036693748 0.92271078 0.37692052 -0.050975613 1.1589506 0.35968825 
		-0.059976682 1.1581405 0.36291468 0.0040389448 0.92271078 0.41021496 0.015452891 
		1.1589506 0.42744723 0.018344643 1.1598047 0.4242208 0.059976682 1.1858995 0.36291468 
		0.050906204 1.2111465 0.35968825 0.036522109 0.92271078 0.37692052 0.036886655 1.8243146 
		0.37692052 0.051023874 1.5880649 0.35968825 0.059976682 1.5880649 0.36291468 -0.004812479 
		1.8243146 0.40862957 0.004812479 1.8243146 0.40862957 -0.004812479 0.92271078 0.40862957 
		-0.043176312 1.8243146 0.37850592 -0.043176312 0.92271078 0.37850592 0.004812479 
		0.92271078 0.40862957 0.043176312 0.92271078 0.37850592 0.043176312 1.8243146 0.37850592 
		-0.055965357 1.5879793 0.36646414 -0.055965357 1.155791 0.36646414 -0.037922356 0.92271078 
		0.38024995 -0.046497203 0.92271078 0.36300027 0.046269722 0.92271078 0.36300027 0.037922356 
		0.92271078 0.38024995 0.055965357 1.18329 0.36646414 0.055965357 1.5904962 0.36646414 
		0.037922356 1.8243146 0.38024995 0.046724845 1.8243146 0.36300027 -0.046463825 1.8243146 
		0.36300027 -0.037922356 1.8243146 0.38024995 0.018714719 1.8243146 0.42413521 0.0054037459 
		1.8243146 0.4068855 0.020598495 1.6099464 0.42067134 0.020598495 1.1624142 0.42067134 
		0.0054037459 0.92271078 0.4068855 0.018556964 0.92271078 0.42413521 -0.018685194 
		0.92271078 0.42413521 -0.0054037459 0.92271078 0.4068855 -0.020598495 1.1369939 0.42067134 
		-0.020598495 1.5872961 0.42067134 -0.0054037459 1.8243146 0.4068855 -0.018548723 
		1.8243146 0.42413521 8.5960717e-05 1.8243146 0.44434056 5.3303684e-05 1.6115513 0.46258289 
		-1.9922585e-05 1.1354673 0.46258289 -6.5674918e-05 0.92271078 0.44434056 -6.4117063e-05 
		0.92271078 0.43418595 -0.00011374213 0.92271078 0.3529495 -0.0001276043 0.92271078 
		0.34279492 -4.7224406e-05 1.1850485 0.3245526 3.2843498e-05 1.5880649 0.3245526 0.00014341371 
		1.8243146 0.34279492 0.00013051141 1.8243146 0.3529495 8.2996121e-05 1.8243146 0.43418595 
		-0.066168398 1.5876378 0.39356774 -0.066168398 1.1463926 0.39356774 -0.048747867 
		0.92271078 0.39356774 -0.037390374 0.92271078 0.39356774 -0.00010875746 0.92271078 
		0.39356774 0.037172858 0.92271078 0.39356774 0.048747867 0.92271078 0.39356774 0.066168398 
		1.1728523 0.39356774 0.066168398 1.6002214 0.39356774 0.048747867 1.8243146 0.39356774 
		0.037515227 1.8243146 0.39356774 0.0001336463 1.8243146 0.39356774 -0.037247933 1.8243146 
		0.39356774 -0.048747867 1.8243146 0.39356774;
	setAttr -s 82 ".vt[0:81]"  -0.3792471 -0.49999809 0.23116781 -0.3806501 -0.23083749 0.23116781
		 -0.4523834 -0.22991851 0.17730762 0.4523834 -0.25699592 0.17730762 0.38199806 -0.28137666 0.23116781
		 0.3828238 -0.49999809 0.23116781 -0.4523834 0.25700545 0.17730762 -0.381576 0.2813862 0.23116781
		 -0.38240176 0.50000381 0.23116781 -0.4456988 -0.49999809 -0.89995366 -0.52443898 -0.23083749 -0.84609348
		 -0.44573307 -0.23083749 -0.89995366 -0.4456988 0.50000381 -0.89995366 -0.44573307 0.23085389 -0.89995366
		 -0.52443898 0.23172542 -0.84609348 0.37966913 0.50000381 0.23116781 0.38107219 0.23085389 0.23116781
		 0.4523834 0.22993492 0.17730762 0.52443898 0.2018591 -0.84609348 0.44512621 0.17469552 -0.89995366
		 0.44361398 0.50000381 -0.89995366 0.44804192 -0.49999809 -0.89995366 0.44615513 -0.23083749 -0.89995366
		 0.52443898 -0.23083749 -0.84609348 -0.4523834 -0.49999809 0.17730762 0.4523834 -0.49999809 0.17730762
		 -0.4523834 0.50000381 0.17730762 -0.52443898 -0.49999809 -0.84609348 -0.52443898 0.50000381 -0.84609348
		 0.4523834 0.50000381 0.17730762 0.52443898 0.50000381 -0.84609348 0.52443898 -0.49999809 -0.84609348
		 -0.57167596 -0.2307456 -0.78684151 -0.57167596 0.23425342 -0.78684151 -0.57167596 0.50000381 -0.78684151
		 -0.43936908 0.50000381 -0.78684151 0.4372195 0.50000381 -0.78684151 0.57167596 0.50000381 -0.78684151
		 0.57167596 0.20466667 -0.78684151 0.57167596 -0.23345333 -0.78684151 0.57167596 -0.49999809 -0.78684151
		 0.44152012 -0.49999809 -0.78684151 -0.43905362 -0.49999809 -0.78684151 -0.57167596 -0.49999809 -0.78684151
		 0.38934562 -0.49999809 0.11805566 0.50796402 -0.49999809 0.11805566 0.50796402 -0.25438008 0.11805566
		 0.50796402 0.22712734 0.11805566 0.50796402 0.50000381 0.11805566 0.38606364 0.50000381 0.11805566
		 -0.38873145 0.50000381 0.11805566 -0.50796402 0.50000381 0.11805566 -0.50796402 0.25447744 0.11805566
		 -0.50796402 -0.23001041 0.11805566 -0.50796402 -0.49999809 0.11805566 -0.38589227 -0.49999809 0.11805566
		 0.001788348 -0.49999809 0.23116781 0.00067397952 -0.25610709 0.23116781 -0.00025190413 0.25612003 0.23116781
		 -0.0013663173 0.50000381 0.23116781 -0.0013339072 0.50000381 0.11805566 -0.001074791 0.50000381 -0.78684151
		 -0.0010424107 0.50000381 -0.89995366 -0.00030343235 0.2027747 -0.89995366 0.00021103024 -0.23083749 -0.89995366
		 0.0011715591 -0.49999809 -0.89995366 0.0012332499 -0.49999809 -0.78684151 0.0017266721 -0.49999809 0.11805566
		 -0.53981996 -0.230378 -0.33439294 -0.53981996 0.24436542 -0.33439294 -0.53981996 0.50000381 -0.33439294
		 -0.41405028 0.50000381 -0.33439294 -0.0012043491 0.50000381 -0.33439294 0.41164157 0.50000381 -0.33439294
		 0.53981996 0.50000381 -0.33439294 0.53981996 0.21589701 -0.33439294 0.53981996 -0.24391671 -0.33439294
		 0.53981996 -0.49999809 -0.33439294 0.41543287 -0.49999809 -0.33439294 0.001479961 -0.49999809 -0.33439294
		 -0.41247296 -0.49999809 -0.33439294 -0.53981996 -0.49999809 -0.33439294;
	setAttr -s 160 ".ed[0:159]"  1 0 1 0 56 0 5 4 1 4 57 1 10 32 1 2 6 0 2 1 1
		 1 7 1 7 6 1 5 44 1 4 3 1 3 17 0 17 16 1 16 4 1 12 35 1 8 59 0 8 7 1 7 58 1 16 15 1
		 0 55 1 9 65 0 9 11 1 11 64 1 22 21 1 11 10 1 10 14 0 14 13 1 13 11 1 6 52 1 13 12 1
		 12 62 0 20 19 1 19 63 1 20 36 1 17 47 1 19 18 1 18 23 0 23 22 1 22 19 1 3 46 1 24 2 0
		 0 24 0 25 5 0 3 25 0 26 8 0 6 26 0 9 27 0 27 10 0 24 54 0 26 51 0 28 14 0 12 28 0
		 29 17 0 15 29 0 29 48 0 30 20 0 18 30 0 25 45 0 31 23 0 21 31 0 32 68 1 33 14 1 34 28 0
		 35 71 1 36 73 1 37 30 0 38 18 1 39 23 1 40 31 0 41 21 1 42 9 1 43 27 0 32 33 1 33 34 1
		 34 35 1 35 61 1 36 37 1 37 38 1 38 39 1 39 40 1 40 41 1 41 66 1 42 43 1 43 32 1 44 78 1
		 45 77 0 46 76 1 47 75 1 48 74 0 49 15 1 50 8 1 51 70 0 52 69 1 53 2 1 54 81 0 55 80 1
		 44 45 1 45 46 1 46 47 1 47 48 1 48 49 1 49 60 1 50 51 1 51 52 1 52 53 1 53 54 1 54 55 1
		 55 67 1 56 5 0 57 1 1 58 16 1 59 15 0 60 50 1 61 36 1 62 20 0 63 13 1 64 22 1 65 21 0
		 66 42 1 67 44 1 56 57 1 57 58 1 58 59 1 59 60 1 60 72 1 61 62 1 62 63 1 63 64 1 64 65 1
		 65 66 1 66 79 1 67 56 1 68 53 1 69 33 1 70 34 0 71 50 1 72 61 1 73 49 1 74 37 0 75 38 1
		 76 39 1 77 40 0 78 41 1 79 67 1 80 42 1 81 43 0 68 69 1 69 70 1 70 71 1 71 72 1 72 73 1
		 73 74 1 74 75 1 75 76 1 76 77 1 77 78 1 78 79 1 79 80 1 80 81 1 81 68 1;
	setAttr -s 80 -ch 320 ".fc[0:79]" -type "polyFaces" 
		f 4 82 71 -47 -71
		mu 0 4 57 58 35 16
		f 4 83 -5 -48 -72
		mu 0 4 59 44 18 36
		f 4 0 1 120 109
		mu 0 4 0 8 76 78
		f 4 6 7 8 -6
		mu 0 4 10 0 1 14
		f 4 79 68 58 -68
		mu 0 4 53 54 43 28
		f 4 80 69 59 -69
		mu 0 4 55 56 27 42
		f 4 10 11 12 13
		mu 0 4 4 11 23 2
		f 4 73 62 50 -62
		mu 0 4 45 46 38 20
		f 4 74 -15 51 -63
		mu 0 4 47 48 19 37
		f 4 16 17 122 -16
		mu 0 4 15 1 79 80
		f 4 129 118 70 20
		mu 0 4 86 87 57 16
		f 4 21 22 128 -21
		mu 0 4 16 7 85 86
		f 4 24 25 26 27
		mu 0 4 7 17 21 5
		f 4 72 61 -26 4
		mu 0 4 44 45 20 18
		f 4 29 30 126 115
		mu 0 4 5 19 83 84
		f 4 125 -31 14 75
		mu 0 4 82 83 19 48
		f 4 33 76 65 55
		mu 0 4 26 49 50 41
		f 4 77 66 56 -66
		mu 0 4 51 52 25 40
		f 4 35 36 37 38
		mu 0 4 6 24 29 3
		f 4 78 67 -37 -67
		mu 0 4 52 53 28 25
		f 4 121 -18 -8 -110
		mu 0 4 78 79 1 0
		f 4 -116 127 -23 -28
		mu 0 4 5 84 85 7
		f 4 -1 -7 -41 -42
		mu 0 4 8 0 10 31
		f 4 -11 -3 -43 -44
		mu 0 4 11 4 13 32
		f 4 -9 -17 -45 -46
		mu 0 4 14 1 15 34
		f 4 -25 -22 46 47
		mu 0 4 17 7 16 35
		f 4 -30 -27 -51 -52
		mu 0 4 19 5 21 37
		f 4 -19 -13 -53 -54
		mu 0 4 22 2 23 39
		f 4 -36 -32 -56 -57
		mu 0 4 24 6 26 41
		f 4 -24 -38 -59 -60
		mu 0 4 27 3 29 42
		f 4 28 104 93 5
		mu 0 4 14 71 72 10
		f 4 45 49 103 -29
		mu 0 4 14 34 69 71
		f 4 44 -91 102 -50
		mu 0 4 34 15 68 70
		f 4 123 112 90 15
		mu 0 4 80 81 68 15
		f 4 100 89 53 54
		mu 0 4 65 67 22 39
		f 4 52 34 99 -55
		mu 0 4 39 23 64 66
		f 4 39 98 -35 -12
		mu 0 4 11 63 64 23
		f 4 43 57 97 -40
		mu 0 4 11 32 61 63
		f 4 42 9 96 -58
		mu 0 4 33 12 60 62
		f 4 19 107 131 -2
		mu 0 4 9 75 88 77
		f 4 41 48 106 -20
		mu 0 4 9 30 73 75
		f 4 40 -94 105 -49
		mu 0 4 31 10 72 74
		f 4 -73 60 146 133
		mu 0 4 45 44 89 90
		f 4 -74 -134 147 134
		mu 0 4 46 45 90 91
		f 4 148 -64 -75 -135
		mu 0 4 92 93 48 47
		f 4 149 136 -76 63
		mu 0 4 93 94 82 48
		f 4 -77 64 151 138
		mu 0 4 50 49 95 96
		f 4 -78 -139 152 139
		mu 0 4 52 51 97 98
		f 4 -79 -140 153 140
		mu 0 4 53 52 98 99
		f 4 -80 -141 154 141
		mu 0 4 54 53 99 100
		f 4 -81 -142 155 142
		mu 0 4 56 55 101 102
		f 4 -119 130 157 144
		mu 0 4 57 87 103 104
		f 4 -83 -145 158 145
		mu 0 4 58 57 104 105
		f 4 159 -61 -84 -146
		mu 0 4 106 89 44 59
		f 4 -121 108 2 3
		mu 0 4 78 76 13 4
		f 4 -14 -111 -122 -4
		mu 0 4 4 2 79 78
		f 4 -123 110 18 -112
		mu 0 4 80 79 2 22
		f 4 101 -124 111 -90
		mu 0 4 67 81 80 22
		f 4 150 -65 -114 -137
		mu 0 4 94 95 49 82
		f 4 -115 -126 113 -34
		mu 0 4 26 83 82 49
		f 4 -127 114 31 32
		mu 0 4 84 83 26 6
		f 4 -128 -33 -39 -117
		mu 0 4 85 84 6 3
		f 4 -129 116 23 -118
		mu 0 4 86 85 3 27
		f 4 81 -130 117 -70
		mu 0 4 56 87 86 27
		f 4 156 -131 -82 -143
		mu 0 4 102 103 87 56
		f 4 -132 119 -10 -109
		mu 0 4 77 88 60 12
		f 4 -147 132 -105 92
		mu 0 4 90 89 72 71
		f 4 -148 -93 -104 91
		mu 0 4 91 90 71 69
		f 4 -136 -149 -92 -103
		mu 0 4 68 93 92 70
		f 4 124 -150 135 -113
		mu 0 4 81 94 93 68
		f 4 -138 -151 -125 -102
		mu 0 4 67 95 94 81
		f 4 -152 137 -101 88
		mu 0 4 96 95 67 65
		f 4 -153 -89 -100 87
		mu 0 4 98 97 66 64
		f 4 -154 -88 -99 86
		mu 0 4 99 98 64 63
		f 4 -155 -87 -98 85
		mu 0 4 100 99 63 61
		f 4 -156 -86 -97 84
		mu 0 4 102 101 62 60
		f 4 -144 -157 -85 -120
		mu 0 4 88 103 102 60
		f 4 -158 143 -108 95
		mu 0 4 104 103 88 75
		f 4 -159 -96 -107 94
		mu 0 4 105 104 75 73
		f 4 -133 -160 -95 -106
		mu 0 4 72 89 106 74;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "group1";
	rename -uid "6963074B-4876-F38E-CCFB-A5918405A945";
createNode transform -n "pCube1" -p "group1";
	rename -uid "AAECD8FA-4244-0843-C370-7B982DC4940F";
	setAttr ".rp" -type "double3" -0.57281982562800149 0 0.69744296077043211 ;
	setAttr ".sp" -type "double3" -0.57281982562800149 0 0.69744296077043211 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "E3BE5645-4443-1270-F99C-31A69877072F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 16 "f[15]" "f[19:20]" "f[26]" "f[31]" "f[35]" "f[41]" "f[45]" "f[49:50]" "f[56]" "f[58]" "f[61]" "f[63]" "f[69]" "f[74]" "f[89]" "f[91]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 9 "f[0]" "f[3]" "f[7]" "f[14]" "f[27]" "f[77]" "f[79]" "f[90]" "f[93]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 21 "f[2]" "f[5]" "f[9]" "f[17]" "f[22]" "f[24]" "f[33]" "f[37]" "f[39]" "f[43]" "f[46]" "f[53]" "f[55]" "f[64]" "f[66]" "f[70]" "f[73]" "f[78]" "f[81]" "f[83]" "f[85]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 19 "f[1]" "f[4]" "f[10]" "f[16]" "f[18]" "f[29:30]" "f[34]" "f[38]" "f[42]" "f[48]" "f[52]" "f[54]" "f[57]" "f[59]" "f[68]" "f[71]" "f[76]" "f[82]" "f[88]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 20 "f[6]" "f[13]" "f[21]" "f[23]" "f[28]" "f[32]" "f[36]" "f[40]" "f[44]" "f[47]" "f[51]" "f[60]" "f[62]" "f[65]" "f[67]" "f[72]" "f[75]" "f[80]" "f[86]" "f[92]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 5 "f[8]" "f[11:12]" "f[25]" "f[84]" "f[87]";
	setAttr ".pv" -type "double2" 0.5 0.49920508719515055 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 120 ".uvst[0].uvsp[0:119]" -type "float2" 0.39031661 0.98468351
		 0.39113581 0.00070930971 0.60968363 0.98468351 0.39020801 0.24926673 0.39009953 0.27284065
		 0.64666516 0.24930945 0.60968363 0.76531649 0.39031661 0.74932677 0.60968363 0.00067330943
		 0.60968363 0.062500037 0.60979223 0.27300489 0.60968345 0.5 0.39031667 0.6875 0.38961065
		 0.76461053 0.85960805 0.00067659421 0.85967904 0.062541842 0.35960799 0.0006766023
		 0.35960793 0.062458158 0.14060469 0.24814804 0.35333484 0.24930945 0.16380328 0.2493234
		 0.39031661 0.49999991 0.60968351 0.50137132 0.64047033 0.24862862 0.39045882 0.2486286
		 0.14032097 0.062541857 0.35966659 0.12500763 0.39031658 0.625 0.60968363 0.6875 0.64039207
		 0.062458158 0.85967892 0.12499239 0.39031661 0.062500045 0.60968363 0.125 0.14032108
		 0.12499239 0.35939503 0.18758109 0.39031667 0.125 0.60968363 0.1875 0.64033347 0.12500763
		 0.85966665 0.18741891 0.39031661 0.56250006 0.60968357 0.625 0.14033332 0.18741891
		 0.35952395 0.24862896 0.39031658 0.1875 0.60954142 0.2486286 0.64060497 0.18758109
		 0.85939533 0.24814804 0.39031667 0.50137132 0.60968363 0.56250006 0.64039201 0.00067659793
		 0.83613861 0.24932246 0.14039199 0.0006766023 0.60968363 0.74932677 0.60979223 0.24926673
		 0.375 0.98464572 0.35964572 0 0.3907553 0 0.3907553 1 0.37134251 -0.0015898256 0.64035428
		 0 0.625 0.98464572 0.62860066 -0.0015750498 0.60968351 1 0.60968351 0 0.37436125
		 0.24940819 0.38999882 0.25310531 0.35242933 0.25 0.375 0.27257067 0.62563908 0.24940825
		 0.625 0.27266422 0.64766425 0.25 0.60994172 0.25311014 0.375 0.7493251 0.125 0.0006749279
		 0.38993996 0.75087827 0.13997781 0 0.375 0.76497781 0.875 0.0006749279 0.625 0.7493251
		 0.625 0.76537883 0.85962117 0 0.60968363 0.75088346 0.375 0.0625 0.625 0.0625 0.375
		 0.5 0.125 0.25 0.875 0.25 0.625 0.5 0.37499997 0.50137132 0.125 0.24862863 0.875
		 0.24862863 0.62499994 0.50137132 0.375 0.125 0.375 0.1875 0.125 0.125 0.375 0.625
		 0.375 0.6875 0.125 0.0625 0.625 0.625 0.875 0.125 0.875 0.0625 0.625 0.6875 0.625
		 0.125 0.625 0.1875 0.125 0.1875 0.375 0.5625 0.375 0.24862862 0.625 0.24862862 0.625
		 0.5625 0.875 0.1875 0.375 1 0.375 0 0.625 0 0.625 1 0.375 0.25 0.625 0.25 0.375 0.75
		 0.125 0 0.875 0 0.625 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 96 ".vt[0:95]"  -0.60247648 3.5762787e-07 0.72342432 -0.59565151 3.5762787e-07 0.72768652
		 -0.59970105 3.5762787e-07 0.7346437 -0.59908867 0.008435607 0.73403651 -0.6062991 0.0082571507 0.72957909
		 -0.60204387 0.0080468655 0.72272766 -0.49090555 3.5762787e-07 0.79271877 -0.49047288 0.0080468655 0.79202217
		 -0.49474105 0.0080121756 0.79889429 -0.50157678 0.0080074072 0.79464924 -0.50199693 3.5762787e-07 0.79532576
		 -0.49775153 3.5762787e-07 0.78849024 -0.75732905 2.96471882 0.87005389 -0.75051677 2.96447349 0.87422854
		 -0.7515074 2.97319388 0.87561524 -0.74680817 2.97319388 0.86814928 -0.75360173 2.97319388 0.86405259
		 -0.75294131 2.96498108 0.86298919 -0.64575809 2.96471882 0.93934834 -0.64137036 2.96498108 0.9322837
		 -0.64201176 2.97319388 0.93331641 -0.64872938 2.97319388 0.92898935 -0.65335041 2.97319388 0.93657851
		 -0.65251988 2.96447349 0.93509245 -0.53701675 0.0080271959 0.61802757 -0.5301823 0.0080074072 0.6222747
		 -0.53077042 3.5762787e-07 0.62284672 -0.53496718 3.5762787e-07 0.62927586 -0.54158944 3.5762787e-07 0.62539005
		 -0.54128206 0.0080468655 0.6248951 -0.4254458 0.0080271959 0.68732208 -0.42971107 0.0080468655 0.69418961
		 -0.43012962 3.5762787e-07 0.6948635 -0.43694782 3.5762787e-07 0.69059026 -0.43270245 3.5762787e-07 0.6837548
		 -0.43228236 0.0080074072 0.68307841 -0.58494788 0.74280095 0.66563737 -0.5891881 0.74329865 0.67246449
		 -0.58235264 0.74329865 0.67670989 -0.47337696 0.74280095 0.73493183 -0.47761714 0.74329865 0.74175894
		 -0.48445266 0.74329865 0.73751354 -0.70813131 2.9651475 0.79084069 -0.70725799 2.97319388 0.78943461
		 -0.70042253 2.97319388 0.79367995 -0.60252255 2.97319388 0.85448366 -0.59568703 2.97319388 0.85872906
		 -0.59656036 2.9651475 0.8601352 -0.69096589 2.95117044 0.76320267 -0.6872285 2.95688438 0.7571851
		 -0.68039298 2.95688438 0.7614305 -0.57939494 2.95117044 0.83249712 -0.57565755 2.95688438 0.82647955
		 -0.58249307 2.95688438 0.82223415 -0.60475141 1.48659694 0.68110299 -0.61158687 1.48659694 0.67685759
		 -0.60733908 1.48668778 0.67001814 -0.66520315 2.23085952 0.7371012 -0.6694293 2.22989511 0.74390566
		 -0.66259378 2.22989511 0.74815106 -0.53545696 1.48659694 0.56953204 -0.54229242 1.48659694 0.56528664
		 -0.54654026 1.48650646 0.57212609 -0.5241338 0.74379647 0.56772065 -0.51989365 0.74329865 0.5608936
		 -0.51305819 0.74329865 0.565139 -0.43496931 1.48650646 0.64142054 -0.43072146 1.48659694 0.63458109
		 -0.43755698 1.48659694 0.63033569 -0.41515821 0.74329865 0.62594265 -0.40832272 0.74329865 0.63018805
		 -0.41256285 0.74379647 0.6370151 -0.4957681 1.48668778 0.73931265 -0.50001591 1.48659694 0.74615204
		 -0.50685143 1.48659694 0.74190664 -0.56469381 2.22989511 0.80895472 -0.55785835 2.22989511 0.81320012
		 -0.5536322 2.23085952 0.80639565 -0.59329927 2.22989511 0.63658005 -0.60013479 2.22989511 0.63233465
		 -0.60436094 2.22893071 0.63913918 -0.75223815 2.95688391 0.86185712 -0.75652289 2.95688391 0.86875594
		 -0.74962389 2.95688391 0.8730408 -0.65185094 2.95688391 0.93376559 -0.64495194 2.95688391 0.93805039
		 -0.6406672 2.95688391 0.93115157 -0.49278995 2.22893071 0.70843363 -0.48856384 2.22989511 0.7016291
		 -0.49539936 2.22989511 0.69738376 -0.60673237 3.5762787e-07 0.73027664 -0.49516141 3.5762787e-07 0.79957116
		 -0.75820106 2.97319388 0.87145793 -0.64663011 2.97319388 0.94075239 -0.53743792 3.5762787e-07 0.61870569
		 -0.42586696 3.5762787e-07 0.68800014;
	setAttr -s 188 ".ed";
	setAttr ".ed[0:165]"  1 0 1 0 28 0 28 27 1 27 1 1 0 5 1 5 29 1 29 28 1 3 2 1
		 2 10 0 10 9 1 9 3 1 2 1 1 1 11 1 11 10 1 5 4 1 4 37 0 37 36 1 36 5 1 4 3 1 3 38 1
		 38 37 1 7 6 1 6 32 0 32 31 1 31 7 1 6 11 1 11 33 1 33 32 1 9 8 1 41 9 1 8 7 1 7 39 1
		 13 12 1 83 13 1 12 17 1 17 81 1 15 14 1 14 22 0 22 21 1 21 15 1 14 13 1 13 23 1 23 22 1
		 17 16 1 16 43 0 43 42 1 42 17 1 16 15 1 15 44 1 44 43 1 19 18 1 86 19 1 18 23 1 23 84 1
		 21 20 1 20 46 0 46 45 1 45 21 1 20 19 1 19 47 1 47 46 1 25 24 1 65 25 1 24 29 1 29 63 1
		 27 26 1 26 34 0 34 33 1 33 27 1 26 25 1 25 35 1 35 34 1 31 30 1 71 31 1 30 35 1 35 69 1
		 56 36 1 38 54 1 41 40 1 74 41 1 40 39 1 39 72 1 49 48 1 48 42 1 44 50 1 50 49 0 53 45 1
		 47 51 1 80 48 1 50 78 1 53 52 0 89 53 1 52 51 1 51 87 1 56 55 1 55 58 0 58 57 1 57 56 1
		 55 54 1 54 59 1 59 58 1 82 81 1 81 57 1 59 83 1 83 82 1 79 78 1 78 60 1 62 80 1 80 79 1
		 62 61 1 61 64 0 64 63 1 63 62 1 61 60 1 60 65 1 65 64 1 88 87 1 87 66 1 68 89 1 89 88 1
		 68 67 1 67 70 0 70 69 1 69 68 1 67 66 1 66 71 1 71 70 1 74 73 1 73 76 0 76 75 1 75 74 1
		 73 72 1 72 77 1 77 76 1 85 84 1 84 75 1 77 86 1 86 85 1 41 38 1 45 44 1 65 69 1 71 39 1
		 36 63 1 48 81 1 53 50 0 86 51 1 83 84 1 56 62 1 60 68 1 66 72 1 74 54 1 57 80 1 75 59 1
		 87 77 1 78 89 1 8 40 0 43 49 0 46 52 0 37 55 0 58 82 0 61 79 0 24 64 0 67 88 0 30 70 0
		 40 73 0 76 85 0;
	setAttr ".ed[166:187]" 49 79 0 12 82 0 18 85 0 52 88 0 0 90 0 90 4 0 2 90 0
		 6 91 0 91 10 0 8 91 0 12 92 0 92 16 0 14 92 0 18 93 0 93 22 0 20 93 0 24 94 0 94 28 0
		 26 94 0 30 95 0 95 34 0 32 95 0;
	setAttr -s 94 -ch 376 ".fc[0:93]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 54 76 13
		f 4 4 5 6 -2
		mu 0 4 55 16 51 75
		f 4 7 8 9 10
		mu 0 4 1 56 63 8
		f 4 11 12 13 -9
		mu 0 4 57 0 2 62
		f 4 14 15 16 17
		mu 0 4 16 58 82 17
		f 4 18 19 20 -16
		mu 0 4 58 1 31 82
		f 4 21 22 23 24
		mu 0 4 49 59 80 14
		f 4 25 26 27 -23
		mu 0 4 60 2 6 79
		f 4 36 37 38 39
		mu 0 4 4 65 71 10
		f 4 40 41 42 -38
		mu 0 4 65 3 53 71
		f 4 43 44 45 46
		mu 0 4 19 66 85 20
		f 4 47 48 49 -45
		mu 0 4 67 4 21 84
		f 4 54 55 56 57
		mu 0 4 10 69 87 11
		f 4 58 59 60 -56
		mu 0 4 70 5 50 86
		f 4 65 66 67 68
		mu 0 4 13 74 81 6
		f 4 69 70 71 -67
		mu 0 4 74 7 52 81
		f 4 94 95 96 97
		mu 0 4 26 92 93 34
		f 4 98 99 100 -96
		mu 0 4 92 35 43 93
		f 4 109 110 111 112
		mu 0 4 33 94 97 25
		f 4 113 114 115 -111
		mu 0 4 95 27 12 96
		f 4 120 121 122 123
		mu 0 4 40 98 101 28
		f 4 124 125 126 -122
		mu 0 4 99 30 15 100
		f 4 127 128 129 130
		mu 0 4 32 102 103 36
		f 4 131 132 133 -129
		mu 0 4 102 37 45 103
		f 4 -11 -30 138 -20
		mu 0 4 1 8 9 31
		f 4 -40 -58 139 -49
		mu 0 4 4 10 11 21
		f 4 140 -76 -71 -63
		mu 0 4 12 28 52 7
		f 4 -69 -27 -13 -4
		mu 0 4 13 6 2 0
		f 4 -25 -74 141 -32
		mu 0 4 49 14 15 29
		f 4 -6 -18 142 -65
		mu 0 4 51 16 17 25
		f 4 143 -36 -47 -84
		mu 0 4 18 42 19 20
		f 4 -140 -87 144 -85
		mu 0 4 21 11 22 47
		f 4 145 -88 -60 -52
		mu 0 4 23 46 50 5
		f 4 146 -54 -42 -34
		mu 0 4 24 44 53 3
		f 4 -143 -77 147 -113
		mu 0 4 25 17 26 33
		f 4 148 -124 -141 -115
		mu 0 4 27 40 28 12
		f 4 -142 -126 149 -82
		mu 0 4 29 15 30 37
		f 4 -139 -80 150 -78
		mu 0 4 31 9 32 35
		f 4 -148 -98 151 -108
		mu 0 4 33 26 34 41
		f 4 -151 -131 152 -100
		mu 0 4 35 32 36 43
		f 4 -150 -118 153 -133
		mu 0 4 37 30 38 45
		f 4 154 -119 -149 -107
		mu 0 4 39 48 40 27
		f 4 -152 -103 -144 -89
		mu 0 4 41 34 42 18
		f 4 -153 -136 -147 -104
		mu 0 4 43 36 44 24
		f 4 -154 -94 -146 -137
		mu 0 4 45 38 46 23
		f 4 -145 -92 -155 -90
		mu 0 4 47 22 48 39
		f 4 28 155 -79 29
		mu 0 4 8 61 83 9
		f 4 30 31 -81 -156
		mu 0 4 61 49 29 83
		f 4 -46 156 82 83
		mu 0 4 20 85 89 18
		f 4 -50 84 85 -157
		mu 0 4 84 21 47 88
		f 4 -57 157 -91 86
		mu 0 4 11 87 91 22
		f 4 -61 87 -93 -158
		mu 0 4 86 50 46 90
		f 4 -17 158 -95 76
		mu 0 4 17 82 92 26
		f 4 -21 77 -99 -159
		mu 0 4 82 31 35 92
		f 4 -97 159 101 102
		mu 0 4 34 93 106 42
		f 4 -101 103 104 -160
		mu 0 4 93 43 24 106
		f 4 -114 160 105 106
		mu 0 4 27 95 105 39
		f 4 -110 107 108 -161
		mu 0 4 94 33 41 104
		f 4 61 161 -116 62
		mu 0 4 7 72 96 12
		f 4 63 64 -112 -162
		mu 0 4 73 51 25 97
		f 4 -125 162 116 117
		mu 0 4 30 99 109 38
		f 4 -121 118 119 -163
		mu 0 4 98 40 48 108
		f 4 72 163 -127 73
		mu 0 4 14 77 100 15
		f 4 74 75 -123 -164
		mu 0 4 78 52 28 101
		f 4 78 164 -128 79
		mu 0 4 9 83 102 32
		f 4 80 81 -132 -165
		mu 0 4 83 29 37 102
		f 4 -130 165 134 135
		mu 0 4 36 103 107 44
		f 4 -134 136 137 -166
		mu 0 4 103 45 23 107
		f 4 -83 166 -109 88
		mu 0 4 18 89 104 41
		f 4 -86 89 -106 -167
		mu 0 4 88 47 39 105
		f 4 32 167 -105 33
		mu 0 4 3 64 106 24
		f 4 34 35 -102 -168
		mu 0 4 64 19 42 106
		f 4 50 168 -138 51
		mu 0 4 5 68 107 23
		f 4 52 53 -135 -169
		mu 0 4 68 53 44 107
		f 4 90 169 -120 91
		mu 0 4 22 91 108 48
		f 4 92 93 -117 -170
		mu 0 4 90 46 38 109
		f 4 -15 -5 170 171
		mu 0 4 58 16 55 111
		f 4 -1 -12 172 -171
		mu 0 4 54 0 57 110
		f 4 -8 -19 -172 -173
		mu 0 4 56 1 58 111
		f 4 -14 -26 173 174
		mu 0 4 62 2 60 113
		f 4 -22 -31 175 -174
		mu 0 4 59 49 61 112
		f 4 -29 -10 -175 -176
		mu 0 4 61 8 63 112
		f 4 -44 -35 176 177
		mu 0 4 66 19 64 114
		f 4 -33 -41 178 -177
		mu 0 4 64 3 65 114
		f 4 -37 -48 -178 -179
		mu 0 4 65 4 67 114
		f 4 -43 -53 179 180
		mu 0 4 71 53 68 115
		f 4 -51 -59 181 -180
		mu 0 4 68 5 70 115
		f 4 -55 -39 -181 -182
		mu 0 4 69 10 71 115
		f 4 -7 -64 182 183
		mu 0 4 75 51 73 117
		f 4 -62 -70 184 -183
		mu 0 4 72 7 74 116
		f 4 -66 -3 -184 -185
		mu 0 4 74 13 76 116
		f 4 -72 -75 185 186
		mu 0 4 81 52 78 119
		f 4 -73 -24 187 -186
		mu 0 4 77 14 80 118
		f 4 -28 -68 -187 -188
		mu 0 4 79 6 81 119;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "F1E5A743-424A-4A9A-0D9A-F29730BBDF64";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "2C3DA5B6-4CA3-D228-28EC-96AE371DD9F8";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "F355000A-42E8-8F2C-01F0-D1BC96F92D8D";
createNode displayLayerManager -n "layerManager";
	rename -uid "7CAA528D-4DEB-1C5B-B5C9-9794A840A83C";
createNode displayLayer -n "defaultLayer";
	rename -uid "090D0899-449D-6AE4-D903-0793C17E2E1E";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "97D9264C-4DBF-36DF-DA35-03B4E8D709C0";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "2AB6E545-4527-9173-0717-8B93A770303F";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "425007B7-40D9-0478-7280-5FB06C07584D";
	setAttr ".version" -type "string" "5.2.1.1";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "E27E968D-433A-A8C0-A23C-70BE7669083B";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "87E92C56-4B1E-36A7-61C6-AEAB844976F4";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "ACC501ED-4F12-3D1E-4B7A-D0A503525CA8";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode polyCloseBorder -n "Table_Pieces_polyCloseBorder1";
	rename -uid "ACCE69C5-441D-04E4-E3A0-BDB1A2F70799";
	setAttr ".ics" -type "componentList" 1 "e[*]";
createNode polySplit -n "Table_Pieces_polySplit18";
	rename -uid "7AAD30AE-4570-780B-63E4-CD9019CD2F95";
	setAttr -s 21 ".e[0:20]"  0.90315098 0.096849397 0.096849397 0.096849397
		 0.90315098 0.096849397 0.096849397 0.90315098 0.096849397 0.096849397 0.90315098
		 0.096849397 0.096849397 0.90315098 0.90315098 0.096849397 0.096849397 0.90315098
		 0.90315098 0.096849397 0.90315098;
	setAttr -s 21 ".d[0:20]"  -2147483406 -2147483405 -2147483404 -2147483401 -2147483400 -2147483399 
		-2147483398 -2147483397 -2147483396 -2147483387 -2147483390 -2147483389 -2147483388 -2147483391 -2147483395 -2147483394 -2147483393 -2147483392 
		-2147483403 -2147483402 -2147483406;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "Table_Pieces_polyTweak13";
	rename -uid "AAC87338-44F5-F6BB-A016-1884EBB709E8";
	setAttr ".uopa" yes;
	setAttr -s 55 ".tk";
	setAttr ".tk[55]" -type "float3" 0.0042792694 -0.06353461 0.0050039701 ;
	setAttr ".tk[56]" -type "float3" 0.0042798063 -0.06353461 0.0024897642 ;
	setAttr ".tk[57]" -type "float3" 0.0042799604 -0.06353461 1.9700415e-16 ;
	setAttr ".tk[58]" -type "float3" 3.6601485e-18 -0.06353461 0.0050031831 ;
	setAttr ".tk[59]" -type "float3" 3.660146e-18 -0.06353461 -9.3197282e-19 ;
	setAttr ".tk[60]" -type "float3" 3.660146e-18 -0.06353461 0.0024898979 ;
	setAttr ".tk[61]" -type "float3" -0.0042792694 -0.06353461 0.0050039701 ;
	setAttr ".tk[62]" -type "float3" -0.0042799604 -0.06353461 -1.9884053e-16 ;
	setAttr ".tk[63]" -type "float3" -0.0042798063 -0.06353461 0.0024897647 ;
	setAttr ".tk[64]" -type "float3" 0.0042792694 -0.06353461 -0.0050039701 ;
	setAttr ".tk[65]" -type "float3" 0.0042798063 -0.06353461 -0.0024897647 ;
	setAttr ".tk[66]" -type "float3" 3.5895581e-18 -0.06353461 -0.0050031831 ;
	setAttr ".tk[67]" -type "float3" 3.660146e-18 -0.06353461 -0.0024898979 ;
	setAttr ".tk[68]" -type "float3" -0.0042792694 -0.06353461 -0.0050039701 ;
	setAttr ".tk[69]" -type "float3" -0.0042798063 -0.06353461 -0.0024897642 ;
	setAttr ".tk[110]" -type "float3" 0.0069856714 -0.06353461 0.0049278014 ;
	setAttr ".tk[111]" -type "float3" 0.0069993711 -0.06353461 0.0049316795 ;
	setAttr ".tk[112]" -type "float3" 0.0061102514 -0.06353461 0.0061212014 ;
	setAttr ".tk[113]" -type "float3" 0.0061004758 -0.06353461 0.0061119976 ;
	setAttr ".tk[114]" -type "float3" 0.0042092805 -0.06353461 0.0070009911 ;
	setAttr ".tk[115]" -type "float3" 0.0042054546 -0.06353461 0.0069890833 ;
	setAttr ".tk[116]" -type "float3" 0.0071521308 -0.06353461 0.0024836948 ;
	setAttr ".tk[117]" -type "float3" 0.0071382271 -0.06353461 0.0024833605 ;
	setAttr ".tk[118]" -type "float3" 0.0071385233 -0.06353461 5.4409405e-16 ;
	setAttr ".tk[119]" -type "float3" 0.0071524214 -0.06353461 5.2980397e-16 ;
	setAttr ".tk[120]" -type "float3" -3.7576906e-22 -0.06353461 0.0071858596 ;
	setAttr ".tk[121]" -type "float3" 0 -0.06353461 0.0071737813 ;
	setAttr ".tk[122]" -type "float3" -0.00611025 -0.06353461 0.0061212 ;
	setAttr ".tk[123]" -type "float3" -0.0061004856 -0.06353461 0.0061120023 ;
	setAttr ".tk[124]" -type "float3" -0.0042054546 -0.06353461 0.0069890833 ;
	setAttr ".tk[125]" -type "float3" -0.0042092805 -0.06353461 0.0070009911 ;
	setAttr ".tk[126]" -type "float3" -0.0069993711 -0.06353461 0.0049316795 ;
	setAttr ".tk[127]" -type "float3" -0.0069856714 -0.06353461 0.0049278014 ;
	setAttr ".tk[128]" -type "float3" -0.0071524214 -0.06353461 -5.2980841e-16 ;
	setAttr ".tk[129]" -type "float3" -0.0071385233 -0.06353461 -5.4410665e-16 ;
	setAttr ".tk[130]" -type "float3" -0.0071382271 -0.06353461 0.0024833605 ;
	setAttr ".tk[131]" -type "float3" -0.0071521308 -0.06353461 0.0024836948 ;
	setAttr ".tk[132]" -type "float3" 0.00611025 -0.06353461 -0.0061212 ;
	setAttr ".tk[133]" -type "float3" 0.0061004856 -0.06353461 -0.0061120023 ;
	setAttr ".tk[134]" -type "float3" 0.0042054546 -0.06353461 -0.0069890833 ;
	setAttr ".tk[135]" -type "float3" 0.0042092805 -0.06353461 -0.0070009911 ;
	setAttr ".tk[136]" -type "float3" 0.0069993711 -0.06353461 -0.0049316795 ;
	setAttr ".tk[137]" -type "float3" 0.0069856714 -0.06353461 -0.0049278014 ;
	setAttr ".tk[138]" -type "float3" 0.0071382271 -0.06353461 -0.0024833605 ;
	setAttr ".tk[139]" -type "float3" 0.0071521308 -0.06353461 -0.0024836948 ;
	setAttr ".tk[140]" -type "float3" 1.8059411e-21 -0.06353461 -0.0071737813 ;
	setAttr ".tk[141]" -type "float3" -2.3870756e-21 -0.06353461 -0.0071858596 ;
	setAttr ".tk[142]" -type "float3" -0.0069856714 -0.06353461 -0.0049278014 ;
	setAttr ".tk[143]" -type "float3" -0.0069993711 -0.06353461 -0.0049316795 ;
	setAttr ".tk[144]" -type "float3" -0.0061102514 -0.06353461 -0.0061212014 ;
	setAttr ".tk[145]" -type "float3" -0.0061004758 -0.06353461 -0.0061119976 ;
	setAttr ".tk[146]" -type "float3" -0.0042092805 -0.06353461 -0.0070009911 ;
	setAttr ".tk[147]" -type "float3" -0.0042054546 -0.06353461 -0.0069890833 ;
	setAttr ".tk[148]" -type "float3" -0.0071521308 -0.06353461 -0.0024836948 ;
	setAttr ".tk[149]" -type "float3" -0.0071382271 -0.06353461 -0.0024833605 ;
createNode polyDelEdge -n "Table_Pieces_polyDelEdge6";
	rename -uid "7C0256B6-4A38-2477-0716-E59425B9DBF2";
	setAttr ".ics" -type "componentList" 20 "e[161]" "e[163]" "e[165]" "e[167]" "e[169]" "e[171]" "e[173]" "e[175]" "e[177]" "e[179]" "e[181]" "e[183]" "e[185]" "e[187]" "e[189]" "e[191]" "e[193]" "e[195]" "e[197]" "e[199]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "Table_Pieces_polyDelEdge5";
	rename -uid "FEE3C0D9-4001-91AA-FC1E-4FB71F13D97D";
	setAttr ".ics" -type "componentList" 20 "e[162]" "e[165]" "e[170]" "e[174]" "e[177]" "e[181]" "e[186]" "e[190]" "e[193]" "e[198]" "e[202]" "e[205]" "e[210]" "e[213]" "e[217]" "e[222]" "e[225]" "e[230]" "e[233]" "e[238]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "Table_Pieces_polyDelEdge4";
	rename -uid "591CD7A8-4FA1-C3B7-276D-B0BF2EC1E1E8";
	setAttr ".ics" -type "componentList" 20 "e[241]" "e[245]" "e[250]" "e[254]" "e[262]" "e[266]" "e[268]" "e[271]" "e[279]" "e[281]" "e[284]" "e[292]" "e[295]" "e[303]" "e[307]" "e[309]" "e[312]" "e[314]" "e[318]" "e[323]";
	setAttr ".cv" yes;
createNode polyBevel3 -n "Table_Pieces_polyBevel1";
	rename -uid "B4131088-4C9D-8112-62FC-7FBE7095D814";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 34 "e[100]" "e[102]" "e[105]" "e[107:108]" "e[111]" "e[113]" "e[116:118]" "e[121]" "e[123]" "e[125]" "e[127:128]" "e[131]" "e[133]" "e[135:137]" "e[140]" "e[144]" "e[148]" "e[154]" "e[160]" "e[163]" "e[167]" "e[172]" "e[174]" "e[179]" "e[182]" "e[186]" "e[189]" "e[192]" "e[199]" "e[202]" "e[206]" "e[209]" "e[212]" "e[215]";
	setAttr ".ix" -type "matrix" 5.3495440598531037 0 0 0 0 0.45296313956898321 0 0 0 0 6.1639835913336682 0
		 0 2.5226111474796236 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.2;
	setAttr ".sg" 2;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "Table_Pieces_polyTweak12";
	rename -uid "6182A8F4-4F15-608D-7746-31819FCB3F63";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[110:129]" -type "float3"  -0.01260991 0 -0.01260991
		 -0.014536869 0 -0.010029458 -0.014849087 0 -0.0049913153 -0.014849087 0 -3.1075786e-20
		 -0.014849087 0 0.0049913153 -0.014536869 0 0.010029458 -0.01260991 0 0.01260991 -0.0085797105
		 0 0.014473202 -1.0267595e-20 0 0.014849087 0.0085797105 0 0.014473202 0.01260991
		 0 0.01260991 0.014536869 0 0.010029458 0.014849087 0 0.0049913153 0.014849087 0 2.2429849e-20
		 0.014849087 0 -0.0049913153 0.014536869 0 -0.010029458 0.01260991 0 -0.01260991 0.0085797105
		 0 -0.014473202 1.207456e-20 0 -0.014849087 -0.0085797105 0 -0.014473202;
createNode polySplit -n "Table_Pieces_polySplit17";
	rename -uid "E356F76E-4C59-55D1-6AE9-F7B212246E5D";
	setAttr -s 21 ".e[0:20]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 21 ".d[0:20]"  -2147483648 -2147483643 -2147483628 -2147483639 -2147483589 -2147483584 
		-2147483583 -2147483596 -2147483577 -2147483559 -2147483558 -2147483570 -2147483560 -2147483606 -2147483613 -2147483607 -2147483605 -2147483620 
		-2147483621 -2147483636 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "Table_Pieces_polyExtrudeFace2";
	rename -uid "2FEBA60A-4C20-3569-443B-A78D0D30A040";
	setAttr ".ics" -type "componentList" 12 "f[1:2]" "f[10:11]" "f[15:16]" "f[18:19]" "f[27:28]" "f[32:33]" "f[35:36]" "f[44:45]" "f[49:50]" "f[52:53]" "f[61:62]" "f[66:67]";
	setAttr ".ix" -type "matrix" 5.3495440598531037 0 0 0 0 0.45296313956898321 0 0 0 0 6.1639835913336682 0
		 0 2.5226111474796236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 2.7535586 0 ;
	setAttr ".rs" 38452;
	setAttr ".lt" -type "double3" -1.3216581183693541e-15 -3.8776490698944066e-16 0.07110851277855762 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.5847720601774595 2.7486512344067546 -2.9918772572735532 ;
	setAttr ".cbx" -type "double3" 2.5847720601774595 2.7584660203760856 2.9918772572735532 ;
createNode polyExtrudeFace -n "Table_Pieces_polyExtrudeFace1";
	rename -uid "EBF2E067-4A11-0648-B45B-F8B28D9783F9";
	setAttr ".ics" -type "componentList" 12 "f[1:2]" "f[10:11]" "f[15:16]" "f[18:19]" "f[27:28]" "f[32:33]" "f[35:36]" "f[44:45]" "f[49:50]" "f[52:53]" "f[61:62]" "f[66:67]";
	setAttr ".ix" -type "matrix" 5.3495440598531037 0 0 0 0 0.45296313956898321 0 0 0 0 6.1639835913336682 0
		 0 2.5226111474796236 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 2.7542832 0 ;
	setAttr ".rs" 34660;
	setAttr ".off" 0.090000003576278687;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.6747720299265518 2.7490929332537712 -3.0819917956668341 ;
	setAttr ".cbx" -type "double3" 2.6747720299265518 2.7594733961328024 3.0819917956668341 ;
createNode polyTweak -n "Table_Pieces_polyTweak11";
	rename -uid "71C2AD23-4387-9B91-98C0-BAAF992CA43F";
	setAttr ".uopa" yes;
	setAttr -s 35 ".tk";
	setAttr ".tk[1]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[3]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[4]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[9]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[11]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[13]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[16]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[17]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[18]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[19]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[25]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[28]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[33]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[37]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[38]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[41]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[42]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[49]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[50]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[52]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[53]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[54]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[64]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[65]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[67]" -type "float3" 0 0.34708002 0 ;
	setAttr ".tk[68]" -type "float3" 0 0.34708002 0 ;
createNode deleteComponent -n "Table_Pieces_deleteComponent9";
	rename -uid "C86001E2-4DCC-FABE-F823-B78E20D7B8F4";
	setAttr ".dc" -type "componentList" 1 "vtx[0:71]";
createNode polyDelEdge -n "Table_Pieces_polyDelEdge3";
	rename -uid "922915D5-4F42-CC40-8AB3-14A0BBA45E27";
	setAttr ".ics" -type "componentList" 17 "e[14:15]" "e[19]" "e[31]" "e[41]" "e[71]" "e[73]" "e[77]" "e[86]" "e[91]" "e[113:114]" "e[118]" "e[127]" "e[134]" "e[156]" "e[160:161]" "e[169]" "e[174]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "Table_Pieces_polyDelEdge2";
	rename -uid "33BBB0EA-4494-FDA7-BF3C-7790DF7BBB49";
	setAttr ".ics" -type "componentList" 39 "e[14:16]" "e[26:27]" "e[31:33]" "e[37]" "e[50:51]" "e[55:57]" "e[70]" "e[86]" "e[91]" "e[98]" "e[118]" "e[120]" "e[123]" "e[126]" "e[128]" "e[132]" "e[143:145]" "e[153:155]" "e[160]" "e[165]" "e[172]" "e[188]" "e[190]" "e[192]" "e[195:196]" "e[200]" "e[210]" "e[212]" "e[214]" "e[226:228]" "e[236]" "e[238]" "e[249]" "e[263]" "e[266]" "e[268:269]" "e[273:274]" "e[283:285]" "e[292:294]";
	setAttr ".cv" yes;
createNode polyMirror -n "Table_Pieces_polyMirror4";
	rename -uid "C746667D-4FFB-FD9A-CBEB-5196442227D2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 5.3495440598531037 0 0 0 0 0.45296313956898321 0 0 0 0 6.1639835913336682 0
		 0 2.5226111474796236 0 1;
	setAttr ".ws" yes;
	setAttr ".a" 2;
	setAttr ".ad" 0;
	setAttr ".mtt" 1;
	setAttr ".mt" 0.1;
	setAttr ".cm" yes;
	setAttr ".fnf" 74;
	setAttr ".lnf" 147;
createNode polyMirror -n "Table_Pieces_polyMirror3";
	rename -uid "1D175B04-4134-044B-BA12-4CA1B3DA6E74";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 5.3495440598531037 0 0 0 0 0.45296313956898321 0 0 0 0 6.1639835913336682 0
		 0 2.5226111474796236 0 1;
	setAttr ".ad" 0;
	setAttr ".ma" 1;
	setAttr ".mtt" 1;
	setAttr ".mt" 0.1;
	setAttr ".cm" yes;
	setAttr ".fnf" 37;
	setAttr ".lnf" 73;
createNode polyTweak -n "Table_Pieces_polyTweak10";
	rename -uid "E8B7C138-4F8D-B869-7925-E48AB99C51DF";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[37:48]" -type "float3"  0.012100026 0 0 0.012100018
		 0 0 0.01317605 0 0 0.012100026 0 0 0.012497521 0 0 0.012472548 0 0 0.012472548 0
		 0 0.012100018 0 0 0.012100026 0 0 0.013176055 0 0 0.012100025 0 0 0.012100018 0 0;
createNode polyDelEdge -n "Table_Pieces_polyDelEdge1";
	rename -uid "6021D9D5-4ECE-BD60-5D54-A98A578577BB";
	setAttr ".ics" -type "componentList" 2 "e[63:71]" "e[79:80]";
	setAttr ".cv" yes;
createNode polyTweak -n "Table_Pieces_polyTweak9";
	rename -uid "C0BD1445-4825-B2CF-20A9-99AC2523D4F4";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[39]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[40]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[41]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[42]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[43]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[44]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[45]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[46]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[47]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[48]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[50]" -type "float3" 0 0 -0.033716436 ;
	setAttr ".tk[59]" -type "float3" 0 0 -0.033716436 ;
createNode deleteComponent -n "Table_Pieces_deleteComponent8";
	rename -uid "BBBD3EBF-47E6-F043-E581-79BE749ACFAA";
	setAttr ".dc" -type "componentList" 1 "f[48:95]";
createNode deleteComponent -n "Table_Pieces_deleteComponent7";
	rename -uid "A7190A93-4106-6B5F-4F96-DCB1E9BE66CC";
	setAttr ".dc" -type "componentList" 16 "f[3]" "f[7]" "f[12:13]" "f[16:17]" "f[22:23]" "f[27:46]" "f[56:64]" "f[72:82]" "f[99]" "f[103]" "f[108:109]" "f[112:113]" "f[118:119]" "f[123:142]" "f[152:160]" "f[168:178]";
createNode polyTweak -n "Table_Pieces_polyTweak8";
	rename -uid "22A0DD36-4F51-8600-F4F9-4994109DB730";
	setAttr ".uopa" yes;
	setAttr -s 67 ".tk";
	setAttr ".tk[8]" -type "float3" 0.010513151 -0.015340626 -0.086501837 ;
	setAttr ".tk[9]" -type "float3" 0.010513151 0.015340626 -0.086501867 ;
	setAttr ".tk[10]" -type "float3" -0.010514097 0.015340626 -0.086501852 ;
	setAttr ".tk[11]" -type "float3" -0.010514097 -0.015340626 -0.086501837 ;
	setAttr ".tk[12]" -type "float3" -0.015280302 0 0 ;
	setAttr ".tk[13]" -type "float3" 0.0025731968 -0.0082472321 -0.080752432 ;
	setAttr ".tk[16]" -type "float3" -0.0025625871 -0.0082472619 -0.080752671 ;
	setAttr ".tk[17]" -type "float3" 0.015322693 0 0 ;
	setAttr ".tk[19]" -type "float3" 0.0064374153 0.0090332646 -0.086501852 ;
	setAttr ".tk[20]" -type "float3" -0.01479362 0 0 ;
	setAttr ".tk[21]" -type "float3" 0.01483601 0 0 ;
	setAttr ".tk[22]" -type "float3" -0.0064267083 0.009032852 -0.086501837 ;
	setAttr ".tk[24]" -type "float3" -0.015322693 0 0 ;
	setAttr ".tk[26]" -type "float3" 0.0028179134 0.0052083307 -0.081117645 ;
	setAttr ".tk[27]" -type "float3" -0.015249731 0 0 ;
	setAttr ".tk[28]" -type "float3" 0.015292125 0 0 ;
	setAttr ".tk[29]" -type "float3" -0.0028072749 0.0052080336 -0.081117973 ;
	setAttr ".tk[31]" -type "float3" -0.015249733 0 0 ;
	setAttr ".tk[32]" -type "float3" 0.0028129835 -0.0050691543 -0.081107482 ;
	setAttr ".tk[35]" -type "float3" -0.0028023981 -0.0050697499 -0.081107691 ;
	setAttr ".tk[36]" -type "float3" 0.015292123 0 0 ;
	setAttr ".tk[38]" -type "float3" 0.017777296 -1.5366822e-08 -0.08657784 ;
	setAttr ".tk[45]" -type "float3" 0.017777296 1.5366822e-08 -0.086577781 ;
	setAttr ".tk[47]" -type "float3" -0.06889683 0 0 ;
	setAttr ".tk[48]" -type "float3" -0.068897039 -1.5366822e-08 -0.086579598 ;
	setAttr ".tk[49]" -type "float3" -0.068896845 0 0 ;
	setAttr ".tk[50]" -type "float3" -0.068896845 0 0 ;
	setAttr ".tk[51]" -type "float3" -0.062123626 0 0 ;
	setAttr ".tk[52]" -type "float3" -0.062123612 0 0 ;
	setAttr ".tk[53]" -type "float3" -0.061669677 0 0 ;
	setAttr ".tk[54]" -type "float3" -0.068896845 0 0 ;
	setAttr ".tk[55]" -type "float3" -0.068897024 1.5366822e-08 -0.086579539 ;
	setAttr ".tk[56]" -type "float3" -0.06889683 0 0 ;
	setAttr ".tk[58]" -type "float3" 0.0075806566 1.5366822e-08 -0.086502254 ;
	setAttr ".tk[65]" -type "float3" 0.0075806566 -1.5366822e-08 -0.086502284 ;
	setAttr ".tk[68]" -type "float3" -0.0097628497 -1.5366822e-08 -0.086502284 ;
	setAttr ".tk[75]" -type "float3" -0.0097628497 1.5366822e-08 -0.086502254 ;
	setAttr ".tk[78]" -type "float3" -0.01479362 0 0 ;
	setAttr ".tk[79]" -type "float3" -0.015249733 0 0 ;
	setAttr ".tk[80]" -type "float3" -0.015249731 0 0 ;
	setAttr ".tk[81]" -type "float3" -0.015280302 0 0 ;
	setAttr ".tk[83]" -type "float3" -0.06889683 0 0 ;
	setAttr ".tk[88]" -type "float3" 0.015322693 0 0 ;
	setAttr ".tk[89]" -type "float3" 0.015292125 0 0 ;
	setAttr ".tk[90]" -type "float3" 0.015292123 0 0 ;
	setAttr ".tk[91]" -type "float3" 0.01483601 0 0 ;
	setAttr ".tk[96]" -type "float3" -0.068896815 0 0 ;
	setAttr ".tk[99]" -type "float3" -0.0010760322 1.5366822e-08 -0.086502254 ;
	setAttr ".tk[106]" -type "float3" -0.0010760287 -1.5366822e-08 -0.086502284 ;
	setAttr ".tk[141]" -type "float3" -0.06889683 0 0 ;
	setAttr ".tk[142]" -type "float3" -0.068896845 0 0 ;
	setAttr ".tk[143]" -type "float3" -0.068896845 0 0 ;
	setAttr ".tk[144]" -type "float3" -0.062123626 0 0 ;
	setAttr ".tk[145]" -type "float3" -0.062123612 0 0 ;
	setAttr ".tk[146]" -type "float3" -0.061669677 0 0 ;
	setAttr ".tk[147]" -type "float3" -0.068896845 0 0 ;
	setAttr ".tk[148]" -type "float3" -0.06889683 0 0 ;
	setAttr ".tk[166]" -type "float3" -0.01479362 0 0 ;
	setAttr ".tk[167]" -type "float3" -0.015249733 0 0 ;
	setAttr ".tk[168]" -type "float3" -0.015249731 0 0 ;
	setAttr ".tk[169]" -type "float3" -0.015280302 0 0 ;
	setAttr ".tk[171]" -type "float3" -0.06889683 0 0 ;
	setAttr ".tk[176]" -type "float3" 0.015322693 0 0 ;
	setAttr ".tk[177]" -type "float3" 0.015292125 0 0 ;
	setAttr ".tk[178]" -type "float3" 0.015292123 0 0 ;
	setAttr ".tk[179]" -type "float3" 0.01483601 0 0 ;
	setAttr ".tk[184]" -type "float3" -0.068896815 0 0 ;
createNode polyMirror -n "Table_Pieces_polyMirror2";
	rename -uid "D15FFD02-4426-72F4-8147-BAB04CBAF4AE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 5.3495440598531037 0 0 0 0 0.45296313956898321 0 0 0 0 6.1639835913336682 0
		 0 2.5226111474796236 0 1;
	setAttr ".ws" yes;
	setAttr ".a" 2;
	setAttr ".ad" 0;
	setAttr ".mtt" 1;
	setAttr ".mt" 0.4024350643157959;
	setAttr ".cm" yes;
	setAttr ".fnf" 96;
	setAttr ".lnf" 191;
createNode deleteComponent -n "Table_Pieces_deleteComponent6";
	rename -uid "D29DD1B1-4B4B-C6E9-3A85-95BBD2DA443F";
	setAttr ".dc" -type "componentList" 1 "f[96:191]";
createNode polyTweak -n "Table_Pieces_polyTweak7";
	rename -uid "50235782-446E-96DF-D800-2AAD0B31D0CC";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[39]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[40]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[41]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[42]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[43]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[44]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[49]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[50]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[51]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[52]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[53]" -type "float3" 0 0 0.012656913 ;
	setAttr ".tk[54]" -type "float3" 0 0 0.012656913 ;
createNode polyMirror -n "Table_Pieces_polyMirror1";
	rename -uid "79B8BC8D-46F1-9BD4-9AB2-96A57B765F30";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 5.3495440598531037 0 0 0 0 0.45296313956898321 0 0 0 0 6.1639835913336682 0
		 0 2.5226111474796236 0 1;
	setAttr ".ws" yes;
	setAttr ".a" 2;
	setAttr ".ad" 0;
	setAttr ".mtt" 1;
	setAttr ".mt" 0.40206289291381836;
	setAttr ".cm" yes;
	setAttr ".fnf" 96;
	setAttr ".lnf" 191;
createNode deleteComponent -n "Table_Pieces_deleteComponent5";
	rename -uid "8194F5C1-421B-7BDB-02C5-7DB37561999E";
	setAttr ".dc" -type "componentList" 15 "f[0:1]" "f[3:5]" "f[10:13]" "f[18:19]" "f[25:27]" "f[30:34]" "f[38:39]" "f[45:49]" "f[55:64]" "f[74:82]" "f[92:96]" "f[106:118]" "f[128:149]" "f[170:173]" "f[185:191]";
createNode polyTweak -n "Table_Pieces_polyTweak6";
	rename -uid "81E89420-4F06-5D13-3653-1EB941A383CE";
	setAttr ".uopa" yes;
	setAttr -s 124 ".tk";
	setAttr ".tk[61]" -type "float3" 0 0 -4.8894435e-09 ;
	setAttr ".tk[62]" -type "float3" 0 0 -2.3283064e-09 ;
	setAttr ".tk[63]" -type "float3" 0 0 -5.1222742e-09 ;
	setAttr ".tk[64]" -type "float3" 0 0 -3.0267984e-09 ;
	setAttr ".tk[65]" -type "float3" 0 0 -4.8894435e-09 ;
	setAttr ".tk[66]" -type "float3" 0 0 -4.8894435e-09 ;
	setAttr ".tk[67]" -type "float3" 0 0 -1.1641532e-10 ;
	setAttr ".tk[68]" -type "float3" 0 0 3.6379788e-12 ;
	setAttr ".tk[69]" -type "float3" 0 0 3.4924597e-10 ;
	setAttr ".tk[70]" -type "float3" 0 0 4.8894435e-09 ;
	setAttr ".tk[71]" -type "float3" 0 0 4.8894435e-09 ;
	setAttr ".tk[72]" -type "float3" 0 0 5.1222742e-09 ;
	setAttr ".tk[73]" -type "float3" 0 0 3.0267984e-09 ;
	setAttr ".tk[74]" -type "float3" 0 0 2.3283064e-09 ;
	setAttr ".tk[75]" -type "float3" 0 0 4.8894435e-09 ;
	setAttr ".tk[76]" -type "float3" 0 0 1.9790605e-09 ;
	setAttr ".tk[77]" -type "float3" 0 0 -7.2759576e-12 ;
	setAttr ".tk[78]" -type "float3" 0 0 -1.1641532e-09 ;
	setAttr ".tk[79]" -type "float3" 0 0 -4.8894435e-09 ;
	setAttr ".tk[80]" -type "float3" 0 0 -2.3283064e-09 ;
	setAttr ".tk[81]" -type "float3" 0 0 -5.1222742e-09 ;
	setAttr ".tk[82]" -type "float3" 0 0 -3.0267984e-09 ;
	setAttr ".tk[83]" -type "float3" 0 0 -4.8894435e-09 ;
	setAttr ".tk[84]" -type "float3" 0 0 -4.8894435e-09 ;
	setAttr ".tk[85]" -type "float3" 0 0 -1.1641532e-10 ;
	setAttr ".tk[86]" -type "float3" 0 0 2.1827873e-11 ;
	setAttr ".tk[87]" -type "float3" 0 0 3.4924597e-10 ;
	setAttr ".tk[88]" -type "float3" 0 0 4.8894435e-09 ;
	setAttr ".tk[89]" -type "float3" 0 0 4.8894435e-09 ;
	setAttr ".tk[90]" -type "float3" 0 0 5.1222742e-09 ;
	setAttr ".tk[91]" -type "float3" 0 0 3.0267984e-09 ;
	setAttr ".tk[92]" -type "float3" 0 0 2.3283064e-09 ;
	setAttr ".tk[93]" -type "float3" 0 0 4.8894435e-09 ;
	setAttr ".tk[94]" -type "float3" 0 0 1.9790605e-09 ;
	setAttr ".tk[95]" -type "float3" 0 0 9.094947e-12 ;
	setAttr ".tk[96]" -type "float3" 0 0 -1.1641532e-09 ;
	setAttr ".tk[97]" -type "float3" -0.074925818 0 0 ;
	setAttr ".tk[98]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[99]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[100]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[101]" -type "float3" -0.072712287 0 0 ;
	setAttr ".tk[102]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[103]" -type "float3" -0.074925803 0 0 ;
	setAttr ".tk[104]" -type "float3" -0.074786775 0 0 ;
	setAttr ".tk[105]" -type "float3" -0.074786812 0 0 ;
	setAttr ".tk[106]" -type "float3" -0.072712287 0 0 ;
	setAttr ".tk[107]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[108]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[109]" -type "float3" -0.072712287 0 0 ;
	setAttr ".tk[110]" -type "float3" -0.072712287 0 0 ;
	setAttr ".tk[111]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[112]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[113]" -type "float3" -0.074786775 0 0 ;
	setAttr ".tk[114]" -type "float3" -0.074786812 0 0 ;
	setAttr ".tk[115]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[116]" -type "float3" 0.074925832 0 0 ;
	setAttr ".tk[117]" -type "float3" 0.074765489 0 0 ;
	setAttr ".tk[118]" -type "float3" 0.074765489 0 0 ;
	setAttr ".tk[119]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[120]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[121]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[122]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[123]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[124]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[125]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[126]" -type "float3" 0.074765489 0 0 ;
	setAttr ".tk[127]" -type "float3" 0.074765489 0 0 ;
	setAttr ".tk[128]" -type "float3" 0.074925832 0 0 ;
	setAttr ".tk[129]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[130]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[131]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[132]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[133]" -type "float3" 1.4901161e-08 0 0 ;
	setAttr ".tk[134]" -type "float3" -1.4901161e-08 0 0 ;
	setAttr ".tk[135]" -type "float3" 1.4901161e-08 0 0 ;
	setAttr ".tk[136]" -type "float3" -1.4901161e-08 0 0 ;
	setAttr ".tk[137]" -type "float3" 1.4901161e-08 0 0 ;
	setAttr ".tk[138]" -type "float3" 1.4901161e-08 0 -0.095006861 ;
	setAttr ".tk[139]" -type "float3" -1.4901161e-08 0 -0.095006861 ;
	setAttr ".tk[140]" -type "float3" 0.072373249 0 -0.095006861 ;
	setAttr ".tk[141]" -type "float3" -0.072712287 0 -0.095006861 ;
	setAttr ".tk[142]" -type "float3" 7.4505806e-09 0 -0.095006861 ;
	setAttr ".tk[143]" -type "float3" -1.4901161e-08 0 -0.095006861 ;
	setAttr ".tk[144]" -type "float3" -1.4901161e-08 0 0 ;
	setAttr ".tk[145]" -type "float3" -1.4901161e-08 0 0 ;
	setAttr ".tk[146]" -type "float3" 1.4901161e-08 0 0 ;
	setAttr ".tk[147]" -type "float3" 1.4901161e-08 0 0 ;
	setAttr ".tk[148]" -type "float3" -1.4901161e-08 0 0 ;
	setAttr ".tk[149]" -type "float3" 7.4505806e-09 0 -2.3283064e-10 ;
	setAttr ".tk[150]" -type "float3" -0.072712272 0 0 ;
	setAttr ".tk[151]" -type "float3" 0.072373249 0 0 ;
	setAttr ".tk[152]" -type "float3" -1.4901161e-08 0 -2.3283064e-10 ;
	setAttr ".tk[153]" -type "float3" 1.4901161e-08 0 0.092048384 ;
	setAttr ".tk[154]" -type "float3" 1.4901161e-08 0 0.092048384 ;
	setAttr ".tk[155]" -type "float3" 1.4901161e-08 0 0.094820999 ;
	setAttr ".tk[156]" -type "float3" -1.4901161e-08 0 0.094820999 ;
	setAttr ".tk[157]" -type "float3" -1.4901161e-08 0 0.095006861 ;
	setAttr ".tk[158]" -type "float3" 1.4901161e-08 0 0.092048392 ;
	setAttr ".tk[159]" -type "float3" -1.4901161e-08 0 0.092048392 ;
	setAttr ".tk[160]" -type "float3" 0.072373249 0 0.092048392 ;
	setAttr ".tk[161]" -type "float3" -0.072712287 0 0.092048392 ;
	setAttr ".tk[162]" -type "float3" 7.4505806e-09 0 0.092048392 ;
	setAttr ".tk[163]" -type "float3" -1.4901161e-08 0 0.092048392 ;
	setAttr ".tk[164]" -type "float3" 1.4901161e-08 0 0.095006861 ;
	setAttr ".tk[165]" -type "float3" -1.4901161e-08 0 0.094820999 ;
	setAttr ".tk[166]" -type "float3" 1.4901161e-08 0 0.094820999 ;
	setAttr ".tk[167]" -type "float3" -1.4901161e-08 0 0.092048384 ;
	setAttr ".tk[168]" -type "float3" -1.4901161e-08 0 0.092048384 ;
	setAttr ".tk[169]" -type "float3" 7.4505806e-09 0 0.092048384 ;
	setAttr ".tk[170]" -type "float3" -0.072712272 0 0.092048384 ;
	setAttr ".tk[171]" -type "float3" 0.072373249 0 0.092048384 ;
	setAttr ".tk[172]" -type "float3" -1.4901161e-08 0 0.092048384 ;
	setAttr ".tk[173]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".tk[175]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[176]" -type "float3" 9.3132257e-10 0 -3.7252903e-09 ;
	setAttr ".tk[178]" -type "float3" 4.6566129e-10 0 0.092048392 ;
	setAttr ".tk[179]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[181]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[182]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[186]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[187]" -type "float3" 9.3132257e-10 0 0.092048384 ;
	setAttr ".tk[188]" -type "float3" 0 0 -5.8207661e-11 ;
	setAttr ".tk[189]" -type "float3" 4.6566129e-10 0 -0.095006861 ;
	setAttr ".tk[193]" -type "float3" 0 0 -7.4505806e-09 ;
createNode polySplit -n "Table_Pieces_polySplit16";
	rename -uid "F8508CE4-41C4-9713-8A52-43B9F20C33C0";
	setAttr -s 23 ".e[0:22]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 23 ".d[0:22]"  -2147483459 -2147483423 -2147483406 -2147483350 -2147483407 -2147483320 
		-2147483455 -2147483409 -2147483410 -2147483411 -2147483451 -2147483450 -2147483414 -2147483415 -2147483310 -2147483447 -2147483360 -2147483446 
		-2147483418 -2147483419 -2147483420 -2147483442 -2147483459;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "Table_Pieces_polyTweak5";
	rename -uid "16812296-42AE-69E7-8E4F-1FB947FE2ACD";
	setAttr ".uopa" yes;
	setAttr -s 28 ".tk";
	setAttr ".tk[0]" -type "float3" -0.018608481 -0.0229172 0.018608481 ;
	setAttr ".tk[1]" -type "float3" 0.018608481 -0.0229172 0.018608481 ;
	setAttr ".tk[2]" -type "float3" -0.018608481 0.0229172 0.018608481 ;
	setAttr ".tk[3]" -type "float3" 0.018608481 0.0229172 0.018608481 ;
	setAttr ".tk[4]" -type "float3" -0.018608481 0.0229172 -0.018608481 ;
	setAttr ".tk[5]" -type "float3" 0.018608481 0.0229172 -0.018608481 ;
	setAttr ".tk[6]" -type "float3" -0.018608481 -0.0229172 -0.018608481 ;
	setAttr ".tk[7]" -type "float3" 0.018608481 -0.0229172 -0.018608481 ;
	setAttr ".tk[20]" -type "float3" -0.019219784 -0.013273115 0.019219784 ;
	setAttr ".tk[24]" -type "float3" -0.019219784 -0.013273112 -0.019219784 ;
	setAttr ".tk[25]" -type "float3" 0.019219784 -0.013273112 -0.019219784 ;
	setAttr ".tk[29]" -type "float3" 0.019219784 -0.013273115 0.019219784 ;
	setAttr ".tk[30]" -type "float3" -0.018608481 0.013494231 -0.018608481 ;
	setAttr ".tk[34]" -type "float3" -0.018608481 0.013494229 0.018608481 ;
	setAttr ".tk[35]" -type "float3" 0.018608481 0.013494229 0.018608481 ;
	setAttr ".tk[39]" -type "float3" 0.018608481 0.013494231 -0.018608481 ;
	setAttr ".tk[41]" -type "float3" -0.019181386 0.0083423462 -0.019181386 ;
	setAttr ".tk[45]" -type "float3" -0.019181387 0.0083423238 0.019181387 ;
	setAttr ".tk[46]" -type "float3" 0.019181387 0.0083423238 0.019181387 ;
	setAttr ".tk[50]" -type "float3" 0.019181386 0.0083423462 -0.019181386 ;
	setAttr ".tk[51]" -type "float3" -0.019181386 -0.0081212278 0.019181386 ;
	setAttr ".tk[55]" -type "float3" -0.019181387 -0.0081212083 -0.019181387 ;
	setAttr ".tk[56]" -type "float3" 0.019181387 -0.0081212083 -0.019181387 ;
	setAttr ".tk[60]" -type "float3" 0.019181386 -0.0081212278 0.019181386 ;
	setAttr ".tk[108]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".tk[123]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".tk[170]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[171]" -type "float3" 0 0 -9.3132257e-10 ;
createNode polySplit -n "Table_Pieces_polySplit15";
	rename -uid "8D879925-43E9-6DE1-3AB5-C8BE7A22B87C";
	setAttr -s 21 ".e[0:20]"  0.89999998 0.1 0.1 0.89999998 0.89999998
		 0.1 0.1 0.1 0.89999998 0.1 0.1 0.1 0.1 0.89999998 0.89999998 0.89999998 0.89999998
		 0.1 0.89999998 0.89999998 0.89999998;
	setAttr -s 21 ".d[0:20]"  -2147483640 -2147483581 -2147483560 -2147483539 -2147483600 -2147483628 
		-2147483462 -2147483390 -2147483438 -2147483498 -2147483625 -2147483596 -2147483535 -2147483554 -2147483575 -2147483637 -2147483506 -2147483430 
		-2147483398 -2147483470 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit14";
	rename -uid "B3108C9C-4748-EC84-B5E4-5EBAE6D4A010";
	setAttr -s 21 ".e[0:20]"  0.89999998 0.1 0.1 0.89999998 0.89999998
		 0.1 0.1 0.1 0.89999998 0.1 0.1 0.1 0.1 0.89999998 0.89999998 0.89999998 0.89999998
		 0.1 0.89999998 0.89999998 0.89999998;
	setAttr -s 21 ".d[0:20]"  -2147483639 -2147483601 -2147483540 -2147483559 -2147483580 -2147483634 
		-2147483471 -2147483399 -2147483429 -2147483507 -2147483631 -2147483576 -2147483555 -2147483534 -2147483595 -2147483638 -2147483497 -2147483439 
		-2147483389 -2147483461 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit13";
	rename -uid "13546462-4D9D-DC45-FB32-F88DE4C98E6B";
	setAttr -s 19 ".e[0:18]"  0.1 0.89999998 0.89999998 0.1 0.1 0.1 0.89999998
		 0.89999998 0.1 0.1 0.89999998 0.89999998 0.1 0.1 0.1 0.89999998 0.1 0.1 0.1;
	setAttr -s 19 ".d[0:18]"  -2147483495 -2147483459 -2147483442 -2147483492 -2147483491 -2147483490 
		-2147483446 -2147483447 -2147483487 -2147483486 -2147483450 -2147483451 -2147483483 -2147483482 -2147483481 -2147483455 -2147483479 -2147483478 
		-2147483495;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit12";
	rename -uid "7065312D-4A89-4740-FC1B-C2B361B6ED2C";
	setAttr -s 19 ".e[0:18]"  0.1 0.89999998 0.89999998 0.89999998 0.1
		 0.89999998 0.89999998 0.89999998 0.1 0.1 0.89999998 0.89999998 0.1 0.1 0.89999998
		 0.89999998 0.89999998 0.1 0.1;
	setAttr -s 19 ".d[0:18]"  -2147483530 -2147483495 -2147483478 -2147483479 -2147483516 -2147483481 
		-2147483482 -2147483483 -2147483520 -2147483521 -2147483486 -2147483487 -2147483524 -2147483525 -2147483490 -2147483491 -2147483492 -2147483529 
		-2147483530;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "Table_Pieces_polyTweak4";
	rename -uid "D6A90CB6-4FAF-DEE9-DAB3-9E96AF6B3A69";
	setAttr ".uopa" yes;
	setAttr -s 60 ".tk";
	setAttr ".tk[0]" -type "float3" 0.09400624 0 -0.09400624 ;
	setAttr ".tk[1]" -type "float3" -0.09400624 0 -0.09400624 ;
	setAttr ".tk[2]" -type "float3" 0.09400624 0 -0.09400624 ;
	setAttr ".tk[3]" -type "float3" -0.09400624 0 -0.09400624 ;
	setAttr ".tk[4]" -type "float3" 0.09400624 0 0.09400624 ;
	setAttr ".tk[5]" -type "float3" -0.09400624 0 0.09400624 ;
	setAttr ".tk[6]" -type "float3" 0.09400624 0 0.09400624 ;
	setAttr ".tk[7]" -type "float3" -0.09400624 0 0.09400624 ;
	setAttr ".tk[20]" -type "float3" 0.097094558 0 -0.097094558 ;
	setAttr ".tk[24]" -type "float3" 0.097094558 0 0.097094558 ;
	setAttr ".tk[25]" -type "float3" -0.097094558 0 0.097094558 ;
	setAttr ".tk[29]" -type "float3" -0.097094558 0 -0.097094558 ;
	setAttr ".tk[30]" -type "float3" 0.09400624 0 0.09400624 ;
	setAttr ".tk[34]" -type "float3" 0.09400624 0 -0.09400624 ;
	setAttr ".tk[35]" -type "float3" -0.09400624 0 -0.09400624 ;
	setAttr ".tk[39]" -type "float3" -0.09400624 0 0.09400624 ;
	setAttr ".tk[41]" -type "float3" 0.096900567 0 0.096900567 ;
	setAttr ".tk[45]" -type "float3" 0.09690062 0 -0.09690062 ;
	setAttr ".tk[46]" -type "float3" -0.09690062 0 -0.09690062 ;
	setAttr ".tk[50]" -type "float3" -0.096900567 0 0.096900567 ;
	setAttr ".tk[51]" -type "float3" 0.096900567 0 -0.096900567 ;
	setAttr ".tk[55]" -type "float3" 0.09690062 0 0.09690062 ;
	setAttr ".tk[56]" -type "float3" -0.09690062 0 0.09690062 ;
	setAttr ".tk[60]" -type "float3" -0.096900567 0 -0.096900567 ;
	setAttr ".tk[61]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[62]" -type "float3" 0 0 2.6077032e-08 ;
	setAttr ".tk[63]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[64]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[65]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[66]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[67]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[68]" -type "float3" 0 0 1.1641532e-10 ;
	setAttr ".tk[69]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[70]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".tk[71]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".tk[72]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[73]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[74]" -type "float3" 0 0 -2.6077032e-08 ;
	setAttr ".tk[75]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".tk[76]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[77]" -type "float3" 0 0 -1.0186341e-10 ;
	setAttr ".tk[78]" -type "float3" 0 0 -5.5879354e-09 ;
	setAttr ".tk[79]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[80]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[81]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[82]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[83]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[84]" -type "float3" 0 0 2.6077032e-08 ;
	setAttr ".tk[85]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[86]" -type "float3" 0 0 -5.5879354e-09 ;
	setAttr ".tk[87]" -type "float3" 0 0 -1.0186341e-10 ;
	setAttr ".tk[88]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[89]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".tk[90]" -type "float3" 0 0 -2.6077032e-08 ;
	setAttr ".tk[91]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[92]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[93]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".tk[94]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".tk[95]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[96]" -type "float3" 0 0 1.1641532e-10 ;
createNode polySplit -n "Table_Pieces_polySplit11";
	rename -uid "630B67E4-4F89-8197-D033-D8A665CAF488";
	setAttr -s 19 ".e[0:18]"  0.40000001 0.60000002 0.60000002 0.40000001
		 0.40000001 0.40000001 0.60000002 0.60000002 0.40000001 0.40000001 0.60000002 0.60000002
		 0.40000001 0.40000001 0.40000001 0.60000002 0.40000001 0.40000001 0.40000001;
	setAttr -s 19 ".d[0:18]"  -2147483622 -2147483530 -2147483529 -2147483557 -2147483578 -2147483621 
		-2147483525 -2147483524 -2147483618 -2147483617 -2147483521 -2147483520 -2147483537 -2147483598 -2147483616 -2147483516 -2147483614 -2147483613 
		-2147483622;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit10";
	rename -uid "C1583BED-42D2-961E-4796-3D8FDDD366F4";
	setAttr -s 19 ".e[0:18]"  0.69999999 0.30000001 0.30000001 0.69999999
		 0.69999999 0.69999999 0.30000001 0.30000001 0.69999999 0.69999999 0.30000001 0.30000001
		 0.69999999 0.69999999 0.69999999 0.30000001 0.69999999 0.69999999 0.69999999;
	setAttr -s 19 ".d[0:18]"  -2147483622 -2147483593 -2147483532 -2147483557 -2147483578 -2147483621 
		-2147483620 -2147483619 -2147483618 -2147483617 -2147483573 -2147483552 -2147483537 -2147483598 -2147483616 -2147483615 -2147483614 -2147483613 
		-2147483622;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode deleteComponent -n "Table_Pieces_deleteComponent4";
	rename -uid "FD2C4BC6-42C0-EAFC-5ABB-DCB3953B6451";
	setAttr ".dc" -type "componentList" 9 "vtx[20:29]" "vtx[35]" "vtx[41]" "vtx[47]" "vtx[53]" "vtx[60]" "vtx[66]" "vtx[72]" "vtx[78]";
createNode deleteComponent -n "Table_Pieces_deleteComponent3";
	rename -uid "F5912690-4D8E-4193-B11F-01B38DA423BD";
	setAttr ".dc" -type "componentList" 9 "e[46:55]" "e[61]" "e[67]" "e[85]" "e[91]" "e[110]" "e[116]" "e[134]" "e[140]";
createNode polyTweak -n "Table_Pieces_polyTweak3";
	rename -uid "68E74FDF-4BD2-24D1-E5A6-6C958A9D351F";
	setAttr ".uopa" yes;
	setAttr -s 49 ".tk";
	setAttr ".tk[20]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[21]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[22]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[23]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[24]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[25]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[26]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[27]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[28]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[29]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[30]" -type "float3" -0.016425574 1.4366451e-08 0.016425574 ;
	setAttr ".tk[31]" -type "float3" -0.016425574 -1.4366444e-08 0.0089123696 ;
	setAttr ".tk[32]" -type "float3" -0.016425574 -1.4366444e-08 -0.00010183919 ;
	setAttr ".tk[33]" -type "float3" -0.016425574 1.4366451e-08 -0.0082525797 ;
	setAttr ".tk[34]" -type "float3" -0.016425574 -1.4366444e-08 -0.016425574 ;
	setAttr ".tk[35]" -type "float3" 3.3080578e-06 -1.4366444e-08 -0.016425574 ;
	setAttr ".tk[36]" -type "float3" 0.016425574 -1.4366444e-08 -0.016425574 ;
	setAttr ".tk[37]" -type "float3" 0.016425574 -1.4366444e-08 -0.0082525797 ;
	setAttr ".tk[38]" -type "float3" 0.016425574 1.4366451e-08 -0.00010183907 ;
	setAttr ".tk[39]" -type "float3" 0.016425574 1.4366451e-08 0.0089123696 ;
	setAttr ".tk[40]" -type "float3" 0.016425574 1.4366451e-08 0.016425574 ;
	setAttr ".tk[41]" -type "float3" 3.3080578e-06 1.4366451e-08 0.016425574 ;
	setAttr ".tk[47]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[53]" -type "float3" -2.9802322e-08 0 0 ;
	setAttr ".tk[54]" -type "float3" 0 1.8385767e-07 0 ;
	setAttr ".tk[55]" -type "float3" 0 0.12119825 0 ;
	setAttr ".tk[56]" -type "float3" 0 0.12119795 0 ;
	setAttr ".tk[57]" -type "float3" 0 0.12119825 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.12119825 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.12119795 0 ;
	setAttr ".tk[60]" -type "float3" -2.9802322e-08 0.12119795 0 ;
	setAttr ".tk[61]" -type "float3" 0 0.12119795 0 ;
	setAttr ".tk[62]" -type "float3" 0 0.12119795 0 ;
	setAttr ".tk[63]" -type "float3" 0 0.12119795 0 ;
	setAttr ".tk[64]" -type "float3" 0 0.12119825 0 ;
	setAttr ".tk[65]" -type "float3" 0 0.12119825 0 ;
	setAttr ".tk[66]" -type "float3" -2.9802322e-08 0.12119825 0 ;
	setAttr ".tk[67]" -type "float3" 0 -0.12119825 0 ;
	setAttr ".tk[68]" -type "float3" 0 -0.12119796 0 ;
	setAttr ".tk[69]" -type "float3" 0 -0.12119796 0 ;
	setAttr ".tk[70]" -type "float3" 0 -0.12119825 0 ;
	setAttr ".tk[71]" -type "float3" 0 -0.12119796 0 ;
	setAttr ".tk[72]" -type "float3" -2.9802322e-08 -0.12119796 0 ;
	setAttr ".tk[73]" -type "float3" 0 -0.12119796 0 ;
	setAttr ".tk[74]" -type "float3" 0 -0.12119796 0 ;
	setAttr ".tk[75]" -type "float3" 0 -0.12119825 0 ;
	setAttr ".tk[76]" -type "float3" 0 -0.12119825 0 ;
	setAttr ".tk[77]" -type "float3" 0 -0.12119825 0 ;
	setAttr ".tk[78]" -type "float3" -2.9802322e-08 -0.12119825 0 ;
createNode deleteComponent -n "Table_Pieces_deleteComponent2";
	rename -uid "50421389-4616-C1A9-260E-978110468E1D";
	setAttr ".dc" -type "componentList" 2 "vtx[54:55]" "vtx[57:65]";
createNode deleteComponent -n "Table_Pieces_deleteComponent1";
	rename -uid "6B9FF9BB-4E18-D86A-F274-1CA198E0DE4F";
	setAttr ".dc" -type "componentList" 1 "e[116:127]";
createNode polyTweak -n "Table_Pieces_polyTweak2";
	rename -uid "4CBF7692-4834-C1FD-34DB-7EB8470A9CB8";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk[66:89]" -type "float3"  -0.0011088494 0 -0.0011088494
		 -0.0011088513 0 -0.0005571181 -0.0011088494 0 -6.8748559e-06 -0.0011088494 0 0.00060166139
		 -0.0011088513 0 0.0011088513 2.2617473e-07 0 0.0011088513 0.0011088513 0 0.0011088513
		 0.0011088513 0 0.00060165953 0.0011088513 0 -6.8748268e-06 0.0011088494 0 -0.0005571181
		 0.0011088494 0 -0.0011088494 2.2617837e-07 0 -0.0011088494 -0.0011088494 0 0.0011088494
		 -0.0011088513 0 0.00060166139 -0.0011088513 0 -6.875096e-06 -0.0011088494 0 -0.0005571181
		 -0.0011088513 0 -0.0011088513 2.2617473e-07 0 -0.0011088513 0.0011088513 0 -0.0011088513
		 0.0011088513 0 -0.0005571181 0.0011088494 0 -6.8748632e-06 0.0011088494 0 0.00060166419
		 0.0011088494 0 0.0011088494 2.2617837e-07 0 0.0011088494;
createNode polySplit -n "Table_Pieces_polySplit9";
	rename -uid "4E901E03-46CB-6BB8-FEA9-20B53531073E";
	setAttr -s 13 ".e[0:12]"  0.80000001 0.2 0.2 0.80000001 0.2 0.2 0.2
		 0.2 0.80000001 0.80000001 0.80000001 0.80000001 0.80000001;
	setAttr -s 13 ".d[0:12]"  -2147483592 -2147483543 -2147483542 -2147483589 -2147483540 -2147483539 
		-2147483538 -2147483537 -2147483584 -2147483583 -2147483582 -2147483581 -2147483592;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit8";
	rename -uid "B509B11F-4C5C-1735-DF9C-FD852888ECCF";
	setAttr -s 13 ".e[0:12]"  0.80000001 0.2 0.80000001 0.80000001 0.2
		 0.2 0.2 0.2 0.2 0.80000001 0.80000001 0.80000001 0.80000001;
	setAttr -s 13 ".d[0:12]"  -2147483568 -2147483541 -2147483566 -2147483565 -2147483544 -2147483533 
		-2147483534 -2147483535 -2147483536 -2147483559 -2147483558 -2147483557 -2147483568;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "Table_Pieces_polyTweak1";
	rename -uid "72A67F64-4CCE-FE10-5539-9CA82B9F00FB";
	setAttr ".uopa" yes;
	setAttr -s 36 ".tk[30:65]" -type "float3"  0 0.16746971 0 0 0.16746962
		 0 0 0.16746962 0 0 0.16746971 0 0 0.16746962 0 0 0.16746962 0 0 0.16746962 0 0 0.16746962
		 0 0 0.16746971 0 0 0.16746971 0 0 0.16746971 0 0 0.16746971 0 0 -0.16746971 0 0 -0.16746962
		 0 0 -0.16746971 0 0 -0.16746971 0 0 -0.16746962 0 0 -0.16746962 0 0 -0.16746962 0
		 0 -0.16746962 0 0 -0.16746962 0 0 -0.16746971 0 0 -0.16746971 0 0 -0.16746971 0 -0.017856242
		 0 0.017856242 -0.017856242 0 0.00968864 -0.017856242 0 -0.00011070856 -0.017856242
		 0 -0.0089713736 -0.017856242 0 -0.017856242 3.6420827e-06 0 -0.017856242 0.017856242
		 0 -0.017856242 0.017856242 0 -0.0089713736 0.017856242 0 -0.00011070855 0.017856242
		 0 0.0096886409 0.017856242 0 0.017856242 3.6420827e-06 0 0.017856242;
createNode polySplit -n "Table_Pieces_polySplit7";
	rename -uid "13470FFE-4681-B7F4-4666-6783F692E16F";
	setAttr -s 13 ".e[0:12]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5;
	setAttr -s 13 ".d[0:12]"  -2147483592 -2147483565 -2147483566 -2147483589 -2147483568 -2147483557 
		-2147483558 -2147483559 -2147483584 -2147483583 -2147483582 -2147483581 -2147483592;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit6";
	rename -uid "1AD1CFDE-4E51-256A-F273-84A2BA1D9F3A";
	setAttr -s 13 ".e[0:12]"  0.039827801 0.960172 0.039827801 0.039827801
		 0.960172 0.960172 0.960172 0.960172 0.960172 0.039827801 0.039827801 0.039827801
		 0.039827801;
	setAttr -s 13 ".d[0:12]"  -2147483640 -2147483589 -2147483632 -2147483624 -2147483592 -2147483581 
		-2147483582 -2147483583 -2147483584 -2147483614 -2147483639 -2147483597 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit5";
	rename -uid "8D035C0D-4668-000A-0AFC-4CAF009E047A";
	setAttr -s 13 ".e[0:12]"  0.042941801 0.95705801 0.95705801 0.042941801
		 0.95705801 0.95705801 0.95705801 0.95705801 0.042941801 0.042941801 0.042941801 0.042941801
		 0.042941801;
	setAttr -s 13 ".d[0:12]"  -2147483644 -2147483624 -2147483632 -2147483616 -2147483640 -2147483597 
		-2147483639 -2147483614 -2147483630 -2147483622 -2147483643 -2147483602 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit4";
	rename -uid "B205E80C-4E54-A9B1-9FB1-DEA18B2DA92A";
	setAttr -s 11 ".e[0:10]"  0.50010198 0.50010198 0.49989799 0.49989799
		 0.50010198 0.50010198 0.50010198 0.49989799 0.50010198 0.50010198 0.50010198;
	setAttr -s 11 ".d[0:10]"  -2147483648 -2147483647 -2147483621 -2147483629 -2147483615 -2147483646 
		-2147483645 -2147483613 -2147483631 -2147483623 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit3";
	rename -uid "48F3DBB0-4BEA-BC70-89D4-17953C657BF8";
	setAttr -s 5 ".e[0:4]"  0.500682 0.499318 0.499318 0.500682 0.500682;
	setAttr -s 5 ".d[0:4]"  -2147483638 -2147483636 -2147483633 -2147483637 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit2";
	rename -uid "464883D8-4F96-075A-8700-53AE276AF247";
	setAttr -s 5 ".e[0:4]"  0.45458999 0.54540998 0.54540998 0.45458999
		 0.45458999;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483635 -2147483634 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "Table_Pieces_polySplit1";
	rename -uid "9CF2637F-4146-4676-D5A1-DC9F67B4D734";
	setAttr -s 5 ".e[0:4]"  0.50309998 0.49689999 0.49689999 0.50309998
		 0.50309998;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483638 -2147483637 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCube -n "Table_Pieces_polyCube1";
	rename -uid "1B5A7AA5-4657-BC46-73C0-37B851064B8C";
	setAttr ".cuv" 4;
createNode objectSet -n "set1";
	rename -uid "88922357-454B-4455-5934-FB8FAC4250F7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId1";
	rename -uid "5721ED59-4EC1-D7A7-8056-9F8F0BA1DCB0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "6D619EA7-412E-7F35-7FA0-A5976A1286DA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 18 "e[0:51]" "e[98:109]" "e[120:123]" "e[133:142]" "e[153:172]" "e[175:182]" "e[185:188]" "e[206:214]" "e[222:232]" "e[242:252]" "e[262:270]" "e[272]" "e[282:284]" "e[286:287]" "e[289]" "e[296:304]" "e[314:323]" "e[334:335]";
createNode deleteComponent -n "deleteComponent1";
	rename -uid "9E6BAE79-4A1A-9837-1E3A-04B3A70596E6";
	setAttr ".dc" -type "componentList" 11 "f[0:25]" "f[52:54]" "f[65:75]" "f[78:85]" "f[94:111]" "f[114:115]" "f[134:136]" "f[138:139]" "f[141]" "f[148:155]" "f[166:167]";
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "5F5E70C1-4B1B-79F0-D3CF-BBAA757C3593";
	setAttr ".ics" -type "componentList" 11 "e[0:5]" "e[52:53]" "e[64]" "e[74]" "e[85]" "e[90]" "e[108:109]" "e[117:118]" "e[128:129]" "e[157]" "e[167]";
createNode polyCube -n "polyCube2";
	rename -uid "F4491DF1-4CD3-1611-3197-859435E315DC";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit9";
	rename -uid "63958C88-494F-146B-8B6F-AEB6B241AFC3";
	setAttr -s 5 ".e[0:4]"  0.177056 0.82294399 0.82294399 0.177056 0.177056;
	setAttr -s 5 ".d[0:4]"  -2147483644 -2147483640 -2147483639 -2147483643 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit10";
	rename -uid "D30C9C61-48FE-5D92-0B28-78B5D2DBBA18";
	setAttr -s 5 ".e[0:4]"  0.84082401 0.15917601 0.15917601 0.84082401
		 0.84082401;
	setAttr -s 5 ".d[0:4]"  -2147483640 -2147483636 -2147483633 -2147483639 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit11";
	rename -uid "03B78C61-40A9-5CD9-1775-6BA8322DB1E4";
	setAttr -s 5 ".e[0:4]"  0.14356001 0.85644001 0.85644001 0.14356001
		 0.14356001;
	setAttr -s 5 ".d[0:4]"  -2147483640 -2147483627 -2147483626 -2147483639 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit12";
	rename -uid "4BE22704-49EA-FD37-F7B4-6BB47F70BEA4";
	setAttr -s 5 ".e[0:4]"  0.378277 0.621723 0.621723 0.378277 0.378277;
	setAttr -s 5 ".d[0:4]"  -2147483640 -2147483619 -2147483618 -2147483639 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "58080947-4CCA-2AC6-2131-4AB6F5D95C32";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[2]" -type "float3" -7.4505806e-09 2.3283064e-10 -2.2351742e-08 ;
	setAttr ".tk[3]" -type "float3" 7.4505806e-09 2.3283064e-10 -2.2351742e-08 ;
	setAttr ".tk[4]" -type "float3" -7.4505806e-09 2.3283064e-10 0.44023871 ;
	setAttr ".tk[5]" -type "float3" 7.4505806e-09 2.3283064e-10 0.44023871 ;
	setAttr ".tk[6]" -type "float3" 0 0 -6.5565109e-07 ;
	setAttr ".tk[7]" -type "float3" 0 0 -6.5565109e-07 ;
	setAttr ".tk[8]" -type "float3" 0 0 -2.2351742e-08 ;
	setAttr ".tk[9]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".tk[10]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".tk[11]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".tk[13]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".tk[20]" -type "float3" 2.9802322e-08 2.3283064e-10 0.12190972 ;
	setAttr ".tk[21]" -type "float3" 0 -2.3283064e-10 -2.9802322e-08 ;
	setAttr ".tk[23]" -type "float3" 7.4505806e-09 0 0.1219097 ;
createNode polySplit -n "polySplit13";
	rename -uid "92D2A855-4EC0-8CB9-0A33-358FE86C2E01";
	setAttr -s 5 ".e[0:4]"  0.043623898 0.95637602 0.95637602 0.043623898
		 0.043623898;
	setAttr -s 5 ".d[0:4]"  -2147483640 -2147483611 -2147483610 -2147483639 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit14";
	rename -uid "4FF1C14A-4803-02C6-A45B-5697FF6148BE";
	setAttr -s 15 ".e[0:14]"  0.0232285 0.97677201 0.0232285 0.0232285
		 0.0232285 0.0232285 0.0232285 0.0232285 0.97677201 0.97677201 0.97677201 0.97677201
		 0.0232285 0.0232285 0.0232285;
	setAttr -s 15 ".d[0:14]"  -2147483648 -2147483629 -2147483623 -2147483615 -2147483607 -2147483599 
		-2147483647 -2147483646 -2147483597 -2147483605 -2147483613 -2147483621 -2147483631 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit15";
	rename -uid "A326DE26-495D-77E8-514C-AFB4AB018714";
	setAttr -s 15 ".e[0:14]"  0.0299668 0.97003299 0.97003299 0.97003299
		 0.0299668 0.0299668 0.0299668 0.0299668 0.97003299 0.97003299 0.97003299 0.97003299
		 0.97003299 0.97003299 0.0299668;
	setAttr -s 15 ".d[0:14]"  -2147483629 -2147483596 -2147483583 -2147483584 -2147483621 -2147483613 
		-2147483605 -2147483597 -2147483589 -2147483590 -2147483591 -2147483592 -2147483593 -2147483594 -2147483629;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit16";
	rename -uid "8F4D938E-494D-F7B7-54B7-0389F911103B";
	setAttr -s 19 ".e[0:18]"  0.033243202 0.966757 0.966757 0.966757 0.966757
		 0.033243202 0.966757 0.966757 0.033243202 0.966757 0.966757 0.033243202 0.033243202
		 0.033243202 0.033243202 0.033243202 0.966757 0.033243202 0.033243202;
	setAttr -s 19 ".d[0:18]"  -2147483642 -2147483600 -2147483608 -2147483616 -2147483624 -2147483632 
		-2147483638 -2147483569 -2147483553 -2147483637 -2147483630 -2147483622 -2147483614 -2147483606 -2147483598 -2147483641 -2147483546 -2147483576 
		-2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit17";
	rename -uid "414CBB92-4783-4C74-DFBC-D69EBE347FB0";
	setAttr -s 19 ".e[0:18]"  0.046529502 0.95346999 0.046529502 0.046529502
		 0.046529502 0.046529502 0.95346999 0.95346999 0.046529502 0.95346999 0.95346999 0.95346999
		 0.95346999 0.95346999 0.046529502 0.046529502 0.95346999 0.046529502 0.046529502;
	setAttr -s 19 ".d[0:18]"  -2147483638 -2147483535 -2147483624 -2147483616 -2147483608 -2147483600 
		-2147483540 -2147483523 -2147483546 -2147483525 -2147483526 -2147483527 -2147483528 -2147483529 -2147483630 -2147483637 -2147483532 -2147483569 
		-2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "27FEDE8B-4751-6FBE-59BF-6499E52E297F";
	setAttr ".uopa" yes;
	setAttr -s 82 ".tk";
	setAttr ".tk[0]" -type "float3" 0.048554283 1.5832484e-08 -0.55187881 ;
	setAttr ".tk[1]" -type "float3" -0.048554283 1.5832484e-08 -0.55187881 ;
	setAttr ".tk[2]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[3]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[4]" -type "float3" 7.4505806e-09 -0.00089767517 -7.4505806e-09 ;
	setAttr ".tk[5]" -type "float3" -7.4505806e-09 -0.00089767517 -7.4505806e-09 ;
	setAttr ".tk[6]" -type "float3" 0.048554283 1.5832484e-08 -0.45477018 ;
	setAttr ".tk[7]" -type "float3" -0.048554283 1.5832484e-08 -0.45477018 ;
	setAttr ".tk[8]" -type "float3" 0 -1.4901161e-08 -0.08755447 ;
	setAttr ".tk[9]" -type "float3" 0 -1.4901161e-08 -0.08755444 ;
	setAttr ".tk[10]" -type "float3" 0 0 -0.08755447 ;
	setAttr ".tk[11]" -type "float3" 0 0 -0.08755447 ;
	setAttr ".tk[12]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".tk[13]" -type "float3" -1.4901161e-08 0 -2.9802322e-08 ;
	setAttr ".tk[16]" -type "float3" -1.4901161e-08 -0.046061739 1.6763806e-08 ;
	setAttr ".tk[17]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[18]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[19]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[20]" -type "float3" -7.4505806e-09 -0.023953484 7.4505806e-09 ;
	setAttr ".tk[21]" -type "float3" -1.4901161e-08 -0.023953484 -3.7252903e-09 ;
	setAttr ".tk[22]" -type "float3" -2.2351742e-08 -0.023953484 -7.4505806e-09 ;
	setAttr ".tk[23]" -type "float3" -2.2351742e-08 -0.023953484 7.4505806e-09 ;
	setAttr ".tk[24]" -type "float3" -0.027162196 -0.0038705247 -0.015581541 ;
	setAttr ".tk[25]" -type "float3" -0.027162196 -0.0038705247 0.015581539 ;
	setAttr ".tk[26]" -type "float3" 0.027162196 -0.0038705247 0.015581539 ;
	setAttr ".tk[27]" -type "float3" 0.027162196 -0.0038705247 -0.015581541 ;
	setAttr ".tk[28]" -type "float3" 0.046298645 1.5832484e-08 -0.55187881 ;
	setAttr ".tk[29]" -type "float3" 0 -1.4901161e-08 -0.08755447 ;
	setAttr ".tk[30]" -type "float3" 4.4703484e-08 0 -2.9802322e-08 ;
	setAttr ".tk[31]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[32]" -type "float3" -1.4901161e-08 -0.023953484 -3.7252903e-09 ;
	setAttr ".tk[33]" -type "float3" -0.025900319 -0.0038705247 0.015581539 ;
	setAttr ".tk[34]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[35]" -type "float3" -1.4901161e-08 -0.00089767517 -7.4505806e-09 ;
	setAttr ".tk[36]" -type "float3" -0.02590034 -0.0038705247 -0.015581541 ;
	setAttr ".tk[37]" -type "float3" -1.1175871e-08 -0.023953484 7.4505806e-09 ;
	setAttr ".tk[38]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[39]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".tk[40]" -type "float3" 0 -1.4901161e-08 -0.08755444 ;
	setAttr ".tk[41]" -type "float3" 0.046298645 1.5832484e-08 -0.45477018 ;
	setAttr ".tk[42]" -type "float3" 0 0 -0.08755447 ;
	setAttr ".tk[43]" -type "float3" -0.045711808 1.5832484e-08 -0.55187881 ;
	setAttr ".tk[44]" -type "float3" -0.045711808 1.5832484e-08 -0.45477018 ;
	setAttr ".tk[45]" -type "float3" 0 -1.4901161e-08 -0.08755444 ;
	setAttr ".tk[47]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[48]" -type "float3" -1.8626451e-08 -0.023953484 7.4505806e-09 ;
	setAttr ".tk[49]" -type "float3" 0.025572076 -0.0038705247 -0.015581541 ;
	setAttr ".tk[50]" -type "float3" 7.4505806e-09 -0.00089767517 -7.4505806e-09 ;
	setAttr ".tk[51]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[52]" -type "float3" 0.025572071 -0.0038705247 0.015581539 ;
	setAttr ".tk[53]" -type "float3" -1.4901161e-08 -0.023953484 -7.4505806e-09 ;
	setAttr ".tk[54]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[56]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[57]" -type "float3" -0.027162196 -0.0038705247 0.014545584 ;
	setAttr ".tk[58]" -type "float3" -1.4901161e-08 -0.023953484 0 ;
	setAttr ".tk[59]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[60]" -type "float3" -1.4901161e-08 0 1.4901161e-08 ;
	setAttr ".tk[61]" -type "float3" 0 -1.4901161e-08 -0.08755447 ;
	setAttr ".tk[62]" -type "float3" 0.048554283 1.5832484e-08 -0.54865062 ;
	setAttr ".tk[63]" -type "float3" 0.046298645 1.5832484e-08 -0.54865062 ;
	setAttr ".tk[64]" -type "float3" -0.045711808 1.5832484e-08 -0.54865062 ;
	setAttr ".tk[65]" -type "float3" -0.048554283 1.5832484e-08 -0.54865062 ;
	setAttr ".tk[66]" -type "float3" 0 0 -0.08755447 ;
	setAttr ".tk[68]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[69]" -type "float3" -2.2351742e-08 -0.023953484 0 ;
	setAttr ".tk[70]" -type "float3" 0.027162196 -0.0038705233 0.014545579 ;
	setAttr ".tk[71]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[74]" -type "float3" 0.048554283 1.5832484e-08 -0.45913842 ;
	setAttr ".tk[75]" -type "float3" 0 -1.4901161e-08 -0.0875545 ;
	setAttr ".tk[76]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".tk[77]" -type "float3" -1.4901161e-08 -0.046061751 -1.6763806e-08 ;
	setAttr ".tk[78]" -type "float3" -7.4505806e-09 -0.023953484 0 ;
	setAttr ".tk[79]" -type "float3" -0.027162196 -0.0038705266 -0.014179733 ;
	setAttr ".tk[80]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[83]" -type "float3" 0 -0.00089767564 0 ;
	setAttr ".tk[84]" -type "float3" 0.027162196 -0.0038705247 -0.014179724 ;
	setAttr ".tk[85]" -type "float3" -2.2351742e-08 -0.023953484 0 ;
	setAttr ".tk[86]" -type "float3" 0 -0.046061721 0 ;
	setAttr ".tk[88]" -type "float3" 0 0 -0.08755447 ;
	setAttr ".tk[89]" -type "float3" -0.048554283 1.5832484e-08 -0.45913842 ;
	setAttr ".tk[90]" -type "float3" -0.045711808 1.5832484e-08 -0.45913842 ;
	setAttr ".tk[91]" -type "float3" 0.046298645 1.5832484e-08 -0.45913842 ;
createNode polySplit -n "polySplit18";
	rename -uid "68CC273E-46EF-BA7B-CADF-EFB9A9137EAD";
	setAttr -s 13 ".e[0:12]"  0.0118625 0.98813802 0.0118625 0.98813802
		 0.98813802 0.0118625 0.98813802 0.98813802 0.0118625 0.0118625 0.98813802 0.0118625
		 0.0118625;
	setAttr -s 13 ".d[0:12]"  -2147483644 -2147483517 -2147483486 -2147483635 -2147483570 -2147483552 
		-2147483634 -2147483472 -2147483513 -2147483643 -2147483554 -2147483582 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCube -n "polyCube3";
	rename -uid "FF9DBAFD-4BDB-26B1-731F-AB88317C5309";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit19";
	rename -uid "9DC561A6-48EE-BCB7-C496-B3AA25DC8D8C";
	setAttr -s 5 ".e[0:4]"  0.0119468 0.98805302 0.98805302 0.0119468
		 0.0119468;
	setAttr -s 5 ".d[0:4]"  -2147483644 -2147483640 -2147483639 -2147483643 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit20";
	rename -uid "E8659048-4A28-893E-CFA9-CCAC960D93C4";
	setAttr -s 5 ".e[0:4]"  0.040991701 0.95900798 0.95900798 0.040991701
		 0.040991701;
	setAttr -s 5 ".d[0:4]"  -2147483640 -2147483636 -2147483633 -2147483639 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit21";
	rename -uid "941F46A7-406A-5BE9-CBBF-868755C0E3A7";
	setAttr -s 9 ".e[0:8]"  0.0260709 0.97392899 0.0260709 0.97392899
		 0.97392899 0.97392899 0.0260709 0.0260709 0.0260709;
	setAttr -s 9 ".d[0:8]"  -2147483642 -2147483624 -2147483632 -2147483638 -2147483637 -2147483630 
		-2147483622 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit22";
	rename -uid "D520E07F-4565-71E2-BCB8-85940E51CE64";
	setAttr -s 9 ".e[0:8]"  0.103358 0.89664203 0.103358 0.89664203 0.89664203
		 0.89664203 0.103358 0.103358 0.103358;
	setAttr -s 9 ".d[0:8]"  -2147483638 -2147483618 -2147483624 -2147483620 -2147483613 -2147483614 
		-2147483630 -2147483637 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "BCBE9F21-4254-326A-CCAC-B7BC00FF3E42";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 768\n            -height 773\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 1\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 0\n            -nurbsCurves 0\n            -nurbsSurfaces 0\n            -polymeshes 0\n            -subdivSurfaces 0\n            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 0\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 768\n            -height 773\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 768\n            -height 773\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 1\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1532\n            -height 1613\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n"
		+ "                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"|persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n"
		+ "                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n"
		+ "                -clipGhosts 1\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 1\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1532\\n    -height 1613\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 1\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1532\\n    -height 1613\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "E8F606E8-41DE-6C37-DC5D-438B8D4E6ED5";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 300 -ast 0 -aet 500 ";
	setAttr ".st" 6;
createNode polySplit -n "polySplit23";
	rename -uid "44118CDF-4A80-8490-6C97-A4B7A4DC5BA0";
	setAttr -s 13 ".e[0:12]"  0.60760099 0.39239901 0.60760099 0.39239901
		 0.39239901 0.60760099 0.39239901 0.39239901 0.60760099 0.60760099 0.39239901 0.60760099
		 0.60760099;
	setAttr -s 13 ".d[0:12]"  -2147483636 -2147483518 -2147483485 -2147483628 -2147483571 -2147483551 
		-2147483625 -2147483473 -2147483512 -2147483633 -2147483541 -2147483581 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "BFEFD148-4073-03F6-D073-179FC022B45C";
	setAttr ".sa" 8;
	setAttr ".sh" 2;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyCube -n "polyCube4";
	rename -uid "A591619A-4737-6D14-D416-AEA6ED87EBF1";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit24";
	rename -uid "C248AD8B-460D-0017-4F78-B195DC750E19";
	setAttr -s 5 ".e[0:4]"  0.93793499 0.062064499 0.062064499 0.93793499
		 0.93793499;
	setAttr -s 5 ".d[0:4]"  -2147483644 -2147483640 -2147483639 -2147483643 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit25";
	rename -uid "63EC44A8-42D3-C19A-9262-538B419E11C6";
	setAttr -s 5 ".e[0:4]"  0.095950797 0.90404898 0.90404898 0.095950797
		 0.095950797;
	setAttr -s 5 ".d[0:4]"  -2147483644 -2147483635 -2147483634 -2147483643 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit26";
	rename -uid "0EE92246-45F5-DF2B-36BD-68A8DBDF137C";
	setAttr -s 9 ".e[0:8]"  0.94288898 0.057110898 0.057110898 0.94288898
		 0.94288898 0.94288898 0.94288898 0.94288898 0.94288898;
	setAttr -s 9 ".d[0:8]"  -2147483648 -2147483621 -2147483629 -2147483647 -2147483646 -2147483631 
		-2147483623 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit27";
	rename -uid "F3B3C5ED-4A9F-8428-6302-CAACF53CAD9C";
	setAttr -s 9 ".e[0:8]"  0.045253199 0.95474702 0.95474702 0.045253199
		 0.045253199 0.045253199 0.045253199 0.045253199 0.045253199;
	setAttr -s 9 ".d[0:8]"  -2147483648 -2147483619 -2147483618 -2147483647 -2147483646 -2147483631 
		-2147483623 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCube -n "polyCube5";
	rename -uid "E239DE4F-4D60-9B13-C351-37AB6F12872E";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit28";
	rename -uid "E7DE46B3-4834-673A-A59A-4FB3355ADFC9";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483647 -2147483646 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit29";
	rename -uid "8BBB0458-4855-A390-3F7D-85ACC2EAFD11";
	setAttr -s 5 ".e[0:4]"  0.1 0.1 0.1 0.1 0.1;
	setAttr -s 5 ".d[0:4]"  -2147483636 -2147483635 -2147483634 -2147483633 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit30";
	rename -uid "92F3FA52-4C37-FC9C-6757-59A2D458B5A8";
	setAttr -s 5 ".e[0:4]"  0.89999998 0.89999998 0.89999998 0.89999998
		 0.89999998;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483647 -2147483646 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "F26B5F5F-4B25-0A77-51A8-0D9520245B54";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[0]" -type "float3" 0 -0.45547774 0 ;
	setAttr ".tk[1]" -type "float3" 0.028901795 -0.45547798 0 ;
	setAttr ".tk[2]" -type "float3" 0 -0.45547774 0 ;
	setAttr ".tk[3]" -type "float3" 0.028901795 -0.45547798 0 ;
	setAttr ".tk[4]" -type "float3" 0 -0.45547774 0 ;
	setAttr ".tk[5]" -type "float3" 0.028901795 -0.45547798 0 ;
	setAttr ".tk[6]" -type "float3" 0 -0.45547774 0 ;
	setAttr ".tk[7]" -type "float3" 0.028901795 -0.45547798 0 ;
	setAttr ".tk[12]" -type "float3" 0.095667653 0 0 ;
	setAttr ".tk[13]" -type "float3" 0.095667653 0 0 ;
	setAttr ".tk[14]" -type "float3" 0.095667653 0 0 ;
	setAttr ".tk[15]" -type "float3" 0.095667653 0 0 ;
	setAttr ".tk[16]" -type "float3" -0.08165323 0 0 ;
	setAttr ".tk[17]" -type "float3" -0.08165323 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.08165323 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.08165323 0 0 ;
createNode polySplit -n "polySplit31";
	rename -uid "9A5B21EE-47E2-7900-1F3E-AFB73399E463";
	setAttr -s 11 ".e[0:10]"  0.964652 0.035348501 0.035348501 0.035348501
		 0.035348501 0.035348501 0.964652 0.964652 0.964652 0.964652 0.964652;
	setAttr -s 11 ".d[0:10]"  -2147483644 -2147483640 -2147483614 -2147483630 -2147483622 -2147483639 
		-2147483643 -2147483624 -2147483632 -2147483616 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit32";
	rename -uid "2E770CA2-4573-1EDF-0FE9-B791E08585B1";
	setAttr -s 13 ".e[0:12]"  0.069219299 0.069219299 0.93078101 0.93078101
		 0.93078101 0.93078101 0.93078101 0.93078101 0.069219299 0.069219299 0.069219299 0.069219299
		 0.069219299;
	setAttr -s 13 ".d[0:12]"  -2147483642 -2147483602 -2147483638 -2147483613 -2147483629 -2147483621 
		-2147483637 -2147483597 -2147483641 -2147483623 -2147483631 -2147483615 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit33";
	rename -uid "0BE35782-415A-0044-DA66-EA902A31330D";
	setAttr -s 13 ".e[0:12]"  0.072347902 0.927652 0.927652 0.927652 0.927652
		 0.927652 0.927652 0.072347902 0.072347902 0.072347902 0.072347902 0.072347902 0.072347902;
	setAttr -s 13 ".d[0:12]"  -2147483644 -2147483579 -2147483611 -2147483610 -2147483609 -2147483608 
		-2147483607 -2147483574 -2147483643 -2147483624 -2147483632 -2147483616 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit34";
	rename -uid "FDB806E7-4A64-06E8-D2A1-C994196C21C8";
	setAttr -s 15 ".e[0:14]"  0.058362201 0.94163799 0.94163799 0.94163799
		 0.94163799 0.94163799 0.94163799 0.94163799 0.058362201 0.058362201 0.058362201 0.058362201
		 0.058362201 0.058362201 0.058362201;
	setAttr -s 15 ".d[0:14]"  -2147483638 -2147483555 -2147483591 -2147483592 -2147483581 -2147483582 
		-2147483583 -2147483584 -2147483597 -2147483550 -2147483637 -2147483621 -2147483629 -2147483613 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "94434827-40B8-C608-4F95-A6A816AFAB37";
	setAttr ".uopa" yes;
	setAttr -s 116 ".tk[0:115]" -type "float3"  3.3306691e-16 7.2164497e-16
		 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 0 0.047902949 0 0 0.047902949
		 0 0 0.047902949 0 0 0.047902949 0 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16
		 7.2164497e-16 -0.34161532 2.220446e-16 -0.047713451 -0.12674125 2.220446e-16 -0.047713451
		 -0.12674125 2.220446e-16 -0.047713451 -0.12674125 2.220446e-16 -0.047713451 -0.12674125
		 2.220446e-16 0.0072354041 -0.010919807 2.220446e-16 0.0072354041 -0.010919807 2.220446e-16
		 0.0072354041 -0.010919807 2.220446e-16 0.0072354041 -0.010919807 0 0.047902979 0
		 0 0.047902964 0 0 0.047902964 0 0 0.047902964 0 0 0.047903024 0 0 0.047903024 0 0
		 0.047903024 0 0 0.047903024 0 0 0.047903053 0 0 0.047903053 0 0 0.047903053 0 0 0.047903053
		 0 3.3306691e-16 7.2164497e-16 -0.34161532 2.220446e-16 -0.047713451 -0.12674125 2.220446e-16
		 0.0072354041 -0.010919807 0 0.047902964 0 0 0.047903024 0 0 0.047903053 0 0 0.047902949
		 0 0 0.047902949 0 0 0.047903053 0 0 0.047903024 0 0 0.047902964 0 2.220446e-16 0.0072354041
		 -0.010919807 2.220446e-16 -0.047713451 -0.12674125 3.3306691e-16 7.2164497e-16 -0.34161532
		 2.220446e-16 -0.047713451 -0.12674125 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16
		 7.2164497e-16 -0.34161532 2.220446e-16 -0.047713451 -0.12674125 2.220446e-16 0.0072354041
		 -0.010919807 0 0.047902957 0 0 0.047903024 0 0 0.047903053 0 0 0.047902949 0 0 0.047902949
		 0 0 0.047903053 0 0 0.047903024 0 0 0.047902964 0 2.220446e-16 0.0072354041 -0.010919807
		 0 0.047902949 0 0 0.047903053 0 0 0.047903024 0 0 0.047902964 0 2.220446e-16 0.0072354041
		 -0.010919807 2.220446e-16 -0.047713451 -0.12674125 3.3306691e-16 7.2164497e-16 -0.34161532
		 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16
		 7.2164497e-16 -0.34161532 2.220446e-16 -0.047713451 -0.12674125 2.220446e-16 0.0072354004
		 -0.010919807 0 0.047902957 0 0 0.047903024 0 0 0.047903039 0 0 0.047902949 0 0 0.047902964
		 0 0 0.047902964 0 3.3306691e-16 7.2164497e-16 -0.34161532 2.220446e-16 -0.047713451
		 -0.12674125 2.220446e-16 0.0072353892 -0.010919807 0 0.047902979 0 0 0.047903024
		 0 0 0.047903053 0 0 0.047902949 0 0 0.047902964 0 0 0.047902964 0 0 0.047902949 0
		 0 0.047903053 0 0 0.047903024 0 0 0.047902964 0 2.220446e-16 0.0072354004 -0.010919807
		 2.220446e-16 -0.047713451 -0.12674125 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16
		 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16
		 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532
		 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16
		 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16
		 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532
		 3.3306691e-16 7.2164497e-16 -0.34161532 3.3306691e-16 7.2164497e-16 -0.34161532 4.4408921e-16
		 -0.0588177 -0.10478631 4.4408921e-16 -0.0588177 -0.10478631 4.4408921e-16 -0.0588177
		 -0.10478631 4.4408921e-16 -0.0588177 -0.10478631 4.4408921e-16 -0.0588177 -0.10478631
		 4.4408921e-16 -0.0588177 -0.10478628 4.4408921e-16 -0.0588177 -0.10478628 4.4408921e-16
		 -0.0588177 -0.10478628 4.4408921e-16 -0.0588177 -0.10478628 4.4408921e-16 -0.0588177
		 -0.10478628 4.4408921e-16 -0.0588177 -0.10478628 4.4408921e-16 -0.0588177 -0.10478631;
createNode polySplit -n "polySplit35";
	rename -uid "A72CE098-4D7D-FD74-724B-258DB3091AAF";
	setAttr -s 13 ".e[0:12]"  0.47152099 0.52847898 0.47152099 0.52847898
		 0.52847898 0.47152099 0.52847898 0.52847898 0.47152099 0.47152099 0.52847898 0.47152099
		 0.47152099;
	setAttr -s 13 ".d[0:12]"  -2147483627 -2147483519 -2147483484 -2147483620 -2147483572 -2147483550 
		-2147483617 -2147483474 -2147483511 -2147483626 -2147483542 -2147483580 -2147483627;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "D8F5229C-4FE6-505A-91D3-CCB0382FCB14";
	setAttr ".uopa" yes;
	setAttr -s 52 ".tk";
	setAttr ".tk[2]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[3]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[4]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[5]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[16]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[17]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[18]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[19]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[20]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[21]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[22]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[23]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[24]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[25]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[26]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[27]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[31]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[32]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[33]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[34]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[35]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[36]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[37]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[38]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[47]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[48]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[49]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[50]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[51]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[52]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[53]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[54]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[56]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[57]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[58]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[59]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[68]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[69]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[70]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[71]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[72]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[73]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[77]" -type "float3" 0.15504152 0 0 ;
	setAttr ".tk[78]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[79]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[80]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[81]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[82]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[83]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[84]" -type "float3" 0.4238866 2.3283064e-10 0 ;
	setAttr ".tk[85]" -type "float3" 0.24791306 2.3283064e-10 0 ;
	setAttr ".tk[86]" -type "float3" 0.15504152 0 0 ;
createNode polySplit -n "polySplit36";
	rename -uid "455AC9DD-48E7-3358-29DD-27AA952B43B5";
	setAttr -s 13 ".e[0:12]"  0.34246099 0.65753901 0.34246099 0.65753901
		 0.65753901 0.34246099 0.65753901 0.65753901 0.34246099 0.34246099 0.65753901 0.34246099
		 0.34246099;
	setAttr -s 13 ".d[0:12]"  -2147483620 -2147483418 -2147483519 -2147483420 -2147483409 -2147483542 
		-2147483411 -2147483412 -2147483474 -2147483617 -2147483415 -2147483572 -2147483620;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit37";
	rename -uid "8F6083A5-4141-3A73-C48B-DD89C4AABF89";
	setAttr -s 13 ".e[0:12]"  0.52546299 0.47453699 0.52546299 0.47453699
		 0.47453699 0.52546299 0.47453699 0.47453699 0.52546299 0.52546299 0.47453699 0.52546299
		 0.52546299;
	setAttr -s 13 ".d[0:12]"  -2147483420 -2147483394 -2147483418 -2147483396 -2147483385 -2147483415 
		-2147483387 -2147483388 -2147483412 -2147483411 -2147483391 -2147483409 -2147483420;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "5BBCAAE7-4589-CD5C-30C7-CF94AA66E261";
	setAttr ".uopa" yes;
	setAttr -s 88 ".tk";
	setAttr ".tk[2]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[3]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[4]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[5]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[16]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[17]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[20]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[21]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[22]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[23]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[24]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[25]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[26]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[27]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[31]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[32]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[33]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[35]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[36]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[37]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[38]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[47]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[48]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[49]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[50]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[51]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[52]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[53]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[54]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[56]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[57]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[58]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[59]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[68]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[69]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[70]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[71]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[72]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[73]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[77]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[78]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[79]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[80]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[81]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[82]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[83]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[84]" -type "float3" 0.31849304 0 0 ;
	setAttr ".tk[85]" -type "float3" 0.17798963 0 0 ;
	setAttr ".tk[86]" -type "float3" -0.071587175 0 0 ;
	setAttr ".tk[116]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[117]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[118]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[119]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[120]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[121]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[122]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[123]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[124]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[125]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[126]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[127]" -type "float3" -0.11881038 0 0 ;
	setAttr ".tk[128]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[129]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[130]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[131]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[132]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[133]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[134]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[135]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[136]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[137]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[138]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[139]" -type "float3" -0.19104677 0 0 ;
	setAttr ".tk[140]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[141]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[142]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[143]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[144]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[145]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[146]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[147]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[148]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[149]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[150]" -type "float3" -0.28884041 0 0 ;
	setAttr ".tk[151]" -type "float3" -0.28884041 0 0 ;
createNode polySplit -n "polySplit38";
	rename -uid "838B9975-48B1-DA72-002B-13BEE4BA32F8";
	setAttr -s 13 ".e[0:12]"  0.49672201 0.50327802 0.49672201 0.50327802
		 0.50327802 0.49672201 0.50327802 0.50327802 0.49672201 0.49672201 0.50327802 0.49672201
		 0.49672201;
	setAttr -s 13 ".d[0:12]"  -2147483627 -2147483419 -2147483484 -2147483417 -2147483416 -2147483550 
		-2147483414 -2147483413 -2147483511 -2147483626 -2147483410 -2147483580 -2147483627;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode objectSet -n "set2";
	rename -uid "C0548DCC-4F4E-CDBF-252B-0FB7B2ABE25E";
	setAttr ".ihi" 0;
createNode objectSet -n "set3";
	rename -uid "05946768-4227-D9DD-A45C-9CBDD9FB9EEC";
	setAttr ".ihi" 0;
createNode objectSet -n "set4";
	rename -uid "112D9534-4699-91FB-F4D8-7A972EBB1716";
	setAttr ".ihi" 0;
	setAttr -s 5 ".dsm";
	setAttr -s 5 ".gn";
createNode groupId -n "groupId4";
	rename -uid "0BF48A52-46B8-17F9-C5C4-9A94F03E11C0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "6C4BA541-4C35-3A78-0017-618A3EE3A94A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[0:7]" "e[16:23]" "e[40:55]";
createNode deleteComponent -n "deleteComponent7";
	rename -uid "2FFFE6C0-4CE1-03C5-0FF3-B590BAF14569";
	setAttr ".dc" -type "componentList" 1 "f[16:31]";
createNode polyDelEdge -n "polyDelEdge20";
	rename -uid "3075E9A3-40BE-DCFD-4813-B8BE737F6978";
	setAttr ".ics" -type "componentList" 1 "e[8:15]";
	setAttr ".cv" yes;
createNode groupId -n "groupId12";
	rename -uid "3C14499A-4984-96BA-FEDF-CF98AD92092D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "8E8FE3F2-4914-D746-06DC-80B0B3E0A5E0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "e[0:7]" "e[16:23]";
createNode polyDelEdge -n "polyDelEdge21";
	rename -uid "03A85AF1-4CE8-E249-0202-90A033674A31";
	setAttr ".ics" -type "componentList" 1 "e[8:15]";
	setAttr ".cv" yes;
createNode groupId -n "groupId13";
	rename -uid "9E637090-44FD-2E40-A89E-FCA521FE5E7E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "BA823E4B-4A7A-EDE7-9FA9-16BA82DECD79";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "e[0:7]" "e[16:23]";
createNode polyDelEdge -n "polyDelEdge22";
	rename -uid "5270F530-419C-A41A-E43B-099F13BD1AA7";
	setAttr ".ics" -type "componentList" 1 "e[8:15]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge23";
	rename -uid "D8F50186-4842-3DE4-BFFB-58827676E83B";
	setAttr ".ics" -type "componentList" 1 "e[8:15]";
	setAttr ".cv" yes;
createNode groupId -n "groupId14";
	rename -uid "9CB4DE2E-496F-2A93-6871-5684412FD05B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "A6C00BA4-473F-651E-3280-44B7D79728F2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "e[0:7]" "e[16:23]";
createNode polyDelEdge -n "polyDelEdge24";
	rename -uid "27AB7D9D-4E4D-4064-C8A7-13A38D268C8B";
	setAttr ".ics" -type "componentList" 1 "e[8:15]";
	setAttr ".cv" yes;
createNode groupId -n "groupId15";
	rename -uid "6DEA90E7-4A98-EE34-6BC7-DAA49DB13044";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "F834B91D-4F73-F9A1-1239-1B9410506E1C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "e[0:7]" "e[16:23]";
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 18 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyCloseBorder1.out" "ChairBase_geoShape.i";
connectAttr "groupId1.id" "ChairBase_geoShape.iog.og[0].gid";
connectAttr "set1.mwc" "ChairBase_geoShape.iog.og[0].gco";
connectAttr "polySplit34.out" "BackBottomArch_geoShape.i";
connectAttr "polySplit27.out" "LeftTopAjoin_geoShape.i";
connectAttr "groupId15.id" "BackLegPillar_geoShape.iog.og[0].gid";
connectAttr "set4.mwc" "BackLegPillar_geoShape.iog.og[0].gco";
connectAttr "polyDelEdge24.out" "BackLegPillar_geoShape.i";
connectAttr "groupId12.id" "RightSideBottomPillar_geoShape.iog.og[0].gid";
connectAttr "set4.mwc" "RightSideBottomPillar_geoShape.iog.og[0].gco";
connectAttr "polyDelEdge20.out" "RightSideBottomPillar_geoShape.i";
connectAttr "groupId13.id" "RightSideTopPillar_geoShape.iog.og[0].gid";
connectAttr "set4.mwc" "RightSideTopPillar_geoShape.iog.og[0].gco";
connectAttr "polyDelEdge21.out" "RightSideTopPillar_geoShape.i";
connectAttr "groupId14.id" "LeftSideBottomPillar_geoShape.iog.og[0].gid";
connectAttr "set4.mwc" "LeftSideBottomPillar_geoShape.iog.og[0].gco";
connectAttr "polyDelEdge23.out" "LeftSideBottomPillar_geoShape.i";
connectAttr "polyDelEdge22.out" "LeftSideTopPillar_geoShape.i";
connectAttr "groupId4.id" "LeftSideTopPillar_geoShape.iog.og[0].gid";
connectAttr "set4.mwc" "LeftSideTopPillar_geoShape.iog.og[0].gco";
connectAttr "polySplit38.out" "ChairLegBL_geoShape.i";
connectAttr "polySplit22.out" "FrontTopAjoin_geoShape.i";
connectAttr "polySplit18.out" "ChairLegFL_geoShape.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "Table_Pieces_polySplit18.out" "Table_Pieces_polyCloseBorder1.ip";
connectAttr "Table_Pieces_polyTweak13.out" "Table_Pieces_polySplit18.ip";
connectAttr "Table_Pieces_polyDelEdge6.out" "Table_Pieces_polyTweak13.ip";
connectAttr "Table_Pieces_polyDelEdge5.out" "Table_Pieces_polyDelEdge6.ip";
connectAttr "Table_Pieces_polyDelEdge4.out" "Table_Pieces_polyDelEdge5.ip";
connectAttr "Table_Pieces_polyBevel1.out" "Table_Pieces_polyDelEdge4.ip";
connectAttr "Table_Pieces_polyTweak12.out" "Table_Pieces_polyBevel1.ip";
connectAttr "ChairBase_geoShape.wm" "Table_Pieces_polyBevel1.mp";
connectAttr "Table_Pieces_polySplit17.out" "Table_Pieces_polyTweak12.ip";
connectAttr "Table_Pieces_polyExtrudeFace2.out" "Table_Pieces_polySplit17.ip";
connectAttr "Table_Pieces_polyExtrudeFace1.out" "Table_Pieces_polyExtrudeFace2.ip"
		;
connectAttr "ChairBase_geoShape.wm" "Table_Pieces_polyExtrudeFace2.mp";
connectAttr "Table_Pieces_polyTweak11.out" "Table_Pieces_polyExtrudeFace1.ip";
connectAttr "ChairBase_geoShape.wm" "Table_Pieces_polyExtrudeFace1.mp";
connectAttr "Table_Pieces_deleteComponent9.og" "Table_Pieces_polyTweak11.ip";
connectAttr "Table_Pieces_polyDelEdge3.out" "Table_Pieces_deleteComponent9.ig";
connectAttr "Table_Pieces_polyDelEdge2.out" "Table_Pieces_polyDelEdge3.ip";
connectAttr "Table_Pieces_polyMirror4.out" "Table_Pieces_polyDelEdge2.ip";
connectAttr "Table_Pieces_polyMirror3.out" "Table_Pieces_polyMirror4.ip";
connectAttr "ChairBase_geoShape.wm" "Table_Pieces_polyMirror4.mp";
connectAttr "Table_Pieces_polyTweak10.out" "Table_Pieces_polyMirror3.ip";
connectAttr "ChairBase_geoShape.wm" "Table_Pieces_polyMirror3.mp";
connectAttr "Table_Pieces_polyDelEdge1.out" "Table_Pieces_polyTweak10.ip";
connectAttr "Table_Pieces_polyTweak9.out" "Table_Pieces_polyDelEdge1.ip";
connectAttr "Table_Pieces_deleteComponent8.og" "Table_Pieces_polyTweak9.ip";
connectAttr "Table_Pieces_deleteComponent7.og" "Table_Pieces_deleteComponent8.ig"
		;
connectAttr "Table_Pieces_polyTweak8.out" "Table_Pieces_deleteComponent7.ig";
connectAttr "Table_Pieces_polyMirror2.out" "Table_Pieces_polyTweak8.ip";
connectAttr "Table_Pieces_deleteComponent6.og" "Table_Pieces_polyMirror2.ip";
connectAttr "ChairBase_geoShape.wm" "Table_Pieces_polyMirror2.mp";
connectAttr "Table_Pieces_polyTweak7.out" "Table_Pieces_deleteComponent6.ig";
connectAttr "Table_Pieces_polyMirror1.out" "Table_Pieces_polyTweak7.ip";
connectAttr "Table_Pieces_deleteComponent5.og" "Table_Pieces_polyMirror1.ip";
connectAttr "ChairBase_geoShape.wm" "Table_Pieces_polyMirror1.mp";
connectAttr "Table_Pieces_polyTweak6.out" "Table_Pieces_deleteComponent5.ig";
connectAttr "Table_Pieces_polySplit16.out" "Table_Pieces_polyTweak6.ip";
connectAttr "Table_Pieces_polyTweak5.out" "Table_Pieces_polySplit16.ip";
connectAttr "Table_Pieces_polySplit15.out" "Table_Pieces_polyTweak5.ip";
connectAttr "Table_Pieces_polySplit14.out" "Table_Pieces_polySplit15.ip";
connectAttr "Table_Pieces_polySplit13.out" "Table_Pieces_polySplit14.ip";
connectAttr "Table_Pieces_polySplit12.out" "Table_Pieces_polySplit13.ip";
connectAttr "Table_Pieces_polyTweak4.out" "Table_Pieces_polySplit12.ip";
connectAttr "Table_Pieces_polySplit11.out" "Table_Pieces_polyTweak4.ip";
connectAttr "Table_Pieces_polySplit10.out" "Table_Pieces_polySplit11.ip";
connectAttr "Table_Pieces_deleteComponent4.og" "Table_Pieces_polySplit10.ip";
connectAttr "Table_Pieces_deleteComponent3.og" "Table_Pieces_deleteComponent4.ig"
		;
connectAttr "Table_Pieces_polyTweak3.out" "Table_Pieces_deleteComponent3.ig";
connectAttr "Table_Pieces_deleteComponent2.og" "Table_Pieces_polyTweak3.ip";
connectAttr "Table_Pieces_deleteComponent1.og" "Table_Pieces_deleteComponent2.ig"
		;
connectAttr "Table_Pieces_polyTweak2.out" "Table_Pieces_deleteComponent1.ig";
connectAttr "Table_Pieces_polySplit9.out" "Table_Pieces_polyTweak2.ip";
connectAttr "Table_Pieces_polySplit8.out" "Table_Pieces_polySplit9.ip";
connectAttr "Table_Pieces_polyTweak1.out" "Table_Pieces_polySplit8.ip";
connectAttr "Table_Pieces_polySplit7.out" "Table_Pieces_polyTweak1.ip";
connectAttr "Table_Pieces_polySplit6.out" "Table_Pieces_polySplit7.ip";
connectAttr "Table_Pieces_polySplit5.out" "Table_Pieces_polySplit6.ip";
connectAttr "Table_Pieces_polySplit4.out" "Table_Pieces_polySplit5.ip";
connectAttr "Table_Pieces_polySplit3.out" "Table_Pieces_polySplit4.ip";
connectAttr "Table_Pieces_polySplit2.out" "Table_Pieces_polySplit3.ip";
connectAttr "Table_Pieces_polySplit1.out" "Table_Pieces_polySplit2.ip";
connectAttr "Table_Pieces_polyCube1.out" "Table_Pieces_polySplit1.ip";
connectAttr "groupId1.msg" "set1.gn" -na;
connectAttr "ChairBase_geoShape.iog.og[0]" "set1.dsm" -na;
connectAttr "Table_Pieces_polyCloseBorder1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyCloseBorder1.ip";
connectAttr "polyCube2.out" "polySplit9.ip";
connectAttr "polySplit9.out" "polySplit10.ip";
connectAttr "polySplit10.out" "polySplit11.ip";
connectAttr "polySplit11.out" "polySplit12.ip";
connectAttr "polySplit12.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "polySplit13.ip";
connectAttr "polySplit13.out" "polySplit14.ip";
connectAttr "polySplit14.out" "polySplit15.ip";
connectAttr "polySplit15.out" "polySplit16.ip";
connectAttr "polySplit16.out" "polySplit17.ip";
connectAttr "polySplit17.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "polySplit18.ip";
connectAttr "polyCube3.out" "polySplit19.ip";
connectAttr "polySplit19.out" "polySplit20.ip";
connectAttr "polySplit20.out" "polySplit21.ip";
connectAttr "polySplit21.out" "polySplit22.ip";
connectAttr "polySurfaceShape1.o" "polySplit23.ip";
connectAttr "polyCube4.out" "polySplit24.ip";
connectAttr "polySplit24.out" "polySplit25.ip";
connectAttr "polySplit25.out" "polySplit26.ip";
connectAttr "polySplit26.out" "polySplit27.ip";
connectAttr "polyCube5.out" "polySplit28.ip";
connectAttr "polySplit28.out" "polySplit29.ip";
connectAttr "polySplit29.out" "polySplit30.ip";
connectAttr "polySplit30.out" "polyTweak5.ip";
connectAttr "polyTweak5.out" "polySplit31.ip";
connectAttr "polySplit31.out" "polySplit32.ip";
connectAttr "polySplit32.out" "polySplit33.ip";
connectAttr "polySplit33.out" "polySplit34.ip";
connectAttr "polySplit23.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "polySplit35.ip";
connectAttr "polySplit35.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "polySplit36.ip";
connectAttr "polySplit36.out" "polySplit37.ip";
connectAttr "polySplit37.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "polySplit38.ip";
connectAttr "groupId4.msg" "set4.gn" -na;
connectAttr "groupId12.msg" "set4.gn" -na;
connectAttr "groupId13.msg" "set4.gn" -na;
connectAttr "groupId14.msg" "set4.gn" -na;
connectAttr "groupId15.msg" "set4.gn" -na;
connectAttr "LeftSideTopPillar_geoShape.iog.og[0]" "set4.dsm" -na;
connectAttr "RightSideBottomPillar_geoShape.iog.og[0]" "set4.dsm" -na;
connectAttr "RightSideTopPillar_geoShape.iog.og[0]" "set4.dsm" -na;
connectAttr "LeftSideBottomPillar_geoShape.iog.og[0]" "set4.dsm" -na;
connectAttr "BackLegPillar_geoShape.iog.og[0]" "set4.dsm" -na;
connectAttr "polyCylinder1.out" "groupParts4.ig";
connectAttr "groupId4.id" "groupParts4.gi";
connectAttr "groupParts4.og" "deleteComponent7.ig";
connectAttr "groupParts5.og" "polyDelEdge20.ip";
connectAttr "polySurfaceShape3.o" "groupParts5.ig";
connectAttr "groupId12.id" "groupParts5.gi";
connectAttr "groupParts6.og" "polyDelEdge21.ip";
connectAttr "polySurfaceShape4.o" "groupParts6.ig";
connectAttr "groupId13.id" "groupParts6.gi";
connectAttr "deleteComponent7.og" "polyDelEdge22.ip";
connectAttr "groupParts7.og" "polyDelEdge23.ip";
connectAttr "polySurfaceShape5.o" "groupParts7.ig";
connectAttr "groupId14.id" "groupParts7.gi";
connectAttr "groupParts8.og" "polyDelEdge24.ip";
connectAttr "polySurfaceShape6.o" "groupParts8.ig";
connectAttr "groupId15.id" "groupParts8.gi";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ChairBase_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "ChairLegFL_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "FrontTopAjoin_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "ChairLegBL_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "LeftSideTopPillar_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "BackLegPillar_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "LeftTopAjoin_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "BackBottomArch_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "BackMiddleArch_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "BackTopArch_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "ChairLegFR_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "FrontBottomAjoin_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "LeftSideBottomPillar_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "RightSideBottomPillar_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "RightSideTopPillar_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "RightTopAjoin_geo1Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
// End of chair001.ma
