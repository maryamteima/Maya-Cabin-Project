This is the basic folders that can be setup to work on the Environment.
The folders in here are for the purposes of demonstration.
The folders should be named after the prop/s that are contained.
This contents of this folder will differ from artist to artist, depending on what they have been assigned to do.

ALL props MUST be in a hierarchy.
All props MUST be modelled and exist in Origin.
Once you want to check a prop into the prop Database you must always follow the following steps:
1. ensure the model is at origin
2. ensure that if this a checked out model you have NOT changed the top node or name.  Do not change anything above the DONOTTOUCH Node in the POS/MOV/ADJ hierarchy.
3. ensure there are no keys 
4. ensure there are no values - Freeze Transforms
5. ensure there is no History