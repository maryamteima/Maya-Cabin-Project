//Maya ASCII 2024 scene
//Name: coffeepotUPDATEUV.ma
//Last modified: Sun, Nov 05, 2023 11:07:32 PM
//Codeset: 1252
requires maya "2024";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiSkyDomeLight"
		 -nodeType "aiStandardSurface" "mtoa" "5.3.3.3";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2024";
fileInfo "version" "2024";
fileInfo "cutIdentifier" "202302170737-4500172811";
fileInfo "osv" "Windows 10 Home v2009 (Build: 19045)";
fileInfo "UUID" "94DAA5C7-45D8-42B8-A233-FA9FF4D208D2";
createNode transform -n "coffeepot_grp";
	rename -uid "A91EF1DA-4795-1B9F-6F9D-889985E837A3";
	setAttr ".s" -type "double3" 1 0.51250530284889273 1 ;
createNode transform -n "POS" -p "coffeepot_grp";
	rename -uid "7186632F-4625-6650-261D-BFA739ADE9CB";
createNode transform -n "MOV" -p "POS";
	rename -uid "45388B0A-4C56-633E-9A51-889783376EE8";
	setAttr ".s" -type "double3" 1 1 0.77748526432274445 ;
createNode transform -n "ADJ" -p "MOV";
	rename -uid "BDAA880E-4133-B70A-9EF2-E9804852388E";
createNode transform -n "coffepot_geo_grp" -p "ADJ";
	rename -uid "3A37437C-4783-BFB9-D335-6A9FC839269B";
createNode transform -n "handle_geo" -p "coffepot_geo_grp";
	rename -uid "9E4F063E-4F9C-B02A-6E27-63ABB7D00A61";
	setAttr ".t" -type "double3" 2.3042659902483558 3.1992545980402447 0 ;
	setAttr ".r" -type "double3" 0 0 -4.8620490099996641 ;
	setAttr ".s" -type "double3" 0.202737205298336 1.3620647764766329 0.29474310751704785 ;
	setAttr ".sh" -type "double3" -0.062601263127512366 0 0 ;
createNode mesh -n "handle_geoShape" -p "handle_geo";
	rename -uid "3859FE4A-4C90-ACEA-4E3D-98ACA4DFD267";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.10030549764633179 0.17023300542496145 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "spoutCircle_geo" -p "coffepot_geo_grp";
	rename -uid "1424840F-411A-5D8B-B314-B3BAA57F3C71";
	setAttr ".t" -type "double3" -1.5385118103096822 1.698015059563559 -0.030107056022297466 ;
	setAttr ".r" -type "double3" -0.73632175831194513 0.15999390589074194 92.520164093570699 ;
	setAttr ".s" -type "double3" 0.72959928101613691 0.029187345794979596 0.48055361546587644 ;
	setAttr ".sh" -type "double3" -0.12268374848212008 0.0026667443505054766 -0.0052486894333964714 ;
createNode mesh -n "spoutCircle_geoShape" -p "spoutCircle_geo";
	rename -uid "DC3E4171-4D60-201E-1748-3CA3110B3C90";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.21220395725686103 0.22280617634532973 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "spout_geo" -p "coffepot_geo_grp";
	rename -uid "33FE9698-42A0-3DC9-6DBE-7DB0F92A5618";
	setAttr ".t" -type "double3" -2.5621445883709391 2.4926445446548446 -0.033653810979646627 ;
	setAttr ".r" -type "double3" 0 0 54.984571777468688 ;
	setAttr ".s" -type "double3" 0.27419921109380302 1.0771613475608384 0.25072778309140464 ;
	setAttr ".sh" -type "double3" 0.68556845593570481 0 0 ;
createNode mesh -n "spout_geoShape" -p "spout_geo";
	rename -uid "B5DFCDEF-4F54-8C81-1F22-2E8B48E1A178";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.55967584252357483 0.16926217079162598 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "topper_geo" -p "coffepot_geo_grp";
	rename -uid "58DA6413-4AA3-5CB3-609C-66A99BC81145";
	setAttr ".t" -type "double3" 0 7.5694758604657766 0.031361028955890831 ;
	setAttr ".s" -type "double3" 0.13406708957084318 0.16561587656482785 0.1724368238510951 ;
createNode mesh -n "topper_geoShape" -p "topper_geo";
	rename -uid "3128F90B-4ED2-2205-DD1A-16955E64DF99";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.46101799607276917 0.50000005960464478 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "lid_geo" -p "coffepot_geo_grp";
	rename -uid "5025BAE4-4F3D-065D-7B73-6D9F3DE4D49B";
	setAttr ".t" -type "double3" 0 7.3670185258935277 0 ;
	setAttr ".s" -type "double3" 0.53420226064025833 0.41629741178738078 0.68708988472674637 ;
createNode mesh -n "lid_geoShape" -p "lid_geo";
	rename -uid "0B0B6379-46AF-20D6-F4CF-E78E2AA37177";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.60185021162033081 0.5583934485912323 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "CoffeepotTopBody_geo" -p "coffepot_geo_grp";
	rename -uid "7B24BBE1-474C-0DD5-0DC8-A99BD385BB79";
	setAttr ".t" -type "double3" 0 6.183349255613904 0 ;
	setAttr ".s" -type "double3" 0.83732202676744971 1.8958665824382135 1.0769619247984439 ;
createNode mesh -n "CoffeepotTopBody_geoShape" -p "CoffeepotTopBody_geo";
	rename -uid "45EDC061-457D-1B3D-534C-268DCD1F3A61";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" -0.36938077211380005 -0.50322949886322021 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape1" -p "CoffeepotTopBody_geo";
	rename -uid "26B6E610-497D-A4E6-83F3-248F1622D2F2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[0:9]" "f[50:79]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[10:19]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:19]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[10:19]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[10:59]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[50:59]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[50:59]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 2 "f[10:49]" "f[80:129]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[50:59]";
	setAttr ".pv" -type "double2" 0.49999994039535522 0.66593748331069946 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 181 ".uvst[0].uvsp[0:180]" -type "float2" 0.56320447 0.11032927
		 0.52414197 0.081948698 0.47585803 0.081948698 0.43679553 0.11032927 0.421875 0.15625
		 0.43679553 0.20217073 0.47585806 0.2305513 0.52414197 0.2305513 0.56320447 0.20217073
		 0.578125 0.15625 0.62640893 0.064408526 0.54828387 0.0076473951 0.45171607 0.00764741
		 0.37359107 0.064408556 0.34375 0.15625 0.37359107 0.24809146 0.4517161 0.3048526
		 0.54828393 0.3048526 0.62640893 0.24809146 0.65625 0.15625 0.375 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.45000002 0.3125 0.47500002 0.3125 0.5 0.3125 0.52499998
		 0.3125 0.54999995 0.3125 0.57499993 0.3125 0.5999999 0.3125 0.62499988 0.3125 0.375
		 0.40625 0.40000001 0.40625 0.42500001 0.40625 0.45000002 0.40625 0.47500002 0.40625
		 0.5 0.40625 0.52499998 0.40625 0.54999995 0.40625 0.57499993 0.40625 0.5999999 0.40625
		 0.62499988 0.40625 0.375 0.5 0.40000001 0.5 0.42500001 0.5 0.45000002 0.5 0.47500002
		 0.5 0.5 0.5 0.52499998 0.5 0.54999995 0.5 0.57499993 0.5 0.5999999 0.5 0.62499988
		 0.5 0.375 0.59375 0.40000001 0.59375 0.42500001 0.59375 0.45000002 0.59375 0.47500002
		 0.59375 0.5 0.59375 0.52499998 0.59375 0.54999995 0.59375 0.57499993 0.59375 0.5999999
		 0.59375 0.62499988 0.59375 0.375 0.6875 0.40000001 0.6875 0.42500001 0.6875 0.45000002
		 0.6875 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875 0.57499993
		 0.6875 0.5999999 0.6875 0.62499988 0.6875 0.5 0.15625 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.52414197 0.081948698 0.56320447
		 0.11032927 0.47585803 0.081948698 0.43679553 0.11032927 0.421875 0.15625 0.43679553
		 0.20217073 0.47585806 0.2305513 0.52414197 0.2305513 0.56320447 0.20217073 0.578125
		 0.15625 0.62499988 0.35000002 0.375 0.35000002 0.5999999 0.35000002 0.57499993 0.35000002
		 0.54999995 0.35000002 0.52499998 0.35000002 0.5 0.35000002 0.47500002 0.35000002
		 0.45000002 0.35000002 0.42500001 0.35000002 0.40000004 0.35000002 0.62499988 0.64999998
		 0.375 0.64999998 0.5999999 0.64999998 0.57499993 0.64999998 0.54999995 0.64999998
		 0.52499998 0.64999998 0.5 0.64999998 0.47500002 0.64999998 0.44999999 0.64999998
		 0.42500001 0.64999998 0.40000001 0.64999998 0.62499988 0.68000001 0.375 0.68000001
		 0.5999999 0.68000001 0.57499993 0.68000001 0.54999995 0.68000001 0.52499998 0.68000001
		 0.5 0.68000001 0.47500002 0.68000001 0.44999999 0.68000001 0.42500001 0.68000001
		 0.40000004 0.68000001 0.62499988 0.65600002 0.375 0.65600002 0.5999999 0.65600002
		 0.57499993 0.65600002 0.54999995 0.65600002 0.52499998 0.65600002 0.5 0.65600002
		 0.47500002 0.65600002 0.44999999 0.65600002 0.42500001 0.65600002 0.40000004 0.65600002
		 0.62499988 0.64437497 0.375 0.64437497 0.5999999 0.64437497 0.57499993 0.64437497
		 0.54999995 0.64437497 0.52499998 0.64437497 0.5 0.64437497 0.47500002 0.64437497
		 0.44999999 0.64437497 0.42500001 0.64437497 0.40000001 0.64437497;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 100 ".pt";
	setAttr ".pt[10]" -type "float3" 0.18086885 0 -0.13140893 ;
	setAttr ".pt[11]" -type "float3" 0.069085762 0 -0.21262415 ;
	setAttr ".pt[12]" -type "float3" -0.069085784 0 -0.21262409 ;
	setAttr ".pt[13]" -type "float3" -0.18086885 0 -0.1314089 ;
	setAttr ".pt[14]" -type "float3" -0.22356625 0 2.665117e-08 ;
	setAttr ".pt[15]" -type "float3" -0.18086877 0 0.13140893 ;
	setAttr ".pt[16]" -type "float3" -0.069085747 0 0.21262415 ;
	setAttr ".pt[17]" -type "float3" 0.069085784 0 0.21262412 ;
	setAttr ".pt[18]" -type "float3" 0.18086879 0 0.13140893 ;
	setAttr ".pt[19]" -type "float3" 0.22356625 0 1.3325585e-08 ;
	setAttr ".pt[20]" -type "float3" 0.18772344 -0.10344335 -0.13638902 ;
	setAttr ".pt[21]" -type "float3" 0.071703985 -0.10344335 -0.22068205 ;
	setAttr ".pt[22]" -type "float3" -0.071703985 -0.10344335 -0.22068202 ;
	setAttr ".pt[23]" -type "float3" -0.18772344 -0.10344335 -0.136389 ;
	setAttr ".pt[24]" -type "float3" -0.23203878 -0.10344335 2.7661175e-08 ;
	setAttr ".pt[25]" -type "float3" -0.18772341 -0.10344335 0.13638902 ;
	setAttr ".pt[26]" -type "float3" -0.071703911 -0.10344335 0.22068205 ;
	setAttr ".pt[27]" -type "float3" 0.071703985 -0.10344335 0.22068205 ;
	setAttr ".pt[28]" -type "float3" 0.1877234 -0.10344335 0.13638902 ;
	setAttr ".pt[29]" -type "float3" 0.23203878 -0.10344335 1.3830586e-08 ;
	setAttr ".pt[30]" -type "float3" 0.14845364 -0.10063063 -0.10785791 ;
	setAttr ".pt[31]" -type "float3" 0.056704253 -0.10063063 -0.17451769 ;
	setAttr ".pt[32]" -type "float3" -0.056704253 -0.10063063 -0.17451766 ;
	setAttr ".pt[33]" -type "float3" -0.14845361 -0.10063063 -0.10785788 ;
	setAttr ".pt[34]" -type "float3" -0.18349878 -0.10063063 1.3844307e-08 ;
	setAttr ".pt[35]" -type "float3" -0.14845359 -0.10063063 0.10785789 ;
	setAttr ".pt[36]" -type "float3" -0.056704186 -0.10063063 0.17451769 ;
	setAttr ".pt[37]" -type "float3" 0.05670426 -0.10063063 0.17451763 ;
	setAttr ".pt[38]" -type "float3" 0.14845361 -0.10063063 0.1078579 ;
	setAttr ".pt[39]" -type "float3" 0.18349878 -0.10063063 2.9069245e-09 ;
	setAttr ".pt[40]" -type "float3" 0.023794793 -0.45202222 -0.017287932 ;
	setAttr ".pt[41]" -type "float3" 0.0090888012 -0.45202222 -0.027972456 ;
	setAttr ".pt[42]" -type "float3" -0.0090888022 -0.45202222 -0.027972452 ;
	setAttr ".pt[43]" -type "float3" -0.023794793 -0.45202222 -0.017287925 ;
	setAttr ".pt[44]" -type "float3" -0.029411983 -0.45202222 2.1816333e-09 ;
	setAttr ".pt[45]" -type "float3" -0.023794793 -0.45202202 0.017287932 ;
	setAttr ".pt[46]" -type "float3" -0.0090887994 -0.45202202 0.027972456 ;
	setAttr ".pt[47]" -type "float3" 0.0090888022 -0.45202202 0.027972452 ;
	setAttr ".pt[48]" -type "float3" 0.023794793 -0.45202222 0.017287925 ;
	setAttr ".pt[49]" -type "float3" 0.029411983 -0.45202222 4.2854292e-10 ;
	setAttr ".pt[50]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[51]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[52]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[53]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[54]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[55]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[56]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[57]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[58]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[59]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[81]" -type "float3" 0.17879441 0 -0.12990178 ;
	setAttr ".pt[82]" -type "float3" 0.22100204 0 1.520908e-09 ;
	setAttr ".pt[83]" -type "float3" 0.17879441 0 0.12990168 ;
	setAttr ".pt[84]" -type "float3" 0.068293378 0 0.21018541 ;
	setAttr ".pt[85]" -type "float3" -0.068293341 0 0.21018544 ;
	setAttr ".pt[86]" -type "float3" -0.17879441 0 0.12990169 ;
	setAttr ".pt[87]" -type "float3" -0.22100204 0 1.4693653e-08 ;
	setAttr ".pt[88]" -type "float3" -0.17879441 0 -0.12990168 ;
	setAttr ".pt[89]" -type "float3" -0.068293378 0 -0.21018544 ;
	setAttr ".pt[90]" -type "float3" 0.06829337 0 -0.21018544 ;
	setAttr ".pt[91]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[92]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[93]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[94]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[95]" -type "float3" 0 -0.59498852 0 ;
	setAttr ".pt[96]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[97]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[98]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[99]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[100]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[101]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[102]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[103]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[104]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[105]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[106]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[107]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[108]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[109]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[110]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[111]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[112]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[113]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[114]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[115]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[116]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[117]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[118]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[119]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[120]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[121]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[122]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[123]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[124]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[125]" -type "float3" 0 -0.59498852 0 ;
	setAttr ".pt[126]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[127]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[128]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[129]" -type "float3" 0 -0.59498841 0 ;
	setAttr ".pt[130]" -type "float3" 0 -0.59498841 0 ;
	setAttr -s 131 ".vt[0:130]"  0.62707394 -1 -0.45559594 0.23952088 -1 -0.73716956
		 -0.23952103 -1 -0.7371695 -0.627074 -1 -0.45559582 -0.77510595 -1 6.2597493e-08 -0.62707394 -1 0.45559591
		 -0.23952089 -1 0.7371695 0.23952097 -1 0.7371695 0.62707388 -1 0.45559585 0.77510583 -1 1.6397587e-08
		 0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524
		 -1.000000119209 -1 5.9604645e-08 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566
		 0.30901703 -1 0.95105654 0.809017 -1 0.58778524 1 -1 0 1.073018789 -0.5 -0.77959377
		 0.40985653 -0.5 -1.26140916 -0.4098568 -0.5 -1.26140904 -1.073018909 -0.5 -0.77959359
		 -1.32632387 -0.5 9.8505467e-08 -1.073018789 -0.5 0.77959371 -0.4098565 -0.5 1.26140904
		 0.40985668 -0.5 1.26140904 1.07301867 -0.5 0.77959359 1.32632375 -0.5 1.9450409e-08
		 1.10187101 0 -0.80055618 0.42087713 0 -1.29532695 -0.42087734 0 -1.29532683 -1.10187113 0 -0.80055594
		 -1.36198759 0 1.0275691e-07 -1.10187101 0 0.80055612 -0.42087713 0 1.29532695 0.42087722 0 1.29532671
		 1.10187089 0 0.800556 1.36198735 0 2.1576131e-08 1.070765615 0.5 -0.77795708 0.40899602 0.5 -1.25876069
		 -0.40899622 0.5 -1.25876057 -1.070765734 0.5 -0.77795684 -1.32353914 0.5 9.8173501e-08
		 -1.070765734 0.5 0.77795702 -0.40899605 0.5 1.25876069 0.40899611 0.5 1.25876057
		 1.070765615 0.5 0.77795684 1.32353902 0.5 1.9284428e-08 0.80901706 1 -0.58778542
		 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-08
		 -0.80901706 1 0.58778536 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524
		 1 1 0 0.62707394 -1 -0.45559594 0.23952088 -1 -0.73716956 -0.23952103 -1 -0.7371695
		 -0.627074 -1 -0.45559582 -0.77510595 -1 6.2597493e-08 -0.62707394 -1 0.45559591 -0.23952089 -1 0.7371695
		 0.23952097 -1 0.7371695 0.62707388 -1 0.45559585 0.77510583 -1 1.6397587e-08 0.62707394 -1.10189629 -0.45559594
		 0.23952088 -1.10189629 -0.73716956 0 -1.10189629 -2.1498515e-10 -0.23952103 -1.10189629 -0.7371695
		 -0.627074 -1.10189629 -0.45559582 -0.77510595 -1.10189629 6.2597493e-08 -0.62707394 -1.10189629 0.45559591
		 -0.23952089 -1.10189629 0.7371695 0.23952097 -1.10189629 0.7371695 0.62707388 -1.10189629 0.45559585
		 0.77510583 -1.10189629 1.4247737e-08 1.042666435 -0.80000001 -0.75754166 1.28880632 -0.80000001 8.8694057e-09
		 1.042666316 -0.80000001 0.75754142 0.39826313 -0.80000001 1.2257278 -0.39826298 -0.80000001 1.22572792
		 -1.042666435 -0.80000001 0.75754154 -1.28880644 -0.80000001 8.5688256e-08 -1.042666554 -0.80000001 -0.75754142
		 -0.39826325 -0.80000001 -1.22572792 0.39826301 -0.80000001 -1.22572792 0.80096322 0.80000001 -0.58193398
		 0.99004495 0.80000001 -5.9336824e-10 0.80096316 0.80000001 0.5819338 0.30594075 0.80000001 0.94158876
		 -0.30594069 0.80000001 0.94158882 -0.80096328 0.80000001 0.58193398 -0.99004507 0.80000001 5.8417911e-08
		 -0.8009634 0.80000001 -0.5819338 -0.30594087 0.80000001 -0.94158882 0.30594066 0.80000001 -0.94158888
		 0.80740631 0.96000004 -0.58661515 0.99800897 0.96000004 -1.1867364e-10 0.80740625 0.96000004 0.58661497
		 0.30840176 0.96000004 0.94916296 -0.30840173 0.96000004 0.94916308 -0.80740631 0.96000004 0.58661509
		 -0.99800915 0.96000004 5.9367299e-08 -0.80740643 0.96000004 -0.58661497 -0.30840188 0.96000004 -0.94916308
		 0.30840167 0.96000004 -0.94916314 0.80225188 0.83200008 -0.58287019 0.99163783 0.83200008 -4.9842935e-10
		 0.80225182 0.83200008 0.58287007 0.30643296 0.83200008 0.94310367 -0.3064329 0.83200008 0.94310367
		 -0.80225188 0.83200008 0.58287019 -0.99163789 0.83200008 5.8607789e-08 -0.80225205 0.83200008 -0.58287007
		 -0.30643308 0.83200008 -0.94310367 0.30643287 0.83200008 -0.94310373 0.82794344 0.76999998 -0.60153633
		 1.023394346 0.76999998 1.3944117e-09 0.82794338 0.76999998 0.6015361 0.31624627 0.76999998 0.97330594
		 -0.31624621 0.76999998 0.97330606 -0.8279435 0.76999998 0.60153633 -1.023394465 0.76999998 6.2393468e-08
		 -0.82794362 0.76999998 -0.6015361 -0.31624642 0.76999998 -0.973306 0.31624618 0.76999998 -0.97330606;
	setAttr -s 260 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 10 0
		 20 21 1 21 22 1 22 23 1 23 24 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1 29 20 1 30 31 1
		 31 32 1 32 33 1 33 34 1 34 35 1 35 36 1 36 37 1 37 38 1 38 39 1 39 30 1 40 41 1 41 42 1
		 42 43 1 43 44 1 44 45 1 45 46 1 46 47 1 47 48 1 48 49 1 49 40 1 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 50 0 0 10 1 1 11 1 2 12 1 3 13 1
		 4 14 1 5 15 1 6 16 1 7 17 1 8 18 1 9 19 1 10 81 0 11 90 0 12 89 0 13 88 0 14 87 0
		 15 86 0 16 85 0 17 84 0 18 83 0 19 82 0 20 30 0 21 31 0 22 32 0 23 33 0 24 34 0 25 35 0
		 26 36 0 27 37 0 28 38 0 29 39 0 30 40 0 31 41 0 32 42 0 33 43 0 34 44 0 35 45 0 36 46 0
		 37 47 0 38 48 0 39 49 0 40 121 0 41 130 0 42 129 0 43 128 0 44 127 0 45 126 0 46 125 0
		 47 124 0 48 123 0 49 122 0 0 60 0 1 61 0 60 61 0 2 62 0 61 62 0 3 63 0 62 63 0 4 64 0
		 63 64 0 5 65 0 64 65 0 6 66 0 65 66 0 7 67 0 66 67 0 8 68 0 67 68 0 9 69 0 68 69 0
		 69 60 0 0 70 0 1 71 0 70 71 0 72 70 1 72 71 1 2 73 0 71 73 0 72 73 1 3 74 0 73 74 0
		 72 74 1 4 75 0 74 75 0 72 75 1 5 76 0 75 76 0 72 76 1 6 77 0 76 77 0 72 77 1 7 78 0
		 77 78 0 72 78 1 8 79 0 78 79 0 72 79 1 9 80 0 79 80 0 72 80 1 80 70 0 81 20 0 82 29 0
		 83 28 0 84 27 0 85 26 0 86 25 0;
	setAttr ".ed[166:259]" 87 24 0 88 23 0 89 22 0 90 21 0 81 82 1 82 83 1 83 84 1
		 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 90 1 90 81 1 91 111 0 92 112 0 93 113 0
		 94 114 0 95 115 0 96 116 0 97 117 0 98 118 0 99 119 0 100 120 0 91 92 1 92 93 1 93 94 1
		 94 95 1 95 96 1 96 97 1 97 98 1 98 99 1 99 100 1 100 91 1 101 50 0 102 59 0 103 58 0
		 104 57 0 105 56 0 106 55 0 107 54 0 108 53 0 109 52 0 110 51 0 101 102 1 102 103 1
		 103 104 1 104 105 1 105 106 1 106 107 1 107 108 1 108 109 1 109 110 1 110 101 1 111 101 0
		 112 102 0 113 103 0 114 104 0 115 105 0 116 106 0 117 107 0 118 108 0 119 109 0 120 110 0
		 111 112 1 112 113 1 113 114 1 114 115 1 115 116 1 116 117 1 117 118 1 118 119 1 119 120 1
		 120 111 1 121 91 0 122 92 0 123 93 0 124 94 0 125 95 0 126 96 0 127 97 0 128 98 0
		 129 99 0 130 100 0 121 122 1 122 123 1 123 124 1 124 125 1 125 126 1 126 127 1 127 128 1
		 128 129 1 129 130 1 130 121 1;
	setAttr -s 130 -ch 510 ".fc[0:129]" -type "polyFaces" 
		f 4 0 61 -11 -61
		mu 0 4 0 1 11 10
		f 4 1 62 -12 -62
		mu 0 4 1 2 12 11
		f 4 2 63 -13 -63
		mu 0 4 2 3 13 12
		f 4 3 64 -14 -64
		mu 0 4 3 4 14 13
		f 4 4 65 -15 -65
		mu 0 4 4 5 15 14
		f 4 5 66 -16 -66
		mu 0 4 5 6 16 15
		f 4 6 67 -17 -67
		mu 0 4 6 7 17 16
		f 4 7 68 -18 -68
		mu 0 4 7 8 18 17
		f 4 8 69 -19 -69
		mu 0 4 8 9 19 18
		f 4 9 60 -20 -70
		mu 0 4 9 0 10 19
		f 4 10 71 179 -71
		mu 0 4 20 21 136 127
		f 4 11 72 178 -72
		mu 0 4 21 22 135 136
		f 4 12 73 177 -73
		mu 0 4 22 23 134 135
		f 4 13 74 176 -74
		mu 0 4 23 24 133 134
		f 4 14 75 175 -75
		mu 0 4 24 25 132 133
		f 4 15 76 174 -76
		mu 0 4 25 26 131 132
		f 4 16 77 173 -77
		mu 0 4 26 27 130 131
		f 4 17 78 172 -78
		mu 0 4 27 28 129 130
		f 4 18 79 171 -79
		mu 0 4 28 29 128 129
		f 4 19 70 170 -80
		mu 0 4 29 30 126 128
		f 4 20 81 -31 -81
		mu 0 4 31 32 43 42
		f 4 21 82 -32 -82
		mu 0 4 32 33 44 43
		f 4 22 83 -33 -83
		mu 0 4 33 34 45 44
		f 4 23 84 -34 -84
		mu 0 4 34 35 46 45
		f 4 24 85 -35 -85
		mu 0 4 35 36 47 46
		f 4 25 86 -36 -86
		mu 0 4 36 37 48 47
		f 4 26 87 -37 -87
		mu 0 4 37 38 49 48
		f 4 27 88 -38 -88
		mu 0 4 38 39 50 49
		f 4 28 89 -39 -89
		mu 0 4 39 40 51 50
		f 4 29 80 -40 -90
		mu 0 4 40 41 52 51
		f 4 30 91 -41 -91
		mu 0 4 42 43 54 53
		f 4 31 92 -42 -92
		mu 0 4 43 44 55 54
		f 4 32 93 -43 -93
		mu 0 4 44 45 56 55
		f 4 33 94 -44 -94
		mu 0 4 45 46 57 56
		f 4 34 95 -45 -95
		mu 0 4 46 47 58 57
		f 4 35 96 -46 -96
		mu 0 4 47 48 59 58
		f 4 36 97 -47 -97
		mu 0 4 48 49 60 59
		f 4 37 98 -48 -98
		mu 0 4 49 50 61 60
		f 4 38 99 -49 -99
		mu 0 4 50 51 62 61
		f 4 39 90 -50 -100
		mu 0 4 51 52 63 62
		f 4 40 101 259 -101
		mu 0 4 53 54 180 171
		f 4 41 102 258 -102
		mu 0 4 54 55 179 180
		f 4 42 103 257 -103
		mu 0 4 55 56 178 179
		f 4 43 104 256 -104
		mu 0 4 56 57 177 178
		f 4 44 105 255 -105
		mu 0 4 57 58 176 177
		f 4 45 106 254 -106
		mu 0 4 58 59 175 176
		f 4 46 107 253 -107
		mu 0 4 59 60 174 175
		f 4 47 108 252 -108
		mu 0 4 60 61 173 174
		f 4 48 109 251 -109
		mu 0 4 61 62 172 173
		f 4 49 100 250 -110
		mu 0 4 62 63 170 172
		f 3 -133 -134 134
		mu 0 3 116 117 75
		f 3 -137 -135 137
		mu 0 3 118 116 75
		f 3 -140 -138 140
		mu 0 3 119 118 75
		f 3 -143 -141 143
		mu 0 3 120 119 75
		f 3 -146 -144 146
		mu 0 3 121 120 75
		f 3 -149 -147 149
		mu 0 3 122 121 75
		f 3 -152 -150 152
		mu 0 3 123 122 75
		f 3 -155 -153 155
		mu 0 3 124 123 75
		f 3 -158 -156 158
		mu 0 3 125 124 75
		f 3 -160 -159 133
		mu 0 3 117 125 75
		f 4 0 111 -113 -111
		mu 0 4 76 77 78 79
		f 4 1 113 -115 -112
		mu 0 4 80 81 82 83
		f 4 2 115 -117 -114
		mu 0 4 84 85 86 87
		f 4 3 117 -119 -116
		mu 0 4 88 89 90 91
		f 4 4 119 -121 -118
		mu 0 4 92 93 94 95
		f 4 5 121 -123 -120
		mu 0 4 96 97 98 99
		f 4 6 123 -125 -122
		mu 0 4 100 101 102 103
		f 4 7 125 -127 -124
		mu 0 4 104 105 106 107
		f 4 8 127 -129 -126
		mu 0 4 108 109 110 111
		f 4 9 110 -130 -128
		mu 0 4 112 113 114 115
		f 4 -1 130 132 -132
		mu 0 4 1 0 117 116
		f 4 -2 131 136 -136
		mu 0 4 2 1 116 118
		f 4 -3 135 139 -139
		mu 0 4 3 2 118 119
		f 4 -4 138 142 -142
		mu 0 4 4 3 119 120
		f 4 -5 141 145 -145
		mu 0 4 5 4 120 121
		f 4 -6 144 148 -148
		mu 0 4 6 5 121 122
		f 4 -7 147 151 -151
		mu 0 4 7 6 122 123
		f 4 -8 150 154 -154
		mu 0 4 8 7 123 124
		f 4 -9 153 157 -157
		mu 0 4 9 8 124 125
		f 4 -10 156 159 -131
		mu 0 4 0 9 125 117
		f 4 -171 160 -30 -162
		mu 0 4 128 126 41 40
		f 4 -172 161 -29 -163
		mu 0 4 129 128 40 39
		f 4 -173 162 -28 -164
		mu 0 4 130 129 39 38
		f 4 -174 163 -27 -165
		mu 0 4 131 130 38 37
		f 4 -175 164 -26 -166
		mu 0 4 132 131 37 36
		f 4 -176 165 -25 -167
		mu 0 4 133 132 36 35
		f 4 -177 166 -24 -168
		mu 0 4 134 133 35 34
		f 4 -178 167 -23 -169
		mu 0 4 135 134 34 33
		f 4 -179 168 -22 -170
		mu 0 4 136 135 33 32
		f 4 -180 169 -21 -161
		mu 0 4 127 136 32 31
		f 4 -191 180 230 -182
		mu 0 4 139 137 159 161
		f 4 -192 181 231 -183
		mu 0 4 140 139 161 162
		f 4 -193 182 232 -184
		mu 0 4 141 140 162 163
		f 4 -194 183 233 -185
		mu 0 4 142 141 163 164
		f 4 -195 184 234 -186
		mu 0 4 143 142 164 165
		f 4 -196 185 235 -187
		mu 0 4 144 143 165 166
		f 4 -197 186 236 -188
		mu 0 4 145 144 166 167
		f 4 -198 187 237 -189
		mu 0 4 146 145 167 168
		f 4 -199 188 238 -190
		mu 0 4 147 146 168 169
		f 4 -200 189 239 -181
		mu 0 4 138 147 169 160
		f 4 -211 200 -60 -202
		mu 0 4 150 148 74 73
		f 4 -212 201 -59 -203
		mu 0 4 151 150 73 72
		f 4 -213 202 -58 -204
		mu 0 4 152 151 72 71
		f 4 -214 203 -57 -205
		mu 0 4 153 152 71 70
		f 4 -215 204 -56 -206
		mu 0 4 154 153 70 69
		f 4 -216 205 -55 -207
		mu 0 4 155 154 69 68
		f 4 -217 206 -54 -208
		mu 0 4 156 155 68 67
		f 4 -218 207 -53 -209
		mu 0 4 157 156 67 66
		f 4 -219 208 -52 -210
		mu 0 4 158 157 66 65
		f 4 -220 209 -51 -201
		mu 0 4 149 158 65 64
		f 4 -231 220 210 -222
		mu 0 4 161 159 148 150
		f 4 -232 221 211 -223
		mu 0 4 162 161 150 151
		f 4 -233 222 212 -224
		mu 0 4 163 162 151 152
		f 4 -234 223 213 -225
		mu 0 4 164 163 152 153
		f 4 -235 224 214 -226
		mu 0 4 165 164 153 154
		f 4 -236 225 215 -227
		mu 0 4 166 165 154 155
		f 4 -237 226 216 -228
		mu 0 4 167 166 155 156
		f 4 -238 227 217 -229
		mu 0 4 168 167 156 157
		f 4 -239 228 218 -230
		mu 0 4 169 168 157 158
		f 4 -240 229 219 -221
		mu 0 4 160 169 158 149
		f 4 -251 240 190 -242
		mu 0 4 172 170 137 139
		f 4 -252 241 191 -243
		mu 0 4 173 172 139 140
		f 4 -253 242 192 -244
		mu 0 4 174 173 140 141
		f 4 -254 243 193 -245
		mu 0 4 175 174 141 142
		f 4 -255 244 194 -246
		mu 0 4 176 175 142 143
		f 4 -256 245 195 -247
		mu 0 4 177 176 143 144
		f 4 -257 246 196 -248
		mu 0 4 178 177 144 145
		f 4 -258 247 197 -249
		mu 0 4 179 178 145 146
		f 4 -259 248 198 -250
		mu 0 4 180 179 146 147
		f 4 -260 249 199 -241
		mu 0 4 171 180 147 138;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "CoffeepotBottomBody_geo" -p "coffepot_geo_grp";
	rename -uid "C4244BB4-46D1-784E-4F00-7D966DE06941";
	setAttr ".t" -type "double3" 0 2.3118367088792411 0 ;
	setAttr ".s" -type "double3" 1.2405976456916192 2.131729632608419 1.5956542234562929 ;
createNode mesh -n "CoffeepotBottomBody_geoShape" -p "CoffeepotBottomBody_geo";
	rename -uid "E2FDE387-4EE4-3677-4B75-E8AC6C4B9204";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64844930171966553 0.40816396474838257 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[135:138]" -type "float3"  -7.4505806e-09 -3.7252903e-09 
		0 7.4505806e-09 -7.4505806e-09 0 -3.7252903e-09 7.4505806e-09 0 7.4505806e-09 3.7252903e-09 
		0;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "coffeepot_geo" -p "coffepot_geo_grp";
	rename -uid "26EACDB3-4C95-ED0E-BD21-0F8D7153B89E";
	setAttr ".v" no;
	setAttr ".s" -type "double3" 0.19761545286781518 0.32471248496761407 0.19761545286781532 ;
	setAttr ".rp" -type "double3" 0 7.9025398787467846e-13 0 ;
	setAttr ".sp" -type "double3" 0 7.9025398787467825e-13 0 ;
	setAttr ".spt" -type "double3" 0 2.0194839173657911e-28 0 ;
createNode mesh -n "coffeepot_geoShape" -p "coffeepot_geo";
	rename -uid "5D9AAD65-4738-1015-36A2-9FBDBB18F43D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10.734674 0.5 14.788734 
		10.734674 0.5 14.788734 -10.734674 38.869171 14.788734 10.734674 38.869171 14.788734 
		-10.734674 38.869171 -14.788734 10.734674 38.869171 -14.788734 -10.734674 0.5 -14.788734 
		10.734674 0.5 -14.788734;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -s -n "persp";
	rename -uid "AA098201-419D-CB18-061A-0182D0C6B3CC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 6.0456603083836011 4.37146016259166 10.876567210705668 ;
	setAttr ".r" -type "double3" -376.53835267404907 3988.9999999991628 9.09125033278851e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "B1B32A0F-4027-0719-DB6F-9AA36F4B2D7E";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 13.104592768861318;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -2.2199605487694436 1.7275941009819848 -0.21862109918049238 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "FE8389F2-47D0-7050-B93B-7895D5E6DE64";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "374EAC53-48DA-9612-A49F-2FBEFE077898";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "D8EB4FE4-476D-407E-C8B9-349A42F85B11";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.7559235301821579 1.5947392063815709 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "885E4162-4BD2-B166-F8A0-7E9B48C77141";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 3.4880938426726189;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "D0B9DA54-481E-6652-AB3C-3A9930568A94";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "F877BC4B-4105-D664-2D52-C7A905F232ED";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "bottom";
	rename -uid "869CB6E8-406C-1EB6-466E-FEB01533B592";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 -1000.1 0 ;
	setAttr ".r" -type "double3" 90 0 0 ;
createNode camera -n "bottomShape" -p "bottom";
	rename -uid "3EC04F63-48C7-5780-845D-E6BA0803EB15";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 6.6776692321321516;
	setAttr ".imn" -type "string" "bottom1";
	setAttr ".den" -type "string" "bottom1_depth";
	setAttr ".man" -type "string" "bottom1_mask";
	setAttr ".hc" -type "string" "viewSet -bo %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "back";
	rename -uid "8AE855AC-4B6C-EFD5-EB30-25BADEB3BEF6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.058355704842013134 3.0813172501704784 -1000.1 ;
	setAttr ".r" -type "double3" 0 180 0 ;
createNode camera -n "backShape" -p "back";
	rename -uid "53B118AC-4DA8-8749-89CD-D9BB5D0E8B47";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 16.582315604741812;
	setAttr ".imn" -type "string" "back1";
	setAttr ".den" -type "string" "back1_depth";
	setAttr ".man" -type "string" "back1_mask";
	setAttr ".hc" -type "string" "viewSet -b %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "aiSkyDomeLight1";
	rename -uid "56F5B792-4444-0F17-D10C-8CA325DB3442";
createNode aiSkyDomeLight -n "aiSkyDomeLightShape1" -p "aiSkyDomeLight1";
	rename -uid "51092397-477B-F85D-264B-539B188FF0D8";
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr -k off ".v";
	setAttr ".csh" no;
	setAttr ".rcsh" no;
	setAttr ".aal" -type "attributeAlias" {"exposure","aiExposure"} ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "FDF9A453-4A71-9354-17CA-AC8C6F944A7C";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "B8D48E84-4E14-7D9D-0603-EE96C448AAAC";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "2F11F369-4612-4C8A-53C2-C7B9C2C8876A";
createNode displayLayerManager -n "layerManager";
	rename -uid "566FF32D-472D-7784-27B8-9BA5E96D3767";
createNode displayLayer -n "defaultLayer";
	rename -uid "3A2894C1-497A-A17B-96DA-C88C72E760C7";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "F0E59F41-415A-2F9B-79F0-F3B8C14210C4";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "5CD33D61-4A36-4A56-A423-13ABBAF6DEA8";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "5C25DCBB-44AC-1559-8497-7EB2F2D565E7";
	setAttr ".version" -type "string" "5.2.1.1";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "5FE6DD64-4E92-2874-0009-5DAD55CBBB48";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "D3242230-41D0-B204-5875-4FAA53AB0BB9";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "91DC100E-4764-D898-5DF8-D58DE421CF33";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode polyCylinder -n "polyCylinder1";
	rename -uid "7AA07647-47B3-77C8-16AC-7AA78CCE4C13";
	setAttr ".sa" 10;
	setAttr ".sh" 4;
	setAttr ".sc" 2;
	setAttr ".cuv" 3;
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "C8039573-4F6F-9B61-0B84-39A4E51AB9BD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:9]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.1848285726213428 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.9604645e-08 0.18482858 -2.9802322e-08 ;
	setAttr ".rs" 47262;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.77510595321655273 0.18482857262134278 -0.73716956377029419 ;
	setAttr ".cbx" -type "double3" 0.77510583400726318 0.18482857262134278 0.73716950416564941 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "739BB4A9-4645-D01D-D427-5C82385843B5";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[0:9]" -type "float3"  0.2225654 0 -0.16170323 0.085012414
		 0 -0.2616412 -0.085012443 0 -0.2616412 -0.2225654 0 -0.1617032 -0.27510586 0 3.2795175e-08
		 -0.2225654 0 0.16170323 -0.085012406 0 0.2616412 0.085012443 0 0.2616412 0.2225654
		 0 0.16170321 0.27510586 0 1.6397587e-08;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "84D0CD7D-4642-3232-EC3B-9BBE2D626223";
	setAttr ".ics" -type "componentList" 1 "f[60:69]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.1848285726213428 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.9604645e-08 0.18482858 -2.9802322e-08 ;
	setAttr ".rs" 65112;
	setAttr ".lt" -type "double3" 0 0 0.10189631795886467 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.77510595321655273 0.18482857262134278 -0.73716956377029419 ;
	setAttr ".cbx" -type "double3" 0.77510583400726318 0.18482857262134278 0.73716950416564941 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "58053E92-4570-6D25-BF93-01A9CA9FC449";
	setAttr ".uopa" yes;
	setAttr -s 30 ".tk[20:49]" -type "float3"  0.2640017 0 -0.19180836 0.10083959
		 0 -0.31035244 -0.10083964 0 -0.31035244 -0.2640017 0 -0.19180833 -0.32632378 0 3.8900819e-08
		 -0.2640017 0 0.19180836 -0.10083953 0 0.31035244 0.10083964 0 0.31035244 0.2640017
		 0 0.19180836 0.32632378 0 1.9450409e-08 0.29285395 0 -0.21277077 0.11186019 0 -0.34427029
		 -0.1118602 0 -0.34427023 -0.29285395 0 -0.21277072 -0.36198741 0 4.3152262e-08 -0.29285392
		 0 0.21277077 -0.11186016 0 0.34427029 0.1118602 0 0.34427023 0.29285392 0 0.21277076
		 0.36198741 0 2.1576131e-08 -0.020134538 0 0.014628546 -0.0076906979 0 0.023669545
		 0.0076907012 0 0.02366956 0.020134538 0 0.014628556 0.024887659 0 -2.9668406e-09
		 0.020134518 0 -0.014628556 0.0076906919 0 -0.023669545 -0.0076907012 0 -0.023669515
		 -0.020134531 0 -0.01462858 -0.024887659 0 -1.4834207e-09;
createNode polySplit -n "polySplit1";
	rename -uid "951CE643-4D59-6A6A-42A9-E4B07993B9EF";
	setAttr -s 11 ".e[0:10]"  0.40000001 0.40000001 0.40000001 0.40000001
		 0.40000001 0.40000001 0.40000001 0.40000001 0.40000001 0.40000001 0.40000001;
	setAttr -s 11 ".d[0:10]"  -2147483568 -2147483559 -2147483560 -2147483561 -2147483562 -2147483563 
		-2147483564 -2147483565 -2147483566 -2147483567 -2147483568;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "BC13ECFC-46E4-6E31-C30B-BABD8863B566";
	setAttr -s 11 ".e[0:10]"  0.60000002 0.60000002 0.60000002 0.60000002
		 0.60000002 0.60000002 0.60000002 0.60000002 0.60000002 0.60000002 0.60000002;
	setAttr -s 11 ".d[0:10]"  -2147483538 -2147483529 -2147483530 -2147483531 -2147483532 -2147483533 
		-2147483534 -2147483535 -2147483536 -2147483537 -2147483538;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "A4E8904E-4EDC-3026-28D5-E9B7F2ABF13C";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[40]" -type "float3" 0.28188312 0 -0.20480017 ;
	setAttr ".tk[41]" -type "float3" 0.10766977 0 -0.33137357 ;
	setAttr ".tk[42]" -type "float3" -0.10766979 0 -0.33137348 ;
	setAttr ".tk[43]" -type "float3" -0.28188312 0 -0.2048001 ;
	setAttr ".tk[44]" -type "float3" -0.34842673 0 4.1535699e-08 ;
	setAttr ".tk[45]" -type "float3" -0.28188312 0 0.20480017 ;
	setAttr ".tk[46]" -type "float3" -0.10766976 0 0.33137357 ;
	setAttr ".tk[47]" -type "float3" 0.10766979 0 0.33137357 ;
	setAttr ".tk[48]" -type "float3" 0.28188312 0 0.20480016 ;
	setAttr ".tk[49]" -type "float3" 0.3484267 0 2.076785e-08 ;
	setAttr ".tk[92]" -type "float3" 0.12804869 0 -0.093032859 ;
	setAttr ".tk[93]" -type "float3" 0.15827684 0 1.0892418e-09 ;
	setAttr ".tk[94]" -type "float3" 0.12804869 0 0.093032829 ;
	setAttr ".tk[95]" -type "float3" 0.048910242 0 0.15053025 ;
	setAttr ".tk[96]" -type "float3" -0.048910208 0 0.15053025 ;
	setAttr ".tk[97]" -type "float3" -0.12804869 0 0.093032837 ;
	setAttr ".tk[98]" -type "float3" -0.15827684 0 1.052328e-08 ;
	setAttr ".tk[99]" -type "float3" -0.12804869 0 -0.093032829 ;
	setAttr ".tk[100]" -type "float3" -0.048910242 0 -0.15053025 ;
	setAttr ".tk[101]" -type "float3" 0.048910223 0 -0.15053025 ;
createNode polySplit -n "polySplit3";
	rename -uid "3410BF81-47F4-D74F-439D-D49F2F916A54";
	setAttr -s 11 ".e[0:10]"  0.80000001 0.80000001 0.80000001 0.80000001
		 0.80000001 0.80000001 0.80000001 0.80000001 0.80000001 0.80000001 0.80000001;
	setAttr -s 11 ".d[0:10]"  -2147483438 -2147483437 -2147483436 -2147483435 -2147483434 -2147483433 
		-2147483432 -2147483431 -2147483430 -2147483429 -2147483438;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "0322BF02-41C6-370B-DE72-068B87394D4C";
	setAttr -s 11 ".e[0:10]"  0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2;
	setAttr -s 11 ".d[0:10]"  -2147483438 -2147483437 -2147483436 -2147483435 -2147483434 -2147483433 
		-2147483432 -2147483431 -2147483430 -2147483429 -2147483438;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "F9B6AEC0-4D30-22F8-221C-688C0C277154";
	setAttr -s 11 ".e[0:10]"  0.89999998 0.89999998 0.89999998 0.89999998
		 0.89999998 0.89999998 0.89999998 0.89999998 0.89999998 0.89999998 0.89999998;
	setAttr -s 11 ".d[0:10]"  -2147483538 -2147483529 -2147483530 -2147483531 -2147483532 -2147483533 
		-2147483534 -2147483535 -2147483536 -2147483537 -2147483538;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "A2EB0537-4B68-103C-CBFF-0C91C80B3862";
	setAttr ".dc" -type "componentList" 2 "f[50:59]" "f[70:79]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "2AD58A89-4630-7B0F-8F1B-928487956EF0";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n"
		+ "            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n"
		+ "            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n"
		+ "            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"all\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n"
		+ "            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1030\n            -height 806\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n"
		+ "            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n"
		+ "            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n"
		+ "                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n"
		+ "                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n"
		+ "                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n"
		+ "                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n"
		+ "                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n"
		+ "                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -excludeObjectPreset \\\"All\\\" \\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1030\\n    -height 806\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -excludeObjectPreset \\\"All\\\" \\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1030\\n    -height 806\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "BC773FAD-42E7-A148-C498-84AFEFB934FA";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 300 -ast 0 -aet 500 ";
	setAttr ".st" 6;
createNode polyCylinder -n "polyCylinder2";
	rename -uid "74266A4D-478E-89F2-BAAA-0A8970F8F5F0";
	setAttr ".sa" 10;
	setAttr ".sh" 4;
	setAttr ".sc" 2;
	setAttr ".cuv" 3;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "9BD0107B-47B3-C9E5-C4A5-9BB79919CE59";
	setAttr ".ics" -type "componentList" 1 "f[60:69]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 5.406155956327436 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 4.4061561 -2.9802322e-08 ;
	setAttr ".rs" 40361;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.80905234813690186 4.406155956327436 -0.76945459842681885 ;
	setAttr ".cbx" -type "double3" 0.80905234813690186 4.406155956327436 0.76945453882217407 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "3645E704-4862-78BA-5C8A-A4B43757D6C3";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[0:9]" -type "float3"  0.25002885 0 -0.18165635 0.095502414
		 0 -0.29392624 -0.095502429 0 -0.29392624 -0.25002882 0 -0.18165633 -0.30905232 0
		 3.6841907e-08 -0.25002846 0 0.18165655 -0.095502406 0 0.29392624 0.095502429 0 0.29392624
		 0.25002846 0 0.18165635 0.30905232 0 1.8420954e-08;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "32E2AE3D-421A-9966-D5C7-1891ADFC1AF3";
	setAttr ".ics" -type "componentList" 1 "f[0:9]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.39938923292385381 0 0 0 0 1 0 0 5.406155956327436 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.9604645e-08 5.0067668 0 ;
	setAttr ".rs" 41129;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.5749993324279785 5.0067667234035822 -1.4979135990142822 ;
	setAttr ".cbx" -type "double3" 1.574999213218689 5.0067667234035822 1.4979135990142822 ;
createNode polyTweak -n "polyTweak5";
	rename -uid "C5E98F8C-4444-D401-D625-4D9374515BED";
	setAttr ".uopa" yes;
	setAttr -s 72 ".tk[0:71]" -type "float3"  0.50629812 0 -0.36784682 0.19338846
		 0 -0.59518898 -0.19338855 0 -0.59518898 -0.50629812 0 -0.36784676 -0.62581879 0 7.4603413e-08
		 -0.50629777 0 0.36784697 -0.19338848 0 0.59518874 0.19338849 0 0.59518874 0.50629777
		 0 0.36784682 0.62581879 0 3.7301703e-08 0.46518436 0 -0.33797601 0.17768463 0 -0.54685694
		 -0.17768466 0 -0.54685688 -0.46518436 0 -0.33797595 -0.57499921 0 6.8545241e-08 -0.4651843
		 0 0.33797604 -0.17768461 0 0.54685694 0.17768466 0 0.54685694 0.46518433 0 0.33797598
		 0.57499921 0 3.427262e-08 0.37190515 -0.2425679 -0.27020481 0.142055 -0.2425679 -0.43720055
		 -0.142055 -0.2425679 -0.43720052 -0.37190515 -0.2425679 -0.27020475 -0.45969954 -0.2425679
		 5.4800445e-08 -0.37190509 -0.2425679 0.27020478 -0.14205495 -0.2425679 0.43720055
		 0.142055 -0.2425679 0.43720061 0.37190518 -0.2425679 0.27020478 0.45969954 -0.2425679
		 2.7400223e-08 0.30076137 -0.42393613 -0.21851587 0.11488045 -0.42393613 -0.35356629
		 -0.11488061 -0.42393613 -0.35356629 -0.30076137 -0.42393613 -0.21851557 -0.3717612
		 -0.42393613 4.4317392e-08 -0.30076131 -0.42393613 0.21851617 -0.11488043 -0.42393613
		 0.35356629 0.11488045 -0.42393613 0.35356632 0.30076134 -0.42393613 0.2185159 0.3717612
		 -0.42393613 2.2158696e-08 -1.1920929e-07 -0.66308862 -1.0430813e-07 2.2351742e-08
		 -0.66308862 3.2782555e-07 -1.4901161e-08 -0.66308862 2.3841858e-07 8.9406967e-08
		 -0.66308862 -8.9406967e-08 -2.682209e-07 -0.66308862 -3.5527137e-15 0 -0.66308862
		 1.0430813e-07 2.2351742e-08 -0.66308862 -3.2782555e-07 0 -0.66308862 -2.682209e-07
		 -5.9604645e-08 -0.66308862 2.9802322e-08 0 -0.66308862 -1.7763568e-15 -0.42340624
		 -0.92043424 0.30762276 -0.16172679 -0.92043424 0.49774387 0.16172695 -0.92043424
		 0.49774382 0.42340624 -0.92043424 0.30762261 0.52335882 -0.92043424 -3.1194613e-08
		 0.42340621 -0.92043424 -0.3076227 0.16172685 -0.92043424 -0.49774408 -0.16172695
		 -0.92043424 -0.4977437 -0.42340618 -0.92043424 -0.30762258 -0.52335882 -0.92043424
		 3.0480664e-14 -0.21170312 -0.92043424 0.15381137 -0.080863416 -0.92043424 0.24887201
		 0.080863461 -0.92043424 0.24887201 0.21170309 -0.92043424 0.15381131 0.26167938 -0.92043424
		 -1.559726e-08 0.21170311 -0.92043424 -0.15381135 0.080863424 -0.92043424 -0.24887204
		 -0.080863468 -0.92043424 -0.24887201 -0.21170312 -0.92043424 -0.15381129 -0.26167944
		 -0.92043424 3.0480664e-14 -3.4745881e-08 -0.92043424 3.0480664e-14 0 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "D6C6941A-4204-3714-D598-6BB7AF55225B";
	setAttr ".ics" -type "componentList" 1 "f[10:19]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.39938923292385381 0 0 0 0 1 0 0 5.406155956327436 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.9604645e-08 5.0581746 0 ;
	setAttr ".rs" 55216;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.5749993324279785 5.0067667234035822 -1.4979135990142822 ;
	setAttr ".cbx" -type "double3" 1.574999213218689 5.1095822860025413 1.4979135990142822 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge2";
	rename -uid "A74A5FCE-430F-E2B8-915B-DB8CDD851080";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[10:19]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.39938923292385381 0 0 0 0 1 0 0 5.406155956327436 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1920929e-07 5.0067668 0 ;
	setAttr ".rs" 58050;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.5749993324279785 5.0067667234035822 -1.4979135990142822 ;
	setAttr ".cbx" -type "double3" 1.5749990940093994 5.0067667234035822 1.4979135990142822 ;
createNode polySplit -n "polySplit6";
	rename -uid "2FFE1DB4-469E-264C-A267-52B001435DDB";
	setAttr -s 11 ".e[0:10]"  0.208673 0.208673 0.208673 0.208673 0.208673
		 0.208673 0.208673 0.208673 0.208673 0.208673 0.208673;
	setAttr -s 11 ".d[0:10]"  -2147483444 -2147483441 -2147483402 -2147483407 -2147483412 -2147483417 
		-2147483422 -2147483427 -2147483432 -2147483437 -2147483444;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit7";
	rename -uid "E9930D60-43F5-C013-0649-EBB120D6E8AE";
	setAttr -s 11 ".e[0:10]"  0.742149 0.742149 0.742149 0.742149 0.742149
		 0.742149 0.742149 0.742149 0.742149 0.742149 0.742149;
	setAttr -s 11 ".d[0:10]"  -2147483494 -2147483491 -2147483452 -2147483457 -2147483462 -2147483467 
		-2147483472 -2147483477 -2147483482 -2147483487 -2147483494;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "161FD628-4AAF-2E50-102D-FA9CFF32C163";
	setAttr ".ics" -type "componentList" 2 "f[60:69]" "f[80:89]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.39938923292385381 0 0 0 0 1 0 0 5.406155956327436 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 5.0067668 -1.1920929e-07 ;
	setAttr ".rs" 60056;
	setAttr ".lt" -type "double3" 0 0 0.14252689543948183 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.4348711967468262 5.0067667234035822 -1.3646435737609863 ;
	setAttr ".cbx" -type "double3" 1.4348711967468262 5.0067667234035822 1.3646433353424072 ;
createNode polyTweak -n "polyTweak6";
	rename -uid "EB85A7C0-4C0C-9798-C25E-8EB9A6D923E2";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[10]" -type "float3" -7.4505806e-08 0 0 ;
	setAttr ".tk[11]" -type "float3" 7.4505806e-09 0 -2.9802322e-08 ;
	setAttr ".tk[12]" -type "float3" -7.4505806e-09 0 -1.4901161e-08 ;
	setAttr ".tk[13]" -type "float3" 7.4505806e-08 0 0 ;
	setAttr ".tk[14]" -type "float3" 4.4703484e-08 0 -5.3290705e-15 ;
	setAttr ".tk[15]" -type "float3" 5.9604645e-08 0 -7.4505806e-09 ;
	setAttr ".tk[16]" -type "float3" -1.4901161e-08 0 2.9802322e-08 ;
	setAttr ".tk[17]" -type "float3" 7.4505806e-09 0 1.4901161e-08 ;
	setAttr ".tk[18]" -type "float3" -7.4505806e-08 0 0 ;
	setAttr ".tk[19]" -type "float3" -5.9604645e-08 0 1.3322676e-15 ;
	setAttr ".tk[132]" -type "float3" -0.03496097 0 0.1075988 ;
	setAttr ".tk[133]" -type "float3" -0.091529004 0 0.066499725 ;
	setAttr ".tk[134]" -type "float3" -0.11313604 0 -2.3954656e-09 ;
	setAttr ".tk[135]" -type "float3" -0.091529004 0 -0.06649971 ;
	setAttr ".tk[136]" -type "float3" -0.034960974 0 -0.10759877 ;
	setAttr ".tk[137]" -type "float3" 0.03496097 0 -0.1075988 ;
	setAttr ".tk[138]" -type "float3" 0.091529004 0 -0.066499718 ;
	setAttr ".tk[139]" -type "float3" 0.11313604 0 -9.1389012e-09 ;
	setAttr ".tk[140]" -type "float3" 0.091529004 0 0.06649971 ;
	setAttr ".tk[141]" -type "float3" 0.034960981 0 0.10759877 ;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "198BF891-4297-6ACD-B3BA-36B7D08FE6C3";
	setAttr ".ics" -type "componentList" 2 "f[60:69]" "f[80:89]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.39938923292385381 0 0 0 0 1 0 0 5.406155956327436 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 4.8642397 -1.1920929e-07 ;
	setAttr ".rs" 55197;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.4348711967468262 4.8642394748477882 -1.3646435737609863 ;
	setAttr ".cbx" -type "double3" 1.4348711967468262 4.8642398557350415 1.3646433353424072 ;
createNode polySplit -n "polySplit8";
	rename -uid "E2478A83-44A9-3BC5-37B2-FF9B337BDCB9";
	setAttr -s 11 ".e[0:10]"  0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1;
	setAttr -s 11 ".d[0:10]"  -2147483368 -2147483367 -2147483365 -2147483363 -2147483361 -2147483359 
		-2147483357 -2147483355 -2147483353 -2147483351 -2147483368;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit9";
	rename -uid "A0EB9ECA-404A-564D-FA6D-12B6507E27C2";
	setAttr -s 11 ".e[0:10]"  0.69999999 0.69999999 0.69999999 0.69999999
		 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999;
	setAttr -s 11 ".d[0:10]"  -2147483298 -2147483297 -2147483296 -2147483295 -2147483294 -2147483293 
		-2147483292 -2147483291 -2147483290 -2147483289 -2147483298;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCylinder -n "polyCylinder3";
	rename -uid "9566EF34-4ED4-764F-51CD-0C9A49CCB6D6";
	setAttr ".sa" 10;
	setAttr ".sh" 4;
	setAttr ".sc" 2;
	setAttr ".cuv" 3;
createNode polyTweak -n "polyTweak7";
	rename -uid "C56535EF-4CFB-733C-119D-038DF1CFD494";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[10:19]" -type "float3"  0.28308427 0 -0.20567261 0.1081286
		 0 -0.33278552 -0.10812861 0 -0.33278549 -0.28308427 0 -0.20567258 -0.34991133 0 4.1712681e-08
		 -0.28308424 0 0.20567262 -0.10812857 0 0.33278552 0.10812861 0 0.33278552 0.28308427
		 0 0.20567259 0.34991133 0 2.0856341e-08;
createNode polySplit -n "polySplit10";
	rename -uid "6CBE4CF9-424D-918C-B0A2-2095D79049CB";
	setAttr -s 11 ".e[0:10]"  0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2;
	setAttr -s 11 ".d[0:10]"  -2147483568 -2147483559 -2147483560 -2147483561 -2147483562 -2147483563 
		-2147483564 -2147483565 -2147483566 -2147483567 -2147483568;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit11";
	rename -uid "565F12E7-40AA-92FB-D312-A3AFD8C05F34";
	setAttr -s 11 ".e[0:10]"  0.40000001 0.40000001 0.40000001 0.40000001
		 0.40000001 0.40000001 0.40000001 0.40000001 0.40000001 0.40000001 0.40000001;
	setAttr -s 11 ".d[0:10]"  -2147483498 -2147483497 -2147483496 -2147483495 -2147483494 -2147483493 
		-2147483492 -2147483491 -2147483490 -2147483489 -2147483498;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "E7AFB264-4ECD-2F60-ADB7-74AF9EE4770A";
	setAttr ".uopa" yes;
	setAttr -s 50 ".tk";
	setAttr ".tk[20]" -type "float3" -0.133589 0.041281257 0.097058088 ;
	setAttr ".tk[21]" -type "float3" -0.051026452 0.041281257 0.15704329 ;
	setAttr ".tk[22]" -type "float3" 0.051026482 0.041281257 0.15704328 ;
	setAttr ".tk[23]" -type "float3" 0.133589 0.041281257 0.097058065 ;
	setAttr ".tk[24]" -type "float3" 0.16512504 0.041281257 -1.9684437e-08 ;
	setAttr ".tk[25]" -type "float3" 0.13358898 0.041281257 -0.097058132 ;
	setAttr ".tk[26]" -type "float3" 0.051026441 0.041281257 -0.15704329 ;
	setAttr ".tk[27]" -type "float3" -0.051026482 0.041281257 -0.15704328 ;
	setAttr ".tk[28]" -type "float3" -0.133589 0.041281257 -0.09705808 ;
	setAttr ".tk[29]" -type "float3" -0.16512504 0.041281257 -9.8422186e-09 ;
	setAttr ".tk[30]" -type "float3" -0.13358912 -0.041281249 0.097058237 ;
	setAttr ".tk[31]" -type "float3" -0.051026449 -0.041281249 0.15704328 ;
	setAttr ".tk[32]" -type "float3" 0.051026471 -0.041281249 0.15704331 ;
	setAttr ".tk[33]" -type "float3" 0.13358912 -0.041281249 0.097058102 ;
	setAttr ".tk[34]" -type "float3" 0.16512522 -0.041281249 -1.9684453e-08 ;
	setAttr ".tk[35]" -type "float3" 0.13358907 -0.041281249 -0.097058281 ;
	setAttr ".tk[36]" -type "float3" 0.051026452 -0.041281249 -0.15704328 ;
	setAttr ".tk[37]" -type "float3" -0.051026471 -0.041281249 -0.15704329 ;
	setAttr ".tk[38]" -type "float3" -0.13358913 -0.041281249 -0.097058207 ;
	setAttr ".tk[39]" -type "float3" -0.16512522 -0.041281249 -9.8422266e-09 ;
	setAttr ".tk[40]" -type "float3" -0.20997503 -0.0050102323 0.1525557 ;
	setAttr ".tk[41]" -type "float3" -0.080203332 -0.0050102323 0.24683997 ;
	setAttr ".tk[42]" -type "float3" 0.080203339 -0.0050102323 0.24683996 ;
	setAttr ".tk[43]" -type "float3" 0.20997503 -0.0050102323 0.15255567 ;
	setAttr ".tk[44]" -type "float3" 0.25954354 -0.0050102323 -3.0940004e-08 ;
	setAttr ".tk[45]" -type "float3" 0.20997494 -0.0050102323 -0.1525557 ;
	setAttr ".tk[46]" -type "float3" 0.08020325 -0.0050102323 -0.24683997 ;
	setAttr ".tk[47]" -type "float3" -0.080203339 -0.0050102323 -0.24683991 ;
	setAttr ".tk[48]" -type "float3" -0.20997499 -0.0050102323 -0.15255569 ;
	setAttr ".tk[49]" -type "float3" -0.25954354 -0.0050102323 -1.5470002e-08 ;
	setAttr ".tk[50]" -type "float3" 0.058174819 0 -0.042266469 ;
	setAttr ".tk[51]" -type "float3" 0.022220798 0 -0.068388596 ;
	setAttr ".tk[52]" -type "float3" -0.022220802 0 -0.068388581 ;
	setAttr ".tk[53]" -type "float3" -0.058174819 0 -0.042266462 ;
	setAttr ".tk[54]" -type "float3" -0.071908005 0 8.5721013e-09 ;
	setAttr ".tk[55]" -type "float3" -0.058174811 0 0.042266473 ;
	setAttr ".tk[56]" -type "float3" -0.022220794 0 0.068388596 ;
	setAttr ".tk[57]" -type "float3" 0.022220802 0 0.068388596 ;
	setAttr ".tk[58]" -type "float3" 0.058174811 0 0.042266466 ;
	setAttr ".tk[59]" -type "float3" 0.071908005 0 4.2860506e-09 ;
	setAttr ".tk[82]" -type "float3" -0.14931773 0 0.10848572 ;
	setAttr ".tk[83]" -type "float3" -0.18456681 0 -1.5819986e-09 ;
	setAttr ".tk[84]" -type "float3" -0.14931773 0 -0.10848569 ;
	setAttr ".tk[85]" -type "float3" -0.05703428 0 -0.17553346 ;
	setAttr ".tk[86]" -type "float3" 0.05703431 0 -0.17553346 ;
	setAttr ".tk[87]" -type "float3" 0.14931771 0 -0.10848572 ;
	setAttr ".tk[88]" -type "float3" 0.18456681 0 -1.2583035e-08 ;
	setAttr ".tk[89]" -type "float3" 0.14931773 0 0.10848569 ;
	setAttr ".tk[90]" -type "float3" 0.05703428 0 0.17553346 ;
	setAttr ".tk[91]" -type "float3" -0.057034325 0 0.17553346 ;
createNode polySplit -n "polySplit12";
	rename -uid "C5A43B93-4C44-F64F-DBCF-B88E09EC833F";
	setAttr -s 11 ".e[0:10]"  0.80000001 0.80000001 0.80000001 0.80000001
		 0.80000001 0.80000001 0.80000001 0.80000001 0.80000001 0.80000001 0.80000001;
	setAttr -s 11 ".d[0:10]"  -2147483538 -2147483529 -2147483530 -2147483531 -2147483532 -2147483533 
		-2147483534 -2147483535 -2147483536 -2147483537 -2147483538;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "0C7E6368-4D83-E9B8-E9F7-98A638586F30";
	setAttr ".ics" -type "componentList" 1 "f[70:79]";
	setAttr ".ix" -type "matrix" 0.13406708957084318 0 0 0 0 0.13406708957084318 0 0
		 0 0 0.13406708957084318 0 0 5.2087446978289318 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.9955106e-09 5.3428116 -3.9955106e-09 ;
	setAttr ".rs" 64655;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.067033552776442837 5.3428117873997749 -0.063752703154918489 ;
	setAttr ".cbx" -type "double3" 0.067033544785421589 5.3428117873997749 0.063752695163897227 ;
createNode polyTweak -n "polyTweak9";
	rename -uid "18F44759-4F31-BFEA-70D8-428323338CEA";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[30:49]" -type "float3"  -0.11058358 0.04390014 0.080343664
		 -0.042239171 0.04390014 0.12999883 0.042239182 0.04390014 0.12999879 0.11058358 0.04390014
		 0.080343656 0.13668881 0.04390014 -1.6294582e-08 0.11058356 0.04390014 -0.080343664
		 0.042239152 0.04390014 -0.12999883 -0.042239182 0.04390014 -0.12999882 -0.11058356
		 0.04390014 -0.080343656 -0.13668881 0.04390014 -8.1472908e-09 -0.098077402 -0.04390014
		 0.07125742 -0.037462223 -0.04390014 0.11529696 0.037462238 -0.04390014 0.11529696
		 0.098077402 -0.04390014 0.071257398 0.12123031 -0.04390014 -1.4451778e-08 0.098077387
		 -0.04390014 -0.071257442 0.03746222 -0.04390014 -0.11529696 -0.037462238 -0.04390014
		 -0.11529696 -0.098077387 -0.04390014 -0.071257405 -0.12123031 -0.04390014 -7.2258888e-09;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "AA6CC627-43B2-5A58-2DEA-09B2E6B1560C";
	setAttr ".ics" -type "componentList" 1 "f[70:79]";
	setAttr ".ix" -type "matrix" 0.13406708957084318 0 0 0 0 0.13406708957084318 0 0
		 0 0 0.13406708957084318 0 0 5.2087446978289318 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -7.9910212e-09 5.3428116 -3.9955106e-09 ;
	setAttr ".rs" 40884;
	setAttr ".lt" -type "double3" 0 -6.9388939039072284e-18 0.029407988745322911 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.12713548595413471 5.3428117873997749 -0.12091306949131599 ;
	setAttr ".cbx" -type "double3" 0.12713546997209221 5.3428117873997749 0.12091306150029474 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "40BDB7BB-4B51-D0FA-3C4C-D8B74021F0EC";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[60]" -type "float3" 0.4070906 0 -0.29576832 ;
	setAttr ".tk[61]" -type "float3" 0.15549472 0 -0.47856328 ;
	setAttr ".tk[62]" -type "float3" -0.1554947 0 -0.47856328 ;
	setAttr ".tk[63]" -type "float3" -0.40709054 0 -0.2957682 ;
	setAttr ".tk[64]" -type "float3" -0.50319076 0 5.998502e-08 ;
	setAttr ".tk[65]" -type "float3" -0.40709054 0 0.29576841 ;
	setAttr ".tk[66]" -type "float3" -0.15549468 0 0.47856328 ;
	setAttr ".tk[67]" -type "float3" 0.1554947 0 0.47856328 ;
	setAttr ".tk[68]" -type "float3" 0.40709051 0 0.29576823 ;
	setAttr ".tk[69]" -type "float3" 0.50319076 0 2.999251e-08 ;
	setAttr ".tk[101]" -type "float3" 0.36268052 0 -0.26350275 ;
	setAttr ".tk[102]" -type "float3" 0.13853161 0 -0.42635643 ;
	setAttr ".tk[104]" -type "float3" -0.13853161 0 -0.42635638 ;
	setAttr ".tk[105]" -type "float3" -0.36268052 0 -0.26350257 ;
	setAttr ".tk[106]" -type "float3" -0.44829741 0 5.3441212e-08 ;
	setAttr ".tk[107]" -type "float3" -0.36268046 0 0.26350275 ;
	setAttr ".tk[108]" -type "float3" -0.13853155 0 0.42635643 ;
	setAttr ".tk[109]" -type "float3" 0.13853161 0 0.42635641 ;
	setAttr ".tk[110]" -type "float3" 0.36268046 0 0.2635026 ;
	setAttr ".tk[111]" -type "float3" 0.44829741 0 2.6720606e-08 ;
createNode polyTweak -n "polyTweak11";
	rename -uid "960E563A-48CC-4D5D-B0FF-2BB4F9048CE3";
	setAttr ".uopa" yes;
	setAttr -s 41 ".tk";
	setAttr ".tk[71]" -type "float3" -3.7252903e-08 0 1.4901161e-08 ;
	setAttr ".tk[72]" -type "float3" 1.4901161e-08 0 -4.4408921e-16 ;
	setAttr ".tk[73]" -type "float3" -3.7252903e-08 0 0 ;
	setAttr ".tk[74]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".tk[75]" -type "float3" -7.4505806e-09 0 1.4901161e-08 ;
	setAttr ".tk[76]" -type "float3" 2.2351742e-08 0 -7.4505806e-09 ;
	setAttr ".tk[77]" -type "float3" -1.4901161e-08 0 8.8817842e-16 ;
	setAttr ".tk[78]" -type "float3" -2.2351742e-08 0 0 ;
	setAttr ".tk[79]" -type "float3" -3.7252903e-09 0 -1.4901161e-08 ;
	setAttr ".tk[80]" -type "float3" 3.7252903e-09 0 -1.4901161e-08 ;
	setAttr ".tk[81]" -type "float3" 0 0 -1.4901161e-08 ;
	setAttr ".tk[82]" -type "float3" 0 0 -1.110223e-16 ;
	setAttr ".tk[83]" -type "float3" 0 0 -2.2351742e-08 ;
	setAttr ".tk[84]" -type "float3" 0 0 4.4703484e-08 ;
	setAttr ".tk[85]" -type "float3" 1.4901161e-08 0 4.4703484e-08 ;
	setAttr ".tk[86]" -type "float3" -1.4901161e-08 0 3.7252903e-08 ;
	setAttr ".tk[87]" -type "float3" 0 0 8.8817842e-16 ;
	setAttr ".tk[88]" -type "float3" -1.4901161e-08 0 2.2351742e-08 ;
	setAttr ".tk[89]" -type "float3" 0 0 -4.4703484e-08 ;
	setAttr ".tk[90]" -type "float3" -2.6077032e-08 0 -4.4703484e-08 ;
	setAttr ".tk[101]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[102]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[103]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[104]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[105]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[106]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[107]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[108]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[109]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[110]" -type "float3" 0 0.12052456 0 ;
	setAttr ".tk[111]" -type "float3" -0.34763768 0.062294435 0.25257367 ;
	setAttr ".tk[112]" -type "float3" -0.13278566 0.062294435 0.40867198 ;
	setAttr ".tk[113]" -type "float3" -1.5791754e-08 5.0511187e-07 1.2029594e-07 ;
	setAttr ".tk[114]" -type "float3" 0.13278563 0.062294435 0.40867221 ;
	setAttr ".tk[115]" -type "float3" 0.34763771 0.062294435 0.25257441 ;
	setAttr ".tk[116]" -type "float3" 0.42970321 0.062294435 6.6689034e-07 ;
	setAttr ".tk[117]" -type "float3" 0.34763768 0.062294435 -0.25257331 ;
	setAttr ".tk[118]" -type "float3" 0.13278557 0.062294435 -0.40867221 ;
	setAttr ".tk[119]" -type "float3" -0.13278566 0.062294435 -0.40867186 ;
	setAttr ".tk[120]" -type "float3" -0.34763768 0.062294435 -0.25257301 ;
	setAttr ".tk[121]" -type "float3" -0.42970321 0.062294435 6.8918283e-08 ;
createNode polySplit -n "polySplit13";
	rename -uid "1BDEBA23-4058-0842-8507-12B033FB687A";
	setAttr -s 11 ".e[0:10]"  0.69999999 0.69999999 0.69999999 0.69999999
		 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999;
	setAttr -s 11 ".d[0:10]"  -2147483508 -2147483507 -2147483506 -2147483505 -2147483504 -2147483503 
		-2147483502 -2147483501 -2147483500 -2147483499 -2147483508;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCylinder -n "polyCylinder4";
	rename -uid "7C2345D2-4E64-B135-535A-CAB9F53C6DCE";
	setAttr ".sa" 16;
	setAttr ".sh" 10;
	setAttr ".sc" 2;
	setAttr ".cuv" 3;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "7890C7D9-4B34-C07B-D232-2D9EB8C3FA3D";
	setAttr ".dc" -type "componentList" 2 "f[0:15]" "f[192:207]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "C444509A-47DA-A46B-896F-4AB3ACF645DD";
	setAttr ".dc" -type "componentList" 1 "f[160:191]";
createNode polyCylinder -n "polyCylinder5";
	rename -uid "E699256B-4B2C-1928-0844-CA985AD04510";
	setAttr ".sa" 10;
	setAttr ".sc" 2;
	setAttr ".cuv" 3;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "88075D02-4B39-6B5D-E415-D6AE0BE756AD";
	setAttr ".ics" -type "componentList" 5 "e[133]" "e[137]" "e[143]" "e[149]" "e[155]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "4E37101E-4EE5-2BD4-26AA-44B34E3DC7B9";
	setAttr ".ics" -type "componentList" 5 "e[101]" "e[103]" "e[105]" "e[107]" "e[109]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "7C89392C-45D8-A466-CD1C-0D89790940F0";
	setAttr ".ics" -type "componentList" 5 "e[223]" "e[230]" "e[236]" "e[242]" "e[248]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "60B5CE32-49FC-272C-2361-4B991E353163";
	setAttr ".uopa" yes;
	setAttr -s 30 ".tk";
	setAttr ".tk[20]" -type "float3" -0.093757965 0 0.068119191 ;
	setAttr ".tk[21]" -type "float3" -0.035812367 0 0.11021915 ;
	setAttr ".tk[22]" -type "float3" 0.03581237 0 0.11021908 ;
	setAttr ".tk[23]" -type "float3" 0.093757965 0 0.068119168 ;
	setAttr ".tk[24]" -type "float3" 0.11589128 0 -1.3815316e-08 ;
	setAttr ".tk[25]" -type "float3" 0.093757965 0 -0.068119191 ;
	setAttr ".tk[26]" -type "float3" 0.035812356 0 -0.11021915 ;
	setAttr ".tk[27]" -type "float3" -0.035812367 0 -0.11021909 ;
	setAttr ".tk[28]" -type "float3" -0.093757965 0 -0.068119176 ;
	setAttr ".tk[29]" -type "float3" -0.11589128 0 -6.9076576e-09 ;
	setAttr ".tk[30]" -type "float3" -0.059044473 0 0.042898316 ;
	setAttr ".tk[31]" -type "float3" -0.022552967 0 0.06941095 ;
	setAttr ".tk[32]" -type "float3" 0.022552993 0 0.069410935 ;
	setAttr ".tk[33]" -type "float3" 0.059044473 0 0.042898316 ;
	setAttr ".tk[34]" -type "float3" 0.072982974 0 -8.7002485e-09 ;
	setAttr ".tk[35]" -type "float3" 0.059044473 0 -0.042898316 ;
	setAttr ".tk[36]" -type "float3" 0.022552978 0 -0.06941095 ;
	setAttr ".tk[37]" -type "float3" -0.022552984 0 -0.06941095 ;
	setAttr ".tk[38]" -type "float3" -0.059044473 0 -0.042898316 ;
	setAttr ".tk[39]" -type "float3" -0.072982974 0 -4.3501243e-09 ;
	setAttr ".tk[122]" -type "float3" -0.070934385 -0.065015443 0.051536839 ;
	setAttr ".tk[123]" -type "float3" -0.087679707 -0.065015443 -8.9170499e-10 ;
	setAttr ".tk[124]" -type "float3" -0.070934385 -0.065015443 -0.051536821 ;
	setAttr ".tk[125]" -type "float3" -0.027094536 -0.065015443 -0.083388358 ;
	setAttr ".tk[126]" -type "float3" 0.027094502 -0.065015443 -0.083388358 ;
	setAttr ".tk[127]" -type "float3" 0.07093437 -0.065015443 -0.051536836 ;
	setAttr ".tk[128]" -type "float3" 0.087679707 -0.065015443 -6.1178214e-09 ;
	setAttr ".tk[129]" -type "float3" 0.07093437 -0.065015443 0.051536821 ;
	setAttr ".tk[130]" -type "float3" 0.027094521 -0.065015443 0.083388358 ;
	setAttr ".tk[131]" -type "float3" -0.027094526 -0.065015443 0.083388358 ;
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "FA1C5294-4899-CA3E-A39E-FF94651608CA";
	setAttr ".ics" -type "componentList" 5 "e[70]" "e[72]" "e[74]" "e[76]" "e[78]";
	setAttr ".cv" yes;
createNode polyCube -n "polyCube1";
	rename -uid "F69F533C-423F-2BC9-877C-398D99773A1A";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "4040BE27-4AA4-5D85-22CD-AA9EF6D4AC91";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 0.28888887942065072 0 0 0 0 1 0 0 0 0 0.32740585852627546 0
		 3.3128118069142447 1.7241884976163335 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" -6.616929226765933e-14 0.64367696711821099 0 ;
	setAttr ".pvt" -type "float3" 3.3128119 2.8678656 0 ;
	setAttr ".rs" 39052;
	setAttr ".lt" -type "double3" -0.4025024940943438 0 0.37691724302874619 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 3.1683673672039192 2.2241884976163337 -0.16370292926313773 ;
	setAttr ".cbx" -type "double3" 3.4572562466245702 2.2241884976163337 0.16370292926313773 ;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "6B81E4E0-4691-EB4A-4240-96AFBD081984";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 0.28888887942065072 0 0 0 0 1 0 0 0 0 0.32740585852627546 0
		 3.3128118069142447 1.7241884976163335 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 3.3124464 1.4315894 -0.0008317352 ;
	setAttr ".rs" 62416;
	setAttr ".lt" -type "double3" -0.76668415251224575 -2.4191599786761719e-16 0.96877563215976947 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 3.1676362606287092 1.4313771038785406 -0.16536641921239373 ;
	setAttr ".cbx" -type "double3" 3.4572562466245702 1.4318015187516637 0.16370294877804761 ;
createNode polyTweak -n "polyTweak13";
	rename -uid "D538D40E-49AC-AAAF-9C15-D897D3991469";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0.20739846 0 ;
	setAttr ".tk[1]" -type "float3" 0 0.20739846 0 ;
	setAttr ".tk[4]" -type "float3" 0.0025341965 0.00020983884 0.003169822 ;
	setAttr ".tk[5]" -type "float3" 0.0025307722 -0.00021455782 0.005080747 ;
	setAttr ".tk[6]" -type "float3" -0.0025307722 0.20761302 -0.005080747 ;
	setAttr ".tk[7]" -type "float3" -0.0025341965 0.20718862 -0.003169822 ;
	setAttr ".tk[8]" -type "float3" 0.23134091 -0.49450344 0 ;
	setAttr ".tk[9]" -type "float3" -0.23134087 -0.25086045 0 ;
	setAttr ".tk[10]" -type "float3" -0.23134087 -0.25086045 0 ;
	setAttr ".tk[11]" -type "float3" 0.23134091 -0.49450344 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "A81E78D3-4605-DA20-5191-06AAAC945F32";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 0.28888887942065072 0 0 0 0 1 0 0 0 0 0.32740585852627546 0
		 3.3128118069142447 1.7241884976163335 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" -0.45782913200075415 0 0 ;
	setAttr ".pvt" -type "float3" 2.4524789 2.8025916 0 ;
	setAttr ".rs" 41916;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.8326964831587644 2.6807700901761482 -0.16370294877804761 ;
	setAttr ".cbx" -type "double3" 2.9879217444219477 2.9244128971829841 0.16370294877804761 ;
createNode polyTweak -n "polyTweak14";
	rename -uid "9F8C00FC-4E9C-5D8E-5E76-0B808B44200F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  1.5543122e-15 -0.069509402
		 0 1.5543122e-15 -0.069509402 0 1.5543122e-15 -0.069509402 0 1.5543122e-15 -0.069509402
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "C37CC519-4ED8-2E4F-6798-9493483F8008";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 0.28888887942065072 0 0 0 0 1 0 0 0 0 0.32740585852627546 0
		 3.3128118069142447 1.7241884976163335 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.4524801 2.8025913 0 ;
	setAttr ".rs" 59453;
	setAttr ".lt" -type "double3" 0.31786291427712726 0 0.93452860708791796 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.3793094336144183 2.6780512839093635 -0.16370294877804761 ;
	setAttr ".cbx" -type "double3" 2.5256506626014303 2.9271314650311897 0.16370294877804761 ;
createNode polyTweak -n "polyTweak15";
	rename -uid "C19BFBFA-4E14-6F8B-7B28-F2B12FB08A94";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  0.52194202 -0.002718694 0
		 -0.5219419 0.0027187001 0 -0.5219419 0.0027187001 0 0.52194202 -0.002718694 0;
createNode polyExtrudeFace -n "polyExtrudeFace13";
	rename -uid "73DD1682-45C2-32CA-89CE-A3963DC4BBAD";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 0.28888887942065072 0 0 0 0 1 0 0 0 0 0.32740585852627546 0
		 3.3128118069142447 2.2791779815619693 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" -0.07907614290189402 -0.45784812196865221 0 ;
	setAttr ".pvt" -type "float3" 2.4660091 0.56061763 -0.00083700911 ;
	setAttr ".rs" 33733;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.4082923103958374 0.97095044249946927 -0.16537172726788513 ;
	setAttr ".cbx" -type "double3" 2.6818778010866877 1.0659809886878238 0.16369770902474087 ;
createNode polyTweak -n "polyTweak16";
	rename -uid "3488726E-4BEF-55B3-75B8-D1AEA1332F28";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[8:23]" -type "float3"  -1.5543122e-15 -0.14423102
		 0 -1.5543122e-15 -0.14423102 0 -1.5543122e-15 -0.14423102 0 -1.5543122e-15 -0.14423102
		 0 0.02787108 0.047402229 0 -0.02773127 -0.047162369 0 -0.027633157 -0.047413819 0
		 0.027488504 0.047174651 0 -1.3322676e-15 -0.14423102 0 -1.3322676e-15 -0.14423102
		 0 -1.3322676e-15 -0.14423102 0 -1.3322676e-15 -0.14423102 0 0.2540628 -0.12804437
		 0 0.2540628 -0.12804437 0 0.2540628 -0.12804437 0 0.2540628 -0.12804437 0;
createNode polyExtrudeFace -n "polyExtrudeFace14";
	rename -uid "9B68D8EA-49F8-B3E3-D46B-BEB71DB1FEDD";
	setAttr ".ics" -type "componentList" 2 "f[7]" "f[15]";
	setAttr ".ix" -type "matrix" 0.28888887942065072 0 0 0 0 1 0 0 0 0 0.32740585852627546 0
		 3.3128118069142447 2.2791779815619693 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" 0 0.49228617993464363 0 ;
	setAttr ".pvt" -type "float3" 2.9186482 3.5507138 0 ;
	setAttr ".rs" 33002;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.3793093647379422 2.7789631664221988 -0.16370294877804761 ;
	setAttr ".cbx" -type "double3" 3.457987163789471 3.3378899871772036 0.16370294877804761 ;
createNode polyTweak -n "polyTweak17";
	rename -uid "8D0D1519-40FB-39A2-34FE-199674928118";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[0]" -type "float3" -0.39047915 -0.14688085 0 ;
	setAttr ".tk[1]" -type "float3" -0.39047915 -0.14688085 0 ;
	setAttr ".tk[2]" -type "float3" 0 -0.054883756 0 ;
	setAttr ".tk[4]" -type "float3" 0 -0.054883756 0 ;
	setAttr ".tk[6]" -type "float3" -0.39047915 -0.14688085 0 ;
	setAttr ".tk[7]" -type "float3" -0.39047915 -0.14688085 0 ;
createNode polyTweak -n "polyTweak18";
	rename -uid "1785B5B3-4088-D874-7F5A-42B9280814B6";
	setAttr ".uopa" yes;
	setAttr -s 22 ".tk";
	setAttr ".tk[0]" -type "float3" -0.27654898 0.0011744426 -0.022324961 ;
	setAttr ".tk[1]" -type "float3" -0.20060702 0 0 ;
	setAttr ".tk[2]" -type "float3" -0.093288466 -0.1036174 -0.022324961 ;
	setAttr ".tk[4]" -type "float3" -0.093400925 -0.10364223 0.021957722 ;
	setAttr ".tk[6]" -type "float3" -0.27643651 0.0011490379 0.02232424 ;
	setAttr ".tk[7]" -type "float3" -0.20060703 0 0 ;
	setAttr ".tk[8]" -type "float3" -0.041671056 -0.14712094 -0.022324961 ;
	setAttr ".tk[11]" -type "float3" -0.041671056 -0.14712094 0.022098508 ;
	setAttr ".tk[12]" -type "float3" 0.15122943 0.092830688 0.022324961 ;
	setAttr ".tk[15]" -type "float3" 0.15113394 0.092883073 -0.022324244 ;
	setAttr ".tk[16]" -type "float3" 0.0055443859 -0.14679886 -0.022324961 ;
	setAttr ".tk[19]" -type "float3" 0.0055443859 -0.14679886 0.022098508 ;
	setAttr ".tk[20]" -type "float3" 0.093400925 -0.060170047 -0.022324961 ;
	setAttr ".tk[23]" -type "float3" 0.093400925 -0.060170047 0.022098508 ;
	setAttr ".tk[24]" -type "float3" 0.20606224 0.14706856 0.022324961 ;
	setAttr ".tk[27]" -type "float3" 0.2059667 0.14712094 -0.022324244 ;
	setAttr ".tk[28]" -type "float3" -0.416877 0 0 ;
	setAttr ".tk[29]" -type "float3" -0.416877 0 0 ;
	setAttr ".tk[30]" -type "float3" 3.1086245e-15 0.0061869659 0 ;
	setAttr ".tk[31]" -type "float3" 3.1086245e-15 0.0061869659 0 ;
	setAttr ".tk[32]" -type "float3" 0.34851396 0.24664426 0 ;
	setAttr ".tk[33]" -type "float3" 0.34851396 0.24664426 0 ;
createNode polySplit -n "polySplit14";
	rename -uid "7C8E47D7-4487-0099-52BA-7B9F171CD313";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483644 -2147483640 -2147483639 -2147483643 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit15";
	rename -uid "EE477281-491A-5A97-FE6C-B19AA332F44B";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483629 -2147483624 -2147483626 -2147483628 -2147483629;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak19";
	rename -uid "CE352F82-4096-37FE-BFE8-479DF15A1BE4";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[34:41]" -type "float3"  0.13962784 0 0 0.13962784
		 0 0 0.13962784 0 0 0.13962784 0 0 0.13962784 0 0 0.13962784 0 0 0.13962784 0 0 0.13962784
		 0 0;
createNode polySplit -n "polySplit16";
	rename -uid "2B765307-4BD5-6D46-31E7-1687033FF4C6";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483636 -2147483632 -2147483633 -2147483593 -2147483590 -2147483635 
		-2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak20";
	rename -uid "2DB38E96-4C81-0E7D-ABEB-44BCD312F9C2";
	setAttr ".uopa" yes;
	setAttr -s 38 ".tk";
	setAttr ".tk[0]" -type "float3" -0.064712159 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.064712159 0 0 ;
	setAttr ".tk[2]" -type "float3" -0.066688083 0 0 ;
	setAttr ".tk[3]" -type "float3" -0.066688083 0 0 ;
	setAttr ".tk[4]" -type "float3" -0.066688083 0 0 ;
	setAttr ".tk[5]" -type "float3" -0.066688083 0 0 ;
	setAttr ".tk[6]" -type "float3" -0.064712159 0 0 ;
	setAttr ".tk[7]" -type "float3" -0.064712159 0 0 ;
	setAttr ".tk[12]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[13]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[14]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[15]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[16]" -type "float3" -0.079804666 0 0 ;
	setAttr ".tk[17]" -type "float3" -0.079804666 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.079804666 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.079804666 0 0 ;
	setAttr ".tk[20]" -type "float3" 0.067871183 0.028188312 0 ;
	setAttr ".tk[21]" -type "float3" 0.067871183 0.028188312 0 ;
	setAttr ".tk[22]" -type "float3" 0.067871183 0.028188312 0 ;
	setAttr ".tk[23]" -type "float3" 0.067871183 0.028188312 0 ;
	setAttr ".tk[24]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[25]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[26]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[27]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[34]" -type "float3" -0.040279437 0 0 ;
	setAttr ".tk[35]" -type "float3" -0.040279437 0 0 ;
	setAttr ".tk[36]" -type "float3" -0.040279437 0 0 ;
	setAttr ".tk[37]" -type "float3" -0.040279437 0 0 ;
	setAttr ".tk[38]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[39]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[40]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[41]" -type "float3" -0.18489775 0 0 ;
	setAttr ".tk[42]" -type "float3" 0.064326063 0.054162409 0 ;
	setAttr ".tk[43]" -type "float3" 0.064326063 0.054162409 0 ;
	setAttr ".tk[44]" -type "float3" 0.064326063 0.054162409 0 ;
	setAttr ".tk[45]" -type "float3" 0.064326063 0.054162409 0 ;
	setAttr ".tk[46]" -type "float3" 0.064326063 0.054162409 0 ;
	setAttr ".tk[47]" -type "float3" 0.064326063 0.054162409 0 ;
createNode polySplit -n "polySplit17";
	rename -uid "6B54B434-459B-7975-4CFE-C691FB1E8503";
	setAttr -s 9 ".e[0:8]"  0.50353998 0.49645999 0.50353998 0.50353998
		 0.50353998 0.50353998 0.50353998 0.50353998 0.50353998;
	setAttr -s 9 ".d[0:8]"  -2147483597 -2147483558 -2147483592 -2147483587 -2147483589 -2147483594 
		-2147483560 -2147483596 -2147483597;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak21";
	rename -uid "640953C7-4E53-11EC-D642-75AE87301F5A";
	setAttr ".uopa" yes;
	setAttr -s 42 ".tk";
	setAttr ".tk[0]" -type "float3" 0.060556866 0.00076266436 0 ;
	setAttr ".tk[1]" -type "float3" 0.27426136 0.00076266436 0 ;
	setAttr ".tk[2]" -type "float3" 0.1087259 0.0013693139 0 ;
	setAttr ".tk[3]" -type "float3" 0.1581724 0.00030429201 0 ;
	setAttr ".tk[4]" -type "float3" 0.1087259 0.0013693139 0 ;
	setAttr ".tk[5]" -type "float3" 0.1581724 0.00030429201 0 ;
	setAttr ".tk[6]" -type "float3" 0.060556866 0.00076266436 0 ;
	setAttr ".tk[7]" -type "float3" 0.27426136 0.00076266436 0 ;
	setAttr ".tk[12]" -type "float3" -0.13023469 -0.0016401997 0 ;
	setAttr ".tk[13]" -type "float3" -0.021506757 -0.0020966376 0 ;
	setAttr ".tk[14]" -type "float3" -0.021506757 -0.0020966376 0 ;
	setAttr ".tk[15]" -type "float3" -0.13023469 -0.0016401997 0 ;
	setAttr ".tk[16]" -type "float3" 0.0047399332 -0.031409703 0 ;
	setAttr ".tk[19]" -type "float3" 0.0047399332 -0.031409703 0 ;
	setAttr ".tk[20]" -type "float3" 0.22084089 0.11028949 0 ;
	setAttr ".tk[21]" -type "float3" -0.0013474289 0.064222038 0 ;
	setAttr ".tk[22]" -type "float3" -0.0013474289 0.064222038 0 ;
	setAttr ".tk[23]" -type "float3" 0.22084089 0.11028949 0 ;
	setAttr ".tk[24]" -type "float3" -0.093992725 -0.048472255 0 ;
	setAttr ".tk[25]" -type "float3" -0.033589453 -0.047711533 0 ;
	setAttr ".tk[26]" -type "float3" -0.036212325 -0.047744565 0 ;
	setAttr ".tk[27]" -type "float3" -0.095985278 -0.048497353 0 ;
	setAttr ".tk[30]" -type "float3" 0 -0.076579869 0 ;
	setAttr ".tk[31]" -type "float3" 0 -0.076579869 0 ;
	setAttr ".tk[32]" -type "float3" 0.016958017 -0.073841199 0 ;
	setAttr ".tk[33]" -type "float3" 0.016958017 -0.073841199 0 ;
	setAttr ".tk[34]" -type "float3" 0.1087259 0.0013693139 0 ;
	setAttr ".tk[35]" -type "float3" 0.1087259 0.0013693139 0 ;
	setAttr ".tk[36]" -type "float3" 0.29830536 0.0013693139 0 ;
	setAttr ".tk[37]" -type "float3" 0.29830536 0.0013693139 0 ;
	setAttr ".tk[38]" -type "float3" -0.050707966 -0.00063862582 0 ;
	setAttr ".tk[39]" -type "float3" -0.050707966 -0.00063862582 0 ;
	setAttr ".tk[40]" -type "float3" 0.083303131 -0.00063862582 0 ;
	setAttr ".tk[41]" -type "float3" 0.083303131 -0.00063862582 0 ;
	setAttr ".tk[45]" -type "float3" 0 -0.076579869 0 ;
	setAttr ".tk[46]" -type "float3" 0 -0.076579869 0 ;
	setAttr ".tk[48]" -type "float3" 0.096645243 0.0012171679 0 ;
	setAttr ".tk[50]" -type "float3" 0 -0.076579869 0 ;
	setAttr ".tk[51]" -type "float3" 0.12080655 -0.075058386 0 ;
	setAttr ".tk[52]" -type "float3" 0.12080655 -0.075058386 0 ;
	setAttr ".tk[53]" -type "float3" 0 -0.076579869 0 ;
	setAttr ".tk[55]" -type "float3" 0.096645243 0.0012171679 0 ;
createNode polySplit -n "polySplit18";
	rename -uid "6EF0D940-4326-A6F5-74BA-269D6A89CC43";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483613 -2147483608 -2147483610 -2147483612 -2147483613;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace15";
	rename -uid "45491B41-4F7D-8019-EFC2-3781A9BA073D";
	setAttr ".ics" -type "componentList" 1 "f[25]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.5741946 0.73236519 -0.00059851963 ;
	setAttr ".rs" 46346;
	setAttr ".off" 0.10000000149011612;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.5471899825668225 0.56639244589558113 -0.1170670173199478 ;
	setAttr ".cbx" -type "double3" 1.6011998671532164 0.89835651495820368 0.11586997808160314 ;
createNode polyTweak -n "polyTweak22";
	rename -uid "CBA2D70A-4DD3-3DAD-3021-25AF1AC7C5C8";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[12]" -type "float3" -0.021293929 0.020147581 -0.028084207 ;
	setAttr ".tk[13]" -type "float3" 0.031346265 0.0091585843 -0.029275697 ;
	setAttr ".tk[14]" -type "float3" 0.031499892 0.0091561619 0.029275697 ;
	setAttr ".tk[15]" -type "float3" -0.021174032 0.020124841 0.027972713 ;
	setAttr ".tk[20]" -type "float3" -0.15213707 -0.044802502 0.027662914 ;
	setAttr ".tk[21]" -type "float3" -0.19500947 -0.060970884 0.028955787 ;
	setAttr ".tk[22]" -type "float3" -0.19500947 -0.060970884 -0.028955787 ;
	setAttr ".tk[23]" -type "float3" -0.15213707 -0.044802502 -0.027676027 ;
	setAttr ".tk[24]" -type "float3" -0.031954575 -0.0061430987 -0.028084207 ;
	setAttr ".tk[25]" -type "float3" 0.014664686 -0.020226711 -0.029275697 ;
	setAttr ".tk[26]" -type "float3" 0.014665341 -0.020231061 0.029275697 ;
	setAttr ".tk[27]" -type "float3" -0.031950936 -0.0061672977 0.027972713 ;
	setAttr ".tk[56]" -type "float3" 0.008426548 -0.038011041 0 ;
	setAttr ".tk[57]" -type "float3" 0.008426548 -0.038011041 0 ;
	setAttr ".tk[58]" -type "float3" 0.008426548 -0.073667899 0 ;
	setAttr ".tk[59]" -type "float3" 0.008426548 -0.073667899 0 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge3";
	rename -uid "6FC5451D-41C0-9563-4E62-6D8FC88B9832";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[118]" "e[120]" "e[122:123]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.5741044 0.73237979 -0.00059852301 ;
	setAttr ".rs" 40135;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.5608432306359927 0.61320924082072925 -0.079793603287910297 ;
	setAttr ".cbx" -type "double3" 1.587360864635176 0.8515609189987472 0.07859655722011244 ;
createNode polyTweak -n "polyTweak23";
	rename -uid "A91A3BF7-4F09-2CF8-DE92-4DA8FFA470BB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[60:63]" -type "float3"  0 0.074409977 -0.27372259
		 0 0.074341774 0.27372259 0 -0.074396826 -0.27372524 0 -0.074421361 0.27372524;
createNode polyExtrudeFace -n "polyExtrudeFace16";
	rename -uid "BA384454-4057-5A88-8281-3DBD78F76976";
	setAttr ".ics" -type "componentList" 1 "f[25]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" -0.13176227467416135 0 0 ;
	setAttr ".pvt" -type "float3" 1.442837 0.73237991 -0.00059852301 ;
	setAttr ".rs" 52534;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.5549910463875847 0.61320930921725192 -0.079793603287910297 ;
	setAttr ".cbx" -type "double3" 1.5942031343996956 0.85156103693805385 0.07859655722011244 ;
createNode polyTweak -n "polyTweak24";
	rename -uid "E5D7BB63-48A1-D543-6F46-C6917083E871";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[60:67]" -type "float3"  0.033807557 0.00042577862
		 0 0.033807557 0.00042577862 0 -0.028914765 -0.00036415795 0 -0.028914765 -0.00036415795
		 0 0.033807557 0.00042577862 0 0.033807557 0.00042577862 0 -0.028914765 -0.00036415795
		 0 -0.028914765 -0.00036415795 0;
createNode polySplit -n "polySplit19";
	rename -uid "00F84F7C-4174-4C0E-26B6-268421B410B3";
	setAttr -s 5 ".e[0:4]"  0.69999999 0.69999999 0.69999999 0.69999999
		 0.69999999;
	setAttr -s 5 ".d[0:4]"  -2147483540 -2147483539 -2147483538 -2147483537 -2147483540;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode objectSet -n "set1";
	rename -uid "BE42CBC9-4678-917A-B248-75A7D4294622";
	setAttr ".ihi" 0;
createNode groupId -n "groupId1";
	rename -uid "AD696FD5-43B9-0B6C-73A2-60BA0098B0F2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "AFB988E4-4822-DF57-1B31-E5A4C6090AEA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "e[37]" "e[39]" "e[41:42]" "e[140:147]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "ABC07079-489D-CC94-9B62-9C919DFF882B";
	setAttr ".dc" -type "componentList" 2 "f[1]" "f[70:73]";
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "3880BEE8-4521-EEEF-F65B-8E8EB0CFBC93";
	setAttr ".ics" -type "componentList" 1 "e[136:139]";
createNode polyExtrudeFace -n "polyExtrudeFace17";
	rename -uid "281C4BE3-494C-5C91-24AD-35BAC2FD3EC6";
	setAttr ".ics" -type "componentList" 1 "f[69]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.3850026 1.9025006 0 ;
	setAttr ".rs" 36703;
	setAttr ".off" 0.10000000149011612;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.3076071019463607 1.8060241988778378 -0.11922404547621139 ;
	setAttr ".cbx" -type "double3" 1.4623980422620124 1.9989770370021502 0.11922404547621139 ;
createNode polyExtrudeFace -n "polyExtrudeFace18";
	rename -uid "2993937B-4B09-8DA8-FCB7-C9A109DC0E42";
	setAttr ".ics" -type "componentList" 1 "f[69]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.3850025 1.9025006 -1.1145667e-05 ;
	setAttr ".rs" 49177;
	setAttr ".lt" -type "double3" 1.2490009027033011e-16 -3.3881317890172014e-21 0.12631637825036804 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.3204070868894933 1.8219795980406028 -0.092848383079237123 ;
	setAttr ".cbx" -type "double3" 1.4495979655366242 1.9830217462361612 0.092826091744006078 ;
createNode polyTweak -n "polyTweak25";
	rename -uid "67EFEC74-450C-8D16-C151-B0AD2454EBE9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[72:75]" -type "float3"  0.25929955 -0.085466892 0.31091905
		 0.25929955 -0.085466892 -0.3111181 -0.25929955 0.085466892 0.33067602 -0.25929955
		 0.085466892 -0.33067602;
createNode objectSet -n "set2";
	rename -uid "C06A0AA9-4AC7-CDCA-9CF5-65A0327FA335";
	setAttr ".ihi" 0;
createNode groupId -n "groupId2";
	rename -uid "A15045B1-4D73-8C75-7F75-D3A623DFB3AD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "47939232-4907-C0B0-CD4B-74A492228E88";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[150]" "e[152]" "e[154:155]";
createNode deleteComponent -n "deleteComponent5";
	rename -uid "7342E506-41F8-B998-BC52-8D887D03E138";
	setAttr ".dc" -type "componentList" 1 "f[69]";
createNode polyBevel3 -n "polyBevel1";
	rename -uid "70D0AB0A-4706-9BD0-AAB7-19A6DD681178";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 31 "e[4:5]" "e[8:9]" "e[12]" "e[16]" "e[19:20]" "e[22]" "e[24]" "e[26:27]" "e[32]" "e[35:48]" "e[51]" "e[54:63]" "e[68:71]" "e[76:77]" "e[79:80]" "e[88]" "e[91:92]" "e[95]" "e[104:107]" "e[114]" "e[116]" "e[118:119]" "e[128:129]" "e[131]" "e[133]" "e[136:139]" "e[142]" "e[144]" "e[146:149]" "e[151]" "e[153]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "BCCBE0D0-4F0B-2806-D8E6-B292CC4BC624";
	setAttr ".ics" -type "componentList" 1 "vtx[0:79]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".am" yes;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "14DE144C-44CE-62AC-56F0-C9AE6752C0A7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 30 "e[4:5]" "e[8:9]" "e[12]" "e[16]" "e[19:20]" "e[22]" "e[24]" "e[27]" "e[32]" "e[35:48]" "e[51]" "e[54:63]" "e[68:71]" "e[76:77]" "e[79:80]" "e[88]" "e[91:92]" "e[95]" "e[104:107]" "e[114]" "e[116]" "e[118:121]" "e[123]" "e[125]" "e[128:131]" "e[134]" "e[136]" "e[138:141]" "e[143]" "e[145]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode deleteComponent -n "deleteComponent6";
	rename -uid "11A96FAF-4EF1-0964-C70B-D3887CB47A06";
	setAttr ".dc" -type "componentList" 1 "f[162]";
createNode polyTweak -n "polyTweak26";
	rename -uid "5200EA33-4BBB-A3B7-36CA-96B21AB23C36";
	setAttr ".uopa" yes;
	setAttr -s 41 ".tk";
	setAttr ".tk[4]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[7]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[86]" -type "float3" 0.0010019431 -0.0066394894 1.110223e-16 ;
	setAttr ".tk[134]" -type "float3" 0.059805885 -0.047939122 0 ;
	setAttr ".tk[135]" -type "float3" 0.059805885 -0.047939122 0 ;
	setAttr ".tk[136]" -type "float3" 0.059805885 -0.047939122 0 ;
	setAttr ".tk[137]" -type "float3" 0.059920169 -0.048696425 0 ;
	setAttr ".tk[138]" -type "float3" 0.059920169 -0.048696425 0 ;
	setAttr ".tk[139]" -type "float3" 0.059920169 -0.048696425 0 ;
	setAttr ".tk[140]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[141]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[142]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[143]" -type "float3" 0.014399125 -0.095417432 0 ;
	setAttr ".tk[144]" -type "float3" 0.014399125 -0.095417432 0 ;
	setAttr ".tk[145]" -type "float3" 0.014399125 -0.095417432 0 ;
	setAttr ".tk[146]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[147]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[148]" -type "float3" 0.0074483403 -0.049357265 0 ;
	setAttr ".tk[149]" -type "float3" 0.014075884 -0.093275674 0 ;
	setAttr ".tk[150]" -type "float3" 0.014075884 -0.093275674 0 ;
	setAttr ".tk[151]" -type "float3" 0.014075884 -0.093275674 0 ;
	setAttr ".tk[155]" -type "float3" 0.018191891 -0.1205506 0 ;
	setAttr ".tk[156]" -type "float3" 0.018191891 -0.1205506 0 ;
	setAttr ".tk[157]" -type "float3" 0.018191891 -0.1205506 0 ;
	setAttr ".tk[158]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[159]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[160]" -type "float3" 0.02753108 -0.18243763 0 ;
	setAttr ".tk[161]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[162]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[163]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[167]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[168]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[169]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[170]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[171]" -type "float3" 0.02753108 -0.18243763 0 ;
	setAttr ".tk[172]" -type "float3" 0.026559843 -0.17600164 0 ;
	setAttr ".tk[173]" -type "float3" 0.018191891 -0.1205506 0 ;
	setAttr ".tk[174]" -type "float3" 0.018191891 -0.1205506 0 ;
	setAttr ".tk[175]" -type "float3" 0.018191891 -0.1205506 0 ;
	setAttr ".tk[276]" -type "float3" 0.02753108 -0.18243763 0 ;
	setAttr ".tk[277]" -type "float3" 0.02753108 -0.18243763 0 ;
createNode polySplit -n "polySplit20";
	rename -uid "87C4649A-4005-3E3A-2793-1B8AE02D500C";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483322 -2147483113;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit21";
	rename -uid "B7A980E5-43A3-F045-FA10-B7A8A13748CE";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483321 -2147483116;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit22";
	rename -uid "2CC43FF3-4C2E-B323-252D-2FB7E9D84AFF";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483301 -2147483107;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit23";
	rename -uid "F612F801-4D18-F6BA-3BD8-6787B78E1B4D";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483308 -2147483110;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge5";
	rename -uid "D212C91E-4DCB-B390-7064-17B0F2EF285A";
	setAttr ".ics" -type "componentList" 83 "e[11:14]" "e[16]" "e[58:59]" "e[63]" "e[65]" "e[67:70]" "e[72]" "e[74]" "e[78]" "e[80]" "e[82:83]" "e[87]" "e[89]" "e[91:94]" "e[98]" "e[100]" "e[102:103]" "e[107]" "e[109]" "e[111:112]" "e[114]" "e[116]" "e[120]" "e[122]" "e[124:125]" "e[127]" "e[129]" "e[131]" "e[133]" "e[135]" "e[137]" "e[141]" "e[143]" "e[148]" "e[150]" "e[153:154]" "e[159]" "e[161]" "e[163:168]" "e[170]" "e[172]" "e[174:175]" "e[178:179]" "e[181:182]" "e[184]" "e[186:187]" "e[189]" "e[192:193]" "e[195:196]" "e[199:200]" "e[202:203]" "e[206:207]" "e[209:211]" "e[213:218]" "e[220:224]" "e[227:228]" "e[230:232]" "e[234:238]" "e[241:242]" "e[244:246]" "e[248:256]" "e[261]" "e[264]" "e[266]" "e[268]" "e[272]" "e[274]" "e[277:278]" "e[281]" "e[283]" "e[285]" "e[287]" "e[290:291]" "e[293:299]" "e[301:305]" "e[308:309]" "e[311:313]" "e[316:318]" "e[320]" "e[322:327]" "e[329:334]" "e[336:341]" "e[343:347]";
	setAttr ".cv" yes;
createNode polySplit -n "polySplit24";
	rename -uid "EE91E659-47FE-B3C7-B607-3E945387FAED";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483618 -2147483421;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit25";
	rename -uid "FAD854F1-4534-F94F-B3EA-6D9F12D4A96B";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483619 -2147483416;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit26";
	rename -uid "5965C2EF-4B66-3F7D-88E1-56890D15DFC9";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483619 -2147483589;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit27";
	rename -uid "1FC4F070-4509-A793-B615-B6B17AD497E9";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483584 -2147483618;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge6";
	rename -uid "0621D903-48B6-7ABA-DD74-439168F18FC5";
	setAttr ".ics" -type "componentList" 1 "e[198:201]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge7";
	rename -uid "97DD01C4-46B9-8AAC-770C-97BE73182839";
	setAttr ".ics" -type "componentList" 2 "e[67]" "e[69]";
	setAttr ".cv" yes;
createNode deleteComponent -n "deleteComponent7";
	rename -uid "86CD579B-46CF-6305-E2D1-99A1B641C202";
	setAttr ".dc" -type "componentList" 1 "e[226]";
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "ED885042-48CF-6643-80A2-CABA5B77094D";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[23]" -type "float2" 0.0087831412 0.0071673114 ;
	setAttr ".uvtk[60]" -type "float2" 0.010902161 0.013766678 ;
	setAttr ".uvtk[61]" -type "float2" -0.00026687433 -3.1990909e-05 ;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "C14F722A-4168-A861-79A2-98B32838A464";
	setAttr ".ics" -type "componentList" 1 "vtx[14:15]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak27";
	rename -uid "57041C09-4106-965F-D5F3-48922808CC78";
	setAttr ".uopa" yes;
	setAttr ".tk[15]" -type "float3"  0.02878952 0.0011484623 -0.00039893389;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "0AFB0B67-48B0-B995-7414-8AB9989457AE";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[100]" -type "float2" 0.0050824652 -3.2072374e-05 ;
	setAttr ".uvtk[142]" -type "float2" 0.0046465714 -0.0049915696 ;
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "3E8994F7-42C9-1793-5A4E-5D804800F79E";
	setAttr ".ics" -type "componentList" 2 "vtx[51]" "vtx[133]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak28";
	rename -uid "EBD2192B-4542-950F-C488-759CE1DA608A";
	setAttr ".uopa" yes;
	setAttr ".tk[51]" -type "float3"  -0.01904583 0.0062782764 0.00054872036;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "A79B6FD5-4A5B-06EE-E198-73B2146969DF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[50:58]" "e[61]" "e[63:64]" "e[191:226]" "e[228:236]" "e[261]" "e[264]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.9;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel4";
	rename -uid "5CD4145F-4D2E-E541-5B23-F282BB2F500C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[390]" "e[393:394]" "e[397]" "e[496]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.9;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "A1E99ED5-488F-884F-F510-FBB5D90EF11E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[371]" -type "float2" -3.5944581e-12 -3.6429748e-12 ;
	setAttr ".uvtk[385]" -type "float2" -4.9960036e-15 -4.9960036e-15 ;
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "19A845E1-4D38-32F0-8F49-D2A3632BC7BB";
	setAttr ".ics" -type "componentList" 1 "vtx[298:299]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak29";
	rename -uid "FA15DF6F-4B1B-8B92-7A98-498C0EF82533";
	setAttr ".uopa" yes;
	setAttr ".tk[299]" -type "float3"  -0.53068638 0.056388378 0;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "9512944C-4E92-A27E-57C3-6F877C36ACB4";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[371]" -type "float2" 4.3520743e-14 4.3520743e-14 ;
	setAttr ".uvtk[382]" -type "float2" -3.678946e-12 -5.4556915e-12 ;
createNode polyMergeVert -n "polyMergeVert5";
	rename -uid "90B69CD2-47FC-1C1B-4D69-80B1D5D9C1F9";
	setAttr ".ics" -type "componentList" 1 "vtx[297:298]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak30";
	rename -uid "EDBECBBF-4055-4A0C-1EA8-53A03ECED9E6";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[297:298]" -type "float3"  0.11736488 0.15981078 0 0
		 0 0;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "C38E5797-415E-1919-29CF-83B066E265B3";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[354]" -type "float2" -5.6368449e-08 -1.2490009e-15 ;
	setAttr ".uvtk[388]" -type "float2" 2.5252127e-09 1.8081092e-12 ;
createNode polyMergeVert -n "polyMergeVert6";
	rename -uid "81371E40-4B66-1950-FC9F-379CBECC9001";
	setAttr ".ics" -type "componentList" 1 "vtx[311:312]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak31";
	rename -uid "D8B86DDD-40E8-44B0-5764-CDBE8A51C998";
	setAttr ".uopa" yes;
	setAttr ".tk[312]" -type "float3"  0.11736488 0.15981078 0;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "C0C1B46D-4EF7-AF58-5299-ADA3DFFCE6AB";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[353:354]" -type "float2" 3.5944581e-12 9.1449071e-13
		 -3.7747583e-15 9.6034292e-15;
createNode polyMergeVert -n "polyMergeVert7";
	rename -uid "C560BE8D-4C75-665A-41B4-41A1009298AC";
	setAttr ".ics" -type "componentList" 1 "vtx[310:311]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak32";
	rename -uid "9AFA8C10-4DBE-0B1A-7147-168C10128D51";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[310:311]" -type "float3"  -0.53068638 0.056388378 0
		 0 0 0;
createNode polyDelEdge -n "polyDelEdge8";
	rename -uid "219E6FCC-491B-5839-AB79-28A6A088697F";
	setAttr ".ics" -type "componentList" 2 "e[596]" "e[608]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak33";
	rename -uid "7E7DAB6B-48F3-59DF-3986-E2A5AC759233";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[297]" -type "float3" 0.0495346 -0.032464396 0 ;
	setAttr ".tk[310]" -type "float3" 0.0495346 -0.032464396 0 ;
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "119615E0-48BE-3157-AF2D-3992179F6B69";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[88]" -type "float2" -8.5071949e-12 -2.4250614e-05 ;
	setAttr ".uvtk[219]" -type "float2" 9.094947e-13 9.1493479e-13 ;
	setAttr ".uvtk[252]" -type "float2" -8.5071949e-12 -9.9379394e-12 ;
	setAttr ".uvtk[253]" -type "float2" -8.5071949e-12 -9.9379394e-12 ;
	setAttr ".uvtk[254]" -type "float2" -1.1690426e-11 8.7624352e-13 ;
createNode polyMergeVert -n "polyMergeVert8";
	rename -uid "15DC883A-4248-72AD-BD82-A8ADA289E037";
	setAttr ".ics" -type "componentList" 2 "vtx[188]" "vtx[191]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak34";
	rename -uid "FE8A87EB-44C9-8C18-BBDA-FFB733A7FDC1";
	setAttr ".uopa" yes;
	setAttr -s 75 ".tk";
	setAttr ".tk[0]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[1]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[2]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[5]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[6]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[7]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[8]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[96]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[97]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[98]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[99]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[100]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[107]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[108]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[109]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[152]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[153]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[154]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[155]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[156]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[157]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[158]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[159]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[160]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[179]" -type "float3" 0.077505857 -0.091115735 0 ;
	setAttr ".tk[180]" -type "float3" 0.077505857 -0.091115735 0 ;
	setAttr ".tk[181]" -type "float3" 0.077505857 -0.091115735 0 ;
	setAttr ".tk[182]" -type "float3" 0.077505857 -0.091115735 0 ;
	setAttr ".tk[183]" -type "float3" 0.070921093 -0.094931372 0 ;
	setAttr ".tk[184]" -type "float3" 0.003566806 -0.023635836 0 ;
	setAttr ".tk[185]" -type "float3" 0.003566806 -0.023635836 0 ;
	setAttr ".tk[186]" -type "float3" 0.003566806 -0.023635836 0 ;
	setAttr ".tk[187]" -type "float3" 0.077505857 -0.091115735 0 ;
	setAttr ".tk[188]" -type "float3" 0.070566684 -0.095243655 0 ;
	setAttr ".tk[189]" -type "float3" 0.077505857 -0.091115735 0 ;
	setAttr ".tk[190]" -type "float3" 0.077505857 -0.091115735 0 ;
	setAttr ".tk[191]" -type "float3" 0.070921063 -0.094931297 0 ;
	setAttr ".tk[192]" -type "float3" 0.003566806 -0.023635836 0 ;
	setAttr ".tk[193]" -type "float3" 0.003566806 -0.023635836 0 ;
	setAttr ".tk[194]" -type "float3" 0.003566806 -0.023635836 0 ;
	setAttr ".tk[204]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[205]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[206]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[207]" -type "float3" -0.062305436 -0.039632365 0 ;
	setAttr ".tk[208]" -type "float3" -0.062305436 -0.039632365 0 ;
	setAttr ".tk[209]" -type "float3" -0.062305436 -0.039632365 0 ;
	setAttr ".tk[210]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[211]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[212]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[213]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[214]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[215]" -type "float3" -0.068156689 -0.00085837801 0 ;
	setAttr ".tk[216]" -type "float3" 0.17913426 -0.099740431 0 ;
	setAttr ".tk[217]" -type "float3" 0.17913426 -0.099740431 0 ;
	setAttr ".tk[218]" -type "float3" 0.17913426 -0.099740431 0 ;
	setAttr ".tk[219]" -type "float3" 0.17913426 -0.099740431 0 ;
	setAttr ".tk[220]" -type "float3" 0.17913426 -0.099740431 0 ;
	setAttr ".tk[221]" -type "float3" 0.17913426 -0.099740423 0 ;
	setAttr ".tk[222]" -type "float3" -0.062305436 -0.039632365 0 ;
	setAttr ".tk[223]" -type "float3" -0.062305436 -0.039632365 0 ;
	setAttr ".tk[224]" -type "float3" -0.062305436 -0.039632365 0 ;
	setAttr ".tk[297]" -type "float3" 0.19846952 -0.071900912 0 ;
	setAttr ".tk[298]" -type "float3" 0.17913426 -0.099740431 0 ;
	setAttr ".tk[299]" -type "float3" 0.19846952 -0.071900912 0 ;
	setAttr ".tk[300]" -type "float3" 0.23549673 -0.055453368 0 ;
	setAttr ".tk[301]" -type "float3" 0.23549673 -0.055453368 0 ;
	setAttr ".tk[302]" -type "float3" 0.19846952 -0.071900912 0 ;
	setAttr ".tk[303]" -type "float3" 0.17913426 -0.099740431 0 ;
	setAttr ".tk[304]" -type "float3" 0.17913426 -0.099740423 0 ;
	setAttr ".tk[305]" -type "float3" 0.19846952 -0.071900912 0 ;
	setAttr ".tk[306]" -type "float3" 0.23549673 -0.055453368 0 ;
	setAttr ".tk[307]" -type "float3" 0.17913426 -0.099740423 0 ;
	setAttr ".tk[308]" -type "float3" 0.19846952 -0.071900912 0 ;
	setAttr ".tk[309]" -type "float3" 0.23549673 -0.055453368 0 ;
	setAttr ".tk[310]" -type "float3" 0.19846952 -0.071900912 0 ;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "6804E0EF-4DD5-8528-2B7A-95A3AB95C48E";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[88]" -type "float2" 5.2017279e-12 -6.9359835e-06 ;
	setAttr ".uvtk[92]" -type "float2" -9.094947e-13 -1.6976735e-05 ;
	setAttr ".uvtk[199]" -type "float2" -1.0041967e-13 -1.5103474e-12 ;
	setAttr ".uvtk[218]" -type "float2" -1.3655743e-14 -9.1632257e-13 ;
	setAttr ".uvtk[251]" -type "float2" -1.1690426e-11 8.7624352e-13 ;
	setAttr ".uvtk[252]" -type "float2" -1.1690426e-11 8.7624352e-13 ;
createNode polyMergeVert -n "polyMergeVert9";
	rename -uid "316A0CBD-4569-CA99-AC33-7AAA400DB627";
	setAttr ".ics" -type "componentList" 1 "vtx[188:189]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak35";
	rename -uid "21A7EA91-41D9-990B-298E-74AA86D46517";
	setAttr ".uopa" yes;
	setAttr ".tk[189]" -type "float3"  0.010703802 0.0037062764 -0.00040686131;
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "6982F649-43B7-0D83-B636-D38628C192F9";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[84]" -type "float2" -8.5064178e-12 7.4490039e-05 ;
	setAttr ".uvtk[222]" -type "float2" 9.0871755e-13 4.5707882e-13 ;
	setAttr ".uvtk[246]" -type "float2" 0 1.1034507e-12 ;
	setAttr ".uvtk[247]" -type "float2" 0 1.1034507e-12 ;
	setAttr ".uvtk[249]" -type "float2" -2.6612046e-12 4.8080984e-12 ;
createNode polyMergeVert -n "polyMergeVert10";
	rename -uid "6C07C0B4-430F-1A11-DA58-809A0A1B3B1E";
	setAttr ".ics" -type "componentList" 1 "vtx[182:183]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak36";
	rename -uid "5FE9AE72-48BB-EF42-75E6-0AA329C1DE14";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[182:183]" -type "float3"  -0.0069394112 -0.0041296482
		 -0.00040957332 0 0 0;
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "5F8DF356-428C-9237-63AA-F99BA7FFFDF3";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[84]" -type "float2" -2.862266e-12 2.1305106e-05 ;
	setAttr ".uvtk[85]" -type "float2" -9.0871755e-13 5.2111463e-05 ;
	setAttr ".uvtk[190]" -type "float2" -4.6018744e-14 1.8152146e-13 ;
	setAttr ".uvtk[221]" -type "float2" -9.0472074e-13 -4.5508042e-13 ;
	setAttr ".uvtk[245]" -type "float2" -2.6612046e-12 4.8080984e-12 ;
	setAttr ".uvtk[246]" -type "float2" -2.6612046e-12 4.8080984e-12 ;
createNode polyMergeVert -n "polyMergeVert11";
	rename -uid "19C63B62-4036-55F5-0255-B8A845C36703";
	setAttr ".ics" -type "componentList" 1 "vtx[181:182]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak37";
	rename -uid "148D8447-4040-C5B5-CCAF-678B9B2016E9";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[181:182]" -type "float3"  0.010699749 0.0037043095 1.4901161e-06
		 0 0 0;
createNode polyAutoProj -n "polyAutoProj1";
	rename -uid "DE10147E-439A-18E0-4CFD-588708A4EDAA";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:134]";
	setAttr ".ix" -type "matrix" 0.13406708957084318 0 0 0 0 0.08487901497544191 0 0
		 0 0 0.13406708957084318 0 0 3.8793965182753949 0.02438273788720402 1;
	setAttr ".s" -type "double3" 5.0866950318621189 5.0866950318621189 5.0866950318621189 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj2";
	rename -uid "E3AE0A47-4795-BD88-9AD3-C2B8D8593CDB";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:159]";
	setAttr ".ix" -type "matrix" 0.15733468234586742 0.11509254014598828 0 0 -0.45846189578008734 0.62673007867190145 0 0
		 0 0 0.19493715670987644 0 -2.5621445883709391 1.2774935472529712 -0.02616534212497823 1;
	setAttr ".s" -type "double3" 5.0866950318621189 5.0866950318621189 5.0866950318621189 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak38";
	rename -uid "846C7B07-4918-0125-8EC5-9A89B4D15AE5";
	setAttr ".uopa" yes;
	setAttr -s 176 ".tk";
	setAttr ".tk[0:165]" -type "float3"  3.41197753 0.089687206 -0.28229067 3.37916517
		 0.018862749 -0.52160525 3.33005667 -0.087133393 -0.68151051 3.27213264 -0.21216439
		 -0.73766178 3.21420836 -0.33719519 -0.68151075 3.16510105 -0.44319123 -0.52160585
		 3.13228965 -0.51401579 -0.28229117 3.12076735 -0.53888583 -5.022269e-07 3.13228941
		 -0.51401591 0.28229028 3.16510105 -0.44319135 0.52160496 3.21420836 -0.33719531 0.68151009
		 3.27213287 -0.21216451 0.73766142 3.33005691 -0.087133564 0.68151021 3.37916517 0.018862749
		 0.52160519 3.41197729 0.089687146 0.28229049 3.42349958 0.11455719 -2.3841858e-07
		 2.15059185 -0.024922768 -0.11141941 2.11088371 -0.05305006 -0.20587692 2.051454544
		 -0.095145717 -0.26899111 1.98135245 -0.1448009 -0.29115322 1.91125095 -0.19445635
		 -0.26899123 1.85182238 -0.23655184 -0.20587665 1.81211281 -0.2646794 -0.11141988
		 1.79816937 -0.27455628 -1.1978254e-07 1.81211281 -0.26467943 0.11141944 1.85182285
		 -0.23655182 0.20587695 1.91125059 -0.1944564 0.26899111 1.98135221 -0.14480092 0.29115322
		 2.051454544 -0.095145814 0.26899117 2.11088347 -0.053050101 0.20587689 2.15059209
		 -0.02492276 0.11141964 2.16453743 -0.015045505 -1.5657729e-08 1.51960301 -0.056307729
		 -0.063299641 1.50811291 -0.056307722 -0.11696254 1.49091649 -0.056307729 -0.15281889
		 1.47063208 -0.056307737 -0.1654101 1.45034695 -0.056307737 -0.15281898 1.43315029
		 -0.056307729 -0.11696267 1.42165971 -0.0563077 -0.063299783 1.41762519 -0.056307722
		 -6.4084844e-08 1.42165971 -0.056307729 0.063299619 1.43315005 -0.056307737 0.11696261
		 1.45034695 -0.056307722 0.15281895 2.057570696 -0.069107875 0.1654101 1.49091649
		 -0.056307737 0.15281898 1.50811291 -0.056307729 0.11696264 1.51960301 -0.056307722
		 0.063299678 1.52363777 -0.056307722 -4.9296052e-09 1.19450545 -0.012800172 0 1.19450569
		 -0.012800165 0 1.19450569 -0.012800172 0 1.19450545 -0.012800165 0 1.19450569 -0.012800157
		 0 1.19450569 -0.012800165 0 1.19450569 -0.012800179 0 1.19450545 -0.012800165 2.7755576e-17
		 1.19450569 -0.012800172 0 1.19450545 -0.012800172 0 1.19450569 -0.012800165 0 1.19450569
		 -0.012800165 0 1.19450569 -0.012800165 0 1.19450569 -0.012800165 0 1.19450545 -0.012800172
		 0 1.19450593 -0.012800172 2.7755576e-17 1.45436919 -0.012800166 0 1.45436919 -0.012800166
		 0 1.45436919 -0.012800174 0 1.45436871 -0.012800174 0 1.45436919 -0.012800166 0 1.45436919
		 -0.012800166 0 1.45436919 -0.012800181 0 1.45436966 -0.012800181 2.7755576e-17 1.45436919
		 -0.012800181 0 1.45436966 -0.012800181 0 1.45436966 -0.012800166 0 1.45436919 -0.012800159
		 0 1.45436919 -0.012800166 0 1.45436919 -0.012800166 0 1.45436919 -0.012800166 0 1.45436919
		 -0.012800166 2.7755576e-17 1.71412063 -0.012800165 0 1.71412063 -0.012800165 0 1.71412134
		 -0.012800165 0 1.71412086 -0.012800165 0 1.71412086 -0.012800142 0 1.71412015 -0.012800165
		 0 1.71412086 -0.012800179 0 1.71412039 -0.01280015 2.7755576e-17 1.7141211 -0.012800179
		 0 1.71412039 -0.012800179 0 1.7141211 -0.012800172 0 1.71412086 -0.012800172 0 1.71412134
		 -0.012800165 0 1.71412063 -0.012800165 0 1.71412063 -0.012800165 0 1.71412063 -0.012800165
		 2.7755576e-17 2.43813562 -0.027120929 0 2.43813562 -0.027120929 0 2.43813562 -0.027120929
		 0 2.43813586 -0.027120929 0 2.4381361 -0.027120914 0 2.43813515 -0.027120929 0 2.43813586
		 -0.027120914 0 2.43813586 -0.027120914 2.7755576e-17 2.43813586 -0.027120929 0 2.43813515
		 -0.027120914 0 2.43813586 -0.027120929 0 2.43813562 -0.027120929 0 2.43813562 -0.027120929
		 0 2.43813562 -0.027120929 0 2.43813562 -0.027120929 0 2.43813586 -0.027120929 2.7755576e-17
		 2.76782537 -0.028336713 0.052089326 2.79732919 -0.028336713 0.096248671 2.84149027
		 -0.028336713 0.12575495 2.89357924 -0.028336713 0.13611622 2.94566727 -0.028336713
		 0.12575504 2.98982835 -0.028336713 0.096248671 3.019333124 -0.028336713 0.052089434
		 3.029695988 -0.028336713 5.273554e-08 3.019333124 -0.028336713 -0.052089378 2.98982835
		 -0.028336713 -0.096248709 2.94566727 -0.028336713 -0.12575495 2.89357972 -0.028336713
		 -0.13611622 2.84149075 -0.028336713 -0.12575501 2.79732919 -0.028336713 -0.096248694
		 2.76782537 -0.028336713 -0.05208946 2.75746298 -0.028336713 4.0565795e-09 2.88012767
		 -0.0020063627 0.022043537 2.94402862 -0.0020063478 0.040731244 3.039661407 -0.0020063627
		 0.05321794 3.15246892 -0.0020063627 0.057602629 3.26527667 -0.0020063627 0.053217955
		 3.36090827 -0.0020063627 0.040731303 3.42481279 -0.0020063627 0.022043606 3.44724941
		 -0.0020063478 1.50898e-08 3.42481136 -0.0020063627 -0.022043586 3.36090875 -0.0020063627
		 -0.040731281 3.26527691 -0.0020063627 -0.05321794 3.15246892 -0.0020063627 -0.05760264
		 3.039661407 -0.0020063627 -0.05321794 2.94402862 -0.0020063627 -0.040731288 2.880126
		 -0.0020063478 -0.022043644 2.85768962 -0.0020063627 -5.5105445e-09 2.67346072 -0.050085988
		 0.13834848 2.75182796 -0.050085988 0.2556349 2.86911225 -0.050085988 0.33400312 3.0074625015
		 -0.050085988 0.36152223 3.14581275 -0.050085988 0.33400324 3.26309657 -0.050085973
		 0.25563517 3.34146476 -0.050085973 0.13834883 3.36898685 -0.050085988 1.538487e-07
		 3.34146476 -0.050085973 -0.13834852 3.26309705 -0.050085973 -0.25563496 3.14581227
		 -0.050085988 -0.33400315 3.0074613094 -0.050085984 -0.36152211 2.86911225 -0.050085988
		 -0.33400315 2.75182819 -0.050085988 -0.25563508 2.67346072 -0.050085988 -0.13834859
		 2.64593816 -0.050085988 2.4558199e-08 2.28645182 -0.077052839 0.15555672 2.37457013
		 -0.077052839 0.28743175 2.50645185 -0.077052839 0.37554803 2.66200471 -0.077052839
		 0.40649015 2.81755781 -0.077052839 0.37554809 2.94943857 -0.077052839 0.28743193;
	setAttr ".tk[166:175]" 3.037556887 -0.077052839 0.1555572 3.068499088 -0.077052839
		 2.4228709e-07 3.037558079 -0.077052839 -0.15555672 2.94943881 -0.077052839 -0.28743172
		 2.81755805 -0.077052839 -0.37554789 2.66200495 -0.077052839 -0.4064897 2.50645208
		 -0.077052839 -0.37554798 2.37457037 -0.077052839 -0.28743184 2.28645086 -0.077052839
		 -0.15555683 2.25550985 -0.077052839 9.6914739e-08;
createNode polyAutoProj -n "polyAutoProj3";
	rename -uid "B3D39DB3-4205-8AC7-89ED-C28A40A86BAF";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:194]";
	setAttr ".ix" -type "matrix" 0.53420226064025833 0 0 0 0 0.21335463110330175 0 0
		 0 0 0.53420226064025833 0 0 3.7756360607064652 0 1;
	setAttr ".s" -type "double3" 5.0866950318621189 5.0866950318621189 5.0866950318621189 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj4";
	rename -uid "78020C11-4C2B-8810-E99B-BE88D0CB2C81";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:44]";
	setAttr ".ix" -type "matrix" -0.032081069965915296 0.37356038849367379 -0.0015840059477259408 0
		 -0.02899921090981453 -0.0024916328314688652 -0.00028384637927339056 0 -0.0037652906023709462 0.0012608708342236922 0.37361346450701755 0
		 -1.5510885800789487 0.87024172234360231 -0.023407792409475617 1;
	setAttr ".s" -type "double3" 5.0866950318621189 5.0866950318621189 5.0866950318621189 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj5";
	rename -uid "047564D8-4077-EBF6-0606-49A30577EFDF";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:124]";
	setAttr ".ix" -type "matrix" 1.2405976456916192 0 0 0 0 1.0925227409519365 0 0 0 0 1.2405976456916192 0
		 0 1.1848285726213428 0 1;
	setAttr ".s" -type "double3" 5.0866950318621189 5.0866950318621189 5.0866950318621189 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyAutoProj -n "polyAutoProj6";
	rename -uid "888F249F-41AC-EC6B-FCCB-68AE75F23F45";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:293]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".s" -type "double3" 5.0866950318621189 5.0866950318621189 5.0866950318621189 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak39";
	rename -uid "9EDE0194-4883-23E6-A002-9A995E0F4CFB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[227]" -type "float3" -0.008869133 0.058772277 0 ;
	setAttr ".tk[235]" -type "float3" -0.008869133 0.058772277 0 ;
	setAttr ".tk[286]" -type "float3" -0.008869133 0.058772277 0 ;
	setAttr ".tk[287]" -type "float3" -0.008869133 0.058772277 0 ;
createNode polyAutoProj -n "polyAutoProj7";
	rename -uid "61A1B469-47EB-8854-B028-13BA9A3A3928";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:129]";
	setAttr ".ix" -type "matrix" 0.83732202676744971 0 0 0 0 0.97164167699359172 0 0
		 0 0 0.83732202676744971 0 0 3.1689992828688789 0 1;
	setAttr ".s" -type "double3" 5.0866950318621189 5.0866950318621189 5.0866950318621189 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "F27100EF-478B-65EA-8847-40B8380B6DB7";
	setAttr ".uopa" yes;
	setAttr -s 487 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0;
	setAttr ".uvtk[250:486]" -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059
		 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0 -1.090581059 0;
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "35D45107-4EA4-4030-40DE-FF83CD8E0F2D";
	setAttr ".uopa" yes;
	setAttr -s 247 ".uvtk[0:246]" -type "float2" 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251835 0 1.032251716
		 0 1.032251716 0 1.032251835 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251835 0 1.032251716
		 0 1.032251716 0 1.032251835 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251835 0 1.032251835 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251835 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251596 0 1.032251596
		 0 1.032251596 0 1.032251596 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251716
		 0 1.032251596 0 1.032251596 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251596 0 1.032251835 0 1.032251835 0 1.032251716 0 1.032251596
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251596
		 0 1.032251835 0 1.032251835 0 1.032251835 0 1.032251835 0 1.032251716 0 1.032251716
		 0 1.032251835 0 1.032251835 0 1.032251596 0 1.032251596 0 1.032251596 0 1.032251596
		 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251716 0 1.032251596 0 1.032251596
		 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251596 0 1.032251596 0 1.032251716
		 0 1.032251716 0 1.032251596 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251835 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251596 0 1.032251716 0 1.032251596 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251835 0 1.032251716 0 1.032251716 0 1.032251596 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251835 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251835 0 1.032251835 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716
		 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251596
		 0 1.032251596 0 1.032251596 0 1.032251596 0 1.032251716 0 1.032251716 0 1.032251596
		 0 1.032251596 0 1.032251716 0 1.032251716 0 1.032251716 0 1.032251716 0;
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "47E49CE6-4B20-0DF5-5012-4382E5CBCC57";
	setAttr ".uopa" yes;
	setAttr -s 259 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016;
	setAttr ".uvtk[250:258]" 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016
		 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016 0 -1.023414016;
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "37C5E0D2-4B99-E804-E337-C2B01BF7A906";
	setAttr ".uopa" yes;
	setAttr -s 289 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.03048408 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842;
	setAttr ".uvtk[250:288]" 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0
		 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.03048408 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842
		 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842 0 1.0304842;
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "36F21521-476E-04E1-AFC8-999D0A55A195";
	setAttr ".uopa" yes;
	setAttr -s 376 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 1.086313248 1.048668861 1.086313248
		 1.048668623 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668742
		 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668861 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623
		 1.086313367 1.048668623 1.086313248 1.048668623 1.086313367 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742
		 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668742 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668623
		 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668623 1.086313248 1.048668861 1.086313367 1.048668623 1.086313248 1.048668742
		 1.086313248 1.048668623 1.086313248 1.048668861 1.086313367 1.048668742 1.086313248
		 1.048668623 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248
		 1.048668742 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248
		 1.048668861 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248
		 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623
		 1.086313248 1.048668623 1.086313248 1.048668742 1.086313128 1.048668742 1.086313128
		 1.048668623 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668861 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668742
		 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668861 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668861
		 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248
		 1.048668861 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668742
		 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668742
		 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248
		 1.048668861 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623
		 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668623 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623
		 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668861 1.086313248 1.048668861 1.086313248 1.048668623
		 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668861 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668623
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668623
		 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668742
		 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623
		 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248
		 1.048668623 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623
		 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861;
	setAttr ".uvtk[250:375]" 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248
		 1.048668861 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668623 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668861
		 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248
		 1.048668861 1.086313248 1.048668742 1.086313248 1.048668623 1.086313248 1.048668623
		 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248
		 1.048668861 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668861
		 1.086313248 1.048668861 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248
		 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668623
		 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248
		 1.048668623 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668623 1.086313248
		 1.048668623 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248 1.048668742
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248
		 1.048668623 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668861 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861 1.086313248 1.048668861
		 1.086313248 1.048668742 1.086313128 1.048668742 1.086313128 1.048668861 1.086313128
		 1.048668861 1.086313128 1.048668742 1.086313248 1.048668742 1.086313248 1.048668861
		 1.086313248 1.048668623 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668623
		 1.086313248 1.048668861 1.086313248 1.048668861 1.086313248 1.048668623 1.086313248
		 1.048668861 1.086313248 1.048668623 1.086313248 1.048668861 1.086313248 1.048668623
		 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248
		 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742 1.086313248 1.048668742
		 1.086313248 1.048668742;
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "5892BA19-43CD-958C-1E34-16ADE5D301B9";
	setAttr ".uopa" yes;
	setAttr -s 230 ".uvtk[0:229]" -type "float2" -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489081 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489081 -1.0083353519 -0.99489081 -1.0083353519 -0.99489081 -1.0083353519
		 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519 -0.99489081 -1.0083353519 -0.99489093
		 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489081 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489081 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489081 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489081 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519 -0.99489081
		 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489093 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489093 -1.0083353519 -0.99489093
		 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489093 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489081
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489093 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489093 -1.0083353519 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489081 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489093 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489093 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519
		 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087 -1.0083353519 -0.99489087
		 -1.0083353519 -0.99489087 -1.0083353519;
createNode polyDelEdge -n "polyDelEdge9";
	rename -uid "DB115AFE-42D8-76A8-166A-B199C5543C13";
	setAttr ".ics" -type "componentList" 5 "e[76]" "e[78]" "e[80]" "e[82]" "e[84]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge10";
	rename -uid "B2E1C72B-4E0A-E677-98BA-9286028A89E7";
	setAttr ".ics" -type "componentList" 5 "e[297]" "e[301]" "e[305]" "e[309]" "e[313]";
	setAttr ".cv" yes;
createNode polySplit -n "polySplit28";
	rename -uid "7A61FBA7-4A04-A7BD-CA2D-A299866B2C2E";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483592 -2147483581;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit29";
	rename -uid "72FD634D-47D6-57AF-66BD-17B8C7DB5E1B";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483594 -2147483583;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit30";
	rename -uid "22286144-46F2-F002-FC6C-DD9DFC1A664F";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483587 -2147483598;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit31";
	rename -uid "56C133DF-4751-8E91-5667-6A970A932BE1";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483585 -2147483596;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit32";
	rename -uid "B9B0B382-4576-567A-9406-DA9B1441FF48";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483579 -2147483590;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge11";
	rename -uid "8F313907-4216-F8F5-7BAA-6ABFE9D07CD6";
	setAttr ".ics" -type "componentList" 5 "e[105]" "e[107]" "e[109]" "e[111]" "e[113]";
	setAttr ".cv" yes;
createNode polyMergeVert -n "polyMergeVert12";
	rename -uid "43D592E6-4C72-6AD5-1B5C-4DA08E07A61F";
	setAttr ".ics" -type "componentList" 1 "vtx[0:131]";
	setAttr ".ix" -type "matrix" 0.13406708957084318 0 0 0 0 0.08487901497544191 0 0
		 0 0 0.13406708957084318 0 0 3.8793965182753949 0.02438273788720402 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert13";
	rename -uid "87F4C914-4763-5951-FA41-5997AD08F6B9";
	setAttr ".ics" -type "componentList" 1 "vtx[0:175]";
	setAttr ".ix" -type "matrix" 0.15733468234586742 0.11509254014598828 0 0 -0.45846189578008734 0.62673007867190145 0 0
		 0 0 0.19493715670987644 0 -2.5621445883709391 1.2774935472529712 -0.02616534212497823 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert14";
	rename -uid "CA6E302F-4B97-1AF6-A57D-43B862F440D7";
	setAttr ".ics" -type "componentList" 1 "vtx[0:191]";
	setAttr ".ix" -type "matrix" 0.53420226064025833 0 0 0 0 0.21335463110330175 0 0
		 0 0 0.53420226064025833 0 0 3.7756360607064652 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert15";
	rename -uid "28357E3D-4A42-4CC7-A42D-3884268FC48D";
	setAttr ".ics" -type "componentList" 1 "vtx[0:41]";
	setAttr ".ix" -type "matrix" -0.032081069965915296 0.37356038849367379 -0.0015840059477259408 0
		 -0.02899921090981453 -0.0024916328314688652 -0.00028384637927339056 0 -0.0037652906023709462 0.0012608708342236922 0.37361346450701755 0
		 -1.5510885800789487 0.87024172234360231 -0.023407792409475617 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert16";
	rename -uid "95F62DB2-4704-6D08-73A2-BC964594F6E4";
	setAttr ".ics" -type "componentList" 1 "vtx[0:130]";
	setAttr ".ix" -type "matrix" 1.2405976456916192 0 0 0 0 1.0925227409519365 0 0 0 0 1.2405976456916192 0
		 0 1.1848285726213428 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert17";
	rename -uid "19D1BA3B-4573-9AED-918B-88BFF32F2ABC";
	setAttr ".ics" -type "componentList" 1 "vtx[0:306]";
	setAttr ".ix" -type "matrix" 0.20200768693109572 -0.0088065776166010544 0 0 0.030484308133501437 0.69925740075633946 0 0
		 0 0 0.229158422855199 0 2.3042659902483558 1.6396349466593279 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert18";
	rename -uid "43656292-4F87-9A3B-0934-FBBC18F41435";
	setAttr ".ics" -type "componentList" 1 "vtx[0:130]";
	setAttr ".ix" -type "matrix" 0.83732202676744971 0 0 0 0 0.97164167699359172 0 0
		 0 0 0.83732202676744971 0 0 3.1689992828688789 0 1;
	setAttr ".am" yes;
createNode polyMapCut -n "polyMapCut1";
	rename -uid "54B523D4-44F0-A8CB-1D04-22ACA5AC9B86";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[191]" "e[207]" "e[223]" "e[239]" "e[255]" "e[271]" "e[287]" "e[303]" "e[319]" "e[335]";
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "4AD322AC-46B3-9EB4-BD4C-A8B06F2B0D87";
	setAttr ".uopa" yes;
	setAttr -s 243 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 0.71902174 0.047719061 ;
	setAttr ".uvtk[1]" -type "float2" 0.06507127 -0.8160798 ;
	setAttr ".uvtk[2]" -type "float2" -0.0055523962 -0.80790633 ;
	setAttr ".uvtk[3]" -type "float2" 0.68143147 0.08528775 ;
	setAttr ".uvtk[4]" -type "float2" 0.73753524 0.00361377 ;
	setAttr ".uvtk[5]" -type "float2" 0.12709934 -0.82325858 ;
	setAttr ".uvtk[6]" -type "float2" 0.07701005 -0.71292263 ;
	setAttr ".uvtk[7]" -type "float2" 0.0074714795 -0.69537324 ;
	setAttr ".uvtk[8]" -type "float2" -0.052957281 -0.80241996 ;
	setAttr ".uvtk[9]" -type "float2" 0.68141758 0.12982327 ;
	setAttr ".uvtk[10]" -type "float2" 0.66723394 0.0080464482 ;
	setAttr ".uvtk[11]" -type "float2" 0.62493706 0.042009532 ;
	setAttr ".uvtk[12]" -type "float2" 0.14091927 -0.70384723 ;
	setAttr ".uvtk[13]" -type "float2" 0.67758751 -0.04230994 ;
	setAttr ".uvtk[14]" -type "float2" 0.087131396 -0.62546903 ;
	setAttr ".uvtk[15]" -type "float2" 0.018512584 -0.59997243 ;
	setAttr ".uvtk[16]" -type "float2" -0.044133574 -0.72617859 ;
	setAttr ".uvtk[17]" -type "float2" 0.63263041 -0.018462121 ;
	setAttr ".uvtk[18]" -type "float2" 0.58718854 0.013091743 ;
	setAttr ".uvtk[19]" -type "float2" 0.15263528 -0.60261494 ;
	setAttr ".uvtk[20]" -type "float2" 0.093894206 -0.56703502 ;
	setAttr ".uvtk[21]" -type "float2" 0.02589006 -0.53622729 ;
	setAttr ".uvtk[22]" -type "float2" 0.024347275 -0.74945599 ;
	setAttr ".uvtk[23]" -type "float2" -0.015545771 -0.68290704 ;
	setAttr ".uvtk[24]" -type "float2" -0.063789979 -0.677414 ;
	setAttr ".uvtk[25]" -type "float2" -0.036762193 -0.72250134 ;
	setAttr ".uvtk[26]" -type "float2" 0.066368565 -0.70783991 ;
	setAttr ".uvtk[27]" -type "float2" 0.029798821 -0.64683455 ;
	setAttr ".uvtk[28]" -type "float2" -0.062602825 -0.60440701 ;
	setAttr ".uvtk[29]" -type "float2" -0.095671251 -0.62422997 ;
	setAttr ".uvtk[30]" -type "float2" -0.1096601 -0.52590674 ;
	setAttr ".uvtk[31]" -type "float2" -0.12755273 -0.5710457 ;
	setAttr ".uvtk[32]" -type "float2" -0.14955315 -0.45935756 ;
	setAttr ".uvtk[33]" -type "float2" -0.15458052 -0.52595824 ;
	setAttr ".uvtk[34]" -type "float2" -0.05647441 -0.50291473 ;
	setAttr ".uvtk[35]" -type "float2" -0.093043908 -0.44190985 ;
	setAttr ".uvtk[36]" -type "float2" -0.088623762 -0.35135627 ;
	setAttr ".uvtk[37]" -type "float2" -0.021866739 -0.33639687 ;
	setAttr ".uvtk[38]" -type "float2" -0.028772503 -0.19152945 ;
	setAttr ".uvtk[39]" -type "float2" -0.079913557 -0.21037513 ;
	setAttr ".uvtk[40]" -type "float2" 0.027215153 -0.30389839 ;
	setAttr ".uvtk[41]" -type "float2" 0.0099984407 -0.15581232 ;
	setAttr ".uvtk[42]" -type "float2" -0.02540046 -0.12400252 ;
	setAttr ".uvtk[43]" -type "float2" -0.071490765 -0.14066035 ;
	setAttr ".uvtk[44]" -type "float2" 0.29042852 0.5662812 ;
	setAttr ".uvtk[45]" -type "float2" -0.13563859 -0.20948106 ;
	setAttr ".uvtk[46]" -type "float2" 0.051149219 -0.25880855 ;
	setAttr ".uvtk[47]" -type "float2" 0.030497164 -0.10866135 ;
	setAttr ".uvtk[48]" -type "float2" 0.0094897449 -0.092291653 ;
	setAttr ".uvtk[49]" -type "float2" -0.031340927 -0.087339222 ;
	setAttr ".uvtk[50]" -type "float2" -0.071341932 -0.10390824 ;
	setAttr ".uvtk[51]" -type "float2" -0.12176412 -0.13972908 ;
	setAttr ".uvtk[52]" -type "float2" 0.27457231 0.55348682 ;
	setAttr ".uvtk[53]" -type "float2" 0.25039959 0.58366632 ;
	setAttr ".uvtk[54]" -type "float2" 0.027868271 -0.050355375 ;
	setAttr ".uvtk[55]" -type "float2" -0.00072535872 -0.056723654 ;
	setAttr ".uvtk[56]" -type "float2" -0.11463901 -0.10390836 ;
	setAttr ".uvtk[57]" -type "float2" 0.23752518 0.5979414 ;
	setAttr ".uvtk[58]" -type "float2" 0.2572735 0.54631186 ;
	setAttr ".uvtk[59]" -type "float2" 0.23598315 0.57982266 ;
	setAttr ".uvtk[60]" -type "float2" 0.0158436 -0.0167225 ;
	setAttr ".uvtk[61]" -type "float2" 0.23169859 0.60614324 ;
	setAttr ".uvtk[62]" -type "float2" 0.224584 0.5944041 ;
	setAttr ".uvtk[63]" -type "float2" 0.24116577 0.54584825 ;
	setAttr ".uvtk[64]" -type "float2" 0.22167571 0.58170485 ;
	setAttr ".uvtk[65]" -type "float2" 0.22013421 0.60354614 ;
	setAttr ".uvtk[66]" -type "float2" 0.21177663 0.59600139 ;
	setAttr ".uvtk[67]" -type "float2" 0.22870149 0.55216682 ;
	setAttr ".uvtk[68]" -type "float2" 0.20965548 0.58902633 ;
	setAttr ".uvtk[69]" -type "float2" 0.20845623 0.60557199 ;
	setAttr ".uvtk[70]" -type "float2" 0.21396609 0.59019637 ;
	setAttr ".uvtk[71]" -type "float2" 0.19844247 0.61191273 ;
	setAttr ".uvtk[72]" -type "float2" -0.4235228 -0.21570289 ;
	setAttr ".uvtk[73]" -type "float2" -0.38748455 -0.21069372 ;
	setAttr ".uvtk[74]" -type "float2" -0.35635665 -0.13856101 ;
	setAttr ".uvtk[75]" -type "float2" -0.38428783 -0.14631355 ;
	setAttr ".uvtk[76]" -type "float2" -0.34833109 -0.21651649 ;
	setAttr ".uvtk[77]" -type "float2" -0.32685918 -0.14147258 ;
	setAttr ".uvtk[78]" -type "float2" -0.34728748 -0.1171881 ;
	setAttr ".uvtk[79]" -type "float2" -0.37244582 -0.12400162 ;
	setAttr ".uvtk[80]" -type "float2" -0.45095965 -0.23078179 ;
	setAttr ".uvtk[81]" -type "float2" -0.40640026 -0.1635493 ;
	setAttr ".uvtk[82]" -type "float2" -0.3120234 -0.23228502 ;
	setAttr ".uvtk[83]" -type "float2" -0.300286 -0.15460432 ;
	setAttr ".uvtk[84]" -type "float2" -0.32068115 -0.11988044 ;
	setAttr ".uvtk[85]" -type "float2" -0.34000808 -0.10532713 ;
	setAttr ".uvtk[86]" -type "float2" -0.36193502 -0.11235988 ;
	setAttr ".uvtk[87]" -type "float2" -0.28408888 -0.25559843 ;
	setAttr ".uvtk[88]" -type "float2" -0.28068262 -0.17595756 ;
	setAttr ".uvtk[89]" -type "float2" -0.29667735 -0.13166893 ;
	setAttr ".uvtk[90]" -type "float2" -0.31705883 -0.10722065 ;
	setAttr ".uvtk[91]" -type "float2" -0.26878044 -0.28290772 ;
	setAttr ".uvtk[92]" -type "float2" -0.27103364 -0.202281 ;
	setAttr ".uvtk[93]" -type "float2" -0.27893063 -0.15075862 ;
	setAttr ".uvtk[94]" -type "float2" -0.29658124 -0.11775243 ;
	setAttr ".uvtk[95]" -type "float2" -0.28169274 -0.13531899 ;
	setAttr ".uvtk[96]" -type "float2" 0.098650604 -0.41324282 ;
	setAttr ".uvtk[97]" -type "float2" 0.13865194 -0.31201053 ;
	setAttr ".uvtk[98]" -type "float2" -0.058773607 -0.35465527 ;
	setAttr ".uvtk[99]" -type "float2" -0.098774731 -0.45588768 ;
	setAttr ".uvtk[100]" -type "float2" 0.16948 -0.39794362 ;
	setAttr ".uvtk[101]" -type "float2" 0.20948121 -0.29671121 ;
	setAttr ".uvtk[102]" -type "float2" 0.21434772 -0.2337923 ;
	setAttr ".uvtk[103]" -type "float2" 0.016922474 -0.27643692 ;
	setAttr ".uvtk[104]" -type "float2" 0.10043436 -0.52207661 ;
	setAttr ".uvtk[105]" -type "float2" -0.09699133 -0.56472147 ;
	setAttr ".uvtk[106]" -type "float2" 0.24033999 -0.38263738 ;
	setAttr ".uvtk[107]" -type "float2" 0.28034115 -0.28140509 ;
	setAttr ".uvtk[108]" -type "float2" 0.28517732 -0.21849287 ;
	setAttr ".uvtk[109]" -type "float2" 0.17126372 -0.50677717 ;
	setAttr ".uvtk[110]" -type "float2" 0.31421509 -0.19049525 ;
	setAttr ".uvtk[111]" -type "float2" 0.11678962 -0.23313987 ;
	setAttr ".uvtk[112]" -type "float2" -0.085657761 -0.33448577 ;
	setAttr ".uvtk[113]" -type "float2" -0.15104994 -0.40205741 ;
	setAttr ".uvtk[114]" -type "float2" 0.5360539 0.15537971 ;
	setAttr ".uvtk[115]" -type "float2" 0.49784112 0.25157326 ;
	setAttr ".uvtk[116]" -type "float2" 0.35603738 -0.20318663 ;
	setAttr ".uvtk[117]" -type "float2" 0.24212354 -0.49147117 ;
	setAttr ".uvtk[118]" -type "float2" 0.54976332 0.12086874 ;
	setAttr ".uvtk[119]" -type "float2" 0.50439155 0.10945612 ;
	setAttr ".uvtk[120]" -type "float2" 0.46617877 0.20564967 ;
	setAttr ".uvtk[121]" -type "float2" 0.56347865 0.086342871 ;
	setAttr ".uvtk[122]" -type "float2" 0.51810098 0.074945271 ;
	setAttr ".uvtk[123]" -type "float2" 0.49271357 0.054911911 ;
	setAttr ".uvtk[124]" -type "float2" 0.45450091 0.15110546 ;
	setAttr ".uvtk[125]" -type "float2" 0.5318163 0.0404194 ;
	setAttr ".uvtk[126]" -type "float2" 0.50642306 0.020400822 ;
	setAttr ".uvtk[127]" -type "float2" 0.46000886 0.25845271 ;
	setAttr ".uvtk[128]" -type "float2" 0.4499203 0.21133298 ;
	setAttr ".uvtk[129]" -type "float2" 0.50279796 5.0485134e-05 ;
	setAttr ".uvtk[130]" -type "float2" 0.46458519 0.096244156 ;
	setAttr ".uvtk[131]" -type "float2" 0.52013844 -0.014124811 ;
	setAttr ".uvtk[192]" -type "float2" 0.50733542 1.0587482 ;
	setAttr ".uvtk[193]" -type "float2" 0.45472687 0.97799182 ;
	setAttr ".uvtk[194]" -type "float2" 0.53422683 1.0029058 ;
	setAttr ".uvtk[195]" -type "float2" 0.47972488 0.92608333 ;
	setAttr ".uvtk[196]" -type "float2" 0.60457379 1.0169266 ;
	setAttr ".uvtk[197]" -type "float2" 0.55838239 1.0883322 ;
	setAttr ".uvtk[198]" -type "float2" 0.53770131 0.94102311 ;
	setAttr ".uvtk[199]" -type "float2" 0.48295587 0.86856103 ;
	setAttr ".uvtk[200]" -type "float2" 0.61942875 0.94207609 ;
	setAttr ".uvtk[201]" -type "float2" 0.51723039 0.88252187 ;
	setAttr ".uvtk[202]" -type "float2" 0.46392578 0.81417847 ;
	setAttr ".uvtk[203]" -type "float2" 0.60068655 0.87517726 ;
	setAttr ".uvtk[204]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[205]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[206]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[207]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[208]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[209]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[210]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[211]" -type "float2" -0.35364199 -0.7570641 ;
	setAttr ".uvtk[212]" -type "float2" -0.074734546 -0.69729346 ;
	setAttr ".uvtk[213]" -type "float2" -0.063016497 -0.69243962 ;
	setAttr ".uvtk[214]" -type "float2" -0.059138469 -0.67743498 ;
	setAttr ".uvtk[215]" -type "float2" -0.070031308 -0.68194669 ;
	setAttr ".uvtk[216]" -type "float2" -0.050332896 -0.69243985 ;
	setAttr ".uvtk[217]" -type "float2" -0.047348171 -0.67743498 ;
	setAttr ".uvtk[218]" -type "float2" -0.083703302 -0.70626217 ;
	setAttr ".uvtk[219]" -type "float2" -0.078368209 -0.69028336 ;
	setAttr ".uvtk[220]" -type "float2" -0.038614757 -0.69729358 ;
	setAttr ".uvtk[221]" -type "float2" -0.03645537 -0.68194705 ;
	setAttr ".uvtk[222]" -type "float2" -0.02964624 -0.70626229 ;
	setAttr ".uvtk[223]" -type "float2" -0.028118454 -0.69028395 ;
	setAttr ".uvtk[224]" -type "float2" -0.024792574 -0.71798044 ;
	setAttr ".uvtk[225]" -type "float2" -0.023606502 -0.70117646 ;
	setAttr ".uvtk[226]" -type "float2" -0.029644072 -0.3182466 ;
	setAttr ".uvtk[227]" -type "float2" -0.068058431 -0.30734587 ;
	setAttr ".uvtk[228]" -type "float2" -0.1816572 -0.3372277 ;
	setAttr ".uvtk[229]" -type "float2" -0.14218101 -0.34155703 ;
	setAttr ".uvtk[230]" -type "float2" -0.22184134 -0.36670923 ;
	setAttr ".uvtk[231]" -type "float2" -0.18670845 -0.36670935 ;
	setAttr ".uvtk[232]" -type "float2" -0.21512353 -0.33355772 ;
	setAttr ".uvtk[233]" -type "float2" -0.25162554 -0.36670923 ;
	setAttr ".uvtk[234]" -type "float2" -0.24414691 -0.38793957 ;
	setAttr ".uvtk[235]" -type "float2" -0.21078268 -0.38793969 ;
	setAttr ".uvtk[236]" -type "float2" -0.27243194 -0.38793981 ;
	setAttr ".uvtk[237]" -type "float2" -0.22149059 -0.40537679 ;
	setAttr ".uvtk[238]" -type "float2" -0.18812627 -0.40537667 ;
	setAttr ".uvtk[239]" -type "float2" -0.19884409 -0.42281377 ;
	setAttr ".uvtk[240]" -type "float2" -0.16547966 -0.42281377 ;
	setAttr ".uvtk[241]" -type "float2" -0.17741811 -0.38793969 ;
	setAttr ".uvtk[242]" -type "float2" -0.15476191 -0.40537679 ;
	setAttr ".uvtk[243]" -type "float2" -0.13211519 -0.42281365 ;
	setAttr ".uvtk[244]" -type "float2" 0.4678826 0.046382606 ;
	setAttr ".uvtk[245]" -type "float2" 0.51941592 0.12759572 ;
	setAttr ".uvtk[246]" -type "float2" 0.27982041 -0.011060894 ;
	setAttr ".uvtk[247]" -type "float2" 0.21583682 -0.082169712 ;
	setAttr ".uvtk[248]" -type "float2" 0.25319925 0.042786658 ;
	setAttr ".uvtk[249]" -type "float2" 0.096340626 -0.093664587 ;
	setAttr ".uvtk[250]" -type "float2" 0.082151562 -0.03777796 ;
	setAttr ".uvtk[251]" -type "float2" 0.022204399 -0.091303408 ;
	setAttr ".uvtk[252]" -type "float2" 0.16159493 -0.1424523 ;
	setAttr ".uvtk[253]" -type "float2" 0.042826205 -0.14144617 ;
	setAttr ".uvtk[254]" -type "float2" 0.094885439 0.029898226 ;
	setAttr ".uvtk[255]" -type "float2" 0.034938544 -0.023626626 ;
	setAttr ".uvtk[256]" -type "float2" -0.028616369 -0.13667971 ;
	setAttr ".uvtk[257]" -type "float2" 0.14209884 0.015747249 ;
	setAttr ".uvtk[258]" -type "float2" 0.15483257 0.083423316 ;
	setAttr ".uvtk[259]" -type "float2" 0.1076017 0.097559154 ;
	setAttr ".uvtk[260]" -type "float2" 0.047654301 0.044033825 ;
	setAttr ".uvtk[261]" -type "float2" 0.16754884 0.15108448 ;
	setAttr ".uvtk[262]" -type "float2" 0.049075209 -0.60982913 ;
	setAttr ".uvtk[263]" -type "float2" 0.099008761 -0.62639815 ;
	setAttr ".uvtk[264]" -type "float2" 0.048188113 -0.58074099 ;
	setAttr ".uvtk[265]" -type "float2" 0.005051367 -0.56642729 ;
	setAttr ".uvtk[266]" -type "float2" 0.14894232 -0.64296728 ;
	setAttr ".uvtk[267]" -type "float2" 0.091325097 -0.59505481 ;
	setAttr ".uvtk[268]" -type "float2" 0.024207018 -0.53999978 ;
	setAttr ".uvtk[269]" -type "float2" -0.011007093 -0.52831501 ;
	setAttr ".uvtk[270]" -type "float2" 0.05942113 -0.55168468 ;
	setAttr ".uvtk[271]" -type "float2" 0.28493705 -0.13844275 ;
	setAttr ".uvtk[272]" -type "float2" 0.32478634 -0.043580532 ;
	setAttr ".uvtk[273]" -type "float2" 0.31751212 0.077105999 ;
	setAttr ".uvtk[274]" -type "float2" 0.28308669 -0.0048447847 ;
	setAttr ".uvtk[275]" -type "float2" 0.28204337 0.14979792 ;
	setAttr ".uvtk[276]" -type "float2" 0.25394073 0.082898855 ;
	setAttr ".uvtk[277]" -type "float2" 0.24866125 -0.086795449 ;
	setAttr ".uvtk[278]" -type "float2" 0.22583827 0.01600039 ;
	setAttr ".uvtk[279]" -type "float2" -0.24175882 -0.77002442 ;
	setAttr ".uvtk[280]" -type "float2" -0.23541719 -0.77002442 ;
	setAttr ".uvtk[281]" -type "float2" -0.2268973 -0.76553333 ;
	setAttr ".uvtk[282]" -type "float2" -0.23279245 -0.76553333 ;
	setAttr ".uvtk[283]" -type "float2" -0.24810047 -0.77002442 ;
	setAttr ".uvtk[284]" -type "float2" -0.23868765 -0.76553333 ;
	setAttr ".uvtk[285]" -type "float2" -0.40969819 -0.72063464 ;
	setAttr ".uvtk[286]" -type "float2" -0.41603988 -0.72063488 ;
	setAttr ".uvtk[287]" -type "float2" -0.40707344 -0.72512573 ;
	setAttr ".uvtk[288]" -type "float2" -0.40117821 -0.72512561 ;
	setAttr ".uvtk[289]" -type "float2" 0.72656643 0.16441017 ;
	setAttr ".uvtk[290]" -type "float2" 0.46112072 1.1000478 ;
	setAttr ".uvtk[291]" -type "float2" 0.41176742 1.0163836 ;
	setAttr ".uvtk[292]" -type "float2" 0.74807143 0.13633817 ;
	setAttr ".uvtk[293]" -type "float2" 0.78010964 0.094516218 ;
	setAttr ".uvtk[294]" -type "float2" 0.54466742 0.28188437 ;
	setAttr ".uvtk[295]" -type "float2" 0.8082484 0.057784498 ;
	setAttr ".uvtk[296]" -type "float2" 0.58288026 0.1856907 ;
	setAttr ".uvtk[297]" -type "float2" 0.59658968 0.15117985 ;
	setAttr ".uvtk[298]" -type "float2" 0.24138869 0.61296821 ;
	setAttr ".uvtk[299]" -type "float2" 0.61030489 0.1166541 ;
	setAttr ".uvtk[300]" -type "float2" 0.24862994 0.60607445 ;
	setAttr ".uvtk[301]" -type "float2" 0.2627303 0.59265053 ;
	setAttr ".uvtk[302]" -type "float2" -0.162893 -0.34649903 ;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "89A3AFF6-4072-FDD8-74C9-CBADA7A13CE0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 46 "e[0:15]" "e[19:20]" "e[25:26]" "e[48:63]" "e[82:83]" "e[90:91]" "e[96:99]" "e[107:111]" "e[113]" "e[116:117]" "e[120:124]" "e[128:143]" "e[146:154]" "e[158:175]" "e[179:181]" "e[185:187]" "e[195]" "e[197]" "e[201]" "e[203]" "e[211]" "e[213]" "e[217]" "e[219]" "e[226]" "e[228]" "e[234]" "e[236]" "e[242]" "e[244]" "e[250]" "e[252]" "e[258:260]" "e[267:268]" "e[273:274]" "e[276]" "e[283:285]" "e[289:290]" "e[292:294]" "e[296:301]" "e[304]" "e[306:315]" "e[318]" "e[320]" "e[322:331]" "e[334]";
createNode deleteComponent -n "deleteComponent8";
	rename -uid "90C623D9-4C5E-209D-7B75-7A9105606C80";
	setAttr ".dc" -type "componentList" 2 "f[0:9]" "f[30:34]";
createNode polyMapCut -n "polyMapCut2";
	rename -uid "EF128BCC-493D-110B-46F1-818BF738645B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[69]" "e[79]" "e[89]" "e[99]" "e[109]" "e[131]" "e[133]" "e[136]" "e[156]" "e[176]" "e[196]" "e[216]";
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "9D681F41-4C04-38AD-A64A-0BADF487E76E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 45 "e[0:9]" "e[13:14]" "e[18:19]" "e[21]" "e[26]" "e[31]" "e[36]" "e[40:42]" "e[45:60]" "e[63:65]" "e[68]" "e[70]" "e[73]" "e[75]" "e[78]" "e[81:82]" "e[86:87]" "e[91:92]" "e[96:97]" "e[100]" "e[103]" "e[105]" "e[108]" "e[110:130]" "e[132]" "e[134]" "e[138:139]" "e[143:149]" "e[152:154]" "e[158:159]" "e[163:169]" "e[172:174]" "e[178:179]" "e[183:184]" "e[188]" "e[193]" "e[198:199]" "e[203:204]" "e[208]" "e[213]" "e[215]" "e[217]" "e[220]" "e[222]" "e[225:226]";
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "0C06FD50-4AE2-C63F-D683-F98D1B5D5CF8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "f[11]" "f[16]" "f[42]" "f[45]" "f[108]" "f[113]";
	setAttr ".ix" -type "matrix" 1.2405976456916192 0 0 0 0 1.0925227409519365 0 0 0 0 1.2405976456916192 0
		 0 1.1848285726213428 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 1.613218904;
	setAttr ".pv" 0.814530015;
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "C443C6FD-4D55-DC87-5198-AD96704A2C4B";
	setAttr ".uopa" yes;
	setAttr -s 187 ".uvtk[0:186]" -type "float2" 0.17350791 -1.71959603 0.5052675
		 -1.24940944 0.48764318 -1.27463615 0.17181745 -1.70367968 0.17610008 -1.73210859
		 0.45187187 -1.11578989 0.45032525 -1.24560213 0.45518398 -1.36271 0.4553611 -1.38819635
		 0.16053598 -1.72463882 0.18023497 -1.70430183 0.19016019 -1.67171943 0.42255336 -1.13900185
		 0.18911466 -1.69257617 0.40270391 -1.15454268 0.42773074 -1.33610153 0.43857163 -1.3681519
		 0.21681409 -1.6593883 0.23368074 -1.64480913 0.38871363 -1.1610949 0.39159957 -1.19694149
		 0.41762558 -1.29613495 0.36452839 -1.33991718 0.34989542 -1.367751 0.34640568 -1.38248575
		 0.3396264 -1.35977221 0.3563799 -1.35269451 0.34344643 -1.37965894 0.34050339 -1.40194631
		 0.3426719 -1.40575027 0.33825275 -1.43645978 0.33362761 -1.42672956 0.31785104 -1.49044645
		 0.31430891 -1.45528996 0.33503816 -1.44837487 0.3429895 -1.48024511 0.043058682 -0.93722636
		 0.0056268289 -1.015149713 0.15008122 -1.12057376 0.17116491 -1.056373358 0.0025588688
		 -1.091887474 0.15702821 -1.18777227 0.20407221 -1.16762865 0.22474343 -1.11260629
		 -0.20274264 -1.82482636 0.21633254 -1.0048155785 0.049927272 -1.14360845 0.18146743
		 -1.25616455 0.20778076 -1.22579706 0.23852104 -1.17853773 0.25389916 -1.12994683
		 0.2661953 -1.069351196 -0.21438845 -1.73595762 -0.047587011 -1.71328545 0.23282579
		 -1.28666139 0.22924742 -1.23206377 0.26816434 -1.085165262 0.030012289 -1.68843162
		 -0.20031238 -1.65652645 -0.029821316 -1.65206194 0.26904541 -1.28896523 0.08336179
		 -1.67376852 0.043501385 -1.63121998 -0.16279872 -1.60075784 0.0099023199 -1.60673368
		 0.095411979 -1.6251328 0.077908322 -1.58744884 -0.11005326 -1.58552623 0.063022226
		 -1.57530236 0.10764672 -1.58292985 0.049000539 -1.58583105 0.15754938 -1.57305419
		 0.13522191 -1.29928267 0.14438419 -1.37057328 0.26026317 -1.42646921 0.23634535 -1.37401676
		 0.12844075 -1.4430145 0.25010499 -1.48178256 0.28121978 -1.42795455 0.27834055 -1.38363624
		 0.093331419 -1.21945012 0.20553447 -1.32477856 0.091500223 -1.50427318 0.21943097
		 -1.52692699 0.27209663 -1.47724819 0.23607588 -1.40879178 0.27221102 -1.37785137
		 0.040815976 -1.54364026 0.17060712 -1.54328752 0.2451053 -1.5179795 0.22500063 -1.45121896
		 -0.036084209 -1.56098711 0.12056541 -1.55229831 0.21449353 -1.54663074 0.21500999
		 -1.49148238 0.22371466 -1.53616714 0.30328047 -1.089144826 0.28810406 -1.13651788
		 0.2828607 -1.11098492 0.26719031 -1.15936184 0.24202916 -1.14634943 0.35780635 -1.20560825
		 0.29745007 -1.038088918 0.23324382 -1.19084418 0.27677134 -1.058701515 0.26706335
		 -1.23219132 0.34040913 -1.26522458 0.12449552 -1.69149148 0.11635005 -1.68782914
		 0.13300671 -1.64640307 0.12676276 -1.6398617 0.06881135 -1.61059177 0.22989823 -1.61822379
		 0.090636112 -1.59688663 0.16180006 -1.57084119 0.27696893 -1.55852079 0.29195601
		 -1.27642751 0.24943142 -1.34474611 0.31704053 -1.35077453 0.32366669 -1.32725358
		 0.28376192 -1.31237626 0.2302355 -1.35134637 0.2204259 -1.375018 0.29008779 -1.37866318
		 0.36973196 -1.3211956 0.27569249 -1.34584486 0.20060158 -1.38124943 0.20683625 -1.4157691
		 0.27845833 -1.4165839 0.39125845 -1.31098974 0.1867386 -1.42220461 0.21183619 -1.45980096
		 0.28518534 -1.45736086 0.32524228 -1.41043639 0.34431359 -1.35099912 0.19195242 -1.46696734
		 0.23565575 -1.49926198 0.31036639 -1.49316227 0.4235459 -1.30747032 0.38840753 -1.36349642
		 0.21658906 -1.50760484 0.19841279 -1.57130706 0.35115975 -1.51679957 0.41486436 -1.38306725
		 0.21311659 -1.56487 0.33626157 -1.51628196 0.43240979 -1.41435504 0.2235072 -1.55879712
		 0.36061963 -1.53602231 0.34661797 -1.36618376 0.36700836 -1.38966966 0.37401313 -1.4236573
		 0.36835799 -1.45876932 0.19579645 -1.70537269 0.19332883 -1.68181503 0.20999417 -1.68547487
		 0.21815939 -1.66907287 0.23368017 -1.67267489 0.27601993 -1.58249462 0.27155015 -1.60077977
		 0.27797696 -1.59603131 0.28814733 -1.55127609 0.29145947 -1.49259949 0.3574273 -1.39326072
		 0.34823129 -1.37248909 0.35558188 -1.4148643 0.30896914 -1.34719014 0.34330788 -1.43284118
		 0.32268319 -1.44318891 0.29342467 -1.47844481 0.25965598 -1.34642935 0.26580465 -1.23638952
		 0.1796461 -1.55886781 0.11650207 -1.59912312 0.3883431 -1.27388871 0.31720233 -1.35444653
		 0.14746813 -1.74911153 0.19284581 -1.72802544 0.13338514 -1.7511183 0.16091387 -1.74264324
		 0.15626499 -1.77187979 0.099109001 -1.72731471 0.087725498 -1.72559452 0.072757967
		 -1.71945214 0.039793093 -1.75017464 -0.040493846 -1.78030837 0.10710923 -0.8695935;
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "4FF53857-49D7-FEB7-5574-DD8DB8D1559E";
	setAttr ".uopa" yes;
	setAttr -s 230 ".uvtk[0:229]" -type "float2" 1.16226435 1.049751163 1.16226435
		 1.0749439 1.13476253 1.0749439 1.13476253 1.053037167 1.17903197 1.040486455 1.17903197
		 1.0749439 1.16226435 1.10013676 1.13476253 1.096850753 1.10802817 1.0749439 1.10802817
		 1.053246617 1.16226435 1.034181237 1.13476253 1.039498091 1.1857692 1.040441513 1.1857692
		 1.0749439 1.17903197 1.10940146 1.16226435 1.1157068 1.13476253 1.11038995 1.10802817
		 1.096641302 1.081327319 1.0749439 1.081327319 1.039707661 1.10802817 1.039836884
		 1.1857692 1.10944641 1.1857692 1.019117951 1.17903197 1.01919055 1.10802817 1.11005104
		 1.081327319 1.11018026 1.07530582 1.0749439 1.07530582 1.037384868 1.1857692 1.13077009
		 1.17903197 1.13069737 1.07530582 1.11250305 0.82619345 1.29287481 0.82619345 1.31806767
		 0.79869157 1.31806767 0.79869157 1.29616082 0.84296107 1.28361022 0.84296107 1.31806767
		 0.82619345 1.34326041 0.79869157 1.3399744 0.77195728 1.31806767 0.77195728 1.29637027
		 0.82619345 1.27730477 0.79869157 1.28262162 0.84969831 1.28356528 0.84969831 1.31806767
		 0.84296107 1.35252512 0.82619345 1.35883045 0.79869157 1.3535136 0.77195728 1.33976495
		 0.74525636 1.31806767 0.74525636 1.28283131 0.77195728 1.28296053 0.84969831 1.35256994
		 0.84969831 1.26224148 0.84296107 1.26231432 0.77195728 1.35317481 0.74525636 1.35330391
		 0.73923486 1.31806767 0.73923486 1.28050852 0.84969831 1.37389374 0.84296107 1.37382102
		 0.73923486 1.3556267 1.22337925 1.052987456 1.22337925 1.088138819 1.21619439 1.088138819
		 1.21619439 1.054910898 1.22337925 1.12329006 1.21619439 1.12136662 1.22337925 1.031262755
		 1.21619439 1.034375072 1.22337925 1.14501476 1.21619439 1.14190245 1.21311808 1.071009398
		 1.21311808 1.10616076 1.20593333 1.10616076 1.20593333 1.072932839 1.21311808 1.141312
		 1.20593333 1.13938856 1.21311808 1.049284816 1.20593333 1.052397013 1.21311808 1.1630367
		 1.20593333 1.15992451 0.93572199 1.50722528 0.94654977 1.49232221 0.99473554 1.50797868
		 0.96550244 1.54821467 0.91820228 1.4831115 0.94654977 1.47390091 0.99473554 1.45824432
		 0.91820228 1.51291788 0.91820228 1.56358337 0.93572199 1.45899773 0.96550244 1.41800845
		 0.90068251 1.50722528 0.87090212 1.54821467 0.91820228 1.45330513 0.91820228 1.40263963
		 0.88985473 1.49232221 0.84166896 1.50797868 0.90068251 1.45899773 0.87090212 1.41800845
		 0.88985473 1.47390091 0.84166896 1.45824432 0.59810609 1.20850289 0.5748933 1.17655325
		 0.57878923 1.17528737 0.60051388 1.20518875 0.5748933 1.13706124 0.57878917 1.13832712
		 0.63566518 1.22070658 0.63566518 1.21661019 0.59810609 1.1051116 0.60051388 1.10842562
		 0.67322427 1.20850289 0.67081648 1.20518875 0.63566518 1.092907906 0.63566518 1.097004294
		 0.69643706 1.17655325 0.69254112 1.17528737 0.67322427 1.1051116 0.67081648 1.10842562
		 0.69643706 1.13706124 0.69254112 1.13832712 1.055047154 1.30114388 1.036875844 1.32615447
		 1.025645375 1.31069708 1.025645375 1.29159069 1.073218346 1.32615447 1.055047154
		 1.33205867 1.021819234 1.34687805 1.0012834072 1.31861281 1.0012834072 1.28367496
		 1.036875844 1.2761333 1.055047154 1.2702291 1.084448814 1.29159069 1.084448814 1.31069708
		 1.088274956 1.34687805 1.055047154 1.35767448 1.021819234 1.25540972 1.055047154
		 1.24461329 1.073218346 1.2761333 1.1088109 1.28367496 1.1088109 1.31861281 1.088274956
		 1.25540972 0.76080686 1.58464241 0.71595883 1.57007039 0.72630447 1.55583096 0.76080686
		 1.5670414 0.76080686 1.58881426 0.7135067 1.57344556 0.68824124 1.53192031 0.70498079
		 1.52648139 0.80565488 1.57007039 0.79530925 1.55583096 0.6842736 1.53320956 0.80810702
		 1.57344556 0.68824124 1.48476434 0.70498079 1.49020338 0.83337253 1.53192031 0.81663293
		 1.52648139 0.6842736 1.48347521 0.83734018 1.53320956 0.71595883 1.44661438 0.72630447
		 1.46085393 0.83337253 1.48476446 0.81663293 1.4902035 0.7135067 1.44323933 0.83734018
		 1.48347521 0.76080686 1.43204236 0.76080686 1.44964337 0.80565488 1.44661438 0.79530925
		 1.46085393 0.76080686 1.42787051 0.80810702 1.44323933 1.048412681 1.042402625 1.074901938
		 1.042402625 1.073174357 1.069904327 1.050140262 1.069904327 1.043541908 1.025635004
		 1.079772711 1.025635004 1.073064327 1.096638799 1.050250292 1.096638799 1.043518305
		 1.018897653 1.079796314 1.018897653 1.10908401 1.025635004 1.096332192 1.042402625
		 1.014230609 1.025635004 1.026982427 1.042402625 1.080182195 1.12333965 1.043132544
		 1.12333965 1.091521144 1.096638799 1.11015594 1.12333965 1.081403255 1.12936115 1.041911364
		 1.12936115 1.031793475 1.096638799 1.013158679 1.12333965 1.11335301 1.12936115 1.0099616051
		 1.12936115 1.11213362 1.067690611 1.085644245 1.067690611 1.087371945 1.04018867
		 1.11040604 1.04018867 1.11700428 1.084458232 1.080773592 1.084458232 1.087481976
		 1.013454437 1.11029589 1.013454437 1.117028 1.091195464 1.080749989 1.091195464 1.051462293
		 1.084458232 1.064213991 1.067690611 1.14631569 1.084458232 1.13356388 1.067690611
		 1.080364108 0.98675352 1.11741376 0.98675352 1.06902504 1.013454437 1.050390363 0.98675352
		 1.079142928 0.98073202 1.11863494 0.98073202 1.12875283 1.013454437 1.1473875 0.98675352
		 1.047193289 0.98073202 1.15058458 0.98073202 0.23447338 0.32886291 0.23447338 0.29190263
		 0.24165814 0.29291391 0.24165814 0.32785171 1.11937368 1.097488403 1.11937368 1.13444865
		 1.11218882 1.1334374 1.11218882 1.098499656;
createNode polyTweakUV -n "polyTweakUV20";
	rename -uid "A38C2B78-4A88-B6CD-7A8C-B1831C863E5E";
	setAttr ".uopa" yes;
	setAttr -s 49 ".uvtk[0:48]" -type "float2" -0.26598549 -0.10942682 -0.32174179
		 -0.10942682 -0.32174179 -0.2991432 -0.26598549 -0.2991432 -0.37749803 -0.10942682
		 -0.37749803 -0.2991432 -0.23152624 -0.10942682 -0.23152624 -0.2991432 -0.41195732
		 -0.10942682 -0.41195732 -0.2991432 -0.38053089 0.29826683 -0.43628716 0.29826683
		 -0.43628716 0.10855036 -0.38053089 0.10855036 -0.49204344 0.29826683 -0.49204344
		 0.10855036 -0.34607166 0.29826683 -0.34607166 0.10855036 -0.52650273 0.29826683 -0.52650273
		 0.10855036 0.43690053 0.045468815 0.40244123 -0.0019602547 0.44754902 -0.01661671
		 0.46477866 0.0070978939 0.40244123 -0.060585909 0.44754902 -0.04592948 0.4926568
		 -0.031273026 0.4926568 0.063585185 0.4926568 0.016156042 0.43690053 -0.108015 0.46477866
		 -0.069644012 0.5484131 0.045468815 0.52053487 0.0070978939 0.4926568 -0.12613131
		 0.4926568 -0.078702234 0.58287233 -0.0019602547 0.53776461 -0.01661671 0.5484131
		 -0.108015 0.52053487 -0.069644012 0.58287239 -0.060585797 0.53776461 -0.04592948
		 -0.2800428 0.036462836 -0.2214172 0.036462836 -0.2214172 0.22617933 -0.2800428 0.22617933
		 0.34914914 -0.1707011 0.29052353 -0.1707011 0.29052353 -0.36041757 0.34914914 -0.36041757;
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "DDEADD2F-46D9-FD7A-3D2E-F38A42A82BAE";
	setAttr ".uopa" yes;
	setAttr -s 219 ".uvtk[0:218]" -type "float2" -0.02101125 1.093860388 -0.02101125
		 1.11288369 -0.031453706 1.11288369 -0.031453706 1.094017744 -0.016929168 1.094453216
		 -0.016929168 1.11288369 -0.02101125 1.13190699 -0.031453706 1.13174975 -0.034540009
		 1.11288369 -0.034540009 1.096367955 -0.02101125 1.082103491 -0.031453706 1.082357883
		 -0.012775556 1.097947359 -0.012775556 1.11288369 -0.016929168 1.13131416 -0.016929168
		 1.08306253 -0.02101125 1.14366388 -0.031453706 1.14340961 -0.034540001 1.12939942
		 -0.012775556 1.12782001 -0.016929168 1.14270484 -0.26001984 1.10106874 -0.26001984
		 1.12009192 -0.2704623 1.12009192 -0.2704623 1.10122585 -0.25593776 1.10166144 -0.25593776
		 1.12009192 -0.26001984 1.1391151 -0.2704623 1.13895786 -0.2735486 1.12009192 -0.2735486
		 1.10357618 -0.26001984 1.089311719 -0.2704623 1.089566112 -0.25178412 1.10515559
		 -0.25178412 1.12009192 -0.25593776 1.13852251 -0.25593776 1.090270638 -0.26001984
		 1.15087211 -0.2704623 1.15061784 -0.2735486 1.13660765 -0.25178412 1.13502812 -0.25593776
		 1.14991307 0.071325786 0.27410963 0.071325786 0.26464775 0.073441975 0.26464775 0.073441975
		 0.27410963 0.071325786 0.2588 0.073441975 0.2588 0.071325786 0.28357145 0.073441975
		 0.28357145 0.071325786 0.2894192 0.073441975 0.2894192 0.32187521 0.27591196 0.32187521
		 0.26645011 0.32399142 0.26645011 0.32399142 0.27591196 0.32187521 0.26060236 0.32399142
		 0.26060236 0.32187521 0.28537381 0.32399142 0.28537381 0.32187521 0.29122153 0.32399142
		 0.29122153 0.20762941 0.26786545 0.20762941 0.27995113 0.20696484 0.27997056 0.20696484
		 0.26786545 0.20825247 0.26786545 0.20825247 0.2803582 0.20762941 0.28742045 0.20696484
		 0.28745189 0.20430654 0.28004834 0.20430654 0.26786545 0.20762941 0.2557798 0.20696484
		 0.25576034 0.20825247 0.25537267 0.20430654 0.28757775 0.2034758 0.28007263 0.2034758
		 0.26786545 0.20430654 0.25568256 0.20762941 0.24831045 0.20696484 0.24827899 0.2034758
		 0.28761709 0.2034758 0.2556583 0.20430654 0.24815314 0.2034758 0.24811384 0.38679761
		 0.25885451 0.38679761 0.27094018 0.38613304 0.27095965 0.38613304 0.25885451 0.38742068
		 0.25885451 0.38742068 0.27134728 0.38679761 0.27840951 0.38613304 0.27844098 0.38347477
		 0.2710374 0.38347477 0.25885451 0.38679761 0.24676886 0.38613304 0.24674942 0.38742068
		 0.24636176 0.38347477 0.27856684 0.38264397 0.27106169 0.38264397 0.25885451 0.38347477
		 0.24667163 0.38679761 0.23929952 0.38613304 0.23926805 0.38264397 0.27860615 0.38264397
		 0.24664736 0.38347477 0.23914219 0.38264397 0.23910289 0.0050676079 1.16835916 0.013116344
		 1.17420685 0.010238264 1.18306482 -0.0024672989 1.17383361 0.023065127 1.17420685
		 0.025943208 1.18306482 0.0084012467 1.18871856 -0.0072766598 1.17732787 0.0019932757
		 1.15889728 -0.0073204059 1.15889728 0.031113863 1.16835916 0.038648769 1.17383361
		 0.027780226 1.18871856 0.0050676377 1.14943552 -0.0024672989 1.14396095 0.034188196
		 1.15889728 0.043501873 1.15889728 0.04345813 1.17732775 0.013116344 1.14358771 0.010238264
		 1.13472986 0.031113863 1.14943552 0.038648769 1.14396107 -0.0072766598 1.14046681
		 0.0084012467 1.12907612 0.023065098 1.14358771 0.025943208 1.13472986 0.027780226
		 1.12907612 0.04345813 1.14046681 0.29373145 1.13446879 0.30778062 1.12426138 0.3098956
		 1.13077068 0.2992686 1.13849175 0.29049659 1.13211834 0.30654502 1.1204586 0.32514623
		 1.12426138 0.32303125 1.13077068 0.31010965 1.13142943 0.29982898 1.13889873 0.28836524
		 1.15098441 0.2952095 1.15098441 0.32638189 1.1204586 0.33919537 1.13446879 0.33365819
		 1.13849175 0.32281724 1.13142943 0.29373145 1.16750014 0.2992686 1.16347718 0.34243023
		 1.13211846 0.34456161 1.15098441 0.33771738 1.15098441 0.3330979 1.13889873 0.30778056
		 1.17770743 0.3098956 1.17119813 0.33919537 1.16750014 0.33365819 1.16347718 0.30654502
		 1.18151021 0.29049659 1.16985047 0.29982898 1.16307008 0.31010965 1.17053938 0.32514623
		 1.17770743 0.32303125 1.17119813 0.32638183 1.18151021 0.32281724 1.17053938 0.34243026
		 1.16985047 0.3330979 1.16307008 -0.045418121 0.26387838 -0.051265858 0.27192712 -0.060727708
		 0.25890398 -0.060727708 0.27500144 -0.045418121 0.25392959 -0.070189573 0.27192712
		 -0.051265873 0.24588087 -0.07603731 0.26387838 -0.060727708 0.24280652 -0.07603731
		 0.25392959 -0.070189573 0.24588087 0.2774969 1.19412518 0.2774969 1.17412305 0.28793934
		 1.17420566 0.28793934 1.19404256 0.27341479 1.19381368 0.27341479 1.17443466 0.19378176
		 1.17952967 0.19378176 1.19953179 0.18333927 1.19944918 0.18333927 1.17961228 0.19786385
		 1.17984128 0.19786385 1.19922018 -0.050484948 0.28848153 -0.050484948 0.29843029
		 -0.052601136 0.29843029 -0.052601136 0.28848153 0.2104781 0.2984302 0.2104781 0.28848141
		 0.21259429 0.28848141 0.21259429 0.2984302 0.060194202 0.27664787 0.060194202 0.28935543
		 0.059529621 0.28936568 0.059529621 0.27663761 0.056871306 0.28940657 0.056871306
		 0.27659675 0.056040574 0.28941932 0.056040574 0.27658394 0.28421885 0.28755307 0.28421885
		 0.27484548 0.28488341 0.27483523 0.28488341 0.28756332 0.28754175 0.27479437 0.28754175
		 0.28760418 0.28837249 0.27478161 0.28837249 0.28761697;
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "74863C39-4ED9-6209-8489-F78CF21752C9";
	setAttr ".uopa" yes;
	setAttr -s 228 ".uvtk[0:227]" -type "float2" -0.89072359 -1.75367057 -0.89072359
		 -1.73014724 -0.89945942 -1.73014724 -0.89945942 -1.75225341 -0.89072359 -1.70662379
		 -0.89945942 -1.70804095 -0.90661103 -1.73014724 -0.90661103 -1.74626243 -0.89072359
		 -1.76820886 -0.89945942 -1.76591587 -0.89072359 -1.6920855 -0.89945942 -1.69437861
		 -0.90661103 -1.71403205 -1.45864761 -1.74646258 -1.45864761 -1.72293925 -1.46738362
		 -1.72293925 -1.46738362 -1.74504542 -1.45864761 -1.6994158 -1.46738362 -1.70083284
		 -1.47453511 -1.72293925 -1.47453511 -1.73905444 -1.45864761 -1.76100087 -1.46738362
		 -1.75870788 -1.45864761 -1.68487751 -1.46738362 -1.68717051 -1.47453511 -1.70682395
		 -1.62399209 -1.70491683 -1.62399209 -1.68174636 -1.62957728 -1.6813935 -1.62957728
		 -1.70491683 -1.62399209 -1.66742623 -1.62957728 -1.66685522 -1.62399209 -1.72808731
		 -1.62957728 -1.72844028 -1.62399209 -1.74240756 -1.62957728 -1.74297845 -0.8937639
		 -1.72834492 -0.8937639 -1.70517445 -0.89934903 -1.70482159 -0.89934903 -1.72834492
		 -0.8937639 -1.69085431 -0.89934903 -1.6902833 -0.8937639 -1.75151551 -0.89934903
		 -1.75186837 -0.8937639 -1.76583564 -0.89934903 -1.76640666 -0.97029382 -1.7294997
		 -0.97029382 -1.75262308 -0.96931541 -1.75262308 -0.96931541 -1.7294997 -0.97029382
		 -1.76691401 -0.96931541 -1.76691401 -0.9631514 -1.75262308 -0.9631514 -1.7294997
		 -0.97029382 -1.70637655 -0.96931541 -1.70637655 -0.9631514 -1.76691401 -0.96050978
		 -1.75262308 -0.96050978 -1.7294997 -0.9631514 -1.70637655 -0.97029382 -1.6920855
		 -0.96931541 -1.6920855 -0.96050978 -1.76691401 -0.96050978 -1.70637655 -0.9631514
		 -1.6920855 -0.96050978 -1.6920855 -0.95908159 -1.72949982 -0.95908159 -1.75262308
		 -0.95810318 -1.75262308 -0.95810318 -1.72949982 -0.95908159 -1.76691401 -0.95810318
		 -1.76691401 -0.95193923 -1.75262308 -0.95193923 -1.72949982 -0.95908159 -1.70637655
		 -0.95810318 -1.70637655 -0.95193923 -1.76691401 -0.94929755 -1.75262308 -0.94929755
		 -1.72949982 -0.95193923 -1.70637655 -0.95908159 -1.69208562 -0.95810318 -1.69208562
		 -0.94929755 -1.76691401 -0.94929755 -1.70637655 -0.95193923 -1.69208562 -0.94929755
		 -1.69208562 -1.18017232 -0.520594 -1.1658814 -0.54026383 -1.16316962 -0.5393827 -1.17849636
		 -0.5182873 -1.1658814 -0.56457704 -1.16316962 -0.56545812 -1.16222751 -0.53907663
		 -1.17791414 -0.51748586 -1.20329559 -0.51308084 -1.20329559 -0.51022953 -1.18017232
		 -0.58424693 -1.17849648 -0.58655357 -1.16222751 -0.56576431 -1.20329559 -0.5092389
		 -1.22641885 -0.520594 -1.2280947 -0.5182873 -1.20329559 -0.59176004 -1.20329559 -0.59461129
		 -1.17791414 -0.58735502 -1.22867703 -0.51748586 -1.24070978 -0.54026383 -1.24342155
		 -0.5393827 -1.22641885 -0.58424687 -1.22809482 -0.58655357 -1.20329559 -0.59560198
		 -1.24436367 -0.53907663 -1.24070978 -0.56457704 -1.24342155 -0.56545812 -1.22867703
		 -0.58735502 -1.24436367 -0.56576431 -0.63388282 -0.71189171 -0.64956939 -0.73348242
		 -0.64599198 -0.73464477 -0.63167185 -0.71493477 -0.64956939 -0.7601701 -0.64599198
		 -0.75900775 -0.60850132 -0.70364475 -0.60850132 -0.70740628 -0.6338827 -0.78176081
		 -0.63167185 -0.77871776 -0.58311987 -0.71189171 -0.58533078 -0.71493477 -0.60850132
		 -0.79000777 -0.60850132 -0.7862463 -0.56743324 -0.73348242 -0.57101059 -0.73464477
		 -0.58311987 -0.78176081 -0.58533078 -0.77871776 -0.56743324 -0.7601701 -0.57101059
		 -0.75900769 -1.11908114 -0.95518893 -1.10027647 -0.96885133 -1.097126842 -0.95915765
		 -1.11083519 -0.94919795 -1.077032566 -0.96885133 -1.080182195 -0.95915765 -1.092692733
		 -0.9455111 -1.099226713 -0.94076389 -1.058227897 -0.95518893 -1.066473842 -0.94919795
		 -1.084616303 -0.9455111 -1.090673566 -0.93929684 -1.093940616 -0.93692333 -1.08663547
		 -0.93929684 -1.11607134 -0.9330827 -1.10172248 -0.9330827 -1.078082323 -0.94076389
		 -1.088654518 -0.9330827 -1.095188498 -0.9330827 -1.093940616 -0.92924213 -1.099226713
		 -0.92540157 -1.083368421 -0.93692333 -1.075586557 -0.9330827 -1.082120538 -0.9330827
		 -1.11083519 -0.91696751 -1.061237693 -0.9330827 -1.090673566 -0.9268685 -1.08663547
		 -0.9268685 -1.084616303 -0.9206543 -1.092692733 -0.9206543 -1.083368421 -0.92924213
		 -1.078082323 -0.92540157 -1.097126842 -0.90700775 -1.066473842 -0.91696751 -1.080182195
		 -0.90700775 -1.10027647 -0.89731407 -1.11908114 -0.91097653 -1.077032566 -0.89731407
		 -1.058227897 -0.91097647 -1.14052582 -1.73178017 -1.14858377 -1.7206893 -1.16162181
		 -1.73863471 -1.12420762 -1.72647798 -1.13849854 -1.70680821 -1.16162181 -1.71645308
		 -1.14052582 -1.74548912 -1.16162181 -1.69929504 -1.12420762 -1.75079131 -1.17465985
		 -1.7206893 -1.14858377 -1.75658 -1.18474507 -1.70680821 -1.13849854 -1.7704612 -1.1827178
		 -1.73178017 -1.16162181 -1.76081634 -1.199036 -1.72647798 -1.16162181 -1.77797425
		 -1.1827178 -1.74548912 -1.17465985 -1.75658 -1.19903612 -1.75079131 -1.18474507 -1.77046108
		 -1.76972914 -1.86806226 -1.76972914 -1.89279616 -1.76099312 -1.89205122 -1.76099312
		 -1.86880732 -1.46417141 -1.90180695 -1.46417141 -1.87707305 -1.4729073 -1.87781799
		 -1.4729073 -1.90106189 -1.80906761 -1.90162146 -1.80906761 -1.87725854 -1.8146528
		 -1.87707305 -1.8146528 -1.90180695 -1.60446966 -0.98877251 -1.60446966 -1.013135433
		 -1.59888446 -1.013320923 -1.59888446 -0.98858696 -1.11219108 -0.9840644 -1.11219108
		 -0.95975119 -1.11316943 -0.95975119 -1.11316943 -0.9840644 -1.11933339 -0.95975119
		 -1.11933339 -0.9840644 -1.12197506 -0.95975119 -1.12197506 -0.9840644 -1.68416548
		 -0.96304291 -1.68416548 -0.98735613 -1.68318713 -0.98735613 -1.68318713 -0.96304291
		 -1.67702305 -0.98735613 -1.67702305 -0.96304291 -1.67438138 -0.98735613 -1.67438138
		 -0.96304291;
createNode polyTweakUV -n "polyTweakUV23";
	rename -uid "6641D057-4625-949E-1675-3CA23AAC61AE";
	setAttr ".uopa" yes;
	setAttr -s 449 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 1.17615283 0.26860407 1.17656529 0.27061212
		 1.17657197 0.29964343 1.1761781 0.30160981 1.17675781 0.2696017 1.17676461 0.30065262
		 1.17369056 0.30692792 1.1736747 0.26334408 1.19229293 0.26878202 1.19176698 0.26960161
		 1.1917696 0.30065271 1.19229448 0.30146527 1.19483089 0.26332402 1.19484806 0.30692098
		 1.1919558 0.27075109 1.19195807 0.29950288 1.17375195 0.30795982 1.17373443 0.26229447
		 1.19479382 0.26229447 1.19481325 0.30795982 0.92472011 0.28501824 0.92368501 0.2850185
		 0.92349899 0.26400191 0.92472011 0.26400164 0.92472011 0.30614847 0.92368388 0.30614868
		 0.88087344 0.28502756 0.88105732 0.26401109 0.9236961 0.25497916 0.92472011 0.25497821
		 0.88068765 0.30615768 0.88002366 0.26401126 0.87983835 0.28502783 0.88104594 0.25502086
		 0.92369628 0.24411708 0.92472011 0.24411689 0.87965298 0.30615789 0.88002366 0.25502184
		 0.88104421 0.24412635 0.88002366 0.24412654 1.7610991 0.28600436 1.76109886 0.28499186
		 1.77861512 0.2848148 1.77861524 0.28582838 1.79615855 0.28463769 1.79615879 0.28565198
		 1.77862334 0.3267715 1.76110709 0.32677063 1.7961669 0.32677403 1.77862358 0.32778478
		 1.76110733 0.32778478 1.79616714 0.32778478 0.76969647 -0.5616737 0.76741165 -0.56165177
		 0.76741499 -0.60742909 0.76968604 -0.60738486 0.7697075 -0.56073117 0.76741159 -0.56070203
		 0.76970422 -0.60836703 0.76741505 -0.60839957 1.039583206 -0.62186736 1.041737318
		 -0.62185478 1.04174161 -0.57608366 1.039586663 -0.57609004 1.041751981 -0.6228171
		 1.039583206 -0.6228171 1.041756392 -0.57511955 1.039586663 -0.57511955 0.83714712
		 0.23967619 0.86060131 0.23959996 0.86059707 0.28537104 0.83714527 0.28529641 0.83714712
		 0.23863591 0.86061621 0.23863591 0.83714521 0.28633335 0.86061174 0.28633335 0.87259144
		 0.23952696 0.8444702 0.23953862 0.84452039 0.21072836 0.87264121 0.21074013 0.84457153
		 0.24065854 0.87258959 0.24065854 0.8433944 0.21061817 0.84334409 0.2396494 0.84462631
		 0.20960775 0.87264311 0.20960775 0.84334242 0.24065854 0.84339619 0.20960775 1.82225955
		 0.24860007 1.79459059 0.24858339 1.79462099 0.21982971 1.82229602 0.21981287 1.79459441
		 0.24973196 1.822258 0.24973196 1.7930125 0.24858242 1.79304898 0.21983062 1.79462826
		 0.2186808 1.82229745 0.2186808 1.79301083 0.24973196 1.79305041 0.2186808 1.054589391
		 -0.71728933 1.054589391 -0.69713587 1.022587538 -0.69713581 1.022587538 -0.71728933
		 1.05570662 -0.69713378 1.05570662 -0.71728933 1.022587538 -0.6961838 1.054589391
		 -0.6961838 1.021470428 -0.69713372 1.021470428 -0.71728933 1.055642128 -0.69603354
		 1.021470428 -0.6961838 1.020214677 -0.69546282 1.055773854 -0.69546229 1.055736303
		 -0.69701791 1.056203961 -0.69642454 1.021440506 -0.69701797 1.056962132 -0.69546229
		 1.017265201 -0.69396162 1.058287501 -0.69399178 1.056979656 -0.69613498 1.056752086
		 -0.72554952 1.057139754 -0.72609401 1.057764649 -0.72630221 1.01937151 -0.72700387
		 1.020414233 -0.72554928 1.059690714 -0.69389468 1.016266465 -0.72864002 1.01728785
		 -0.69318795 1.059800506 -0.69318795 1.05562973 -0.72641838 1.056547523 -0.72682434
		 1.057782769 -0.72699755 1.059880972 -0.69449377 1.060842156 -0.72806078 1.021536589
		 -0.72641838 1.02038312 -0.72641838 1.060920715 -0.69318795 1.015230298 -0.72863984
		 1.01622963 -0.69395298 1.059381247 -0.7286346 1.016251564 -0.69318795 1.01731348
		 -0.69232208 1.05991447 -0.69236755 1.060735464 -0.72870815 1.060941577 -0.69391614
		 1.061932802 -0.72867823 1.06089592 -0.69235843 1.016244292 -0.72941399 1.015208006
		 -0.72941399 1.060829043 -0.72941399 1.016277194 -0.69233102 1.059038401 -0.66414332
		 1.018133521 -0.66414332 1.061953783 -0.72941399 1.060050488 -0.66414332 1.017121553
		 -0.66414332 1.05903554 -0.63024259 1.018136382 -0.63024259 1.060050488 -0.63024259
		 1.017121553 -0.63024259 1.059035182 -0.58124334 1.018136263 -0.58124334 1.060050488
		 -0.58124334 1.017121553 -0.58124334 1.058968306 -0.55336022 1.018136024 -0.5534119
		 1.059982419 -0.55335885 1.017121553 -0.55341303 1.018135548 -0.52947146 1.058901906
		 -0.52936822 1.059914351 -0.52936578 1.017121553 -0.5294739 1.9808681 0.2044138 1.96093178
		 0.20440806 1.96093154 0.17031427 1.9808681 0.17030858 1.96089625 0.20556122 1.9808681
		 0.20556122 1.95976245 0.20440775 1.95976245 0.17031457 1.96089625 0.16916119 1.9808681
		 0.16916119 1.95976245 0.20556122 1.95976245 0.16916119 0.63649344 -0.54278594 0.63547909
		 -0.54278851 0.63547879 -0.58757871 0.63648856 -0.58757639 0.63423991 -0.63809973
		 0.63527548 -0.63839859 0.6776188 -0.5874806 0.6776154 -0.54268241 0.6352644 -0.63904327
		 0.63421673 -0.63904548 0.67884028 -0.63831365 0.67862624 -0.54267985 0.67862588 -0.58747828
		 0.67885208 -0.63894969 0.67985922 -0.63800615 0.67988223 -0.63894755 1.059431672
		 0.0026105866 1.058411241 0.0026130932 1.058297038 -0.021370178 1.059317589 -0.02137184
		 1.058182955 -0.051012442 1.059207439 -0.051012442 1.10208118 -0.021440053 1.1020838
		 0.002502057 1.058182955 -0.091597579 1.059207439 -0.091597579 1.10208309 -0.051012442
		 1.10310769 0.0024993327 1.10310769 -0.021441687 1.058182955 -0.11710334 1.059207439
		 -0.11710334 1.10208309 -0.091597579 1.10310769 -0.051012442 1.058182955 -0.12324423
		 1.059207439 -0.12324423 1.10208309 -0.11710334 1.10310769 -0.091597579 1.058182955
		 -0.1240962 1.059207439 -0.1240962 1.10208309 -0.12324423 1.10310769 -0.11710334 1.058182955
		 -0.14377797 1.059267044 -0.14377797 1.10208309 -0.1240962 1.10310769 -0.12324423
		 1.058182955 -0.18785828 1.05921793 -0.18785828 1.10202348 -0.14377797 1.10310769
		 -0.1240962 1.057293653 -0.22239225 1.058295131 -0.22238874 1.1020726 -0.18785828
		 1.10310769 -0.14377797 1.058397174 -0.22321188 1.057272434 -0.22321188 1.10295987
		 -0.22236004 1.10310769 -0.18785828 1.10298181 -0.22321188 1.10399616 -0.22235928
		 1.10401821 -0.22321188 1.59847105 0.32583043 1.5982939 0.32686403 1.56227064 0.32778472
		 1.56230736 0.3268421 1.59910762 0.32581332;
	setAttr ".uvtk[250:448]" 1.59910774 0.32684314 1.56235409 0.28113094 1.59846067
		 0.28220117 1.59910488 0.28221786 1.56229389 0.28014883 1.5982883 0.28115496 1.59910464
		 0.28117776 1.24308991 -0.52362663 1.24309039 -0.46889982 1.20009422 -0.46879122 1.20009673
		 -0.52351326 1.24412513 -0.46890256 1.24412549 -0.52362138 1.19905806 -0.46878871
		 1.19905829 -0.52350533 1.19878376 -0.58008617 1.24440408 -0.58020568 1.24544108 -0.58020854
		 1.19774354 -0.58008343 0.72869271 -0.07982365 0.71978509 -0.044764996 0.71079636
		 -0.040384732 0.72085685 -0.079823822 0.73236489 -0.079398088 0.72510993 -0.04999242
		 0.70477062 -0.011713875 0.69583952 -0.0037851851 0.6987555 -0.074093729 0.70351166
		 -0.10264213 0.71673727 -0.17060113 0.72979319 -0.15070482 0.72532427 -0.049101517
		 0.73265272 -0.078743748 0.71560711 -0.026143311 0.73756343 -0.14175178 0.69588315
		 -0.0026147268 0.70487261 -0.010608578 0.6889599 -0.049713634 0.69850618 -0.074888147
		 0.70213729 -0.15237422 0.71692419 -0.1715093 0.72988045 -0.15182754 0.71576786 -0.025118211
		 0.73767596 -0.11932894 0.73104066 -0.15097553 0.67486656 0.00081089069 0.67491525
		 -0.00036289054 0.68886685 -0.050895095 0.67127663 -0.055302009 0.70182174 -0.15177186
		 0.70207632 -0.21468762 0.68967658 -0.18651478 0.7022959 -0.21558963 0.73947048 -0.14483465
		 0.65373647 -0.021031009 0.6538974 -0.02208809 0.67135072 -0.056431822 0.65363765
		 -0.077954181 0.68944991 -0.1856726 0.68224061 -0.21462795 0.69369102 -0.2493839 0.69393641
		 -0.25012362 0.63416511 -0.0757498 0.63438165 -0.076652907 0.65380716 -0.078962281
		 0.63819414 -0.12291353 0.68199074 -0.21388771 0.68201137 -0.21544541 0.69346929 -0.25020754
		 0.6937117 -0.25094327 0.61500275 -0.13233696 0.61515075 -0.13344188 0.63841939 -0.12375251
		 0.62401313 -0.17422333 0.68177146 -0.21471724 0.59153616 -0.14565872 0.59185266 -0.14677064
		 0.60264969 -0.18274522 0.62376553 -0.17529586 0.6022917 -0.1830896 0.59140909 -0.14706637
		 0.60270607 -0.18383513 1.16298079 -0.078021877 1.17299795 -0.035577241 1.16400909
		 -0.039957777 1.15514493 -0.078021742 1.1803261 -0.10083995 1.18508649 -0.072344616
		 1.18756616 -0.001884534 1.17902362 -0.010032585 1.15862787 -0.048267741 1.15147316
		 -0.077593766 1.15404439 -0.14890291 1.16710043 -0.16879921 1.18533576 -0.073140435
		 1.18170047 -0.15057239 1.18794501 -0.00092619227 1.17892146 -0.0089255059 1.16822064
		 -0.024459329 1.15841246 -0.047371101 1.15118492 -0.076941848 1.14627421 -0.1399499
		 1.16691351 -0.16970739 1.15395725 -0.15002562 1.19497919 -0.049201276 1.19488597
		 -0.048018921 1.1820159 -0.14996998 1.18176138 -0.21288559 1.19416106 -0.18471275
		 1.16806018 -0.02343009 1.14616168 -0.117527 1.15279698 -0.14917363 1.1815418 -0.2137877
		 1.21249545 -0.054629721 1.212569 -0.053499933 1.19438779 -0.18387066 1.19014764 -0.24751468
		 1.2015928 -0.21282518 1.14436722 -0.14303276 1.18989229 -0.24828871 1.230039 -0.077051707
		 1.23020768 -0.076046549 1.20891333 0.0014391323 1.18809783 -0.0020357931 1.20183969
		 -0.21205841 1.19038117 -0.24836722 1.20181417 -0.21368037 1.19012594 -0.24914132
		 1.2454313 -0.12185013 1.24565589 -0.12101331 1.22993088 -0.020174392 1.20896149 0.0026130907
		 1.2020663 -0.21291532 1.25984359 -0.17232656 1.24945176 -0.07473734 1.23009169 -0.019115213
		 1.26008976 -0.17339653 1.2686882 -0.13151813 1.24966872 -0.073831797 1.28115094 -0.18202934
		 1.28120792 -0.180942 1.29199028 -0.14496499 1.26883686 -0.13040988 1.28156614 -0.18128213
		 1.29242527 -0.14528777 1.29230595 -0.14385423 1.12600851 -0.72918487 1.12616146 -0.70135957
		 1.11134136 -0.7076723 1.11099088 -0.73533732 1.12654781 -0.70109147 1.12637055 -0.69994026
		 1.11136138 -0.70608938 1.51536429 0.18260792 1.51501381 0.21027799 1.50019765 0.21667755
		 1.50035107 0.18885136 1.49998879 0.21809597 1.51499379 0.21185595 1.49981189 0.2169468
		 1.40912616 -0.57903683 1.40402317 -0.55888319 1.39465487 -0.58755714 1.39971793 -0.60758054
		 1.40429759 -0.55804706 1.40940106 -0.57820266 1.40378213 -0.5579313 1.39437795 -0.58646262
		 1.39437854 -0.58846551 1.3994354 -0.60843742 1.40405715 -0.55709714 1.39409149 -0.58733171
		 1.2468704 -0.62380087 1.25193346 -0.60377741 1.24256504 -0.57510364 1.23746216 -0.5952571
		 1.25220966 -0.60468566 1.24715281 -0.62465751 1.2428062 -0.5741514 1.25221038 -0.60268301
		 1.24229062 -0.57426727 1.23718715 -0.59442282 1.25249684 -0.60355192 1.24253118 -0.57331735
		 0.91305411 0.19166076 0.8984859 0.15796798 0.91057438 0.12120063 0.92037386 0.14552633
		 0.91358572 0.19150941 0.91284811 0.19149479 0.9134329 0.19261904 1.47108364 -0.66584939
		 1.47078788 -0.66629297 1.46967602 -0.66597646 1.47034216 -0.66814536 1.040737867
		 -0.67162383 1.04184866 -0.67130816 1.042171359 -0.67174315 1.041405797 -0.66945404;
createNode polyTweakUV -n "polyTweakUV24";
	rename -uid "027883F6-4AD3-2330-D1C4-F9A741D1BA61";
	setAttr ".uopa" yes;
	setAttr -s 159 ".uvtk[0:158]" -type "float2" -0.76841223 -0.15071386 -0.78994763
		 -0.11298782 -0.81414366 -0.12679976 -0.7920292 -0.16554034 -0.7567395 -0.14263582
		 -0.77543008 -0.10470069 -0.81148297 -0.07526172 -0.83625805 -0.088059254 -0.83833969
		 -0.14061171 -0.78662872 -0.17457198 -0.7479319 -0.13999136 -0.7551055 -0.16092001
		 -0.75093621 -0.12843153 -0.7717942 -0.10262521 -0.79859191 -0.069317937 -0.73409188
		 -0.13426654 -0.79183894 -0.063076407 -0.81350893 -0.058612905 -0.86687684 -0.71434844
		 -0.8514055 -0.14807014 -0.78667557 -0.17880097 -1.17234612 -0.83776152 -0.78341001
		 -0.071543306 -0.77987117 -0.054070178 -0.83371311 -0.055448666 -0.84495884 -0.076707527
		 -0.85285723 -0.14889884 -0.78668082 -0.17927083 -0.8453663 -0.076473057 -0.85440576
		 -0.14978279 -0.83830464 -0.17798908 -0.87050682 -0.12157651 -0.86059999 -0.15331869
		 -0.8443954 -0.18170618 -1.55296874 -0.74591023 -0.76920897 -0.18459739 -0.83969653
		 -0.058992546 -0.84106976 -0.05871902 -0.87680453 -0.1249312 -0.86253572 -0.15442367
		 -0.84629875 -0.18286778 -0.77105916 -0.18988355 -0.84656268 -0.057624776 -0.87877256
		 -0.12597962 -0.77163744 -0.19153547 -0.84827924 -0.057282679 -0.70168442 -0.00045913918
		 -0.66371113 -0.021555375 -0.61918867 -0.051496111 -0.68917537 0.024462696 -0.70432121
		 -0.014328726 -0.67182922 -0.036168076 -0.64817035 -0.094196141 -0.62034959 -0.096088983
		 -0.5913372 -0.052221235 -0.70263159 0.034743905 -0.72440499 -0.016277935 -0.72709578
		 0.0072979089 -0.70620179 -0.028831193 -0.67797178 -0.050436195 -0.66145533 -0.089419782
		 -0.72969317 -0.023190582 -0.67334288 -0.10571535 -0.65440869 -0.12001541 -0.61833948
		 -0.1128993 -0.62934405 0.040305343 -0.7046876 0.040428508 -0.72856176 0.029514123
		 -0.67490059 -0.083666615 -0.68198586 -0.10675848 -0.63602322 -0.13256982 -0.61449045
		 -0.11755835 -0.62853223 0.041766632 -0.704916 0.041060165 -0.61406279 -0.11807605
		 -0.62766629 0.043325275 -0.65605748 0.059098098 -0.5736894 -0.085170045 -0.62420249
		 0.049560089 -0.65277612 0.065434262 -1.53900969 -0.68218648 -0.72447336 0.038644254
		 -0.62729061 -0.13279013 -0.62608397 -0.13368899 -0.56656492 -0.085564248 -0.56348616
		 -0.052946445 -0.65175068 0.067414224 -0.72511649 0.044629022 -0.62125725 -0.13728498
		 -0.56433856 -0.085687421 -0.7253176 0.046499215 -0.61974889 -0.13840863 -0.76095301
		 -0.096175909 -0.7432766 -0.1151672 -0.73990071 -0.11370195 -0.75817907 -0.095420845
		 -0.72286594 -0.12155966 -0.72155994 -0.11835162 -0.77146578 -0.073095225 -0.76959616
		 -0.072561041 -0.77072096 -0.05381063 -0.76928687 -0.054951355 -0.69398522 -0.054115351
		 -0.71593565 -0.041226707 -0.71846139 -0.043065071 -0.69590873 -0.055243328 -0.73982066
		 -0.03863357 -0.74078888 -0.042210445 -0.68864989 -0.08415883 -0.69162649 -0.082163081
		 -0.69495761 -0.10445706 -0.69735873 -0.10174938 -0.94726735 -0.83316028 -0.71812648
		 -0.13301018 -0.74076259 -0.023890648 -0.90271997 -0.72634548 -0.72995913 -0.077867575
		 -0.69433796 -0.056654658 -1.59276593 -0.78859609 -0.67861873 -0.15870783 -1.48768234
		 -0.65627563 -0.79990441 -0.016306363 -0.57629764 -0.052612886 -0.57462656 -0.05265636
		 -0.57284403 -0.052702732 -0.56571418 -0.052888412 -0.62312013 0.051508453 -0.66375047
		 -0.050335906 -0.64703965 -0.050770964 -0.69045705 -0.058429655 -0.63665038 0.02715393
		 -0.65018076 0.0027991545 -0.67914701 -0.04836829 -0.7117548 -0.17071131 -1.20616043
		 -0.87690377 -0.7117548 -0.17071131 -1.61995041 -0.81102574 -1.16110015 -0.81650406
		 -1.45454526 -0.64427513 -0.79990441 -0.016306363 -0.94174838 -0.71820259 -0.86682945
		 -0.71011871 -0.77272195 0.0061256974 -1.66836607 -0.84204537 -0.76874626 -0.18327583
		 -1.39755607 -0.63171512 -0.72431254 0.037148096 -0.90815973 -0.80297905 -0.95834053
		 -0.83385992 -0.93885911 -0.7525937 -0.88675553 -0.72508734 -0.89085317 -0.7089929
		 -0.84129262 -0.078817427 -1.16476548 -0.81439406 -0.76265293 -0.17992595 -0.93262792
		 -0.74341798 -0.77067047 -0.040963218 -0.91735941 -0.81608784 -0.68821853 -0.11593761;
createNode aiStandardSurface -n "aiStandardSurface1";
	rename -uid "2D422C1D-4E58-FDEE-8F95-6DADB358BFD3";
	setAttr ".base_color" -type "float3" 0.0114 0.0114 0.021600001 ;
	setAttr ".specular_roughness" 0.28671327233314514;
	setAttr ".metalness" 0.95104897022247314;
createNode shadingEngine -n "aiStandardSurface1SG";
	rename -uid "F56084F5-449A-6C26-0470-2D8C07A9E60A";
	setAttr ".ihi" 0;
	setAttr -s 8 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "0A832C9D-4B1E-7BE2-8762-4DB0BABA0410";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "E904EF12-4A0D-198F-E100-4B8E838D5403";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -83.333330021964315 -34.523808151956651 ;
	setAttr ".tgi[0].vh" -type "double2" 574.99997715155382 66.666664017571449 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".dli" 1;
	setAttr ".fprt" yes;
	setAttr ".rtfm" 1;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :lightList1;
select -ne :standardSurface1;
	setAttr ".bc" -type "float3" 0.40000001 0.40000001 0.40000001 ;
	setAttr ".sr" 0.5;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "standardSurface1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultLightSet;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId1.id" "handle_geoShape.iog.og[2].gid";
connectAttr "set1.mwc" "handle_geoShape.iog.og[2].gco";
connectAttr "groupId2.id" "handle_geoShape.iog.og[3].gid";
connectAttr "set2.mwc" "handle_geoShape.iog.og[3].gco";
connectAttr "polyTweakUV23.out" "handle_geoShape.i";
connectAttr "polyTweakUV23.uvtk[0]" "handle_geoShape.uvst[0].uvtw";
connectAttr "polyTweakUV20.out" "spoutCircle_geoShape.i";
connectAttr "polyTweakUV20.uvtk[0]" "spoutCircle_geoShape.uvst[0].uvtw";
connectAttr "polyTweakUV18.out" "spout_geoShape.i";
connectAttr "polyTweakUV18.uvtk[0]" "spout_geoShape.uvst[0].uvtw";
connectAttr "polyTweakUV19.out" "topper_geoShape.i";
connectAttr "polyTweakUV19.uvtk[0]" "topper_geoShape.uvst[0].uvtw";
connectAttr "polyTweakUV22.out" "lid_geoShape.i";
connectAttr "polyTweakUV22.uvtk[0]" "lid_geoShape.uvst[0].uvtw";
connectAttr "polyTweakUV21.out" "CoffeepotTopBody_geoShape.i";
connectAttr "polyTweakUV21.uvtk[0]" "CoffeepotTopBody_geoShape.uvst[0].uvtw";
connectAttr "polyTweakUV24.out" "CoffeepotBottomBody_geoShape.i";
connectAttr "polyTweakUV24.uvtk[0]" "CoffeepotBottomBody_geoShape.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "polyTweak1.out" "polyExtrudeEdge1.ip";
connectAttr "CoffeepotBottomBody_geoShape.wm" "polyExtrudeEdge1.mp";
connectAttr "polyCylinder1.out" "polyTweak1.ip";
connectAttr "polyExtrudeEdge1.out" "polyExtrudeFace1.ip";
connectAttr "CoffeepotBottomBody_geoShape.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "deleteComponent1.ig";
connectAttr "polyTweak4.out" "polyExtrudeFace2.ip";
connectAttr "lid_geoShape.wm" "polyExtrudeFace2.mp";
connectAttr "polyCylinder2.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyExtrudeFace3.ip";
connectAttr "lid_geoShape.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak5.ip";
connectAttr "polyExtrudeFace3.out" "polyExtrudeFace4.ip";
connectAttr "lid_geoShape.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace4.out" "polyExtrudeEdge2.ip";
connectAttr "lid_geoShape.wm" "polyExtrudeEdge2.mp";
connectAttr "polyExtrudeEdge2.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polySplit7.ip";
connectAttr "polyTweak6.out" "polyExtrudeFace5.ip";
connectAttr "lid_geoShape.wm" "polyExtrudeFace5.mp";
connectAttr "polySplit7.out" "polyTweak6.ip";
connectAttr "polyExtrudeFace5.out" "polyExtrudeFace6.ip";
connectAttr "lid_geoShape.wm" "polyExtrudeFace6.mp";
connectAttr "polyExtrudeFace6.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polySplit9.ip";
connectAttr "polyCylinder3.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "polySplit10.ip";
connectAttr "polySplit10.out" "polySplit11.ip";
connectAttr "polySplit11.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "polySplit12.ip";
connectAttr "polyTweak9.out" "polyExtrudeFace7.ip";
connectAttr "topper_geoShape.wm" "polyExtrudeFace7.mp";
connectAttr "polySplit12.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polyExtrudeFace8.ip";
connectAttr "topper_geoShape.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak10.ip";
connectAttr "polyExtrudeFace8.out" "polyTweak11.ip";
connectAttr "polyTweak11.out" "polySplit13.ip";
connectAttr "polyCylinder4.out" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "deleteComponent3.ig";
connectAttr "deleteComponent1.og" "polyDelEdge1.ip";
connectAttr "polySplit9.out" "polyDelEdge2.ip";
connectAttr "polyTweak12.out" "polyDelEdge3.ip";
connectAttr "polySplit13.out" "polyTweak12.ip";
connectAttr "polyCylinder5.out" "polyDelEdge4.ip";
connectAttr "polyCube1.out" "polyExtrudeFace9.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace9.mp";
connectAttr "polyTweak13.out" "polyExtrudeFace10.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace10.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak13.ip";
connectAttr "polyTweak14.out" "polyExtrudeFace11.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace11.mp";
connectAttr "polyExtrudeFace10.out" "polyTweak14.ip";
connectAttr "polyTweak15.out" "polyExtrudeFace12.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace12.mp";
connectAttr "polyExtrudeFace11.out" "polyTweak15.ip";
connectAttr "polyTweak16.out" "polyExtrudeFace13.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace13.mp";
connectAttr "polyExtrudeFace12.out" "polyTweak16.ip";
connectAttr "polyTweak17.out" "polyExtrudeFace14.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace14.mp";
connectAttr "polyExtrudeFace13.out" "polyTweak17.ip";
connectAttr "polyExtrudeFace14.out" "polyTweak18.ip";
connectAttr "polyTweak18.out" "polySplit14.ip";
connectAttr "polySplit14.out" "polySplit15.ip";
connectAttr "polySplit15.out" "polyTweak19.ip";
connectAttr "polyTweak19.out" "polySplit16.ip";
connectAttr "polySplit16.out" "polyTweak20.ip";
connectAttr "polyTweak20.out" "polySplit17.ip";
connectAttr "polySplit17.out" "polyTweak21.ip";
connectAttr "polyTweak21.out" "polySplit18.ip";
connectAttr "polyTweak22.out" "polyExtrudeFace15.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace15.mp";
connectAttr "polySplit18.out" "polyTweak22.ip";
connectAttr "polyTweak23.out" "polyExtrudeEdge3.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeEdge3.mp";
connectAttr "polyExtrudeFace15.out" "polyTweak23.ip";
connectAttr "polyTweak24.out" "polyExtrudeFace16.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace16.mp";
connectAttr "polyExtrudeEdge3.out" "polyTweak24.ip";
connectAttr "polyExtrudeFace16.out" "polySplit19.ip";
connectAttr "groupId1.msg" "set1.gn" -na;
connectAttr "handle_geoShape.iog.og[2]" "set1.dsm" -na;
connectAttr "polySplit19.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "polyCloseBorder1.ip";
connectAttr "polyCloseBorder1.out" "polyExtrudeFace17.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace17.mp";
connectAttr "polyTweak25.out" "polyExtrudeFace18.ip";
connectAttr "handle_geoShape.wm" "polyExtrudeFace18.mp";
connectAttr "polyExtrudeFace17.out" "polyTweak25.ip";
connectAttr "groupId2.msg" "set2.gn" -na;
connectAttr "handle_geoShape.iog.og[3]" "set2.dsm" -na;
connectAttr "polyExtrudeFace18.out" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "groupParts2.og" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "polyBevel1.ip";
connectAttr "handle_geoShape.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polyMergeVert1.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert1.mp";
connectAttr "polyMergeVert1.out" "polyBevel2.ip";
connectAttr "handle_geoShape.wm" "polyBevel2.mp";
connectAttr "polyBevel2.out" "deleteComponent6.ig";
connectAttr "deleteComponent6.og" "polyTweak26.ip";
connectAttr "polyTweak26.out" "polySplit20.ip";
connectAttr "polySplit20.out" "polySplit21.ip";
connectAttr "polySplit21.out" "polySplit22.ip";
connectAttr "polySplit22.out" "polySplit23.ip";
connectAttr "polySplit23.out" "polyDelEdge5.ip";
connectAttr "polyDelEdge5.out" "polySplit24.ip";
connectAttr "polySplit24.out" "polySplit25.ip";
connectAttr "polySplit25.out" "polySplit26.ip";
connectAttr "polySplit26.out" "polySplit27.ip";
connectAttr "polySplit27.out" "polyDelEdge6.ip";
connectAttr "polyDelEdge6.out" "polyDelEdge7.ip";
connectAttr "polyDelEdge7.out" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "polyTweakUV1.ip";
connectAttr "polyTweak27.out" "polyMergeVert2.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert2.mp";
connectAttr "polyTweakUV1.out" "polyTweak27.ip";
connectAttr "polyMergeVert2.out" "polyTweakUV2.ip";
connectAttr "polyTweak28.out" "polyMergeVert3.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert3.mp";
connectAttr "polyTweakUV2.out" "polyTweak28.ip";
connectAttr "polyMergeVert3.out" "polyBevel3.ip";
connectAttr "handle_geoShape.wm" "polyBevel3.mp";
connectAttr "polyBevel3.out" "polyBevel4.ip";
connectAttr "handle_geoShape.wm" "polyBevel4.mp";
connectAttr "polyBevel4.out" "polyTweakUV3.ip";
connectAttr "polyTweak29.out" "polyMergeVert4.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert4.mp";
connectAttr "polyTweakUV3.out" "polyTweak29.ip";
connectAttr "polyMergeVert4.out" "polyTweakUV4.ip";
connectAttr "polyTweak30.out" "polyMergeVert5.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert5.mp";
connectAttr "polyTweakUV4.out" "polyTweak30.ip";
connectAttr "polyMergeVert5.out" "polyTweakUV5.ip";
connectAttr "polyTweak31.out" "polyMergeVert6.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert6.mp";
connectAttr "polyTweakUV5.out" "polyTweak31.ip";
connectAttr "polyMergeVert6.out" "polyTweakUV6.ip";
connectAttr "polyTweak32.out" "polyMergeVert7.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert7.mp";
connectAttr "polyTweakUV6.out" "polyTweak32.ip";
connectAttr "polyTweak33.out" "polyDelEdge8.ip";
connectAttr "polyMergeVert7.out" "polyTweak33.ip";
connectAttr "polyDelEdge8.out" "polyTweakUV7.ip";
connectAttr "polyTweak34.out" "polyMergeVert8.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert8.mp";
connectAttr "polyTweakUV7.out" "polyTweak34.ip";
connectAttr "polyMergeVert8.out" "polyTweakUV8.ip";
connectAttr "polyTweak35.out" "polyMergeVert9.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert9.mp";
connectAttr "polyTweakUV8.out" "polyTweak35.ip";
connectAttr "polyMergeVert9.out" "polyTweakUV9.ip";
connectAttr "polyTweak36.out" "polyMergeVert10.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert10.mp";
connectAttr "polyTweakUV9.out" "polyTweak36.ip";
connectAttr "polyMergeVert10.out" "polyTweakUV10.ip";
connectAttr "polyTweak37.out" "polyMergeVert11.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert11.mp";
connectAttr "polyTweakUV10.out" "polyTweak37.ip";
connectAttr "polyDelEdge3.out" "polyAutoProj1.ip";
connectAttr "topper_geoShape.wm" "polyAutoProj1.mp";
connectAttr "polyTweak38.out" "polyAutoProj2.ip";
connectAttr "spout_geoShape.wm" "polyAutoProj2.mp";
connectAttr "deleteComponent3.og" "polyTweak38.ip";
connectAttr "polyDelEdge2.out" "polyAutoProj3.ip";
connectAttr "lid_geoShape.wm" "polyAutoProj3.mp";
connectAttr "polyDelEdge4.out" "polyAutoProj4.ip";
connectAttr "spoutCircle_geoShape.wm" "polyAutoProj4.mp";
connectAttr "polyDelEdge1.out" "polyAutoProj5.ip";
connectAttr "CoffeepotBottomBody_geoShape.wm" "polyAutoProj5.mp";
connectAttr "polyTweak39.out" "polyAutoProj6.ip";
connectAttr "handle_geoShape.wm" "polyAutoProj6.mp";
connectAttr "polyMergeVert11.out" "polyTweak39.ip";
connectAttr "polySurfaceShape1.o" "polyAutoProj7.ip";
connectAttr "CoffeepotTopBody_geoShape.wm" "polyAutoProj7.mp";
connectAttr "polyAutoProj6.out" "polyTweakUV11.ip";
connectAttr "polyAutoProj5.out" "polyTweakUV12.ip";
connectAttr "polyAutoProj7.out" "polyTweakUV13.ip";
connectAttr "polyAutoProj2.out" "polyTweakUV14.ip";
connectAttr "polyAutoProj3.out" "polyTweakUV15.ip";
connectAttr "polyAutoProj1.out" "polyTweakUV16.ip";
connectAttr "polyAutoProj4.out" "polyDelEdge9.ip";
connectAttr "polyTweakUV15.out" "polyDelEdge10.ip";
connectAttr "polyDelEdge10.out" "polySplit28.ip";
connectAttr "polySplit28.out" "polySplit29.ip";
connectAttr "polySplit29.out" "polySplit30.ip";
connectAttr "polySplit30.out" "polySplit31.ip";
connectAttr "polySplit31.out" "polySplit32.ip";
connectAttr "polySplit32.out" "polyDelEdge11.ip";
connectAttr "polyTweakUV16.out" "polyMergeVert12.ip";
connectAttr "topper_geoShape.wm" "polyMergeVert12.mp";
connectAttr "polyTweakUV14.out" "polyMergeVert13.ip";
connectAttr "spout_geoShape.wm" "polyMergeVert13.mp";
connectAttr "polyDelEdge11.out" "polyMergeVert14.ip";
connectAttr "lid_geoShape.wm" "polyMergeVert14.mp";
connectAttr "polyDelEdge9.out" "polyMergeVert15.ip";
connectAttr "spoutCircle_geoShape.wm" "polyMergeVert15.mp";
connectAttr "polyTweakUV12.out" "polyMergeVert16.ip";
connectAttr "CoffeepotBottomBody_geoShape.wm" "polyMergeVert16.mp";
connectAttr "polyTweakUV11.out" "polyMergeVert17.ip";
connectAttr "handle_geoShape.wm" "polyMergeVert17.mp";
connectAttr "polyTweakUV13.out" "polyMergeVert18.ip";
connectAttr "CoffeepotTopBody_geoShape.wm" "polyMergeVert18.mp";
connectAttr "polyMergeVert13.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyTweakUV17.ip";
connectAttr "polyTweakUV17.out" "polyMapSewMove1.ip";
connectAttr "polyMergeVert15.out" "deleteComponent8.ig";
connectAttr "polyMergeVert16.out" "polyMapCut2.ip";
connectAttr "polyMapCut2.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyFlipUV1.ip";
connectAttr "CoffeepotBottomBody_geoShape.wm" "polyFlipUV1.mp";
connectAttr "polyMapSewMove1.out" "polyTweakUV18.ip";
connectAttr "polyMergeVert12.out" "polyTweakUV19.ip";
connectAttr "deleteComponent8.og" "polyTweakUV20.ip";
connectAttr "polyMergeVert18.out" "polyTweakUV21.ip";
connectAttr "polyMergeVert14.out" "polyTweakUV22.ip";
connectAttr "polyMergeVert17.out" "polyTweakUV23.ip";
connectAttr "polyFlipUV1.out" "polyTweakUV24.ip";
connectAttr "aiStandardSurface1.out" "aiStandardSurface1SG.ss";
connectAttr "coffeepot_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "CoffeepotBottomBody_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "CoffeepotTopBody_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "lid_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "topper_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "spout_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "spoutCircle_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "handle_geoShape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "aiStandardSurface1SG.msg" "materialInfo1.sg";
connectAttr "aiStandardSurface1.msg" "materialInfo1.m";
connectAttr "aiStandardSurface1.msg" "materialInfo1.t" -na;
connectAttr "aiStandardSurface1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface1.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "aiSkyDomeLightShape1.ltd" ":lightList1.l" -na;
connectAttr "aiSkyDomeLight1.iog" ":defaultLightSet.dsm" -na;
// End of coffeepotUPDATEUV.ma
