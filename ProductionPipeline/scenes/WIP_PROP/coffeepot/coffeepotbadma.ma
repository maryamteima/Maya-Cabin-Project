//Maya ASCII 2024 scene
//Name: coffeepotbadma.ma
//Last modified: Thu, Oct 12, 2023 03:22:48 PM
//Codeset: 1252
requires maya "2024";
requires "stereoCamera" "10.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "5.3.1.1";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2024";
fileInfo "version" "2024";
fileInfo "cutIdentifier" "202304191415-7fa20164c6";
fileInfo "osv" "Windows 11 Enterprise v2009 (Build: 22000)";
fileInfo "UUID" "D7355237-4D8B-BB9C-C267-8DBA138C3311";
fileInfo "license" "education";
createNode transform -n "coffeepot_geo";
	rename -uid "26EACDB3-4C95-ED0E-BD21-0F8D7153B89E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 2.7349776386277841e-13 0 ;
	setAttr ".s" -type "double3" 0.19761545286781518 0.16641687044714357 0.15364310260719224 ;
	setAttr ".rp" -type "double3" 0 1.3151159552047907e-13 0 ;
	setAttr ".sp" -type "double3" 0 7.9025398787467825e-13 0 ;
	setAttr ".spt" -type "double3" 0 -6.587423923541992e-13 0 ;
createNode mesh -n "coffeepot_geoShape" -p "coffeepot_geo";
	rename -uid "5D9AAD65-4738-1015-36A2-9FBDBB18F43D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10.734674 0.5 14.788734 
		10.734674 0.5 14.788734 -10.734674 38.869171 14.788734 10.734674 38.869171 14.788734 
		-10.734674 38.869171 -14.788734 10.734674 38.869171 -14.788734 -10.734674 0.5 -14.788734 
		10.734674 0.5 -14.788734;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "coffeepot_grp";
	rename -uid "A91EF1DA-4795-1B9F-6F9D-889985E837A3";
	setAttr ".s" -type "double3" 1 0.51250530284889273 1 ;
createNode transform -n "POS" -p "coffeepot_grp";
	rename -uid "7186632F-4625-6650-261D-BFA739ADE9CB";
createNode transform -n "MOV" -p "POS";
	rename -uid "45388B0A-4C56-633E-9A51-889783376EE8";
	setAttr ".s" -type "double3" 1 1 0.77748526432274445 ;
createNode transform -n "ADJ" -p "MOV";
	rename -uid "BDAA880E-4133-B70A-9EF2-E9804852388E";
createNode transform -n "coffepot_geo_grp" -p "ADJ";
	rename -uid "3A37437C-4783-BFB9-D335-6A9FC839269B";
createNode transform -n "coffeepot_geo1" -p "coffepot_geo_grp";
	rename -uid "07E59DDE-4E1C-EF3E-9E8D-59A11FC37993";
	setAttr ".v" no;
	setAttr ".s" -type "double3" 0.19761545286781518 0.32471248496761407 0.19761545286781532 ;
	setAttr ".rp" -type "double3" 0 7.9025398787467846e-13 0 ;
	setAttr ".sp" -type "double3" 0 7.9025398787467825e-13 0 ;
	setAttr ".spt" -type "double3" 0 2.0194839173657911e-28 0 ;
createNode mesh -n "coffeepot_geo1Shape" -p "coffeepot_geo1";
	rename -uid "E044AA57-4186-6253-A2D6-8E9BA59B2050";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10.734674 0.5 14.788734 
		10.734674 0.5 14.788734 -10.734674 38.869171 14.788734 10.734674 38.869171 14.788734 
		-10.734674 38.869171 -14.788734 10.734674 38.869171 -14.788734 -10.734674 0.5 -14.788734 
		10.734674 0.5 -14.788734;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -s -n "persp";
	rename -uid "D5ECEF3E-4C57-7A68-064A-FCB19D274783";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 16.885197108577739 5.7179140943987683 -1.0248630594551265 ;
	setAttr ".r" -type "double3" -11.138352729875537 454.59999999990259 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "D12565E8-4E2A-9BA1-15DD-81AE37380304";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 17.43959821435061;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "4C2756DD-42CE-6CC2-F414-A5AF9CD98CCD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "AD378600-4135-0C2B-BDAC-28A13EF905A0";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "AF0B3000-4954-E7D3-27F1-549B9B02EA64";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.21092824712573505 3.3690839188710315 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "9ECE856A-4F4E-DED3-5620-679D55F9B748";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 4.967218340453285;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "B57EAA17-4EC1-0483-F72B-648E374A20A5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "4CD1B68A-404F-0BBB-042B-23B9A9C609C4";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCylinder1";
	rename -uid "E23275B8-45CC-2724-2987-969CA416842D";
	setAttr ".t" -type "double3" 0 1.2797302381744078 0 ;
	setAttr ".s" -type "double3" 2.4743144126697452 1.2542508650987299 2.4743144126697452 ;
	setAttr ".rp" -type "double3" 0 -1.2797302381744078 0 ;
	setAttr ".sp" -type "double3" 0 -1.1078584810113574 0 ;
	setAttr ".spt" -type "double3" 0 -0.17187175716305564 0 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "00EA50F2-4733-34AF-2992-82A4B1933132";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.84375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCylinder2";
	rename -uid "7776DD50-4C12-7C3E-EAFB-F1AE785C1FF8";
	setAttr ".t" -type "double3" 0.02930512295252663 3.6383040869709418 0 ;
	setAttr ".s" -type "double3" 2.4743144126697452 1.2542508650987299 2.4743144126697452 ;
	setAttr ".rp" -type "double3" 0 -1.2797302381744078 0 ;
	setAttr ".sp" -type "double3" 0 -1.1078584810113574 0 ;
	setAttr ".spt" -type "double3" 0 -0.17187175716305564 0 ;
createNode mesh -n "pCylinderShape2" -p "pCylinder2";
	rename -uid "99B76433-4CA3-B8C7-A4C5-96B58C8C5BA0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 6 "f[8:14]" "f[64:70]" "f[72]" "f[74:79]" "f[88]" "f[90:95]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 3 "f[0:7]" "f[56:63]" "f[80:87]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 2 "f[15:54]" "f[96:119]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.1562500074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 144 ".uvst[0].uvsp[0:143]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.61048543 0.95423543 0.5 1 0.38951457 0.95423543 0.34375 0.84375
		 0.38951457 0.73326457 0.5 0.6875 0.61048543 0.73326457 0.65625 0.84375 0.61048543
		 0.95423543 0.5 1 0.38951457 0.95423543 0.34375 0.84375 0.38951457 0.73326457 0.5
		 0.6875 0.61048543 0.73326457 0.65625 0.84375 0.61048543 0.95423543 0.65625 0.84375
		 0.61048543 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457
		 0.95423543 0.5 1 0.65625 0.84375 0.61048543 0.95423543 0.61048543 0.95423543 0.65625
		 0.84375 0.61048543 0.73326457 0.61048543 0.73326457 0.5 0.6875 0.5 0.6875 0.38951457
		 0.73326457 0.38951457 0.73326457 0.34375 0.84375 0.34375 0.84375 0.38951457 0.95423543
		 0.38951457 0.95423543 0.5 1 0.5 1 0.625 0.34999999 0.37499997 0.34999999 0.59375
		 0.34999999 0.56249994 0.34999999 0.53125 0.34999999 0.5 0.34999999 0.46875 0.34999999
		 0.43749997 0.34999999 0.40625 0.34999999 0.5 1.4901161e-08 0.61048543 0.04576458
		 0.34375 0.15625 0.38951457 0.04576458 0.38951457 0.26673543 0.5 0.3125 0.61048543
		 0.26673543 0.65625 0.15625 0.4375 0.3125 0.61048543 0.04576458 0.5 1.4901161e-08
		 0.38951457 0.04576458 0.4375 0.3125 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125
		 0.61048543 0.26673543 0.65625 0.15625 0.625 0.31625 0.37499997 0.31625 0.59375 0.31625
		 0.56249994 0.31625 0.53125 0.31625 0.5 0.31625 0.46875 0.31625 0.43749997 0.31625
		 0.40625 0.31625 0.61048543 0.04576458 0.5 1.4901161e-08 0.38951457 0.04576458 0.4375
		 0.3125 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543 0.26673543 0.65625
		 0.15625 0.61048543 0.95423543 0.65625006 0.84375 0.61048543 0.73326463 0.5 0.6875
		 0.38951457 0.73326463 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543
		 0.65625 0.84375 0.61048543 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375
		 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625006 0.84375 0.61048543 0.73326463
		 0.5 0.6875 0.38951457 0.73326463 0.34375 0.84375 0.38951457 0.95423543 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 49 ".pt";
	setAttr ".pt[0]" -type "float3" -0.114187 0.83771306 0.11093815 ;
	setAttr ".pt[1]" -type "float3" -0.0032488052 0.83771306 0.15689023 ;
	setAttr ".pt[2]" -type "float3" 0.1119258 0.83771306 0.11630522 ;
	setAttr ".pt[3]" -type "float3" 0.16013907 0.83771306 -9.1884853e-05 ;
	setAttr ".pt[4]" -type "float3" 0.10768929 0.83771306 -0.11093815 ;
	setAttr ".pt[5]" -type "float3" -0.0032488052 0.83771306 -0.15689023 ;
	setAttr ".pt[6]" -type "float3" -0.11418701 0.83771306 -0.11093816 ;
	setAttr ".pt[7]" -type "float3" -0.16013907 0.83771306 -9.2716967e-10 ;
	setAttr ".pt[56]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[57]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[58]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[59]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[60]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[61]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[62]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[63]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[64]" -type "float3" -0.114187 0.55832016 0.11093814 ;
	setAttr ".pt[65]" -type "float3" -0.0032488049 0.55832016 0.15689026 ;
	setAttr ".pt[66]" -type "float3" -0.0044712671 0.55832016 -9.1884853e-05 ;
	setAttr ".pt[67]" -type "float3" 0.1119258 0.55832016 0.11630524 ;
	setAttr ".pt[68]" -type "float3" 0.1601391 0.55832016 -9.1884853e-05 ;
	setAttr ".pt[69]" -type "float3" 0.10768931 0.55832016 -0.11093814 ;
	setAttr ".pt[70]" -type "float3" -0.0032488049 0.55832016 -0.15689026 ;
	setAttr ".pt[71]" -type "float3" -0.11418699 0.55832016 -0.11093815 ;
	setAttr ".pt[72]" -type "float3" -0.1601391 0.55832016 -9.2716967e-10 ;
	setAttr ".pt[73]" -type "float3" -0.114187 0.84135193 0.11093815 ;
	setAttr ".pt[74]" -type "float3" -0.0032488052 0.84135193 0.15689023 ;
	setAttr ".pt[75]" -type "float3" 0.1119258 0.84135193 0.11630522 ;
	setAttr ".pt[76]" -type "float3" 0.16013907 0.84135193 -9.1884853e-05 ;
	setAttr ".pt[77]" -type "float3" 0.10768929 0.84135193 -0.11093815 ;
	setAttr ".pt[78]" -type "float3" -0.0032488052 0.84135193 -0.15689023 ;
	setAttr ".pt[79]" -type "float3" -0.11418701 0.84135193 -0.11093816 ;
	setAttr ".pt[80]" -type "float3" -0.16013907 0.84135193 -9.2716967e-10 ;
	setAttr ".pt[81]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[82]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[83]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[84]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[85]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[86]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[87]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[88]" -type "float3" 0 0.86156863 0 ;
	setAttr ".pt[89]" -type "float3" -0.114187 0.55577457 0.11093814 ;
	setAttr ".pt[90]" -type "float3" -0.0032488052 0.55577457 0.15689026 ;
	setAttr ".pt[91]" -type "float3" 0.1119258 0.55577457 0.11630524 ;
	setAttr ".pt[92]" -type "float3" 0.1601391 0.55577457 -9.1884853e-05 ;
	setAttr ".pt[93]" -type "float3" 0.10768931 0.55577457 -0.11093814 ;
	setAttr ".pt[94]" -type "float3" -0.0032488052 0.55577457 -0.15689026 ;
	setAttr ".pt[95]" -type "float3" -0.11418699 0.55577457 -0.11093815 ;
	setAttr ".pt[96]" -type "float3" -0.1601391 0.55577457 -9.2716967e-10 ;
	setAttr -s 121 ".vt[0:120]"  0.58666706 -1 -0.59009373 -0.0034266931 -1 -0.83451855
		 -0.61605471 -1 -0.61864167 -0.87250692 -1 0.00048874685 -0.59352052 -1 0.59009373
		 -0.0034266931 -1 0.83451855 0.58666724 -1 0.59009385 0.831092 -1 4.9317301e-09 0.70710671 0.74698615 -0.70710671
		 0 0.74698615 -0.99999994 -0.70710671 0.74698615 -0.70710671 -0.99999994 0.74698615 0
		 -0.70710671 0.74698615 0.70710671 0 0.74698615 0.99999994 0.70710677 0.74698615 0.70710677
		 1 0.74698615 2.6645353e-15 0.55923408 1.19366026 -0.55085653 0.007791827 1.19366026 -0.77927148
		 -0.54365039 1.19366026 -0.55085653 -0.77206522 1.19366026 0.00058566954 -0.54365039 1.19366026 0.55202794
		 0.007791834 1.19366026 0.78044271 0.55923408 1.19366026 0.55202782 0.78764892 1.19366026 0.00058567856
		 0.51473516 1.70832944 -0.50635761 0.0077918284 1.70832944 -0.71634042 -0.4991515 1.70832944 -0.50635761
		 -0.70913422 1.70832944 0.00058566511 -0.4991515 1.70832944 0.50752902 0.0077918344 1.70832944 0.71751171
		 0.51473516 1.70832944 0.50752902 0.72471792 1.70832944 0.00058567344 0.51473516 1.62083292 -0.50635761
		 0.72471792 1.62083292 0.00058567344 0.51473516 1.62083292 0.50752902 0.0077918344 1.62083292 0.71751171
		 -0.4991515 1.62083292 0.50752902 -0.70913422 1.62083292 0.00058566511 -0.4991515 1.62083292 -0.50635761
		 0.0077918284 1.62083292 -0.71634042 0.55446815 1.60681748 -0.55446815 0.78413641 1.60681748 2.2063283e-08
		 0.55446815 1.72234464 -0.55446815 0.78413641 1.72234464 2.2063283e-08 0.55446821 1.60681748 0.55446815
		 0.55446821 1.72234464 0.55446815 8.8327106e-09 1.60681748 0.7841363 8.8327106e-09 1.72234464 0.7841363
		 -0.55446815 1.60681748 0.55446815 -0.55446815 1.72234464 0.55446815 -0.78413641 1.60681748 1.3230573e-08
		 -0.78413641 1.72234464 1.3230573e-08 -0.55446815 1.60681748 -0.55446815 -0.55446815 1.72234464 -0.55446815
		 0 1.60681748 -0.78413641 0 1.72234464 -0.78413641 0.70710671 -0.5469867 -0.70710671
		 1 -0.5469867 2.6645353e-15 0.70710671 -0.5469867 0.70710671 0 -0.5469867 0.99999994
		 -0.70710671 -0.5469867 0.70710671 -1.037273288 -0.5469867 0.0005270915 -0.73140907 -0.5469867 -0.73789465
		 0 -0.5469867 -0.99999982 0.58666706 -1.11151743 -0.59009373 -0.0034266931 -1.11151743 -0.83451855
		 0.003075734 -1.11151743 0.00048874685 -0.61605471 -1.11151743 -0.61864167 -0.87250692 -1.11151743 0.00048874685
		 -0.59352052 -1.11151743 0.59009373 -0.0034266931 -1.11151743 0.83451855 0.58666724 -1.11151743 0.59009385
		 0.831092 -1.11151743 4.9317301e-09 0.58666706 -1.033455253 -0.59009373 -0.0034266929 -1.033455253 -0.83451855
		 -0.61605471 -1.033455253 -0.61864167 -0.87250698 -1.033455253 0.00048874685 -0.59352052 -1.033455253 0.59009373
		 -0.0034266929 -1.033455253 0.83451855 0.58666724 -1.033455253 0.59009385 0.831092 -1.033455253 4.9317301e-09
		 0.59871101 -0.97999996 -0.60179502 0.84798282 -0.97999996 4.4385575e-09 0.59871113 -0.97999996 0.60179514
		 -0.0030840237 -0.97999996 0.85106671 -0.60487914 -0.97999996 0.60179502 -0.88898355 -0.97999996 0.00049258128
		 -0.62759012 -0.97999996 -0.63056695 -0.0030840237 -0.97999996 -0.85106665 0.58666706 -1.088098764 -0.59009373
		 -0.0034266929 -1.088098764 -0.83451855 -0.61605471 -1.088098764 -0.61864167 -0.87250698 -1.088098764 0.00048874685
		 -0.59352052 -1.088098764 0.59009373 -0.0034266929 -1.088098764 0.83451855 0.58666724 -1.088098764 0.59009385
		 0.831092 -1.088098764 4.9317301e-09 0.50931209 1.24357104 -0.50093454 0.71704853 1.24357104 0.00058567291
		 0.50931209 1.24357104 0.50210583 0.0077918349 1.24357104 0.70984232 -0.49372831 1.24357104 0.50210589
		 -0.70146477 1.24357104 0.00058566465 -0.49372831 1.24357104 -0.50093454 0.0077918288 1.24357104 -0.70867109
		 0.5292809 1.22360671 -0.52090335 0.74528867 1.22360671 0.00058567518 0.5292809 1.22360671 0.52207464
		 0.0077918344 1.22360671 0.73808247 -0.51369715 1.22360671 0.5220747 -0.72970498 1.22360671 0.00058566662
		 -0.51369715 1.22360671 -0.52090335 0.0077918279 1.22360671 -0.73691124 0.51039672 1.28349972 -0.50201917
		 0.71858245 1.28349972 0.00058567303 0.51039672 1.28349972 0.50319046 0.0077918349 1.28349972 0.71137625
		 -0.49481297 1.28349972 0.50319052 -0.7029987 1.28349972 0.00058566476 -0.49481297 1.28349972 -0.50201917
		 0.0077918288 1.28349972 -0.71020502;
	setAttr -s 240 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 0 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 81 0 1 88 0 2 87 0 3 86 0
		 4 85 0 5 84 0 6 83 0 7 82 0 8 16 1 9 17 1 16 17 0 10 18 1 17 18 0 11 19 1 18 19 0
		 12 20 1 19 20 0 13 21 1 20 21 0 14 22 1 21 22 0 15 23 1 22 23 0 23 16 0 16 105 0
		 17 112 0 24 25 0 18 111 0 25 26 0 19 110 0 26 27 0 20 109 0 27 28 0 21 108 0 28 29 0
		 22 107 0 29 30 0 23 106 0 30 31 0 31 24 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0
		 37 38 0 38 39 0 39 32 0 32 40 0 33 41 0 40 41 0 24 42 0 40 42 0 31 43 0 43 42 0 41 43 0
		 34 44 0 41 44 0 30 45 0 45 43 0 44 45 0 35 46 0 44 46 0 29 47 0 47 45 0 46 47 0 36 48 0
		 46 48 0 28 49 0 49 47 0 48 49 0 37 50 0 48 50 0 27 51 0 51 49 0 50 51 0 38 52 0 50 52 0
		 26 53 0 53 51 0 52 53 0 39 54 0 52 54 0 25 55 0 55 53 0 54 55 0 54 40 0 42 55 0 56 8 0
		 57 15 0 58 14 0 59 13 0 60 12 0 61 11 0 62 10 0 63 9 0 56 57 1 57 58 1 58 59 1 59 60 1
		 60 61 1 61 62 1 62 63 1 63 56 1 0 73 0 1 74 0 64 65 0 66 64 1 66 65 0 2 75 0 3 76 0
		 67 68 0 66 67 0 66 68 1 4 77 0 68 69 0 66 69 1 5 78 0 69 70 0 66 70 1 6 79 0 70 71 0
		 66 71 1 7 80 0 71 72 0 66 72 1 72 64 0 65 67 0 73 89 0 74 90 0 75 91 0 76 92 0 77 93 0
		 78 94 0 79 95 0 80 96 0 73 74 1 74 75 1 75 76 1 76 77 1 77 78 1 78 79 1 79 80 1 80 73 1
		 81 56 0 82 57 0 83 58 0 84 59 0 85 60 0 86 61 0;
	setAttr ".ed[166:239]" 87 62 0 88 63 0 81 82 1 82 83 1 83 84 1 84 85 1 85 86 1
		 86 87 1 87 88 1 88 81 1 89 64 0 90 65 0 91 67 0 92 68 0 93 69 0 94 70 0 95 71 0 96 72 0
		 89 90 1 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 89 1 97 113 0 98 114 0
		 99 115 0 100 116 0 101 117 0 102 118 0 103 119 0 104 120 0 97 98 1 98 99 1 99 100 1
		 100 101 1 101 102 1 102 103 1 103 104 1 104 97 1 105 97 0 106 98 0 107 99 0 108 100 0
		 109 101 0 110 102 0 111 103 0 112 104 0 105 106 1 106 107 1 107 108 1 108 109 1 109 110 1
		 110 111 1 111 112 1 112 105 1 113 32 0 114 33 0 115 34 0 116 35 0 117 36 0 118 37 0
		 119 38 0 120 39 0 113 114 1 114 115 1 115 116 1 116 117 1 117 118 1 118 119 1 119 120 1
		 120 113 1;
	setAttr -s 120 -ch 472 ".fc[0:119]" -type "polyFaces" 
		f 4 0 17 175 -17
		mu 0 4 8 9 110 103
		f 4 1 18 174 -18
		mu 0 4 9 10 109 110
		f 4 2 19 173 -19
		mu 0 4 10 11 108 109
		f 4 3 20 172 -20
		mu 0 4 11 12 107 108
		f 4 4 21 171 -21
		mu 0 4 12 13 106 107
		f 4 5 22 170 -22
		mu 0 4 13 14 105 106
		f 4 6 23 169 -23
		mu 0 4 14 15 104 105
		f 4 7 16 168 -24
		mu 0 4 15 16 102 104
		f 3 -123 -124 124
		mu 0 3 84 85 34
		f 3 -128 -129 129
		mu 0 3 86 87 34
		f 3 -132 -130 132
		mu 0 3 88 86 34
		f 3 -135 -133 135
		mu 0 3 89 88 34
		f 3 -138 -136 138
		mu 0 3 90 89 34
		f 3 -141 -139 141
		mu 0 3 91 90 34
		f 3 -143 -142 123
		mu 0 3 85 91 34
		f 4 8 25 -27 -25
		mu 0 4 32 31 36 35
		f 4 9 27 -29 -26
		mu 0 4 31 30 37 36
		f 4 10 29 -31 -28
		mu 0 4 30 29 38 37
		f 4 11 31 -33 -30
		mu 0 4 29 28 39 38
		f 4 12 33 -35 -32
		mu 0 4 28 27 40 39
		f 4 13 35 -37 -34
		mu 0 4 27 26 41 40
		f 4 14 37 -39 -36
		mu 0 4 26 33 42 41
		f 4 15 24 -40 -38
		mu 0 4 33 32 35 42
		f 4 26 41 223 -41
		mu 0 4 35 36 135 128
		f 4 28 43 222 -42
		mu 0 4 36 37 134 135
		f 4 30 45 221 -44
		mu 0 4 37 38 133 134
		f 4 32 47 220 -46
		mu 0 4 38 39 132 133
		f 4 34 49 219 -48
		mu 0 4 39 40 131 132
		f 4 36 51 218 -50
		mu 0 4 40 41 130 131
		f 4 38 53 217 -52
		mu 0 4 41 42 129 130
		f 4 39 40 216 -54
		mu 0 4 42 35 128 129
		f 4 -67 68 -71 -72
		mu 0 4 59 60 61 62
		f 4 -74 71 -76 -77
		mu 0 4 63 59 62 64
		f 4 -79 76 -81 -82
		mu 0 4 65 63 64 66
		f 4 -84 81 -86 -87
		mu 0 4 67 65 66 68
		f 4 -89 86 -91 -92
		mu 0 4 69 67 68 70
		f 4 -94 91 -96 -97
		mu 0 4 71 69 70 72
		f 4 -99 96 -101 -102
		mu 0 4 73 71 72 74
		f 4 -103 101 -104 -69
		mu 0 4 60 73 74 61
		f 4 -57 64 66 -66
		mu 0 4 52 51 60 59
		f 4 -56 69 70 -68
		mu 0 4 43 50 62 61
		f 4 -58 65 73 -73
		mu 0 4 53 52 59 63
		f 4 -55 74 75 -70
		mu 0 4 50 49 64 62
		f 4 -59 72 78 -78
		mu 0 4 54 53 63 65
		f 4 -53 79 80 -75
		mu 0 4 49 48 66 64
		f 4 -60 77 83 -83
		mu 0 4 55 54 65 67
		f 4 -51 84 85 -80
		mu 0 4 48 47 68 66
		f 4 -61 82 88 -88
		mu 0 4 56 55 67 69
		f 4 -49 89 90 -85
		mu 0 4 47 46 70 68
		f 4 -62 87 93 -93
		mu 0 4 57 56 69 71
		f 4 -47 94 95 -90
		mu 0 4 46 45 72 70
		f 4 -63 92 98 -98
		mu 0 4 58 57 71 73
		f 4 -45 99 100 -95
		mu 0 4 45 44 74 72
		f 4 -64 97 102 -65
		mu 0 4 51 58 73 60
		f 4 -43 67 103 -100
		mu 0 4 44 43 61 74
		f 3 -144 -125 128
		mu 0 3 92 84 34
		f 4 -113 104 -16 -106
		mu 0 4 77 75 25 24
		f 4 -114 105 -15 -107
		mu 0 4 78 77 24 23
		f 4 -115 106 -14 -108
		mu 0 4 79 78 23 22
		f 4 -116 107 -13 -109
		mu 0 4 80 79 22 21
		f 4 -117 108 -12 -110
		mu 0 4 81 80 21 20
		f 4 -118 109 -11 -111
		mu 0 4 82 81 20 19
		f 4 -119 110 -10 -112
		mu 0 4 83 82 19 18
		f 4 -120 111 -9 -105
		mu 0 4 76 83 18 17
		f 4 -1 120 152 -122
		mu 0 4 1 0 93 94
		f 4 -3 125 154 -127
		mu 0 4 3 2 95 97
		f 4 -4 126 155 -131
		mu 0 4 4 3 97 98
		f 4 -5 130 156 -134
		mu 0 4 5 4 98 99
		f 4 -6 133 157 -137
		mu 0 4 6 5 99 100
		f 4 -7 136 158 -140
		mu 0 4 7 6 100 101
		f 4 -8 139 159 -121
		mu 0 4 0 7 101 93
		f 4 -2 121 153 -126
		mu 0 4 10 1 94 96
		f 4 -153 144 184 -146
		mu 0 4 94 93 111 112
		f 4 -154 145 185 -147
		mu 0 4 96 94 112 114
		f 4 -155 146 186 -148
		mu 0 4 97 95 113 115
		f 4 -156 147 187 -149
		mu 0 4 98 97 115 116
		f 4 -157 148 188 -150
		mu 0 4 99 98 116 117
		f 4 -158 149 189 -151
		mu 0 4 100 99 117 118
		f 4 -159 150 190 -152
		mu 0 4 101 100 118 119
		f 4 -160 151 191 -145
		mu 0 4 93 101 119 111
		f 4 -169 160 112 -162
		mu 0 4 104 102 75 77
		f 4 -170 161 113 -163
		mu 0 4 105 104 77 78
		f 4 -171 162 114 -164
		mu 0 4 106 105 78 79
		f 4 -172 163 115 -165
		mu 0 4 107 106 79 80
		f 4 -173 164 116 -166
		mu 0 4 108 107 80 81
		f 4 -174 165 117 -167
		mu 0 4 109 108 81 82
		f 4 -175 166 118 -168
		mu 0 4 110 109 82 83
		f 4 -176 167 119 -161
		mu 0 4 103 110 83 76
		f 4 -185 176 122 -178
		mu 0 4 112 111 85 84
		f 4 -186 177 143 -179
		mu 0 4 114 112 84 92
		f 4 -187 178 127 -180
		mu 0 4 115 113 87 86
		f 4 -188 179 131 -181
		mu 0 4 116 115 86 88
		f 4 -189 180 134 -182
		mu 0 4 117 116 88 89
		f 4 -190 181 137 -183
		mu 0 4 118 117 89 90
		f 4 -191 182 140 -184
		mu 0 4 119 118 90 91
		f 4 -192 183 142 -177
		mu 0 4 111 119 91 85
		f 4 -201 192 232 -194
		mu 0 4 121 120 136 137
		f 4 -202 193 233 -195
		mu 0 4 122 121 137 138
		f 4 -203 194 234 -196
		mu 0 4 123 122 138 139
		f 4 -204 195 235 -197
		mu 0 4 124 123 139 140
		f 4 -205 196 236 -198
		mu 0 4 125 124 140 141
		f 4 -206 197 237 -199
		mu 0 4 126 125 141 142
		f 4 -207 198 238 -200
		mu 0 4 127 126 142 143
		f 4 -208 199 239 -193
		mu 0 4 120 127 143 136
		f 4 -217 208 200 -210
		mu 0 4 129 128 120 121
		f 4 -218 209 201 -211
		mu 0 4 130 129 121 122
		f 4 -219 210 202 -212
		mu 0 4 131 130 122 123
		f 4 -220 211 203 -213
		mu 0 4 132 131 123 124
		f 4 -221 212 204 -214
		mu 0 4 133 132 124 125
		f 4 -222 213 205 -215
		mu 0 4 134 133 125 126
		f 4 -223 214 206 -216
		mu 0 4 135 134 126 127
		f 4 -224 215 207 -209
		mu 0 4 128 135 127 120
		f 4 -233 224 56 -226
		mu 0 4 137 136 51 52
		f 4 -234 225 57 -227
		mu 0 4 138 137 52 53
		f 4 -235 226 58 -228
		mu 0 4 139 138 53 54
		f 4 -236 227 59 -229
		mu 0 4 140 139 54 55
		f 4 -237 228 60 -230
		mu 0 4 141 140 55 56
		f 4 -238 229 61 -231
		mu 0 4 142 141 56 57
		f 4 -239 230 62 -232
		mu 0 4 143 142 57 58
		f 4 -240 231 63 -225
		mu 0 4 136 143 58 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "35044D5E-4CA8-5C5C-FD87-A7BC763A9458";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "3972244A-4C60-4736-EA4F-058FBAACFC70";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "50C569A6-460D-624F-5806-B193B835ABFF";
createNode displayLayerManager -n "layerManager";
	rename -uid "6A94C799-4E27-BAC1-42A1-298B490B652A";
createNode displayLayer -n "defaultLayer";
	rename -uid "AE43359B-454D-F57F-362D-20BB544A1515";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "B0892276-4288-7470-259B-90BFF3192321";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "CAE4D9D0-48CF-E8D5-FF5D-70BBFA3CD622";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "EFC7D6E3-49F4-61F9-9839-8BBA0DDCC363";
	setAttr ".version" -type "string" "5.3.1.1";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "FDD6AE2F-4065-AAB0-373C-4093417C5A87";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "7748241B-4DEF-6050-40A3-9FAF479F904C";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "B07E0630-48C3-97E8-4F9F-7F99BB7FD5F8";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode polyCylinder -n "polyCylinder1";
	rename -uid "EC3FDD37-4E4D-6E8E-957B-C292A2BDDA38";
	setAttr ".sa" 8;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "D0E1B0A0-4F3F-298A-9BEE-008B7E302671";
	setAttr ".ics" -type "componentList" 2 "f[9]" "f[16:23]";
	setAttr ".ix" -type "matrix" 1.9359961149447997 0 0 0 0 1.1444486477112727 0 0 0 0 1.9359961149447997 0
		 0 1.1444486753692333 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.1539436e-07 1.1444486 5.7697182e-08 ;
	setAttr ".rs" 45684;
	setAttr ".ls" -type "double3" 0.30412277470266474 0.30412277470266474 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.9359958841560783 2.7657960588456376e-08 -1.9359958841560783 ;
	setAttr ".cbx" -type "double3" 1.9359961149447997 2.2888973230805059 1.935995999550439 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "5EA9714F-4761-140B-002E-C293FC351B9C";
	setAttr ".ics" -type "componentList" 2 "f[9]" "f[16:23]";
	setAttr ".ix" -type "matrix" 1.9359961149447997 0 0 0 0 1.1444486477112727 0 0 0 0 1.9359961149447997 0
		 0 1.1444486753692333 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.1444486 -5.7697182e-08 ;
	setAttr ".rs" 52448;
	setAttr ".lt" -type "double3" 0 0 0.72462382727644892 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.3228717197202438 2.7657960588456376e-08 -1.3228717197202438 ;
	setAttr ".cbx" -type "double3" 1.3228717197202438 2.2888971866515959 1.322871604325883 ;
createNode polySplit -n "polySplit1";
	rename -uid "84CBE425-48EC-DDE4-CB3D-BAA9CF0FB3D6";
	setAttr -s 9 ".e[0:8]"  0.69999999 0.69999999 0.69999999 0.69999999
		 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999;
	setAttr -s 9 ".d[0:8]"  -2147483588 -2147483568 -2147483571 -2147483574 -2147483577 -2147483580 
		-2147483583 -2147483587 -2147483588;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "84E6BBAF-41FB-FF9B-F535-1D80B81CEB40";
	setAttr ".ics" -type "componentList" 1 "f[46:53]";
	setAttr ".ix" -type "matrix" 1.9359961149447997 0 0 0 0 1.1444486477112727 0 0 0 0 1.9359961149447997 0
		 0 1.1444486753692333 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 2.9048271 -5.7697182e-08 ;
	setAttr ".rs" 32875;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.3228717197202438 2.7961335991234524 -1.3228717197202438 ;
	setAttr ".cbx" -type "double3" 1.3228717197202438 3.0135206525298068 1.322871604325883 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "C7F499B9-4D05-3871-89CC-0DB9719AAA96";
	setAttr ".uopa" yes;
	setAttr -s 46 ".tk";
	setAttr ".tk[2]" -type "float3" -0.027002657 0 -0.034208816 ;
	setAttr ".tk[3]" -type "float3" -0.041415006 0 0.00058565725 ;
	setAttr ".tk[16]" -type "float3" 0.0077918284 0 0.00058565725 ;
	setAttr ".tk[20]" -type "float3" 0.031567011 -1.1175871e-08 -0.023189494 ;
	setAttr ".tk[21]" -type "float3" 0.0077918284 -1.1175871e-08 -0.033037528 ;
	setAttr ".tk[22]" -type "float3" -0.015983332 -1.1175871e-08 -0.023189494 ;
	setAttr ".tk[23]" -type "float3" -0.025831353 -1.1175871e-08 0.00058565743 ;
	setAttr ".tk[24]" -type "float3" -0.015983332 -1.1175871e-08 0.024360837 ;
	setAttr ".tk[25]" -type "float3" 0.0077918265 -1.1175871e-08 0.034208827 ;
	setAttr ".tk[26]" -type "float3" 0.031567004 -1.1175871e-08 0.024360832 ;
	setAttr ".tk[27]" -type "float3" 0.041415017 -1.1175871e-08 0.00058565807 ;
	setAttr ".tk[31]" -type "float3" 0.03156703 0 -0.023189507 ;
	setAttr ".tk[32]" -type "float3" 0.0077918284 0 -0.033037521 ;
	setAttr ".tk[33]" -type "float3" 0.0077918284 0 0.0005856572 ;
	setAttr ".tk[34]" -type "float3" -0.015983343 0 -0.023189507 ;
	setAttr ".tk[35]" -type "float3" -0.025831345 0 0.00058565743 ;
	setAttr ".tk[36]" -type "float3" -0.015983343 0 0.024360837 ;
	setAttr ".tk[37]" -type "float3" 0.0077918265 0 0.034208816 ;
	setAttr ".tk[38]" -type "float3" 0.031567004 0 0.024360839 ;
	setAttr ".tk[39]" -type "float3" 0.041415013 0 0.00058565807 ;
	setAttr ".tk[40]" -type "float3" 0.031567011 0 -0.0231895 ;
	setAttr ".tk[41]" -type "float3" 0.041415017 0 0.00058565807 ;
	setAttr ".tk[42]" -type "float3" 0.031567004 0 0.024360837 ;
	setAttr ".tk[43]" -type "float3" 0.0077918265 0 0.034208827 ;
	setAttr ".tk[44]" -type "float3" -0.015983332 0 0.024360837 ;
	setAttr ".tk[45]" -type "float3" -0.025831353 0 0.00058565743 ;
	setAttr ".tk[46]" -type "float3" -0.015983332 0 -0.0231895 ;
	setAttr ".tk[47]" -type "float3" 0.0077918284 0 -0.033037528 ;
	setAttr ".tk[48]" -type "float3" 0.071300045 -0.014015193 -0.071300045 ;
	setAttr ".tk[49]" -type "float3" 0.10083347 -0.014015193 6.669489e-09 ;
	setAttr ".tk[50]" -type "float3" 0.071300045 0.014015193 -0.071300037 ;
	setAttr ".tk[51]" -type "float3" 0.10083347 0.014015193 6.669489e-09 ;
	setAttr ".tk[52]" -type "float3" 0.071300045 -0.014015193 0.071300045 ;
	setAttr ".tk[53]" -type "float3" 0.071300045 0.014015193 0.071300045 ;
	setAttr ".tk[54]" -type "float3" 1.1358133e-09 -0.014015193 0.10083347 ;
	setAttr ".tk[55]" -type "float3" 1.1358133e-09 0.014015193 0.10083347 ;
	setAttr ".tk[56]" -type "float3" -0.071300045 -0.014015193 0.071300045 ;
	setAttr ".tk[57]" -type "float3" -0.071300045 0.014015193 0.071300045 ;
	setAttr ".tk[58]" -type "float3" -0.10083347 -0.014015193 5.5336757e-09 ;
	setAttr ".tk[59]" -type "float3" -0.10083347 0.014015193 5.5336757e-09 ;
	setAttr ".tk[60]" -type "float3" -0.071300045 -0.014015193 -0.071300045 ;
	setAttr ".tk[61]" -type "float3" -0.071300045 0.014015193 -0.071300037 ;
	setAttr ".tk[62]" -type "float3" 0 -0.014015193 -0.10083347 ;
	setAttr ".tk[63]" -type "float3" 0 0.014015193 -0.10083347 ;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "0F216B8A-4DF5-172F-1567-B2B9D4416080";
	setAttr ".dc" -type "componentList" 2 "f[9]" "f[35:37]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "2A407794-4A84-B60E-3477-CE9CEBEE7FC9";
	setAttr ".dc" -type "componentList" 1 "e[32:33]";
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "3E0DE0FB-4F1B-E9D7-A4E6-1C8B2590DB24";
	setAttr ".ics" -type "componentList" 2 "e[1]" "e[25:26]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "8725EE36-443E-6447-FDAF-F88BD8F61248";
	setAttr ".dc" -type "componentList" 1 "f[23]";
createNode polyCloseBorder -n "polyCloseBorder2";
	rename -uid "0BEB76B8-4869-A332-2713-8182898AAF85";
	setAttr ".ics" -type "componentList" 2 "e[1]" "e[25:26]";
createNode polyTweak -n "polyTweak2";
	rename -uid "41445ADC-44D5-F5FE-57DA-EC997FAE2F24";
	setAttr ".uopa" yes;
	setAttr -s 34 ".tk";
	setAttr ".tk[17]" -type "float3" 0.044498939 0.19366066 -0.04449892 ;
	setAttr ".tk[18]" -type "float3" -1.6327606e-09 0.19366066 -0.062930979 ;
	setAttr ".tk[19]" -type "float3" -0.044498939 0.19366066 -0.04449892 ;
	setAttr ".tk[20]" -type "float3" -0.062931001 0.19366066 4.342732e-09 ;
	setAttr ".tk[21]" -type "float3" -0.044498939 0.19366066 0.044498939 ;
	setAttr ".tk[22]" -type "float3" -1.1028142e-09 0.19366066 0.062930979 ;
	setAttr ".tk[23]" -type "float3" 0.044498939 0.19366066 0.044498932 ;
	setAttr ".tk[24]" -type "float3" 0.062931001 0.19366066 5.0737099e-09 ;
	setAttr ".tk[25]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[26]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[27]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[28]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[29]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[30]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[31]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[32]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[33]" -type "float3" 0 -0.10245258 0 ;
	setAttr ".tk[42]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[43]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[44]" -type "float3" 0 -0.10245261 0 ;
	setAttr ".tk[45]" -type "float3" 0 -0.10245261 0 ;
	setAttr ".tk[46]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[47]" -type "float3" 0 -0.10245261 0 ;
	setAttr ".tk[48]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[49]" -type "float3" 0 -0.10245261 0 ;
	setAttr ".tk[50]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[51]" -type "float3" 0 -0.10245261 0 ;
	setAttr ".tk[52]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[53]" -type "float3" 0 -0.10245261 0 ;
	setAttr ".tk[54]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[55]" -type "float3" 0 -0.10245261 0 ;
	setAttr ".tk[56]" -type "float3" 0 2.2351742e-08 0 ;
	setAttr ".tk[57]" -type "float3" 0 -0.10245261 0 ;
createNode polySplit -n "polySplit2";
	rename -uid "95469D00-4F14-5F76-6E16-AABB6E8202C5";
	setAttr -s 9 ".e[0:8]"  0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1;
	setAttr -s 9 ".d[0:8]"  -2147483632 -2147483625 -2147483626 -2147483627 -2147483628 -2147483629 
		-2147483630 -2147483631 -2147483632;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "CCE7EC8A-4832-A408-7B29-AC9B57530B80";
	setAttr ".ics" -type "componentList" 2 "f[8:14]" "f[63]";
	setAttr ".ix" -type "matrix" 2.4743144126697452 0 0 0 0 1.1444486477112727 0 0 0 0 2.4743144126697452 0
		 0 1.1444486753692333 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.051236689 2.7657961e-08 0 ;
	setAttr ".rs" 52470;
	setAttr ".lt" -type "double3" 0 2.1606913489800949e-16 0.12762593892038754 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.1588562912262153 2.7657960588456376e-08 -2.0648612804245357 ;
	setAttr ".cbx" -type "double3" 2.0563829138734016 2.7657960588456376e-08 2.0648612804245357 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "4E663311-45CF-678B-E4A1-45B9D8753C1D";
	setAttr ".uopa" yes;
	setAttr -s 13 ".tk";
	setAttr ".tk[0]" -type "float3" -0.12043965 2.9802322e-08 0.11701296 ;
	setAttr ".tk[1]" -type "float3" -0.0034266929 2.9802322e-08 0.16548136 ;
	setAttr ".tk[2]" -type "float3" 0.11805466 2.9802322e-08 0.12267385 ;
	setAttr ".tk[3]" -type "float3" 0.16890801 2.9802322e-08 -9.6910408e-05 ;
	setAttr ".tk[4]" -type "float3" 0.11358625 2.9802322e-08 -0.11701295 ;
	setAttr ".tk[5]" -type "float3" -0.0034266929 2.9802322e-08 -0.16548136 ;
	setAttr ".tk[6]" -type "float3" -0.12043962 2.9802322e-08 -0.11701296 ;
	setAttr ".tk[7]" -type "float3" -0.16890801 2.9802322e-08 4.9317297e-09 ;
	setAttr ".tk[14]" -type "float3" -7.4505806e-09 -2.9802322e-08 -1.4901161e-08 ;
	setAttr ".tk[15]" -type "float3" 1.4901161e-08 -2.9802322e-08 2.6645353e-15 ;
	setAttr ".tk[16]" -type "float3" -0.0047160946 2.9802322e-08 -9.6910408e-05 ;
	setAttr ".tk[59]" -type "float3" 1.4901161e-08 4.4703484e-08 2.6645353e-15 ;
	setAttr ".tk[60]" -type "float3" -2.2351742e-08 4.4703484e-08 0 ;
createNode polySplit -n "polySplit3";
	rename -uid "A86D0D70-43C6-29AF-886A-A9827143FA7F";
	setAttr -s 9 ".e[0:8]"  0.30000001 0.30000001 0.30000001 0.30000001
		 0.30000001 0.30000001 0.30000001 0.30000001 0.30000001;
	setAttr -s 9 ".d[0:8]"  -2147483520 -2147483519 -2147483515 -2147483514 -2147483510 -2147483507 
		-2147483504 -2147483501 -2147483520;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "354DF090-45FC-040E-55F4-3FA25A88DEAC";
	setAttr -s 9 ".e[0:8]"  0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1;
	setAttr -s 9 ".d[0:8]"  -2147483632 -2147483625 -2147483626 -2147483627 -2147483628 -2147483629 
		-2147483630 -2147483631 -2147483632;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "EC7620A2-4122-7DB0-49A5-6AA1BAF29CF1";
	setAttr -s 9 ".e[0:8]"  0.69999999 0.69999999 0.69999999 0.69999999
		 0.69999999 0.69999999 0.69999999 0.69999999 0.69999999;
	setAttr -s 9 ".d[0:8]"  -2147483496 -2147483495 -2147483494 -2147483493 -2147483492 -2147483491 
		-2147483490 -2147483489 -2147483496;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit6";
	rename -uid "6EC74670-4F3A-5AF1-4096-428B92AD8F6C";
	setAttr -s 9 ".e[0:8]"  0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2;
	setAttr -s 9 ".d[0:8]"  -2147483608 -2147483588 -2147483591 -2147483594 -2147483597 -2147483600 
		-2147483603 -2147483607 -2147483608;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "652BCDAB-4510-B138-F8C0-DFAAD6CC507F";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[98:105]" -type "float3"  -0.041022263 0 0.041022263
		 -0.058014244 0 -4.6828368e-09 -0.041022263 0 -0.041022263 9.8592345e-10 0 -0.058014244
		 0.041022263 0 -0.041022263 0.058014244 0 -4.0094332e-09 0.041022263 0 0.041022263
		 1.4789322e-09 0 0.058014244;
createNode polySplit -n "polySplit7";
	rename -uid "C0A98E67-446A-4881-DC95-98BF4B232885";
	setAttr -s 9 ".e[0:8]"  0.60000002 0.60000002 0.60000002 0.60000002
		 0.60000002 0.60000002 0.60000002 0.60000002 0.60000002;
	setAttr -s 9 ".d[0:8]"  -2147483608 -2147483588 -2147483591 -2147483594 -2147483597 -2147483600 
		-2147483603 -2147483607 -2147483608;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit8";
	rename -uid "5B497C94-4830-0193-0171-CF997BB95C95";
	setAttr -s 9 ".e[0:8]"  0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2;
	setAttr -s 9 ".d[0:8]"  -2147483448 -2147483447 -2147483446 -2147483445 -2147483444 -2147483443 
		-2147483442 -2147483441 -2147483448;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "3D6032B1-4911-6210-A2E6-AB93E2B63E9A";
	setAttr ".uopa" yes;
	setAttr -s 49 ".tk";
	setAttr ".tk[8]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[9]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[10]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[11]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[12]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[13]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[14]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[15]" -type "float3" 0 -0.25301358 0 ;
	setAttr ".tk[24]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[25]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[26]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[28]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[29]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[30]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[31]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[32]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[33]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[36]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[37]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[38]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[39]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[40]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[41]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[42]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[43]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[44]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[45]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[48]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[49]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[50]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[51]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[52]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[53]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[54]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[55]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[56]" -type "float3" 0 0.17761852 0 ;
	setAttr ".tk[57]" -type "float3" 0 0.25301328 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.25301325 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.25301325 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.25301328 0 ;
	setAttr ".tk[61]" -type "float3" 0 0.25301328 0 ;
	setAttr ".tk[62]" -type "float3" 0 0.25301328 0 ;
	setAttr ".tk[63]" -type "float3" 0 0.25301328 0 ;
	setAttr ".tk[64]" -type "float3" 0 0.25301328 0 ;
createNode deleteComponent -n "deleteComponent4";
	rename -uid "EB7EF5A0-4F19-24E1-DA5D-DDA5FB432325";
	setAttr ".dc" -type "componentList" 1 "f[15:22]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "B2EBEA15-46E3-D1FE-3F76-16A1F057F675";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1017\n            -height 1590\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n"
		+ "            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n"
		+ "            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n"
		+ "            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 1\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n"
		+ "            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 1590\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n"
		+ "            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n"
		+ "            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 0\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n"
		+ "                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n"
		+ "                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"|persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n"
		+ "                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n"
		+ "                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -bluePencil 1\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"|persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -excludeObjectPreset \\\"All\\\" \\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1017\\n    -height 1590\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"|persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -excludeObjectPreset \\\"All\\\" \\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1017\\n    -height 1590\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "3699C102-4D26-62D5-C6D2-239149D59065";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
	setAttr ".rtfm" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :standardSurface1;
	setAttr ".bc" -type "float3" 0.40000001 0.40000001 0.40000001 ;
	setAttr ".sr" 0.5;
select -ne :initialShadingGroup;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "standardSurface1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "deleteComponent4.og" "pCylinderShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "polyCylinder1.out" "polyExtrudeFace1.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace2.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polyExtrudeFace3.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyCloseBorder1.ip";
connectAttr "polyCloseBorder1.out" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "polyCloseBorder2.ip";
connectAttr "polyCloseBorder2.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "polySplit2.ip";
connectAttr "polyTweak3.out" "polyExtrudeFace4.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polySplit2.out" "polyTweak3.ip";
connectAttr "polyExtrudeFace4.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polyTweak5.ip";
connectAttr "polyTweak5.out" "deleteComponent4.ig";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "coffeepot_geoShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "coffeepot_geo1Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog" ":initialShadingGroup.dsm" -na;
// End of coffeepotbadma.ma
