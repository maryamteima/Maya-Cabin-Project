//Maya ASCII 2023 scene
//Name: daybed_005.ma
//Last modified: Fri, Oct 27, 2023 06:23:18 PM
//Codeset: 1252
requires maya "2023";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "5.3.3.3";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202202161415-df43006fd3";
fileInfo "osv" "Windows 11 Home v2009 (Build: 22621)";
fileInfo "UUID" "A8822CD3-417D-0D06-4BCC-90AE8739CE58";
fileInfo "license" "education";
createNode transform -n "daybed_grp";
	rename -uid "9735EB3A-439A-B46C-80AB-B5940348A8DF";
createNode transform -n "POS" -p "daybed_grp";
	rename -uid "9682279B-4B2E-E445-0900-6A8B48E07FED";
createNode transform -n "MOV" -p "|daybed_grp|POS";
	rename -uid "7770C8F2-4F39-016C-BF06-EB86200FE420";
createNode transform -n "ADJ" -p "|daybed_grp|POS|MOV";
	rename -uid "977926AA-4DB4-E0B0-7018-FF892DB37D4A";
createNode transform -n "daybed_geo_grp" -p "|daybed_grp|POS|MOV|ADJ";
	rename -uid "42AA6971-4306-FA15-1335-18BC2336242F";
createNode transform -n "board_R_grp" -p "|daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "E47E84FC-4B25-AC74-45D5-7195D5915993";
	setAttr ".s" -type "double3" -1 1 1 ;
createNode transform -n "board_L_grp" -p "|daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "BD24394B-4E9C-1DAD-5A1F-CCAE66B231A9";
createNode transform -n "pillow_grp" -p "|daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "ACE8E452-48D0-CBEC-F1EB-C7A5F0DC5B0D";
createNode transform -s -n "persp";
	rename -uid "236D88FC-41C6-4BCA-5D9E-F29E5FEFB207";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 124.98841455582948 29.509494416101379 98.106604072054736 ;
	setAttr ".r" -type "double3" 1.4616472591165497 -1748.5999999977153 -1.5931332636126973e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "A1A37277-4FAF-96EE-6462-D2A6DDF79603";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 169.99895621917301;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "3087FDA7-483C-28E4-B7C5-788D7B51D9BD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "1AB8AC25-46F4-D6F9-7326-4289EDD09269";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 64.356708157656755;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "355EC5F4-471E-D503-7726-EE894C0661C7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -18.15177906513598 16.46722598130178 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "4C9B7645-43E5-243E-F3AD-2983325650A6";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 129.71099432915895;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "689689DB-4768-CD2D-73A2-22B270B4F85C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 48.236539975777447 0.10744481486193402 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "2EBA4F15-4079-8E91-B170-3493D87E85E7";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 6.9114542962720185;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "left";
	rename -uid "F2DD7D98-434F-DD74-EF2E-40A07F18DD1F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1000.1 14.428214978638767 -4.7535360932770381 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
createNode camera -n "leftShape" -p "left";
	rename -uid "6300F0B5-48CC-087C-433E-DF82380C8B5D";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 107.7947355670992;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "daybed_003_daybed_grp";
	rename -uid "34432CE3-4C7C-F57B-DB01-5684EAAA0DA3";
createNode transform -n "POS" -p "daybed_003_daybed_grp";
	rename -uid "A6176C46-4BF2-86C6-796E-41B1D575FFED";
createNode transform -n "MOV" -p "|daybed_003_daybed_grp|POS";
	rename -uid "59C9EA08-4AA6-D52E-0572-38BD6B26DB1C";
createNode transform -n "ADJ" -p "|daybed_003_daybed_grp|POS|MOV";
	rename -uid "BC67673E-4F6B-2FCF-A49C-D8BE786FC35D";
createNode transform -n "daybed_geo_grp" -p "|daybed_003_daybed_grp|POS|MOV|ADJ";
	rename -uid "A131C9FC-409E-B039-2BCC-5C873AE12EBE";
createNode transform -n "backboard" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "0E5AF162-46CB-46B3-D0BC-808ECA3D0A57";
	setAttr ".rp" -type "double3" 0.15730165094147711 5.9365530014038139 -13.935576105548547 ;
	setAttr ".sp" -type "double3" 0.15730165094147711 5.9365530014038139 -13.935576105548547 ;
createNode mesh -n "backboardShape" -p "backboard";
	rename -uid "63A96966-451A-5E9C-75E5-16B517549C1B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.97270870208740234 0.41350451111793518 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 54 ".pt";
	setAttr ".pt[43]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[44]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[45]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[49]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[50]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[51]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[64]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[65]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[66]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[67]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[68]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[69]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[70]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[71]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[72]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[73]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[74]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[75]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[88]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[89]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[90]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[91]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[92]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[93]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[94]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[95]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[96]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[97]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[98]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[99]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[112]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[113]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[114]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[115]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[116]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[117]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[118]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[119]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[120]" -type "float3" 0 0.88952678 0 ;
	setAttr ".pt[121]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[122]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[123]" -type "float3" 0 1.6195019 0 ;
	setAttr ".pt[130]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[131]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[132]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[133]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[134]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[135]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[142]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[143]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[144]" -type "float3" 0 0.31579059 0 ;
	setAttr ".pt[145]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[146]" -type "float3" 0 0.47291502 0 ;
	setAttr ".pt[147]" -type "float3" 0 0.47291502 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape1" -p "backboard";
	rename -uid "081C5D51-47E5-B5F2-123A-749B8FAF0DF9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "baseboard_front" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "42BF1E02-46CA-0D91-AC44-FB9CD31F76ED";
	setAttr ".rp" -type "double3" 0 5.9365536481038124 15.554662414870045 ;
	setAttr ".sp" -type "double3" 0 5.9365536481037067 15.554662414870046 ;
createNode mesh -n "baseboard_frontShape" -p "baseboard_front";
	rename -uid "4E46C3FA-40DE-EA07-B377-1E8781648B8A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49995917826890945 0.49921416561119258 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape3" -p "baseboard_front";
	rename -uid "18A14FD0-4FFD-6CF9-7DDA-A1BC29941C45";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49995917826890945 0.49921416561119258 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -31.010706 6.436554 15.054663 
		-32.010708 13.61459 15.054663 -31.010706 5.436554 13.469717 -32.010708 12.61459 13.469717 
		32.010708 5.436554 14.469717 31.010706 12.61459 14.469717 32.010708 6.436554 16.054663 
		31.010706 13.61459 16.054663;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "mattress" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "B0542CCD-4A2E-61F4-516C-A396A632F81E";
	setAttr ".t" -type "double3" 0 -0.87667853992232025 -0.10159716074224967 ;
	setAttr ".s" -type "double3" 0.998 1.0672064754216544 0.994 ;
	setAttr ".rp" -type "double3" 0 11.643906082203634 -13.038650184764249 ;
	setAttr ".sp" -type "double3" 0 11.643906082203634 -13.038650184764347 ;
createNode mesh -n "mattressShape" -p "mattress";
	rename -uid "9B63E138-4DDF-3561-5466-A68ABB52D2FE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.37754493951797485 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 287 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[1]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[2]" -type "float3" 0 0.22236556 0 ;
	setAttr ".pt[3]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[4]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[5]" -type "float3" 0 0.21855265 0 ;
	setAttr ".pt[6]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[7]" -type "float3" 0 0.22536977 0 ;
	setAttr ".pt[8]" -type "float3" 0 0.23341517 0 ;
	setAttr ".pt[9]" -type "float3" 0 0.12064306 0 ;
	setAttr ".pt[11]" -type "float3" 0 -0.016626444 0 ;
	setAttr ".pt[14]" -type "float3" 0 0.12064306 0 ;
	setAttr ".pt[15]" -type "float3" 0 0.23341517 0 ;
	setAttr ".pt[16]" -type "float3" 0 0.22536977 0 ;
	setAttr ".pt[18]" -type "float3" 0 -0.016626444 0 ;
	setAttr ".pt[19]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[20]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[21]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[22]" -type "float3" 0 0.22236556 0 ;
	setAttr ".pt[23]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[24]" -type "float3" 0 0.21855265 0 ;
	setAttr ".pt[25]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[26]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[27]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[28]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[29]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[30]" -type "float3" 0 0.22236556 0 ;
	setAttr ".pt[31]" -type "float3" 0 0.12064306 0 ;
	setAttr ".pt[32]" -type "float3" 0 0.24103785 0 ;
	setAttr ".pt[33]" -type "float3" 0 0.23940943 0 ;
	setAttr ".pt[36]" -type "float3" 0 0.22536977 0 ;
	setAttr ".pt[37]" -type "float3" 0 0.23341517 0 ;
	setAttr ".pt[38]" -type "float3" 0 0.12064306 0 ;
	setAttr ".pt[39]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[40]" -type "float3" 0 0.22236556 0 ;
	setAttr ".pt[41]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[42]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[43]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[44]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[45]" -type "float3" 0 -0.18375675 0 ;
	setAttr ".pt[46]" -type "float3" 0 -0.014673167 0 ;
	setAttr ".pt[47]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[48]" -type "float3" 0 -0.17288573 0 ;
	setAttr ".pt[51]" -type "float3" 0 -0.10793575 0 ;
	setAttr ".pt[53]" -type "float3" 0 -0.17288573 0 ;
	setAttr ".pt[54]" -type "float3" 0 -0.10793575 0 ;
	setAttr ".pt[56]" -type "float3" 0 -0.18375675 0 ;
	setAttr ".pt[57]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[58]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[59]" -type "float3" 0 -0.014673167 0 ;
	setAttr ".pt[60]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[61]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[62]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[63]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[64]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[65]" -type "float3" 0 -0.072780751 0 ;
	setAttr ".pt[66]" -type "float3" 0 -0.061439119 0 ;
	setAttr ".pt[69]" -type "float3" 0 -0.061439119 0 ;
	setAttr ".pt[70]" -type "float3" 0 -0.072780751 0 ;
	setAttr ".pt[71]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[72]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[73]" -type "float3" 0 -0.45272332 0 ;
	setAttr ".pt[74]" -type "float3" 0 -0.2776953 0 ;
	setAttr ".pt[78]" -type "float3" 0 -0.45272332 0 ;
	setAttr ".pt[79]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[80]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[81]" -type "float3" 0 -0.11743796 0 ;
	setAttr ".pt[82]" -type "float3" 0 -0.14676136 0 ;
	setAttr ".pt[85]" -type "float3" 0 -0.14676136 0 ;
	setAttr ".pt[86]" -type "float3" 0 -0.11743796 0 ;
	setAttr ".pt[87]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[88]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[89]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[90]" -type "float3" 0 0.14242145 0 ;
	setAttr ".pt[93]" -type "float3" 0 0.14242145 0 ;
	setAttr ".pt[94]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[95]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[120]" -type "float3" 0 0.21960971 0 ;
	setAttr ".pt[121]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[124]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[125]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[126]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[127]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[128]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[129]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[130]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[131]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[132]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[133]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[136]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[137]" -type "float3" 0 0.21978553 0 ;
	setAttr ".pt[140]" -type "float3" 0 0.21960971 0 ;
	setAttr ".pt[141]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[144]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[145]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[146]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[147]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[148]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[149]" -type "float3" 0 0.21960971 0 ;
	setAttr ".pt[152]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[153]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[154]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[155]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[156]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[157]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[158]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[159]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[160]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[161]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[162]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[163]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[164]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[165]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[186]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[187]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[188]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[189]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[190]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[191]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[210]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[211]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[212]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[213]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[214]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[215]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[216]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[217]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[218]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[219]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[220]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[221]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[240]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[241]" -type "float3" 0 0.21953698 0 ;
	setAttr ".pt[242]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[243]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[244]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[245]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[246]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[247]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[248]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[249]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[250]" -type "float3" 0 0.22238091 0 ;
	setAttr ".pt[251]" -type "float3" 0 0.22252581 0 ;
	setAttr ".pt[270]" -type "float3" 0 0.22238091 0 ;
	setAttr ".pt[271]" -type "float3" 0 0.22252581 0 ;
	setAttr ".pt[272]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[273]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[274]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[275]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[276]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[277]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[278]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[279]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[280]" -type "float3" 0 0.2297212 0 ;
	setAttr ".pt[281]" -type "float3" 0 0.2292258 0 ;
	setAttr ".pt[300]" -type "float3" 0 0.22252581 0 ;
	setAttr ".pt[301]" -type "float3" 0 0.22238091 0 ;
	setAttr ".pt[302]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[303]" -type "float3" 0 0.45272335 0 ;
	setAttr ".pt[304]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[305]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[324]" -type "float3" 0 0.26249304 0 ;
	setAttr ".pt[325]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[326]" -type "float3" 0 0.26248765 0 ;
	setAttr ".pt[330]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[331]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[332]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[333]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[334]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[335]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[336]" -type "float3" 0 0.3434239 0 ;
	setAttr ".pt[337]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[338]" -type "float3" 0 0.34343013 0 ;
	setAttr ".pt[339]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[340]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[341]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[342]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[343]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[344]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[348]" -type "float3" 0 0.26248765 0 ;
	setAttr ".pt[349]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[350]" -type "float3" 0 0.26251075 0 ;
	setAttr ".pt[354]" -type "float3" 0 0.26249549 0 ;
	setAttr ".pt[355]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[356]" -type "float3" 0 0.26248622 0 ;
	setAttr ".pt[360]" -type "float3" 0 0.34343013 0 ;
	setAttr ".pt[361]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[362]" -type "float3" 0 0.3434239 0 ;
	setAttr ".pt[363]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[364]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[365]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[366]" -type "float3" 0 0.26248765 0 ;
	setAttr ".pt[367]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[368]" -type "float3" 0 0.26249304 0 ;
	setAttr ".pt[372]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[373]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[374]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[375]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[376]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[377]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[378]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[379]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[380]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[381]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[382]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[383]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[384]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[385]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[386]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[387]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[388]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[389]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[390]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[391]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[392]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[396]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[397]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[398]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[399]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[400]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[401]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[402]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[403]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[404]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[405]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[406]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[407]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[408]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[409]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[410]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[411]" -type "float3" 0 0.34346816 0 ;
	setAttr ".pt[412]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[413]" -type "float3" 0 0.34055859 0 ;
	setAttr ".pt[414]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[415]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[416]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[417]" -type "float3" 0 0.34342161 0 ;
	setAttr ".pt[418]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[419]" -type "float3" 0 0.34343344 0 ;
	setAttr ".pt[465]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[466]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[467]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[495]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[496]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[497]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[498]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[499]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[500]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[501]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[502]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[503]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[504]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[505]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[506]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[534]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[535]" -type "float3" 0 0.26775891 0 ;
	setAttr ".pt[536]" -type "float3" 0 0.26248693 0 ;
	setAttr ".pt[537]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[538]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[539]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[540]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[541]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[542]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[543]" -type "float3" 0 0.26536992 0 ;
	setAttr ".pt[544]" -type "float3" 0 0.27067572 0 ;
	setAttr ".pt[545]" -type "float3" 0 0.26543766 0 ;
	setAttr ".pt[573]" -type "float3" 0 0.26539835 0 ;
	setAttr ".pt[574]" -type "float3" 0 0.27067572 0 ;
	setAttr ".pt[575]" -type "float3" 0 0.26540917 0 ;
	setAttr ".pt[576]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[577]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[578]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[579]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[580]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[581]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[582]" -type "float3" 0 0.27244717 0 ;
	setAttr ".pt[583]" -type "float3" 0 0.27769521 0 ;
	setAttr ".pt[584]" -type "float3" 0 0.27240136 0 ;
	setAttr ".pt[612]" -type "float3" 0 0.26540917 0 ;
	setAttr ".pt[613]" -type "float3" 0 0.27067572 0 ;
	setAttr ".pt[614]" -type "float3" 0 0.26539835 0 ;
	setAttr ".pt[615]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[616]" -type "float3" 0 0.35738042 0 ;
	setAttr ".pt[617]" -type "float3" 0 0.3434245 0 ;
	setAttr ".pt[618]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[619]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".pt[620]" -type "float3" 0 -0.72656494 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "board_R_grp" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "F1F5B7C7-424C-3DE1-632F-CF952CA3E667";
	setAttr ".s" -type "double3" -1 1 1 ;
createNode transform -n "back_leg_R" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_R_grp";
	rename -uid "AD5AB021-47E1-38E7-5681-DAB91FBB451C";
	setAttr ".t" -type "double3" -31.844260444737046 9.6656860980046595 -13.782653904732056 ;
	setAttr ".s" -type "double3" 2.0520273429232301 36.403444470495486 3.7647923684188305 ;
	setAttr ".rp" -type "double3" 0 -10.771063804626463 0 ;
	setAttr ".sp" -type "double3" 0 -0.50000001991083431 0 ;
	setAttr ".spt" -type "double3" 0 -10.271063784715629 0 ;
createNode mesh -n "back_leg_RShape" -p "back_leg_R";
	rename -uid "9B9D1930-4C58-C52B-6DA7-9EB3EA9DF74E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.38972215354442596 0.83326727151870728 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape4" -p "back_leg_R";
	rename -uid "D76E289C-4591-5777-BBFF-EF843F53990D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape8" -p "back_leg_R";
	rename -uid "1D8D1CE1-4ABD-E263-F96F-3D864A3494DB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 76 ".uvst[0].uvsp[0:75]" -type "float2" 0.39999747 0.98637497
		 0.39999747 0.0013884306 0.60000253 0.98637497 0.63862503 0.0013884306 0.39999747
		 0.24861157 0.60000253 0.24861157 0.63862503 0.24861157 0.13862509 0.0013884306 0.39999747
		 0.48637491 0.60000253 0.48637491 0.86137486 0.24861157 0.86137491 0.0013884306 0.60000253
		 0.76362509 0.39999747 0.74861157 0.60000253 0.74861157 0.60000253 0.0013884306 0.39999747
		 0.26362503 0.60000253 0.26362503 0.39999747 0.50138843 0.60000253 0.50138843 0.39999747
		 0.76362509 0.36137497 0.0013884306 0.36137497 0.24861157 0.13862509 0.24861157 0.375
		 0.98637491 0.36137491 0 0.39999747 0 0.39999747 1 0.37500006 0.0013884212 0.63862509
		 0 0.625 0.98637491 0.62499994 0.0013884212 0.60000253 1 0.60000253 0 0.36137491 0.25
		 0.375 0.26362509 0.37500006 0.24861158 0.39999747 0.24999829 0.625 0.26362509 0.63862509
		 0.25 0.60000253 0.24999829 0.62499994 0.24861158 0.125 0.24861157 0.375 0.50138843
		 0.375 0.48637486 0.13862514 0.25 0.39999747 0.50000173 0.625 0.50138843 0.875 0.24861157
		 0.60000253 0.50000173 0.86137486 0.25 0.625 0.48637486 0.13862514 0 0.375 0.76362514
		 0.375 0.74861157 0.125 0.001388438 0.39999747 0.74999827 0.625 0.76362514 0.86137486
		 0 0.60000253 0.74999827 0.875 0.001388438 0.625 0.74861157 0.375 1 0.375 0 0.625
		 0 0.625 1 0.375 0.25 0.625 0.25 0.125 0.25 0.375 0.5 0.625 0.5 0.875 0.25 0.125 0
		 0.375 0.75 0.625 0.75 0.875 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 56 ".vt[0:55]"  -0.5 -0.5 0.4454999 -0.40001011 -0.5 0.4454999
		 -0.40001011 -0.5 0.50000024 -0.40001011 -0.49444628 0.50000024 -0.5 -0.49444628 0.50000024
		 -0.5 -0.49444628 0.4454999 0.5 -0.5 0.4454999 0.5 -0.49444628 0.4454999 0.5 -0.49444628 0.50000024
		 0.40001011 -0.49444628 0.50000024 0.40001011 -0.5 0.50000024 0.40001011 -0.5 0.4454999
		 -0.5 0.5 0.4454999 -0.5 0.49444631 0.4454999 -0.5 0.49444631 0.50000024 -0.40001011 0.49444631 0.50000024
		 -0.40001011 0.5 0.50000024 -0.40001011 0.5 0.4454999 0.5 0.5 0.4454999 0.40001011 0.5 0.4454999
		 0.40001011 0.5 0.50000024 0.40001011 0.49444631 0.50000024 0.5 0.49444631 0.50000024
		 0.5 0.49444631 0.4454999 -0.5 0.49444631 -0.50000024 -0.5 0.49444631 -0.44549966
		 -0.5 0.5 -0.44549966 -0.40001011 0.5 -0.44549966 -0.40001011 0.5 -0.50000024 -0.40001011 0.49444631 -0.50000024
		 0.5 0.49444631 -0.50000024 0.40001011 0.49444631 -0.50000024 0.40001011 0.5 -0.50000024
		 0.40001011 0.5 -0.44549966 0.5 0.5 -0.44549966 0.5 0.49444631 -0.44549966 -0.5 -0.5 -0.44549966
		 -0.5 -0.49444628 -0.44549966 -0.5 -0.49444628 -0.50000024 -0.40001011 -0.49444628 -0.50000024
		 -0.40001011 -0.5 -0.50000024 -0.40001011 -0.5 -0.44549966 0.5 -0.5 -0.44549966 0.40001011 -0.5 -0.44549966
		 0.40001011 -0.5 -0.50000024 0.40001011 -0.49444628 -0.50000024 0.5 -0.49444628 -0.50000024
		 0.5 -0.49444628 -0.44549966 -0.5 -0.5 0.50000024 0.5 -0.5 0.50000024 -0.5 0.5 0.50000024
		 0.5 0.5 0.50000024 -0.5 0.5 -0.50000024 0.5 0.5 -0.50000024 -0.5 -0.5 -0.50000024
		 0.5 -0.5 -0.50000024;
	setAttr -s 108 ".ed[0:107]"  1 0 1 0 36 0 36 41 1 41 1 1 0 5 1 5 37 1
		 37 36 1 3 2 1 2 10 0 10 9 1 9 3 1 2 1 1 1 11 1 11 10 1 5 4 1 4 14 0 14 13 1 13 5 1
		 4 3 1 3 15 1 15 14 1 7 6 1 6 42 0 42 47 1 47 7 1 6 11 1 11 43 1 43 42 1 9 8 1 8 22 0
		 22 21 1 21 9 1 8 7 1 7 23 1 23 22 1 13 12 1 12 26 0 26 25 1 25 13 1 12 17 1 17 27 1
		 27 26 1 17 16 1 16 20 0 20 19 1 19 17 1 16 15 1 15 21 1 21 20 1 19 18 1 18 34 0 34 33 1
		 33 19 1 18 23 1 23 35 1 35 34 1 25 24 1 24 38 0 38 37 1 37 25 1 24 29 1 29 39 1 39 38 1
		 29 28 1 28 32 0 32 31 1 31 29 1 28 27 1 27 33 1 33 32 1 31 30 1 30 46 0 46 45 1 45 31 1
		 30 35 1 35 47 1 47 46 1 41 40 1 40 44 0 44 43 1 43 41 1 40 39 1 39 45 1 45 44 1 0 48 0
		 48 4 0 2 48 0 6 49 0 49 10 0 8 49 0 12 50 0 50 16 0 14 50 0 18 51 0 51 22 0 20 51 0
		 24 52 0 52 28 0 26 52 0 30 53 0 53 34 0 32 53 0 36 54 0 54 40 0 38 54 0 42 55 0 55 46 0
		 44 55 0;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 24 53 20
		f 4 4 5 6 -2
		mu 0 4 25 21 7 52
		f 4 7 8 9 10
		mu 0 4 1 26 33 15
		f 4 11 12 13 -9
		mu 0 4 27 0 2 32
		f 4 14 15 16 17
		mu 0 4 21 28 36 22
		f 4 18 19 20 -16
		mu 0 4 28 1 4 36
		f 4 21 22 23 24
		mu 0 4 3 29 58 11
		f 4 25 26 27 -23
		mu 0 4 30 2 12 57
		f 4 28 29 30 31
		mu 0 4 15 31 41 5
		f 4 32 33 34 -30
		mu 0 4 31 3 6 41
		f 4 35 36 37 38
		mu 0 4 22 34 45 23
		f 4 39 40 41 -37
		mu 0 4 35 16 8 44
		f 4 42 43 44 45
		mu 0 4 16 37 40 17
		f 4 46 47 48 -44
		mu 0 4 37 4 5 40
		f 4 49 50 51 52
		mu 0 4 17 38 51 9
		f 4 53 54 55 -51
		mu 0 4 39 6 10 50
		f 4 56 57 58 59
		mu 0 4 23 42 55 7
		f 4 60 61 62 -58
		mu 0 4 43 18 13 54
		f 4 63 64 65 66
		mu 0 4 18 46 49 19
		f 4 67 68 69 -65
		mu 0 4 46 8 9 49
		f 4 70 71 72 73
		mu 0 4 19 47 61 14
		f 4 74 75 76 -72
		mu 0 4 48 10 11 60
		f 4 77 78 79 80
		mu 0 4 20 56 59 12
		f 4 81 82 83 -79
		mu 0 4 56 13 14 59
		f 4 -11 -32 -48 -20
		mu 0 4 1 15 5 4
		f 4 -46 -53 -69 -41
		mu 0 4 16 17 9 8
		f 4 -67 -74 -83 -62
		mu 0 4 18 19 14 13
		f 4 -81 -27 -13 -4
		mu 0 4 20 12 2 0
		f 4 -25 -76 -55 -34
		mu 0 4 3 11 10 6
		f 4 -6 -18 -39 -60
		mu 0 4 7 21 22 23
		f 4 -15 -5 84 85
		mu 0 4 28 21 25 63
		f 4 -1 -12 86 -85
		mu 0 4 24 0 27 62
		f 4 -8 -19 -86 -87
		mu 0 4 26 1 28 63
		f 4 -14 -26 87 88
		mu 0 4 32 2 30 65
		f 4 -22 -33 89 -88
		mu 0 4 29 3 31 64
		f 4 -29 -10 -89 -90
		mu 0 4 31 15 33 64
		f 4 -43 -40 90 91
		mu 0 4 37 16 35 66
		f 4 -36 -17 92 -91
		mu 0 4 34 22 36 66
		f 4 -21 -47 -92 -93
		mu 0 4 36 4 37 66
		f 4 -35 -54 93 94
		mu 0 4 41 6 39 67
		f 4 -50 -45 95 -94
		mu 0 4 38 17 40 67
		f 4 -49 -31 -95 -96
		mu 0 4 40 5 41 67
		f 4 -64 -61 96 97
		mu 0 4 46 18 43 69
		f 4 -57 -38 98 -97
		mu 0 4 42 23 45 68
		f 4 -42 -68 -98 -99
		mu 0 4 44 8 46 69
		f 4 -56 -75 99 100
		mu 0 4 50 10 48 71
		f 4 -71 -66 101 -100
		mu 0 4 47 19 49 70
		f 4 -70 -52 -101 -102
		mu 0 4 49 9 51 70
		f 4 -78 -3 102 103
		mu 0 4 56 20 53 73
		f 4 -7 -59 104 -103
		mu 0 4 52 7 55 72
		f 4 -63 -82 -104 -105
		mu 0 4 54 13 56 73
		f 4 -77 -24 105 106
		mu 0 4 60 11 58 75
		f 4 -28 -80 107 -106
		mu 0 4 57 12 59 74
		f 4 -84 -73 -107 -108
		mu 0 4 59 14 61 74;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "sideboard_R" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_R_grp";
	rename -uid "986AD057-4DE4-18CD-50BE-4290A0E1B840";
	setAttr ".t" -type "double3" -6.2307564944182392 0 0.87370180667271491 ;
	setAttr ".s" -type "double3" 0.8 1 0.99247700925403159 ;
	setAttr ".rp" -type "double3" -24.923025977672996 5.9365536481038301 -13.044227874402194 ;
	setAttr ".sp" -type "double3" -31.153782472091223 5.9365536481037093 -13.143103319044648 ;
	setAttr ".spt" -type "double3" 6.2307564944182445 0 0.098875444642480548 ;
createNode mesh -n "sideboard_RShape" -p "sideboard_R";
	rename -uid "829D6956-417E-0B5E-5A48-A490DFAA7DC6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape2" -p "sideboard_R";
	rename -uid "CC29B510-46AD-B813-D44B-2FAA837E8E75";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -30.653782 6.4365544 13.353498 
		-31.653782 26.592043 13.353498 -32.238728 5.4365544 13.353498 -33.238728 25.592043 
		13.353498 -32.238728 5.4365544 -12.643104 -33.238728 25.592043 -12.643104 -30.653782 
		6.4365544 -12.643104 -31.653782 26.592043 -12.643104;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape6" -p "sideboard_R";
	rename -uid "2EFE5B50-4D48-0B10-8A77-C58F633A066D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 174 ".uvst[0].uvsp[0:173]" -type "float2" 0.37657258 0.9981966
		 0.6268034 0.024997951 0.37611949 0.49933338 0.87433338 0.22500265 0.37657255 0.25180343
		 0.62342745 0.25180343 0.62342745 0.31260726 0.37657255 0.9375 0.6875 0.02499795 0.3125
		 0.22500259 0.3731966 0.024997955 0.37319657 0.22500263 0.12566662 0.024997953 0.14062501
		 0.024998024 0.12566662 0.22500265 0.37611949 0.75066662 0.62388051 0.75066662 0.62388051
		 0.76552206 0.859375 0.22500265 0.37611949 0.484375 0.21875 0.22500263 0.25000006
		 0.024998128 0.37652802 0.375 0.62343353 0.40675676 0.75 0.22500271 0.78125 0.024997845
		 0.37643006 0.84375 0.62350076 0.87481177 0.25 0.2250026 0.3125 0.02499789 0.37652805
		 0.875 0.62342745 0.93739271 0.6875 0.22500266 0.75 0.024998099 0.37657258 0.3125
		 0.62350076 0.37518823 0.171875 0.22500265 0.1875 0.02499786 0.37629163 0.4375 0.6237042
		 0.45360824 0.8125 0.2250026 0.828125 0.024997935 0.37620014 0.796875 0.6234467 0.81197637
		 0.1875 0.2250026 0.21875 0.024997905 0.37629163 0.8125 0.62343371 0.8432433 0.78125
		 0.2250025 0.8125 0.024998009 0.37643006 0.40625 0.62344611 0.43802378 0.140625 0.22500266
		 0.15625 0.02499792 0.37614852 0.46875 0.62388051 0.48447794 0.84375 0.22500263 0.85937494
		 0.024997955 0.37611961 0.765625 0.62382436 0.78091121 0.15625 0.22500263 0.171875
		 0.02499792 0.3761484 0.78125 0.62370449 0.79639184 0.828125 0.22500265 0.84375 0.024997875
		 0.37620014 0.453125 0.62382585 0.46908873 0.6268034 0.22500265 0.87433338 0.024997955
		 0.62342745 0.9981966 0.62388051 0.49933338 0.375 0.99819654 0.37319657 0 0.37657255
		 1 0.37489823 0.024997953 0.62680346 0 0.625 0.99819654 0.62510175 0.024997957 0.62342745
		 1 0.37319657 0.25 0.375 0.25180343 0.37489823 0.22500265 0.37657255 0.23991923 0.625
		 0.25180343 0.62680346 0.25 0.62342745 0.23991923 0.62510175 0.22500265 0.375 0.49933338
		 0.1256666 0.25 0.37611949 0.51073605 0.125 0.22500265 0.87433338 0.25 0.625 0.49933338
		 0.875 0.22500265 0.62388051 0.51073605 0.1256666 0 0.375 0.75066662 0.125 0.024997953
		 0.37611949 0.7392872 0.625 0.75066662 0.87433338 0 0.62388051 0.7392872 0.875 0.024997953
		 0.375 0.3125 0.3125 0.25 0.6875 0.25 0.625 0.3125 0.375 0.765625 0.140625 0 0.859375
		 0 0.625 0.765625 0.25 0.25 0.375 0.375 0.375 0.40625 0.21875 0.25 0.25 0 0.375 0.875
		 0.375 0.9375 0.3125 0 0.625 0.875 0.75 0 0.6875 0 0.625 0.9375 0.75 0.25 0.625 0.375
		 0.78125 0.25 0.625 0.40625 0.1875 0 0.375 0.8125 0.375 0.84375 0.21875 0 0.1875 0.25
		 0.375 0.4375 0.375 0.453125 0.171875 0.25 0.8125 0.25 0.625 0.4375 0.828125 0.25
		 0.625 0.453125 0.625 0.8125 0.8125 0 0.78125 0 0.625 0.84375 0.15625 0 0.375 0.78125
		 0.375 0.796875 0.171875 0 0.15625 0.25 0.375 0.46875 0.375 0.484375 0.140625 0.25
		 0.625 0.46875 0.84375 0.25 0.859375 0.25 0.625 0.484375 0.625 0.78125 0.84375 0 0.828125
		 0 0.625 0.796875 0.375 1 0.375 0 0.625 0 0.625 1 0.375 0.25 0.625 0.25 0.375 0.5
		 0.125 0.25 0.875 0.25 0.625 0.5 0.125 0 0.375 0.75 0.625 0.75 0.875 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 132 ".vt[0:131]"  -31.15378189 5.93655443 13.72575474 -31.15378189 6.063337326 13.72575474
		 -31.15378189 6.063337326 13.85350037 -31.31226349 5.93655443 13.85350037 -31.31226349 5.93655443 13.72575474
		 -31.15378189 26.092042923 13.72575474 -31.31226349 26.092042923 13.72575474 -31.31226349 26.092042923 13.85350037
		 -31.15378189 25.96525955 13.85350037 -31.15378189 25.96525955 13.72575474 -32.73872757 5.93655443 13.72575474
		 -32.58024979 5.93655443 13.72575474 -32.58024979 5.93655443 13.85350037 -32.73872757 6.063337326 13.85350037
		 -32.73872757 6.063337326 13.72575474 -32.73872757 26.092042923 13.72575474 -32.73872757 25.96525955 13.72575474
		 -32.73872757 25.96525955 13.85350037 -32.58024979 26.092042923 13.85350037 -32.58024979 26.092042923 13.72575474
		 -32.73872757 5.93655443 -13.015358925 -32.73872757 6.063337326 -13.015358925 -32.73872757 6.063337326 -13.14310169
		 -32.58024979 5.93655443 -13.14310169 -32.58024979 5.93655443 -13.015358925 -32.73872757 34.2490387 -13.015358925
		 -32.58024979 34.2490387 -13.015358925 -32.58024979 34.2490387 -13.14310169 -32.73872757 34.12225723 -13.14310169
		 -32.73872757 34.12225723 -13.015358925 -31.15378189 5.93655443 -13.015358925 -31.31226349 5.93655443 -13.015358925
		 -31.31226349 5.93655443 -13.14310169 -31.15378189 6.063337326 -13.14310169 -31.15378189 6.063337326 -13.015358925
		 -31.15378189 34.2490387 -13.015358925 -31.15378189 34.12225723 -13.015358925 -31.15378189 34.12225723 -13.14310169
		 -31.31226349 34.2490387 -13.14310169 -31.31226349 34.2490387 -13.015358925 -32.58024979 5.93655443 9.42637634
		 -32.73872757 5.93655443 9.42637634 -32.73872757 6.063337326 9.42637634 -32.58024979 26.092042923 9.42637634
		 -32.73872757 26.092042923 9.42637634 -32.73872757 25.96525955 9.41833973 -31.31226349 5.93655443 -10.14884377
		 -31.15378189 5.93655443 -10.14884377 -31.15378189 6.063337326 -10.14884377 -31.31226349 34.2490387 -10.14884377
		 -31.15378189 34.2490387 -10.14884377 -31.15378189 34.12225723 -10.16856956 -32.73872757 6.063337326 4.74440336
		 -32.73872757 5.93655443 4.74440336 -32.58024979 5.93655443 4.74440336 -32.58024979 5.93655443 0.19457889
		 -32.73872757 5.93655443 0.19457889 -32.73872757 6.063337326 0.19457889 -31.15378189 6.063337326 4.74440336
		 -31.15378189 5.93655443 4.74440336 -31.31226349 5.93655443 4.74440336 -31.31226349 5.93655443 9.42637634
		 -31.15378189 5.93655443 9.42637634 -31.15378189 6.063337326 9.42637634 -31.31226349 26.679039 4.74440336
		 -31.15378189 26.679039 4.74440336 -31.15378189 26.55468369 4.71699905 -31.15378189 25.96525955 9.41833973
		 -31.15378189 26.092042923 9.42637634 -31.31226349 26.092042923 9.42637634 -32.58024979 26.679039 4.74440336
		 -32.73872757 26.679039 4.74440336 -32.73872757 26.55468369 4.71699905 -32.58024979 28.10052872 0.19457889
		 -32.73872757 28.10052872 0.19457889 -32.73872757 27.98528099 0.13802826 -31.15378189 6.063337326 -3.29256773
		 -31.15378189 5.93655443 -3.29256773 -31.31226349 5.93655443 -3.29256773 -31.31226349 5.93655443 0.19457889
		 -31.15378189 5.93655443 0.19457889 -31.15378189 6.063337326 0.19457889 -32.73872757 6.063337326 -3.29256773
		 -32.73872757 5.93655443 -3.29256773 -32.58024979 5.93655443 -3.29256773 -32.58024979 5.93655443 -5.58533001
		 -32.73872757 5.93655443 -5.58533001 -32.73872757 6.063337326 -5.58533001 -32.58024979 30.47538185 -3.29256773
		 -32.73872757 30.47538185 -3.29256773 -32.73872757 30.3739624 -3.36942387 -32.58024979 32.34690094 -5.58533001
		 -32.73872757 32.34690094 -5.58533001 -32.73872757 32.24062729 -5.65624094 -31.31226349 30.47538185 -3.29256773
		 -31.15378189 30.47538185 -3.29256773 -31.15378189 30.3739624 -3.36942387 -31.15378189 27.98528099 0.13802826
		 -31.15378189 28.10052872 0.19457889 -31.31226349 28.10052872 0.19457889 -31.15378189 6.063337326 -7.87809229
		 -31.15378189 5.93655443 -7.87809229 -31.31226349 5.93655443 -7.87809229 -31.31226349 5.93655443 -5.58533001
		 -31.15378189 5.93655443 -5.58533001 -31.15378189 6.063337326 -5.58533001 -32.73872757 6.063337326 -7.87809229
		 -32.73872757 5.93655443 -7.87809229 -32.58024979 5.93655443 -7.87809229 -32.58024979 5.93655443 -10.14884377
		 -32.73872757 5.93655443 -10.14884377 -32.73872757 6.063337326 -10.14884377 -32.58024979 33.53609467 -7.87809229
		 -32.73872757 33.53609467 -7.87809229 -32.73872757 33.41857529 -7.92731762 -32.73872757 34.12225723 -10.16856956
		 -32.73872757 34.2490387 -10.14884377 -32.58024979 34.2490387 -10.14884377 -31.31226349 33.53609467 -7.87809229
		 -31.15378189 33.53609467 -7.87809229 -31.15378189 33.41857529 -7.92731762 -31.15378189 32.24062729 -5.65624094
		 -31.15378189 32.34690094 -5.58533001 -31.31226349 32.34690094 -5.58533001 -31.15378189 5.93655443 13.85350037
		 -31.15378189 26.092042923 13.85350037 -32.73872757 5.93655443 13.85350037 -32.73872757 26.092042923 13.85350037
		 -32.73872757 5.93655443 -13.14310169 -32.73872757 34.2490387 -13.14310169 -31.15378189 5.93655443 -13.14310169
		 -31.15378189 34.2490387 -13.14310169;
	setAttr -s 252 ".ed";
	setAttr ".ed[0:165]"  1 0 1 63 1 1 0 4 1 4 61 1 2 8 0 2 1 1 1 9 1 9 8 1 4 3 1
		 3 12 0 12 11 1 11 4 1 6 5 1 69 6 1 5 9 1 9 67 1 7 18 0 7 6 1 6 19 1 19 18 1 11 10 1
		 10 41 0 41 40 1 40 11 1 10 14 1 14 42 1 42 41 1 14 13 1 13 17 0 17 16 1 16 14 1 16 15 1
		 45 16 1 15 19 1 19 43 1 21 20 1 111 21 1 20 24 1 24 109 1 22 28 0 22 21 1 21 29 1
		 29 28 1 24 23 1 23 32 0 32 31 1 31 24 1 26 25 1 117 26 1 25 29 1 29 115 1 27 38 0
		 27 26 1 26 39 1 39 38 1 31 30 1 30 47 0 47 46 1 46 31 1 30 34 1 34 48 1 48 47 1 34 33 1
		 33 37 0 37 36 1 36 34 1 36 35 1 51 36 1 35 39 1 39 49 1 54 40 1 42 52 1 45 44 1 72 45 1
		 44 43 1 43 70 1 102 46 1 48 100 1 51 50 1 120 51 1 50 49 1 49 118 1 54 53 1 53 56 0
		 56 55 1 55 54 1 53 52 1 52 57 1 57 56 1 84 55 1 57 82 1 81 58 1 60 79 1 60 59 1 59 62 0
		 62 61 1 61 60 1 59 58 1 58 63 1 63 62 1 99 64 1 66 97 1 66 65 1 65 68 0 68 67 1 67 66 1
		 65 64 1 64 69 1 69 68 1 72 71 1 75 72 1 71 70 1 70 73 1 75 74 1 90 75 1 74 73 1 73 88 1
		 105 76 1 78 103 1 78 77 1 77 80 0 80 79 1 79 78 1 77 76 1 76 81 1 81 80 1 84 83 1
		 83 86 0 86 85 1 85 84 1 83 82 1 82 87 1 87 86 1 108 85 1 87 106 1 90 89 1 93 90 1
		 89 88 1 88 91 1 93 92 1 114 93 1 92 91 1 91 112 1 123 94 1 96 121 1 96 95 1 95 98 0
		 98 97 1 97 96 1 95 94 1 94 99 1 99 98 1 102 101 1 101 104 0 104 103 1 103 102 1 101 100 1
		 100 105 1 105 104 1 108 107 1 107 110 0 110 109 1 109 108 1 107 106 1 106 111 1 111 110 1;
	setAttr ".ed[166:251]" 114 113 1 113 116 0 116 115 1 115 114 1 113 112 1 112 117 1
		 117 116 1 120 119 1 119 122 0 122 121 1 121 120 1 119 118 1 118 123 1 123 122 1 45 42 1
		 63 67 1 69 43 1 40 61 1 46 109 1 51 48 1 117 49 1 111 115 1 55 79 1 60 54 1 52 72 1
		 75 57 1 70 64 1 99 73 1 81 97 1 66 58 1 85 103 1 78 84 1 82 90 1 93 87 1 88 94 1
		 123 91 1 105 121 1 96 76 1 102 108 1 106 114 1 112 118 1 120 100 1 15 44 0 35 50 0
		 41 53 0 0 62 0 5 68 0 44 71 0 71 74 0 59 80 0 56 83 0 74 89 0 89 92 0 65 98 0 47 101 0
		 77 104 0 86 107 0 20 110 0 92 113 0 25 116 0 50 119 0 95 122 0 0 124 0 124 3 0 2 124 0
		 5 125 0 125 8 0 7 125 0 10 126 0 126 13 0 12 126 0 15 127 0 127 18 0 17 127 0 20 128 0
		 128 23 0 22 128 0 25 129 0 129 28 0 27 129 0 30 130 0 130 33 0 32 130 0 35 131 0
		 131 38 0 37 131 0;
	setAttr -s 120 -ch 480 ".fc[0:119]" -type "polyFaces" 
		f 4 5 6 7 -5
		mu 0 4 74 0 70 79
		f 4 8 9 10 11
		mu 0 4 10 75 82 11
		f 4 17 18 19 -17
		mu 0 4 78 1 68 87
		f 4 20 21 22 23
		mu 0 4 11 80 105 9
		f 4 24 25 26 -22
		mu 0 4 81 4 34 104
		f 4 27 28 29 30
		mu 0 4 4 83 86 5
		f 4 40 41 42 -40
		mu 0 4 90 2 71 95
		f 4 43 44 45 46
		mu 0 4 14 91 98 12
		f 4 52 53 54 -52
		mu 0 4 94 3 69 103
		f 4 55 56 57 58
		mu 0 4 12 96 109 13
		f 4 59 60 61 -57
		mu 0 4 97 15 58 108
		f 4 62 63 64 65
		mu 0 4 15 99 102 16
		f 4 82 83 84 85
		mu 0 4 28 112 115 20
		f 4 86 87 88 -84
		mu 0 4 113 22 50 114
		f 4 93 94 95 96
		mu 0 4 21 116 119 29
		f 4 97 98 99 -95
		mu 0 4 117 30 7 118
		f 4 102 103 104 105
		mu 0 4 27 120 123 31
		f 4 106 107 108 -104
		mu 0 4 121 33 8 122
		f 4 119 120 121 122
		mu 0 4 37 128 131 45
		f 4 123 124 125 -121
		mu 0 4 129 46 26 130
		f 4 126 127 128 129
		mu 0 4 44 132 135 36
		f 4 130 131 132 -128
		mu 0 4 133 38 66 134
		f 4 145 146 147 148
		mu 0 4 43 140 143 47
		f 4 149 150 151 -147
		mu 0 4 141 49 25 142
		f 4 152 153 154 155
		mu 0 4 53 144 147 61
		f 4 156 157 158 -154
		mu 0 4 145 62 42 146
		f 4 159 160 161 162
		mu 0 4 60 148 151 52
		f 4 163 164 165 -161
		mu 0 4 149 54 19 150
		f 4 166 167 168 169
		mu 0 4 67 152 155 55
		f 4 170 171 172 -168
		mu 0 4 153 56 18 154
		f 4 173 174 175 176
		mu 0 4 59 156 159 63
		f 4 177 178 179 -175
		mu 0 4 157 65 41 158
		f 4 -31 -33 180 -26
		mu 0 4 4 5 6 34
		f 4 181 -16 -7 -2
		mu 0 4 7 31 70 0
		f 4 -14 182 -35 -19
		mu 0 4 1 8 32 68
		f 4 183 -4 -12 -24
		mu 0 4 9 29 10 11
		f 4 -59 184 -39 -47
		mu 0 4 12 13 52 14
		f 4 -66 -68 185 -61
		mu 0 4 15 16 17 58
		f 4 186 -70 -54 -49
		mu 0 4 18 57 69 3
		f 4 187 -51 -42 -37
		mu 0 4 19 55 71 2
		f 4 188 -93 189 -86
		mu 0 4 20 45 21 28
		f 4 190 -111 191 -88
		mu 0 4 22 35 23 50
		f 4 192 -101 193 -113
		mu 0 4 24 33 25 48
		f 4 194 -102 195 -92
		mu 0 4 26 47 27 30
		f 4 -190 -97 -184 -71
		mu 0 4 28 21 29 9
		f 4 -196 -106 -182 -99
		mu 0 4 30 27 31 7
		f 4 -183 -108 -193 -76
		mu 0 4 32 8 33 24
		f 4 -181 -74 -191 -72
		mu 0 4 34 6 35 22
		f 4 196 -119 197 -130
		mu 0 4 36 61 37 44
		f 4 198 -137 199 -132
		mu 0 4 38 51 39 66
		f 4 200 -144 201 -139
		mu 0 4 40 49 41 64
		f 4 202 -145 203 -118
		mu 0 4 42 63 43 46
		f 4 -198 -123 -189 -90
		mu 0 4 44 37 45 20
		f 4 -204 -149 -195 -125
		mu 0 4 46 43 47 26
		f 4 -194 -151 -201 -117
		mu 0 4 48 25 49 40
		f 4 -192 -115 -199 -91
		mu 0 4 50 23 51 38
		f 4 -185 -77 204 -163
		mu 0 4 52 13 53 60
		f 4 205 -170 -188 -165
		mu 0 4 54 67 55 19
		f 4 206 -82 -187 -172
		mu 0 4 56 65 57 18
		f 4 -186 -80 207 -78
		mu 0 4 58 17 59 62
		f 4 -205 -156 -197 -134
		mu 0 4 60 53 61 36
		f 4 -208 -177 -203 -158
		mu 0 4 62 59 63 42
		f 4 -202 -179 -207 -143
		mu 0 4 64 41 65 56
		f 4 -200 -141 -206 -135
		mu 0 4 66 39 67 54
		f 4 31 208 -73 32
		mu 0 4 5 84 107 6
		f 4 33 34 -75 -209
		mu 0 4 85 68 32 106
		f 4 66 209 -79 67
		mu 0 4 16 100 111 17
		f 4 68 69 -81 -210
		mu 0 4 101 69 57 110
		f 4 -23 210 -83 70
		mu 0 4 9 105 112 28
		f 4 -27 71 -87 -211
		mu 0 4 104 34 22 113
		f 4 0 211 -100 1
		mu 0 4 0 72 118 7
		f 4 2 3 -96 -212
		mu 0 4 73 10 29 119
		f 4 12 212 -109 13
		mu 0 4 1 76 122 8
		f 4 14 15 -105 -213
		mu 0 4 77 70 31 123
		f 4 72 213 -110 73
		mu 0 4 6 107 125 35
		f 4 74 75 -112 -214
		mu 0 4 106 32 24 124
		f 4 109 214 -114 110
		mu 0 4 35 125 127 23
		f 4 111 112 -116 -215
		mu 0 4 124 24 48 126
		f 4 -98 215 -126 91
		mu 0 4 30 117 130 26
		f 4 -94 92 -122 -216
		mu 0 4 116 21 45 131
		f 4 -85 216 -127 89
		mu 0 4 20 115 132 44
		f 4 -89 90 -131 -217
		mu 0 4 114 50 38 133
		f 4 113 217 -136 114
		mu 0 4 23 127 137 51
		f 4 115 116 -138 -218
		mu 0 4 126 48 40 136
		f 4 135 218 -140 136
		mu 0 4 51 137 139 39
		f 4 137 138 -142 -219
		mu 0 4 136 40 64 138
		f 4 -107 219 -152 100
		mu 0 4 33 121 142 25
		f 4 -103 101 -148 -220
		mu 0 4 120 27 47 143
		f 4 -58 220 -153 76
		mu 0 4 13 109 144 53
		f 4 -62 77 -157 -221
		mu 0 4 108 58 62 145
		f 4 -124 221 -159 117
		mu 0 4 46 129 146 42
		f 4 -120 118 -155 -222
		mu 0 4 128 37 61 147
		f 4 -129 222 -160 133
		mu 0 4 36 135 148 60
		f 4 -133 134 -164 -223
		mu 0 4 134 66 54 149
		f 4 35 223 -166 36
		mu 0 4 2 88 150 19
		f 4 37 38 -162 -224
		mu 0 4 89 14 52 151
		f 4 139 224 -167 140
		mu 0 4 39 139 152 67
		f 4 141 142 -171 -225
		mu 0 4 138 64 56 153
		f 4 47 225 -173 48
		mu 0 4 3 92 154 18
		f 4 49 50 -169 -226
		mu 0 4 93 71 55 155
		f 4 78 226 -174 79
		mu 0 4 17 111 156 59
		f 4 80 81 -178 -227
		mu 0 4 110 57 65 157
		f 4 -150 227 -180 143
		mu 0 4 49 141 158 41
		f 4 -146 144 -176 -228
		mu 0 4 140 43 63 159
		f 4 -9 -3 228 229
		mu 0 4 75 10 73 161
		f 4 -1 -6 230 -229
		mu 0 4 72 0 74 160
		f 4 -8 -15 231 232
		mu 0 4 79 70 77 163
		f 4 -13 -18 233 -232
		mu 0 4 76 1 78 162
		f 4 -28 -25 234 235
		mu 0 4 83 4 81 164
		f 4 -21 -11 236 -235
		mu 0 4 80 11 82 164
		f 4 -20 -34 237 238
		mu 0 4 87 68 85 165
		f 4 -32 -30 239 -238
		mu 0 4 84 5 86 165
		f 4 -44 -38 240 241
		mu 0 4 91 14 89 167
		f 4 -36 -41 242 -241
		mu 0 4 88 2 90 166
		f 4 -43 -50 243 244
		mu 0 4 95 71 93 169
		f 4 -48 -53 245 -244
		mu 0 4 92 3 94 168
		f 4 -63 -60 246 247
		mu 0 4 99 15 97 171
		f 4 -56 -46 248 -247
		mu 0 4 96 12 98 170
		f 4 -55 -69 249 250
		mu 0 4 103 69 101 173
		f 4 -67 -65 251 -250
		mu 0 4 100 16 102 172;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "front_leg_R" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_R_grp";
	rename -uid "1D4D90AD-4C01-4C55-19CB-209C89CE45FA";
	setAttr ".t" -type "double3" -31.844260444737046 9.6656860980046595 14.526437023076681 ;
	setAttr ".s" -type "double3" 2.0520273429232301 28.581919168118063 3.7647923684188305 ;
	setAttr ".rp" -type "double3" 0 -10.771063804626463 0 ;
	setAttr ".sp" -type "double3" 0 -0.50000001991083431 0 ;
	setAttr ".spt" -type "double3" 0 -10.271063784715629 0 ;
createNode mesh -n "front_leg_RShape" -p "front_leg_R";
	rename -uid "F8C54DA2-4483-2648-9AF1-FD9A05261AE8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.38972263038158417 0.8332669734954834 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape7" -p "front_leg_R";
	rename -uid "8EFD78DD-4B8D-96AA-C786-1296E6B4AC0E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 76 ".uvst[0].uvsp[0:75]" -type "float2" 0.39999747 0.98637486
		 0.39999747 0.0017814934 0.60000253 0.98637486 0.63862514 0.0017814934 0.39999747
		 0.24821851 0.60000253 0.24821851 0.63862514 0.24821851 0.13862503 0.0017814934 0.39999747
		 0.48637497 0.60000253 0.48637497 0.86137491 0.24821851 0.86137497 0.0017814934 0.60000253
		 0.76362503 0.39999747 0.74821854 0.60000253 0.74821848 0.60000253 0.0017814934 0.39999747
		 0.26362514 0.60000253 0.26362514 0.39999747 0.50178146 0.60000253 0.50178146 0.39999747
		 0.76362503 0.36137486 0.0017814934 0.36137483 0.24821851 0.13862503 0.24821851 0.375
		 0.98637497 0.36137497 0 0.39999747 0 0.39999747 1 0.375 0.0017814827 0.63862503 0
		 0.625 0.98637497 0.625 0.0017814827 0.60000253 1 0.60000253 0 0.36137497 0.25 0.375
		 0.26362503 0.37499997 0.24821852 0.39999747 0.25000054 0.625 0.26362503 0.63862503
		 0.25 0.60000253 0.25000054 0.625 0.24821852 0.125 0.24821852 0.375 0.50178146 0.375
		 0.48637497 0.13862503 0.25 0.39999747 0.49999943 0.625 0.50178146 0.875 0.24821852
		 0.60000253 0.49999943 0.86137497 0.25 0.625 0.48637497 0.13862503 0 0.375 0.76362503
		 0.375 0.74821854 0.125 0.001781486 0.39999747 0.75000054 0.625 0.76362503 0.86137497
		 0 0.60000253 0.75000048 0.875 0.001781486 0.625 0.74821854 0.375 1 0.375 0 0.625
		 0 0.625 1 0.375 0.25 0.625 0.25 0.125 0.25 0.375 0.5 0.625 0.5 0.875 0.25 0.125 0
		 0.375 0.75 0.625 0.75 0.875 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 56 ".vt[0:55]"  -0.5 -0.5 0.44549942 -0.40001011 -0.5 0.44549942
		 -0.40001011 -0.5 0.49999905 -0.40001011 -0.49287406 0.49999905 -0.5 -0.49287406 0.49999905
		 -0.5 -0.49287406 0.44549942 0.5 -0.5 0.44549942 0.5 -0.49287406 0.44549942 0.5 -0.49287406 0.49999905
		 0.40001011 -0.49287406 0.49999905 0.40001011 -0.5 0.49999905 0.40001011 -0.5 0.44549942
		 -0.5 0.5 0.44549942 -0.5 0.49287406 0.44549942 -0.5 0.49287406 0.49999905 -0.40001011 0.49287406 0.49999905
		 -0.40001011 0.5 0.49999905 -0.40001011 0.5 0.44549942 0.5 0.5 0.44549942 0.40001011 0.5 0.44549942
		 0.40001011 0.5 0.49999905 0.40001011 0.49287406 0.49999905 0.5 0.49287406 0.49999905
		 0.5 0.49287406 0.44549942 -0.5 0.49287406 -0.5 -0.5 0.49287406 -0.44550014 -0.5 0.5 -0.44550014
		 -0.40001011 0.5 -0.44550014 -0.40001011 0.5 -0.5 -0.40001011 0.49287406 -0.5 0.5 0.49287406 -0.5
		 0.40001011 0.49287406 -0.5 0.40001011 0.5 -0.5 0.40001011 0.5 -0.44550014 0.5 0.5 -0.44550014
		 0.5 0.49287406 -0.44550014 -0.5 -0.5 -0.44550014 -0.5 -0.49287406 -0.44550014 -0.5 -0.49287406 -0.5
		 -0.40001011 -0.49287406 -0.5 -0.40001011 -0.5 -0.5 -0.40001011 -0.5 -0.44550014 0.5 -0.5 -0.44550014
		 0.40001011 -0.5 -0.44550014 0.40001011 -0.5 -0.5 0.40001011 -0.49287406 -0.5 0.5 -0.49287406 -0.5
		 0.5 -0.49287406 -0.44550014 -0.5 -0.5 0.49999905 0.5 -0.5 0.49999905 -0.5 0.5 0.49999905
		 0.5 0.5 0.49999905 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 108 ".ed[0:107]"  1 0 1 0 36 0 36 41 1 41 1 1 0 5 1 5 37 1
		 37 36 1 3 2 1 2 10 0 10 9 1 9 3 1 2 1 1 1 11 1 11 10 1 5 4 1 4 14 0 14 13 1 13 5 1
		 4 3 1 3 15 1 15 14 1 7 6 1 6 42 0 42 47 1 47 7 1 6 11 1 11 43 1 43 42 1 9 8 1 8 22 0
		 22 21 1 21 9 1 8 7 1 7 23 1 23 22 1 13 12 1 12 26 0 26 25 1 25 13 1 12 17 1 17 27 1
		 27 26 1 17 16 1 16 20 0 20 19 1 19 17 1 16 15 1 15 21 1 21 20 1 19 18 1 18 34 0 34 33 1
		 33 19 1 18 23 1 23 35 1 35 34 1 25 24 1 24 38 0 38 37 1 37 25 1 24 29 1 29 39 1 39 38 1
		 29 28 1 28 32 0 32 31 1 31 29 1 28 27 1 27 33 1 33 32 1 31 30 1 30 46 0 46 45 1 45 31 1
		 30 35 1 35 47 1 47 46 1 41 40 1 40 44 0 44 43 1 43 41 1 40 39 1 39 45 1 45 44 1 0 48 0
		 48 4 0 2 48 0 6 49 0 49 10 0 8 49 0 12 50 0 50 16 0 14 50 0 18 51 0 51 22 0 20 51 0
		 24 52 0 52 28 0 26 52 0 30 53 0 53 34 0 32 53 0 36 54 0 54 40 0 38 54 0 42 55 0 55 46 0
		 44 55 0;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 24 53 20
		f 4 4 5 6 -2
		mu 0 4 25 21 7 52
		f 4 7 8 9 10
		mu 0 4 1 26 33 15
		f 4 11 12 13 -9
		mu 0 4 27 0 2 32
		f 4 14 15 16 17
		mu 0 4 21 28 36 22
		f 4 18 19 20 -16
		mu 0 4 28 1 4 36
		f 4 21 22 23 24
		mu 0 4 3 29 58 11
		f 4 25 26 27 -23
		mu 0 4 30 2 12 57
		f 4 28 29 30 31
		mu 0 4 15 31 41 5
		f 4 32 33 34 -30
		mu 0 4 31 3 6 41
		f 4 35 36 37 38
		mu 0 4 22 34 45 23
		f 4 39 40 41 -37
		mu 0 4 35 16 8 44
		f 4 42 43 44 45
		mu 0 4 16 37 40 17
		f 4 46 47 48 -44
		mu 0 4 37 4 5 40
		f 4 49 50 51 52
		mu 0 4 17 38 51 9
		f 4 53 54 55 -51
		mu 0 4 39 6 10 50
		f 4 56 57 58 59
		mu 0 4 23 42 55 7
		f 4 60 61 62 -58
		mu 0 4 43 18 13 54
		f 4 63 64 65 66
		mu 0 4 18 46 49 19
		f 4 67 68 69 -65
		mu 0 4 46 8 9 49
		f 4 70 71 72 73
		mu 0 4 19 47 61 14
		f 4 74 75 76 -72
		mu 0 4 48 10 11 60
		f 4 77 78 79 80
		mu 0 4 20 56 59 12
		f 4 81 82 83 -79
		mu 0 4 56 13 14 59
		f 4 -11 -32 -48 -20
		mu 0 4 1 15 5 4
		f 4 -46 -53 -69 -41
		mu 0 4 16 17 9 8
		f 4 -67 -74 -83 -62
		mu 0 4 18 19 14 13
		f 4 -81 -27 -13 -4
		mu 0 4 20 12 2 0
		f 4 -25 -76 -55 -34
		mu 0 4 3 11 10 6
		f 4 -6 -18 -39 -60
		mu 0 4 7 21 22 23
		f 4 -15 -5 84 85
		mu 0 4 28 21 25 63
		f 4 -1 -12 86 -85
		mu 0 4 24 0 27 62
		f 4 -8 -19 -86 -87
		mu 0 4 26 1 28 63
		f 4 -14 -26 87 88
		mu 0 4 32 2 30 65
		f 4 -22 -33 89 -88
		mu 0 4 29 3 31 64
		f 4 -29 -10 -89 -90
		mu 0 4 31 15 33 64
		f 4 -43 -40 90 91
		mu 0 4 37 16 35 66
		f 4 -36 -17 92 -91
		mu 0 4 34 22 36 66
		f 4 -21 -47 -92 -93
		mu 0 4 36 4 37 66
		f 4 -35 -54 93 94
		mu 0 4 41 6 39 67
		f 4 -50 -45 95 -94
		mu 0 4 38 17 40 67
		f 4 -49 -31 -95 -96
		mu 0 4 40 5 41 67
		f 4 -64 -61 96 97
		mu 0 4 46 18 43 69
		f 4 -57 -38 98 -97
		mu 0 4 42 23 45 68
		f 4 -42 -68 -98 -99
		mu 0 4 44 8 46 69
		f 4 -56 -75 99 100
		mu 0 4 50 10 48 71
		f 4 -71 -66 101 -100
		mu 0 4 47 19 49 70
		f 4 -70 -52 -101 -102
		mu 0 4 49 9 51 70
		f 4 -78 -3 102 103
		mu 0 4 56 20 53 73
		f 4 -7 -59 104 -103
		mu 0 4 52 7 55 72
		f 4 -63 -82 -104 -105
		mu 0 4 54 13 56 73
		f 4 -77 -24 105 106
		mu 0 4 60 11 58 75
		f 4 -28 -80 107 -106
		mu 0 4 57 12 59 74
		f 4 -84 -73 -107 -108
		mu 0 4 59 14 61 74;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "board_L_grp" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "6E1CE9CF-4A62-279B-F4F2-A1B3E813B673";
createNode transform -n "back_leg_L" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_L_grp";
	rename -uid "E176C83A-42DD-B656-1347-84ABDF48F751";
	setAttr ".t" -type "double3" -31.844260444737046 9.6656860980046595 -13.782653904732056 ;
	setAttr ".s" -type "double3" 2.0520273429232301 36.403444470495486 3.7647923684188305 ;
	setAttr ".rp" -type "double3" 0 -10.771063804626463 0 ;
	setAttr ".sp" -type "double3" 0 -0.50000001991083431 0 ;
	setAttr ".spt" -type "double3" 0 -10.271063784715629 0 ;
createNode mesh -n "back_leg_LShape" -p "back_leg_L";
	rename -uid "BC3FAE86-451E-7A53-6E9A-778C6FDADE41";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.38972215354442596 0.83326727151870728 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape4" -p "back_leg_L";
	rename -uid "C09CB3FE-4E2C-641D-F984-89B8C2294428";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "sideboard_L" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_L_grp";
	rename -uid "88FB98CC-4752-8992-E5CB-14A6E66D2836";
	setAttr ".t" -type "double3" -6.2307564944182392 0 0.87370180667271491 ;
	setAttr ".s" -type "double3" 0.8 1 0.99247700925403159 ;
	setAttr ".rp" -type "double3" -24.923025977672996 5.9365536481038301 -13.044227874402194 ;
	setAttr ".sp" -type "double3" -31.153782472091223 5.9365536481037093 -13.143103319044648 ;
	setAttr ".spt" -type "double3" 6.2307564944182445 0 0.098875444642480548 ;
createNode mesh -n "sideboard_LShape" -p "sideboard_L";
	rename -uid "2F24842B-4222-C0B3-1690-39BEB2F85FD6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape2" -p "sideboard_L";
	rename -uid "A0401386-4660-0B70-7354-9FB454A997BC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -30.653782 6.4365544 13.353498 
		-31.653782 26.592043 13.353498 -32.238728 5.4365544 13.353498 -33.238728 25.592043 
		13.353498 -32.238728 5.4365544 -12.643104 -33.238728 25.592043 -12.643104 -30.653782 
		6.4365544 -12.643104 -31.653782 26.592043 -12.643104;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "front_leg_L" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_L_grp";
	rename -uid "E9AEFEF1-4B2E-E93E-4DA7-BBAFA09DBCD7";
	setAttr ".t" -type "double3" -31.844260444737046 9.6656860980046595 14.526437023076681 ;
	setAttr ".s" -type "double3" 2.0520273429232301 28.581919168118063 3.7647923684188305 ;
	setAttr ".rp" -type "double3" 0 -10.771063804626463 0 ;
	setAttr ".sp" -type "double3" 0 -0.50000001991083431 0 ;
	setAttr ".spt" -type "double3" 0 -10.271063784715629 0 ;
createNode mesh -n "front_leg_LShape" -p "front_leg_L";
	rename -uid "6A709526-4891-5941-8706-6DAA373CFD7D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.60666513442993164 0.03589729976374656 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "pillow_grp" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "F4239A7D-41C9-D344-A906-47AAF1D5B6E8";
createNode transform -n "round_pillow_small" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|pillow_grp";
	rename -uid "35CAB2CD-4E9C-04D8-66BC-3AB719A88493";
	setAttr ".t" -type "double3" 0.5679120293376636 20.157923568642591 -2.6389771991112561 ;
	setAttr ".r" -type "double3" 89.958783407018743 57.051833004563029 5.8478967360878534e-15 ;
	setAttr ".s" -type "double3" 2.9760731459566685 7.5311732521491113 2.7980077408856361 ;
createNode mesh -n "round_pillow_smallShape" -p "round_pillow_small";
	rename -uid "7FD2E243-4ABB-2CBD-225D-3C8614D09D63";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.51640617847442627 0.30468828976154327 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 86 ".uvst[0].uvsp[0:85]" -type "float2" 0.375 0.31832153
		 0.37656233 0.3125 0.40468764 0.3125 0.62343764 0.3125 0.59531236 0.3125 0.40781236
		 0.3125 0.43593764 0.3125 0.43906236 0.3125 0.46718764 0.3125 0.47031236 0.3125 0.49843764
		 0.3125 0.5015623 0.3125 0.52968764 0.3125 0.53281236 0.3125 0.56093764 0.3125 0.56406236
		 0.3125 0.59218764 0.3125 0.62499994 0.68167865 0.62343764 0.6875 0.59531236 0.6875
		 0.37656236 0.6875 0.40468764 0.68750006 0.40781236 0.6875 0.43593764 0.6875 0.43906236
		 0.6875 0.46718767 0.68750006 0.47031236 0.6875 0.49843767 0.6875 0.50156236 0.6875
		 0.52968764 0.6875 0.53281236 0.6875 0.56093764 0.6875 0.56406236 0.6875 0.59218764
		 0.6875 0.40625 0.31832153 0.375 0.68167865 0.4375 0.31832153 0.40625 0.68167865 0.46875
		 0.31832153 0.4375 0.68167865 0.5 0.31832153 0.46875 0.68167865 0.53125 0.31832141
		 0.5 0.68167865 0.5625 0.31832153 0.53125 0.68167865 0.59375 0.31832153 0.5625 0.68167865
		 0.625 0.31832141 0.59375 0.68167865 0.59943801 0.056812026 0.5 0.015623452 0.40056202
		 0.056812018 0.35937345 0.15625 0.40056202 0.25568798 0.5 0.29687658 0.59943801 0.25568798
		 0.5 0.15625 0.64062655 0.15625 0.5 0.98437655 0.40056202 0.94318801 0.35937345 0.84375
		 0.40056199 0.74431199 0.5 0.70312345 0.59943801 0.74431199 0.64062655 0.84375 0.59943801
		 0.94318801 0.5 0.84375 0.37499997 0.6635108 0.62499994 0.6635108 0.40625 0.6635108
		 0.4375 0.6635108 0.46875 0.6635108 0.5 0.6635108 0.53125 0.6635108 0.5625 0.6635108
		 0.59375 0.6635108 0.40625 0.33648938 0.375 0.33648938 0.625 0.33648926 0.59375 0.33648938
		 0.56249994 0.33648938 0.53125 0.33648926 0.5 0.33648938 0.46875 0.33648938 0.43749997
		 0.33648938;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 43 ".pt";
	setAttr ".pt[0]" -type "float3" -1.9636546e-17 -5.260461e-05 0.19682853 ;
	setAttr ".pt[1]" -type "float3" -3.4513648e-18 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[2]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[3]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[4]" -type "float3" -1.9636546e-17 -5.260461e-05 0.19682853 ;
	setAttr ".pt[5]" -type "float3" -1.9636548e-17 -5.260461e-05 0.19682853 ;
	setAttr ".pt[6]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[7]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[8]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[9]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[10]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[11]" -type "float3" 0.021085775 0.012799504 0.2122492 ;
	setAttr ".pt[12]" -type "float3" -1.1480638e-17 -3.4316367e-05 0.037479322 ;
	setAttr ".pt[13]" -type "float3" -1.2601719e-17 -3.3758937e-05 0.1263144 ;
	setAttr ".pt[14]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[15]" -type "float3" 0.021085775 0.012803619 0.1968534 ;
	setAttr ".pt[16]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[17]" -type "float3" 0 -5.260461e-05 0.19682853 ;
	setAttr ".pt[18]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[19]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[20]" -type "float3" -3.4513648e-18 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[21]" -type "float3" -3.4513731e-18 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[22]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[23]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[24]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[25]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[26]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[27]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[28]" -type "float3" 5.6334838e-18 1.1530868e-05 -0.13406542 ;
	setAttr ".pt[29]" -type "float3" -3.4513706e-18 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[30]" -type "float3" 0 5.5124583e-06 -0.020625746 ;
	setAttr ".pt[31]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[32]" -type "float3" 0 5.5124583e-06 -0.020625746 ;
	setAttr ".pt[33]" -type "float3" 0 -9.2459259e-06 0.034595098 ;
	setAttr ".pt[39]" -type "float3" 1.0335234e-17 2.0776788e-05 -0.25419125 ;
	setAttr ".pt[42]" -type "float3" -1.4127454e-17 -3.7846228e-05 0.14160769 ;
	setAttr ".pt[43]" -type "float3" 0 -3.7846228e-05 0.14160769 ;
	setAttr ".pt[44]" -type "float3" 0 -3.7846228e-05 0.14160769 ;
	setAttr ".pt[45]" -type "float3" 0 -3.7846228e-05 0.14160769 ;
	setAttr ".pt[46]" -type "float3" -3.7922207e-18 -1.7069444e-05 -0.11258356 ;
	setAttr ".pt[47]" -type "float3" 0 -3.7846228e-05 0.14160769 ;
	setAttr ".pt[48]" -type "float3" 0 -3.7846228e-05 0.14160769 ;
	setAttr ".pt[49]" -type "float3" 0 -3.7846228e-05 0.14160769 ;
	setAttr -s 50 ".vt[0:49]"  5.3751887e-09 -1.022347212 0 5.3751887e-09 1.022347212 0
		 0.63835222 -0.96895266 -0.63835222 0.57452339 -1 -0.57452333 5.795572e-09 -0.96895266 -0.90276635
		 9.9994066e-09 -1 -0.81249875 -0.63835222 -0.96895266 -0.63835222 -0.57452339 -1 -0.57452339
		 -0.90276629 -0.96895266 6.7261334e-09 -0.81249875 -1 3.3630667e-09 -0.63835216 -0.96895266 0.63835222
		 -0.57452339 -1 0.57452339 1.2521708e-08 -0.96895266 0.90276629 2.3494422e-08 -1 0.81249875
		 0.63835227 -0.96895266 0.63835227 0.57452345 -1 0.57452339 0.90276647 -0.96895266 -6.7261334e-09
		 0.81249887 -1 1.0972717e-08 0.63835222 0.96895266 -0.63835222 0.57452339 1 -0.57452333
		 5.795572e-09 0.96895266 -0.90276635 1.2521708e-08 1 -0.81249875 -0.63835222 0.96895266 -0.63835222
		 -0.57452339 1 -0.57452339 -0.90276635 0.96895266 -6.7261334e-09 -0.81249875 1 6.7261334e-09
		 -0.63835216 0.96895266 0.63835222 -0.57452339 1 0.57452339 1.9247842e-08 0.96895266 0.90276635
		 -4.7050364e-11 1 0.81249875 0.63835227 0.96895266 0.63835227 0.57452345 1 0.57452339
		 0.90276647 0.96895266 2.0178403e-08 0.81249887 1 8.8351548e-10 0.70710671 0.87205738 -0.70710671
		 0 0.87205738 -0.99999988 -0.70710671 0.87205738 -0.70710671 -0.99999988 0.87205738 0
		 -0.70710671 0.87205738 0.70710671 0 0.87205738 0.99999988 0.70710677 0.87205738 0.70710677
		 1 0.87205738 0 0 -0.87205738 -0.99999982 0.70710671 -0.87205738 -0.70710671 1 -0.87205738 0
		 0.70710671 -0.87205738 0.70710671 0 -0.87205738 0.99999982 -0.70710671 -0.87205738 0.70710671
		 -0.99999982 -0.87205738 0 -0.70710671 -0.87205738 -0.70710671;
	setAttr -s 96 ".ed[0:95]"  2 3 0 3 5 0 5 4 0 4 2 0 2 16 0 16 17 0 17 3 0
		 5 7 0 7 6 0 6 4 0 7 9 0 9 8 0 8 6 0 9 11 0 11 10 0 10 8 0 11 13 0 13 12 0 12 10 0
		 13 15 0 15 14 0 14 12 0 15 17 0 16 14 0 18 19 0 19 33 0 33 32 0 32 18 0 18 20 0 20 21 0
		 21 19 0 20 22 0 22 23 0 23 21 0 22 24 0 24 25 0 25 23 0 24 26 0 26 27 0 27 25 0 26 28 0
		 28 29 0 29 27 0 28 30 0 30 31 0 31 29 0 30 32 0 33 31 0 4 42 0 18 34 0 6 49 0 8 48 0
		 10 47 0 12 46 0 14 45 0 16 44 0 0 5 1 0 9 1 0 13 1 0 17 1 21 1 1 25 1 1 29 1 1 33 1 1
		 34 43 0 35 20 0 36 22 0 37 24 0 38 26 0 39 28 0 40 30 0 41 32 0 34 35 1 35 36 1 36 37 1
		 37 38 1 38 39 1 39 40 1 40 41 1 41 34 1 42 35 0 43 2 0 44 41 0 45 40 0 46 39 0 47 38 0
		 48 37 0 49 36 0 42 43 1 43 44 1 44 45 1 45 46 1 46 47 1 47 48 1 48 49 1 49 42 1;
	setAttr -s 48 -ch 192 ".fc[0:47]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 34
		f 4 -1 4 5 6
		mu 0 4 3 48 46 4
		f 4 -3 7 8 9
		mu 0 4 34 5 6 36
		f 4 -9 10 11 12
		mu 0 4 36 7 8 38
		f 4 -12 13 14 15
		mu 0 4 38 9 10 40
		f 4 -15 16 17 18
		mu 0 4 40 11 12 42
		f 4 -18 19 20 21
		mu 0 4 42 13 14 44
		f 4 -21 22 -6 23
		mu 0 4 44 15 16 46
		f 4 24 25 26 27
		mu 0 4 17 18 19 49
		f 4 -25 28 29 30
		mu 0 4 20 35 37 21
		f 4 -30 31 32 33
		mu 0 4 22 37 39 23
		f 4 -33 34 35 36
		mu 0 4 24 39 41 25
		f 4 -36 37 38 39
		mu 0 4 26 41 43 27
		f 4 -39 40 41 42
		mu 0 4 28 43 45 29
		f 4 -42 43 44 45
		mu 0 4 30 45 47 31
		f 4 -45 46 -27 47
		mu 0 4 32 47 49 33
		f 4 -4 48 88 81
		mu 0 4 0 34 77 78
		f 4 -10 50 95 -49
		mu 0 4 34 36 85 77
		f 4 -13 51 94 -51
		mu 0 4 36 38 84 85
		f 4 -16 52 93 -52
		mu 0 4 38 40 83 84
		f 4 -19 53 92 -53
		mu 0 4 40 42 82 83
		f 4 -22 54 91 -54
		mu 0 4 42 44 81 82
		f 4 -24 55 90 -55
		mu 0 4 44 46 80 81
		f 4 -5 -82 89 -56
		mu 0 4 46 48 79 80
		f 4 56 -2 -7 -60
		mu 0 4 57 51 50 58
		f 4 -8 -57 57 -11
		mu 0 4 52 51 57 53
		f 4 -14 -58 58 -17
		mu 0 4 54 53 57 55
		f 4 -20 -59 59 -23
		mu 0 4 56 55 57 58
		f 4 -31 60 -64 -26
		mu 0 4 66 59 67 65
		f 4 -61 -34 -37 61
		mu 0 4 67 59 60 61
		f 4 -62 -40 -43 62
		mu 0 4 67 61 62 63
		f 4 -63 -46 -48 63
		mu 0 4 67 63 64 65
		f 4 72 65 -29 49
		mu 0 4 68 70 37 35
		f 4 79 -50 -28 -72
		mu 0 4 76 69 17 49
		f 4 78 71 -47 -71
		mu 0 4 75 76 49 47
		f 4 77 70 -44 -70
		mu 0 4 74 75 47 45
		f 4 76 69 -41 -69
		mu 0 4 73 74 45 43
		f 4 75 68 -38 -68
		mu 0 4 72 73 43 41
		f 4 74 67 -35 -67
		mu 0 4 71 72 41 39
		f 4 73 66 -32 -66
		mu 0 4 70 71 39 37
		f 4 -89 80 -73 64
		mu 0 4 78 77 70 68
		f 4 -83 -90 -65 -80
		mu 0 4 76 80 79 69
		f 4 -84 -91 82 -79
		mu 0 4 75 81 80 76
		f 4 -85 -92 83 -78
		mu 0 4 74 82 81 75
		f 4 -86 -93 84 -77
		mu 0 4 73 83 82 74
		f 4 -87 -94 85 -76
		mu 0 4 72 84 83 73
		f 4 -88 -95 86 -75
		mu 0 4 71 85 84 72
		f 4 -81 -96 87 -74
		mu 0 4 70 77 85 71;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pillow3" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|pillow_grp";
	rename -uid "FC551F3C-43A3-B7F2-8C9F-86B6ADC8A7C1";
	setAttr ".t" -type "double3" 9.5480520998974203 22.401206509083323 -3.0549105735569615 ;
	setAttr ".r" -type "double3" -29.39023575594732 -49.988175066040696 1.5862092636819622 ;
	setAttr ".s" -type "double3" 11.536498701579061 10.658727704657204 3.5345302341516427 ;
createNode mesh -n "pillowShape3" -p "pillow3";
	rename -uid "B90AA62F-4D26-C67D-1D22-75971E45EBCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4934241771697998 0.37617464736104012 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 80 ".pt";
	setAttr ".pt[18]" -type "float3" 0.0057965769 0.0056879669 -0.017022874 ;
	setAttr ".pt[19]" -type "float3" 0 0 -0.0076521104 ;
	setAttr ".pt[20]" -type "float3" 0.0053376812 0.0053379526 -0.0063660871 ;
	setAttr ".pt[21]" -type "float3" 0 0 -0.0076654116 ;
	setAttr ".pt[22]" -type "float3" 0.0056973547 0.0057974625 -0.01690164 ;
	setAttr ".pt[23]" -type "float3" 0 0 -0.017779386 ;
	setAttr ".pt[24]" -type "float3" 0.0056958701 0.0057968753 0.016882341 ;
	setAttr ".pt[25]" -type "float3" 0 0 0.0075757247 ;
	setAttr ".pt[26]" -type "float3" 0.0053374767 0.0053377538 0.0063379612 ;
	setAttr ".pt[27]" -type "float3" 0 0 0.007690019 ;
	setAttr ".pt[28]" -type "float3" 0.0057961005 0.0056886198 0.016975984 ;
	setAttr ".pt[29]" -type "float3" 0 0 0.017746355 ;
	setAttr ".pt[31]" -type "float3" 0.0029500169 0.0056298189 -0.0018172329 ;
	setAttr ".pt[34]" -type "float3" 0.0056297197 0.0029503254 -0.0018170779 ;
	setAttr ".pt[37]" -type "float3" -2.7556555e-07 0.0054106861 -0.0022264465 ;
	setAttr ".pt[39]" -type "float3" 0.0053374674 -0.0053375592 -0.0063453661 ;
	setAttr ".pt[40]" -type "float3" 0 0 -0.0076591307 ;
	setAttr ".pt[41]" -type "float3" 0.0057961 -0.0056885197 -0.016943499 ;
	setAttr ".pt[42]" -type "float3" 0 0 -0.017715396 ;
	setAttr ".pt[43]" -type "float3" 0.0056969016 -0.0057968851 -0.016836125 ;
	setAttr ".pt[44]" -type "float3" 0 0 -0.0076054563 ;
	setAttr ".pt[46]" -type "float3" 0.0056295171 -0.0029502253 -0.0018159735 ;
	setAttr ".pt[49]" -type "float3" 0.0029500281 -0.005629973 -0.0018170725 ;
	setAttr ".pt[51]" -type "float3" 0.0056962837 -0.0057968535 0.016914196 ;
	setAttr ".pt[52]" -type "float3" 0 0 0.017778754 ;
	setAttr ".pt[53]" -type "float3" 0.0057965424 -0.005687458 0.017023636 ;
	setAttr ".pt[54]" -type "float3" 0 0 0.0076520056 ;
	setAttr ".pt[55]" -type "float3" 0.0053376579 -0.0053375796 0.0063255467 ;
	setAttr ".pt[56]" -type "float3" 0 0 0.0076031908 ;
	setAttr ".pt[58]" -type "float3" -2.7621883e-07 -0.005410641 -0.0022264516 ;
	setAttr ".pt[61]" -type "float3" 0.0056297197 -0.0029502758 0.0018182941 ;
	setAttr ".pt[64]" -type "float3" 0.0029499785 -0.0056297667 0.0018172048 ;
	setAttr ".pt[67]" -type "float3" -2.758924e-07 -0.0054106279 0.0022276577 ;
	setAttr ".pt[70]" -type "float3" 0.0056295199 0.0029502821 0.0018172001 ;
	setAttr ".pt[73]" -type "float3" 0.0029500227 0.0056300289 0.00181829 ;
	setAttr ".pt[76]" -type "float3" -2.7654565e-07 0.0054106857 0.0022276626 ;
	setAttr ".pt[79]" -type "float3" 0.0054103867 2.6034535e-08 -0.0022264463 ;
	setAttr ".pt[81]" -type "float3" -0.0057966444 0.0056882845 -0.016985187 ;
	setAttr ".pt[82]" -type "float3" 0 0 -0.017745618 ;
	setAttr ".pt[83]" -type "float3" -0.0056966706 0.0057973741 -0.016877633 ;
	setAttr ".pt[84]" -type "float3" 0 0 -0.0075636064 ;
	setAttr ".pt[85]" -type "float3" -0.0053379955 0.0053379484 -0.0062962431 ;
	setAttr ".pt[86]" -type "float3" 0 0 -0.0076310863 ;
	setAttr ".pt[88]" -type "float3" -0.0029505256 0.0056296182 -0.0018161371 ;
	setAttr ".pt[90]" -type "float3" -0.0056971451 0.005796921 0.016910115 ;
	setAttr ".pt[91]" -type "float3" 0 0 0.017776975 ;
	setAttr ".pt[92]" -type "float3" -0.0057970937 0.0056878114 0.017018393 ;
	setAttr ".pt[93]" -type "float3" 0 0 0.0076496289 ;
	setAttr ".pt[94]" -type "float3" -0.0053381771 0.0053377715 0.0063262461 ;
	setAttr ".pt[95]" -type "float3" 0 0 0.0076077618 ;
	setAttr ".pt[97]" -type "float3" -0.0056300731 0.0029502809 -0.0018159779 ;
	setAttr ".pt[100]" -type "float3" -0.0054109367 2.8647163e-08 -0.0022264512 ;
	setAttr ".pt[102]" -type "float3" -0.005697147 -0.0057968646 -0.016877262 ;
	setAttr ".pt[103]" -type "float3" 0 0 -0.017744876 ;
	setAttr ".pt[104]" -type "float3" -0.0057970965 -0.0056877588 -0.016985578 ;
	setAttr ".pt[105]" -type "float3" 0 0 -0.0076175351 ;
	setAttr ".pt[106]" -type "float3" -0.0053381808 -0.0053377114 -0.0062941429 ;
	setAttr ".pt[107]" -type "float3" 0 0 -0.0075756684 ;
	setAttr ".pt[109]" -type "float3" -0.0056298631 -0.0029502218 -0.001816138 ;
	setAttr ".pt[111]" -type "float3" -0.0057966434 -0.0056882333 0.017018208 ;
	setAttr ".pt[112]" -type "float3" 0 0 0.017778503 ;
	setAttr ".pt[113]" -type "float3" -0.0056966804 -0.0057973415 0.016911481 ;
	setAttr ".pt[114]" -type "float3" 0 0 0.0075965133 ;
	setAttr ".pt[115]" -type "float3" -0.0053379955 -0.0053378944 0.0063284757 ;
	setAttr ".pt[116]" -type "float3" 0 0 0.0076632737 ;
	setAttr ".pt[118]" -type "float3" -0.0029505296 -0.0056297667 -0.0018159758 ;
	setAttr ".pt[121]" -type "float3" -0.0029505733 -0.005629973 0.0018182936 ;
	setAttr ".pt[124]" -type "float3" -0.0056300722 -0.0029502253 0.0018172001 ;
	setAttr ".pt[127]" -type "float3" 0.0054103858 2.6034535e-08 0.0022276619 ;
	setAttr ".pt[130]" -type "float3" -0.0054109404 2.6034535e-08 0.0022276619 ;
	setAttr ".pt[133]" -type "float3" -0.0056298622 0.0029502695 0.0018173553 ;
	setAttr ".pt[136]" -type "float3" -0.0029505298 0.0056298189 0.0018172039 ;
	setAttr ".pt[138]" -type "float3" 0.0053378339 0.0053381133 -0.01553804 ;
	setAttr ".pt[139]" -type "float3" 0.0053373273 0.0053375964 0.015504681 ;
	setAttr ".pt[140]" -type "float3" 0.0053373277 -0.0053375456 -0.015471433 ;
	setAttr ".pt[141]" -type "float3" 0.0053378004 -0.0053375866 0.01553789 ;
	setAttr ".pt[142]" -type "float3" -0.0053378642 0.005338077 -0.01550494 ;
	setAttr ".pt[143]" -type "float3" -0.0053383103 0.005337643 0.015537689 ;
	setAttr ".pt[144]" -type "float3" -0.0053383075 -0.0053375904 -0.015504717 ;
	setAttr ".pt[145]" -type "float3" -0.0053378623 -0.0053380341 0.015537652 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape5" -p "pillow3";
	rename -uid "5C7C588A-4CCC-1DB0-4742-2D9E997D5E94";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000001490116119 0.37500001490116119 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 121 ".uvst[0].uvsp[0:120]" -type "float2" 0.5 0.625 0.5 0.125
		 0.37852642 0.94935614 0.37552088 0.93975037 0.37552571 0.81417471 0.375898 -3.085006e-09
		 0.43294376 1.3969839e-09 0.37823847 0.0032384668 0.4375 0 0.49957642 0.00042358108
		 0.37823847 0.2467615 0.37855938 0.32386965 0.37552571 0.43918562 0.125 0.24549557
		 0.125 0.1875 0.37852642 0.44935456 0.375 0.74909395 0.375 0.69208223 0.37823844 0.74676156
		 0.62409395 0 0.56708211 0 0.689174 0.00052574463 0.69818497 0.0035264529 0.67611796
		 0.24644059 0.81082535 0.24947429 0.62147355 0.32318479 0.80181348 0.24647354 0.875
		 0.1875 0.125 0.18583454 0.875 0.18718334 0.875 0.125 0.875 0.0045044087 0.875 0.0625
		 0.62147355 0.8231864 0.50042361 0.00042358413 0.5625 0 0.125 0.125 0.125 0.062816635
		 0.62457639 0.62542361 0.625 0.6875 0.4375 0.0030095838 0.4375 0.06249997 0.4377692
		 0.27677882 0.37800953 0.68750006 0.43749997 0.68750006 0.37855941 0.82387042 0.4377692
		 0.77677923 0.82388103 0.0035594283 0.84806252 0.062770769 0.19887039 0.0035594292
		 0.32435626 0.0035264147 0.34822115 0.062769189 0.5625 0.0030184686 0.5 0.0625 0.5
		 0.47771072 0.56222922 0.27716166 0.5 0.746876 0.5 0.97771114 0.56222922 0.77716202
		 0.15177925 0.18723084 0.30113035 0.24644059 0.1761187 0.24644059 0.37823844 0.50323844
		 0.43749997 0.5030095 0.43749997 0.56249994 0.56250006 0.50300062 0.5 0.56249988 0.65193713
		 0.18722923 0.5 0.24687593 0.37800956 0.1875 0.4375 0.18750003 0.6219905 0.0625 0.6217615
		 0.0032384649 0.5625 0.24699043 0.5625 0.18750003 0.6217615 0.2467615 0.56223083 0.47322074
		 0.62144059 0.44888124 0.6219905 0.5625 0.62176156 0.50323844 0.5625 0.74699044 0.5625
		 0.68750006 0.62176156 0.74676156 0.56222922 0.97306287 0.62144059 0.94888198 0.5
		 0.0031240731 0.4375 0.24699932 0.5 0.1875 0.43777078 0.47283795 0.5 0.27228886 0.5
		 0.503124 0.43749991 0.74699938 0.5 0.68750006 0.4377692 0.97299796 0.5 0.77228928
		 0.15216205 0.062770754 0.35271114 0.125 0.37812397 0.625 0.62198162 0.68750006 0.56250006
		 0.625 0.65216166 0.062770754 0.85271072 0.125 0.62187594 0.125 0.5625 0.0625 0.37800068
		 0.062499981 0.4375 0.125 0.14728928 0.125 0.34783837 0.18722925 0.37812406 0.125
		 0.62198156 0.1875 0.5625 0.125 0.6472888 0.125 0.84783792 0.18722923 0.621876 0.625
		 0.56250012 0.5625 0.37800059 0.56249994 0.43749991 0.625 0.37552571 0.314174 0.68524963
		 0.24947913 0.875 0.24640988 0.8147487 0.00052088778;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 -0.07847707 ;
	setAttr ".pt[1]" -type "float3" 0 0 0.07847707 ;
	setAttr ".pt[2]" -type "float3" 0 0 0.067240857 ;
	setAttr ".pt[3]" -type "float3" 0 0 -0.0672407 ;
	setAttr ".pt[4]" -type "float3" 0 0 0.067240857 ;
	setAttr ".pt[5]" -type "float3" 0 0 -0.0672407 ;
	setAttr ".pt[6]" -type "float3" 0 0 -0.062606692 ;
	setAttr ".pt[7]" -type "float3" 0 0 -0.0672407 ;
	setAttr ".pt[8]" -type "float3" 0 0 -0.062606692 ;
	setAttr ".pt[9]" -type "float3" 0 0 0.062606692 ;
	setAttr ".pt[10]" -type "float3" 0 0 0.067240857 ;
	setAttr ".pt[11]" -type "float3" 0 0 0.062606692 ;
	setAttr ".pt[12]" -type "float3" 0 0 0.062606692 ;
	setAttr ".pt[13]" -type "float3" 0 0 0.067240857 ;
	setAttr ".pt[14]" -type "float3" 0 0 0.062606692 ;
	setAttr ".pt[15]" -type "float3" 0 0 -0.062606692 ;
	setAttr ".pt[16]" -type "float3" 0 0 -0.0672407 ;
	setAttr ".pt[17]" -type "float3" 0 0 -0.062606692 ;
	setAttr ".pt[86]" -type "float3" -0.0062480271 0 0.024292469 ;
	setAttr ".pt[87]" -type "float3" 0.0062479973 0 -0.024292469 ;
	setAttr -s 90 ".vt[0:89]"  0 0 -0.48190594 0 0 0.48190689 0.25 0 0.41290855
		 0.25 0 -0.41290665 -0.25 0 0.41290855 -0.25 0 -0.41290665 -0.26002654 -0.26002669 -0.38445044
		 0 -0.25000012 -0.41290665 0.26002654 -0.26002669 -0.38445044 0.26002654 -0.26002669 0.38445139
		 0 -0.25000012 0.41290855 -0.26002654 -0.26002669 0.38445139 -0.26002654 0.26002669 0.38445139
		 0 0.25 0.41290855 0.26002654 0.26002669 0.38445139 0.26002654 0.26002669 -0.38445044
		 0 0.25 -0.41290665 -0.26002654 0.26002669 -0.38445044 -0.52486908 -0.53897476 0.035040379
		 -0.5250743 -0.52507436 0.080927372 -0.53897482 -0.52486908 0.035040379 -0.26957035 -0.50753212 0.19020414
		 -0.26888999 -0.51996672 0.14157343 -0.5250743 0.52507424 0.080927372 -0.52472669 0.53896427 0.034718037
		 -0.53896427 0.52472663 0.034718037 -0.26888999 0.5199666 0.14157343 -0.26957175 0.50756907 0.19017506
		 -0.53896427 0.52472663 -0.03471756 -0.52486908 0.53897476 -0.035038471 -0.5250743 0.52507424 -0.080926418
		 -0.26888311 0.51996613 -0.14134312 -0.26957035 0.50753212 -0.19020319 -0.5250743 -0.52507436 -0.080926418
		 -0.52472669 -0.53896427 -0.03471756 -0.53896427 -0.52472663 -0.03471756 -0.26888999 -0.51996672 -0.14157248
		 -0.26957175 -0.50756907 -0.19017315 -0.51996672 -0.26889002 0.14157343 -0.50756907 -0.2695719 0.19017506
		 0.52472669 -0.53896427 0.034718037 0.53897482 -0.52486908 0.035040379 0.5250743 -0.52507436 0.080927372
		 0.51996624 -0.26888323 0.14134407 0.50753224 -0.26957047 0.19020414 0.53896427 0.52472663 0.034718037
		 0.52486908 0.53897476 0.035040379 0.5250743 0.52507424 0.080927372 0.52472669 0.53896427 -0.03471756
		 0.53897482 0.52486897 -0.035038471 0.5250743 0.52507424 -0.080926418 -0.51996672 0.26889014 -0.14157248
		 -0.50756907 0.26957178 -0.19017315 0.51996624 0.26888323 -0.14134312 0.50753224 0.26957035 -0.19020319
		 0.53896427 -0.52472663 -0.03471756 0.52486908 -0.53897476 -0.035038471 0.5250743 -0.52507436 -0.080926418
		 0 -0.50000012 0.17893934 0 -0.48750401 0.22752428 0.26956892 -0.50749528 0.19023323
		 0.26888311 -0.51996613 0.14134407 0 0.5 0.17893934 0 0.48750401 0.22752428 0.26888311 0.51996613 0.14134407
		 0.26957035 0.50753212 0.19020414 0 0.5 -0.17893744 0 0.48750401 -0.22752237 0.26888999 0.5199666 -0.14157248
		 0.26957175 0.50756907 -0.19017315 0 -0.50000012 -0.17893744 0 -0.48750401 -0.22752237
		 0.26888311 -0.51996613 -0.14134312 0.26957035 -0.50753212 -0.19020319 -0.48750398 0 0.22752428
		 -0.5 0 0.17893934 -0.51996624 0.26888323 0.14134407 -0.50753224 0.26957035 0.19020414
		 -0.48750398 0 -0.22752237 -0.5 0 -0.17893744 -0.51996624 -0.26888323 -0.14134312
		 -0.50753224 -0.26957047 -0.19020319 0.5 0 -0.17893744 0.48750398 0 -0.22752237 0.50749534 -0.26956904 -0.19023228
		 0.51996624 -0.26888323 -0.14134312 0.5 0 0.17893934 0.48750398 0 0.22752428 0.50749534 0.26956892 0.19023323
		 0.51996624 0.26888323 0.14134407;
	setAttr -s 180 ".ed";
	setAttr ".ed[0:165]"  0 7 1 1 13 1 0 3 1 1 4 1 2 1 1 2 14 1 3 8 1 5 0 1 4 12 1
		 5 6 1 9 2 1 10 1 1 11 4 1 6 7 1 7 8 1 9 10 1 10 11 1 15 3 1 16 0 1 17 5 1 12 13 1
		 13 14 1 15 16 1 16 17 1 18 20 0 20 35 0 35 34 0 34 18 0 19 18 0 18 22 0 22 21 1 21 19 0
		 20 19 0 19 39 0 39 38 1 38 20 0 22 58 0 58 59 1 59 21 0 23 25 0 25 76 0 76 77 1 77 23 0
		 24 23 0 23 27 0 27 26 1 26 24 0 25 24 0 24 29 0 29 28 0 28 25 0 27 63 0 63 62 1 62 26 0
		 28 30 0 30 52 0 52 51 1 51 28 0 30 29 0 29 31 0 31 32 1 32 30 0 31 66 0 66 67 1 67 32 0
		 33 35 0 35 80 0 80 81 1 81 33 0 34 33 0 33 37 0 37 36 1 36 34 0 37 71 0 71 70 1 70 36 0
		 39 74 0 74 75 1 75 38 0 40 42 0 42 60 0 60 61 1 61 40 0 41 40 0 40 56 0 56 55 0 55 41 0
		 42 41 0 41 43 0 43 44 1 44 42 0 43 86 0 86 87 1 87 44 0 45 47 0 47 88 0 88 89 1 89 45 0
		 46 45 0 45 49 0 49 48 0 48 46 0 47 46 0 46 64 0 64 65 1 65 47 0 48 50 0 50 69 0 69 68 1
		 68 48 0 50 49 0 49 53 0 53 54 1 54 50 0 52 78 0 78 79 1 79 51 0 53 82 0 82 83 1 83 54 0
		 55 57 0 57 84 0 84 85 1 85 55 0 57 56 0 56 72 0 72 73 1 73 57 0 58 61 0 60 59 0 63 65 0
		 64 62 0 66 68 0 69 67 0 71 73 0 72 70 0 74 77 0 76 75 0 78 81 0 80 79 0 82 85 0 84 83 0
		 86 89 0 88 87 0 21 11 1 11 39 1 26 31 1 81 6 1 6 37 1 36 22 1 85 43 1 38 80 1 60 9 1
		 10 59 1 66 62 1 64 68 1 71 7 1 8 73 1 58 70 1 72 61 1 51 76 1 32 17 1 17 52 1 69 15 1
		 16 67 1 89 53 1;
	setAttr ".ed[166:179]" 63 13 1 14 65 1 77 12 1 12 27 1 44 9 1 14 88 1 54 15 1
		 8 84 1 75 79 1 78 5 1 3 83 1 82 86 1 87 2 1 4 74 1;
	setAttr -s 92 -ch 360 ".fc[0:91]" -type "polyFaces" 
		f 4 -14 -10 7 0
		mu 0 4 92 44 116 0
		f 4 -15 -1 2 6
		mu 0 4 81 92 0 99
		f 4 -16 10 4 -12
		mu 0 4 53 103 110 1
		f 4 -13 -17 11 3
		mu 0 4 105 41 53 1
		f 4 -21 -9 -4 1
		mu 0 4 87 70 105 1
		f 4 -22 -2 -5 5
		mu 0 4 74 87 1 110
		f 4 -23 17 -3 -19
		mu 0 4 66 114 99 0
		f 4 -20 -24 18 -8
		mu 0 4 116 64 66 0
		f 4 24 25 26 27
		mu 0 4 2 3 4 45
		f 4 28 29 30 31
		mu 0 4 7 5 6 40
		f 4 32 33 34 35
		mu 0 4 50 7 104 51
		f 4 -31 36 37 38
		mu 0 4 40 8 9 85
		f 4 39 40 41 42
		mu 0 4 10 60 107 69
		f 4 43 44 45 46
		mu 0 4 11 10 86 42
		f 4 47 48 49 50
		mu 0 4 117 11 15 12
		f 4 -46 51 52 53
		mu 0 4 42 86 68 89
		f 4 54 55 56 57
		mu 0 4 61 13 14 59
		f 4 58 59 60 61
		mu 0 4 62 15 88 63
		f 4 -61 62 63 64
		mu 0 4 63 88 54 90
		f 4 65 66 67 68
		mu 0 4 18 16 17 43
		f 4 69 70 71 72
		mu 0 4 45 18 91 46
		f 4 -72 73 74 75
		mu 0 4 46 91 56 94
		f 4 -35 76 77 78
		mu 0 4 51 104 108 96
		f 4 79 80 81 82
		mu 0 4 19 72 52 20
		f 4 83 84 85 86
		mu 0 4 22 21 120 47
		f 4 87 88 89 90
		mu 0 4 72 22 100 71
		f 4 -90 91 92 93
		mu 0 4 71 100 111 102
		f 4 94 95 96 97
		mu 0 4 23 75 109 67
		f 4 98 99 100 101
		mu 0 4 118 23 26 24
		f 4 102 103 104 105
		mu 0 4 75 25 55 73
		f 4 106 107 108 109
		mu 0 4 77 79 65 76
		f 4 110 111 112 113
		mu 0 4 119 26 112 27
		f 4 -57 114 115 116
		mu 0 4 59 28 36 106
		f 4 -113 117 118 119
		mu 0 4 29 112 101 30
		f 4 120 121 122 123
		mu 0 4 47 31 32 48
		f 4 124 125 126 127
		mu 0 4 82 33 58 80
		f 4 -38 128 -82 129
		mu 0 4 85 34 35 52
		f 4 -53 130 -105 131
		mu 0 4 89 68 73 55
		f 4 -64 132 -109 133
		mu 0 4 90 54 76 65
		f 4 -75 134 -127 135
		mu 0 4 94 56 80 58
		f 4 -78 136 -42 137
		mu 0 4 96 108 69 107
		f 4 -116 138 -68 139
		mu 0 4 106 36 37 95
		f 4 -119 140 -123 141
		mu 0 4 113 38 39 98
		f 4 -93 142 -97 143
		mu 0 4 102 111 67 109
		f 4 -32 144 145 -34
		mu 0 4 7 40 41 104
		f 4 -47 146 -60 -49
		mu 0 4 11 42 88 15
		f 4 147 148 -71 -69
		mu 0 4 43 44 91 18
		f 4 -73 149 -30 -28
		mu 0 4 45 46 93 2
		f 4 -87 -124 150 -89
		mu 0 4 22 47 48 100
		f 4 -26 -36 151 -67
		mu 0 4 49 50 51 95
		f 4 152 15 153 -130
		mu 0 4 52 103 53 85
		f 4 154 -132 155 -133
		mu 0 4 54 89 55 76
		f 4 156 14 157 -135
		mu 0 4 56 92 81 80
		f 4 158 -136 159 -129
		mu 0 4 57 94 58 83
		f 4 160 -41 -51 -58
		mu 0 4 59 107 60 61
		f 4 -62 161 162 -56
		mu 0 4 62 63 64 115
		f 4 163 22 164 -134
		mu 0 4 65 114 66 90
		f 4 165 -112 -100 -98
		mu 0 4 67 112 26 23
		f 4 166 21 167 -131
		mu 0 4 68 87 74 73
		f 4 168 169 -45 -43
		mu 0 4 69 70 86 10
		f 4 170 -153 -81 -91
		mu 0 4 71 103 52 72
		f 4 -168 171 -96 -106
		mu 0 4 73 74 109 75
		f 4 -156 -104 -102 -110
		mu 0 4 76 55 25 77
		f 4 172 -164 -108 -114
		mu 0 4 78 114 65 79
		f 4 -158 173 -122 -128
		mu 0 4 80 81 98 82
		f 4 -160 -126 -85 -83
		mu 0 4 83 58 33 84
		f 4 16 -145 -39 -154
		mu 0 4 53 41 40 85
		f 4 -170 20 -167 -52
		mu 0 4 86 70 87 68
		f 4 -147 -54 -155 -63
		mu 0 4 88 42 89 54
		f 4 23 -162 -65 -165
		mu 0 4 66 64 63 90
		f 4 -149 13 -157 -74
		mu 0 4 91 44 92 56
		f 4 -150 -76 -159 -37
		mu 0 4 93 46 94 57
		f 4 -152 -79 174 -140
		mu 0 4 95 51 96 106
		f 4 175 9 -148 -139
		mu 0 4 97 116 44 43
		f 4 -174 -7 176 -142
		mu 0 4 98 81 99 113
		f 4 -151 -141 177 -92
		mu 0 4 100 48 101 111
		f 4 178 -11 -171 -94
		mu 0 4 102 110 103 71
		f 4 -146 12 179 -77
		mu 0 4 104 41 105 108
		f 4 -175 -138 -161 -117
		mu 0 4 106 96 107 59
		f 4 -180 8 -169 -137
		mu 0 4 108 105 70 69
		f 4 -172 -6 -179 -144
		mu 0 4 109 74 110 102
		f 4 -178 -118 -166 -143
		mu 0 4 111 101 112 67
		f 4 -177 -18 -173 -120
		mu 0 4 113 99 114 78
		f 4 -163 19 -176 -115
		mu 0 4 115 64 116 97
		f 3 -29 -33 -25
		mu 0 3 5 7 50
		f 3 -44 -48 -40
		mu 0 3 10 11 117
		f 3 -50 -59 -55
		mu 0 3 12 15 62
		f 3 -70 -27 -66
		mu 0 3 18 45 16
		f 3 -84 -88 -80
		mu 0 3 21 22 72
		f 3 -99 -103 -95
		mu 0 3 23 118 75
		f 3 -101 -111 -107
		mu 0 3 24 26 119
		f 3 -86 -125 -121
		mu 0 3 47 120 31;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pillow2" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|pillow_grp";
	rename -uid "89B99736-446A-3654-03F2-5398CEE8E2A9";
	setAttr ".t" -type "double3" 17.782679241213334 22.401206509083323 0.43447804688017122 ;
	setAttr ".r" -type "double3" -16.231296867366733 -41.169863221571937 -6.6271283016410001 ;
	setAttr ".s" -type "double3" 13.854887247673117 10.658727704657204 3.5345302341516427 ;
createNode mesh -n "pillowShape2" -p "pillow2";
	rename -uid "13DB2FC2-4A0F-158A-F072-C99887CE722A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000001490116119 0.37500001490116119 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 121 ".uvst[0].uvsp[0:120]" -type "float2" 0.5 0.625 0.5 0.125
		 0.37852642 0.94935614 0.37552088 0.93975037 0.37552571 0.81417471 0.375898 -3.085006e-09
		 0.43294376 1.3969839e-09 0.37823847 0.0032384668 0.4375 0 0.49957642 0.00042358108
		 0.37823847 0.2467615 0.37855938 0.32386965 0.37552571 0.43918562 0.125 0.24549557
		 0.125 0.1875 0.37852642 0.44935456 0.375 0.74909395 0.375 0.69208223 0.37823844 0.74676156
		 0.62409395 0 0.56708211 0 0.689174 0.00052574463 0.69818497 0.0035264529 0.67611796
		 0.24644059 0.81082535 0.24947429 0.62147355 0.32318479 0.80181348 0.24647354 0.875
		 0.1875 0.125 0.18583454 0.875 0.18718334 0.875 0.125 0.875 0.0045044087 0.875 0.0625
		 0.62147355 0.8231864 0.50042361 0.00042358413 0.5625 0 0.125 0.125 0.125 0.062816635
		 0.62457639 0.62542361 0.625 0.6875 0.4375 0.0030095838 0.4375 0.06249997 0.4377692
		 0.27677882 0.37800953 0.68750006 0.43749997 0.68750006 0.37855941 0.82387042 0.4377692
		 0.77677923 0.82388103 0.0035594283 0.84806252 0.062770769 0.19887039 0.0035594292
		 0.32435626 0.0035264147 0.34822115 0.062769189 0.5625 0.0030184686 0.5 0.0625 0.5
		 0.47771072 0.56222922 0.27716166 0.5 0.746876 0.5 0.97771114 0.56222922 0.77716202
		 0.15177925 0.18723084 0.30113035 0.24644059 0.1761187 0.24644059 0.37823844 0.50323844
		 0.43749997 0.5030095 0.43749997 0.56249994 0.56250006 0.50300062 0.5 0.56249988 0.65193713
		 0.18722923 0.5 0.24687593 0.37800956 0.1875 0.4375 0.18750003 0.6219905 0.0625 0.6217615
		 0.0032384649 0.5625 0.24699043 0.5625 0.18750003 0.6217615 0.2467615 0.56223083 0.47322074
		 0.62144059 0.44888124 0.6219905 0.5625 0.62176156 0.50323844 0.5625 0.74699044 0.5625
		 0.68750006 0.62176156 0.74676156 0.56222922 0.97306287 0.62144059 0.94888198 0.5
		 0.0031240731 0.4375 0.24699932 0.5 0.1875 0.43777078 0.47283795 0.5 0.27228886 0.5
		 0.503124 0.43749991 0.74699938 0.5 0.68750006 0.4377692 0.97299796 0.5 0.77228928
		 0.15216205 0.062770754 0.35271114 0.125 0.37812397 0.625 0.62198162 0.68750006 0.56250006
		 0.625 0.65216166 0.062770754 0.85271072 0.125 0.62187594 0.125 0.5625 0.0625 0.37800068
		 0.062499981 0.4375 0.125 0.14728928 0.125 0.34783837 0.18722925 0.37812406 0.125
		 0.62198156 0.1875 0.5625 0.125 0.6472888 0.125 0.84783792 0.18722923 0.621876 0.625
		 0.56250012 0.5625 0.37800059 0.56249994 0.43749991 0.625 0.37552571 0.314174 0.68524963
		 0.24947913 0.875 0.24640988 0.8147487 0.00052088778;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  0 0 -0.07847707 0 0 0.07847707 
		0 0 0.067240857 0 0 -0.0672407 0 0 0.067240857 0 0 -0.0672407 0 0 -0.062606692 0 
		0 -0.0672407 0 0 -0.062606692 0 0 0.062606692 0 0 0.067240857 0 0 0.062606692 0 0 
		0.062606692 0 0 0.067240857 0 0 0.062606692 0 0 -0.062606692 0 0 -0.0672407 0 0 -0.062606692;
	setAttr -s 90 ".vt[0:89]"  0 0 -0.48190594 0 0 0.48190689 0.25 0 0.41290855
		 0.25 0 -0.41290665 -0.25 0 0.41290855 -0.25 0 -0.41290665 -0.26002654 -0.26002669 -0.38445044
		 0 -0.25000012 -0.41290665 0.26002654 -0.26002669 -0.38445044 0.26002654 -0.26002669 0.38445139
		 0 -0.25000012 0.41290855 -0.26002654 -0.26002669 0.38445139 -0.26002654 0.26002669 0.38445139
		 0 0.25 0.41290855 0.26002654 0.26002669 0.38445139 0.26002654 0.26002669 -0.38445044
		 0 0.25 -0.41290665 -0.26002654 0.26002669 -0.38445044 -0.52486908 -0.53897476 0.035040379
		 -0.5250743 -0.52507436 0.080927372 -0.53897482 -0.52486908 0.035040379 -0.26957035 -0.50753212 0.19020414
		 -0.26888999 -0.51996672 0.14157343 -0.5250743 0.52507424 0.080927372 -0.52472669 0.53896427 0.034718037
		 -0.53896427 0.52472663 0.034718037 -0.26888999 0.5199666 0.14157343 -0.26957175 0.50756907 0.19017506
		 -0.53896427 0.52472663 -0.03471756 -0.52486908 0.53897476 -0.035038471 -0.5250743 0.52507424 -0.080926418
		 -0.26888311 0.51996613 -0.14134312 -0.26957035 0.50753212 -0.19020319 -0.5250743 -0.52507436 -0.080926418
		 -0.52472669 -0.53896427 -0.03471756 -0.53896427 -0.52472663 -0.03471756 -0.26888999 -0.51996672 -0.14157248
		 -0.26957175 -0.50756907 -0.19017315 -0.51996672 -0.26889002 0.14157343 -0.50756907 -0.2695719 0.19017506
		 0.52472669 -0.53896427 0.034718037 0.53897482 -0.52486908 0.035040379 0.5250743 -0.52507436 0.080927372
		 0.51996624 -0.26888323 0.14134407 0.50753224 -0.26957047 0.19020414 0.53896427 0.52472663 0.034718037
		 0.52486908 0.53897476 0.035040379 0.5250743 0.52507424 0.080927372 0.52472669 0.53896427 -0.03471756
		 0.53897482 0.52486897 -0.035038471 0.5250743 0.52507424 -0.080926418 -0.51996672 0.26889014 -0.14157248
		 -0.50756907 0.26957178 -0.19017315 0.51996624 0.26888323 -0.14134312 0.50753224 0.26957035 -0.19020319
		 0.53896427 -0.52472663 -0.03471756 0.52486908 -0.53897476 -0.035038471 0.5250743 -0.52507436 -0.080926418
		 0 -0.50000012 0.17893934 0 -0.48750401 0.22752428 0.26956892 -0.50749528 0.19023323
		 0.26888311 -0.51996613 0.14134407 0 0.5 0.17893934 0 0.48750401 0.22752428 0.26888311 0.51996613 0.14134407
		 0.26957035 0.50753212 0.19020414 0 0.5 -0.17893744 0 0.48750401 -0.22752237 0.26888999 0.5199666 -0.14157248
		 0.26957175 0.50756907 -0.19017315 0 -0.50000012 -0.17893744 0 -0.48750401 -0.22752237
		 0.26888311 -0.51996613 -0.14134312 0.26957035 -0.50753212 -0.19020319 -0.48750398 0 0.22752428
		 -0.5 0 0.17893934 -0.51996624 0.26888323 0.14134407 -0.50753224 0.26957035 0.19020414
		 -0.48750398 0 -0.22752237 -0.5 0 -0.17893744 -0.51996624 -0.26888323 -0.14134312
		 -0.50753224 -0.26957047 -0.19020319 0.5 0 -0.17893744 0.48750398 0 -0.22752237 0.50749534 -0.26956904 -0.19023228
		 0.51996624 -0.26888323 -0.14134312 0.5 0 0.17893934 0.48750398 0 0.22752428 0.50749534 0.26956892 0.19023323
		 0.51996624 0.26888323 0.14134407;
	setAttr -s 180 ".ed";
	setAttr ".ed[0:165]"  0 7 1 1 13 1 0 3 1 1 4 1 2 1 1 2 14 1 3 8 1 5 0 1 4 12 1
		 5 6 1 9 2 1 10 1 1 11 4 1 6 7 1 7 8 1 9 10 1 10 11 1 15 3 1 16 0 1 17 5 1 12 13 1
		 13 14 1 15 16 1 16 17 1 18 20 0 20 35 0 35 34 0 34 18 0 19 18 0 18 22 0 22 21 1 21 19 0
		 20 19 0 19 39 0 39 38 1 38 20 0 22 58 0 58 59 1 59 21 0 23 25 0 25 76 0 76 77 1 77 23 0
		 24 23 0 23 27 0 27 26 1 26 24 0 25 24 0 24 29 0 29 28 0 28 25 0 27 63 0 63 62 1 62 26 0
		 28 30 0 30 52 0 52 51 1 51 28 0 30 29 0 29 31 0 31 32 1 32 30 0 31 66 0 66 67 1 67 32 0
		 33 35 0 35 80 0 80 81 1 81 33 0 34 33 0 33 37 0 37 36 1 36 34 0 37 71 0 71 70 1 70 36 0
		 39 74 0 74 75 1 75 38 0 40 42 0 42 60 0 60 61 1 61 40 0 41 40 0 40 56 0 56 55 0 55 41 0
		 42 41 0 41 43 0 43 44 1 44 42 0 43 86 0 86 87 1 87 44 0 45 47 0 47 88 0 88 89 1 89 45 0
		 46 45 0 45 49 0 49 48 0 48 46 0 47 46 0 46 64 0 64 65 1 65 47 0 48 50 0 50 69 0 69 68 1
		 68 48 0 50 49 0 49 53 0 53 54 1 54 50 0 52 78 0 78 79 1 79 51 0 53 82 0 82 83 1 83 54 0
		 55 57 0 57 84 0 84 85 1 85 55 0 57 56 0 56 72 0 72 73 1 73 57 0 58 61 0 60 59 0 63 65 0
		 64 62 0 66 68 0 69 67 0 71 73 0 72 70 0 74 77 0 76 75 0 78 81 0 80 79 0 82 85 0 84 83 0
		 86 89 0 88 87 0 21 11 1 11 39 1 26 31 1 81 6 1 6 37 1 36 22 1 85 43 1 38 80 1 60 9 1
		 10 59 1 66 62 1 64 68 1 71 7 1 8 73 1 58 70 1 72 61 1 51 76 1 32 17 1 17 52 1 69 15 1
		 16 67 1 89 53 1;
	setAttr ".ed[166:179]" 63 13 1 14 65 1 77 12 1 12 27 1 44 9 1 14 88 1 54 15 1
		 8 84 1 75 79 1 78 5 1 3 83 1 82 86 1 87 2 1 4 74 1;
	setAttr -s 92 -ch 360 ".fc[0:91]" -type "polyFaces" 
		f 4 -14 -10 7 0
		mu 0 4 92 44 116 0
		f 4 -15 -1 2 6
		mu 0 4 81 92 0 99
		f 4 -16 10 4 -12
		mu 0 4 53 103 110 1
		f 4 -13 -17 11 3
		mu 0 4 105 41 53 1
		f 4 -21 -9 -4 1
		mu 0 4 87 70 105 1
		f 4 -22 -2 -5 5
		mu 0 4 74 87 1 110
		f 4 -23 17 -3 -19
		mu 0 4 66 114 99 0
		f 4 -20 -24 18 -8
		mu 0 4 116 64 66 0
		f 4 24 25 26 27
		mu 0 4 2 3 4 45
		f 4 28 29 30 31
		mu 0 4 7 5 6 40
		f 4 32 33 34 35
		mu 0 4 50 7 104 51
		f 4 -31 36 37 38
		mu 0 4 40 8 9 85
		f 4 39 40 41 42
		mu 0 4 10 60 107 69
		f 4 43 44 45 46
		mu 0 4 11 10 86 42
		f 4 47 48 49 50
		mu 0 4 117 11 15 12
		f 4 -46 51 52 53
		mu 0 4 42 86 68 89
		f 4 54 55 56 57
		mu 0 4 61 13 14 59
		f 4 58 59 60 61
		mu 0 4 62 15 88 63
		f 4 -61 62 63 64
		mu 0 4 63 88 54 90
		f 4 65 66 67 68
		mu 0 4 18 16 17 43
		f 4 69 70 71 72
		mu 0 4 45 18 91 46
		f 4 -72 73 74 75
		mu 0 4 46 91 56 94
		f 4 -35 76 77 78
		mu 0 4 51 104 108 96
		f 4 79 80 81 82
		mu 0 4 19 72 52 20
		f 4 83 84 85 86
		mu 0 4 22 21 120 47
		f 4 87 88 89 90
		mu 0 4 72 22 100 71
		f 4 -90 91 92 93
		mu 0 4 71 100 111 102
		f 4 94 95 96 97
		mu 0 4 23 75 109 67
		f 4 98 99 100 101
		mu 0 4 118 23 26 24
		f 4 102 103 104 105
		mu 0 4 75 25 55 73
		f 4 106 107 108 109
		mu 0 4 77 79 65 76
		f 4 110 111 112 113
		mu 0 4 119 26 112 27
		f 4 -57 114 115 116
		mu 0 4 59 28 36 106
		f 4 -113 117 118 119
		mu 0 4 29 112 101 30
		f 4 120 121 122 123
		mu 0 4 47 31 32 48
		f 4 124 125 126 127
		mu 0 4 82 33 58 80
		f 4 -38 128 -82 129
		mu 0 4 85 34 35 52
		f 4 -53 130 -105 131
		mu 0 4 89 68 73 55
		f 4 -64 132 -109 133
		mu 0 4 90 54 76 65
		f 4 -75 134 -127 135
		mu 0 4 94 56 80 58
		f 4 -78 136 -42 137
		mu 0 4 96 108 69 107
		f 4 -116 138 -68 139
		mu 0 4 106 36 37 95
		f 4 -119 140 -123 141
		mu 0 4 113 38 39 98
		f 4 -93 142 -97 143
		mu 0 4 102 111 67 109
		f 4 -32 144 145 -34
		mu 0 4 7 40 41 104
		f 4 -47 146 -60 -49
		mu 0 4 11 42 88 15
		f 4 147 148 -71 -69
		mu 0 4 43 44 91 18
		f 4 -73 149 -30 -28
		mu 0 4 45 46 93 2
		f 4 -87 -124 150 -89
		mu 0 4 22 47 48 100
		f 4 -26 -36 151 -67
		mu 0 4 49 50 51 95
		f 4 152 15 153 -130
		mu 0 4 52 103 53 85
		f 4 154 -132 155 -133
		mu 0 4 54 89 55 76
		f 4 156 14 157 -135
		mu 0 4 56 92 81 80
		f 4 158 -136 159 -129
		mu 0 4 57 94 58 83
		f 4 160 -41 -51 -58
		mu 0 4 59 107 60 61
		f 4 -62 161 162 -56
		mu 0 4 62 63 64 115
		f 4 163 22 164 -134
		mu 0 4 65 114 66 90
		f 4 165 -112 -100 -98
		mu 0 4 67 112 26 23
		f 4 166 21 167 -131
		mu 0 4 68 87 74 73
		f 4 168 169 -45 -43
		mu 0 4 69 70 86 10
		f 4 170 -153 -81 -91
		mu 0 4 71 103 52 72
		f 4 -168 171 -96 -106
		mu 0 4 73 74 109 75
		f 4 -156 -104 -102 -110
		mu 0 4 76 55 25 77
		f 4 172 -164 -108 -114
		mu 0 4 78 114 65 79
		f 4 -158 173 -122 -128
		mu 0 4 80 81 98 82
		f 4 -160 -126 -85 -83
		mu 0 4 83 58 33 84
		f 4 16 -145 -39 -154
		mu 0 4 53 41 40 85
		f 4 -170 20 -167 -52
		mu 0 4 86 70 87 68
		f 4 -147 -54 -155 -63
		mu 0 4 88 42 89 54
		f 4 23 -162 -65 -165
		mu 0 4 66 64 63 90
		f 4 -149 13 -157 -74
		mu 0 4 91 44 92 56
		f 4 -150 -76 -159 -37
		mu 0 4 93 46 94 57
		f 4 -152 -79 174 -140
		mu 0 4 95 51 96 106
		f 4 175 9 -148 -139
		mu 0 4 97 116 44 43
		f 4 -174 -7 176 -142
		mu 0 4 98 81 99 113
		f 4 -151 -141 177 -92
		mu 0 4 100 48 101 111
		f 4 178 -11 -171 -94
		mu 0 4 102 110 103 71
		f 4 -146 12 179 -77
		mu 0 4 104 41 105 108
		f 4 -175 -138 -161 -117
		mu 0 4 106 96 107 59
		f 4 -180 8 -169 -137
		mu 0 4 108 105 70 69
		f 4 -172 -6 -179 -144
		mu 0 4 109 74 110 102
		f 4 -178 -118 -166 -143
		mu 0 4 111 101 112 67
		f 4 -177 -18 -173 -120
		mu 0 4 113 99 114 78
		f 4 -163 19 -176 -115
		mu 0 4 115 64 116 97
		f 3 -29 -33 -25
		mu 0 3 5 7 50
		f 3 -44 -48 -40
		mu 0 3 10 11 117
		f 3 -50 -59 -55
		mu 0 3 12 15 62
		f 3 -70 -27 -66
		mu 0 3 18 45 16
		f 3 -84 -88 -80
		mu 0 3 21 22 72
		f 3 -99 -103 -95
		mu 0 3 23 118 75
		f 3 -101 -111 -107
		mu 0 3 24 26 119
		f 3 -86 -125 -121
		mu 0 3 47 120 31;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "round_pillow_large" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|pillow_grp";
	rename -uid "90E78DAD-4829-27B3-7F32-B08AD624C607";
	setAttr ".t" -type "double3" 26.807458903547587 19.95683538784542 0.9277314058825965 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 4.3667002560107697 11.050258026386494 4.1054303840095558 ;
createNode mesh -n "round_pillow_largeShape" -p "round_pillow_large";
	rename -uid "1CCC8F66-4BC8-C2A2-296B-229854AED8CA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.50000005960464478 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pillow1" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|pillow_grp";
	rename -uid "623DF204-490C-EBE7-886B-1385C63E5C5B";
	setAttr ".t" -type "double3" 14.765206907123979 24.14402516054778 -7.8412793225459803 ;
	setAttr ".r" -type "double3" -15.676919138699894 -22.089364145663538 0.41037852069626468 ;
	setAttr ".s" -type "double3" 17.134390007606434 12.590375354028625 5.9126764269775949 ;
createNode mesh -n "pillowShape1" -p "pillow1";
	rename -uid "FA248D74-4071-D732-C647-F292EB2269A9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000001490116119 0.37500001490116119 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  0 0 -0.07847707 0 0 0.07847707 
		0 0 0.067240857 0 0 -0.0672407 0 0 0.067240857 0 0 -0.0672407 0 0 -0.062606692 0 
		0 -0.0672407 0 0 -0.062606692 0 0 0.062606692 0 0 0.067240857 0 0 0.062606692 0 0 
		0.062606692 0 0 0.067240857 0 0 0.062606692 0 0 -0.062606692 0 0 -0.0672407 0 0 -0.062606692;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "baseboard" -p "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp";
	rename -uid "1F4F20EC-42E0-16BE-DA78-7FAD4F1309A4";
	setAttr ".s" -type "double3" 1 1 1.0197103360719595 ;
	setAttr ".rp" -type "double3" 0 8.2204584973945938 -14.027793207328823 ;
	setAttr ".sp" -type "double3" 0 8.2204584973945938 -14.027793207328926 ;
createNode mesh -n "baseboardShape" -p "baseboard";
	rename -uid "7B5D2F15-489A-0F75-1DC0-9AB16C4AC6FA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.69492623209953308 0.24851855635643005 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "daybed_003_left";
	rename -uid "F524E740-4183-DABE-CE71-3FA41807F4F8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1000.1 8.3674818241903512 3.4500991407799084 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
createNode camera -n "daybed_003_leftShape" -p "daybed_003_left";
	rename -uid "A0B5524D-434F-AB72-4328-D3B09E53EF2B";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "back";
	rename -uid "5D3366DA-4EBE-3155-4294-75B46E7D6DF3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 -1000.1 ;
	setAttr ".r" -type "double3" 0 180 0 ;
createNode camera -n "backShape" -p "back";
	rename -uid "8CEAF11E-44B5-30C5-802E-15931978CEC2";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "back1";
	setAttr ".den" -type "string" "back1_depth";
	setAttr ".man" -type "string" "back1_mask";
	setAttr ".hc" -type "string" "viewSet -b %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube1";
	rename -uid "CC6A680C-4776-F894-E2B3-23ACB81C4EFB";
	setAttr ".rp" -type "double3" 0 36.411197207638899 -13.191629619340668 ;
	setAttr ".sp" -type "double3" 0 36.411197207638899 -13.191629619340668 ;
createNode transform -n "transform1" -p "pCube1";
	rename -uid "09DC5FE5-4E17-A76C-0CF8-748E89F3A0CC";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform1";
	rename -uid "B5C75CC8-4A62-F1E7-D85F-EEAF87EA1058";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:165]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 9 "f[2:3]" "f[19:20]" "f[44:45]" "f[49:50]" "f[76]" "f[94:96]" "f[121]" "f[123]" "f[141:144]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 17 "f[1]" "f[6]" "f[10:11]" "f[14:15]" "f[17:18]" "f[22]" "f[37:43]" "f[47]" "f[55]" "f[61]" "f[75]" "f[82:83]" "f[85]" "f[87]" "f[91]" "f[107]" "f[115:119]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 21 "f[4:5]" "f[7:9]" "f[12:13]" "f[16]" "f[21]" "f[23:36]" "f[46]" "f[48]" "f[51:54]" "f[56:60]" "f[62:74]" "f[77:81]" "f[86]" "f[88:90]" "f[92:93]" "f[97:106]" "f[108:114]" "f[122]" "f[124:128]" "f[130:140]" "f[145:165]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 4 "f[0]" "f[84]" "f[120]" "f[129]";
	setAttr ".pv" -type "double2" 0.81725442409515381 0.078125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 341 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.625 0.99030656 0.63430965
		 0 0.625 0.0079555809 0.625 0.054343313 0.63449723 0.0625 0.60863948 0.06454809 0.5858705
		 0.24844559 0.63494098 0.24873993 0.625 0.12720175 0.61638188 0.12616007 0.63450885
		 0.15625 0.61719072 0.15781589 0.625 0.98995543 0.59495431 1 0.63478547 0.125 0.625
		 0.14567295 0.63478547 0.13416214 0.63498092 0.14965986 0.6351037 0.15515491 0.63502342
		 0.002043966 0.625 0.050235718 0.63499635 0.0055155288 0.63427281 0.049165845 0.63043666
		 0.058120996 0.5 0.25984952 0.63484949 0.10937474 0.63457477 0.12247326 0.5 0.12607265
		 0.625 0.99015051 0.5 1 0.63484955 0.0625 0.625 0.0069961934 0.5 0.15736194 0.63484949
		 0.20312466 0.63484949 0.15625 0.625 0.12727627 0.5 0.064662173 0.63484949 0.078124993
		 0.6348502 0.125 0.63484949 0.15625 0.62499994 0.15411265 0.625 0.12761638 0.63484949
		 0.125 0.625 0.15388547 0.625 0.12784693 0.50000161 1 0.63484955 0 0.63484955 0.0625
		 0.625 0.054320712 0.625 0.0073226504 0.63484949 0 0.625 0.05226836 0.625 0.010000308
		 0.5 0.058048066 0.5 0 0.60975283 0.057673845 0.61414421 0.079872899 0.61414421 0.10835759
		 0.5 0.078125 0.5 0.12285385 0.5 0.109375 0.61288255 0.12294819 0.61793804 0.15507942
		 0.61607099 0.20275837 0.5 0.203125 0.61404693 0.21851015 0.5 0.2483993 0.5 0.21875
		 0.57285893 0.25994098 0.50000191 0.99015051 0.58364719 0.990053 0.60416532 1 0.60861856
		 1 0.5 1 0.63484949 0.125 0.625 0.15417421 0.625 0.12718 0.63484955 0.15625 0.63441753
		 0.064769991 0.62662035 0.15763547 0.63432097 0.125 0.625 0.15407537 0.60861456 0
		 0.5 0.25 0.61981398 1 0.61981273 0 0.63444579 0 0.625 0.99055421 0.62681496 0.0061105671
		 0.62054348 0.0091053983 0.62674046 0.056035422 0.62970001 0.06307216 0.61664486 0.067363873
		 0.59772676 0.061285894 0.62093151 0.056145545 0.56672198 0.25036284 0.60759926 0.24683756
		 0.625 0.2599684 0.6349684 0.25 0.62129474 0.12015017 0.63440347 0.12351692 0.6267454
		 0.12674747 0.6218425 0.12649637 0.61003226 0.12444412 0.62675732 0.15459047 0.63417661
		 0.15974049 0.5 0.125 0.625 0.078125 0.5 0.0625 0.625 1 0.57825834 0.99839783 0.61189985
		 0.98996753 0.61249161 0.9978261 0.5 1 0.625 0 0.625 0.0625 0.63510948 0.21880291
		 0.625 0.203125 0.625 0.125 0.625 0.15625 0.625 0.109375 0.625 0.125 0.625 0.125 0.625
		 0.15625 0.625 0.15625 0.62688059 0.13546424 0.62730813 0.12713899 0.63472325 0.12978108
		 0.62660116 0.14900215 0.62617099 0.15406647 0.625 0 0.625 0.0625 0.62657166 0.012641458
		 0.62698138 0.0074202842 0.62670559 0.054311644 0.62936586 0.0577057 0.625 0 0.625
		 1 0.625 0.25 0.625 0.125 0.625 1 0.625 0.21875 0.625 0.125 0.625 0 0.63483679 0.13823341
		 0.62680715 0.13902065 0.625 0.13252984 0.625 0.13451728 0.625 0.13425523 0.625 0.13434239
		 0.625 0.13426147 0.62196219 0.13379239 0.61679065 0.13375722 0.5 0.1341818 0.5 0.063581087
		 0.60318315 0.062916994 0.62761557 0.064569548 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0;
	setAttr ".uvst[0].uvsp[250:340]" 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 193 ".vt";
	setAttr ".vt[0:165]"  0 33.59151459 -12.69336224 0 34.65673828 -12.69024277
		 0 35.1015892 -12.69024277 0 37.61158752 -12.69336224 0 38.77528381 -12.69336224 0 32.95821762 -12.69336224
		 0 39.95692444 -12.69336224 0 40.03200531 -12.72007179 0 40.03200531 -12.73276043
		 0.44521621 33.51446152 -12.72007179 0.44521621 33.51451111 -12.73213577 0.46012953 33.59151459 -12.76728821
		 0.5251345 33.55181122 -12.73060131 0.5179131 33.55622864 -12.72007179 0.495013 33.64230347 -12.69336224
		 0.44419986 33.67302704 -12.72950554 0.39983001 33.59147263 -12.69336224 0.39546958 34.28607941 -12.72007179
		 0.40724596 34.31156921 -12.72990036 0.33631822 34.32092667 -12.73047638 0.49061492 34.36193085 -12.73103237
		 0.48636115 34.3592186 -12.72007179 0.36236796 34.35732269 -12.69336224 0.26297507 34.32092667 -12.72950554
		 0.29529253 34.27202606 -12.69336224 0.34923014 34.25489807 -12.72950554 0.4043473 34.20772934 -12.69336224
		 0.070933953 40.020717621 -12.72007179 0.091286831 39.95877075 -12.69336224 0.15685023 39.96533966 -12.69336224
		 0.15461667 39.97050095 -12.73312664 0.13290232 39.99663544 -12.73323536 0.077465184 40.020717621 -12.73312664
		 0.5744921 35.37215042 -12.72007179 0.57761031 35.36927795 -12.73166084 0.54511714 35.43628693 -12.7309761
		 0.59581751 35.47023773 -12.73064613 0.59897333 35.47238159 -12.72007179 0.62854272 35.53083801 -12.69336224
		 0.56523168 35.47076035 -12.70714092 0.50336057 35.40065002 -12.69336224 0.48347512 35.40000916 -12.72950554
		 0.9835763 36.73796082 -12.72007179 0.99406648 36.76429367 -12.73139763 0.91888499 36.70302582 -12.73993683
		 0.93620884 36.72708511 -12.72677231 0.87514263 36.70144272 -12.69336224 0.97065812 36.64940262 -12.69336224
		 0 35.39906311 -12.69336224 0 35.40000916 -12.72950554 0.74611133 34.63708496 -12.73276043
		 0.74611133 34.63708496 -12.72007179 0.68132597 34.66417694 -12.68430996 0 34.2758255 -12.69336224
		 0 34.32092667 -12.72950554 -6.2740087e-06 34.35969543 -12.69336224 0.37265733 32.95821762 -12.73276043
		 0.37265733 32.95821762 -12.72007179 0.31054461 32.97812271 -12.69336224 0.049790539 32.52607346 -12.72007179
		 0.053216148 32.52607346 -12.73315048 0.079516791 32.5398674 -12.7334919 0.12908775 32.57535172 -12.73354053
		 0.13096993 32.58108902 -12.72007179 0.085834712 32.587883 -12.69336224 1.8822026e-05 32.51260757 -12.73276043
		 1.8822026e-05 32.51260757 -12.72007179 1.8822026e-05 32.58986664 -12.69336224 0.98333162 33.34693146 -12.69336224
		 0.96705687 33.2817955 -12.72007179 0.96705687 33.2817955 -12.73276043 0.96705687 33.85316849 -12.73276043
		 0.96705687 33.85316849 -12.72007179 0.9474631 33.79035568 -12.69336224 0.671413 38.77528381 -12.73379993
		 0.61258161 38.75743484 -12.69336224 0.90584761 37.61158752 -12.73276043 0.90584761 37.61158752 -12.72007179
		 0.84114379 37.60486603 -12.69336224 0 36.74917984 -12.69336224 1.58643198 36.040988922 -12.73276043
		 1.58643198 36.040988922 -12.72007179 1.55792272 36.062789917 -12.69336224 1.63387585 37.048316956 -12.69336224
		 1.61056793 37.1084938 -12.72007179 1.61056793 37.1084938 -12.73276043 0.74611133 35.12124252 -12.73276043
		 0.74611133 35.12124252 -12.72007179 0.68132597 35.10547638 -12.68430996 2.34963179 36.12643433 -12.73276043
		 2.34963179 36.12643433 -12.72007179 2.34896064 36.15680313 -12.69336224 2.90837622 35.90962601 -12.69336224
		 2.87209368 35.88949203 -12.72007179 2.87209368 35.88949203 -12.73276043 2.54264712 37.12960815 -12.69336224
		 2.54876423 37.19647217 -12.72007179 2.54876423 37.19647217 -12.73276043 3.24782181 36.65264893 -12.73276043
		 3.24782181 36.65264893 -12.72007179 3.18993163 36.60686111 -12.69336224 3.1914947 35.53639603 -12.72007179
		 3.14324117 35.58275986 -12.69336224 3.10066581 35.56794357 -12.72007179 3.10000706 35.56861496 -12.73250389
		 3.14976621 35.53636932 -12.73225498 3.19263029 35.53788376 -12.73250389 3.29052496 35.80698395 -12.72007179
		 3.2930882 35.80698395 -12.7332859 3.33281803 36.10623932 -12.73377705 3.33281803 36.10623932 -12.72007179
		 3.26048923 35.87325668 -12.69336224 1.37137282 33.37491989 -12.69336224 1.39864612 33.31293106 -12.72007179
		 1.39864612 33.31293106 -12.73276043 1.39864612 33.8290329 -12.73276043 1.39864612 33.8290329 -12.72007179
		 1.42004657 33.7639389 -12.69336224 1.7265507 33.70728302 -12.72007179 1.65946901 33.66613007 -12.69336224
		 1.66485214 33.58854294 -12.72007179 1.68561602 33.61952591 -12.73345566 1.72311258 33.68399048 -12.73334789
		 1.82012129 34.066856384 -12.72007179 1.81971347 34.064113617 -12.73045349 1.83724976 34.18022156 -12.72555065
		 1.75006568 34.11040878 -12.72545528 1.73603094 34.0053024292 -12.69336224 0.46012953 33.59151459 -12.75621414
		 0.13290232 39.99663544 -12.72007179 0.52899921 35.40000916 -12.72950554 0.079516791 32.5398674 -12.72007179
		 0.671413 38.77528381 -12.72007179 3.14976621 35.53636932 -12.72007179 1.69068849 33.63597107 -12.72007179
		 3.27544093 35.74458313 -12.73270988 3.27544093 35.74458313 -12.72007179 3.23154783 35.79618073 -12.69336224
		 2.96785378 36.18877029 -12.69336224 2.355335 36.41127014 -12.69336224 1.57786787 36.33632278 -12.69336224
		 0.8383534 36.053031921 -12.69336224 0.76228106 36.0054855347 -12.72950554 0.69223171 35.99369812 -12.69336224
		 0 35.81474304 -12.69336224 -1.2548017e-05 34.33067322 -12.71143913 0.31068262 34.32948685 -12.71143913
		 0 40.03200531 -13.66417885 0.077465184 40.020717621 -13.66454506 0.37265733 32.95821762 -13.66417885
		 0.44521621 33.51451111 -13.66355515 0.5251345 33.55181122 -13.66201973 0.46012953 33.59151459 -13.69870758
		 0.96705687 33.2817955 -13.66417885 0.96705687 33.85316849 -13.66417885 0.40682244 34.30685043 -13.66259575
		 0.49061492 34.36193085 -13.66245174 0.33631822 34.32092667 -13.66238117 0.74611133 34.63708496 -13.66417885
		 0.13290232 39.99663544 -13.66465473 0.15461667 39.97050095 -13.66454506 0.671413 38.77528381 -13.66521931
		 0.54511714 35.43628693 -13.66239548 0.57761031 35.36927795 -13.66308022 0.59581751 35.47023773 -13.66206551
		 0.74611133 35.12124252 -13.66417885;
	setAttr ".vt[166:192]" 1.58643198 36.040988922 -13.66417885 1.61056793 37.1084938 -13.66417885
		 0.99406648 36.74362946 -13.662817 0.93815875 36.76512527 -13.66078091 0.91888499 36.70302582 -13.6713562
		 0.90584761 37.61158752 -13.66417885 0.12908775 32.57535172 -13.66495991 0.079516791 32.5398674 -13.66491127
		 0.053216148 32.52607346 -13.66456985 1.8822026e-05 32.51260757 -13.66417885 1.39864612 33.31293106 -13.66417885
		 1.39864612 33.8290329 -13.66417885 2.34963179 36.12643433 -13.66417885 2.54876423 37.19647217 -13.66417885
		 2.87209368 35.88949203 -13.66417885 3.10000706 35.56861496 -13.66392326 3.24782181 36.65264893 -13.66417885
		 3.33281803 36.10623932 -13.66519547 3.14976621 35.53636932 -13.66367435 3.19263029 35.53788376 -13.66392326
		 3.27877235 35.74886703 -13.6641283 3.29052496 35.80698395 -13.66470528 1.68561602 33.61952591 -13.66487503
		 1.75050509 34.11076355 -13.65956593 1.72311258 33.68399048 -13.66476631 1.81971347 34.064113617 -13.66187286
		 1.83724952 34.18022156 -13.65970802;
	setAttr -s 356 ".ed";
	setAttr ".ed[0:165]"  1 2 0 3 4 0 5 0 0 8 7 0 7 27 0 27 32 1 32 8 0 7 6 0
		 6 28 1 28 27 1 10 9 1 9 57 0 57 56 1 56 10 0 9 16 1 16 58 1 58 57 1 12 11 0 11 10 0
		 14 13 1 13 69 0 69 68 1 68 14 1 13 12 1 12 70 0 70 69 0 16 15 1 15 25 1 25 24 1 24 16 1
		 15 14 1 14 26 1 26 25 1 18 17 1 17 72 0 72 71 1 71 18 0 17 26 1 26 73 1 73 72 1 20 18 0
		 19 18 0 22 21 1 52 22 1 21 20 1 20 50 0 24 23 1 23 54 1 54 53 0 53 24 1 23 146 1
		 22 55 1 55 145 0 30 29 1 29 28 1 28 75 1 32 31 0 31 30 0 30 74 0 35 34 0 36 35 0
		 34 33 1 33 87 0 87 86 0 86 34 0 33 40 1 40 88 1 88 87 1 38 37 1 82 38 1 37 36 1 36 80 0
		 40 39 1 39 142 1 46 143 1 39 38 1 38 141 1 47 45 1 41 49 1 49 48 0 48 40 1 41 40 1
		 43 42 1 85 43 0 42 47 1 47 83 1 45 44 0 44 43 0 46 45 1 78 46 1 45 76 0 52 51 1 88 52 1
		 51 50 0 50 86 0 63 62 1 62 56 0 58 64 1 64 63 1 61 60 0 62 61 0 60 59 1 59 66 0 66 65 0
		 65 60 0 59 64 1 64 67 1 67 66 0 113 112 1 112 68 1 70 114 0 114 113 0 116 115 0 115 71 0
		 73 117 1 117 116 1 77 76 1 76 74 0 75 78 1 78 77 1 82 81 1 91 82 1 81 80 1 80 89 0
		 85 84 1 97 85 0 84 83 1 83 95 1 91 90 1 90 93 0 93 92 1 92 91 1 90 89 0 89 94 0 94 93 0
		 103 102 1 102 92 1 94 104 0 104 103 1 97 96 0 96 99 0 99 98 0 98 97 0 96 95 1 95 100 1
		 100 99 1 110 109 1 109 98 0 100 111 1 111 110 1 105 104 0 106 105 0 102 101 1 101 136 0
		 107 111 1 111 137 1 101 106 1 106 135 0 108 107 1 109 108 0 120 119 1 119 112 1 114 121 0
		 121 120 1 126 115 0 117 127 1;
	setAttr ".ed[166:331]" 127 126 1 122 121 0 119 118 1 118 123 0 123 127 1 127 119 1
		 118 122 1 122 124 0 124 123 1 125 124 0 126 125 0 0 53 0 4 6 0 2 48 0 48 144 0 68 73 1
		 82 140 1 55 1 0 95 139 1 92 138 1 67 5 0 112 117 1 0 16 1 52 1 1 88 2 1 78 3 1 75 4 1
		 58 5 1 21 51 0 57 63 0 69 113 0 72 116 0 45 77 0 37 81 0 42 84 0 51 87 0 81 90 0
		 93 103 0 84 96 0 99 110 0 113 120 0 116 126 0 9 128 0 128 15 1 11 128 0 13 128 0
		 17 19 0 19 25 1 21 18 0 23 19 1 27 129 0 129 31 0 29 129 0 33 130 0 130 41 1 35 130 0
		 37 130 0 39 130 1 42 45 0 59 131 0 131 63 0 61 131 0 77 132 0 132 29 0 74 132 1 101 133 0
		 133 105 0 103 133 0 107 110 0 118 134 0 134 121 0 120 134 0 123 125 0 132 75 1 135 108 0
		 136 107 0 137 102 1 138 100 1 139 91 1 140 83 1 141 47 1 142 45 1 143 40 1 144 79 0
		 135 136 1 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 142 1 142 143 1 143 144 1
		 145 54 0 146 22 1 145 146 1 146 18 1 79 3 0 46 79 1 8 147 0 32 148 0 148 147 0 56 149 1
		 10 150 0 149 150 0 12 151 0 11 152 0 151 152 0 152 150 0 70 153 0 151 153 0 71 154 1
		 18 155 0 154 155 0 20 156 0 156 155 0 19 157 0 157 155 0 50 158 0 156 158 0 31 159 1
		 148 159 0 30 160 1 159 160 0 74 161 0 160 161 0 35 162 0 34 163 0 162 163 0 36 164 0
		 164 162 0 86 165 0 165 163 0 80 166 1 164 166 0 85 167 1 43 168 0 167 168 0 45 169 0
		 44 170 0 169 170 0 170 168 0 76 171 1 169 171 0 158 165 0 62 172 0 172 149 0 61 173 1
		 60 174 0 173 174 0 172 173 0 65 175 0 175 174 0 114 176 0 153 176 0 115 177 0 177 154 0
		 171 161 0 89 178 0 166 178 0 97 179 0 179 167 0 94 180 0 178 180 0 104 181 0 180 181 0;
	setAttr ".ed[332:355]" 98 182 0 182 179 0 109 183 0 183 182 0 105 184 0 184 181 0
		 106 185 0 185 184 0 135 186 1 185 186 0 108 187 0 183 187 0 121 188 0 176 188 0 126 189 0
		 189 177 0 122 190 0 190 188 0 124 191 0 190 191 0 125 192 0 192 191 0 189 192 0 186 187 0;
	setAttr -s 166 -ch 656 ".fc[0:165]" -type "polyFaces" 
		f 4 3 4 5 6
		mu 0 4 24 83 95 68
		f 4 7 8 9 -5
		mu 0 4 83 66 6 95
		f 4 10 11 12 13
		mu 0 4 0 84 109 28
		f 4 14 15 16 -12
		mu 0 4 84 72 71 109
		f 4 19 20 21 22
		mu 0 4 2 88 114 31
		f 4 23 24 25 -21
		mu 0 4 88 1 46 114
		f 4 26 27 28 29
		mu 0 4 82 89 94 55
		f 4 30 31 32 -28
		mu 0 4 89 2 3 94
		f 4 33 34 35 36
		mu 0 4 4 90 115 30
		f 4 37 38 39 -35
		mu 0 4 90 3 48 115
		f 4 46 47 48 49
		mu 0 4 55 93 108 53
		f 4 261 260 51 52
		mu 0 4 154 155 5 36
		f 4 53 -230 -231 -59
		mu 0 4 7 96 141 116
		f 4 61 62 63 64
		mu 0 4 26 99 120 25
		f 4 65 66 67 -63
		mu 0 4 99 61 57 120
		f 4 72 73 257 248
		mu 0 4 9 102 151 152
		f 4 75 76 256 -74
		mu 0 4 102 8 150 151
		f 4 -82 78 79 80
		mu 0 4 61 103 106 59
		f 4 81 -81 -80 -79
		mu 0 4 103 9 27 106
		f 4 101 102 103 104
		mu 0 4 70 110 113 69
		f 4 105 106 107 -103
		mu 0 4 110 13 45 113
		f 4 116 117 230 -229
		mu 0 4 117 33 116 141
		f 4 118 119 228 239
		mu 0 4 65 63 117 141
		f 4 128 129 130 131
		mu 0 4 76 121 122 41
		f 4 132 133 134 -130
		mu 0 4 121 38 42 122
		f 4 139 140 141 142
		mu 0 4 77 123 124 39
		f 4 143 144 145 -141
		mu 0 4 123 40 43 124
		f 4 152 153 251 242
		mu 0 4 44 125 145 146
		f 4 156 157 250 -154
		mu 0 4 125 16 144 145
		f 4 168 169 170 171
		mu 0 4 52 132 134 20
		f 4 172 173 174 -170
		mu 0 4 132 21 22 134
		f 4 -32 -23 181 -39
		mu 0 4 3 2 31 48
		f 4 255 -77 -70 182
		mu 0 4 149 150 8 35
		f 4 253 244 -132 185
		mu 0 4 147 148 76 41
		f 4 252 -186 -137 -243
		mu 0 4 146 147 41 44
		f 4 -182 -110 187 -115
		mu 0 4 48 31 49 51
		f 4 -188 -162 -172 -166
		mu 0 4 51 49 52 20
		f 4 -178 188 -30 -50
		mu 0 4 53 54 82 55
		f 4 -184 -52 -44 189
		mu 0 4 58 36 5 56
		f 4 190 -1 -190 -93
		mu 0 4 57 60 58 56
		f 4 -180 -191 -67 -81
		mu 0 4 59 60 57 61
		f 4 258 -181 80 -249
		mu 0 4 152 153 27 9
		f 4 -2 -192 -119 192
		mu 0 4 67 64 63 65
		f 4 -179 -193 -56 -9
		mu 0 4 66 67 65 6
		f 4 193 -187 -107 -98
		mu 0 4 71 73 45 13
		f 4 -189 -3 -194 -16
		mu 0 4 72 29 73 71
		f 4 254 -183 -122 -245
		mu 0 4 148 149 35 76
		f 4 42 194 -92 43
		mu 0 4 5 92 107 56
		f 4 44 45 -94 -195
		mu 0 4 92 78 37 107
		f 4 -13 195 95 96
		mu 0 4 28 109 112 12
		f 4 -17 97 98 -196
		mu 0 4 109 71 13 112
		f 4 -22 196 108 109
		mu 0 4 31 114 130 49
		f 4 -26 110 111 -197
		mu 0 4 114 46 50 130
		f 4 -36 197 112 113
		mu 0 4 30 115 131 47
		f 4 -40 114 115 -198
		mu 0 4 115 48 51 131
		f 4 88 198 -120 89
		mu 0 4 11 79 117 63
		f 3 90 -117 -199
		mu 0 3 79 33 117
		f 4 68 199 -121 69
		mu 0 4 8 101 118 35
		f 4 70 71 -123 -200
		mu 0 4 101 80 74 118
		f 4 82 200 -125 83
		mu 0 4 10 104 119 34
		f 4 84 85 -127 -201
		mu 0 4 104 81 75 119
		f 4 91 201 -68 92
		mu 0 4 56 107 120 57
		f 4 93 94 -64 -202
		mu 0 4 107 37 25 120
		f 4 120 202 -129 121
		mu 0 4 35 118 121 76
		f 4 122 123 -133 -203
		mu 0 4 118 74 38 121
		f 4 -131 203 135 136
		mu 0 4 41 122 126 44
		f 4 -135 137 138 -204
		mu 0 4 122 42 14 126
		f 4 124 204 -140 125
		mu 0 4 34 119 123 77
		f 4 126 127 -144 -205
		mu 0 4 119 75 40 123
		f 4 -142 205 146 147
		mu 0 4 39 124 129 18
		f 4 -146 148 149 -206
		mu 0 4 124 43 15 129
		f 4 -109 206 160 161
		mu 0 4 49 130 133 52
		f 4 -112 162 163 -207
		mu 0 4 130 50 19 133
		f 3 -113 207 164
		mu 0 3 47 131 23
		f 4 -116 165 166 -208
		mu 0 4 131 51 20 23
		f 4 -27 -15 208 209
		mu 0 4 89 82 85 136
		f 4 -11 -19 210 -209
		mu 0 4 84 0 87 137
		f 4 -18 -24 211 -211
		mu 0 4 86 1 88 136
		f 4 -20 -31 -210 -212
		mu 0 4 88 2 89 136
		f 4 -33 -38 212 213
		mu 0 4 94 3 90 91
		f 3 -34 -42 -213
		mu 0 3 90 4 91
		f 3 -41 -45 214
		mu 0 3 156 78 92
		f 4 -43 -261 262 -215
		mu 0 4 92 5 155 156
		f 4 -47 -29 -214 -216
		mu 0 4 93 55 94 91
		f 4 -57 -6 216 217
		mu 0 4 97 68 95 138
		f 4 -10 -55 218 -217
		mu 0 4 95 6 96 138
		f 4 -54 -58 -218 -219
		mu 0 4 96 7 98 138
		f 4 81 -66 219 220
		mu 0 4 103 61 99 139
		f 4 -62 -60 221 -220
		mu 0 4 99 26 100 139
		f 4 -61 -71 222 -222
		mu 0 4 100 80 101 139
		f 4 -69 -76 223 -223
		mu 0 4 101 8 102 139
		f 4 -73 -82 -221 -224
		mu 0 4 102 9 103 139
		f 3 -78 -85 224
		mu 0 3 79 81 104
		f 4 -83 -88 -87 -225
		mu 0 4 104 10 105 79
		f 4 -99 -106 225 226
		mu 0 4 112 13 110 140
		f 4 -102 -100 227 -226
		mu 0 4 110 70 111 140
		f 4 -101 -96 -227 -228
		mu 0 4 111 12 112 140
		f 4 -152 -157 231 232
		mu 0 4 127 16 125 142
		f 4 -153 -136 233 -232
		mu 0 4 125 44 126 142
		f 4 -139 -151 -233 -234
		mu 0 4 126 14 127 142
		f 3 -150 -155 234
		mu 0 3 129 15 128
		f 4 -159 -160 -147 -235
		mu 0 4 128 17 18 129
		f 4 -168 -173 235 236
		mu 0 4 19 21 132 143
		f 4 -169 -161 237 -236
		mu 0 4 132 52 133 143
		f 3 -164 -237 -238
		mu 0 3 133 19 143
		f 4 -167 -171 238 -177
		mu 0 4 23 20 134 135
		f 3 -175 -176 -239
		mu 0 3 134 22 135
		f 4 -240 229 54 55
		mu 0 4 65 141 96 6
		f 4 -251 240 158 -242
		mu 0 4 145 144 17 128
		f 4 -252 241 154 155
		mu 0 4 146 145 128 15
		f 4 -244 -253 -156 -149
		mu 0 4 43 147 146 15
		f 4 184 -254 243 -145
		mu 0 4 40 148 147 43
		f 4 -246 -255 -185 -128
		mu 0 4 75 149 148 40
		f 4 -247 -256 245 -86
		mu 0 4 81 150 149 75
		f 4 -257 246 77 -248
		mu 0 4 151 150 81 79
		f 4 -258 247 -89 74
		mu 0 4 152 151 79 62
		f 4 50 -262 259 -48
		mu 0 4 93 155 154 108
		f 4 -263 -51 215 41
		mu 0 4 156 155 93 91
		f 4 191 -264 -265 -90
		mu 0 4 63 64 32 11
		f 4 264 -250 -259 -75
		mu 0 4 62 32 153 152
		f 4 266 267 -266 -7
		mu 0 4 157 160 159 158
		f 4 268 270 -270 -14
		mu 0 4 161 164 163 162
		f 4 17 272 -274 -272
		mu 0 4 165 166 167 168
		f 4 18 269 -275 -273
		mu 0 4 169 170 171 172
		f 4 271 276 -276 -25
		mu 0 4 173 176 175 174
		f 4 277 279 -279 -37
		mu 0 4 177 180 179 178
		f 4 40 278 -282 -281
		mu 0 4 181 182 183 184
		f 4 41 278 -284 -283
		mu 0 4 185 186 187 188
		f 4 280 285 -285 -46
		mu 0 4 189 192 191 190
		f 4 56 286 -288 -267
		mu 0 4 193 194 195 196
		f 4 57 288 -290 -287
		mu 0 4 197 198 199 200
		f 4 58 290 -292 -289
		mu 0 4 201 202 203 204
		f 4 59 293 -295 -293
		mu 0 4 205 206 207 208
		f 4 60 292 -297 -296
		mu 0 4 209 210 211 212
		f 4 297 298 -294 -65
		mu 0 4 213 216 215 214
		f 4 295 300 -300 -72
		mu 0 4 217 220 219 218
		f 4 301 303 -303 -84
		mu 0 4 221 224 223 222
		f 4 86 305 -307 -305
		mu 0 4 225 226 227 228
		f 4 87 302 -308 -306
		mu 0 4 229 230 231 232
		f 4 304 309 -309 -91
		mu 0 4 233 236 235 234
		f 4 284 310 -298 -95
		mu 0 4 237 240 239 238
		f 4 311 312 -269 -97
		mu 0 4 241 244 243 242
		f 4 99 314 -316 -314
		mu 0 4 245 246 247 248
		f 4 100 313 -317 -312
		mu 0 4 249 250 251 252
		f 4 317 318 -315 -105
		mu 0 4 253 256 255 254
		f 4 275 320 -320 -111
		mu 0 4 257 260 259 258
		f 4 321 322 -278 -114
		mu 0 4 261 264 263 262
		f 4 308 323 -291 -118
		mu 0 4 265 268 267 266
		f 4 299 325 -325 -124
		mu 0 4 269 272 271 270
		f 4 326 327 -302 -126
		mu 0 4 273 276 275 274
		f 4 324 329 -329 -134
		mu 0 4 277 280 279 278
		f 4 328 331 -331 -138
		mu 0 4 281 284 283 282
		f 4 332 333 -327 -143
		mu 0 4 285 288 287 286
		f 4 334 335 -333 -148
		mu 0 4 289 292 291 290
		f 4 150 330 -338 -337
		mu 0 4 293 294 295 296
		f 4 151 336 -340 -339
		mu 0 4 297 298 299 300
		f 4 338 341 -341 -158
		mu 0 4 301 304 303 302
		f 4 159 342 -344 -335
		mu 0 4 305 306 307 308
		f 4 319 345 -345 -163
		mu 0 4 309 312 311 310
		f 4 346 347 -322 -165
		mu 0 4 313 316 315 314
		f 4 167 344 -350 -349
		mu 0 4 317 318 319 320
		f 4 348 351 -351 -174
		mu 0 4 321 324 323 322
		f 4 175 350 -354 -353
		mu 0 4 325 326 327 328
		f 4 176 352 -355 -347
		mu 0 4 329 330 331 332
		f 4 340 355 -343 -241
		mu 0 4 333 336 335 334
		f 4 -42 282 283 -279
		mu 0 4 337 338 339 340;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube2";
	rename -uid "E8FCE8A0-4AF4-3ACF-8EFC-22ADF8DCD33B";
	setAttr ".s" -type "double3" -1 1 1 ;
	setAttr ".rp" -type "double3" 0 36.411197207638899 -13.191629619340668 ;
	setAttr ".sp" -type "double3" 0 36.411197207638899 -13.191629619340668 ;
createNode transform -n "transform2" -p "pCube2";
	rename -uid "AD426D55-4653-223E-4D86-E3AC999F0942";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform2";
	rename -uid "9B495913-414D-E340-035B-AA996C471BE7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:165]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 9 "f[2:3]" "f[19:20]" "f[44:45]" "f[49:50]" "f[76]" "f[94:96]" "f[121]" "f[123]" "f[141:144]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 17 "f[1]" "f[6]" "f[10:11]" "f[14:15]" "f[17:18]" "f[22]" "f[37:43]" "f[47]" "f[55]" "f[61]" "f[75]" "f[82:83]" "f[85]" "f[87]" "f[91]" "f[107]" "f[115:119]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 21 "f[4:5]" "f[7:9]" "f[12:13]" "f[16]" "f[21]" "f[23:36]" "f[46]" "f[48]" "f[51:54]" "f[56:60]" "f[62:74]" "f[77:81]" "f[86]" "f[88:90]" "f[92:93]" "f[97:106]" "f[108:114]" "f[122]" "f[124:128]" "f[130:140]" "f[145:165]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 4 "f[0]" "f[84]" "f[120]" "f[129]";
	setAttr ".pv" -type "double2" 0.81725442409515381 0.078125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 341 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.625 0.99030656 0.63430965
		 0 0.625 0.0079555809 0.625 0.054343313 0.63449723 0.0625 0.60863948 0.06454809 0.5858705
		 0.24844559 0.63494098 0.24873993 0.625 0.12720175 0.61638188 0.12616007 0.63450885
		 0.15625 0.61719072 0.15781589 0.625 0.98995543 0.59495431 1 0.63478547 0.125 0.625
		 0.14567295 0.63478547 0.13416214 0.63498092 0.14965986 0.6351037 0.15515491 0.63502342
		 0.002043966 0.625 0.050235718 0.63499635 0.0055155288 0.63427281 0.049165845 0.63043666
		 0.058120996 0.5 0.25984952 0.63484949 0.10937474 0.63457477 0.12247326 0.5 0.12607265
		 0.625 0.99015051 0.5 1 0.63484955 0.0625 0.625 0.0069961934 0.5 0.15736194 0.63484949
		 0.20312466 0.63484949 0.15625 0.625 0.12727627 0.5 0.064662173 0.63484949 0.078124993
		 0.6348502 0.125 0.63484949 0.15625 0.62499994 0.15411265 0.625 0.12761638 0.63484949
		 0.125 0.625 0.15388547 0.625 0.12784693 0.50000161 1 0.63484955 0 0.63484955 0.0625
		 0.625 0.054320712 0.625 0.0073226504 0.63484949 0 0.625 0.05226836 0.625 0.010000308
		 0.5 0.058048066 0.5 0 0.60975283 0.057673845 0.61414421 0.079872899 0.61414421 0.10835759
		 0.5 0.078125 0.5 0.12285385 0.5 0.109375 0.61288255 0.12294819 0.61793804 0.15507942
		 0.61607099 0.20275837 0.5 0.203125 0.61404693 0.21851015 0.5 0.2483993 0.5 0.21875
		 0.57285893 0.25994098 0.50000191 0.99015051 0.58364719 0.990053 0.60416532 1 0.60861856
		 1 0.5 1 0.63484949 0.125 0.625 0.15417421 0.625 0.12718 0.63484955 0.15625 0.63441753
		 0.064769991 0.62662035 0.15763547 0.63432097 0.125 0.625 0.15407537 0.60861456 0
		 0.5 0.25 0.61981398 1 0.61981273 0 0.63444579 0 0.625 0.99055421 0.62681496 0.0061105671
		 0.62054348 0.0091053983 0.62674046 0.056035422 0.62970001 0.06307216 0.61664486 0.067363873
		 0.59772676 0.061285894 0.62093151 0.056145545 0.56672198 0.25036284 0.60759926 0.24683756
		 0.625 0.2599684 0.6349684 0.25 0.62129474 0.12015017 0.63440347 0.12351692 0.6267454
		 0.12674747 0.6218425 0.12649637 0.61003226 0.12444412 0.62675732 0.15459047 0.63417661
		 0.15974049 0.5 0.125 0.625 0.078125 0.5 0.0625 0.625 1 0.57825834 0.99839783 0.61189985
		 0.98996753 0.61249161 0.9978261 0.5 1 0.625 0 0.625 0.0625 0.63510948 0.21880291
		 0.625 0.203125 0.625 0.125 0.625 0.15625 0.625 0.109375 0.625 0.125 0.625 0.125 0.625
		 0.15625 0.625 0.15625 0.62688059 0.13546424 0.62730813 0.12713899 0.63472325 0.12978108
		 0.62660116 0.14900215 0.62617099 0.15406647 0.625 0 0.625 0.0625 0.62657166 0.012641458
		 0.62698138 0.0074202842 0.62670559 0.054311644 0.62936586 0.0577057 0.625 0 0.625
		 1 0.625 0.25 0.625 0.125 0.625 1 0.625 0.21875 0.625 0.125 0.625 0 0.63483679 0.13823341
		 0.62680715 0.13902065 0.625 0.13252984 0.625 0.13451728 0.625 0.13425523 0.625 0.13434239
		 0.625 0.13426147 0.62196219 0.13379239 0.61679065 0.13375722 0.5 0.1341818 0.5 0.063581087
		 0.60318315 0.062916994 0.62761557 0.064569548 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0;
	setAttr ".uvst[0].uvsp[250:340]" 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 193 ".vt";
	setAttr ".vt[0:165]"  0 33.59151459 -12.69336224 0 34.65673828 -12.69024277
		 0 35.1015892 -12.69024277 0 37.61158752 -12.69336224 0 38.77528381 -12.69336224 0 32.95821762 -12.69336224
		 0 39.95692444 -12.69336224 0 40.03200531 -12.72007179 0 40.03200531 -12.73276043
		 0.44521621 33.51446152 -12.72007179 0.44521621 33.51451111 -12.73213577 0.46012953 33.59151459 -12.76728821
		 0.5251345 33.55181122 -12.73060131 0.5179131 33.55622864 -12.72007179 0.495013 33.64230347 -12.69336224
		 0.44419986 33.67302704 -12.72950554 0.39983001 33.59147263 -12.69336224 0.39546958 34.28607941 -12.72007179
		 0.40724596 34.31156921 -12.72990036 0.33631822 34.32092667 -12.73047638 0.49061492 34.36193085 -12.73103237
		 0.48636115 34.3592186 -12.72007179 0.36236796 34.35732269 -12.69336224 0.26297507 34.32092667 -12.72950554
		 0.29529253 34.27202606 -12.69336224 0.34923014 34.25489807 -12.72950554 0.4043473 34.20772934 -12.69336224
		 0.070933953 40.020717621 -12.72007179 0.091286831 39.95877075 -12.69336224 0.15685023 39.96533966 -12.69336224
		 0.15461667 39.97050095 -12.73312664 0.13290232 39.99663544 -12.73323536 0.077465184 40.020717621 -12.73312664
		 0.5744921 35.37215042 -12.72007179 0.57761031 35.36927795 -12.73166084 0.54511714 35.43628693 -12.7309761
		 0.59581751 35.47023773 -12.73064613 0.59897333 35.47238159 -12.72007179 0.62854272 35.53083801 -12.69336224
		 0.56523168 35.47076035 -12.70714092 0.50336057 35.40065002 -12.69336224 0.48347512 35.40000916 -12.72950554
		 0.9835763 36.73796082 -12.72007179 0.99406648 36.76429367 -12.73139763 0.91888499 36.70302582 -12.73993683
		 0.93620884 36.72708511 -12.72677231 0.87514263 36.70144272 -12.69336224 0.97065812 36.64940262 -12.69336224
		 0 35.39906311 -12.69336224 0 35.40000916 -12.72950554 0.74611133 34.63708496 -12.73276043
		 0.74611133 34.63708496 -12.72007179 0.68132597 34.66417694 -12.68430996 0 34.2758255 -12.69336224
		 0 34.32092667 -12.72950554 -6.2740087e-06 34.35969543 -12.69336224 0.37265733 32.95821762 -12.73276043
		 0.37265733 32.95821762 -12.72007179 0.31054461 32.97812271 -12.69336224 0.049790539 32.52607346 -12.72007179
		 0.053216148 32.52607346 -12.73315048 0.079516791 32.5398674 -12.7334919 0.12908775 32.57535172 -12.73354053
		 0.13096993 32.58108902 -12.72007179 0.085834712 32.587883 -12.69336224 1.8822026e-05 32.51260757 -12.73276043
		 1.8822026e-05 32.51260757 -12.72007179 1.8822026e-05 32.58986664 -12.69336224 0.98333162 33.34693146 -12.69336224
		 0.96705687 33.2817955 -12.72007179 0.96705687 33.2817955 -12.73276043 0.96705687 33.85316849 -12.73276043
		 0.96705687 33.85316849 -12.72007179 0.9474631 33.79035568 -12.69336224 0.671413 38.77528381 -12.73379993
		 0.61258161 38.75743484 -12.69336224 0.90584761 37.61158752 -12.73276043 0.90584761 37.61158752 -12.72007179
		 0.84114379 37.60486603 -12.69336224 0 36.74917984 -12.69336224 1.58643198 36.040988922 -12.73276043
		 1.58643198 36.040988922 -12.72007179 1.55792272 36.062789917 -12.69336224 1.63387585 37.048316956 -12.69336224
		 1.61056793 37.1084938 -12.72007179 1.61056793 37.1084938 -12.73276043 0.74611133 35.12124252 -12.73276043
		 0.74611133 35.12124252 -12.72007179 0.68132597 35.10547638 -12.68430996 2.34963179 36.12643433 -12.73276043
		 2.34963179 36.12643433 -12.72007179 2.34896064 36.15680313 -12.69336224 2.90837622 35.90962601 -12.69336224
		 2.87209368 35.88949203 -12.72007179 2.87209368 35.88949203 -12.73276043 2.54264712 37.12960815 -12.69336224
		 2.54876423 37.19647217 -12.72007179 2.54876423 37.19647217 -12.73276043 3.24782181 36.65264893 -12.73276043
		 3.24782181 36.65264893 -12.72007179 3.18993163 36.60686111 -12.69336224 3.1914947 35.53639603 -12.72007179
		 3.14324117 35.58275986 -12.69336224 3.10066581 35.56794357 -12.72007179 3.10000706 35.56861496 -12.73250389
		 3.14976621 35.53636932 -12.73225498 3.19263029 35.53788376 -12.73250389 3.29052496 35.80698395 -12.72007179
		 3.2930882 35.80698395 -12.7332859 3.33281803 36.10623932 -12.73377705 3.33281803 36.10623932 -12.72007179
		 3.26048923 35.87325668 -12.69336224 1.37137282 33.37491989 -12.69336224 1.39864612 33.31293106 -12.72007179
		 1.39864612 33.31293106 -12.73276043 1.39864612 33.8290329 -12.73276043 1.39864612 33.8290329 -12.72007179
		 1.42004657 33.7639389 -12.69336224 1.7265507 33.70728302 -12.72007179 1.65946901 33.66613007 -12.69336224
		 1.66485214 33.58854294 -12.72007179 1.68561602 33.61952591 -12.73345566 1.72311258 33.68399048 -12.73334789
		 1.82012129 34.066856384 -12.72007179 1.81971347 34.064113617 -12.73045349 1.83724976 34.18022156 -12.72555065
		 1.75006568 34.11040878 -12.72545528 1.73603094 34.0053024292 -12.69336224 0.46012953 33.59151459 -12.75621414
		 0.13290232 39.99663544 -12.72007179 0.52899921 35.40000916 -12.72950554 0.079516791 32.5398674 -12.72007179
		 0.671413 38.77528381 -12.72007179 3.14976621 35.53636932 -12.72007179 1.69068849 33.63597107 -12.72007179
		 3.27544093 35.74458313 -12.73270988 3.27544093 35.74458313 -12.72007179 3.23154783 35.79618073 -12.69336224
		 2.96785378 36.18877029 -12.69336224 2.355335 36.41127014 -12.69336224 1.57786787 36.33632278 -12.69336224
		 0.8383534 36.053031921 -12.69336224 0.76228106 36.0054855347 -12.72950554 0.69223171 35.99369812 -12.69336224
		 0 35.81474304 -12.69336224 -1.2548017e-05 34.33067322 -12.71143913 0.31068262 34.32948685 -12.71143913
		 0 40.03200531 -13.66417885 0.077465184 40.020717621 -13.66454506 0.37265733 32.95821762 -13.66417885
		 0.44521621 33.51451111 -13.66355515 0.5251345 33.55181122 -13.66201973 0.46012953 33.59151459 -13.69870758
		 0.96705687 33.2817955 -13.66417885 0.96705687 33.85316849 -13.66417885 0.40682244 34.30685043 -13.66259575
		 0.49061492 34.36193085 -13.66245174 0.33631822 34.32092667 -13.66238117 0.74611133 34.63708496 -13.66417885
		 0.13290232 39.99663544 -13.66465473 0.15461667 39.97050095 -13.66454506 0.671413 38.77528381 -13.66521931
		 0.54511714 35.43628693 -13.66239548 0.57761031 35.36927795 -13.66308022 0.59581751 35.47023773 -13.66206551
		 0.74611133 35.12124252 -13.66417885;
	setAttr ".vt[166:192]" 1.58643198 36.040988922 -13.66417885 1.61056793 37.1084938 -13.66417885
		 0.99406648 36.74362946 -13.662817 0.93815875 36.76512527 -13.66078091 0.91888499 36.70302582 -13.6713562
		 0.90584761 37.61158752 -13.66417885 0.12908775 32.57535172 -13.66495991 0.079516791 32.5398674 -13.66491127
		 0.053216148 32.52607346 -13.66456985 1.8822026e-05 32.51260757 -13.66417885 1.39864612 33.31293106 -13.66417885
		 1.39864612 33.8290329 -13.66417885 2.34963179 36.12643433 -13.66417885 2.54876423 37.19647217 -13.66417885
		 2.87209368 35.88949203 -13.66417885 3.10000706 35.56861496 -13.66392326 3.24782181 36.65264893 -13.66417885
		 3.33281803 36.10623932 -13.66519547 3.14976621 35.53636932 -13.66367435 3.19263029 35.53788376 -13.66392326
		 3.27877235 35.74886703 -13.6641283 3.29052496 35.80698395 -13.66470528 1.68561602 33.61952591 -13.66487503
		 1.75050509 34.11076355 -13.65956593 1.72311258 33.68399048 -13.66476631 1.81971347 34.064113617 -13.66187286
		 1.83724952 34.18022156 -13.65970802;
	setAttr -s 356 ".ed";
	setAttr ".ed[0:165]"  1 2 0 3 4 0 5 0 0 8 7 0 7 27 0 27 32 1 32 8 0 7 6 0
		 6 28 1 28 27 1 10 9 1 9 57 0 57 56 1 56 10 0 9 16 1 16 58 1 58 57 1 12 11 0 11 10 0
		 14 13 1 13 69 0 69 68 1 68 14 1 13 12 1 12 70 0 70 69 0 16 15 1 15 25 1 25 24 1 24 16 1
		 15 14 1 14 26 1 26 25 1 18 17 1 17 72 0 72 71 1 71 18 0 17 26 1 26 73 1 73 72 1 20 18 0
		 19 18 0 22 21 1 52 22 1 21 20 1 20 50 0 24 23 1 23 54 1 54 53 0 53 24 1 23 146 1
		 22 55 1 55 145 0 30 29 1 29 28 1 28 75 1 32 31 0 31 30 0 30 74 0 35 34 0 36 35 0
		 34 33 1 33 87 0 87 86 0 86 34 0 33 40 1 40 88 1 88 87 1 38 37 1 82 38 1 37 36 1 36 80 0
		 40 39 1 39 142 1 46 143 1 39 38 1 38 141 1 47 45 1 41 49 1 49 48 0 48 40 1 41 40 1
		 43 42 1 85 43 0 42 47 1 47 83 1 45 44 0 44 43 0 46 45 1 78 46 1 45 76 0 52 51 1 88 52 1
		 51 50 0 50 86 0 63 62 1 62 56 0 58 64 1 64 63 1 61 60 0 62 61 0 60 59 1 59 66 0 66 65 0
		 65 60 0 59 64 1 64 67 1 67 66 0 113 112 1 112 68 1 70 114 0 114 113 0 116 115 0 115 71 0
		 73 117 1 117 116 1 77 76 1 76 74 0 75 78 1 78 77 1 82 81 1 91 82 1 81 80 1 80 89 0
		 85 84 1 97 85 0 84 83 1 83 95 1 91 90 1 90 93 0 93 92 1 92 91 1 90 89 0 89 94 0 94 93 0
		 103 102 1 102 92 1 94 104 0 104 103 1 97 96 0 96 99 0 99 98 0 98 97 0 96 95 1 95 100 1
		 100 99 1 110 109 1 109 98 0 100 111 1 111 110 1 105 104 0 106 105 0 102 101 1 101 136 0
		 107 111 1 111 137 1 101 106 1 106 135 0 108 107 1 109 108 0 120 119 1 119 112 1 114 121 0
		 121 120 1 126 115 0 117 127 1;
	setAttr ".ed[166:331]" 127 126 1 122 121 0 119 118 1 118 123 0 123 127 1 127 119 1
		 118 122 1 122 124 0 124 123 1 125 124 0 126 125 0 0 53 0 4 6 0 2 48 0 48 144 0 68 73 1
		 82 140 1 55 1 0 95 139 1 92 138 1 67 5 0 112 117 1 0 16 1 52 1 1 88 2 1 78 3 1 75 4 1
		 58 5 1 21 51 0 57 63 0 69 113 0 72 116 0 45 77 0 37 81 0 42 84 0 51 87 0 81 90 0
		 93 103 0 84 96 0 99 110 0 113 120 0 116 126 0 9 128 0 128 15 1 11 128 0 13 128 0
		 17 19 0 19 25 1 21 18 0 23 19 1 27 129 0 129 31 0 29 129 0 33 130 0 130 41 1 35 130 0
		 37 130 0 39 130 1 42 45 0 59 131 0 131 63 0 61 131 0 77 132 0 132 29 0 74 132 1 101 133 0
		 133 105 0 103 133 0 107 110 0 118 134 0 134 121 0 120 134 0 123 125 0 132 75 1 135 108 0
		 136 107 0 137 102 1 138 100 1 139 91 1 140 83 1 141 47 1 142 45 1 143 40 1 144 79 0
		 135 136 1 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 142 1 142 143 1 143 144 1
		 145 54 0 146 22 1 145 146 1 146 18 1 79 3 0 46 79 1 8 147 0 32 148 0 148 147 0 56 149 1
		 10 150 0 149 150 0 12 151 0 11 152 0 151 152 0 152 150 0 70 153 0 151 153 0 71 154 1
		 18 155 0 154 155 0 20 156 0 156 155 0 19 157 0 157 155 0 50 158 0 156 158 0 31 159 1
		 148 159 0 30 160 1 159 160 0 74 161 0 160 161 0 35 162 0 34 163 0 162 163 0 36 164 0
		 164 162 0 86 165 0 165 163 0 80 166 1 164 166 0 85 167 1 43 168 0 167 168 0 45 169 0
		 44 170 0 169 170 0 170 168 0 76 171 1 169 171 0 158 165 0 62 172 0 172 149 0 61 173 1
		 60 174 0 173 174 0 172 173 0 65 175 0 175 174 0 114 176 0 153 176 0 115 177 0 177 154 0
		 171 161 0 89 178 0 166 178 0 97 179 0 179 167 0 94 180 0 178 180 0 104 181 0 180 181 0;
	setAttr ".ed[332:355]" 98 182 0 182 179 0 109 183 0 183 182 0 105 184 0 184 181 0
		 106 185 0 185 184 0 135 186 1 185 186 0 108 187 0 183 187 0 121 188 0 176 188 0 126 189 0
		 189 177 0 122 190 0 190 188 0 124 191 0 190 191 0 125 192 0 192 191 0 189 192 0 186 187 0;
	setAttr -s 166 -ch 656 ".fc[0:165]" -type "polyFaces" 
		f 4 3 4 5 6
		mu 0 4 24 83 95 68
		f 4 7 8 9 -5
		mu 0 4 83 66 6 95
		f 4 10 11 12 13
		mu 0 4 0 84 109 28
		f 4 14 15 16 -12
		mu 0 4 84 72 71 109
		f 4 19 20 21 22
		mu 0 4 2 88 114 31
		f 4 23 24 25 -21
		mu 0 4 88 1 46 114
		f 4 26 27 28 29
		mu 0 4 82 89 94 55
		f 4 30 31 32 -28
		mu 0 4 89 2 3 94
		f 4 33 34 35 36
		mu 0 4 4 90 115 30
		f 4 37 38 39 -35
		mu 0 4 90 3 48 115
		f 4 46 47 48 49
		mu 0 4 55 93 108 53
		f 4 261 260 51 52
		mu 0 4 154 155 5 36
		f 4 53 -230 -231 -59
		mu 0 4 7 96 141 116
		f 4 61 62 63 64
		mu 0 4 26 99 120 25
		f 4 65 66 67 -63
		mu 0 4 99 61 57 120
		f 4 72 73 257 248
		mu 0 4 9 102 151 152
		f 4 75 76 256 -74
		mu 0 4 102 8 150 151
		f 4 -82 78 79 80
		mu 0 4 61 103 106 59
		f 4 81 -81 -80 -79
		mu 0 4 103 9 27 106
		f 4 101 102 103 104
		mu 0 4 70 110 113 69
		f 4 105 106 107 -103
		mu 0 4 110 13 45 113
		f 4 116 117 230 -229
		mu 0 4 117 33 116 141
		f 4 118 119 228 239
		mu 0 4 65 63 117 141
		f 4 128 129 130 131
		mu 0 4 76 121 122 41
		f 4 132 133 134 -130
		mu 0 4 121 38 42 122
		f 4 139 140 141 142
		mu 0 4 77 123 124 39
		f 4 143 144 145 -141
		mu 0 4 123 40 43 124
		f 4 152 153 251 242
		mu 0 4 44 125 145 146
		f 4 156 157 250 -154
		mu 0 4 125 16 144 145
		f 4 168 169 170 171
		mu 0 4 52 132 134 20
		f 4 172 173 174 -170
		mu 0 4 132 21 22 134
		f 4 -32 -23 181 -39
		mu 0 4 3 2 31 48
		f 4 255 -77 -70 182
		mu 0 4 149 150 8 35
		f 4 253 244 -132 185
		mu 0 4 147 148 76 41
		f 4 252 -186 -137 -243
		mu 0 4 146 147 41 44
		f 4 -182 -110 187 -115
		mu 0 4 48 31 49 51
		f 4 -188 -162 -172 -166
		mu 0 4 51 49 52 20
		f 4 -178 188 -30 -50
		mu 0 4 53 54 82 55
		f 4 -184 -52 -44 189
		mu 0 4 58 36 5 56
		f 4 190 -1 -190 -93
		mu 0 4 57 60 58 56
		f 4 -180 -191 -67 -81
		mu 0 4 59 60 57 61
		f 4 258 -181 80 -249
		mu 0 4 152 153 27 9
		f 4 -2 -192 -119 192
		mu 0 4 67 64 63 65
		f 4 -179 -193 -56 -9
		mu 0 4 66 67 65 6
		f 4 193 -187 -107 -98
		mu 0 4 71 73 45 13
		f 4 -189 -3 -194 -16
		mu 0 4 72 29 73 71
		f 4 254 -183 -122 -245
		mu 0 4 148 149 35 76
		f 4 42 194 -92 43
		mu 0 4 5 92 107 56
		f 4 44 45 -94 -195
		mu 0 4 92 78 37 107
		f 4 -13 195 95 96
		mu 0 4 28 109 112 12
		f 4 -17 97 98 -196
		mu 0 4 109 71 13 112
		f 4 -22 196 108 109
		mu 0 4 31 114 130 49
		f 4 -26 110 111 -197
		mu 0 4 114 46 50 130
		f 4 -36 197 112 113
		mu 0 4 30 115 131 47
		f 4 -40 114 115 -198
		mu 0 4 115 48 51 131
		f 4 88 198 -120 89
		mu 0 4 11 79 117 63
		f 3 90 -117 -199
		mu 0 3 79 33 117
		f 4 68 199 -121 69
		mu 0 4 8 101 118 35
		f 4 70 71 -123 -200
		mu 0 4 101 80 74 118
		f 4 82 200 -125 83
		mu 0 4 10 104 119 34
		f 4 84 85 -127 -201
		mu 0 4 104 81 75 119
		f 4 91 201 -68 92
		mu 0 4 56 107 120 57
		f 4 93 94 -64 -202
		mu 0 4 107 37 25 120
		f 4 120 202 -129 121
		mu 0 4 35 118 121 76
		f 4 122 123 -133 -203
		mu 0 4 118 74 38 121
		f 4 -131 203 135 136
		mu 0 4 41 122 126 44
		f 4 -135 137 138 -204
		mu 0 4 122 42 14 126
		f 4 124 204 -140 125
		mu 0 4 34 119 123 77
		f 4 126 127 -144 -205
		mu 0 4 119 75 40 123
		f 4 -142 205 146 147
		mu 0 4 39 124 129 18
		f 4 -146 148 149 -206
		mu 0 4 124 43 15 129
		f 4 -109 206 160 161
		mu 0 4 49 130 133 52
		f 4 -112 162 163 -207
		mu 0 4 130 50 19 133
		f 3 -113 207 164
		mu 0 3 47 131 23
		f 4 -116 165 166 -208
		mu 0 4 131 51 20 23
		f 4 -27 -15 208 209
		mu 0 4 89 82 85 136
		f 4 -11 -19 210 -209
		mu 0 4 84 0 87 137
		f 4 -18 -24 211 -211
		mu 0 4 86 1 88 136
		f 4 -20 -31 -210 -212
		mu 0 4 88 2 89 136
		f 4 -33 -38 212 213
		mu 0 4 94 3 90 91
		f 3 -34 -42 -213
		mu 0 3 90 4 91
		f 3 -41 -45 214
		mu 0 3 156 78 92
		f 4 -43 -261 262 -215
		mu 0 4 92 5 155 156
		f 4 -47 -29 -214 -216
		mu 0 4 93 55 94 91
		f 4 -57 -6 216 217
		mu 0 4 97 68 95 138
		f 4 -10 -55 218 -217
		mu 0 4 95 6 96 138
		f 4 -54 -58 -218 -219
		mu 0 4 96 7 98 138
		f 4 81 -66 219 220
		mu 0 4 103 61 99 139
		f 4 -62 -60 221 -220
		mu 0 4 99 26 100 139
		f 4 -61 -71 222 -222
		mu 0 4 100 80 101 139
		f 4 -69 -76 223 -223
		mu 0 4 101 8 102 139
		f 4 -73 -82 -221 -224
		mu 0 4 102 9 103 139
		f 3 -78 -85 224
		mu 0 3 79 81 104
		f 4 -83 -88 -87 -225
		mu 0 4 104 10 105 79
		f 4 -99 -106 225 226
		mu 0 4 112 13 110 140
		f 4 -102 -100 227 -226
		mu 0 4 110 70 111 140
		f 4 -101 -96 -227 -228
		mu 0 4 111 12 112 140
		f 4 -152 -157 231 232
		mu 0 4 127 16 125 142
		f 4 -153 -136 233 -232
		mu 0 4 125 44 126 142
		f 4 -139 -151 -233 -234
		mu 0 4 126 14 127 142
		f 3 -150 -155 234
		mu 0 3 129 15 128
		f 4 -159 -160 -147 -235
		mu 0 4 128 17 18 129
		f 4 -168 -173 235 236
		mu 0 4 19 21 132 143
		f 4 -169 -161 237 -236
		mu 0 4 132 52 133 143
		f 3 -164 -237 -238
		mu 0 3 133 19 143
		f 4 -167 -171 238 -177
		mu 0 4 23 20 134 135
		f 3 -175 -176 -239
		mu 0 3 134 22 135
		f 4 -240 229 54 55
		mu 0 4 65 141 96 6
		f 4 -251 240 158 -242
		mu 0 4 145 144 17 128
		f 4 -252 241 154 155
		mu 0 4 146 145 128 15
		f 4 -244 -253 -156 -149
		mu 0 4 43 147 146 15
		f 4 184 -254 243 -145
		mu 0 4 40 148 147 43
		f 4 -246 -255 -185 -128
		mu 0 4 75 149 148 40
		f 4 -247 -256 245 -86
		mu 0 4 81 150 149 75
		f 4 -257 246 77 -248
		mu 0 4 151 150 81 79
		f 4 -258 247 -89 74
		mu 0 4 152 151 79 62
		f 4 50 -262 259 -48
		mu 0 4 93 155 154 108
		f 4 -263 -51 215 41
		mu 0 4 156 155 93 91
		f 4 191 -264 -265 -90
		mu 0 4 63 64 32 11
		f 4 264 -250 -259 -75
		mu 0 4 62 32 153 152
		f 4 266 267 -266 -7
		mu 0 4 157 160 159 158
		f 4 268 270 -270 -14
		mu 0 4 161 164 163 162
		f 4 17 272 -274 -272
		mu 0 4 165 166 167 168
		f 4 18 269 -275 -273
		mu 0 4 169 170 171 172
		f 4 271 276 -276 -25
		mu 0 4 173 176 175 174
		f 4 277 279 -279 -37
		mu 0 4 177 180 179 178
		f 4 40 278 -282 -281
		mu 0 4 181 182 183 184
		f 4 41 278 -284 -283
		mu 0 4 185 186 187 188
		f 4 280 285 -285 -46
		mu 0 4 189 192 191 190
		f 4 56 286 -288 -267
		mu 0 4 193 194 195 196
		f 4 57 288 -290 -287
		mu 0 4 197 198 199 200
		f 4 58 290 -292 -289
		mu 0 4 201 202 203 204
		f 4 59 293 -295 -293
		mu 0 4 205 206 207 208
		f 4 60 292 -297 -296
		mu 0 4 209 210 211 212
		f 4 297 298 -294 -65
		mu 0 4 213 216 215 214
		f 4 295 300 -300 -72
		mu 0 4 217 220 219 218
		f 4 301 303 -303 -84
		mu 0 4 221 224 223 222
		f 4 86 305 -307 -305
		mu 0 4 225 226 227 228
		f 4 87 302 -308 -306
		mu 0 4 229 230 231 232
		f 4 304 309 -309 -91
		mu 0 4 233 236 235 234
		f 4 284 310 -298 -95
		mu 0 4 237 240 239 238
		f 4 311 312 -269 -97
		mu 0 4 241 244 243 242
		f 4 99 314 -316 -314
		mu 0 4 245 246 247 248
		f 4 100 313 -317 -312
		mu 0 4 249 250 251 252
		f 4 317 318 -315 -105
		mu 0 4 253 256 255 254
		f 4 275 320 -320 -111
		mu 0 4 257 260 259 258
		f 4 321 322 -278 -114
		mu 0 4 261 264 263 262
		f 4 308 323 -291 -118
		mu 0 4 265 268 267 266
		f 4 299 325 -325 -124
		mu 0 4 269 272 271 270
		f 4 326 327 -302 -126
		mu 0 4 273 276 275 274
		f 4 324 329 -329 -134
		mu 0 4 277 280 279 278
		f 4 328 331 -331 -138
		mu 0 4 281 284 283 282
		f 4 332 333 -327 -143
		mu 0 4 285 288 287 286
		f 4 334 335 -333 -148
		mu 0 4 289 292 291 290
		f 4 150 330 -338 -337
		mu 0 4 293 294 295 296
		f 4 151 336 -340 -339
		mu 0 4 297 298 299 300
		f 4 338 341 -341 -158
		mu 0 4 301 304 303 302
		f 4 159 342 -344 -335
		mu 0 4 305 306 307 308
		f 4 319 345 -345 -163
		mu 0 4 309 312 311 310
		f 4 346 347 -322 -165
		mu 0 4 313 316 315 314
		f 4 167 344 -350 -349
		mu 0 4 317 318 319 320
		f 4 348 351 -351 -174
		mu 0 4 321 324 323 322
		f 4 175 350 -354 -353
		mu 0 4 325 326 327 328
		f 4 176 352 -355 -347
		mu 0 4 329 330 331 332
		f 4 340 355 -343 -241
		mu 0 4 333 336 335 334
		f 4 -42 282 283 -279
		mu 0 4 337 338 339 340;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "bottom";
	rename -uid "DDC086D3-466D-CD55-3AC1-D2B09D88EB06";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 -1000.1 0 ;
	setAttr ".r" -type "double3" 90 0 0 ;
createNode camera -n "bottomShape" -p "bottom";
	rename -uid "0ED9C1B5-44CE-EA5D-AD31-7A92904F6854";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 146.89644246557094;
	setAttr ".imn" -type "string" "bottom1";
	setAttr ".den" -type "string" "bottom1_depth";
	setAttr ".man" -type "string" "bottom1_mask";
	setAttr ".hc" -type "string" "viewSet -bo %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube3";
	rename -uid "0078737B-4A44-A30D-B2CE-8FA3937A1E92";
	setAttr ".t" -type "double3" 0 2.0176014845176837 -0.14681804638274798 ;
	setAttr ".s" -type "double3" 0.93663816698334901 0.84531035554551925 1 ;
	setAttr ".rp" -type "double3" 0 36.272306442260742 -13.191508769989014 ;
	setAttr ".sp" -type "double3" 0 36.272306442260742 -13.191508769989014 ;
createNode mesh -n "pCube3Shape" -p "pCube3";
	rename -uid "0DC97E23-4D0F-459E-50FD-D9BE663A9CAD";
	setAttr -k off ".v";
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.82771698315464048 0.89206391572952271 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 3;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "0E1BD9DF-4591-0A86-8EEF-C0A7C9CE1042";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "6C249357-426E-71D7-7C5C-3887804A403A";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "5498B489-4A30-4043-8A80-0787C3617B0A";
createNode displayLayerManager -n "layerManager";
	rename -uid "69156C8D-4D81-96C0-A71B-49A4A1C3576B";
createNode displayLayer -n "defaultLayer";
	rename -uid "B7838757-4E2B-110A-00D8-11969C9A8D15";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "18B7E710-471A-CE8B-EE95-F591AD1F2061";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "5BA8270F-4DBB-8882-89BC-08BDDFDE5344";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "32897DE9-4D8F-75FC-1D6B-1081C9DE2A3D";
	setAttr ".version" -type "string" "5.3.3.3";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "CA98AD84-4987-4AF1-95DA-659E2485148F";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "03A774A1-479C-47E7-8F8E-AB9907ED0A3C";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "7D284BAD-42DC-88E7-9F7A-C89DB4F2B8BB";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "72AB6AAC-4670-D110-B17A-1295C376DBBE";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 2745\n            -height 1682\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 1\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 0\n            -nurbsCurves 0\n            -nurbsSurfaces 0\n            -polymeshes 0\n            -subdivSurfaces 0\n            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 0\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 1\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 2748\n            -height 1681\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n"
		+ "                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"|persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n"
		+ "                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n"
		+ "                -clipGhosts 1\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 1\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 2748\\n    -height 1681\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 1\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 2748\\n    -height 1681\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "CBD6F462-4A94-3E74-8638-AD9511189493";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "pasted__polyCube3";
	rename -uid "47617AAE-4F65-159C-A588-B38901EEFA9B";
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube3";
	rename -uid "69212025-4A50-9C50-147B-CDAAB85929AD";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit1";
	rename -uid "A0D2D42D-4F27-F701-E82B-BFA19EC44F47";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483638 -2147483637 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "2C65F075-48E7-B65F-509E-E4ACB58CBD14";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483635 -2147483634 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit3";
	rename -uid "37604A70-481D-422C-37FF-E09E0BAEB06E";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483638 -2147483636 -2147483633 -2147483637 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "077B43DD-4F78-5350-C627-4BA022A5D27F";
	setAttr ".txf" -type "matrix" 0 0 26.942658920980762 0 0 4.8229923591373547 0 0
		 -61.735811555244446 0 0 0 0 14.055402217020124 0.43268010300644155 1;
createNode transformGeometry -n "transformGeometry3";
	rename -uid "2D772556-44E3-52C2-6877-3B81ADE89FC9";
	setAttr ".txf" -type "matrix" 0 0 27.929016556398253 0 0 2.5356339022665977 0 0
		 -64.283691684157816 0 0 0 0 9.4882754249999337 -0.063284071563059818 1;
createNode transformGeometry -n "transformGeometry4";
	rename -uid "0F22C640-46E6-DBC6-5D4A-A8A55B042038";
	setAttr ".txf" -type "matrix" 0 20.155488241077791 0 0 0 0 -1.5849456024209805 0
		 -63.270555652540573 0 0 0 0.15730165094147711 16.014298387521411 -13.935576105548547 1;
createNode polyTweak -n "polyTweak1";
	rename -uid "5EEF25C1-4129-758B-1CEE-9DA94738CC01";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[10:11]" -type "float3"  0 10.043622017 0 0 10.043622017
		 0;
createNode polySplit -n "polySplit4";
	rename -uid "2978FF1C-4A76-3434-B56C-4299BEC1F0A8";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483635 -2147483628 -2147483625 -2147483634 -2147483635;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "FD14EACB-49BA-1B98-1D25-E08BDFBADDF3";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483636 -2147483620 -2147483617 -2147483633 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "A4EBF41A-419F-8730-2B6A-078097BB20F4";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[12:27]" -type "float3"  3.51617122 0 0 3.51617122
		 0 0 3.51617122 0 0 3.51617122 0 0 -3.51617122 0 0 -3.51617122 0 0 -3.51617122 0 0
		 -3.51617122 0 0 1.699422 0 0 1.699422 0 0 1.699422 3.33820844 0 1.699422 3.33820844
		 0 -1.69942188 0 0 -1.69942188 0 0 -1.69942188 3.33820844 0 -1.69942188 3.33820844
		 0;
createNode polySplit -n "polySplit6";
	rename -uid "9C5FC36F-4E4E-1A94-DF00-D4BED985A084";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483627 -2147483626 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit7";
	rename -uid "7D21F2D1-4970-B8AA-FCD0-FEBEF3DA557A";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483638 -2147483619 -2147483618 -2147483637 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "645F1CA0-417C-1230-279D-AAA0F0DFE759";
	setAttr ".uopa" yes;
	setAttr -s 28 ".tk[8:35]" -type "float3"  -2.5854307e-08 0 0 -2.5854307e-08
		 0 0 -2.5854307e-08 -1.83709979 0 -2.5854307e-08 -1.83709979 0 -1.26227212 0 0 -1.26227212
		 0 0 -1.26227212 2.78770232 0 -1.26227212 2.78770232 0 1.26227212 0 0 1.26227212 0
		 0 1.26227212 2.78770232 0 1.26227212 2.78770232 0 -0.63715571 0 0 -0.63715571 0 0
		 -0.63715571 -0.96156466 0 -0.63715571 -0.96156466 0 0.63715571 0 0 0.63715571 0 0
		 0.63715571 -0.96156466 0 0.63715571 -0.96156466 0 -1.158288 0 0 -1.158288 0 0 -1.158288
		 0.86742365 0 -1.158288 0.86742365 0 1.158288 0 0 1.158288 0 0 1.158288 0.86742365
		 0 1.158288 0.86742365 0;
createNode polySplit -n "polySplit8";
	rename -uid "ED770DE0-4BF2-E48F-31C7-8D94484B0C7E";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483627 -2147483596 -2147483593 -2147483626 -2147483627;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit9";
	rename -uid "68B853EA-4561-93D7-B16C-C7A2739FBFEB";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483619 -2147483588 -2147483585 -2147483618 -2147483619;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit10";
	rename -uid "BF22CE73-4E1A-1F3D-67CC-009EB66DFFCD";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483638 -2147483637 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit11";
	rename -uid "5DEF78BC-425B-D96B-0164-4BAB2403D7F2";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483638 -2147483636 -2147483633 -2147483637 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit12";
	rename -uid "9FBB3C87-4F71-1030-2C47-91AC27DBA549";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483635 -2147483634 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit13";
	rename -uid "5CDA7D9E-4426-1F72-E8A7-8886FB75B1A0";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483638 -2147483627 -2147483626 -2147483637 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit14";
	rename -uid "A38D4D1F-46C1-07BC-82E9-0EB2194DF49E";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483636 -2147483628 -2147483625 -2147483633 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "DA478371-40C6-72CC-3966-0EA345C10FEE";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[5]" -type "float3" 0 7.101881 0 ;
	setAttr ".tk[7]" -type "float3" 0 7.101881 0 ;
	setAttr ".tk[8]" -type "float3" 0 0 2.0671794 ;
	setAttr ".tk[9]" -type "float3" 0 0 2.0671794 ;
	setAttr ".tk[10]" -type "float3" 0 0.61676186 2.0671794 ;
	setAttr ".tk[11]" -type "float3" 0 0.61676186 2.0671794 ;
	setAttr ".tk[12]" -type "float3" 0 0 1.2416531 ;
	setAttr ".tk[13]" -type "float3" 0 0 1.2416531 ;
	setAttr ".tk[14]" -type "float3" 0 4.2464657 1.2416531 ;
	setAttr ".tk[15]" -type "float3" 0 4.2464657 1.2416531 ;
	setAttr ".tk[18]" -type "float3" 0 2.9802322e-08 0 ;
	setAttr ".tk[19]" -type "float3" 0 2.9802322e-08 0 ;
	setAttr ".tk[20]" -type "float3" 0 0 1.1669327 ;
	setAttr ".tk[21]" -type "float3" 0 0 1.1669327 ;
	setAttr ".tk[22]" -type "float3" 0 7.101882 1.1669327 ;
	setAttr ".tk[23]" -type "float3" 0 7.101882 1.1669327 ;
	setAttr ".tk[24]" -type "float3" 0 0 1.3542243 ;
	setAttr ".tk[25]" -type "float3" 0 0 1.3542243 ;
	setAttr ".tk[26]" -type "float3" 0 2.2226548 1.3542243 ;
	setAttr ".tk[27]" -type "float3" 0 2.2226548 1.3542243 ;
createNode polySplit -n "polySplit15";
	rename -uid "57234D22-4AAC-EF28-E58C-C69564B4206D";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483638 -2147483611 -2147483610 -2147483637 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "B0D245F3-4361-9450-F47D-A9BB7A1623A3";
	setAttr ".uopa" yes;
	setAttr -s 26 ".tk";
	setAttr ".tk[5]" -type "float3" 0 0.71294302 0 ;
	setAttr ".tk[7]" -type "float3" 0 0.71294302 0 ;
	setAttr ".tk[8]" -type "float3" 0 0 2.3220258 ;
	setAttr ".tk[9]" -type "float3" 0 0 2.3220258 ;
	setAttr ".tk[10]" -type "float3" 0 -0.16663945 2.3220258 ;
	setAttr ".tk[11]" -type "float3" 0 -0.16663945 2.3220258 ;
	setAttr ".tk[12]" -type "float3" 0 0 1.8597324 ;
	setAttr ".tk[13]" -type "float3" 0 0 1.8597324 ;
	setAttr ".tk[14]" -type "float3" 0 0 1.8597324 ;
	setAttr ".tk[15]" -type "float3" 0 0 1.8597324 ;
	setAttr ".tk[16]" -type "float3" 0 0 2.3220258 ;
	setAttr ".tk[17]" -type "float3" 0 0 2.3220258 ;
	setAttr ".tk[18]" -type "float3" 0 0 2.3220258 ;
	setAttr ".tk[19]" -type "float3" 0 0 2.3220258 ;
	setAttr ".tk[20]" -type "float3" 0 0 0.72350264 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.72350264 ;
	setAttr ".tk[22]" -type "float3" 0 0 0.72350264 ;
	setAttr ".tk[23]" -type "float3" 0 0 0.72350264 ;
	setAttr ".tk[24]" -type "float3" 0 0 1.8597323 ;
	setAttr ".tk[25]" -type "float3" 0 0 1.8597323 ;
	setAttr ".tk[26]" -type "float3" 0 -0.35104096 1.8597323 ;
	setAttr ".tk[27]" -type "float3" 0 -0.35104096 1.8597323 ;
	setAttr ".tk[28]" -type "float3" 0 0 0.72350264 ;
	setAttr ".tk[29]" -type "float3" 0 0 0.72350264 ;
	setAttr ".tk[30]" -type "float3" 0 0.71294302 0.72350264 ;
	setAttr ".tk[31]" -type "float3" 0 0.71294302 0.72350264 ;
createNode polySplit -n "polySplit16";
	rename -uid "048ADBAA-4BE1-97B7-E4C5-8A9CC5C014A1";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483627 -2147483612 -2147483609 -2147483626 -2147483627;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit17";
	rename -uid "3F1942C3-425A-2557-26FF-DEBE6AA4A88A";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483638 -2147483637 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit18";
	rename -uid "C8035035-4DAE-5ACD-BFF3-878980069BF4";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483648 -2147483647 -2147483629 -2147483646 -2147483645 -2147483631 
		-2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit19";
	rename -uid "8A74FA3E-4A00-CBF8-A871-6FA142872526";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483638 -2147483636 -2147483620 -2147483633 -2147483637 -2147483618 
		-2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit20";
	rename -uid "B44CFB9A-4DC2-52D2-D90C-2D8DA1F441EE";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483642 -2147483635 -2147483617 -2147483634 -2147483641 -2147483621 
		-2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit21";
	rename -uid "A8269469-475C-9E82-C3A2-33B71CA58771";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483629 -2147483594 -2147483627 -2147483628 -2147483596 -2147483623 
		-2147483606 -2147483624 -2147483625 -2147483608 -2147483629;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit22";
	rename -uid "1797E089-4360-9B41-C528-52A038406011";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483648 -2147483647 -2147483593 -2147483626 -2147483609 -2147483646 
		-2147483645 -2147483605 -2147483631 -2147483597 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCube -n "polyCube4";
	rename -uid "5AC5B875-454B-C70D-CFC3-9F8428EB8E6C";
	setAttr ".cuv" 4;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "287F6771-4EB7-630B-730B-FB997BDD3F05";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[0:15]" "e[20:21]" "e[23:24]" "e[32:33]" "e[35:36]" "e[44:45]" "e[47:48]" "e[58:59]" "e[63:64]" "e[76:77]" "e[81:82]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.98692533935277804 0 0 0 -0.08056620222682831 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak6";
	rename -uid "A6D33517-4CF5-D051-3397-729EBBC89E86";
	setAttr ".uopa" yes;
	setAttr -s 15 ".tk";
	setAttr ".tk[13]" -type "float3" 0 0.43048182 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[15]" -type "float3" 0 0.43048182 0 ;
	setAttr ".tk[20]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[29]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[30]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[31]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[32]" -type "float3" 0 0.43048182 0 ;
	setAttr ".tk[38]" -type "float3" 0 0.43048182 0 ;
	setAttr ".tk[39]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[41]" -type "float3" 0 0.43048182 0 ;
	setAttr ".tk[42]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[43]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[44]" -type "float3" 0 0.62310314 0 ;
	setAttr ".tk[45]" -type "float3" 0 0.43048182 0 ;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "E004A9A9-4282-D7D1-F6D1-69BC032D5330";
	setAttr ".ics" -type "componentList" 12 "e[45]" "e[132]" "e[186]" "e[242]" "e[245:246]" "e[248:249]" "e[254:255]" "e[258:260]" "e[267:268]" "e[270]" "e[279:280]" "e[284]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "915943C1-4E93-8217-0EFD-FDA9886B1312";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[139]" -type "float3" 0.55559671 -0.57164991 0.24247274 ;
	setAttr ".tk[140]" -type "float3" -0.55559671 -0.57164991 0.24247274 ;
	setAttr ".tk[143]" -type "float3" 0.55559671 -0.57164991 -0.24247274 ;
	setAttr ".tk[144]" -type "float3" -0.55559671 -0.57164991 -0.24247274 ;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "881875E0-41EA-A833-A1F4-C798A2E898FF";
	setAttr ".ics" -type "componentList" 1 "e[249]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "6FEC0F7A-45B7-53C9-43D5-3F8A104382E4";
	setAttr ".ics" -type "componentList" 1 "e[240]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "A73FE3A1-44FE-9A14-46F7-8F84A51812A3";
	setAttr ".ics" -type "componentList" 4 "e[175]" "e[224]" "e[233]" "e[246]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge5";
	rename -uid "1CAAAB72-4195-B9C4-F831-9DB5FE7E592A";
	setAttr ".ics" -type "componentList" 1 "e[232]";
	setAttr ".cv" yes;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "A630A6E8-4DB8-8EEC-CE2D-988822E91117";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.3;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "0441525F-4B68-C387-A352-CC987A221A7B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 2.0520273429232301 0 0 0 0 28.793656662988084 0 0 0 0 3.7647923684188305 0
		 -31.844260444737046 13.291451198177965 14.526437023076681 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.2;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel4";
	rename -uid "E148B7C1-4E4E-5F73-C22F-459FF932BF9E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 2.0520273429232301 0 0 0 0 36.944705356201474 0 0 0 0 3.7647923684188305 0
		 -31.844260444737046 17.366975707078844 -13.782653904732056 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.2;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel5";
	rename -uid "B69C6C05-4A2A-5365-D8C4-EC97FA821358";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[0:15]" "e[20:23]" "e[28:31]" "e[36:39]" "e[44:47]" "e[52:55]" "e[60:63]";
	setAttr ".ix" -type "matrix" 0.80000000000000004 0 -9.7971743931788262e-17 0 0 1 0 0
		 1.2154337926102879e-16 0 0.99247700925403159 0 -6.2307564944182392 0 0.87370180667271491 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.2;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak8";
	rename -uid "A585BBAB-44C8-6D39-E086-C79C2C5609D0";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[5]" -type "float3" 0 0.34216937 0 ;
	setAttr ".tk[7]" -type "float3" 0 0.34216937 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.13687313 0 ;
	setAttr ".tk[11]" -type "float3" 0 0.13687313 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.13687313 0 ;
	setAttr ".tk[15]" -type "float3" 0 0.13687313 0 ;
	setAttr ".tk[22]" -type "float3" 0 0.34216937 0 ;
	setAttr ".tk[23]" -type "float3" 0 0.34216937 0 ;
	setAttr ".tk[26]" -type "float3" 0 0.13687313 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.13687313 0 ;
	setAttr ".tk[30]" -type "float3" 0 0.34216937 0 ;
	setAttr ".tk[31]" -type "float3" 0 0.34216937 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.58068615 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.58068615 0 ;
createNode polyBevel3 -n "polyBevel6";
	rename -uid "70273D7D-4609-3FA9-D0F7-11952AD26116";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[0:15]" "e[20:23]" "e[28:31]" "e[36:39]" "e[44:47]" "e[52:55]" "e[60:63]" "e[68:71]" "e[76:79]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.2;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak9";
	rename -uid "92DADCC9-49F3-A1E7-1D2B-E1BBE1FFAB59";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk";
	setAttr ".tk[1]" -type "float3" 0 8.1338711 0 ;
	setAttr ".tk[3]" -type "float3" 0 8.1338711 0 ;
	setAttr ".tk[5]" -type "float3" 0 8.1338711 0 ;
	setAttr ".tk[7]" -type "float3" 0 8.1338711 0 ;
	setAttr ".tk[8]" -type "float3" -3.9811187e-08 0 0 ;
	setAttr ".tk[9]" -type "float3" -3.9811187e-08 0 0 ;
	setAttr ".tk[10]" -type "float3" -3.9811187e-08 8.1338711 0 ;
	setAttr ".tk[11]" -type "float3" -3.9811187e-08 8.1338711 0 ;
	setAttr ".tk[12]" -type "float3" -1.9077837 0 0 ;
	setAttr ".tk[13]" -type "float3" -1.9077837 0 0 ;
	setAttr ".tk[14]" -type "float3" -1.9077837 8.8031473 0 ;
	setAttr ".tk[15]" -type "float3" -1.9077837 8.8031473 0 ;
	setAttr ".tk[16]" -type "float3" 1.9077837 0 0 ;
	setAttr ".tk[17]" -type "float3" 1.9077837 0 0 ;
	setAttr ".tk[18]" -type "float3" 1.9077837 8.8031473 0 ;
	setAttr ".tk[19]" -type "float3" 1.9077837 8.8031473 0 ;
	setAttr ".tk[20]" -type "float3" -1.3685542 0 0 ;
	setAttr ".tk[21]" -type "float3" -1.3685542 0 0 ;
	setAttr ".tk[22]" -type "float3" -1.3685542 8.1338711 0 ;
	setAttr ".tk[23]" -type "float3" -1.3685542 8.1338711 0 ;
	setAttr ".tk[24]" -type "float3" 1.3685542 0 0 ;
	setAttr ".tk[25]" -type "float3" 1.3685542 0 0 ;
	setAttr ".tk[26]" -type "float3" 1.3685542 8.1338711 0 ;
	setAttr ".tk[27]" -type "float3" 1.3685542 8.1338711 0 ;
	setAttr ".tk[28]" -type "float3" -3.37766 0 0 ;
	setAttr ".tk[29]" -type "float3" -3.37766 0 0 ;
	setAttr ".tk[30]" -type "float3" -3.37766 7.5043812 0 ;
	setAttr ".tk[31]" -type "float3" -3.37766 7.5043812 0 ;
	setAttr ".tk[32]" -type "float3" 3.37766 0 0 ;
	setAttr ".tk[33]" -type "float3" 3.37766 0 0 ;
	setAttr ".tk[34]" -type "float3" 3.37766 7.5043812 0 ;
	setAttr ".tk[35]" -type "float3" 3.37766 7.5043812 0 ;
	setAttr ".tk[36]" -type "float3" -3.165648 0 0 ;
	setAttr ".tk[37]" -type "float3" -3.165648 0 0 ;
	setAttr ".tk[38]" -type "float3" -3.165648 7.5043812 0 ;
	setAttr ".tk[39]" -type "float3" -3.165648 7.5043812 0 ;
	setAttr ".tk[40]" -type "float3" 3.1656485 0 0 ;
	setAttr ".tk[41]" -type "float3" 3.1656485 0 0 ;
	setAttr ".tk[42]" -type "float3" 3.1656485 7.5043812 0 ;
	setAttr ".tk[43]" -type "float3" 3.1656485 7.5043812 0 ;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "5EEF9D28-451F-023D-D28D-118521FB6CE7";
	setAttr ".dc" -type "componentList" 3 "f[0]" "f[2]" "f[4:5]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "AE61CE90-429B-B2EE-674A-C8ADDD3737C5";
	setAttr ".dc" -type "componentList" 15 "f[0]" "f[3:4]" "f[9:10]" "f[13:14]" "f[19]" "f[40]" "f[42]" "f[116]" "f[119]" "f[122]" "f[125]" "f[128]" "f[131]" "f[134]" "f[137]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "F63C4BF9-443D-CB0B-4D4B-BBA654DEA940";
	setAttr ".dc" -type "componentList" 18 "f[2]" "f[5]" "f[8]" "f[13]" "f[16]" "f[19]" "f[22]" "f[27]" "f[48]" "f[50]" "f[140]" "f[143]" "f[146]" "f[149]" "f[152]" "f[155]" "f[158]" "f[161]";
createNode polyCube -n "polyCube5";
	rename -uid "86B8C314-4FFC-A242-CC25-B5B9EA2A495D";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit23";
	rename -uid "9CF08EEC-4E83-FDD0-BB03-BEB05A331572";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483647 -2147483646 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit24";
	rename -uid "FDC60AE7-4411-5764-2674-0F8840B808D7";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483644 -2147483640 -2147483630 -2147483639 -2147483643 -2147483632 
		-2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak10";
	rename -uid "AD8C7F11-41EF-A630-7892-92BC4721CBB9";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk[0:17]" -type "float3"  -0.040106311 -0.040106311
		 -0.42093989 0.040106311 -0.040106311 -0.42093989 -0.040106311 0.040106311 -0.42093989
		 0.040106311 0.040106311 -0.42093989 -0.040106311 0.040106311 0.42093989 0.040106311
		 0.040106311 0.42093989 -0.040106311 -0.040106311 0.42093989 0.040106311 -0.040106311
		 0.42093989 0 0 -0.16826414 0 0 -0.16826414 0 0 0.16826414 0 0 0.16826414 0 0 -0.16826414
		 0 0 0.16826414 0 0 -0.055246271 0 0 0.16826414 0 0 -0.16826414 0 0 0.055246271;
createNode polySplit -n "polySplit25";
	rename -uid "993AB2AF-416D-8643-6B1D-2792FE31124E";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483636 -2147483618 -2147483635 -2147483634 -2147483620 -2147483633 
		-2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit26";
	rename -uid "CDA1FF2A-4817-D6DB-1DA9-D48C8867A250";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483648 -2147483617 -2147483647 -2147483646 -2147483621 -2147483645 
		-2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit27";
	rename -uid "C48E7AFA-4CFA-12D8-90F0-2490354603E7";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483644 -2147483627 -2147483594 -2147483626 -2147483606 -2147483625 
		-2147483643 -2147483610 -2147483632 -2147483598 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit28";
	rename -uid "CA08A1F1-4848-5E56-A305-F5A3ACB2F3C9";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483640 -2147483628 -2147483597 -2147483623 -2147483609 -2147483624 
		-2147483639 -2147483607 -2147483630 -2147483595 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyBevel3 -n "polyBevel7";
	rename -uid "B9F9AA53-4C14-B291-59B4-7799477509D8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 13 "e[0:15]" "e[20:21]" "e[23:24]" "e[32]" "e[34:35]" "e[37]" "e[44]" "e[46:47]" "e[49]" "e[56:57]" "e[61:62]" "e[76:77]" "e[81:82]";
	setAttr ".ix" -type "matrix" 10.658727704657204 0 0 0 0 10.658727704657204 0 0 0 0 3.5345302341516427 0
		 0 21.828086205301702 24.690143131539756 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak11";
	rename -uid "F0AB42FE-4DE2-71F2-15BB-26A524BF7C44";
	setAttr ".uopa" yes;
	setAttr -s 50 ".tk[0:49]" -type "float3"  0 0 -0.009630952 0 0 -0.009630952
		 0 0 -0.009630952 0 0 -0.009630952 0 0 0.0096310172 0 0 0.0096310172 0 0 0.0096310172
		 0 0 0.0096310172 0 0 -0.11396638 0 0 -0.11396638 0 0 0.11396657 0 0 0.11396657 0
		 0 -0.11396638 0 0 0.11396657 0 0 0.073339626 0 0 0.11396657 0 0 -0.11396646 0 0 -0.073339731
		 0 0 -0.025021235 0 0 -0.030583227 0 0 -0.025021235 0 0 0.025021132 0 0 0.030583227
		 0 0 0.025021132 0 0 -0.025021235 0 0 -0.030583227 0 0 -0.025021235 0 0 0.025021132
		 0 0 0.030583227 0 0 0.025021132 0 0 -0.025021235 0 0 0.025021132 0 0 -0.060006894
		 0 0 0.030583227 0 0 -0.060006894 0 0 0.025021132 0 0 -0.025021259 0 0 0.060006894
		 0 0 -0.030583227 0 0 0.060006894 0 0 0.025021132 0 0 -0.025021235 0 0 0.060006894
		 0 0 -0.030583227 0 0 0.060006894 0 0 -0.025021259 0 0 0.025021132 0 0 -0.060006894
		 0 0 0.030583227 0 0 -0.060006894;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "CCF39BF2-4B14-2250-1CE3-748A705C1960";
	setAttr ".sa" 8;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyBevel3 -n "polyBevel8";
	rename -uid "B82E8F5B-4632-259B-F299-F38DF50F3C8C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:15]";
	setAttr ".ix" -type "matrix" 2.4140046262587744 0 0 0 0 0 7.1827070641498514 0 0 -2.4140046262587744 0 0
		 0 0 31.356904174950671 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.1;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polySplit -n "polySplit29";
	rename -uid "07100CE2-4266-8752-DB65-2792C88FF079";
	setAttr -s 9 ".e[0:8]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 9 ".d[0:8]"  -2147483600 -2147483599 -2147483593 -2147483594 -2147483595 -2147483596 
		-2147483597 -2147483598 -2147483600;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit30";
	rename -uid "20A21EFC-4258-04F8-B0E8-E391C65F95FA";
	setAttr -s 9 ".e[0:8]"  0.1 0.89999998 0.89999998 0.89999998 0.89999998
		 0.89999998 0.89999998 0.89999998 0.1;
	setAttr -s 9 ".d[0:8]"  -2147483599 -2147483576 -2147483569 -2147483570 -2147483571 -2147483572 
		-2147483573 -2147483574 -2147483599;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit31";
	rename -uid "F53C0EA0-41AA-244D-C372-D1B84B48392E";
	setAttr -s 9 ".e[0:8]"  0.1 0.89999998 0.1 0.1 0.1 0.1 0.1 0.1 0.1;
	setAttr -s 9 ".d[0:8]"  -2147483600 -2147483575 -2147483593 -2147483594 -2147483595 -2147483596 
		-2147483597 -2147483598 -2147483600;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge6";
	rename -uid "77770114-435D-E309-13C7-B09AB693D52A";
	setAttr ".ics" -type "componentList" 1 "e[80:87]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "B1BDBE35-4693-618E-650C-F88FB01AEAEC";
	setAttr ".uopa" yes;
	setAttr -s 38 ".tk[0:37]" -type "float3"  5.3751887e-09 -0.022347197
		 0 5.3751887e-09 0.022347197 0 -0.068754502 0 0.068754509 -0.061879732 0 0.061879735
		 5.795572e-09 0 0.097233534 9.9994066e-09 0 0.087511182 0.068754509 0 0.068754502
		 0.061879754 0 0.061879754 0.097233579 0 6.7261334e-09 0.087511159 0 3.3630667e-09
		 0.068754524 0 -0.068754502 0.061879739 0 -0.061879743 1.2521708e-08 0 -0.097233571
		 1.5065167e-08 0 -0.087511174 -0.068754509 0 -0.068754487 -0.061879743 0 -0.061879754
		 -0.097233541 0 -6.7261334e-09 -0.087511167 0 2.5434614e-09 -0.068754517 0 0.068754509
		 -0.061879747 0 0.061879739 5.795572e-09 0 0.097233556 1.2521708e-08 0 0.087511167
		 0.068754509 0 0.068754487 0.061879754 0 0.061879754 0.097233556 0 -6.7261334e-09
		 0.087511182 0 6.7261334e-09 0.068754531 0 -0.068754502 0.061879732 0 -0.061879743
		 1.9247842e-08 0 -0.097233549 -8.476305e-09 0 -0.087511167 -0.068754502 0 -0.068754524
		 -0.061879735 0 -0.061879747 -0.097233556 0 2.0178403e-08 -0.087511182 0 -7.54574e-09
		 1.4901161e-08 0 -7.4505806e-09 1.4901161e-08 0 3.7252903e-09 -1.4901161e-08 0 0 -7.4505806e-09
		 0 -1.110223e-16;
createNode deleteComponent -n "deleteComponent4";
	rename -uid "00AF989C-4AB8-D49B-8070-2A95B921E65F";
	setAttr ".dc" -type "componentList" 7 "e[56]" "e[58]" "e[60]" "e[62]" "e[65:66]" "e[68]" "e[70]";
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "CF761520-4560-0E1C-3DB2-EC803FD798A1";
	setAttr ".ics" -type "componentList" 1 "vtx[86:87]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "A99408DE-4482-C07F-FF37-F5A7F91C4F1B";
	setAttr ".ics" -type "componentList" 1 "vtx[87:88]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak13";
	rename -uid "DD6F9C74-4C84-1F40-0907-52BB24F061ED";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[87:88]" -type "float3"  0.0062354207 -0.00034284592
		 -0.02444458 -0.0062354803 0.00034284592 0.02444458;
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "66451C09-40AC-774A-7D9A-E4ABF80ADBCB";
	setAttr ".ics" -type "componentList" 1 "vtx[45:47]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak14";
	rename -uid "4BCFAACF-4704-107C-D183-CB89E6A576A7";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[45:47]" -type "float3"  -0.0093283653 0.0048652291
		 0.015510559 0.0047668219 -0.0093829036 0.015188217 0.0045616031 0.0045176148 -0.030698776;
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "C321E2B5-44A0-420B-EB66-18B157EB0C56";
	setAttr ".ics" -type "componentList" 1 "vtx[62:63]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak15";
	rename -uid "48772AC6-44B6-B5A9-705B-C1872F129AA5";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[62:63]" -type "float3"  0.00034362078 -0.0062170029
		 0.024430037 -0.00034362078 0.0062170029 -0.024430037;
createNode polyMergeVert -n "polyMergeVert5";
	rename -uid "B7EC9768-4DCB-E022-CC52-6EADF11368D6";
	setAttr ".ics" -type "componentList" 1 "vtx[60:61]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak16";
	rename -uid "1AFCEB9F-4A37-A7F3-7D93-8B8201959D95";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[60:61]" -type "float3"  -1.6653345e-16 -0.0062479973
		 0.024292469 -1.6653345e-16 0.0062479973 -0.024292469;
createNode polyMergeVert -n "polyMergeVert6";
	rename -uid "ECCF09DE-4F5B-F218-C841-29938EC42800";
	setAttr ".ics" -type "componentList" 1 "vtx[26:27]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak17";
	rename -uid "A227354F-4A46-7809-0F7C-98B2CBF8DE5D";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[26:27]" -type "float3"  -0.00034087896 -0.0061987638
		 0.024300814 0.00034087896 0.0061987638 -0.024300814;
createNode polyMergeVert -n "polyMergeVert7";
	rename -uid "C3E977C6-4DAA-7EF3-8ECB-349E48B303B8";
	setAttr ".ics" -type "componentList" 1 "vtx[23:25]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak18";
	rename -uid "9F999832-40A3-C198-C905-679C3990B053";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[23:26]" -type "float3"  -0.0045140982 0.0045141578
		 -0.030806225 -0.0048617125 -0.0093758702 0.015403111 0.0093758702 0.0048617721 0.015403111
		 0 0 0;
createNode polyMergeVert -n "polyMergeVert8";
	rename -uid "7631DAB1-462A-EDC3-FDDE-D4B0DB2A00D0";
	setAttr ".ics" -type "componentList" 1 "vtx[25:27]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak19";
	rename -uid "F2F61270-44B9-D338-3657-FC9AF1ED9591";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[25:27]" -type "float3"  0.0093283653 0.0048652291
		 -0.015509922 -0.0047668219 -0.0093829036 -0.015189011 -0.0045616031 0.0045176148
		 0.030698936;
createNode polyMergeVert -n "polyMergeVert9";
	rename -uid "ACB63583-4A2A-AFD6-FECE-67BAC5E17C9A";
	setAttr ".ics" -type "componentList" 1 "vtx[26:27]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak20";
	rename -uid "6D625EB7-4483-46D7-8BBE-3F8C8A89B360";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[26:27]" -type "float3"  -0.00034362078 -0.0062170029
		 -0.024430037 0.00034362078 0.0062170029 0.024430037;
createNode polyMergeVert -n "polyMergeVert10";
	rename -uid "B696D152-4387-0035-8AAC-1EA639F5F57C";
	setAttr ".ics" -type "componentList" 1 "vtx[56:57]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak21";
	rename -uid "6CBBCAB5-4412-52C8-DD34-4ABB6AFC9D38";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[56:57]" -type "float3"  -2.220446e-16 -0.0062479973
		 -0.024292469 -2.220446e-16 0.0062479973 0.024292469;
createNode polyMergeVert -n "polyMergeVert11";
	rename -uid "46C7C7E6-4D6B-4040-6D72-1D942A49B363";
	setAttr ".ics" -type "componentList" 1 "vtx[57:58]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak22";
	rename -uid "561D2D90-43BA-FE6B-76D1-EDA636A257F9";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[57:58]" -type "float3"  0.00034087896 -0.0061987638
		 -0.024300337 -0.00034087896 0.0061987638 0.024300337;
createNode polyMergeVert -n "polyMergeVert12";
	rename -uid "4D3A7320-4F2D-01C5-0371-20BF47FA38AC";
	setAttr ".ics" -type "componentList" 1 "vtx[40:42]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak23";
	rename -uid "B5085DBF-4E66-65AE-6C8B-5991D36FC6D7";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[40:42]" -type "float3"  0.0048652291 -0.0093284249
		 -0.015509922 -0.0093829036 0.0047668815 -0.015189011 0.0045176148 0.0045616031 0.030698936;
createNode polyMergeVert -n "polyMergeVert13";
	rename -uid "9619FC95-428E-1F94-1E18-A89567E7305D";
	setAttr ".ics" -type "componentList" 1 "vtx[43:44]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak24";
	rename -uid "D3BDDAF3-41DC-7A91-5E5B-1E8FA274C089";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[43:44]" -type "float3"  -0.0062170029 0.00034356117
		 -0.024430037 0.0062170029 -0.00034356117 0.024430037;
createNode polyMergeVert -n "polyMergeVert14";
	rename -uid "89A0AA62-4023-2F0B-8440-829E5DE56B5F";
	setAttr ".ics" -type "componentList" 1 "vtx[67:68]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak25";
	rename -uid "91B388F9-4536-DA1E-AA53-0FBC478B7337";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[67:68]" -type "float3"  -0.0062479973 -4.4408921e-16
		 -0.024292469 0.0062480271 -4.4408921e-16 0.024292469;
createNode polyMergeVert -n "polyMergeVert15";
	rename -uid "4D9CB679-4605-7D2B-30F0-DFB4079BA4B0";
	setAttr ".ics" -type "componentList" 1 "vtx[68:69]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak26";
	rename -uid "57AF7D70-4498-4F8F-B6A3-638BD2E533D6";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[68:69]" -type "float3"  0.0062354207 0.00034290552
		 0.02444458 -0.0062354803 -0.00034290552 -0.02444458;
createNode polyMergeVert -n "polyMergeVert16";
	rename -uid "89DB3E7B-44C6-CBC5-759B-36B4EF2C46A4";
	setAttr ".ics" -type "componentList" 1 "vtx[44:46]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak27";
	rename -uid "3C3AB13D-48FD-B735-F12A-D3890B1294BF";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[44:46]" -type "float3"  -0.0093283653 -0.0048652887
		 -0.015509922 0.0047668219 0.009382844 -0.015189011 0.0045616031 -0.0045175552 0.030698936;
createNode polyMergeVert -n "polyMergeVert17";
	rename -uid "049DB6AB-429A-3573-00E9-4C86408F44FB";
	setAttr ".ics" -type "componentList" 1 "vtx[34:36]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak28";
	rename -uid "78FAF301-45AF-C889-82EE-1AB2119F43ED";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[34:36]" -type "float3"  0.0048652291 0.0093283653
		 0.015510559 -0.0093829036 -0.0047668219 0.015188217 0.0045176148 -0.0045615435 -0.030698776;
createNode polyMergeVert -n "polyMergeVert18";
	rename -uid "39C179F9-4BAE-E9EB-BE89-DEB35D374BF6";
	setAttr ".ics" -type "componentList" 1 "vtx[35:36]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak29";
	rename -uid "131A7E92-4215-A493-970A-1B88F79280A2";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[35:36]" -type "float3"  -0.0062170029 -0.00034362078
		 0.024430037 0.0062170029 0.00034362078 -0.024430037;
createNode polyMergeVert -n "polyMergeVert19";
	rename -uid "6FD36712-4119-04CF-F19F-66A12D0D2A72";
	setAttr ".ics" -type "componentList" 1 "vtx[38:39]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak30";
	rename -uid "0360B506-477B-0DAB-8768-10836E4D7EDE";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[38:39]" -type "float3"  0.0061988235 0.00034081936
		 -0.024300337 -0.0061988235 -0.00034081936 0.024300337;
createNode polyMergeVert -n "polyMergeVert20";
	rename -uid "7B6DE540-48D6-1582-95CF-2CB4D89429B2";
	setAttr ".ics" -type "componentList" 1 "vtx[57:58]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak31";
	rename -uid "865327FF-4D6A-C931-E216-90BFB62E25D8";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[57:58]" -type "float3"  -0.0062480271 -4.4408921e-16
		 0.024292469 0.0062479973 -4.4408921e-16 -0.024292469;
createNode polyMergeVert -n "polyMergeVert21";
	rename -uid "2989F549-4AEB-1837-00C0-D2839105CA67";
	setAttr ".ics" -type "componentList" 1 "vtx[58:59]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak32";
	rename -uid "B68E297D-43E6-F0E1-3DA6-FBA8AB76A623";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[58:59]" -type "float3"  0.0062170029 -0.00034362078
		 -0.024430037 -0.0062170029 0.00034362078 0.024430037;
createNode polyMergeVert -n "polyMergeVert22";
	rename -uid "ED278209-4AD6-9D1D-56F1-7399925C0E2C";
	setAttr ".ics" -type "componentList" 1 "vtx[27:29]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak33";
	rename -uid "93A1D3A0-469B-DEC7-FCB0-1AA61F9F21D4";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[27:29]" -type "float3"  -0.0045140982 -0.0045140386
		 0.030805904 -0.0048617125 0.0093758702 -0.015402954 0.0093758702 -0.0048617721 -0.015402954;
createNode polyMergeVert -n "polyMergeVert23";
	rename -uid "B9AB3267-4CCA-2823-DFD2-6EB591C63124";
	setAttr ".ics" -type "componentList" 1 "vtx[18:20]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak34";
	rename -uid "C371E7E7-4881-646D-463A-E0BF6D55C8F1";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[18:20]" -type "float3"  -0.0047703385 0.0093353391
		 0.015295666 -0.0045651197 -0.0045650601 -0.030591328 0.0093353987 -0.0047703385 0.015295666;
createNode polyMergeVert -n "polyMergeVert24";
	rename -uid "2E8B3321-4B67-FCA8-C1FE-B3B8BA840BB5";
	setAttr ".ics" -type "componentList" 1 "vtx[28:29]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak35";
	rename -uid "D06E7DD9-4FE6-7AB3-BE6D-829F2C37923B";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[28:29]" -type "float3"  0.0061988235 -0.00034093857
		 0.024300814 -0.0061988235 0.00034093857 -0.024300814;
createNode polyMergeVert -n "polyMergeVert25";
	rename -uid "0A1BE4D6-42A3-D9A9-DBDA-498F0922CD6F";
	setAttr ".ics" -type "componentList" 1 "vtx[48:49]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak36";
	rename -uid "437EB398-4B44-22F4-4540-2CAEBCD989CB";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[48:49]" -type "float3"  -0.0062480271 0 -0.024292469
		 0.0062479973 0 0.024292469;
createNode polyMergeVert -n "polyMergeVert26";
	rename -uid "2BFEEAB5-46C8-F8A2-DD20-7691C393263E";
	setAttr ".ics" -type "componentList" 1 "vtx[49:50]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak37";
	rename -uid "248BAB22-42A2-8D6D-3B8A-2EB304412A05";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[49:50]" -type "float3"  0.0062170029 0.00034356117
		 0.024430037 -0.0062170029 -0.00034356117 -0.024430037;
createNode polyMergeVert -n "polyMergeVert27";
	rename -uid "7870CE61-4E54-3D1B-5604-6B877DBEC86B";
	setAttr ".ics" -type "componentList" 1 "vtx[19:20]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak38";
	rename -uid "75F8F538-4C81-67A9-34DA-AEBB29A34D51";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[19:20]" -type "float3"  0.00034016371 -0.0062173009
		 -0.024315357 -0.00034019351 0.0062173009 0.024315357;
createNode polyMergeVert -n "polyMergeVert28";
	rename -uid "5492F71A-425F-2A50-3922-189BCD5F16B8";
	setAttr ".ics" -type "componentList" 1 "vtx[25:26]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak39";
	rename -uid "59DE7269-4CF7-72A5-60B3-8C89CF7EE5E1";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[25:26]" -type "float3"  -0.00034087896 0.0061988235
		 -0.024300337 0.00034087896 -0.0061988235 0.024300337;
createNode polyMergeVert -n "polyMergeVert29";
	rename -uid "10CC78B7-48A8-E059-6737-9DAAA22E0EDB";
	setAttr ".ics" -type "componentList" 1 "vtx[34:35]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak40";
	rename -uid "91EC2124-41D2-CBC1-9004-A8A95A9B8190";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[34:35]" -type "float3"  -1.6653345e-16 0.0062480569
		 0.024292469 -1.6653345e-16 -0.0062480569 -0.024292469;
createNode polyMergeVert -n "polyMergeVert30";
	rename -uid "3041E3D6-484B-DF98-6C09-92826568C87E";
	setAttr ".ics" -type "componentList" 1 "vtx[41:42]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak41";
	rename -uid "200A0AEB-427A-FE8C-811C-97B1FABFD3F3";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[41:42]" -type "float3"  -5.5511151e-17 0.0062480569
		 -0.024292469 -5.5511151e-17 -0.0062480569 0.024292469;
createNode polyMergeVert -n "polyMergeVert31";
	rename -uid "E9359366-4525-500C-EA69-1A93903B0BCA";
	setAttr ".ics" -type "componentList" 1 "vtx[35:36]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak42";
	rename -uid "640987F0-417C-7D7C-974E-EE8AF5B2E599";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[35:36]" -type "float3"  -0.00034290552 -0.0062354207
		 -0.02444458 0.00034290552 0.0062354207 0.02444458;
createNode polyMergeVert -n "polyMergeVert32";
	rename -uid "7F9DB2EB-4CB1-E66A-CE99-6DB688721D6C";
	setAttr ".ics" -type "componentList" 1 "vtx[41:42]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak43";
	rename -uid "FAE8F18D-4861-D1B8-A082-C980ADC4ED10";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[41:42]" -type "float3"  0.00034362078 0.0062170029
		 -0.024430037 -0.00034362078 -0.0062170029 0.024430037;
createNode polyBevel3 -n "polyBevel9";
	rename -uid "11A0D364-49CC-DECC-DB28-5CB8B78F4D6F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[24:59]";
	setAttr ".ix" -type "matrix" 7.4144999032268153 0.20531973076455054 8.8359400906040371 0
		 3.7477449742158759 9.3942636386855796 -3.3631376090098706 0 -2.4058340983096573 1.6686327837324904 1.9800330554300818 0
		 9.5480520998974203 22.401206509083323 -3.0549105735569615 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyAutoProj -n "polyAutoProj1";
	rename -uid "DE90E87B-4CAC-18AD-110E-2D80114657D3";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".s" -type "double3" 63.270557403564453 63.270557403564453 63.270557403564453 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "0F0D5AAE-4880-E24A-4F1C-5298BBECD3C8";
	setAttr ".uopa" yes;
	setAttr -s 104 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[1]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[2]" -type "float2" 0.52188134 -8.9406967e-08 ;
	setAttr ".uvtk[3]" -type "float2" 0.52188134 -8.9406967e-08 ;
	setAttr ".uvtk[4]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[5]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[6]" -type "float2" 0.52188134 -8.9406967e-08 ;
	setAttr ".uvtk[7]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[8]" -type "float2" 0.52188134 -2.682209e-07 ;
	setAttr ".uvtk[9]" -type "float2" 0.52188134 -2.682209e-07 ;
	setAttr ".uvtk[10]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[11]" -type "float2" 0.52188134 -2.682209e-07 ;
	setAttr ".uvtk[12]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[13]" -type "float2" 0.52188134 -8.9406967e-08 ;
	setAttr ".uvtk[14]" -type "float2" 0.52188134 -4.4703484e-07 ;
	setAttr ".uvtk[15]" -type "float2" 0.52188134 -4.4703484e-07 ;
	setAttr ".uvtk[16]" -type "float2" 0.52188134 1.4901161e-07 ;
	setAttr ".uvtk[17]" -type "float2" 0.52188134 -4.4703484e-07 ;
	setAttr ".uvtk[18]" -type "float2" 0.52188134 -2.682209e-07 ;
	setAttr ".uvtk[19]" -type "float2" 0.52188134 -6.8545341e-07 ;
	setAttr ".uvtk[20]" -type "float2" 0.52188134 -6.8545341e-07 ;
	setAttr ".uvtk[21]" -type "float2" 0.52188134 -6.8545341e-07 ;
	setAttr ".uvtk[22]" -type "float2" 0.52188134 -4.4703484e-07 ;
	setAttr ".uvtk[23]" -type "float2" 0.52188134 -1.0430813e-06 ;
	setAttr ".uvtk[24]" -type "float2" 0.52188134 -1.0430813e-06 ;
	setAttr ".uvtk[25]" -type "float2" 0.52188134 -1.0430813e-06 ;
	setAttr ".uvtk[26]" -type "float2" 0.52188134 -6.8545341e-07 ;
	setAttr ".uvtk[27]" -type "float2" 0.52188134 -1.3411045e-06 ;
	setAttr ".uvtk[28]" -type "float2" 0.52188134 -1.3411045e-06 ;
	setAttr ".uvtk[29]" -type "float2" 0.52188134 -1.3411045e-06 ;
	setAttr ".uvtk[30]" -type "float2" 0.52188134 -1.0430813e-06 ;
	setAttr ".uvtk[31]" -type "float2" 0.52188134 -1.5646219e-06 ;
	setAttr ".uvtk[32]" -type "float2" 0.52188134 -1.5646219e-06 ;
	setAttr ".uvtk[33]" -type "float2" 0.52188134 -1.5646219e-06 ;
	setAttr ".uvtk[34]" -type "float2" 0.52188134 -1.3411045e-06 ;
	setAttr ".uvtk[35]" -type "float2" 0.52188134 -1.7881393e-06 ;
	setAttr ".uvtk[36]" -type "float2" 0.52188134 -1.7881393e-06 ;
	setAttr ".uvtk[37]" -type "float2" 0.52188134 -1.7881393e-06 ;
	setAttr ".uvtk[38]" -type "float2" 0.52188134 -1.5646219e-06 ;
	setAttr ".uvtk[39]" -type "float2" 0.52188134 -2.0414591e-06 ;
	setAttr ".uvtk[40]" -type "float2" 0.52188134 -2.0414591e-06 ;
	setAttr ".uvtk[41]" -type "float2" 0.52188134 -2.0414591e-06 ;
	setAttr ".uvtk[42]" -type "float2" 0.52188134 -1.7881393e-06 ;
	setAttr ".uvtk[43]" -type "float2" 0.52188134 -2.176268e-06 ;
	setAttr ".uvtk[44]" -type "float2" 0.52188134 -2.176268e-06 ;
	setAttr ".uvtk[45]" -type "float2" 0.52188134 -2.176268e-06 ;
	setAttr ".uvtk[46]" -type "float2" 0.52188134 -2.0414591e-06 ;
	setAttr ".uvtk[47]" -type "float2" 0.52188134 -2.1676533e-06 ;
	setAttr ".uvtk[48]" -type "float2" 0.52188134 -2.1676533e-06 ;
	setAttr ".uvtk[49]" -type "float2" 0.52188134 -2.176268e-06 ;
	setAttr ".uvtk[50]" -type "float2" 0.52188134 -2.1676533e-06 ;
	setAttr ".uvtk[51]" -type "float2" 0.52188134 -2.1676533e-06 ;
	setAttr ".uvtk[104]" -type "float2" 1.0162691 1.4901161e-07 ;
	setAttr ".uvtk[105]" -type "float2" 1.016269 1.4901161e-07 ;
	setAttr ".uvtk[106]" -type "float2" 1.016269 -8.9406967e-08 ;
	setAttr ".uvtk[107]" -type "float2" 1.0162691 -8.9406967e-08 ;
	setAttr ".uvtk[108]" -type "float2" 1.0162691 1.4901161e-07 ;
	setAttr ".uvtk[109]" -type "float2" 1.016269 1.4901161e-07 ;
	setAttr ".uvtk[110]" -type "float2" 1.016269 -2.682209e-07 ;
	setAttr ".uvtk[111]" -type "float2" 1.0162691 -2.682209e-07 ;
	setAttr ".uvtk[112]" -type "float2" 1.01627 -8.9406967e-08 ;
	setAttr ".uvtk[113]" -type "float2" 1.01627 1.4901161e-07 ;
	setAttr ".uvtk[114]" -type "float2" 1.01627 1.4901161e-07 ;
	setAttr ".uvtk[115]" -type "float2" 1.016269 -4.4703484e-07 ;
	setAttr ".uvtk[116]" -type "float2" 1.0162691 -4.4703484e-07 ;
	setAttr ".uvtk[117]" -type "float2" 1.0162702 -2.682209e-07 ;
	setAttr ".uvtk[118]" -type "float2" 1.01627 1.4901161e-07 ;
	setAttr ".uvtk[119]" -type "float2" 1.01627 -8.9406967e-08 ;
	setAttr ".uvtk[120]" -type "float2" 1.01627 1.4901161e-07 ;
	setAttr ".uvtk[121]" -type "float2" 1.016269 -6.8545341e-07 ;
	setAttr ".uvtk[122]" -type "float2" 1.0162691 -6.8545341e-07 ;
	setAttr ".uvtk[123]" -type "float2" 1.0162702 -4.4703484e-07 ;
	setAttr ".uvtk[124]" -type "float2" 1.0162702 -2.682209e-07 ;
	setAttr ".uvtk[125]" -type "float2" 1.016269 -1.0430813e-06 ;
	setAttr ".uvtk[126]" -type "float2" 1.0162691 -1.0430813e-06 ;
	setAttr ".uvtk[127]" -type "float2" 1.0162704 -7.4505806e-07 ;
	setAttr ".uvtk[128]" -type "float2" 1.0162702 -4.4703484e-07 ;
	setAttr ".uvtk[129]" -type "float2" 1.016269 -1.3411045e-06 ;
	setAttr ".uvtk[130]" -type "float2" 1.0162691 -1.3411045e-06 ;
	setAttr ".uvtk[131]" -type "float2" 1.0162704 -1.0430813e-06 ;
	setAttr ".uvtk[132]" -type "float2" 1.0162703 -6.8545341e-07 ;
	setAttr ".uvtk[133]" -type "float2" 1.016269 -1.5646219e-06 ;
	setAttr ".uvtk[134]" -type "float2" 1.0162691 -1.5646219e-06 ;
	setAttr ".uvtk[135]" -type "float2" 1.0162704 -1.3411045e-06 ;
	setAttr ".uvtk[136]" -type "float2" 1.0162704 -1.0430813e-06 ;
	setAttr ".uvtk[137]" -type "float2" 1.016269 -1.847744e-06 ;
	setAttr ".uvtk[138]" -type "float2" 1.0162691 -1.847744e-06 ;
	setAttr ".uvtk[139]" -type "float2" 1.0162702 -1.5944242e-06 ;
	setAttr ".uvtk[140]" -type "float2" 1.0162703 -1.3411045e-06 ;
	setAttr ".uvtk[141]" -type "float2" 1.016269 -1.989305e-06 ;
	setAttr ".uvtk[142]" -type "float2" 1.0162691 -1.989305e-06 ;
	setAttr ".uvtk[143]" -type "float2" 1.0162702 -1.8179417e-06 ;
	setAttr ".uvtk[144]" -type "float2" 1.0162702 -1.5646219e-06 ;
	setAttr ".uvtk[145]" -type "float2" 1.016269 -2.1876767e-06 ;
	setAttr ".uvtk[146]" -type "float2" 1.0162691 -2.1876767e-06 ;
	setAttr ".uvtk[147]" -type "float2" 1.01627 -1.989305e-06 ;
	setAttr ".uvtk[148]" -type "float2" 1.0162702 -1.847744e-06 ;
	setAttr ".uvtk[149]" -type "float2" 1.0162691 -2.1791784e-06 ;
	setAttr ".uvtk[150]" -type "float2" 1.016269 -2.1791784e-06 ;
	setAttr ".uvtk[151]" -type "float2" 1.01627 -2.1876767e-06 ;
	setAttr ".uvtk[152]" -type "float2" 1.01627 -1.989305e-06 ;
	setAttr ".uvtk[153]" -type "float2" 1.01627 -2.1791784e-06 ;
	setAttr ".uvtk[154]" -type "float2" 1.01627 -2.1520536e-06 ;
	setAttr ".uvtk[155]" -type "float2" 1.01627 -2.1791784e-06 ;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "E5945390-496F-E882-BB0B-A4A1CE590FEE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 24 "e[1]" "e[12]" "e[27]" "e[42]" "e[53]" "e[68]" "e[107]" "e[118]" "e[149]" "e[156]" "e[184]" "e[191]" "e[252]" "e[254]" "e[258:259]" "e[263:264]" "e[268:269]" "e[272:273]" "e[276:277]" "e[282]" "e[284]" "e[288:289]" "e[294]" "e[296]";
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "374C0DF7-4B2F-037E-91E1-EEBA11B46AB8";
	setAttr ".uopa" yes;
	setAttr -s 182 ".uvtk[0:181]" -type "float2" -0.72002757 -0.31381643 -0.71924359
		 -0.31381643 -0.71924359 -0.28921875 -0.72002757 -0.28921875 -0.71924359 -0.31460047
		 -0.72002757 -0.31460047 -0.71297115 -0.28921875 -0.71297115 -0.31381643 -0.71924359
		 -0.26451707 -0.72002757 -0.26451707 -0.71297115 -0.31460047 -0.71297115 -0.26451707
		 -0.71218723 -0.31381628 -0.71218723 -0.28921854 -0.71924359 -0.23464188 -0.72002757
		 -0.23464188 -0.71218723 -0.31460029 -0.71297115 -0.23464188 -0.71218723 -0.26451692
		 -0.71924359 -0.19874594 -0.72002757 -0.19874594 -0.71297115 -0.19874594 -0.71218723
		 -0.23464179 -0.71924359 -0.15810759 -0.72002757 -0.15810759 -0.71297115 -0.15810759
		 -0.71218723 -0.19874585 -0.71924359 -0.11746928 -0.72002757 -0.11746928 -0.71297115
		 -0.11746928 -0.71218723 -0.15810759 -0.71924359 -0.081573367 -0.72002757 -0.081573367
		 -0.71297115 -0.081573367 -0.71218723 -0.11746933 -0.71924359 -0.051698141 -0.72002757
		 -0.051698126 -0.71297115 -0.051698141 -0.71218723 -0.081573471 -0.71924359 -0.026996497
		 -0.72002757 -0.026996501 -0.71297115 -0.026996497 -0.71218723 -0.05169829 -0.71924359
		 -0.00239873 -0.72002757 -0.0023987261 -0.71297115 -0.00239873 -0.71218723 -0.026996691
		 -0.71924359 -0.0016147611 -0.72002757 -0.0016147572 -0.71218723 -0.0023989533 -0.71297127
		 -0.0016147611 -0.71218723 -0.0016149831 0.48270315 -0.30445498 0.48346353 -0.30445457
		 0.48346353 -0.2805942 0.48270315 -0.2805942 0.48271161 -0.30521464 0.48346353 -0.30521464
		 0.48346353 -0.25663352 0.48270315 -0.25663352 0.47661889 -0.2805942 0.47661889 -0.30445498
		 0.47661889 -0.30521464 0.48346353 -0.22765446 0.48270315 -0.22765446 0.47661889 -0.25663352
		 0.47585833 -0.30445457 0.47585833 -0.2805942 0.47585833 -0.30521464 0.48346353 -0.19283518
		 0.48270315 -0.19283518 0.47661889 -0.22765446 0.47585833 -0.25663352 0.48346353 -0.15341577
		 0.48270315 -0.15341577 0.47661889 -0.19283518 0.47585833 -0.22765446 0.48346353 -0.1139964
		 0.48270315 -0.1139964 0.47661889 -0.15341577 0.47585833 -0.19283518 0.48346353 -0.079177201
		 0.48270315 -0.079177201 0.47661889 -0.1139964 0.47585833 -0.15341577 0.48346353 -0.050198056
		 0.48270315 -0.050198056 0.47661889 -0.079177201 0.47585833 -0.1139964 0.48346353
		 -0.026237372 0.48270315 -0.026237372 0.47661889 -0.050198056 0.47585833 -0.079177201
		 0.48346353 -0.0023769657 0.48270315 -0.0023766039 0.47661889 -0.026237372 0.47585833
		 -0.050198056 0.48270315 -0.0016169408 0.48346353 -0.0016169408 0.47661889 -0.0023766039
		 0.47585833 -0.026237372 0.47661024 -0.0016169408 0.47585833 -0.0023769657 0.47585833
		 -0.0016169408 -0.72081161 -0.31381649 -0.72081161 -0.28921875 -0.72081161 -0.31460053
		 -0.72081161 -0.26451713 -0.86036539 -0.28912604 -0.85922045 -0.31381649 -0.8591944
		 -0.31460053 -0.72081161 -0.23464185 -0.86513817 -0.26430422 -0.8600052 -0.31381693
		 -0.86114585 -0.28921875 -0.85996884 -0.31460053 -0.72081161 -0.19874597 -0.87635475
		 -0.23431587 -0.86589551 -0.26451713 -0.72081161 -0.15810759 -0.8958028 -0.19851127
		 -0.87706983 -0.23464185 -0.72081161 -0.11746927 -0.89977705 -0.15810759 -0.8965674
		 -0.19874597 -0.72081161 -0.081573352 -0.8958028 -0.11770396 -0.90056473 -0.15810759
		 -0.72081161 -0.051698111 -0.87635475 -0.08189936 -0.8965674 -0.11746928 -0.72081161
		 -0.026996505 -0.86513817 -0.051910944 -0.87706983 -0.081573352 -0.72081161 -0.0023987223
		 -0.86036539 -0.027089152 -0.86589551 -0.051698111 -0.72081161 -0.0016147534 -0.85922045
		 -0.0023987254 -0.86114585 -0.026996505 -0.85918337 -0.0016147568 -0.8600052 -0.0023983107
		 -0.85996884 -0.0016147568 -0.71140325 -0.3138161 -0.71140325 -0.28921837 -0.71140325
		 -0.31460002 -0.57299471 -0.3138161 -0.57184976 -0.28912574 -0.71140325 -0.26451677
		 -0.57303172 -0.31460002 -0.57220995 -0.3138164 -0.5710693 -0.28921837 -0.5670771
		 -0.26430395 -0.71140325 -0.23464167 -0.57224631 -0.31460002 -0.5663197 -0.26451677
		 -0.55586052 -0.23431566 -0.71140325 -0.19874579 -0.55514538 -0.23464167 -0.53641242
		 -0.19851109 -0.71140325 -0.15810759 -0.53564787 -0.19874579 -0.53243822 -0.15810759
		 -0.71140325 -0.11746939 -0.53165054 -0.15810759 -0.53641242 -0.11770406 -0.71140325
		 -0.081573591 -0.53564787 -0.11746939 -0.55586052 -0.081899568 -0.71140325 -0.051698431
		 -0.55514538 -0.081573606 -0.5670771 -0.051911265 -0.71140325 -0.026996888 -0.5663197
		 -0.051698439 -0.57184976 -0.027089532 -0.71140325 -0.0023991773 -0.5710693 -0.026996892
		 -0.57299471 -0.0023991833 -0.71140325 -0.0016152052 -0.57220995 -0.0023987358 -0.57302082
		 -0.001615211 -0.57224631 -0.001615211;
createNode polyAutoProj -n "polyAutoProj2";
	rename -uid "3A1513EF-47D2-DF08-FEA5-ED922CDEBBAB";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:119]";
	setAttr ".ix" -type "matrix" 0.80000000000000004 0 0 0 0 1 0 0 0 0 0.99247700925403159 0
		 -6.230756494418241 0 0.87370180667271669 1;
	setAttr ".s" -type "double3" 28.312484264373779 28.312484264373779 28.312484264373779 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "69B39755-4642-B925-B685-E2B8EDE77499";
	setAttr ".uopa" yes;
	setAttr -s 88 ".uvtk[44:131]" -type "float2" -0.021928117 -0.47206208
		 -0.021928117 -0.47206208 -0.021928176 -0.47206208 -0.021928176 -0.47206208 -0.021928057
		 -0.47206208 -0.021928057 -0.47206208 -0.021928117 -0.47206187 -0.021928117 -0.47206187
		 -0.021928176 -0.47206208 -0.021928176 -0.47206208 -0.021928057 -0.47206187 -0.021928117
		 -0.47206187 -0.021928176 -0.47206187 -0.021928176 -0.47206187 -0.021928236 -0.47206208
		 -0.021928236 -0.47206208 -0.021928057 -0.47206187 -0.021928176 -0.47206187 -0.021928236
		 -0.47206187 -0.021928295 -0.47206208 -0.021928295 -0.47206208 -0.021928236 -0.47206187
		 -0.021928236 -0.47206184 -0.021928295 -0.47206208 -0.021928295 -0.47206208 -0.021928295
		 -0.47206184 -0.021928295 -0.47206181 -0.021928295 -0.47206208 -0.021928295 -0.47206208
		 -0.021928295 -0.47206181 -0.021928295 -0.47206181 -0.021928355 -0.47206208 -0.021928355
		 -0.47206208 -0.021928295 -0.47206181 -0.021928355 -0.47206181 -0.021928355 -0.47206208
		 -0.021928355 -0.47206208 -0.021928355 -0.47206181 -0.021928355 -0.47206181 -0.021928385
		 -0.47206208 -0.021928385 -0.47206208 -0.021928355 -0.47206181 -0.021928385 -0.47206181
		 -0.021928385 -0.47206181 0.45735389 -0.96793902 0.4595927 -0.97017777 0.48181427
		 -0.94795626 0.47957546 -0.94571745 0.45457655 -0.97071636 0.45681536 -0.97295511
		 0.48235279 -0.94294012 0.4845916 -0.94517893 0.40422863 -0.87037063 0.38200706 -0.89259219
		 0.37922972 -0.89536953 0.40700597 -0.86759329 0.32217693 -0.78831887 0.29995537 -0.81054044
		 0.29717803 -0.81331778 0.32495427 -0.78554153 0.24244095 -0.70858294 0.22021939 -0.73080444
		 0.21744205 -0.73358178 0.24521829 -0.7058056 0.18132867 -0.64747053 0.15910704 -0.66969204
		 0.15632977 -0.67246938 0.18410601 -0.6446932 0.14114787 -0.60728979 0.1189263 -0.62951136
		 0.11614896 -0.63228869 0.1439252 -0.60451245 0.10096706 -0.56710899 0.078745499 -0.58933055
		 0.075968161 -0.59210789 0.1037444 -0.56433165 0.061172023 -0.52731395 0.038950458
		 -0.54953545 0.03617312 -0.55231279 0.063949361 -0.52453661 -0.01128532 -0.49929965
		 0.010936245 -0.47707808 -0.014062658 -0.50207698 0.013713583 -0.47430077 0.0086975247
		 -0.47483939 -0.013524041 -0.49706095 -0.016301379 -0.49983829 0.011474863 -0.47206208;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "B9F2B5EB-4581-65E0-F65B-40A631283B07";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 15 "e[21]" "e[56]" "e[83]" "e[94]" "e[120]" "e[127]" "e[153]" "e[160]" "e[210:211]" "e[215:216]" "e[220:223]" "e[228]" "e[234]" "e[240]" "e[246]";
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "3EB00A1E-4CBF-6754-27AA-7E9DB34C7321";
	setAttr ".uopa" yes;
	setAttr -s 154 ".uvtk[0:153]" -type "float2" -0.4646461 0.71190995 -0.4633871
		 0.71190995 -0.46338704 0.90805435 -0.4646461 0.90805435 -0.46338704 0.71066046 -0.4646461
		 0.71066052 -0.42101431 0.71190995 -0.42093512 0.90805435 -0.46338704 0.9093039 -0.4646461
		 0.9093039 -0.42101431 0.71066046 -0.37487096 0.71190995 -0.37460086 0.91386348 -0.42101431
		 0.9093039 -0.37487096 0.71066046 -0.33002993 0.71190995 -0.32947254 0.92796278 -0.3748709
		 0.91508901 -0.33002993 0.71066052 -0.29566211 0.71190995 -0.29490471 0.95150459 -0.33002993
		 0.92909861 -0.29566216 0.71066052 -0.27306572 0.71191001 -0.27236688 0.96990156 -0.29566211
		 0.95250404 -0.27306572 0.71066046 -0.25046936 0.71191001 -0.2499842 0.98151088 -0.27306572
		 0.97094893 -0.25046936 0.71066046 -0.22808979 0.71191001 -0.22789542 0.988446 -0.2504693
		 0.98266906 -0.22808982 0.71066052 -0.19983868 0.71191001 -0.19983868 0.988446 -0.22808979
		 0.98969555 -0.19983871 0.71066052 -0.19857974 0.71191001 -0.19857974 0.988446 -0.19983868
		 0.98969555 -0.19857977 0.71066052 -0.19857974 0.98969555 -0.46338704 0.69504005 -0.4633871
		 0.69379044 -0.42101437 0.69379044 -0.42101431 0.69504005 -0.46464622 0.69379044 -0.46464616
		 0.69504005 -0.4633871 0.49764603 -0.42093524 0.49764603 -0.37487096 0.69379044 -0.37487096
		 0.69504005 -0.46464622 0.49764603 -0.4633871 0.49639648 -0.42101437 0.49639648 -0.37460092
		 0.49183691 -0.33002993 0.69379044 -0.33002993 0.69504005 -0.46464622 0.49639648 -0.37487096
		 0.49061131 -0.32947254 0.47773761 -0.29566211 0.69379044 -0.29566216 0.69504005 -0.33002993
		 0.47660178 -0.29490471 0.45419577 -0.27306572 0.69379044 -0.27306572 0.69504005 -0.29566211
		 0.4531962 -0.27236688 0.43579882 -0.2504693 0.69379044 -0.2504693 0.69504005 -0.27306572
		 0.43475133 -0.24998415 0.42418945 -0.22808978 0.69379044 -0.22808976 0.69504005 -0.2504693
		 0.42303127 -0.22789539 0.41725433 -0.19983865 0.69379044 -0.19983868 0.69504005 -0.22808978
		 0.41600478 -0.19983865 0.41725433 -0.19857971 0.69379044 -0.19857977 0.69504005 -0.19983865
		 0.41600478 -0.19857971 0.41725433 -0.19857971 0.41600478 -0.46338704 0.70909858 -0.46464604
		 0.70909858 -0.4646461 0.69660187 -0.46338704 0.69660187 -0.42101425 0.69660187 -0.42101425
		 0.70909858 -0.37487096 0.69660187 -0.37487084 0.70909858 -0.33002988 0.69660187 -0.33002988
		 0.70909858 -0.29566222 0.69660187 -0.29566211 0.70909858 -0.27306578 0.69660187 -0.27306572
		 0.70909864 -0.25046936 0.69660187 -0.2504693 0.70909864 -0.22808982 0.69660187 -0.22808976
		 0.70909858 -0.19983865 0.70909858 -0.19983871 0.69660187 -0.19857977 0.69660187 -0.19857977
		 0.70909858 0.56696403 -0.50183022 0.56696403 -0.5031566 0.58012885 -0.5031566 0.58012885
		 -0.50183022 0.5653187 -0.5031566 0.5653187 -0.50183022 0.56696403 -0.54779446 0.58012885
		 -0.54779446 0.58177429 -0.5031566 0.58177429 -0.50183022 0.5653187 -0.54779446 0.56696403
		 -0.59640461 0.58012885 -0.59640461 0.58177429 -0.54779446 0.5653187 -0.59640461 0.56696403
		 -0.64364272 0.58012885 -0.64364272 0.58177429 -0.59640461 0.5653187 -0.64364272 0.56696403
		 -0.67984766 0.58012885 -0.67984766 0.58177429 -0.64364272 0.5653187 -0.67984766 0.56696403
		 -0.70365202 0.58012885 -0.70365202 0.58177429 -0.67984766 0.5653187 -0.70365202 0.56696403
		 -0.72745645 0.58012885 -0.72745645 0.58177429 -0.70365202 0.5653187 -0.72745645 0.56696403
		 -0.75103229 0.58012885 -0.75103229 0.58177429 -0.72745645 0.5653187 -0.75103229 0.56696403
		 -0.78079355 0.58012885 -0.78079355 0.58177429 -0.75103229 0.5653187 -0.78079355 0.58012885
		 -0.78211987 0.56696403 -0.78211987 0.58177429 -0.78079355 0.5653187 -0.78211987 0.58177429
		 -0.78211987;
createNode polyAutoProj -n "polyAutoProj3";
	rename -uid "E69A6562-4F5F-C78D-350A-618767C8D155";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:119]";
	setAttr ".ix" -type "matrix" -0.80000000000000004 0 0 0 0 1 0 0 0 0 0.99247700925403159 0
		 6.230756494418241 0 0.87370180667271669 1;
	setAttr ".s" -type "double3" 28.312484264373779 28.312484264373779 28.312484264373779 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "F2E11B7D-4506-F68D-6510-76B09B7A20C4";
	setAttr ".uopa" yes;
	setAttr -s 88 ".uvtk[44:131]" -type "float2" -0.021928117 -0.47206208
		 -0.021928117 -0.47206208 -0.021928176 -0.47206208 -0.021928176 -0.47206208 -0.021928057
		 -0.47206208 -0.021928057 -0.47206208 -0.021928117 -0.47206187 -0.021928117 -0.47206187
		 -0.021928176 -0.47206208 -0.021928176 -0.47206208 -0.021928057 -0.47206187 -0.021928117
		 -0.47206187 -0.021928176 -0.47206187 -0.021928176 -0.47206187 -0.021928236 -0.47206208
		 -0.021928236 -0.47206208 -0.021928057 -0.47206187 -0.021928176 -0.47206187 -0.021928236
		 -0.47206187 -0.021928295 -0.47206208 -0.021928295 -0.47206208 -0.021928236 -0.47206187
		 -0.021928236 -0.47206184 -0.021928295 -0.47206208 -0.021928295 -0.47206208 -0.021928295
		 -0.47206184 -0.021928295 -0.47206181 -0.021928295 -0.47206208 -0.021928295 -0.47206208
		 -0.021928295 -0.47206181 -0.021928295 -0.47206181 -0.021928355 -0.47206208 -0.021928355
		 -0.47206208 -0.021928295 -0.47206181 -0.021928355 -0.47206181 -0.021928355 -0.47206208
		 -0.021928355 -0.47206208 -0.021928355 -0.47206181 -0.021928355 -0.47206181 -0.021928385
		 -0.47206208 -0.021928385 -0.47206208 -0.021928355 -0.47206181 -0.021928385 -0.47206181
		 -0.021928385 -0.47206181 0.45735389 -0.96793902 0.4595927 -0.97017777 0.48181427
		 -0.94795626 0.47957546 -0.94571745 0.45457655 -0.97071636 0.45681536 -0.97295511
		 0.48235279 -0.94294012 0.4845916 -0.94517893 0.40422863 -0.87037063 0.38200706 -0.89259219
		 0.37922972 -0.89536953 0.40700597 -0.86759329 0.32217693 -0.78831887 0.29995537 -0.81054044
		 0.29717803 -0.81331778 0.32495427 -0.78554153 0.24244095 -0.70858294 0.22021939 -0.73080444
		 0.21744205 -0.73358178 0.24521829 -0.7058056 0.18132867 -0.64747053 0.15910704 -0.66969204
		 0.15632977 -0.67246938 0.18410601 -0.6446932 0.14114787 -0.60728979 0.1189263 -0.62951136
		 0.11614896 -0.63228869 0.1439252 -0.60451245 0.10096706 -0.56710899 0.078745499 -0.58933055
		 0.075968161 -0.59210789 0.1037444 -0.56433165 0.061172023 -0.52731395 0.038950458
		 -0.54953545 0.03617312 -0.55231279 0.063949361 -0.52453661 -0.01128532 -0.49929965
		 0.010936245 -0.47707808 -0.014062658 -0.50207698 0.013713583 -0.47430077 0.0086975247
		 -0.47483939 -0.013524041 -0.49706095 -0.016301379 -0.49983829 0.011474863 -0.47206208;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "55818AC7-4563-506D-1707-368644AE9441";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 15 "e[21]" "e[56]" "e[83]" "e[94]" "e[120]" "e[127]" "e[153]" "e[160]" "e[210:211]" "e[215:216]" "e[220:223]" "e[228]" "e[234]" "e[240]" "e[246]";
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "C5511542-457B-0341-3145-8885B4A4CF4A";
	setAttr ".uopa" yes;
	setAttr -s 154 ".uvtk[0:153]" -type "float2" -0.16239542 0.6962769 -0.16113645
		 0.6962769 -0.16113639 0.89242131 -0.16239542 0.89242131 -0.16113639 0.69502741 -0.16239542
		 0.69502741 -0.11876369 0.6962769 -0.11868447 0.89242131 -0.16113639 0.8936708 -0.16239542
		 0.8936708 -0.11876363 0.69502741 -0.072620332 0.6962769 -0.072350204 0.89823043 -0.11876369
		 0.8936708 -0.072620332 0.69502741 -0.027779222 0.6962769 -0.027221918 0.91232979
		 -0.072620273 0.89945596 -0.027779222 0.69502741 0.0065885186 0.6962769 0.007346034
		 0.93587154 -0.027779222 0.9134655 0.0065885186 0.69502741 0.029184937 0.69627696
		 0.029883802 0.95426857 0.0065885186 0.93687105 0.029184937 0.69502741 0.051781356
		 0.69627696 0.052266538 0.96587789 0.029184937 0.95531583 0.051781356 0.69502741 0.074160844
		 0.69627696 0.074355274 0.97281301 0.051781356 0.96703601 0.074160814 0.69502741 0.1024119
		 0.69627696 0.1024119 0.97281301 0.074160844 0.9740625 0.10241199 0.69502741 0.10367092
		 0.69627696 0.10367092 0.97281301 0.1024119 0.9740625 0.1036709 0.69502741 0.10367092
		 0.9740625 -0.16113639 0.679407 -0.16113651 0.67815745 -0.11876369 0.67815745 -0.11876363
		 0.679407 -0.16239548 0.67815745 -0.16239542 0.679407 -0.16113651 0.48201299 -0.11868453
		 0.48201299 -0.072620332 0.67815745 -0.072620332 0.679407 -0.16239548 0.48201299 -0.16113651
		 0.48076338 -0.11876369 0.48076338 -0.072350204 0.4762038 -0.027779222 0.67815745
		 -0.027779222 0.679407 -0.16239548 0.48076338 -0.072620332 0.47497821 -0.027221918
		 0.4621045 0.0065885782 0.67815745 0.0065885186 0.679407 -0.027779222 0.46096867 0.007346034
		 0.43856272 0.029184997 0.67815745 0.029184937 0.679407 0.0065885782 0.43756315 0.029883862
		 0.42016566 0.051781416 0.67815745 0.051781356 0.679407 0.029184997 0.41911829 0.052266538
		 0.4085564 0.074160933 0.67815745 0.074160874 0.679407 0.051781416 0.40739816 0.074355364
		 0.40162116 0.10241193 0.67815745 0.1024119 0.679407 0.074160933 0.40037173 0.10241193
		 0.40162116 0.10367095 0.67815745 0.10367095 0.679407 0.10241193 0.40037173 0.10367095
		 0.40162116 0.10367095 0.40037173 -0.16113633 0.69346559 -0.16239536 0.69346547 -0.16239542
		 0.68096882 -0.16113633 0.68096882 -0.11876363 0.68096888 -0.11876363 0.69346559 -0.072620332
		 0.68096888 -0.072620273 0.69346559 -0.027779222 0.68096888 -0.027779162 0.69346547
		 0.006588459 0.68096888 0.0065885186 0.69346547 0.029184937 0.68096888 0.029184937
		 0.69346565 0.051781356 0.68096888 0.051781356 0.69346565 0.074160814 0.68096888 0.074160874
		 0.69346547 0.10241193 0.69346547 0.10241199 0.68096882 0.1036709 0.68096882 0.10367095
		 0.69346547 0.55185169 -0.50183022 0.55185169 -0.5031566 0.56501645 -0.5031566 0.56501645
		 -0.50183022 0.55020624 -0.5031566 0.55020624 -0.50183022 0.55185169 -0.54779452 0.56501645
		 -0.54779452 0.56666178 -0.5031566 0.56666178 -0.50183022 0.55020624 -0.54779452 0.55185169
		 -0.59640461 0.56501645 -0.59640461 0.56666178 -0.54779452 0.55020624 -0.59640461
		 0.55185169 -0.64364278 0.56501645 -0.64364278 0.56666178 -0.59640461 0.55020624 -0.64364278
		 0.55185169 -0.67984784 0.56501645 -0.67984784 0.56666178 -0.64364278 0.55020624 -0.67984784
		 0.55185169 -0.70365214 0.56501645 -0.70365214 0.56666178 -0.67984784 0.55020624 -0.70365214
		 0.55185169 -0.72745663 0.56501645 -0.72745663 0.56666178 -0.70365214 0.55020624 -0.72745663
		 0.55185169 -0.75103247 0.56501645 -0.75103247 0.56666178 -0.72745663 0.55020624 -0.75103247
		 0.55185169 -0.78079379 0.56501645 -0.78079379 0.56666178 -0.75103247 0.55020624 -0.78079379
		 0.56501645 -0.78212011 0.55185169 -0.78212011 0.56666178 -0.78079379 0.55020624 -0.78212011
		 0.56666178 -0.78212011;
createNode polyAutoProj -n "polyAutoProj4";
	rename -uid "D98EE473-4343-6E38-5225-25A0669D4BF9";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".s" -type "double3" 63.021415710449219 63.021415710449219 63.021415710449219 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode deleteComponent -n "deleteComponent5";
	rename -uid "69674116-405A-8420-B114-16A8D111B562";
	setAttr ".dc" -type "componentList" 1 "f[36]";
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "599CA4F1-49DE-557B-37F0-959E8C37BE72";
	setAttr ".ics" -type "componentList" 3 "e[39]" "e[42]" "e[90:91]";
createNode polyMapCut -n "polyMapCut1";
	rename -uid "09F2C80F-40CA-B61C-E44E-E5A5360405FA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[90]";
createNode deleteComponent -n "deleteComponent6";
	rename -uid "D0C7C6AE-4C6F-6EDF-080C-7087BFE2EE14";
	setAttr ".dc" -type "componentList" 8 "f[17:18]" "f[20]" "f[23]" "f[26]" "f[41]" "f[45]" "f[49]" "f[52]";
createNode deleteComponent -n "deleteComponent7";
	rename -uid "6FAC666A-4929-CA69-7905-288A6F0E466C";
	setAttr ".dc" -type "componentList" 9 "f[2]" "f[5]" "f[8]" "f[13]" "f[20]" "f[27]" "f[30]" "f[32]" "f[35]";
createNode polyTweak -n "polyTweak44";
	rename -uid "BCC856ED-4C85-4CF4-D522-AEB7E814BFEB";
	setAttr ".uopa" yes;
	setAttr -s 77 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0.54731631 0 ;
	setAttr ".tk[2]" -type "float3" 0 0.54731631 0 ;
	setAttr ".tk[3]" -type "float3" 0 0.26513007 0 ;
	setAttr ".tk[5]" -type "float3" 0 0.54731631 0 ;
	setAttr ".tk[6]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[7]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.26513007 0 ;
	setAttr ".tk[11]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[12]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[13]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[18]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".tk[20]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[25]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[28]" -type "float3" 0.2889739 0.25569761 0.27704632 ;
	setAttr ".tk[29]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[30]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[31]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[32]" -type "float3" 0 0.6711154 0 ;
	setAttr ".tk[33]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[35]" -type "float3" -0.2889739 0.25569761 0.27704632 ;
	setAttr ".tk[36]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[37]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[38]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[39]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[43]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".tk[46]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".tk[48]" -type "float3" 1.1920929e-07 0 2.9802322e-08 ;
	setAttr ".tk[51]" -type "float3" 0 0 0.2433496 ;
	setAttr ".tk[54]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[55]" -type "float3" 0.2889739 0.25569761 -0.27704632 ;
	setAttr ".tk[56]" -type "float3" 0 0.25569761 0.77178276 ;
	setAttr ".tk[57]" -type "float3" 0 0.25569761 0.77178282 ;
	setAttr ".tk[58]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.25569761 0.99287903 ;
	setAttr ".tk[61]" -type "float3" 0 0.25569761 0.77178282 ;
	setAttr ".tk[62]" -type "float3" 0 0.25569761 0.77178282 ;
	setAttr ".tk[63]" -type "float3" 0 0.25569761 0.77178276 ;
	setAttr ".tk[64]" -type "float3" -0.2889739 0.25569761 -0.27704632 ;
	setAttr ".tk[65]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[66]" -type "float3" -1.1920929e-07 0 2.9802322e-08 ;
	setAttr ".tk[69]" -type "float3" 0 0 0.2433496 ;
	setAttr ".tk[70]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[73]" -type "float3" -5.9604645e-08 0 0 ;
	setAttr ".tk[75]" -type "float3" 0 0 0.47596499 ;
	setAttr ".tk[76]" -type "float3" -5.9604645e-08 0 0 ;
	setAttr ".tk[78]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[79]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[80]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[81]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[86]" -type "float3" 5.9604645e-08 0 0 ;
	setAttr ".tk[88]" -type "float3" 0 0 0.47596499 ;
	setAttr ".tk[91]" -type "float3" 0 0 0.47596499 ;
	setAttr ".tk[92]" -type "float3" 5.9604645e-08 0 0 ;
	setAttr ".tk[94]" -type "float3" 0 0.25569761 0.99287903 ;
	setAttr ".tk[95]" -type "float3" 0 0.25569761 0.77178282 ;
	setAttr ".tk[96]" -type "float3" 0 0.25569761 0.99287903 ;
	setAttr ".tk[97]" -type "float3" 0 0.25569761 0.77178282 ;
	setAttr ".tk[99]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[102]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[104]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[105]" -type "float3" 0 0.6711154 0 ;
	setAttr ".tk[106]" -type "float3" 0 0.6711154 0 ;
	setAttr ".tk[107]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[108]" -type "float3" 0 0.82543188 0 ;
	setAttr ".tk[109]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[110]" -type "float3" 0 0.25569761 0 ;
	setAttr ".tk[111]" -type "float3" 0 0.6711154 0 ;
	setAttr ".tk[113]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".tk[116]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".tk[118]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[119]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".tk[120]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".tk[121]" -type "float3" -1.1920929e-07 0 0 ;
createNode deleteComponent -n "deleteComponent8";
	rename -uid "E24030F0-44D2-F824-6BAC-EE8F98DB11BE";
	setAttr ".dc" -type "componentList" 27 "f[1:2]" "f[6:7]" "f[9]" "f[16]" "f[18]" "f[25]" "f[27]" "f[30]" "f[35]" "f[39]" "f[46:47]" "f[52:53]" "f[55:56]" "f[62:63]" "f[70:71]" "f[74]" "f[78]" "f[83:84]" "f[86]" "f[88]" "f[93:94]" "f[100]" "f[103]" "f[105]" "f[109]" "f[113]" "f[117]";
createNode polyAutoProj -n "polyAutoProj5";
	rename -uid "01C76D51-403D-3BF2-813E-46B32D642045";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".ix" -type "matrix" 2.0520273429232301 0 0 0 0 28.581919168118063 0 0 0 0 3.7647923684188305 0
		 -31.844260444737046 13.185582446527084 14.526437023076681 1;
	setAttr ".s" -type "double3" 28.581919168118063 28.581919168118063 28.581919168118063 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "CA2CD560-4F63-7C3B-EC83-45B15FA800A6";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[16:31]" -type "float2" 0.66478068 0 0.66478068 0
		 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068
		 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068
		 0;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "9E1D5B13-47C4-24DC-1228-91B5B402F286";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[29]" "e[89]" "e[94]";
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "8A3D90BF-4172-7353-7682-1C94E2CCA970";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[76:91]" -type "float2" -0.0023462772 -0.66888404
		 -0.035290271 -0.63593996 -0.36007532 -0.96072364 -0.32713133 -0.99366772 -0.032944024
		 -0.63359368 -2.9802322e-08 -0.66653788 -0.29887843 -0.37235066 -0.62366349 -0.69713438
		 -0.3624216 -0.96306986 -0.32947761 -0.996014 -0.29653218 -0.37000442 -0.33182469
		 -0.33940426 -0.65660977 -0.66418791 -0.62600976 -0.69948059 -0.32947844 -0.33705801
		 -0.65895605 -0.66653419;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "C7657D2F-438E-E6FA-5A26-8BAA900A7DCB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[57]" "e[96]" "e[104]";
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "E6D5A3FE-4C3B-AFB6-E53F-4487AF8EDE17";
	setAttr ".uopa" yes;
	setAttr -s 28 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.050524265 0.33326751 ;
	setAttr ".uvtk[1]" -type "float2" -0.050524265 0.33326754 ;
	setAttr ".uvtk[2]" -type "float2" -0.050524205 0.33326754 ;
	setAttr ".uvtk[3]" -type "float2" -0.050524205 0.33326751 ;
	setAttr ".uvtk[4]" -type "float2" -0.050524265 0.33326754 ;
	setAttr ".uvtk[5]" -type "float2" -0.050524265 0.33326751 ;
	setAttr ".uvtk[6]" -type "float2" -0.050524265 0.33326742 ;
	setAttr ".uvtk[7]" -type "float2" -0.050524205 0.33326742 ;
	setAttr ".uvtk[8]" -type "float2" -0.050524175 0.33326751 ;
	setAttr ".uvtk[9]" -type "float2" -0.050524175 0.33326751 ;
	setAttr ".uvtk[10]" -type "float2" -0.050524265 0.33326742 ;
	setAttr ".uvtk[11]" -type "float2" -0.050524265 0.33326742 ;
	setAttr ".uvtk[12]" -type "float2" -0.050524205 0.33326742 ;
	setAttr ".uvtk[13]" -type "float2" -0.050524175 0.33326742 ;
	setAttr ".uvtk[14]" -type "float2" -0.050524265 0.33326742 ;
	setAttr ".uvtk[15]" -type "float2" -0.050524175 0.33326742 ;
	setAttr ".uvtk[76]" -type "float2" -0.050524175 0.33326742 ;
	setAttr ".uvtk[77]" -type "float2" -0.050524175 0.33326751 ;
	setAttr ".uvtk[78]" -type "float2" -0.050524175 0.33326742 ;
	setAttr ".uvtk[79]" -type "float2" -0.050524056 0.33326742 ;
	setAttr ".uvtk[80]" -type "float2" -0.050524056 0.33326751 ;
	setAttr ".uvtk[81]" -type "float2" -0.050524175 0.33326751 ;
	setAttr ".uvtk[82]" -type "float2" -0.050524056 0.33326742 ;
	setAttr ".uvtk[83]" -type "float2" -0.050524086 0.33326742 ;
	setAttr ".uvtk[84]" -type "float2" -0.050524116 0.33326751 ;
	setAttr ".uvtk[85]" -type "float2" -0.050524056 0.33326751 ;
	setAttr ".uvtk[86]" -type "float2" -0.050524086 0.33326742 ;
	setAttr ".uvtk[87]" -type "float2" -0.050524116 0.33326751 ;
createNode polyMapSewMove -n "polyMapSewMove6";
	rename -uid "456EAEE8-41B3-E8F6-0E02-2A9427019ACA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[15]" "e[85]" "e[92]";
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "0B99F720-43FC-630C-1653-2E80B40D1036";
	setAttr ".uopa" yes;
	setAttr -s 84 ".uvtk[0:83]" -type "float2" -0.12313446 0.16025084 -0.12313446
		 0.1594761 0.14452252 0.15947616 0.14452252 0.1602509 -0.13950613 0.1594761 -0.13950613
		 0.16025084 -0.12313434 0.052307844 0.14452258 0.052307904 0.16089439 0.15947616 0.16089439
		 0.16025007 -0.13950613 0.052307844 -0.12313434 0.051533043 0.14452258 0.051533103
		 0.16089445 0.052307904 -0.13950613 0.051532984 0.16089445 0.051533997 -0.46950769
		 0.1594761 -0.46950769 0.16025084 -0.73716378 0.16025084 -0.73716378 0.1594761 -0.45313624
		 0.1594761 -0.45313624 0.16025084 -0.7535364 0.1594761 -0.7535364 0.16025084 -0.73716378
		 0.052307844 -0.46950769 0.052307844 -0.45313624 0.052307844 -0.7535364 0.052307844
		 -0.46950769 0.051532984 -0.73716378 0.051532984 -0.45313624 0.051532984 -0.7535364
		 0.051532984 -0.25569424 -0.2551837 -0.25569424 -0.28654334 0.011962608 -0.28654334
		 0.011962608 -0.2551837 -0.27206615 -0.2551837 -0.27206615 -0.28654334 0.028334586
		 -0.2551837 0.028334586 -0.28654334 0.011962608 -0.0042732321 -0.25569424 -0.0042732321
		 -0.27206615 -0.0042732321 0.028334586 -0.0042732321 -0.25569424 0.027086705 0.011962608
		 0.027086705 -0.27206615 0.027086705 0.028334586 0.027086705 -0.018261861 -0.95307869
		 -0.018261861 -0.92171824 -0.28591818 -0.92171824 -0.28591818 -0.95307869 -0.0018904805
		 -0.92171824 -0.0018904805 -0.95307869 -0.018261861 -0.67080885 -0.28591818 -0.67080885
		 -0.30229062 -0.92171824 -0.30229062 -0.95307869 -0.0018904805 -0.67080885 -0.018261861
		 -0.63944834 -0.28591818 -0.63944834 -0.30229062 -0.67080885 -0.0018904805 -0.63944834
		 -0.30229062 -0.63944834 -0.17086577 0.1594761 -0.17086577 0.16025084 -0.42177621
		 0.16025084 -0.42177621 0.1594761 -0.42177621 0.052307844 -0.17086577 0.052307844
		 -0.17086577 0.051532984 -0.42177621 0.051532984 0.19225371 0.052308023 0.19225365
		 0.15947622 0.19225371 0.051534832 0.44316188 0.052308023 0.44316176 0.15947628 0.19225365
		 0.16024923 0.44316188 0.051534891 0.47452328 0.052308023 0.47452328 0.15947622 0.44316176
		 0.16024929 0.47452328 0.051534891 0.47452328 0.16024929;
createNode polyAutoProj -n "polyAutoProj6";
	rename -uid "D38564AE-4F47-CAE7-205B-7D842BF956B4";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".ix" -type "matrix" -2.0520273429232301 0 0 0 0 28.581919168118063 0 0 0 0 3.7647923684188305 0
		 31.844260444737046 13.185582446527084 14.526437023076681 1;
	setAttr ".s" -type "double3" 28.581919168118063 28.581919168118063 28.581919168118063 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "D9B5A222-4853-33D0-497C-219D4F6BFAAF";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[16:31]" -type "float2" 0.66478068 0 0.66478068 0
		 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068
		 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068 0 0.66478068
		 0;
createNode polyMapSewMove -n "polyMapSewMove7";
	rename -uid "337E57E1-4078-E4AF-3B85-60972B5E1AF5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[29]" "e[89]" "e[94]";
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "72939106-4794-D103-C983-CA8FC685429D";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[0:15]" -type "float2" -0.050524294 0.33326754 -0.050524294
		 0.33326754 -0.050524235 0.33326754 -0.050524235 0.33326754 -0.050524294 0.33326754
		 -0.050524294 0.33326754 -0.050524294 0.33326751 -0.050524235 0.33326751 -0.050524235
		 0.33326754 -0.050524235 0.33326754 -0.050524294 0.33326751 -0.050524294 0.33326751
		 -0.050524235 0.33326751 -0.050524235 0.33326751 -0.050524294 0.33326751 -0.050524235
		 0.33326751;
createNode polyMapSewMove -n "polyMapSewMove8";
	rename -uid "D71F38EF-4FB9-B668-78C6-1BA9FE69DA74";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[15]" "e[85]" "e[92]";
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "E90494C2-4774-C427-E5F4-2EB025460924";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[72:87]" -type "float2" -0.052870482 -0.33561659
		 -0.085814476 -0.30267245 -0.41059953 -0.62745607 -0.37765554 -0.66040021 -0.083468229
		 -0.30032617 -0.050524235 -0.33327031 -0.34940258 -0.039083183 -0.67418766 -0.36386681
		 -0.41294581 -0.62980235 -0.38000181 -0.66274649 -0.34705633 -0.036736906 -0.38234884
		 -0.006136775 -0.70713389 -0.3309204 -0.67653394 -0.36621308 -0.38000259 -0.0037904978
		 -0.70948017 -0.33326668;
createNode polyMapSewMove -n "polyMapSewMove9";
	rename -uid "46C55E81-4874-02DF-97EB-609389F35FC1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[57]" "e[96]" "e[104]";
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "E80D1175-4CD7-F2CD-F69B-8AAE4CDFBC38";
	setAttr ".uopa" yes;
	setAttr -s 84 ".uvtk[0:83]" -type "float2" -0.1231344 0.3810125 -0.1231344
		 0.3802377 0.14452252 0.3802377 0.14452252 0.3810125 -0.13950616 0.3802377 -0.13950616
		 0.3810125 -0.1231344 0.27306956 0.14452252 0.27306956 0.16089445 0.3802377 0.16089445
		 0.38101166 -0.13950616 0.27306956 -0.1231344 0.2722947 0.14452252 0.2722947 0.16089451
		 0.27306956 -0.13950616 0.2722947 0.16089451 0.27229553 -0.46950763 0.3802377 -0.46950763
		 0.3810125 -0.7371639 0.3810125 -0.7371639 0.3802377 -0.45313621 0.3802377 -0.45313621
		 0.3810125 -0.7535364 0.3802377 -0.7535364 0.3810125 -0.7371639 0.27306956 -0.46950763
		 0.27306956 -0.45313621 0.27306956 -0.7535364 0.27306956 -0.46950763 0.2722947 -0.7371639
		 0.2722947 -0.45313621 0.2722947 -0.7535364 0.2722947 0.25813192 -0.28426236 0.25813192
		 -0.31562203 0.52578878 -0.31562203 0.52578878 -0.28426236 0.24176002 -0.28426236
		 0.24176002 -0.31562203 0.54216075 -0.28426236 0.54216075 -0.31562203 0.52578878 -0.03335188
		 0.25813192 -0.03335188 0.24176002 -0.03335188 0.54216075 -0.03335188 0.25813192 -0.0019919367
		 0.52578878 -0.0019919367 0.24176002 -0.0019919367 0.54216075 -0.0019919367 0.52578932
		 -0.96630782 0.52578932 -0.93494737 0.25813302 -0.93494737 0.25813302 -0.96630782
		 0.54216075 -0.93494737 0.54216075 -0.96630782 0.52578932 -0.68403798 0.25813302 -0.68403798
		 0.24176049 -0.93494737 0.24176049 -0.96630782 0.54216075 -0.68403798 0.52578932 -0.65267748
		 0.25813302 -0.65267748 0.24176049 -0.68403798 0.54216075 -0.65267748 0.24176049 -0.65267748
		 -0.17086583 0.3802377 -0.17086583 0.3810125 -0.42177632 0.3810125 -0.42177632 0.3802377
		 -0.42177632 0.27306956 -0.17086583 0.27306956 -0.17086583 0.2722947 -0.42177632 0.2722947
		 0.19225371 0.27306956 0.19225365 0.3802377 0.19225371 0.27229637 0.4431619 0.27306956
		 0.4431619 0.38023773 0.19225365 0.38101083 0.4431619 0.27229637 0.47452337 0.27306962
		 0.47452331 0.38023773 0.4431619 0.38101086 0.47452337 0.27229643 0.47452331 0.38101086;
createNode polyAutoProj -n "polyAutoProj7";
	rename -uid "3F99442D-4F2A-D84B-14FC-118BCC20B572";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".ix" -type "matrix" -2.0520273429232301 0 0 0 0 36.403444470495486 0 0 0 0 3.7647923684188305 0
		 31.844260444737046 17.096345253448888 -13.782653904732056 1;
	setAttr ".s" -type "double3" 36.403444470495486 36.403444470495486 36.403444470495486 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "16E93B14-41FE-9076-BF16-389267FE296A";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[64:79]" -type "float2" -0.31007278 -0.3680422 -0.31190258
		 -0.3662124 -0.57549232 -0.62980211 -0.57366252 -0.63163197 -0.27712798 -0.3350974
		 -0.27895778 -0.3332676 -0.60660696 -0.66457641 -0.60843676 -0.66274655 -0.24784277
		 -0.95745164 0.015746966 -0.6938619 0.048691764 -0.6609171 -0.28078723 -0.99039608
		 0.017576411 -0.69569135 -0.24601339 -0.95928109 0.05052121 -0.66274655 -0.27895784
		 -0.99222553;
createNode polyMapSewMove -n "polyMapSewMove10";
	rename -uid "7C12EDD2-47C4-E503-1690-F68CFA780972";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[15]" "e[85]" "e[92]";
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "A48531E8-4BA6-028E-E0F6-938AB56023F4";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[76:91]" -type "float2" 0.32764876 -0.33888462 0.29470396
		 -0.30593997 -0.031115785 -0.63176107 0.0018290132 -0.66470575 0.29653412 -0.30410981
		 0.32947892 -0.33705446 0.031114146 -0.042351246 -0.29470557 -0.36817241 -0.032945946
		 -0.63359118 -1.1473894e-06 -0.66653585 0.032944307 -0.040521085 -0.0018302947 -0.0094069242
		 -0.32765001 -0.33522809 -0.29653573 -0.37000257 -1.3411045e-07 -0.0075767636 -0.32948017
		 -0.33705825;
createNode polyMapSewMove -n "polyMapSewMove11";
	rename -uid "9815E680-40AD-294D-2D1F-3FBF69DA2282";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[57]" "e[96]" "e[104]";
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "443D4A16-4706-9AE5-CC82-409E3DEF9CCA";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[16:31]" -type "float2" 0.040218532 0.63538271 0.038388729
		 0.63721251 -0.25517708 0.34364665 -0.25334728 0.34181684 0.058174193 0.65333837 0.05634439
		 0.65516818 -0.27130431 0.32385981 -0.27313411 0.32568961 0.072472394 0.015997306
		 0.3660382 0.30956316 0.38399386 0.32751882 0.054515362 -0.0019597262 0.36786765 0.30773371
		 0.074301839 0.01416786 0.38582331 0.32568938 0.056344807 -0.0037891716;
createNode polyMapSewMove -n "polyMapSewMove12";
	rename -uid "B6249E0B-4FD3-4B8B-84F3-A5BBEC893D57";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[71]" "e[99]" "e[106]";
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "409BA461-4D30-659F-6639-2A89F981DECF";
	setAttr ".uopa" yes;
	setAttr -s 84 ".uvtk[0:83]" -type "float2" -0.33870578 0.042056296 -0.3349815
		 0.042056296 -0.3349815 -0.15936367 -0.33870578 -0.15936367 -0.33498153 0.054376666
		 -0.33870566 0.054376666 0.3280912 0.042056296 0.3280912 -0.1593637 -0.33498147 -0.1716844
		 -0.33870542 -0.17168443 0.32809114 0.054376666 0.3318156 0.042056296 0.3318156 -0.1593637
		 0.32809114 -0.17168443 0.3318156 0.054376666 0.33181524 -0.17168443 -0.33498159 -0.65853369
		 -0.33870554 -0.65853369 -0.33870548 -0.45711368 -0.33498156 -0.45711368 -0.33498162
		 -0.67085302 -0.33870554 -0.67085302 -0.33498153 -0.44479299 -0.3387053 -0.44479299
		 0.3280912 -0.45711368 0.32809114 -0.65853369 0.32809114 -0.67085302 0.3280912 -0.44479305
		 0.33181554 -0.65853369 0.3318156 -0.45711368 0.33181554 -0.67085302 0.33181524 -0.44479305
		 0.075720683 0.10384259 0.10302913 0.10384259 0.10302913 0.30526257 0.075720683 0.30526257
		 0.075720683 0.091522187 0.10302913 0.091522187 0.075720683 0.31758326 0.10302913
		 0.31758326 -0.14277272 0.30526257 -0.14277272 0.10384259 -0.14277272 0.091522187
		 -0.14277272 0.31758326 -0.17008078 0.10384259 -0.17008078 0.30526257 -0.17008078
		 0.091522187 -0.17008078 0.31758326 -0.014209808 -0.62206221 -0.014209808 -0.5947541
		 -0.21562977 -0.5947541 -0.21562977 -0.62206221 -0.0018905036 -0.5947541 -0.0018905036
		 -0.62206221 -0.014209808 -0.37626076 -0.21562977 -0.37626076 -0.22795045 -0.5947541
		 -0.22795045 -0.62206221 -0.0018905036 -0.37626076 -0.014209808 -0.34895271 -0.21562977
		 -0.34895271 -0.22795045 -0.37626076 -0.0018905036 -0.34895271 -0.22795045 -0.34895271
		 -0.33498153 0.081685126 -0.33870548 0.081685126 -0.33870548 0.3001785 -0.33498153
		 0.30017856 -0.33498153 0.32748663 -0.33870548 0.32748657 0.32809114 0.3001785 0.32809114
		 0.081685126 0.32809114 0.32748657 0.33181554 0.081685126 0.3318156 0.3001785 0.3318156
		 0.32748657 0.3280912 -0.1989927 -0.33498147 -0.19899273 0.33181489 -0.1989927 0.3280912
		 -0.41748506 -0.33498147 -0.417485 -0.33870506 -0.19899279 0.33181489 -0.41748506
		 -0.33870506 -0.417485;
createNode polyAutoProj -n "polyAutoProj8";
	rename -uid "1EF2BC6B-43DF-7962-B8BF-15A504941A51";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".ix" -type "matrix" 2.0520273429232301 0 0 0 0 36.403444470495486 0 0 0 0 3.7647923684188305 0
		 -31.844260444737046 17.096345253448888 -13.782653904732056 1;
	setAttr ".s" -type "double3" 36.403444470495486 36.403444470495486 36.403444470495486 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "017DAC4E-408F-4293-F688-A3830D5165A2";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[16:31]" -type "float2" 0.6793927 0.65908796 0.6793927
		 0.66274768 0.092259705 0.66274768 0.092259705 0.65908796 0.71530408 0.65908796 0.71530408
		 0.66274768 0.056345582 0.65908796 0.056345582 0.66274768 0.092259705 0.0074472427
		 0.6793927 0.0074472427 0.71530408 0.0074472427 0.056345582 0.0074472427 0.6793927
		 0.0037882924 0.092259705 0.0037882924 0.71530408 0.0037882924 0.056345582 0.0037882924;
createNode polyMapSewMove -n "polyMapSewMove13";
	rename -uid "813530A0-4008-143B-9A0A-36A3CCD7404D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[71]" "e[99]" "e[106]";
createNode polyTweakUV -n "polyTweakUV20";
	rename -uid "67F69EC5-4CFC-B535-7113-80B29A8240A8";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[0:15]" -type "float2" -0.31152388 0.97805828 -0.31335333
		 0.97622889 -0.019786268 0.68266302 -0.017956823 0.68449247 -0.33131012 0.99418563
		 -0.32948068 0.99601501 -0.63917309 0.65040773 -0.34560603 0.35684192 -0.0018291175
		 0.66470593 3.2782555e-07 0.66653538 -0.65712988 0.66836447 -0.64100254 0.64857835
		 -0.34743547 0.35501242 -0.32764888 0.33888483 -0.65895933 0.66653508 -0.32947832
		 0.33705533;
createNode polyMapSewMove -n "polyMapSewMove14";
	rename -uid "78D67528-4ABC-FF9E-1A66-1BB6D602BA61";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[57]" "e[96]" "e[104]";
createNode angleBetween -n "angleBetween1";
	rename -uid "B8000F7A-472B-76FE-E8D7-A783FDAF40D0";
	setAttr ".v1" -type "double3" 0 -0.3258197009563446 0 ;
	setAttr ".v2" -type "double3" 1.6763806343078613e-08 0.3258211612701416 0 ;
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "AC2105D4-4AA9-5D91-95AE-37B9258B19F2";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[64:79]" -type "float2" -0.67432785 0.65908808 -0.67432785
		 0.6627478 -1.2015084 0.6627478 -1.2015084 0.65908808 -0.60843807 0.65908808 -0.60843807
		 0.6627478 -1.2673974 0.65908808 -1.2673974 0.6627478 -1.2015084 0.0074473619 -0.67432785
		 0.0074472427 -0.60843807 0.0074472427 -1.2673974 0.0074473619 -0.67432785 0.0037884116
		 -1.2015084 0.0037884116 -0.60843807 0.0037884116 -1.2673974 0.0037884116;
createNode polyMapSewMove -n "polyMapSewMove15";
	rename -uid "B97992E2-4B22-12D0-45AB-B6BE36E80FFA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[15]" "e[85]" "e[92]";
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "DD2E850C-422A-A658-834C-A3BC76E772DC";
	setAttr ".uopa" yes;
	setAttr -s 84 ".uvtk[0:83]" -type "float2" 0.21028177 0.41710013 0.21028177
		 0.4124611 0.02403757 0.4124611 0.02403757 0.41710013 0.22167386 0.41246116 0.22167386
		 0.41709995 0.21028177 -0.41348651 0.02403757 -0.41348651 0.012645066 0.41246116 0.012645066
		 0.41709977 0.22167389 -0.41348645 0.21028177 -0.41812554 0.02403757 -0.41812542 0.012645125
		 -0.41348657 0.22167389 -0.41812548 0.012645125 -0.41812512 -0.44881713 0.41246128
		 -0.44881713 0.41709983 -0.26257288 0.41709983 -0.26257288 0.41246128 -0.46020818
		 0.41246128 -0.46020818 0.41709983 -0.25118053 0.41246128 -0.25118053 0.41709965 -0.26257288
		 -0.41348645 -0.44881713 -0.41348645 -0.46020818 -0.41348645 -0.25118041 -0.41348645
		 -0.44881713 -0.41812536 -0.26257288 -0.41812536 -0.46020818 -0.41812536 -0.25118041
		 -0.41812506 0.034570623 0.051876187 0.043212444 0.051876187 0.043212444 -0.051876295
		 0.034570623 -0.051876295 0.034570623 0.058222473 0.043212444 0.058222473 0.034570623
		 -0.058222346 0.043212444 -0.058222346 -0.034570813 -0.051876295 -0.034570813 0.051876187
		 -0.034570813 0.058222473 -0.034570813 -0.058222346 -0.043212235 0.051876187 -0.043212235
		 -0.051876295 -0.043212235 0.058222473 -0.043212235 -0.058222346 -0.051992118 -0.043141007
		 -0.051992118 -0.034513831 0.051992774 -0.034513831 0.051992774 -0.043141007 -0.058353711
		 -0.034513831 -0.058353711 -0.043141007 -0.051992118 0.034513831 0.051992774 0.034513831
		 0.058353066 -0.034513831 0.058353066 -0.043141007 -0.058353711 0.034513831 -0.051992118
		 0.043141007 0.051992774 0.043141007 0.058353066 0.034513831 -0.058353711 0.043141007
		 0.058353066 0.043141007 0.24805419 0.41246116 0.24805418 0.41709971 0.45912099 0.41709971
		 0.45912099 0.41246116 0.48550087 0.41246116 0.48550087 0.41709971 0.45912099 -0.41348657
		 0.24805419 -0.41348645 0.48550087 -0.41348657 0.24805421 -0.41812548 0.45912099 -0.41812548
		 0.48550087 -0.41812548 -0.013734967 -0.41348657 -0.013735026 0.41246116 -0.013734967
		 -0.41812482 -0.22480065 -0.41348651 -0.22480077 0.41246122 -0.013735026 0.41709942
		 -0.22480065 -0.41812477 -0.22480077 0.41709948;
createNode polyLayoutUV -n "polyLayoutUV1";
	rename -uid "B7E744FF-4C15-D115-221D-6C89CC0160BC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".l" 1;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
	setAttr ".rbf" 1;
	setAttr ".lm" 1;
createNode polyLayoutUV -n "polyLayoutUV2";
	rename -uid "D1F84BE2-41AB-C192-7B4F-B6ACC2CF070E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".l" 1;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
	setAttr ".rbf" 1;
	setAttr ".lm" 1;
createNode polyTweakUV -n "polyTweakUV23";
	rename -uid "1915737A-4818-AAF0-46B6-C88241558425";
	setAttr ".uopa" yes;
	setAttr -s 84 ".uvtk[0:83]" -type "float2" 0.76143068 -0.23205984 0.76143068
		 -0.2280677 0.69519377 -0.2280677 0.69519377 -0.23205984 0.76548219 -0.2280677 0.76548219
		 -0.23205984 0.76143068 0.48277417 0.69519377 0.48277417 0.6911422 -0.2280677 0.6911422
		 -0.23205984 0.76548219 0.48277417 0.76143068 0.48676628 0.69519377 0.48676628 0.6911422
		 0.48277417 0.76548219 0.48676628 0.6911422 0.48676628 0.58033413 -0.2280677 0.58033413
		 -0.23205984 0.6465711 -0.23205984 0.6465711 -0.2280677 0.57628262 -0.2280677 0.57628262
		 -0.23205984 0.65062261 -0.2280677 0.65062261 -0.23205984 0.6465711 0.48277417 0.58033413
		 0.48277417 0.57628262 0.48277417 0.65062261 0.48277417 0.58033413 0.48676628 0.6465711
		 0.48676628 0.57628262 0.48676628 0.65062261 0.48676628 -0.52751184 -0.33450449 -0.50823045
		 -0.33450449 -0.50823045 -0.019281438 -0.52751184 -0.019281438 -0.52751184 -0.3537859
		 -0.50823045 -0.3537859 -0.52751184 9.5510948e-08 -0.50823045 9.5510948e-08 -0.6817826
		 -0.019281438 -0.6817826 -0.33450449 -0.6817826 -0.3537859 -0.6817826 9.5510948e-08
		 -0.70106399 -0.33450449 -0.70106399 -0.019281438 -0.70106399 -0.3537859 -0.70106399
		 9.5510948e-08 -0.51253104 -0.33471617 -0.49323729 -0.33471617 -0.49323729 -0.019293621
		 -0.51253104 -0.019293621 -0.49323729 -0.3540099 -0.51253104 -0.3540099 -0.33886895
		 -0.33471617 -0.33886895 -0.019293621 -0.49323729 9.5510948e-08 -0.51253104 9.5510948e-08
		 -0.33886895 -0.3540099 -0.31957534 -0.33471617 -0.31957534 -0.019293621 -0.33886895
		 9.5510948e-08 -0.31957534 -0.3540099 -0.31957534 9.5510948e-08 0.76953375 -0.2280677
		 0.76953375 -0.23205984 0.80195022 -0.23205984 0.80195022 -0.2280677 0.80600178 -0.2280677
		 0.80600178 -0.23205984 0.80195022 0.48277417 0.76953375 0.48277417 0.80600178 0.48277417
		 0.76953375 0.48676628 0.80195022 0.48676628 0.80600178 0.48676628 0.68709069 0.48277417
		 0.68709069 -0.2280677 0.68709069 0.48676628 0.65467411 0.48277417 0.65467411 -0.2280677
		 0.68709069 -0.23205984 0.65467411 0.48676628 0.65467411 -0.23205984;
createNode polyLayoutUV -n "polyLayoutUV3";
	rename -uid "9936EC2F-4160-8410-9D43-E89A146CEF04";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".l" 1;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
	setAttr ".rbf" 1;
	setAttr ".lm" 1;
createNode polyLayoutUV -n "polyLayoutUV4";
	rename -uid "A961224D-404F-0AA3-EA3E-019E014226F5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:53]";
	setAttr ".l" 1;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
	setAttr ".rbf" 1;
	setAttr ".lm" 1;
createNode polyAutoProj -n "polyAutoProj9";
	rename -uid "8ADE11F3-4A38-59EA-44C1-608CB6F5E0A8";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:35]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".s" -type "double3" 63.021415710449219 63.021415710449219 63.021415710449219 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweakUV -n "polyTweakUV24";
	rename -uid "3847638E-4F40-8E12-3E4F-4F941C3E155F";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[16:31]" -type "float2" -0.039790466 -1.0430813e-07
		 -0.039790466 -1.0430813e-07 -0.039790466 -6.9849193e-09 -0.039790466 -6.9849193e-09
		 -0.039790466 -4.4703484e-08 -0.039790466 -4.4703484e-08 -0.039790466 -1.4668331e-08
		 -0.039790466 -1.4668331e-08 -0.039790466 -6.9849193e-09 -0.039790466 -1.0430813e-07
		 -0.039790466 -4.4703484e-08 -0.039790466 -1.4668331e-08 -0.039790466 -1.0430813e-07
		 -0.039790466 -6.9849193e-09 -0.039790466 -4.4703484e-08 -0.039790466 -1.4668331e-08;
createNode polyMapSewMove -n "polyMapSewMove16";
	rename -uid "04A7E983-4079-E2E6-CD23-A691A50E8C8E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[16]" "e[63]" "e[81]";
createNode polyTweakUV -n "polyTweakUV25";
	rename -uid "C254246A-48AA-B23F-D25B-AF90B46BF701";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[0:15]" -type "float2" -0.14587247 -1.4901161e-08
		 -0.14587247 -1.4901161e-08 -0.14587247 -6.9849193e-09 -0.14587247 -6.9849193e-09
		 -0.14587247 -1.4901161e-08 -0.14587247 -1.4901161e-08 -0.14587247 -1.4901161e-08
		 -0.14587247 -6.9849193e-09 -0.14587247 -1.4668331e-08 -0.14587247 -1.4668331e-08
		 -0.14587247 -1.4901161e-08 -0.14587247 -1.4901161e-08 -0.14587247 -6.9849193e-09
		 -0.14587247 -1.4668331e-08 -0.14587247 -1.4901161e-08 -0.14587247 -1.4668331e-08;
createNode polyMapSewMove -n "polyMapSewMove17";
	rename -uid "8C0587A4-4C0C-5CB6-B150-07A488B47057";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[1]" "e[60]" "e[78]";
createNode polyTweakUV -n "polyTweakUV26";
	rename -uid "003A6002-40F8-E20D-E997-A49B431FF914";
	setAttr ".uopa" yes;
	setAttr -s 17 ".uvtk";
	setAttr ".uvtk[40]" -type "float2" -0.047152936 -6.6123903e-08 ;
	setAttr ".uvtk[41]" -type "float2" -0.047152951 -6.6123903e-08 ;
	setAttr ".uvtk[42]" -type "float2" -0.047152951 5.2619725e-08 ;
	setAttr ".uvtk[43]" -type "float2" -0.047152936 5.2619725e-08 ;
	setAttr ".uvtk[44]" -type "float2" -0.047152936 -6.6123903e-08 ;
	setAttr ".uvtk[45]" -type "float2" -0.047152951 -6.6123903e-08 ;
	setAttr ".uvtk[46]" -type "float2" -0.047152936 -6.6123903e-08 ;
	setAttr ".uvtk[47]" -type "float2" -0.047152936 5.2619725e-08 ;
	setAttr ".uvtk[48]" -type "float2" -0.047152951 4.4703484e-08 ;
	setAttr ".uvtk[49]" -type "float2" -0.047152936 4.4703484e-08 ;
	setAttr ".uvtk[50]" -type "float2" -0.047152936 -6.6123903e-08 ;
	setAttr ".uvtk[51]" -type "float2" -0.047152936 -6.6123903e-08 ;
	setAttr ".uvtk[52]" -type "float2" -0.047152936 5.2619725e-08 ;
	setAttr ".uvtk[53]" -type "float2" -0.047152936 4.4703484e-08 ;
	setAttr ".uvtk[54]" -type "float2" -0.047152936 -6.6123903e-08 ;
	setAttr ".uvtk[55]" -type "float2" -0.047152936 4.4703484e-08 ;
createNode polyMapSewMove -n "polyMapSewMove18";
	rename -uid "FB887C3B-4D39-98E0-05EB-0298D587A2DF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[38]" "e[69]" "e[76]";
createNode polyTweak -n "polyTweak57";
	rename -uid "06B0467B-40D7-20D8-AB12-088EBE7B2DA4";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[46]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[47]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[59]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[60]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[69]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[70]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[71]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[72]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[73]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[74]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[75]" -type "float3" 0 0 -0.26047733 ;
	setAttr ".tk[76]" -type "float3" 0 0 -0.26047733 ;
createNode polySplit -n "polySplit46";
	rename -uid "D082DE52-4A8F-F968-70FF-C9A467FDBBF5";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483481 -2147483548 -2147483561 -2147483562 -2147483500 -2147483496 
		-2147483503 -2147483573 -2147483574 -2147483587 -2147483483;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit47";
	rename -uid "ED378962-4D1B-937E-F31E-738BCEDBEA4D";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483529 -2147483527 -2147483524 -2147483525 -2147483642 -2147483643 
		-2147483646 -2147483520 -2147483522 -2147483516 -2147483518;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit48";
	rename -uid "59CA8CA3-4419-066C-4636-27BA91DAA5B3";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483482 -2147483589 -2147483598 -2147483597 -2147483639 -2147483648 
		-2147483644 -2147483615 -2147483613 -2147483623 -2147483484;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit49";
	rename -uid "6BEA8F76-4E36-1E6B-1796-9CA0FDB48504";
	setAttr -s 11 ".e[0:10]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 11 ".d[0:10]"  -2147483633 -2147483631 -2147483616 -2147483617 -2147483512 -2147483497 
		-2147483506 -2147483607 -2147483608 -2147483591 -2147483490;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak58";
	rename -uid "8C64F89A-4538-346B-0DDF-97B295CA33DD";
	setAttr ".uopa" yes;
	setAttr -s 46 ".tk";
	setAttr ".tk[0]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[1]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[2]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[4]" -type "float3" 0 -0.14763039 0 ;
	setAttr ".tk[6]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[7]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[8]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[14]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[15]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[20]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[21]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[26]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[27]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[33]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[34]" -type "float3" 0 -0.1632577 0 ;
	setAttr ".tk[77]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[78]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[81]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[82]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[83]" -type "float3" 0 -0.14763039 0 ;
	setAttr ".tk[85]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[86]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[89]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[90]" -type "float3" 0 -0.16865344 0 ;
	setAttr ".tk[97]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[98]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[99]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[100]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[101]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[102]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[103]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[104]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[105]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[106]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[107]" -type "float3" 0 -0.11824104 0 ;
	setAttr ".tk[108]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[109]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[110]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[111]" -type "float3" 0 -0.14772713 0 ;
	setAttr ".tk[112]" -type "float3" 0 -0.14772713 0 ;
	setAttr ".tk[113]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[114]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[115]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[116]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[117]" -type "float3" 0 -9.6720643e-05 0 ;
	setAttr ".tk[118]" -type "float3" 0 -9.6720643e-05 0 ;
createNode polySplit -n "polySplit50";
	rename -uid "1692DC61-4B62-1159-F925-DAA237B19EAE";
	setAttr -s 15 ".e[0:14]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5;
	setAttr -s 15 ".d[0:14]"  -2147483485 -2147483534 -2147483564 -2147483566 -2147483452 -2147483641 
		-2147483432 -2147483645 -2147483410 -2147483638 -2147483390 -2147483568 -2147483569 -2147483536 -2147483538;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit51";
	rename -uid "1E6BB392-4F2A-D747-6152-E1877D0D48BA";
	setAttr -s 15 ".e[0:14]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5;
	setAttr -s 15 ".d[0:14]"  -2147483486 -2147483543 -2147483531 -2147483533 -2147483453 -2147483640 
		-2147483431 -2147483647 -2147483411 -2147483637 -2147483389 -2147483541 -2147483542 -2147483545 -2147483488;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit52";
	rename -uid "749CB24E-4A1F-ED12-6EEB-AC9FC02C6A6F";
	setAttr -s 15 ".e[0:14]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5;
	setAttr -s 15 ".d[0:14]"  -2147483489 -2147483546 -2147483559 -2147483558 -2147483454 -2147483501 
		-2147483430 -2147483491 -2147483412 -2147483505 -2147483388 -2147483611 -2147483610 -2147483593 -2147483595;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit53";
	rename -uid "4434FBEE-4362-9C4E-3B7A-BBB9F96844D6";
	setAttr -s 15 ".e[0:14]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5;
	setAttr -s 15 ".d[0:14]"  -2147483585 -2147483583 -2147483570 -2147483571 -2147483451 -2147483502 
		-2147483433 -2147483494 -2147483409 -2147483513 -2147483391 -2147483620 -2147483621 -2147483635 -2147483487;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyBevel3 -n "polyBevel11";
	rename -uid "F9A25055-43C4-A476-195A-19B79A929BE3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 28 "e[0]" "e[2]" "e[4:6]" "e[9]" "e[29]" "e[54]" "e[64]" "e[81]" "e[83]" "e[91]" "e[104]" "e[108:109]" "e[111]" "e[113]" "e[116]" "e[136]" "e[138:142]" "e[145]" "e[148]" "e[150:153]" "e[184:186]" "e[205:207]" "e[226:228]" "e[247:249]" "e[279:292]" "e[308:321]" "e[337:350]" "e[366:379]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.0672064754216544 0 0 0 0 0.98692533935277804 0
		 0 -1.65922442784799 -0.28623479486675407 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.05;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel12";
	rename -uid "223F5AA4-43EF-7BA1-8C75-749EC5D1ABC3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "e[183]" "e[196]" "e[209]" "e[238]" "e[251]" "e[260]" "e[269]" "e[280]" "e[293]" "e[304]" "e[311]" "e[322]" "e[329]" "e[338]" "e[345]" "e[368]" "e[427]" "e[486]" "e[545]" "e[707:785]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.0672064754216544 0 0 0 0 0.98692533935277804 0
		 0 -1.65922442784799 -0.28623479486675407 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.1;
	setAttr ".c" no;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak59";
	rename -uid "B8344F99-461C-D673-7BDB-5D81939E5897";
	setAttr ".uopa" yes;
	setAttr -s 105 ".tk";
	setAttr ".tk[97]" -type "float3" 0 -0.14128934 2.3283064e-10 ;
	setAttr ".tk[100]" -type "float3" 0 -0.14128934 1.8626451e-09 ;
	setAttr ".tk[103]" -type "float3" 0 -0.14128934 2.3283064e-10 ;
	setAttr ".tk[106]" -type "float3" 0 -0.14128934 -1.8626451e-09 ;
	setAttr ".tk[109]" -type "float3" 0 -0.14128934 2.3283064e-10 ;
	setAttr ".tk[112]" -type "float3" 0 -0.14128934 1.8626451e-09 ;
	setAttr ".tk[115]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[118]" -type "float3" 0 -0.14128934 -1.8626451e-09 ;
	setAttr ".tk[121]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[124]" -type "float3" 0 -0.14128934 -1.8626451e-09 ;
	setAttr ".tk[127]" -type "float3" 0 -0.14128934 -3.7252903e-09 ;
	setAttr ".tk[130]" -type "float3" 0 -0.14128934 1.8626451e-09 ;
	setAttr ".tk[133]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[136]" -type "float3" 0 -0.14128934 -7.4505806e-09 ;
	setAttr ".tk[139]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[142]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[145]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[148]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[151]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[154]" -type "float3" 0 -0.14128934 -7.4505806e-09 ;
	setAttr ".tk[157]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[160]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[163]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[166]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[169]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[172]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[175]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[178]" -type "float3" 0 -0.14128934 -7.4505806e-09 ;
	setAttr ".tk[181]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[184]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[187]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[190]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[193]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[196]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[199]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[202]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[205]" -type "float3" 0 -0.14128934 -3.7252903e-09 ;
	setAttr ".tk[208]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[211]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[214]" -type "float3" 0 -0.14128934 0 ;
	setAttr ".tk[217]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[220]" -type "float3" 0 -0.14128934 0 ;
	setAttr ".tk[223]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[226]" -type "float3" 0 -0.14128934 0 ;
	setAttr ".tk[229]" -type "float3" 0 -0.14128934 -3.7252903e-09 ;
	setAttr ".tk[232]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[235]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[238]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[241]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[244]" -type "float3" 0 -0.14128934 0 ;
	setAttr ".tk[247]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[250]" -type "float3" 0 -0.14128934 -1.8626451e-09 ;
	setAttr ".tk[253]" -type "float3" 0 -0.14128934 2.3283064e-10 ;
	setAttr ".tk[256]" -type "float3" 0 -0.14128934 1.8626451e-09 ;
	setAttr ".tk[259]" -type "float3" 0 -0.14128934 -3.7252903e-09 ;
	setAttr ".tk[262]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[265]" -type "float3" 0 -0.14128934 -7.4505806e-09 ;
	setAttr ".tk[268]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[271]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[274]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[277]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[280]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[283]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[286]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[289]" -type "float3" 0 -0.14128934 0 ;
	setAttr ".tk[292]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[295]" -type "float3" 0 -0.14128934 -1.8626451e-09 ;
	setAttr ".tk[298]" -type "float3" 0 -0.14128934 2.3283064e-10 ;
	setAttr ".tk[301]" -type "float3" 0 -0.14128934 1.8626451e-09 ;
	setAttr ".tk[304]" -type "float3" 0 -0.14128934 -3.7252903e-09 ;
	setAttr ".tk[307]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[310]" -type "float3" 0 -0.14128934 -7.4505806e-09 ;
	setAttr ".tk[313]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[316]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[319]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[322]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[325]" -type "float3" 0 -0.14128934 2.2351742e-08 ;
	setAttr ".tk[328]" -type "float3" 0 -0.14128934 2.2351742e-08 ;
	setAttr ".tk[331]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[334]" -type "float3" 0 -0.14128934 3.7252903e-08 ;
	setAttr ".tk[337]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[340]" -type "float3" 0 -0.14128934 -1.8626451e-09 ;
	setAttr ".tk[343]" -type "float3" 0 -0.14128934 2.3283064e-10 ;
	setAttr ".tk[346]" -type "float3" 0 -0.14128934 1.8626451e-09 ;
	setAttr ".tk[349]" -type "float3" 0 -0.14128934 -3.7252903e-09 ;
	setAttr ".tk[352]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[355]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[358]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[361]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[364]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[367]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[370]" -type "float3" 0 -0.14128934 2.2351742e-08 ;
	setAttr ".tk[373]" -type "float3" 0 -0.14128934 2.2351742e-08 ;
	setAttr ".tk[376]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[379]" -type "float3" 0 -0.14128934 3.7252903e-08 ;
	setAttr ".tk[382]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[385]" -type "float3" 0 -0.14128934 -1.8626451e-09 ;
	setAttr ".tk[388]" -type "float3" 0 -0.14128934 2.3283064e-10 ;
	setAttr ".tk[391]" -type "float3" 0 -0.14128934 1.8626451e-09 ;
	setAttr ".tk[394]" -type "float3" 0 -0.14128934 -3.7252903e-09 ;
	setAttr ".tk[397]" -type "float3" 0 -0.14128934 7.4505806e-09 ;
	setAttr ".tk[400]" -type "float3" 0 -0.14128934 -1.4901161e-08 ;
	setAttr ".tk[403]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[406]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
	setAttr ".tk[409]" -type "float3" 0 -0.14128934 -2.2351742e-08 ;
createNode polyUnite -n "polyUnite1";
	rename -uid "711D0481-4DB0-F57C-20DE-81A6914BD921";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId1";
	rename -uid "DD33B60A-4731-D770-A96A-368408D76A4F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId2";
	rename -uid "27F61059-49B0-D43D-8C62-30B40769A188";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "63A4982D-486F-FDCA-0C66-A7B64A80A2D7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "90415F71-4318-472B-9A8E-E7A146C1C646";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "4EE5C109-4546-7D08-A0A9-04BE483A35D5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "76D316FF-457B-D7D5-EE70-BA83D6DE89FE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:331]";
createNode polyMergeVert -n "polyMergeVert33";
	rename -uid "1AD14287-4E87-5AFC-8A8F-E3A43552DA32";
	setAttr ".ics" -type "componentList" 1 "vtx[0:385]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyTweak -n "polyTweak60";
	rename -uid "3FF0A82B-42BC-EEE9-CBAD-BB813F061713";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[130]" -type "float3" -0.027742989 0.038571741 0.014112017 ;
	setAttr ".tk[305]" -type "float3" 0.027742989 0.038571741 0.014112017 ;
createNode polySplit -n "polySplit54";
	rename -uid "038EE07E-4496-33CC-F7F1-A8B1903CEDB9";
	setAttr -s 5 ".e[0:4]"  0 0.87529999 0.18172701 0.87529999 1;
	setAttr -s 5 ".d[0:4]"  -2147483575 -2147483400 -2147483468 -2147483060 -2147483227;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit55";
	rename -uid "37EE48D5-4793-D2BA-D03A-D7A206E65762";
	setAttr -s 5 ".e[0:4]"  0 0.075542197 0.930022 0.075542197 0;
	setAttr -s 5 ".d[0:4]"  -2147483237 -2147483233 -2147483469 -2147483582 -2147483583;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode deleteComponent -n "deleteComponent9";
	rename -uid "140CD81F-4121-2A93-6F3D-6DA33725CB2F";
	setAttr ".dc" -type "componentList" 2 "e[65]" "e[72]";
createNode deleteComponent -n "deleteComponent10";
	rename -uid "23EC7599-47E3-3015-58CF-0BA39CAED478";
	setAttr ".dc" -type "componentList" 2 "e[412]" "e[419]";
createNode polyMergeVert -n "polyMergeVert34";
	rename -uid "14A25BAA-449A-3860-8C5F-1496C0068B43";
	setAttr ".ics" -type "componentList" 1 "vtx[224:225]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak61";
	rename -uid "5E5B89A6-4855-ACB8-A005-24A07CD32101";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[224:225]" -type "float3"  -0.0099427104 -0.00032043457
		 -0.018071175 0.0099427402 0.00032043457 0.018072128;
createNode polyMergeVert -n "polyMergeVert35";
	rename -uid "C9304882-41A7-95D7-DB5D-28A375659838";
	setAttr ".ics" -type "componentList" 1 "vtx[40:41]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak62";
	rename -uid "10C6DE95-45A5-C3F2-14CB-0FB59C07703E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[40:41]" -type "float3"  0.0099427104 -0.00032043457
		 -0.018071175 -0.0099427402 0.00032043457 0.018072128;
createNode polyMergeVert -n "polyMergeVert36";
	rename -uid "918C46D1-40BB-DB2F-DDEB-4289152F2CA5";
	setAttr ".ics" -type "componentList" 2 "vtx[119]" "vtx[133]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak63";
	rename -uid "0759FE7C-4247-F242-8EEC-A48EF1AADA7A";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk";
	setAttr ".tk[33]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[35]" -type "float3" 0 0 -0.037554085 ;
	setAttr ".tk[39]" -type "float3" 0.012602437 -0.015275513 -0.011044422 ;
	setAttr ".tk[40]" -type "float3" 0 0.03575049 -0.037554089 ;
	setAttr ".tk[47]" -type "float3" 0 0.03575049 -0.037554089 ;
	setAttr ".tk[119]" -type "float3" -0.012918234 0.023715973 0 ;
	setAttr ".tk[129]" -type "float3" 0.020505609 -0.0039296802 -0.037554085 ;
	setAttr ".tk[133]" -type "float3" 0.012918115 -0.023712158 0 ;
	setAttr ".tk[162]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[216]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[217]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[218]" -type "float3" 0 0 -0.037554085 ;
	setAttr ".tk[222]" -type "float3" -0.012602437 -0.015275513 -0.011044422 ;
	setAttr ".tk[223]" -type "float3" 0 0.03575049 -0.037554089 ;
	setAttr ".tk[303]" -type "float3" -0.020505609 -0.0039296802 -0.037554085 ;
	setAttr ".tk[333]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[362]" -type "float3" 0 -0.015275513 0 ;
	setAttr ".tk[363]" -type "float3" 0 -0.015275513 0 ;
	setAttr ".tk[364]" -type "float3" 0 -0.015275513 0 ;
	setAttr ".tk[365]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[366]" -type "float3" 0 0.048995469 0 ;
	setAttr ".tk[367]" -type "float3" 0 0.048995469 0 ;
createNode polyMergeVert -n "polyMergeVert37";
	rename -uid "A0F4D9C6-4547-7BFC-B55D-C0AADD451DA3";
	setAttr ".ics" -type "componentList" 2 "vtx[292]" "vtx[306]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak64";
	rename -uid "21C6E743-4E66-CAFF-4553-FDB56F652776";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[292]" -type "float3" 0.012918234 0.023715973 0 ;
	setAttr ".tk[306]" -type "float3" -0.012918115 -0.023712158 0 ;
createNode polyMergeVert -n "polyMergeVert38";
	rename -uid "D54D38F9-4BD7-810B-2446-DD812C6B7F47";
	setAttr ".ics" -type "componentList" 2 "vtx[290]" "vtx[292]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak65";
	rename -uid "4907E8FE-4EDC-4083-C2B5-9588E639250A";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[290]" -type "float3" -0.024390101 -0.047512054 0 ;
	setAttr ".tk[292]" -type "float3" 0.024390221 0.047512054 0 ;
createNode polyMergeVert -n "polyMergeVert39";
	rename -uid "6AB12304-43D5-5AF0-5A87-319E6876116A";
	setAttr ".ics" -type "componentList" 1 "vtx[292:293]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak66";
	rename -uid "41840216-4A41-F02C-8B61-8F8D86AD8D39";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[292:293]" -type "float3"  0.018748283 0.032230377 5.3405762e-05
		 -0.018748283 -0.032234192 -5.4359436e-05;
createNode polyMergeVert -n "polyMergeVert40";
	rename -uid "F769EC61-4216-38BF-64DF-DE9384120C3B";
	setAttr ".ics" -type "componentList" 2 "vtx[353]" "vtx[355]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak67";
	rename -uid "C94997DF-41D7-A6D1-5B1D-1A95C7D0AF58";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[353]" -type "float3" 0.018748283 0.032230377 5.4359436e-05 ;
	setAttr ".tk[355]" -type "float3" -0.018748283 -0.032234192 -5.4359436e-05 ;
createNode polyMergeVert -n "polyMergeVert41";
	rename -uid "A9E88253-456B-AA0D-4B72-B480C2C98A5D";
	setAttr ".ics" -type "componentList" 2 "vtx[117]" "vtx[119]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak68";
	rename -uid "185DD018-4736-077A-DAB3-679F83912403";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[117]" -type "float3" 0.024390221 -0.047512054 0 ;
	setAttr ".tk[119]" -type "float3" -0.024390101 0.047512054 0 ;
createNode polyMergeVert -n "polyMergeVert42";
	rename -uid "783C49DD-4494-95D8-FAAB-DBA25AC4DA55";
	setAttr ".ics" -type "componentList" 1 "vtx[119:120]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak69";
	rename -uid "E84EFC8F-4F16-AE3E-860B-B7B06E50A66E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[119:120]" -type "float3"  -0.018748283 0.032230377 5.3405762e-05
		 0.018748283 -0.032234192 -5.4359436e-05;
createNode polyMergeVert -n "polyMergeVert43";
	rename -uid "FED2FAAA-4DD6-5CF7-8E48-FB88494427C3";
	setAttr ".ics" -type "componentList" 2 "vtx[184]" "vtx[186]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak70";
	rename -uid "C2E719C0-4587-AAE7-76A4-BCAFD7FC6792";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[184]" -type "float3" -0.018748283 0.032230377 5.4359436e-05 ;
	setAttr ".tk[186]" -type "float3" 0.018748283 -0.032234192 -5.4359436e-05 ;
createNode polyAutoProj -n "polyAutoProj10";
	rename -uid "8757A9A4-4C9A-00E2-7B20-32BCA9A30121";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:327]";
	setAttr ".ix" -type "matrix" 0.93663816698334901 0 0 0 0 0.84531035554551925 0 0
		 0 0 1 0 0 7.6285516716149679 -0.14681804638274798 1;
	setAttr ".s" -type "double3" 6.3562247733645734 6.3562247733645734 6.3562247733645734 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyMapCut -n "polyMapCut2";
	rename -uid "03BCCBE7-4F07-8DAC-3D06-58A5F8758325";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[34]" "e[206]";
createNode polyMapCut -n "polyMapCut3";
	rename -uid "E58A159C-45D5-5CA8-66F6-6EA40B84E9F1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[374]" "e[535]";
createNode polyMapCut -n "polyMapCut4";
	rename -uid "2C7D8302-4B59-B5DC-A51C-C0B01A7D98C4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[202]" "e[374]" "e[535]";
createNode polyMapCut -n "polyMapCut5";
	rename -uid "FD334C13-4105-0133-C2FE-D6AC1FB38AF4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[202]" "e[374]" "e[531]" "e[535]";
createNode polyMapCut -n "polyMapCut6";
	rename -uid "BDCFF0B5-4DD5-9D1A-8E8A-28A87A046127";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[126]" "e[193]" "e[196:197]" "e[216]" "e[227]";
createNode polyMapCut -n "polyMapCut7";
	rename -uid "9AF3B652-4D9C-B74D-B884-45AD9F290A5D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[225]";
createNode polyMapCut -n "polyMapCut8";
	rename -uid "05E99D5B-40B0-E69F-5B5C-6EAC405495E5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[228]";
createNode polyMapCut -n "polyMapCut9";
	rename -uid "072CBA68-41D9-439E-BDDC-C6BA5586AC2E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[210]" "e[539]";
createNode polyMapCut -n "polyMapCut10";
	rename -uid "426A28B5-41F2-0479-86ED-04A01D9F4C5B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[557]";
createNode polyMapCut -n "polyMapCut11";
	rename -uid "2609D9E0-49C1-30C7-A853-6892051ED958";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[554]" "e[556]";
createNode polyMapCut -n "polyMapCut12";
	rename -uid "E163F76E-43E8-A673-D8DC-6D98B9283243";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[461]" "e[522]" "e[525:526]" "e[545]" "e[556]";
createNode polyDelEdge -n "polyDelEdge7";
	rename -uid "D60BFCAF-470A-AC74-EA42-9E94B1B2266C";
	setAttr ".ics" -type "componentList" 2 "e[495]" "e[504]";
	setAttr ".cv" yes;
createNode polySplit -n "polySplit56";
	rename -uid "FBA3AE13-4D2F-18F8-B6E9-23A355F9D7DE";
	setAttr -s 4 ".e[0:3]"  0 0.0088335397 0.0087138098 1;
	setAttr -s 4 ".d[0:3]"  -2147483146 -2147482985 -2147482989 -2147483204;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge8";
	rename -uid "C077165C-4A06-3F8B-2547-3691495958DC";
	setAttr ".ics" -type "componentList" 2 "e[160]" "e[169]";
	setAttr ".cv" yes;
createNode polySplit -n "polySplit57";
	rename -uid "3AA67384-4E18-6EAF-856F-24B0BC129EB1";
	setAttr -s 4 ".e[0:3]"  1 0.0071042599 0.0068232999 0;
	setAttr -s 4 ".d[0:3]"  -2147483539 -2147483313 -2147483309 -2147483481;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV27";
	rename -uid "5AFE5631-4C19-2B84-34F8-FC9C6FF78973";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" -0.17676944 -0.24941109 ;
	setAttr ".uvtk[8]" -type "float2" -0.16361606 -0.25510329 ;
	setAttr ".uvtk[14]" -type "float2" -0.15789622 -0.26836699 ;
	setAttr ".uvtk[15]" -type "float2" -0.17155164 -0.26210704 ;
	setAttr ".uvtk[17]" -type "float2" -0.095658898 -0.34797978 ;
	setAttr ".uvtk[18]" -type "float2" -0.09300822 -0.34178787 ;
	setAttr ".uvtk[90]" -type "float2" 0.049204767 -0.35571361 ;
	setAttr ".uvtk[91]" -type "float2" 0.043047726 -0.34505448 ;
	setAttr ".uvtk[95]" -type "float2" 0.11409271 -0.40300578 ;
	setAttr ".uvtk[96]" -type "float2" 0.11894041 -0.4003672 ;
	setAttr ".uvtk[576]" -type "float2" 0.032288849 -0.34177384 ;
	setAttr ".uvtk[577]" -type "float2" 0.037944078 -0.35206702 ;
createNode polyMapSewMove -n "polyMapSewMove19";
	rename -uid "E81FE6B9-4F7C-25D6-021D-229E62A5F692";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[14]" "e[27:29]" "e[201]" "e[352]" "e[365:367]" "e[528]";
createNode polyTweakUV -n "polyTweakUV28";
	rename -uid "1D94DAA5-4BAE-11FC-DD2D-ADB883B1C780";
	setAttr ".uopa" yes;
	setAttr -s 15 ".uvtk[312:326]" -type "float2" -0.51938009 -0.11878002 -0.52539432
		 -0.11748445 -0.52548337 -0.11789763 -0.51837802 -0.11942822 -0.53140867 -0.11618894
		 -0.53258878 -0.1163671 -0.52557248 -0.11831111 -0.51728499 -0.12009633 -0.51607859
		 -0.11948174 -0.51769787 -0.11911917 -0.53470623 -0.11546916 -0.53308129 -0.11580539
		 -0.53385991 -0.11652589 -0.51431763 -0.12009633 -0.53656405 -0.11530417;
createNode polyMapSewMove -n "polyMapSewMove20";
	rename -uid "732EB541-4526-8A41-7895-938EBD106D6F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[42]" "e[51]" "e[380]" "e[388]";
createNode polyTweakUV -n "polyTweakUV29";
	rename -uid "5C0072C9-4CFD-9063-919D-03B1D8DC8AB0";
	setAttr ".uopa" yes;
	setAttr -s 30 ".uvtk";
	setAttr ".uvtk[312]" -type "float2" 0.6617161 -0.58154905 ;
	setAttr ".uvtk[313]" -type "float2" 0.6617161 -0.58215761 ;
	setAttr ".uvtk[314]" -type "float2" 0.66175789 -0.58215761 ;
	setAttr ".uvtk[315]" -type "float2" 0.66175789 -0.58143866 ;
	setAttr ".uvtk[316]" -type "float2" 0.6617161 -0.58276612 ;
	setAttr ".uvtk[317]" -type "float2" 0.66175789 -0.58287656 ;
	setAttr ".uvtk[318]" -type "float2" 0.66179359 -0.58215761 ;
	setAttr ".uvtk[319]" -type "float2" 0.66179073 -0.5813095 ;
	setAttr ".uvtk[320]" -type "float2" 0.66171521 -0.5812152 ;
	setAttr ".uvtk[321]" -type "float2" 0.66171384 -0.58137935 ;
	setAttr ".uvtk[322]" -type "float2" 0.66171521 -0.58309996 ;
	setAttr ".uvtk[323]" -type "float2" 0.66171384 -0.58293587 ;
	setAttr ".uvtk[324]" -type "float2" 0.66179073 -0.58300567 ;
	setAttr ".uvtk[325]" -type "float2" 0.66176212 -0.58101928 ;
	setAttr ".uvtk[326]" -type "float2" 0.66176212 -0.58329588 ;
	setAttr ".uvtk[526]" -type "float2" 0.66429967 -0.58079785 ;
	setAttr ".uvtk[527]" -type "float2" 0.66358984 -0.58039165 ;
	setAttr ".uvtk[528]" -type "float2" 0.66355252 -0.58054501 ;
	setAttr ".uvtk[529]" -type "float2" 0.66431433 -0.58093441 ;
	setAttr ".uvtk[530]" -type "float2" 0.66250807 -0.58054501 ;
	setAttr ".uvtk[531]" -type "float2" 0.66244394 -0.58039165 ;
	setAttr ".uvtk[532]" -type "float2" 0.66431409 -0.58215761 ;
	setAttr ".uvtk[533]" -type "float2" 0.66354334 -0.58215761 ;
	setAttr ".uvtk[534]" -type "float2" 0.66249043 -0.58215761 ;
	setAttr ".uvtk[535]" -type "float2" 0.66431433 -0.58338082 ;
	setAttr ".uvtk[536]" -type "float2" 0.66355252 -0.58377016 ;
	setAttr ".uvtk[537]" -type "float2" 0.66250807 -0.58377016 ;
	setAttr ".uvtk[538]" -type "float2" 0.66429967 -0.58351731 ;
	setAttr ".uvtk[539]" -type "float2" 0.66358984 -0.58392352 ;
	setAttr ".uvtk[540]" -type "float2" 0.66244394 -0.58392352 ;
createNode polyMapSewMove -n "polyMapSewMove21";
	rename -uid "08A5A7E7-430E-F0EC-8912-A7B73D63B994";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[47]" "e[207]" "e[385]" "e[534]";
createNode polyTweakUV -n "polyTweakUV30";
	rename -uid "4773055C-4DE5-18F0-672D-A7BE86D3BD20";
	setAttr ".uopa" yes;
	setAttr -s 14 ".uvtk";
	setAttr ".uvtk[347]" -type "float2" 0.68888688 -0.84696376 ;
	setAttr ".uvtk[348]" -type "float2" 0.68888569 -0.84801626 ;
	setAttr ".uvtk[349]" -type "float2" 0.72468519 -0.84805536 ;
	setAttr ".uvtk[350]" -type "float2" 0.72468638 -0.8470028 ;
	setAttr ".uvtk[351]" -type "float2" 0.75383508 -0.84764057 ;
	setAttr ".uvtk[352]" -type "float2" 0.72477067 -0.76974362 ;
	setAttr ".uvtk[353]" -type "float2" 0.68897116 -0.76970452 ;
	setAttr ".uvtk[354]" -type "float2" 0.75395608 -0.77015811 ;
	setAttr ".uvtk[355]" -type "float2" 0.64250082 -0.76978517 ;
	setAttr ".uvtk[356]" -type "float2" 0.64245158 -0.84715033 ;
	setAttr ".uvtk[357]" -type "float2" 0.76106679 -0.84764045 ;
	setAttr ".uvtk[358]" -type "float2" 0.76115137 -0.7701543 ;
	setAttr ".uvtk[580]" -type "float2" 0.76106757 -0.84695601 ;
	setAttr ".uvtk[582]" -type "float2" 0.75383615 -0.84696531 ;
createNode polyMapSewMove -n "polyMapSewMove22";
	rename -uid "F1CA5BC6-4210-5C49-1BBD-988716FAC7BA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[372]" "e[501]" "e[516]" "e[526]" "e[531]";
createNode polyMapCut -n "polyMapCut13";
	rename -uid "965E13AA-4A3D-0435-5F93-8083507E047F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[47]" "e[207]" "e[385]" "e[443]" "e[531]" "e[534]" "e[682:683]";
createNode polyTweakUV -n "polyTweakUV31";
	rename -uid "E691CBC9-4268-6B71-F932-368D9455A362";
	setAttr ".uopa" yes;
	setAttr -s 14 ".uvtk";
	setAttr ".uvtk[270]" -type "float2" 0.67545927 -0.46324331 ;
	setAttr ".uvtk[271]" -type "float2" 0.71125889 -0.463204 ;
	setAttr ".uvtk[272]" -type "float2" 0.7112577 -0.46215144 ;
	setAttr ".uvtk[273]" -type "float2" 0.67545813 -0.46219081 ;
	setAttr ".uvtk[274]" -type "float2" 0.67554414 -0.54050303 ;
	setAttr ".uvtk[275]" -type "float2" 0.71134382 -0.54046363 ;
	setAttr ".uvtk[276]" -type "float2" 0.74040794 -0.46256605 ;
	setAttr ".uvtk[277]" -type "float2" 0.62902373 -0.46305713 ;
	setAttr ".uvtk[278]" -type "float2" 0.62907356 -0.5404228 ;
	setAttr ".uvtk[279]" -type "float2" 0.74052948 -0.54004896 ;
	setAttr ".uvtk[280]" -type "float2" 0.74772477 -0.54005277 ;
	setAttr ".uvtk[281]" -type "float2" 0.74763966 -0.46256599 ;
	setAttr ".uvtk[576]" -type "float2" 0.74040878 -0.4631165 ;
	setAttr ".uvtk[578]" -type "float2" 0.74764025 -0.46309474 ;
createNode polyMapSewMove -n "polyMapSewMove23";
	rename -uid "5DA72B49-4E16-9780-E901-5FBE9D30D5E3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[34]" "e[41]" "e[47]" "e[168]" "e[189]" "e[199]" "e[204]" "e[207]" "e[379]" "e[385]" "e[531]" "e[534]";
createNode polyMapCut -n "polyMapCut14";
	rename -uid "8B41E7AE-4848-815B-8D19-F78F9290CA05";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[36]" "e[110]" "e[686:687]";
createNode polyMapCut -n "polyMapCut15";
	rename -uid "1EB9332B-419F-4B26-5CF5-6ABC5E4CFF35";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[211]" "e[214]";
createNode polyTweakUV -n "polyTweakUV32";
	rename -uid "6071EBDF-4E4F-7EF6-C23C-FC882BF9E023";
	setAttr ".uopa" yes;
	setAttr -s 11 ".uvtk";
	setAttr ".uvtk[166]" -type "float2" -0.19241303 -0.50680029 ;
	setAttr ".uvtk[170]" -type "float2" -0.17953581 -0.4747262 ;
	setAttr ".uvtk[171]" -type "float2" -0.18201151 -0.48720285 ;
	setAttr ".uvtk[173]" -type "float2" -0.20292851 -0.51859283 ;
	setAttr ".uvtk[175]" -type "float2" -0.20094949 -0.50614107 ;
	setAttr ".uvtk[176]" -type "float2" -0.21207532 -0.51861787 ;
	setAttr ".uvtk[237]" -type "float2" 0 -1.4901161e-08 ;
	setAttr ".uvtk[244]" -type "float2" 0 -3.7252903e-09 ;
	setAttr ".uvtk[544]" -type "float2" -0.17769721 -0.49208453 ;
	setAttr ".uvtk[545]" -type "float2" -0.17535728 -0.48029205 ;
	setAttr ".uvtk[553]" -type "float2" 0 -1.4901161e-08 ;
createNode polyMapSewMove -n "polyMapSewMove24";
	rename -uid "CE5303C9-4AE0-04E6-D187-00892C2E0C57";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[9]" "e[54]" "e[211]" "e[214]" "e[347]" "e[390]";
createNode polyTweakUV -n "polyTweakUV33";
	rename -uid "068319F2-40EC-49E6-B5CA-48B0C96ECE18";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[135]" -type "float2" -0.41083401 -0.71740746 ;
	setAttr ".uvtk[138]" -type "float2" -0.37910518 -0.75444776 ;
	setAttr ".uvtk[142]" -type "float2" -0.40050131 -0.72277856 ;
	setAttr ".uvtk[252]" -type "float2" -0.56805426 0.014838587 ;
	setAttr ".uvtk[255]" -type "float2" -0.56805426 0.01528485 ;
	setAttr ".uvtk[256]" -type "float2" -0.56833988 0.014768814 ;
	setAttr ".uvtk[540]" -type "float2" -0.56805426 0.014313475 ;
createNode polyMapSewMove -n "polyMapSewMove25";
	rename -uid "7869ABB0-4664-BD94-7426-61993A9DB965";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[465]" "e[479]" "e[482]" "e[484]";
createNode polyMapCut -n "polyMapCut16";
	rename -uid "B6EF97A6-40CD-5526-4EE2-07BABFFBA4EF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[538]";
createNode polyTweakUV -n "polyTweakUV34";
	rename -uid "A7876501-43FA-B995-99A1-81A7EB22140D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[198]" -type "float2" -0.56805432 0.014768928 ;
	setAttr ".uvtk[213]" -type "float2" -0.56833982 0.014313787 ;
	setAttr ".uvtk[526]" -type "float2" -0.56805432 0.013797849 ;
	setAttr ".uvtk[527]" -type "float2" -0.56805432 0.01424399 ;
createNode polyMapSewMove -n "polyMapSewMove26";
	rename -uid "24256B98-444D-6699-5463-34A862B6E162";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[132]" "e[149]";
createNode polyTweakUV -n "polyTweakUV35";
	rename -uid "4F4B6D9F-46F6-96D3-956E-909525EC42FD";
	setAttr ".uopa" yes;
	setAttr -s 48 ".uvtk";
	setAttr ".uvtk[192]" -type "float2" -0.47201642 0.016127154 ;
	setAttr ".uvtk[193]" -type "float2" -0.44099724 0.016127214 ;
	setAttr ".uvtk[194]" -type "float2" -0.44099721 0.016880557 ;
	setAttr ".uvtk[195]" -type "float2" -0.47201645 0.016880497 ;
	setAttr ".uvtk[196]" -type "float2" -0.47205627 0.014541373 ;
	setAttr ".uvtk[197]" -type "float2" -0.43884307 0.014541432 ;
	setAttr ".uvtk[199]" -type "float2" -0.42746571 0.016865358 ;
	setAttr ".uvtk[200]" -type "float2" -0.44099736 0.072180197 ;
	setAttr ".uvtk[201]" -type "float2" -0.47201654 0.072180137 ;
	setAttr ".uvtk[202]" -type "float2" -0.51732874 0.016126975 ;
	setAttr ".uvtk[203]" -type "float2" -0.51732868 0.016880378 ;
	setAttr ".uvtk[204]" -type "float2" -0.51902121 0.014541194 ;
	setAttr ".uvtk[205]" -type "float2" -0.42451143 0.016127273 ;
	setAttr ".uvtk[206]" -type "float2" -0.42451143 0.016850695 ;
	setAttr ".uvtk[207]" -type "float2" -0.42746586 0.072164997 ;
	setAttr ".uvtk[208]" -type "float2" -0.51732886 0.072180018 ;
	setAttr ".uvtk[209]" -type "float2" -0.57595539 0.016126856 ;
	setAttr ".uvtk[210]" -type "float2" -0.57614279 0.016754702 ;
	setAttr ".uvtk[211]" -type "float2" -0.5741998 0.014541075 ;
	setAttr ".uvtk[212]" -type "float2" -0.422034 0.016127273 ;
	setAttr ".uvtk[214]" -type "float2" -0.42196649 0.016865358 ;
	setAttr ".uvtk[215]" -type "float2" -0.42451158 0.072150335 ;
	setAttr ".uvtk[216]" -type "float2" -0.57614291 0.072054312 ;
	setAttr ".uvtk[217]" -type "float2" -0.57915294 0.019003913 ;
	setAttr ".uvtk[218]" -type "float2" -0.5796802 0.018078759 ;
	setAttr ".uvtk[219]" -type "float2" -0.57870692 0.016014948 ;
	setAttr ".uvtk[220]" -type "float2" -0.42196667 0.072164997 ;
	setAttr ".uvtk[221]" -type "float2" -0.57915306 0.072073922 ;
	setAttr ".uvtk[222]" -type "float2" -0.5822224 0.017843649 ;
	setAttr ".uvtk[223]" -type "float2" -0.58023369 0.014541045 ;
	setAttr ".uvtk[224]" -type "float2" -0.61151725 0.014540985 ;
	setAttr ".uvtk[225]" -type "float2" -0.61151725 0.016770646 ;
	setAttr ".uvtk[226]" -type "float2" -0.64280081 0.014540866 ;
	setAttr ".uvtk[227]" -type "float2" -0.6408121 0.01784347 ;
	setAttr ".uvtk[228]" -type "float2" -0.64432752 0.01601474 ;
	setAttr ".uvtk[229]" -type "float2" -0.64335436 0.018078551 ;
	setAttr ".uvtk[230]" -type "float2" -0.64707899 0.016126648 ;
	setAttr ".uvtk[231]" -type "float2" -0.64883471 0.014540896 ;
	setAttr ".uvtk[233]" -type "float2" -0.70401323 0.014540717 ;
	setAttr ".uvtk[237]" -type "float2" -0.75101811 0.016126394 ;
	setAttr ".uvtk[238]" -type "float2" -0.75097823 0.014540613 ;
	setAttr ".uvtk[244]" -type "float2" -0.78419137 0.014540493 ;
	setAttr ".uvtk[520]" -type "float2" -0.42742655 0.016127273 ;
	setAttr ".uvtk[521]" -type "float2" -0.44099724 0.016127214 ;
	setAttr ".uvtk[522]" -type "float2" -0.51732874 0.016126975 ;
	setAttr ".uvtk[523]" -type "float2" -0.47201642 0.016127154 ;
	setAttr ".uvtk[536]" -type "float2" -0.78203732 0.016126275 ;
	setAttr ".uvtk[537]" -type "float2" -0.70570576 0.016126484 ;
createNode polyMapSewMove -n "polyMapSewMove27";
	rename -uid "62326531-416E-BA3D-A60C-A5B1F520BAFF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[68]" "e[73]" "e[118]" "e[127:128]";
createNode polyTweakUV -n "polyTweakUV36";
	rename -uid "2B143A21-447B-CC16-C917-218CF9AAFD01";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[60]" -type "float2" -0.60814881 -0.017287552 ;
	setAttr ".uvtk[519]" -type "float2" -0.61294043 -0.0089098215 ;
	setAttr ".uvtk[520]" -type "float2" -0.58121163 -0.0339517 ;
createNode polyMapSewMove -n "polyMapSewMove28";
	rename -uid "5D059377-461B-56BB-2572-6ABC8AA3ACDF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[146]" "e[151]";
createNode polyMapCut -n "polyMapCut17";
	rename -uid "B9BC4346-4E59-DC85-5EE0-70825C9B84CE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[214]";
createNode polyTweakUV -n "polyTweakUV37";
	rename -uid "9593F333-4024-EFEB-22ED-3E9D4962377D";
	setAttr ".uopa" yes;
	setAttr -s 130 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" -0.96953547 -0.0017137378 ;
	setAttr ".uvtk[8]" -type "float2" -0.9704411 0.0040098429 ;
	setAttr ".uvtk[14]" -type "float2" -0.96799505 0.0094064921 ;
	setAttr ".uvtk[15]" -type "float2" -0.96615094 0.0032827556 ;
	setAttr ".uvtk[17]" -type "float2" -0.95471251 0.050362647 ;
	setAttr ".uvtk[18]" -type "float2" -0.95802724 0.049643129 ;
	setAttr ".uvtk[90]" -type "float2" -0.90890956 0.0042324662 ;
	setAttr ".uvtk[91]" -type "float2" -0.91178989 -0.0014775693 ;
	setAttr ".uvtk[95]" -type "float2" -0.91173232 0.045589149 ;
	setAttr ".uvtk[96]" -type "float2" -0.91487151 0.046873778 ;
	setAttr ".uvtk[272]" -type "float2" -1.0306203 0.029174745 ;
	setAttr ".uvtk[273]" -type "float2" -1.0004587 0.021755576 ;
	setAttr ".uvtk[276]" -type "float2" -1.0549791 0.046917796 ;
	setAttr ".uvtk[281]" -type "float2" -1.0610471 0.051272303 ;
	setAttr ".uvtk[294]" -type "float2" -0.10458743 -0.58544624 ;
	setAttr ".uvtk[295]" -type "float2" -0.1045249 -0.58477008 ;
	setAttr ".uvtk[296]" -type "float2" -0.10504402 -0.58416605 ;
	setAttr ".uvtk[297]" -type "float2" -0.10547654 -0.58507693 ;
	setAttr ".uvtk[298]" -type "float2" -0.10427883 -0.58462989 ;
	setAttr ".uvtk[299]" -type "float2" -0.10486101 -0.58411682 ;
	setAttr ".uvtk[300]" -type "float2" -0.10487279 -0.5932374 ;
	setAttr ".uvtk[301]" -type "float2" -0.10546923 -0.5932374 ;
	setAttr ".uvtk[302]" -type "float2" -0.090164416 -0.58462989 ;
	setAttr ".uvtk[303]" -type "float2" -0.090153605 -0.58411682 ;
	setAttr ".uvtk[304]" -type "float2" -0.10547654 -0.60139799 ;
	setAttr ".uvtk[305]" -type "float2" -0.10458743 -0.60102868 ;
	setAttr ".uvtk[306]" -type "float2" -0.10504402 -0.60230887 ;
	setAttr ".uvtk[312]" -type "float2" -0.95228803 0.052398831 ;
	setAttr ".uvtk[313]" -type "float2" -0.93459666 0.050848931 ;
	setAttr ".uvtk[314]" -type "float2" -0.93449384 0.052020922 ;
	setAttr ".uvtk[315]" -type "float2" -0.95539463 0.053852886 ;
	setAttr ".uvtk[316]" -type "float2" -0.91690534 0.049298942 ;
	setAttr ".uvtk[317]" -type "float2" -0.91359305 0.050188974 ;
	setAttr ".uvtk[318]" -type "float2" -0.934403 0.053057402 ;
	setAttr ".uvtk[319]" -type "float2" -0.95906603 0.0551368 ;
	setAttr ".uvtk[320]" -type "float2" -0.96201658 0.052919894 ;
	setAttr ".uvtk[321]" -type "float2" -0.95722723 0.052812964 ;
	setAttr ".uvtk[322]" -type "float2" -0.90725064 0.047989771 ;
	setAttr ".uvtk[323]" -type "float2" -0.91196382 0.048871413 ;
	setAttr ".uvtk[324]" -type "float2" -0.90975428 0.050814733 ;
	setAttr ".uvtk[325]" -type "float2" -0.96757567 0.055043131 ;
	setAttr ".uvtk[326]" -type "float2" -0.90139061 0.049242184 ;
	setAttr ".uvtk[327]" -type "float2" -0.95074105 0.11533394 ;
	setAttr ".uvtk[328]" -type "float2" -0.95547539 0.11583933 ;
	setAttr ".uvtk[329]" -type "float2" -0.9586817 -0.010244355 ;
	setAttr ".uvtk[330]" -type "float2" -0.95392776 -0.0099793822 ;
	setAttr ".uvtk[344]" -type "float2" -0.87479365 0.010751426 ;
	setAttr ".uvtk[345]" -type "float2" -0.84380162 0.012815408 ;
	setAttr ".uvtk[346]" -type "float2" -0.8445366 0.017116427 ;
	setAttr ".uvtk[347]" -type "float2" -0.81672978 0.026054449 ;
	setAttr ".uvtk[353]" -type "float2" -0.80999726 0.029288046 ;
	setAttr ".uvtk[367]" -type "float2" -0.8942855 0.10949476 ;
	setAttr ".uvtk[368]" -type "float2" -0.89894706 0.11046433 ;
	setAttr ".uvtk[369]" -type "float2" -0.92591476 -0.012476191 ;
	setAttr ".uvtk[370]" -type "float2" -0.92127532 -0.013546824 ;
	setAttr ".uvtk[435]" -type "float2" -0.96769089 -0.038587421 ;
	setAttr ".uvtk[436]" -type "float2" -0.96339512 -0.037614137 ;
	setAttr ".uvtk[437]" -type "float2" -0.95365334 -0.065382108 ;
	setAttr ".uvtk[438]" -type "float2" -0.95057684 -0.065191075 ;
	setAttr ".uvtk[439]" -type "float2" -0.94262075 -0.040782779 ;
	setAttr ".uvtk[440]" -type "float2" -0.93888998 0.0018217117 ;
	setAttr ".uvtk[441]" -type "float2" -0.9504348 -0.068458512 ;
	setAttr ".uvtk[442]" -type "float2" -0.94851625 -0.069561556 ;
	setAttr ".uvtk[443]" -type "float2" -0.94479072 -0.065563187 ;
	setAttr ".uvtk[444]" -type "float2" -0.92161191 -0.041273043 ;
	setAttr ".uvtk[445]" -type "float2" -0.93485868 0.047858104 ;
	setAttr ".uvtk[446]" -type "float2" -0.94524598 -0.070760816 ;
	setAttr ".uvtk[447]" -type "float2" -0.93902802 -0.066202402 ;
	setAttr ".uvtk[448]" -type "float2" -0.90932536 -0.0069862604 ;
	setAttr ".uvtk[449]" -type "float2" -0.91755068 -0.042978138 ;
	setAttr ".uvtk[450]" -type "float2" -0.94181693 -0.070148215 ;
	setAttr ".uvtk[451]" -type "float2" -0.93603146 -0.066925228 ;
	setAttr ".uvtk[452]" -type "float2" -0.93973577 -0.069395393 ;
	setAttr ".uvtk[453]" -type "float2" -0.96140277 0.050874054 ;
	setAttr ".uvtk[454]" -type "float2" -0.96246195 0.045659095 ;
	setAttr ".uvtk[455]" -type "float2" -0.90805769 0.040895015 ;
	setAttr ".uvtk[456]" -type "float2" -0.90819418 0.046214655 ;
	setAttr ".uvtk[457]" -type "float2" -1.0014584 0.020780176 ;
	setAttr ".uvtk[458]" -type "float2" -0.97189236 0.0081546307 ;
	setAttr ".uvtk[459]" -type "float2" -0.90528917 0.0023222864 ;
	setAttr ".uvtk[460]" -type "float2" -0.87397879 0.009616971 ;
	setAttr ".uvtk[461]" -type "float2" -1.0024103 0.025120378 ;
	setAttr ".uvtk[462]" -type "float2" -1.0334066 0.021787047 ;
	setAttr ".uvtk[463]" -type "float2" -1.0064836 -0.008839488 ;
	setAttr ".uvtk[464]" -type "float2" -0.97394001 0.0024989843 ;
	setAttr ".uvtk[465]" -type "float2" -0.90425575 -0.0036031455 ;
	setAttr ".uvtk[466]" -type "float2" -0.90743864 -0.0015071481 ;
	setAttr ".uvtk[467]" -type "float2" -0.87417811 -0.020425268 ;
	setAttr ".uvtk[468]" -type "float2" -0.84234172 0.005055733 ;
	setAttr ".uvtk[469]" -type "float2" -0.87228703 0.013725534 ;
	setAttr ".uvtk[470]" -type "float2" -1.0532422 0.039886087 ;
	setAttr ".uvtk[471]" -type "float2" -1.0324237 -0.004670471 ;
	setAttr ".uvtk[472]" -type "float2" -1.0057725 -0.013317376 ;
	setAttr ".uvtk[473]" -type "float2" -0.8756566 -0.024711452 ;
	setAttr ".uvtk[474]" -type "float2" -0.84790808 -0.020828277 ;
	setAttr ".uvtk[475]" -type "float2" -0.81966245 0.019431859 ;
	setAttr ".uvtk[476]" -type "float2" -1.0585368 0.044522464 ;
	setAttr ".uvtk[477]" -type "float2" -1.0500897 0.016617417 ;
	setAttr ".uvtk[478]" -type "float2" -1.0346239 -0.0086801648 ;
	setAttr ".uvtk[479]" -type "float2" -0.84643835 -0.025159299 ;
	setAttr ".uvtk[480]" -type "float2" -0.82681108 -0.0029347762 ;
	setAttr ".uvtk[481]" -type "float2" -0.81364262 0.023077458 ;
	setAttr ".uvtk[482]" -type "float2" -1.0529991 0.016441107 ;
	setAttr ".uvtk[483]" -type "float2" -0.82397658 -0.0036140606 ;
	setAttr ".uvtk[484]" -type "float2" -0.96754754 0.12937942 ;
	setAttr ".uvtk[485]" -type "float2" -0.98116511 0.10977849 ;
	setAttr ".uvtk[486]" -type "float2" -0.97680223 0.10830295 ;
	setAttr ".uvtk[487]" -type "float2" -0.96354073 0.12945804 ;
	setAttr ".uvtk[488]" -type "float2" -0.97946358 0.077937692 ;
	setAttr ".uvtk[489]" -type "float2" -0.98408496 0.076464176 ;
	setAttr ".uvtk[490]" -type "float2" -0.92798066 0.12633446 ;
	setAttr ".uvtk[491]" -type "float2" -0.92994457 0.10392645 ;
	setAttr ".uvtk[492]" -type "float2" -0.93262732 0.073316962 ;
	setAttr ".uvtk[493]" -type "float2" -0.89241934 0.12322471 ;
	setAttr ".uvtk[494]" -type "float2" -0.88304013 0.10008521 ;
	setAttr ".uvtk[495]" -type "float2" -0.88570148 0.069719955 ;
	setAttr ".uvtk[496]" -type "float2" -0.88848746 0.12245023 ;
	setAttr ".uvtk[497]" -type "float2" -0.87848705 0.10077938 ;
	setAttr ".uvtk[498]" -type "float2" -0.88140684 0.067465022 ;
	setAttr ".uvtk[499]" -type "float2" 0.015242457 -1.7881393e-07 ;
	setAttr ".uvtk[500]" -type "float2" 0.0077978373 0.00052469969 ;
	setAttr ".uvtk[501]" -type "float2" 0.0077594519 -2.9802322e-07 ;
	setAttr ".uvtk[536]" -type "float2" -0.8155728 0.02636043 ;
	setAttr ".uvtk[538]" -type "float2" -0.80838752 0.028653763 ;
	setAttr ".uvtk[539]" -type "float2" -0.88013446 0.0058481693 ;
	setAttr ".uvtk[540]" -type "float2" -1.0561256 0.047304988 ;
	setAttr ".uvtk[541]" -type "float2" -1.0628181 0.050783575 ;
	setAttr ".uvtk[542]" -type "float2" -1.0291489 0.033282429 ;
	setAttr ".uvtk[543]" -type "float2" -0.99605131 0.015998155 ;
	setAttr ".uvtk[545]" -type "float2" -0.1045249 -0.60170478 ;
createNode polyMapSewMove -n "polyMapSewMove29";
	rename -uid "C1F91146-4A7A-0AE8-E8CA-3AB26645570C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[76]" "e[78]" "e[212]" "e[413]" "e[539]" "e[668:671]" "e[675:678]";
createNode polyMapSewMove -n "polyMapSewMove30";
	rename -uid "A194B167-4963-49A2-2193-71A082CBC206";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[404]" "e[409]" "e[451]" "e[461]";
createNode polyMapCut -n "polyMapCut18";
	rename -uid "9E0832E0-41BF-978E-1855-71BC0CD90E6A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[100]" "e[306]";
createNode polyTweakUV -n "polyTweakUV38";
	rename -uid "BF57D15B-4C4A-EC51-C6B4-CFA6A4D09CA1";
	setAttr ".uopa" yes;
	setAttr -s 9 ".uvtk";
	setAttr ".uvtk[178]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[179]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[183]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[184]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[188]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[189]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[190]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[191]" -type "float2" -0.03809315 -0.2809096 ;
	setAttr ".uvtk[522]" -type "float2" -0.03809315 -0.2809096 ;
createNode polyMapSewMove -n "polyMapSewMove31";
	rename -uid "4E28E0B8-45DB-183D-F69C-49995D19B76D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[546]" "e[625]";
createNode polyTweakUV -n "polyTweakUV39";
	rename -uid "F42DAABA-40C7-79E1-9E1B-468EE4F823FC";
	setAttr ".uopa" yes;
	setAttr -s 9 ".uvtk";
	setAttr ".uvtk[177]" -type "float2" 0.18606916 -0.28090957 ;
	setAttr ".uvtk[180]" -type "float2" 0.18606916 -0.28090963 ;
	setAttr ".uvtk[181]" -type "float2" 0.18606916 -0.28090543 ;
	setAttr ".uvtk[182]" -type "float2" 0.18606892 -0.28090543 ;
	setAttr ".uvtk[185]" -type "float2" 0.18606928 -0.28090963 ;
	setAttr ".uvtk[186]" -type "float2" 0.18606928 -0.28090957 ;
	setAttr ".uvtk[187]" -type "float2" 0.18606928 -0.28090543 ;
	setAttr ".uvtk[518]" -type "float2" 0.18606892 -0.28090957 ;
	setAttr ".uvtk[520]" -type "float2" 0.18606892 -0.28090963 ;
createNode polyMapSewMove -n "polyMapSewMove32";
	rename -uid "661CA668-4848-9627-527C-458083A728C7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[219]" "e[302]";
createNode polyTweakUV -n "polyTweakUV40";
	rename -uid "B03A15A8-4CFB-99C0-5A0E-E7BB5C0757BD";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[68]" -type "float2" -0.28177652 0.40298384 ;
	setAttr ".uvtk[69]" -type "float2" -0.28178036 0.40298384 ;
	setAttr ".uvtk[70]" -type "float2" -0.2817803 0.40298373 ;
	setAttr ".uvtk[71]" -type "float2" -0.28177646 0.40298373 ;
	setAttr ".uvtk[72]" -type "float2" -0.28178144 0.40298378 ;
	setAttr ".uvtk[73]" -type "float2" -0.2817803 0.40297496 ;
	setAttr ".uvtk[74]" -type "float2" -0.28177646 0.40297496 ;
	setAttr ".uvtk[75]" -type "float2" -0.28177327 0.40298384 ;
	setAttr ".uvtk[76]" -type "float2" -0.28177327 0.40298373 ;
	setAttr ".uvtk[77]" -type "float2" -0.28178144 0.40297508 ;
	setAttr ".uvtk[78]" -type "float2" -0.28177327 0.40297496 ;
	setAttr ".uvtk[502]" -type "float2" -0.28178144 0.40298373 ;
createNode polyMapSewMove -n "polyMapSewMove33";
	rename -uid "B57DF359-44C7-D1AF-280C-2B9AA3FA5DD0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[108]" "e[308]";
createNode polyTweakUV -n "polyTweakUV41";
	rename -uid "B2013D82-4457-3C4B-1673-6C8CAD5577C5";
	setAttr ".uopa" yes;
	setAttr -s 21 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.34386259 0.47448993 ;
	setAttr ".uvtk[1]" -type "float2" -0.34386259 0.47448695 ;
	setAttr ".uvtk[2]" -type "float2" -0.34386253 0.47448695 ;
	setAttr ".uvtk[4]" -type "float2" -0.34386763 0.47448993 ;
	setAttr ".uvtk[5]" -type "float2" -0.34386763 0.47448695 ;
	setAttr ".uvtk[6]" -type "float2" -0.34386259 0.47448486 ;
	setAttr ".uvtk[7]" -type "float2" -0.34386253 0.47448492 ;
	setAttr ".uvtk[9]" -type "float2" -0.34386283 0.47449034 ;
	setAttr ".uvtk[10]" -type "float2" -0.34386781 0.47449034 ;
	setAttr ".uvtk[11]" -type "float2" -0.34386763 0.47448486 ;
	setAttr ".uvtk[12]" -type "float2" -0.34386259 0.47448468 ;
	setAttr ".uvtk[13]" -type "float2" -0.34386253 0.47448468 ;
	setAttr ".uvtk[16]" -type "float2" -0.34386763 0.47448468 ;
	setAttr ".uvtk[177]" -type "float2" -0.34386259 0.47448456 ;
	setAttr ".uvtk[180]" -type "float2" -0.34386253 0.4744845 ;
	setAttr ".uvtk[181]" -type "float2" -0.34386763 0.47448456 ;
	setAttr ".uvtk[182]" -type "float2" -0.34386763 0.47448426 ;
	setAttr ".uvtk[476]" -type "float2" -0.34386253 0.47448993 ;
	setAttr ".uvtk[477]" -type "float2" -0.34386271 0.47449034 ;
	setAttr ".uvtk[512]" -type "float2" -0.34386259 0.47448426 ;
	setAttr ".uvtk[514]" -type "float2" -0.34386253 0.47448426 ;
createNode polyMapSewMove -n "polyMapSewMove34";
	rename -uid "4C9FCC8D-48B3-CB5A-51ED-A98E2C400560";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[202]" "e[261]";
createNode polyTweakUV -n "polyTweakUV42";
	rename -uid "C245391D-40E7-71A2-5C0A-DCBB23E6BB59";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[316:327]" -type "float2" 0.13729042 -0.47449058 0.13729042
		 -0.47449049 0.13728809 -0.47449049 0.13728809 -0.47449058 0.13729286 -0.47449058
		 0.1372928 -0.47449052 0.13729042 -0.47448549 0.13728809 -0.47448549 0.13729316 -0.47449031
		 0.13729316 -0.47449037 0.1372928 -0.47448549 0.13729316 -0.47448531;
createNode polyMapSewMove -n "polyMapSewMove35";
	rename -uid "935A41AB-4690-1033-EA5B-E788D53E8176";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[529]" "e[584]";
createNode polyTweakUV -n "polyTweakUV43";
	rename -uid "870B7208-4D53-DB57-624A-4096F2E22543";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[147]" -type "float2" -0.14240122 -0.19696471 ;
	setAttr ".uvtk[148]" -type "float2" -0.14240104 -0.19696441 ;
	setAttr ".uvtk[149]" -type "float2" -0.1424107 -0.19696447 ;
	setAttr ".uvtk[150]" -type "float2" -0.14241076 -0.19696471 ;
	setAttr ".uvtk[151]" -type "float2" -0.14239305 -0.19696471 ;
	setAttr ".uvtk[152]" -type "float2" -0.14239305 -0.19696447 ;
	setAttr ".uvtk[153]" -type "float2" -0.14240104 -0.19694257 ;
	setAttr ".uvtk[154]" -type "float2" -0.1424107 -0.19694263 ;
	setAttr ".uvtk[155]" -type "float2" -0.14241344 -0.19696459 ;
	setAttr ".uvtk[156]" -type "float2" -0.14239305 -0.19694257 ;
	setAttr ".uvtk[157]" -type "float2" -0.14241344 -0.19694269 ;
	setAttr ".uvtk[490]" -type "float2" -0.14241344 -0.19696441 ;
createNode polyMapSewMove -n "polyMapSewMove36";
	rename -uid "A4D55D4C-4F6D-DEF5-FC0D-59B7D972F017";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[441]" "e[630]";
createNode polyTweakUV -n "polyTweakUV44";
	rename -uid "F1E43B28-4856-4A14-E81B-179F13935660";
	setAttr ".uopa" yes;
	setAttr -s 49 ".uvtk";
	setAttr ".uvtk[79]" -type "float2" -0.87681854 -1.1218789 ;
	setAttr ".uvtk[80]" -type "float2" -0.88038635 -1.121894 ;
	setAttr ".uvtk[81]" -type "float2" -0.88038635 -1.2863966 ;
	setAttr ".uvtk[82]" -type "float2" -0.87663376 -1.2863966 ;
	setAttr ".uvtk[83]" -type "float2" -0.86642087 -1.099106 ;
	setAttr ".uvtk[84]" -type "float2" -0.86969554 -1.099106 ;
	setAttr ".uvtk[85]" -type "float2" -0.88038635 -1.3979275 ;
	setAttr ".uvtk[86]" -type "float2" -0.87640303 -1.3996242 ;
	setAttr ".uvtk[87]" -type "float2" -0.60117716 -1.2863966 ;
	setAttr ".uvtk[88]" -type "float2" -0.60136187 -1.1218789 ;
	setAttr ".uvtk[89]" -type "float2" -0.59097135 -1.0991068 ;
	setAttr ".uvtk[92]" -type "float2" -0.87641728 -1.4101185 ;
	setAttr ".uvtk[93]" -type "float2" -0.88038635 -1.4101185 ;
	setAttr ".uvtk[94]" -type "float2" -0.60094637 -1.3996242 ;
	setAttr ".uvtk[97]" -type "float2" -0.60096061 -1.4101194 ;
	setAttr ".uvtk[147]" -type "float2" -0.88037026 -0.71901554 ;
	setAttr ".uvtk[148]" -type "float2" -0.87642801 -0.721385 ;
	setAttr ".uvtk[149]" -type "float2" -0.87730181 -0.59946227 ;
	setAttr ".uvtk[150]" -type "float2" -0.88037211 -0.59865135 ;
	setAttr ".uvtk[151]" -type "float2" -0.88037467 -0.82156527 ;
	setAttr ".uvtk[152]" -type "float2" -0.87662244 -0.82156527 ;
	setAttr ".uvtk[153]" -type "float2" -0.60103422 -0.72138071 ;
	setAttr ".uvtk[154]" -type "float2" -0.60190701 -0.59945804 ;
	setAttr ".uvtk[155]" -type "float2" -0.87875241 -0.56513286 ;
	setAttr ".uvtk[156]" -type "float2" -0.60120434 -0.82156384 ;
	setAttr ".uvtk[157]" -type "float2" -0.60254836 -0.56512862 ;
	setAttr ".uvtk[178]" -type "float2" -0.876634 -1.4336343 ;
	setAttr ".uvtk[179]" -type "float2" -0.88038653 -1.4336343 ;
	setAttr ".uvtk[183]" -type "float2" -0.87651825 -1.4178963 ;
	setAttr ".uvtk[184]" -type "float2" -0.88038635 -1.4189093 ;
	setAttr ".uvtk[185]" -type "float2" -0.60106158 -1.4178983 ;
	setAttr ".uvtk[316]" -type "float2" -0.88038146 -0.94919562 ;
	setAttr ".uvtk[317]" -type "float2" -0.87662905 -0.94919562 ;
	setAttr ".uvtk[318]" -type "float2" -0.88038212 -1.0820177 ;
	setAttr ".uvtk[319]" -type "float2" -0.87726802 -1.0798823 ;
	setAttr ".uvtk[320]" -type "float2" -0.60118663 -0.94919717 ;
	setAttr ".uvtk[321]" -type "float2" -0.60182595 -1.0798836 ;
	setAttr ".uvtk[322]" -type "float2" -0.9296214 0.36263925 ;
	setAttr ".uvtk[327]" -type "float2" -0.69393605 0.19573365 ;
	setAttr ".uvtk[328]" -type "float2" -0.67192692 0.3151412 ;
	setAttr ".uvtk[329]" -type "float2" -0.71315551 0.098620877 ;
	setAttr ".uvtk[330]" -type "float2" -0.64379555 0.47022122 ;
	setAttr ".uvtk[331]" -type "float2" -0.90186489 0.517667 ;
	setAttr ".uvtk[333]" -type "float2" -0.7175402 0.074614197 ;
	setAttr ".uvtk[486]" -type "float2" -0.97370857 0.12183105 ;
	setAttr ".uvtk[487]" -type "float2" -0.87631279 -0.56513286 ;
	setAttr ".uvtk[488]" -type "float2" -0.96932042 0.14596176 ;
	setAttr ".uvtk[493]" -type "float2" -0.95163053 0.24323179 ;
	setAttr ".uvtk[504]" -type "float2" -0.60117733 -1.4336363 ;
createNode polyMapSewMove -n "polyMapSewMove37";
	rename -uid "320D6BE3-42C1-F351-0DE6-158E55D154FF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[661]" "e[679]";
createNode polyTweakUV -n "polyTweakUV45";
	rename -uid "49FDB74E-480A-765F-439C-2FAE6192FF9D";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[292]" -type "float2" 0.20991202 0.12953058 ;
	setAttr ".uvtk[293]" -type "float2" 0.20992331 0.12955251 ;
	setAttr ".uvtk[294]" -type "float2" 0.20990376 0.12953529 ;
	setAttr ".uvtk[295]" -type "float2" 0.20942961 0.12955251 ;
	setAttr ".uvtk[296]" -type "float2" 0.20942996 0.12953529 ;
	setAttr ".uvtk[496]" -type "float2" 0.20992945 0.12955084 ;
createNode polyMapSewMove -n "polyMapSewMove38";
	rename -uid "BCEA3EAB-4A12-2368-87BB-609027F27013";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[397]" "e[605]";
createNode polyTweakUV -n "polyTweakUV46";
	rename -uid "26B900C3-475E-6B8B-35B7-F38DF99BF596";
	setAttr ".uopa" yes;
	setAttr -s 17 ".uvtk";
	setAttr ".uvtk[114]" -type "float2" 0.25230208 -0.71235561 ;
	setAttr ".uvtk[115]" -type "float2" 0.25229594 -0.71235406 ;
	setAttr ".uvtk[116]" -type "float2" 0.25229558 -0.71251297 ;
	setAttr ".uvtk[117]" -type "float2" 0.25230232 -0.71251297 ;
	setAttr ".uvtk[118]" -type "float2" 0.25229558 -0.71276957 ;
	setAttr ".uvtk[119]" -type "float2" 0.25230232 -0.71276957 ;
	setAttr ".uvtk[120]" -type "float2" 0.25279585 -0.71251297 ;
	setAttr ".uvtk[121]" -type "float2" 0.25279436 -0.71235561 ;
	setAttr ".uvtk[122]" -type "float2" 0.25229558 -0.71291679 ;
	setAttr ".uvtk[123]" -type "float2" 0.25230142 -0.71291536 ;
	setAttr ".uvtk[124]" -type "float2" 0.25279585 -0.71276957 ;
	setAttr ".uvtk[125]" -type "float2" 0.25230083 -0.712942 ;
	setAttr ".uvtk[126]" -type "float2" 0.25279495 -0.71291536 ;
	setAttr ".uvtk[127]" -type "float2" 0.25279501 -0.71294451 ;
	setAttr ".uvtk[292]" -type "float2" 0.25231358 -0.7123338 ;
	setAttr ".uvtk[293]" -type "float2" 0.25232181 -0.71233857 ;
	setAttr ".uvtk[294]" -type "float2" 0.25279304 -0.71233845 ;
createNode polyMapSewMove -n "polyMapSewMove39";
	rename -uid "B3542545-4232-3C01-B823-F48DEE481756";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[540]" "e[604]";
createNode polyTweakUV -n "polyTweakUV47";
	rename -uid "C9DBD40F-4EF7-56BE-581B-D7AD7D0FEE2D";
	setAttr ".uopa" yes;
	setAttr -s 18 ".uvtk";
	setAttr ".uvtk[128]" -type "float2" 0.13135104 -0.69535697 ;
	setAttr ".uvtk[129]" -type "float2" 0.13134722 -0.69535744 ;
	setAttr ".uvtk[130]" -type "float2" 0.13134716 -0.69542116 ;
	setAttr ".uvtk[131]" -type "float2" 0.13135104 -0.69542116 ;
	setAttr ".uvtk[132]" -type "float2" 0.13106017 -0.69535744 ;
	setAttr ".uvtk[133]" -type "float2" 0.13106011 -0.69542247 ;
	setAttr ".uvtk[134]" -type "float2" 0.13134699 -0.69544035 ;
	setAttr ".uvtk[136]" -type "float2" 0.13105993 -0.69544035 ;
	setAttr ".uvtk[137]" -type "float2" 0.13134681 -0.69553256 ;
	setAttr ".uvtk[139]" -type "float2" 0.13105975 -0.69553256 ;
	setAttr ".uvtk[140]" -type "float2" 0.13134716 -0.695701 ;
	setAttr ".uvtk[141]" -type "float2" 0.13135104 -0.695701 ;
	setAttr ".uvtk[143]" -type "float2" 0.13106011 -0.695701 ;
	setAttr ".uvtk[144]" -type "float2" 0.13134716 -0.69586861 ;
	setAttr ".uvtk[145]" -type "float2" 0.13135104 -0.69586861 ;
	setAttr ".uvtk[146]" -type "float2" 0.13106011 -0.69586861 ;
	setAttr ".uvtk[471]" -type "float2" 0.13135104 -0.69544035 ;
	setAttr ".uvtk[472]" -type "float2" 0.13135104 -0.69553256 ;
createNode polyMapSewMove -n "polyMapSewMove40";
	rename -uid "A51FDE8E-46E2-5770-7E56-269E8E76766D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[486]" "e[649]";
createNode polyTweakUV -n "polyTweakUV48";
	rename -uid "E7CC4DB5-4818-1996-BF72-3A9E45F4BEC2";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[326:337]" -type "float2" 0.34341463 -0.86716872 0.34341851
		 -0.86716872 0.34341851 -0.86688226 0.34341463 -0.86688226 0.34341505 -0.86735696
		 0.34341851 -0.86736017 0.34313026 -0.86688226 0.34313026 -0.86716872 0.34341249 -0.8673799
		 0.34341648 -0.86737466 0.34313068 -0.86735696 0.34312806 -0.8673799;
createNode polyMapSewMove -n "polyMapSewMove41";
	rename -uid "9EF467CB-4BA1-9A70-9D91-9A8828EAAC64";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[469]" "e[637]";
createNode polyTweakUV -n "polyTweakUV49";
	rename -uid "102053F2-4E92-CDB5-1760-1FBA84B17891";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[98:113]" -type "float2" -0.30454132 -0.75658798 -0.30367944
		 -0.75647604 -0.30425838 -0.73068184 -0.30455592 -0.73068184 -0.3045437 -0.75715446
		 -0.30425838 -0.75715446 -0.30425838 -0.70545888 -0.30453339 -0.70545888 -0.32474425
		 -0.73068184 -0.32472971 -0.75658798 -0.3247321 -0.75715446 -0.30440363 -0.6862874
		 -0.32472178 -0.70545888 -0.32464805 -0.68711197 -0.30468896 -0.68576598 -0.32487729
		 -0.68576598;
createNode polyMapSewMove -n "polyMapSewMove42";
	rename -uid "A955CD71-46C1-2FC2-7417-B7A94B10794F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[418]" "e[617]";
createNode polyTweakUV -n "polyTweakUV50";
	rename -uid "C809D601-4A4B-B3AF-9C41-3498A2C32C6A";
	setAttr ".uopa" yes;
	setAttr -s 15 ".uvtk";
	setAttr ".uvtk[158]" -type "float2" -0.092155457 -1.4550474 ;
	setAttr ".uvtk[159]" -type "float2" -0.090476155 -1.4550552 ;
	setAttr ".uvtk[160]" -type "float2" -0.090617776 -1.4547722 ;
	setAttr ".uvtk[161]" -type "float2" -0.092155457 -1.4547722 ;
	setAttr ".uvtk[162]" -type "float2" -0.092155457 -1.4752386 ;
	setAttr ".uvtk[163]" -type "float2" -0.090476155 -1.4752464 ;
	setAttr ".uvtk[164]" -type "float2" -0.089274406 -1.4550576 ;
	setAttr ".uvtk[165]" -type "float2" -0.089274406 -1.4547722 ;
	setAttr ".uvtk[167]" -type "float2" -0.093834758 -1.4550552 ;
	setAttr ".uvtk[168]" -type "float2" -0.093834758 -1.4752464 ;
	setAttr ".uvtk[169]" -type "float2" -0.089274406 -1.4752488 ;
	setAttr ".uvtk[172]" -type "float2" -0.095036507 -1.4550576 ;
	setAttr ".uvtk[174]" -type "float2" -0.095036507 -1.4752488 ;
	setAttr ".uvtk[460]" -type "float2" -0.093693197 -1.4547722 ;
	setAttr ".uvtk[461]" -type "float2" -0.095036507 -1.4547722 ;
createNode polyMapSewMove -n "polyMapSewMove43";
	rename -uid "C89C16BF-458D-800E-8C75-1A8C8DBA25E7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[536]" "e[598]";
createNode polyTweakUV -n "polyTweakUV51";
	rename -uid "A7257C4F-4E3F-9241-C549-5C988F1679A0";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[19:34]" -type "float2" -0.30434766 -0.97196275 -0.30433306
		 -0.94604748 -0.30463073 -0.94604748 -0.30520985 -0.97185081 -0.28415227 -0.97196275
		 -0.28413761 -0.94604748 -0.30463073 -0.92081571 -0.30435559 -0.92081571 -0.30463073
		 -0.97252941 -0.30434528 -0.97252941 -0.28414989 -0.97252941 -0.28416014 -0.92081571
		 -0.30448547 -0.90163755 -0.28423387 -0.9024623 -0.28400457 -0.90111583 -0.30420002
		 -0.90111583;
createNode polyMapSewMove -n "polyMapSewMove44";
	rename -uid "BE36E3CC-4E5C-022D-99A3-D4A1F73432C2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[209]" "e[275]";
createNode polyTweakUV -n "polyTweakUV52";
	rename -uid "10A61E24-4D0B-71FD-3C8F-9DAB3AC9CD27";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[264:275]" -type "float2" 0.24069899 -1.59655666 0.24069899
		 -1.59684777 0.24070293 -1.59684777 0.24070293 -1.59655666 0.24041003 -1.59655666
		 0.24041003 -1.59684777 0.24069941 -1.59636533 0.24070293 -1.59636211 0.24041045 -1.59636533
		 0.24070084 -1.59634733 0.24069679 -1.59634209 0.24040776 -1.59634209;
createNode polyMapSewMove -n "polyMapSewMove45";
	rename -uid "16C7C987-4E46-C1EC-95B0-27B53B1DF4AD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[83]" "e[294]";
createNode polyTweakUV -n "polyTweakUV53";
	rename -uid "21AA92AB-4C09-3875-F997-6BAFF1D9402C";
	setAttr ".uopa" yes;
	setAttr -s 18 ".uvtk";
	setAttr ".uvtk[49]" -type "float2" -0.055976629 -1.9061019 ;
	setAttr ".uvtk[50]" -type "float2" -0.055976629 -1.9061662 ;
	setAttr ".uvtk[51]" -type "float2" -0.055972695 -1.9061662 ;
	setAttr ".uvtk[52]" -type "float2" -0.055972755 -1.9061023 ;
	setAttr ".uvtk[53]" -type "float2" -0.055976629 -1.9061855 ;
	setAttr ".uvtk[54]" -type "float2" -0.055972517 -1.9061855 ;
	setAttr ".uvtk[55]" -type "float2" -0.055684805 -1.9061675 ;
	setAttr ".uvtk[56]" -type "float2" -0.055684865 -1.9061023 ;
	setAttr ".uvtk[57]" -type "float2" -0.055976629 -1.906278 ;
	setAttr ".uvtk[58]" -type "float2" -0.055972397 -1.906278 ;
	setAttr ".uvtk[59]" -type "float2" -0.055684626 -1.9061855 ;
	setAttr ".uvtk[61]" -type "float2" -0.055972695 -1.9064469 ;
	setAttr ".uvtk[62]" -type "float2" -0.055976629 -1.9064469 ;
	setAttr ".uvtk[63]" -type "float2" -0.055684477 -1.906278 ;
	setAttr ".uvtk[64]" -type "float2" -0.055684805 -1.9064469 ;
	setAttr ".uvtk[65]" -type "float2" -0.055972695 -1.906615 ;
	setAttr ".uvtk[66]" -type "float2" -0.055976629 -1.906615 ;
	setAttr ".uvtk[67]" -type "float2" -0.055684805 -1.906615 ;
createNode polyMapSewMove -n "polyMapSewMove46";
	rename -uid "1523EE7B-42CA-3B92-DC2E-D08E28897EC2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[136]" "e[315]";
createNode polyTweakUV -n "polyTweakUV54";
	rename -uid "B0838631-4C4B-151E-40B6-0AB6D4C3C94F";
	setAttr ".uopa" yes;
	setAttr -s 24 ".uvtk";
	setAttr ".uvtk[181]" -type "float2" 0.5299629 -1.9226639 ;
	setAttr ".uvtk[183]" -type "float2" 0.52861983 -1.9308059 ;
	setAttr ".uvtk[184]" -type "float2" 0.53015971 -1.9227014 ;
	setAttr ".uvtk[188]" -type "float2" 0.52794409 -1.9343407 ;
	setAttr ".uvtk[189]" -type "float2" 0.54306835 -1.9335513 ;
	setAttr ".uvtk[190]" -type "float2" 0.54460824 -1.9254466 ;
	setAttr ".uvtk[192]" -type "float2" 0.53240925 -1.9108623 ;
	setAttr ".uvtk[194]" -type "float2" 0.52760458 -1.935076 ;
	setAttr ".uvtk[195]" -type "float2" 0.52779359 -1.9351119 ;
	setAttr ".uvtk[196]" -type "float2" 0.54239261 -1.937086 ;
	setAttr ".uvtk[197]" -type "float2" 0.54685771 -1.9136076 ;
	setAttr ".uvtk[198]" -type "float2" 0.53512287 -1.8955071 ;
	setAttr ".uvtk[199]" -type "float2" 0.5352962 -1.8954892 ;
	setAttr ".uvtk[201]" -type "float2" 0.52748156 -1.9357233 ;
	setAttr ".uvtk[203]" -type "float2" 0.5276711 -1.9357775 ;
	setAttr ".uvtk[204]" -type "float2" 0.54224211 -1.9378572 ;
	setAttr ".uvtk[205]" -type "float2" 0.54974473 -1.8982346 ;
	setAttr ".uvtk[206]" -type "float2" 0.53603333 -1.8948144 ;
	setAttr ".uvtk[209]" -type "float2" 0.54211962 -1.9385228 ;
	setAttr ".uvtk[210]" -type "float2" 0.54989928 -1.897449 ;
	setAttr ".uvtk[444]" -type "float2" 0.5277493 -1.9343143 ;
	setAttr ".uvtk[445]" -type "float2" 0.52842301 -1.9307686 ;
	setAttr ".uvtk[446]" -type "float2" 0.53221238 -1.9108249 ;
	setAttr ".uvtk[471]" -type "float2" 0.53581774 -1.8946308 ;
createNode polyMapSewMove -n "polyMapSewMove47";
	rename -uid "FCB205F1-49C1-4A3E-B87E-63885AF7458C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[153]" "e[327]";
createNode polyTweakUV -n "polyTweakUV55";
	rename -uid "FB5DA09C-4B49-E32E-263D-C7B5414D677E";
	setAttr ".uopa" yes;
	setAttr -s 145 ".uvtk";
	setAttr ".uvtk[19]" -type "float2" -0.60741681 2.4292507 ;
	setAttr ".uvtk[20]" -type "float2" -0.60655785 2.3703444 ;
	setAttr ".uvtk[21]" -type "float2" -0.60588127 2.3703547 ;
	setAttr ".uvtk[22]" -type "float2" -0.60545319 2.4290257 ;
	setAttr ".uvtk[23]" -type "float2" -0.65332133 2.4285555 ;
	setAttr ".uvtk[24]" -type "float2" -0.65246236 2.3696494 ;
	setAttr ".uvtk[25]" -type "float2" -0.60501271 2.3130026 ;
	setAttr ".uvtk[26]" -type "float2" -0.60563803 2.312993 ;
	setAttr ".uvtk[27]" -type "float2" -0.60679257 2.4305487 ;
	setAttr ".uvtk[28]" -type "float2" -0.60744137 2.4305389 ;
	setAttr ".uvtk[29]" -type "float2" -0.65334719 2.4298429 ;
	setAttr ".uvtk[30]" -type "float2" -0.65154254 2.3122978 ;
	setAttr ".uvtk[31]" -type "float2" -0.60310829 2.2686958 ;
	setAttr ".uvtk[32]" -type "float2" -0.65074325 2.2705832 ;
	setAttr ".uvtk[33]" -type "float2" -0.65572214 2.267396 ;
	setAttr ".uvtk[34]" -type "float2" -0.60384887 2.2686238 ;
	setAttr ".uvtk[49]" -type "float2" -0.59491235 2.0647721 ;
	setAttr ".uvtk[50]" -type "float2" -0.59532773 2.0776927 ;
	setAttr ".uvtk[51]" -type "float2" -0.59611261 2.0776689 ;
	setAttr ".uvtk[52]" -type "float2" -0.59568805 2.0648303 ;
	setAttr ".uvtk[53]" -type "float2" -0.59544557 2.0815682 ;
	setAttr ".uvtk[54]" -type "float2" -0.59626621 2.0815432 ;
	setAttr ".uvtk[55]" -type "float2" -0.65396345 2.0761762 ;
	setAttr ".uvtk[56]" -type "float2" -0.65361732 2.0630717 ;
	setAttr ".uvtk[57]" -type "float2" -0.59601074 2.1001523 ;
	setAttr ".uvtk[58]" -type "float2" -0.5968619 2.1001265 ;
	setAttr ".uvtk[59]" -type "float2" -0.65410906 2.0797842 ;
	setAttr ".uvtk[61]" -type "float2" -0.59783065 2.1340613 ;
	setAttr ".uvtk[62]" -type "float2" -0.59704262 2.1340852 ;
	setAttr ".uvtk[63]" -type "float2" -0.65470469 2.0983675 ;
	setAttr ".uvtk[64]" -type "float2" -0.65567344 2.1323023 ;
	setAttr ".uvtk[65]" -type "float2" -0.59885776 2.1678338 ;
	setAttr ".uvtk[66]" -type "float2" -0.59806973 2.1678579 ;
	setAttr ".uvtk[67]" -type "float2" -0.65670025 2.1660743 ;
	setAttr ".uvtk[98]" -type "float2" -0.60765368 2.4449282 ;
	setAttr ".uvtk[99]" -type "float2" -0.60569757 2.4452119 ;
	setAttr ".uvtk[100]" -type "float2" -0.60790396 2.503849 ;
	setAttr ".uvtk[101]" -type "float2" -0.60858059 2.5038385 ;
	setAttr ".uvtk[102]" -type "float2" -0.60763973 2.4436398 ;
	setAttr ".uvtk[103]" -type "float2" -0.60699087 2.4436495 ;
	setAttr ".uvtk[104]" -type "float2" -0.60877413 2.5612063 ;
	setAttr ".uvtk[105]" -type "float2" -0.60939956 2.5611966 ;
	setAttr ".uvtk[106]" -type "float2" -0.6544894 2.5031419 ;
	setAttr ".uvtk[107]" -type "float2" -0.65356243 2.4442315 ;
	setAttr ".uvtk[108]" -type "float2" -0.65354764 2.4429431 ;
	setAttr ".uvtk[109]" -type "float2" -0.60821372 2.6055541 ;
	setAttr ".uvtk[110]" -type "float2" -0.65530825 2.5605001 ;
	setAttr ".uvtk[111]" -type "float2" -0.65577376 2.6022243 ;
	setAttr ".uvtk[112]" -type "float2" -0.60895622 2.6056042 ;
	setAttr ".uvtk[113]" -type "float2" -0.66084647 2.6052594 ;
	setAttr ".uvtk[114]" -type "float2" -0.60687578 2.9769526 ;
	setAttr ".uvtk[115]" -type "float2" -0.60615045 2.9767704 ;
	setAttr ".uvtk[116]" -type "float2" -0.60623944 2.9955592 ;
	setAttr ".uvtk[117]" -type "float2" -0.60703415 2.995554 ;
	setAttr ".uvtk[118]" -type "float2" -0.60644543 3.0258837 ;
	setAttr ".uvtk[119]" -type "float2" -0.6072402 3.0258784 ;
	setAttr ".uvtk[120]" -type "float2" -0.66537178 2.9951572 ;
	setAttr ".uvtk[121]" -type "float2" -0.66506433 2.9765544 ;
	setAttr ".uvtk[122]" -type "float2" -0.60656369 3.0432873 ;
	setAttr ".uvtk[123]" -type "float2" -0.60724914 3.0431128 ;
	setAttr ".uvtk[124]" -type "float2" -0.66557783 3.0254817 ;
	setAttr ".uvtk[125]" -type "float2" -0.60719967 3.0462675 ;
	setAttr ".uvtk[126]" -type "float2" -0.66558677 3.042716 ;
	setAttr ".uvtk[127]" -type "float2" -0.66561919 3.0461659 ;
	setAttr ".uvtk[128]" -type "float2" -0.606197 2.809638 ;
	setAttr ".uvtk[129]" -type "float2" -0.60697061 2.8095565 ;
	setAttr ".uvtk[130]" -type "float2" -0.60700637 2.796711 ;
	setAttr ".uvtk[131]" -type "float2" -0.60622114 2.7967105 ;
	setAttr ".uvtk[132]" -type "float2" -0.66492689 2.8095603 ;
	setAttr ".uvtk[133]" -type "float2" -0.66487634 2.796452 ;
	setAttr ".uvtk[134]" -type "float2" -0.60704267 2.7928338 ;
	setAttr ".uvtk[136]" -type "float2" -0.6649127 2.792841 ;
	setAttr ".uvtk[137]" -type "float2" -0.60707551 2.7742405 ;
	setAttr ".uvtk[139]" -type "float2" -0.6649456 2.7742479 ;
	setAttr ".uvtk[140]" -type "float2" -0.6070168 2.7402916 ;
	setAttr ".uvtk[141]" -type "float2" -0.60622841 2.7402916 ;
	setAttr ".uvtk[143]" -type "float2" -0.66488689 2.7402987 ;
	setAttr ".uvtk[144]" -type "float2" -0.60702097 2.7065034 ;
	setAttr ".uvtk[145]" -type "float2" -0.60623258 2.7065034 ;
	setAttr ".uvtk[146]" -type "float2" -0.66489184 2.7065105 ;
	setAttr ".uvtk[158]" -type "float2" -0.607517 2.4370899 ;
	setAttr ".uvtk[159]" -type "float2" -0.60747719 2.4332714 ;
	setAttr ".uvtk[160]" -type "float2" -0.60683858 2.4336028 ;
	setAttr ".uvtk[161]" -type "float2" -0.60689163 2.4370995 ;
	setAttr ".uvtk[162]" -type "float2" -0.65342414 2.4363928 ;
	setAttr ".uvtk[163]" -type "float2" -0.65338415 2.4325745 ;
	setAttr ".uvtk[165]" -type "float2" -0.60759312 2.4409075 ;
	setAttr ".uvtk[166]" -type "float2" -0.65350008 2.4402108 ;
	setAttr ".uvtk[181]" -type "float2" -0.59330106 2.0123427 ;
	setAttr ".uvtk[183]" -type "float2" -0.59507757 2.0448613 ;
	setAttr ".uvtk[184]" -type "float2" -0.59409142 2.0123186 ;
	setAttr ".uvtk[188]" -type "float2" -0.59549177 2.059058 ;
	setAttr ".uvtk[189]" -type "float2" -0.65309334 2.0431032 ;
	setAttr ".uvtk[190]" -type "float2" -0.65210718 2.0105605 ;
	setAttr ".uvtk[192]" -type "float2" -0.59265077 1.9647808 ;
	setAttr ".uvtk[194]" -type "float2" -0.59481132 2.062181 ;
	setAttr ".uvtk[195]" -type "float2" -0.59557027 2.0621579 ;
	setAttr ".uvtk[196]" -type "float2" -0.65350759 2.0573001 ;
	setAttr ".uvtk[197]" -type "float2" -0.65066659 1.9630227 ;
	setAttr ".uvtk[198]" -type "float2" -0.58999658 1.9032985 ;
	setAttr ".uvtk[199]" -type "float2" -0.59064931 1.9030819 ;
	setAttr ".uvtk[202]" -type "float2" -0.65358609 2.0603998 ;
	setAttr ".uvtk[203]" -type "float2" -0.64866501 1.9013238 ;
	setAttr ".uvtk[204]" -type "float2" -0.59291327 1.8998524 ;
	setAttr ".uvtk[207]" -type "float2" -0.64858991 1.8981652 ;
	setAttr ".uvtk[218]" -type "float2" -0.60617459 2.9096527 ;
	setAttr ".uvtk[220]" -type "float2" -0.60919523 2.9747286 ;
	setAttr ".uvtk[221]" -type "float2" -0.60683358 2.9713836 ;
	setAttr ".uvtk[222]" -type "float2" -0.6069653 2.9096527 ;
	setAttr ".uvtk[225]" -type "float2" -0.66489607 2.9745369 ;
	setAttr ".uvtk[226]" -type "float2" -0.6648761 2.9713836 ;
	setAttr ".uvtk[227]" -type "float2" -0.66500783 2.9096527 ;
	setAttr ".uvtk[228]" -type "float2" -0.6069653 2.8620925 ;
	setAttr ".uvtk[229]" -type "float2" -0.60617459 2.8295348 ;
	setAttr ".uvtk[231]" -type "float2" -0.66500783 2.8620925 ;
	setAttr ".uvtk[232]" -type "float2" -0.6069653 2.8295348 ;
	setAttr ".uvtk[233]" -type "float2" -0.66500783 2.8295348 ;
	setAttr ".uvtk[234]" -type "float2" -0.60694933 2.8153324 ;
	setAttr ".uvtk[235]" -type "float2" -0.60617459 2.8152909 ;
	setAttr ".uvtk[236]" -type "float2" -0.66499186 2.8153324 ;
	setAttr ".uvtk[237]" -type "float2" -0.60693389 2.8122315 ;
	setAttr ".uvtk[239]" -type "float2" -0.66497636 2.8122315 ;
	setAttr ".uvtk[261]" -type "float2" -0.60063106 2.2260971 ;
	setAttr ".uvtk[262]" -type "float2" -0.59984308 2.2261209 ;
	setAttr ".uvtk[263]" -type "float2" -0.65847319 2.2243366 ;
	setAttr ".uvtk[264]" -type "float2" -0.60171163 2.264385 ;
	setAttr ".uvtk[265]" -type "float2" -0.60102808 2.2650578 ;
	setAttr ".uvtk[266]" -type "float2" -0.65955377 2.2626245 ;
	setAttr ".uvtk[276]" -type "float2" -0.60822225 2.9747319 ;
	setAttr ".uvtk[312]" -type "float2" -0.60702825 2.6482105 ;
	setAttr ".uvtk[313]" -type "float2" -0.60623991 2.6482103 ;
	setAttr ".uvtk[314]" -type "float2" -0.60694855 2.6099057 ;
	setAttr ".uvtk[315]" -type "float2" -0.60624492 2.6092539 ;
	setAttr ".uvtk[316]" -type "float2" -0.66489995 2.6482182 ;
	setAttr ".uvtk[317]" -type "float2" -0.66482025 2.6099129 ;
	setAttr ".uvtk[441]" -type "float2" -0.59471869 2.0591226 ;
	setAttr ".uvtk[442]" -type "float2" -0.59428716 2.0448854 ;
	setAttr ".uvtk[443]" -type "float2" -0.59186047 1.9648046 ;
	setAttr ".uvtk[446]" -type "float2" -0.60694474 2.4405956 ;
	setAttr ".uvtk[448]" -type "float2" -0.60622162 2.7928333 ;
	setAttr ".uvtk[449]" -type "float2" -0.606224 2.7742405 ;
	setAttr ".uvtk[450]" -type "float2" -0.60617459 2.8122315 ;
	setAttr ".uvtk[451]" -type "float2" -0.60617459 2.9711871 ;
	setAttr ".uvtk[454]" -type "float2" -0.60617459 2.8620925 ;
	setAttr ".uvtk[468]" -type "float2" -0.59192592 1.8993287 ;
createNode polyMapSewMove -n "polyMapSewMove48";
	rename -uid "700CFD69-4494-3E4C-EF90-25A7A70E7A0F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[213]" "e[281]";
createNode polyTweakUV -n "polyTweakUV56";
	rename -uid "1DBD73A1-491E-A3AC-0120-27AABF2519D8";
	setAttr ".uopa" yes;
	setAttr -s 14 ".uvtk[35:48]" -type "float2" -0.30529875 -0.59253055 -0.30557489
		 -0.6671924 -0.30238551 -0.6671924 -0.30238551 -0.59180832 -0.53942156 -0.59253055
		 -0.53969777 -0.6671924 -0.30238551 -0.78889126 -0.30557489 -0.78889126 -0.53969777
		 -0.78889126 -0.30238551 -0.85873592 -0.30514064 -0.85805404 -0.53926349 -0.85805404
		 -0.30485612 -0.87071288 -0.53929967 -0.87189901;
createNode polyMapSewMove -n "polyMapSewMove49";
	rename -uid "6175A3B6-4414-73EF-7843-9AB577FF65C5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[61]" "e[282]";
createNode polyTweakUV -n "polyTweakUV57";
	rename -uid "22F6EEF1-4C01-C4E0-2435-0CBDF3DB0BBF";
	setAttr ".uopa" yes;
	setAttr -s 49 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.6519388 -1.4032681 ;
	setAttr ".uvtk[1]" -type "float2" -0.65221691 -1.6509691 ;
	setAttr ".uvtk[2]" -type "float2" -0.64656699 -1.6509691 ;
	setAttr ".uvtk[4]" -type "float2" -1.0666716 -1.4032681 ;
	setAttr ".uvtk[5]" -type "float2" -1.0669497 -1.6509691 ;
	setAttr ".uvtk[6]" -type "float2" -0.65256435 -1.8214471 ;
	setAttr ".uvtk[7]" -type "float2" -0.64656699 -1.8188922 ;
	setAttr ".uvtk[9]" -type "float2" -0.66759372 -1.3689811 ;
	setAttr ".uvtk[10]" -type "float2" -1.0823157 -1.3689806 ;
	setAttr ".uvtk[11]" -type "float2" -1.0672972 -1.8214471 ;
	setAttr ".uvtk[12]" -type "float2" -0.65254492 -1.837247 ;
	setAttr ".uvtk[13]" -type "float2" -0.64656931 -1.837247 ;
	setAttr ".uvtk[16]" -type "float2" -1.0672691 -1.8372489 ;
	setAttr ".uvtk[68]" -type "float2" -0.64658207 -0.79669547 ;
	setAttr ".uvtk[69]" -type "float2" -0.64658207 -0.61545742 ;
	setAttr ".uvtk[70]" -type "float2" -0.65120476 -0.61667836 ;
	setAttr ".uvtk[71]" -type "float2" -0.6525178 -0.80026305 ;
	setAttr ".uvtk[72]" -type "float2" -0.64902163 -0.56498706 ;
	setAttr ".uvtk[73]" -type "float2" -1.0658787 -0.61667883 ;
	setAttr ".uvtk[74]" -type "float2" -1.0671908 -0.80026305 ;
	setAttr ".uvtk[75]" -type "float2" -0.646577 -0.95111024 ;
	setAttr ".uvtk[76]" -type "float2" -0.65222663 -0.95111024 ;
	setAttr ".uvtk[77]" -type "float2" -1.0649149 -0.5649876 ;
	setAttr ".uvtk[78]" -type "float2" -1.0669186 -0.95110971 ;
	setAttr ".uvtk[172]" -type "float2" -0.65239525 -1.8489571 ;
	setAttr ".uvtk[175]" -type "float2" -0.64657158 -1.8504825 ;
	setAttr ".uvtk[176]" -type "float2" -1.0671102 -1.8489603 ;
	setAttr ".uvtk[177]" -type "float2" -1.0669359 -1.8726554 ;
	setAttr ".uvtk[242]" -type "float2" -0.64657265 -1.1432741 ;
	setAttr ".uvtk[243]" -type "float2" -0.65222228 -1.1432741 ;
	setAttr ".uvtk[244]" -type "float2" -1.0669335 -1.143273 ;
	setAttr ".uvtk[245]" -type "float2" -0.64657313 -1.3432539 ;
	setAttr ".uvtk[246]" -type "float2" -0.65126181 -1.3400385 ;
	setAttr ".uvtk[247]" -type "float2" -1.0659726 -1.3400375 ;
	setAttr ".uvtk[248]" -type "float2" -0.66266322 -1.3689811 ;
	setAttr ".uvtk[249]" -type "float2" -0.84266973 0.7887007 ;
	setAttr ".uvtk[250]" -type "float2" -0.86468208 0.49883005 ;
	setAttr ".uvtk[253]" -type "float2" -1.4682443 0.8362056 ;
	setAttr ".uvtk[254]" -type "float2" -1.4902563 0.54633403 ;
	setAttr ".uvtk[256]" -type "float2" -0.81219727 1.1645435 ;
	setAttr ".uvtk[257]" -type "float2" -1.4386072 1.2123983 ;
	setAttr ".uvtk[258]" -type "float2" -1.5051032 0.30977836 ;
	setAttr ".uvtk[259]" -type "float2" -1.5096223 0.25152543 ;
	setAttr ".uvtk[435]" -type "float2" -0.64656699 -1.4032911 ;
	setAttr ".uvtk[452]" -type "float2" -0.88215619 0.26276776 ;
	setAttr ".uvtk[453]" -type "float2" -0.65185922 -0.56498706 ;
	setAttr ".uvtk[454]" -type "float2" -0.88649094 0.20420584 ;
	setAttr ".uvtk[463]" -type "float2" -0.65222102 -1.8726518 ;
	setAttr ".uvtk[465]" -type "float2" -0.64657134 -1.8726518 ;
createNode polyMapSewMove -n "polyMapSewMove50";
	rename -uid "3FA70353-4AD7-A798-3FAC-62939AEF1EC2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[339]" "e[685]";
createNode polyDelEdge -n "polyDelEdge9";
	rename -uid "6D88758D-477E-35DA-80CD-14AFFDAB4A2B";
	setAttr ".ics" -type "componentList" 6 "e[41]" "e[267]" "e[271:272]" "e[379]" "e[590]" "e[594:595]";
	setAttr ".cv" yes;
createNode deleteComponent -n "deleteComponent11";
	rename -uid "5007F7BA-40A4-3DE3-7096-34A80FAEABCF";
	setAttr ".dc" -type "componentList" 6 "e[41]" "e[267]" "e[271:272]" "e[379]" "e[590]" "e[594:595]";
createNode objectSet -n "set1";
	rename -uid "70FF15CB-4766-B832-20B6-C88288FCB88F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "624FF360-4B5C-AD85-C8A1-F6BC9562BF55";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "3C7397BD-4240-5E21-3501-C3A4FA631E25";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "e[41]" "e[267]" "e[271:272]" "e[379]" "e[590]" "e[594:595]";
createNode deleteComponent -n "deleteComponent12";
	rename -uid "FC2D89E9-4F05-BFFC-1167-04BEFD424182";
	setAttr ".dc" -type "componentList" 2 "f[123]" "f[281]";
createNode objectSet -n "set2";
	rename -uid "E1005A18-4269-F8CD-5E6B-F18F9A786833";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "6FD6082F-48B1-B063-AC38-029F0513FA3B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "B21E2055-4153-D52B-EF58-B69B336C7794";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "e[41]" "e[267]" "e[271:272]" "e[379]" "e[590]" "e[594:595]";
createNode deleteComponent -n "deleteComponent13";
	rename -uid "F301126C-42CB-6411-C802-F2B97A9D5B20";
	setAttr ".dc" -type "componentList" 2 "f[158]" "f[315]";
createNode polyMapSewMove -n "polyMapSewMove51";
	rename -uid "E9E3B231-4AC2-AFC7-0211-2E81C6DD454C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[267]" "e[588]";
createNode polyMapCut -n "polyMapCut19";
	rename -uid "7274C031-4C9F-2F34-992F-16B729BBC67C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[458]";
createNode polyMapCut -n "polyMapCut20";
	rename -uid "E3471F05-4ADC-69B6-09DB-ECA6FC57D9B3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[587]";
createNode polyMapCut -n "polyMapCut21";
	rename -uid "5767BC1B-423A-6879-3ADB-F8878A8FDB56";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[657]";
createNode polyMapCut -n "polyMapCut22";
	rename -uid "8BA18450-473A-8EEA-097C-788DB54CF3EB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[337]";
createNode polySplit -n "polySplit58";
	rename -uid "AA772193-4E82-30B5-D1BA-F790F5C5BC27";
	setAttr -s 3 ".e[0:2]"  0 0.48389599 1;
	setAttr -s 3 ".d[0:2]"  -2147483607 -2147483397 -2147483606;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit59";
	rename -uid "BCDEF51A-4DEF-37C5-5C1B-74B451EE52A7";
	setAttr -s 3 ".e[0:2]"  0 0.45434901 1;
	setAttr -s 3 ".d[0:2]"  -2147483271 -2147483074 -2147483270;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyMapCut -n "polyMapCut23";
	rename -uid "4CBC78A2-4852-D284-2847-89B2FF67C124";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 18 "e[376]" "e[381]" "e[393:394]" "e[398]" "e[404]" "e[413]" "e[417]" "e[424]" "e[451]" "e[453]" "e[461]" "e[465]" "e[470]" "e[475]" "e[478:479]" "e[485]" "e[487]" "e[554]";
createNode polyDelEdge -n "polyDelEdge10";
	rename -uid "9A04E3C9-4128-AD76-CAAF-9784FB912E50";
	setAttr ".ics" -type "componentList" 1 "e[531]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge11";
	rename -uid "66A39975-4219-DF76-CE93-DDBF24523EC7";
	setAttr ".ics" -type "componentList" 1 "e[377]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge12";
	rename -uid "40FE283B-40E5-8190-2295-F384A5A2C999";
	setAttr ".ics" -type "componentList" 1 "e[206]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge13";
	rename -uid "D08397F7-407A-31C9-1307-B48B6ED4DC0D";
	setAttr ".ics" -type "componentList" 1 "e[41]";
	setAttr ".cv" yes;
createNode polyMapCut -n "polyMapCut24";
	rename -uid "E237D734-4D39-3882-CA23-5495681835BC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 18 "e[40]" "e[44]" "e[58:59]" "e[63]" "e[69]" "e[79]" "e[83]" "e[90]" "e[119]" "e[121]" "e[129]" "e[133]" "e[138]" "e[143]" "e[146:147]" "e[153]" "e[155]" "e[227]";
createNode polyMapCut -n "polyMapCut25";
	rename -uid "B51FA7FC-40F4-D44E-F78A-949866A0A908";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 18 "e[40]" "e[44]" "e[58:59]" "e[63]" "e[69]" "e[79]" "e[83]" "e[90]" "e[119]" "e[121]" "e[129]" "e[133]" "e[138]" "e[143]" "e[146:147]" "e[153]" "e[155]" "e[227]";
createNode polyMapCut -n "polyMapCut26";
	rename -uid "4517B7CF-4421-12CA-B79E-22887C6799A8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[82]" "e[211]" "e[534]";
createNode polyTweakUV -n "polyTweakUV58";
	rename -uid "AE7BDC13-4DD8-74E4-E83A-89ADD3715BFF";
	setAttr ".uopa" yes;
	setAttr -s 32 ".uvtk";
	setAttr ".uvtk[49]" -type "float2" 0.14210492 0.72845584 ;
	setAttr ".uvtk[50]" -type "float2" 0.14921722 0.78378212 ;
	setAttr ".uvtk[53]" -type "float2" 0.1513792 0.80037206 ;
	setAttr ".uvtk[57]" -type "float2" 0.16174649 0.87992698 ;
	setAttr ".uvtk[62]" -type "float2" 0.1806765 1.0251871 ;
	setAttr ".uvtk[66]" -type "float2" 0.19951667 1.1697607 ;
	setAttr ".uvtk[181]" -type "float2" 0.11292865 0.50400496 ;
	setAttr ".uvtk[194]" -type "float2" 0.14075412 0.71734864 ;
	setAttr ".uvtk[198]" -type "float2" 0.052047793 0.037216902 ;
	setAttr ".uvtk[258]" -type "float2" 0.23201284 1.4191744 ;
	setAttr ".uvtk[261]" -type "float2" 0.25373024 1.5858555 ;
	setAttr ".uvtk[422]" -type "float2" 0.13904651 0.70425677 ;
	setAttr ".uvtk[423]" -type "float2" 0.13109794 0.64331108 ;
	setAttr ".uvtk[424]" -type "float2" 0.08638753 0.30050796 ;
	setAttr ".uvtk[478]" -type "float2" 0.1458573 0.7842201 ;
	setAttr ".uvtk[479]" -type "float2" 0.14786626 0.80082977 ;
	setAttr ".uvtk[480]" -type "float2" 0.15810299 0.88040203 ;
	setAttr ".uvtk[481]" -type "float2" 0.1388406 0.72923702 ;
	setAttr ".uvtk[482]" -type "float2" 0.13750531 0.71777207 ;
	setAttr ".uvtk[483]" -type "float2" 0.13570878 0.70451277 ;
	setAttr ".uvtk[484]" -type "float2" 0.17730325 1.025627 ;
	setAttr ".uvtk[485]" -type "float2" 0.19614334 1.1702001 ;
	setAttr ".uvtk[486]" -type "float2" 0.12771448 0.64375234 ;
	setAttr ".uvtk[487]" -type "float2" 0.10954525 0.50444573 ;
	setAttr ".uvtk[488]" -type "float2" 0.22863963 1.419614 ;
	setAttr ".uvtk[489]" -type "float2" 0.083004452 0.30094951 ;
	setAttr ".uvtk[492]" -type "float2" 0.25035578 1.5834591 ;
	setAttr ".uvtk[493]" -type "float2" 0.049118329 0.036743164 ;
	setAttr ".uvtk[495]" -type "float2" 0.028779956 0.024981737 ;
	setAttr ".uvtk[500]" -type "float2" 0.033051841 0.024071991 ;
	setAttr ".uvtk[501]" -type "float2" 0.24736974 1.6027834 ;
	setAttr ".uvtk[502]" -type "float2" 0.24416527 1.602985 ;
createNode polyMapSewMove -n "polyMapSewMove52";
	rename -uid "227A6F2E-40F5-F512-E838-4DA76FE44AA9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[61]" "e[125]" "e[185]" "e[190]" "e[192:194]" "e[209]" "e[212]" "e[223]" "e[683]";
createNode polyTweakUV -n "polyTweakUV59";
	rename -uid "07D11153-474E-55FF-4AF4-E1B6BAFFA19D";
	setAttr ".uopa" yes;
	setAttr -s 250 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" -0.0086548328 -3.0890706 ;
	setAttr ".uvtk[8]" -type "float2" -0.034999043 -3.0901725 ;
	setAttr ".uvtk[14]" -type "float2" -0.058113486 -3.0762947 ;
	setAttr ".uvtk[15]" -type "float2" -0.029470056 -3.0711603 ;
	setAttr ".uvtk[17]" -type "float2" -0.23629323 -2.9948478 ;
	setAttr ".uvtk[18]" -type "float2" -0.23477364 -3.0102065 ;
	setAttr ".uvtk[37]" -type "float2" -0.5215106 -3.1782355 ;
	setAttr ".uvtk[38]" -type "float2" -0.53629827 -3.0135925 ;
	setAttr ".uvtk[41]" -type "float2" -0.4904182 -3.3942645 ;
	setAttr ".uvtk[44]" -type "float2" -0.46261945 -3.4777498 ;
	setAttr ".uvtk[49]" -type "float2" -0.68269187 -3.6725969 ;
	setAttr ".uvtk[50]" -type "float2" -0.68594825 -3.7237568 ;
	setAttr ".uvtk[53]" -type "float2" -0.68695146 -3.7390988 ;
	setAttr ".uvtk[57]" -type "float2" -0.69176251 -3.8126705 ;
	setAttr ".uvtk[60]" -type "float2" -0.70278007 -3.6961336 ;
	setAttr ".uvtk[62]" -type "float2" -0.7005471 -3.947006 ;
	setAttr ".uvtk[66]" -type "float2" -0.70928997 -4.0807061 ;
	setAttr ".uvtk[72]" -type "float2" -0.29598394 -3.4750066 ;
	setAttr ".uvtk[90]" -type "float2" -0.0038396865 -2.8119311 ;
	setAttr ".uvtk[91]" -type "float2" 0.020463973 -2.8279347 ;
	setAttr ".uvtk[95]" -type "float2" -0.19224903 -2.8030708 ;
	setAttr ".uvtk[96]" -type "float2" -0.19969657 -2.8165886 ;
	setAttr ".uvtk[115]" -type "float2" 0.86243975 3.7143967 ;
	setAttr ".uvtk[116]" -type "float2" 0.88867819 3.8310928 ;
	setAttr ".uvtk[118]" -type "float2" 0.93063837 4.0195246 ;
	setAttr ".uvtk[122]" -type "float2" 0.95472008 4.1276693 ;
	setAttr ".uvtk[125]" -type "float2" -0.15599966 1.2208501 ;
	setAttr ".uvtk[135]" -type "float2" -0.66283864 -2.0734551 ;
	setAttr ".uvtk[138]" -type "float2" -0.7378571 -2.0611556 ;
	setAttr ".uvtk[142]" -type "float2" -0.68010432 -2.079006 ;
	setAttr ".uvtk[164]" -type "float2" -1.7191072 -2.8585281 ;
	setAttr ".uvtk[167]" -type "float2" -1.7038404 -2.9182296 ;
	setAttr ".uvtk[168]" -type "float2" -1.7048886 -2.8944678 ;
	setAttr ".uvtk[169]" -type "float2" -1.7148634 -2.832006 ;
	setAttr ".uvtk[170]" -type "float2" -1.7042969 -2.8522584 ;
	setAttr ".uvtk[171]" -type "float2" -1.7025818 -2.8285356 ;
	setAttr ".uvtk[181]" -type "float2" -0.69441503 -3.476325 ;
	setAttr ".uvtk[182]" -type "float2" -0.70252782 -3.6051755 ;
	setAttr ".uvtk[185]" -type "float2" -0.7484085 -3.4772737 ;
	setAttr ".uvtk[186]" -type "float2" -0.73476309 -3.6252606 ;
	setAttr ".uvtk[187]" -type "float2" -0.65382069 -3.6537635 ;
	setAttr ".uvtk[191]" -type "float2" -0.66641319 -3.275476 ;
	setAttr ".uvtk[193]" -type "float2" -0.71275139 -3.2687838 ;
	setAttr ".uvtk[194]" -type "float2" -0.65270746 -3.6652482 ;
	setAttr ".uvtk[198]" -type "float2" -0.6301834 -3.0156109 ;
	setAttr ".uvtk[200]" -type "float2" -0.61863565 -3.0245643 ;
	setAttr ".uvtk[201]" -type "float2" -0.62983465 -3.6679041 ;
	setAttr ".uvtk[205]" -type "float2" -0.61073804 -3.0031536 ;
	setAttr ".uvtk[206]" -type "float2" -0.60352355 -3.0053823 ;
	setAttr ".uvtk[208]" -type "float2" -0.60577589 -2.9651382 ;
	setAttr ".uvtk[209]" -type "float2" -0.60669118 -2.9978817 ;
	setAttr ".uvtk[210]" -type "float2" -0.59069878 -2.8588974 ;
	setAttr ".uvtk[211]" -type "float2" -0.57699525 -2.8319867 ;
	setAttr ".uvtk[212]" -type "float2" -0.5747056 -2.7199125 ;
	setAttr ".uvtk[213]" -type "float2" -0.55102414 -2.6982605 ;
	setAttr ".uvtk[214]" -type "float2" -0.56997722 -2.7138462 ;
	setAttr ".uvtk[215]" -type "float2" -0.54732901 -2.6817653 ;
	setAttr ".uvtk[216]" -type "float2" -0.55089945 -2.6627517 ;
	setAttr ".uvtk[217]" -type "float2" -0.58048099 -2.6929815 ;
	setAttr ".uvtk[219]" -type "float2" -0.61818022 -2.4469137 ;
	setAttr ".uvtk[223]" -type "float2" -0.45071119 -2.1730072 ;
	setAttr ".uvtk[224]" -type "float2" -0.60581863 -2.2380974 ;
	setAttr ".uvtk[230]" -type "float2" -0.55821502 -2.0909688 ;
	setAttr ".uvtk[238]" -type "float2" -0.60144585 -2.1091921 ;
	setAttr ".uvtk[240]" -type "float2" -0.59773964 -2.099117 ;
	setAttr ".uvtk[241]" -type "float2" -0.60797906 -2.1092701 ;
	setAttr ".uvtk[251]" -type "float2" -0.18020371 -3.3490293 ;
	setAttr ".uvtk[252]" -type "float2" -0.13090211 -3.2165761 ;
	setAttr ".uvtk[255]" -type "float2" -0.27313659 -3.449857 ;
	setAttr ".uvtk[258]" -type "float2" -0.72436708 -4.3113613 ;
	setAttr ".uvtk[261]" -type "float2" -0.73444337 -4.4655066 ;
	setAttr ".uvtk[263]" -type "float2" -0.59571129 -2.9863777 ;
	setAttr ".uvtk[264]" -type "float2" -0.56457824 -2.834538 ;
	setAttr ".uvtk[265]" -type "float2" -0.53334647 -2.6827188 ;
	setAttr ".uvtk[266]" -type "float2" -0.53274238 -2.6651802 ;
	setAttr ".uvtk[268]" -type "float2" -0.24422926 -2.9828238 ;
	setAttr ".uvtk[269]" -type "float2" -0.22797564 -2.9036689 ;
	setAttr ".uvtk[270]" -type "float2" -0.23321936 -2.9025917 ;
	setAttr ".uvtk[271]" -type "float2" -0.25242552 -2.9961059 ;
	setAttr ".uvtk[272]" -type "float2" -0.21172154 -2.8245142 ;
	setAttr ".uvtk[273]" -type "float2" -0.21401319 -2.8090773 ;
	setAttr ".uvtk[274]" -type "float2" -0.23785675 -2.9016392 ;
	setAttr ".uvtk[275]" -type "float2" -0.2601482 -3.0120301 ;
	setAttr ".uvtk[276]" -type "float2" -0.25167036 -3.0265257 ;
	setAttr ".uvtk[277]" -type "float2" -0.24868333 -3.0049334 ;
	setAttr ".uvtk[278]" -type "float2" -0.20075718 -2.7815588 ;
	setAttr ".uvtk[279]" -type "float2" -0.20720592 -2.8024013 ;
	setAttr ".uvtk[280]" -type "float2" -0.21483487 -2.7913983 ;
	setAttr ".uvtk[281]" -type "float2" -0.20335478 -2.7544165 ;
	setAttr ".uvtk[289]" -type "float2" -0.015471697 -2.6543181 ;
	setAttr ".uvtk[290]" -type "float2" -0.0085999221 -2.5131543 ;
	setAttr ".uvtk[291]" -type "float2" -0.028424829 -2.5142281 ;
	setAttr ".uvtk[292]" -type "float2" -0.054289043 -2.3838677 ;
	setAttr ".uvtk[302]" -type "float2" -1.7218293 -2.8731217 ;
	setAttr ".uvtk[303]" -type "float2" -1.7195168 -2.8877854 ;
	setAttr ".uvtk[304]" -type "float2" -1.7032255 -2.8733828 ;
	setAttr ".uvtk[305]" -type "float2" -1.4081273 -3.0293441 ;
	setAttr ".uvtk[306]" -type "float2" -1.4104216 -2.8774884 ;
	setAttr ".uvtk[307]" -type "float2" -1.4127545 -3.0438607 ;
	setAttr ".uvtk[308]" -type "float2" -1.1233215 -3.0899856 ;
	setAttr ".uvtk[309]" -type "float2" -1.1220648 -2.8815317 ;
	setAttr ".uvtk[310]" -type "float2" -1.4038703 -2.7257564 ;
	setAttr ".uvtk[311]" -type "float2" -1.1252122 -3.1059949 ;
	setAttr ".uvtk[312]" -type "float2" -0.89957714 -3.1015494 ;
	setAttr ".uvtk[313]" -type "float2" -0.90836501 -2.8845284 ;
	setAttr ".uvtk[314]" -type "float2" -1.1174762 -2.6731246 ;
	setAttr ".uvtk[315]" -type "float2" -1.4080887 -2.7111161 ;
	setAttr ".uvtk[316]" -type "float2" -0.90614301 -3.1165922 ;
	setAttr ".uvtk[317]" -type "float2" -0.72356653 -3.0586846 ;
	setAttr ".uvtk[318]" -type "float2" -0.67681712 -2.8877752 ;
	setAttr ".uvtk[319]" -type "float2" -0.89349538 -2.6678388 ;
	setAttr ".uvtk[320]" -type "float2" -1.1189172 -2.6570685 ;
	setAttr ".uvtk[321]" -type "float2" -0.72673064 -3.0760002 ;
	setAttr ".uvtk[322]" -type "float2" -0.71875602 -2.7156217 ;
	setAttr ".uvtk[323]" -type "float2" -0.8996371 -2.6526175 ;
	setAttr ".uvtk[324]" -type "float2" -0.88701373 -3.1253986 ;
	setAttr ".uvtk[325]" -type "float2" -0.73877662 -3.094686 ;
	setAttr ".uvtk[326]" -type "float2" -0.7214334 -2.6982241 ;
	setAttr ".uvtk[327]" -type "float2" -0.9090023 -3.1282918 ;
	setAttr ".uvtk[328]" -type "float2" -0.98816645 -3.2883537 ;
	setAttr ".uvtk[329]" -type "float2" -0.81154388 -3.2769489 ;
	setAttr ".uvtk[330]" -type "float2" -0.73295057 -2.6792073 ;
	setAttr ".uvtk[331]" -type "float2" -0.88026834 -2.6443512 ;
	setAttr ".uvtk[332]" -type "float2" -1.0029973 -3.2823689 ;
	setAttr ".uvtk[333]" -type "float2" -1.0114676 -3.5132594 ;
	setAttr ".uvtk[334]" -type "float2" -0.83281678 -3.4693408 ;
	setAttr ".uvtk[335]" -type "float2" -0.80057895 -2.4949765 ;
	setAttr ".uvtk[336]" -type "float2" -0.97681218 -2.4786234 ;
	setAttr ".uvtk[337]" -type "float2" -0.90216714 -2.6408427 ;
	setAttr ".uvtk[338]" -type "float2" -1.0280573 -3.5145423 ;
	setAttr ".uvtk[339]" -type "float2" -0.88418329 -3.6754694 ;
	setAttr ".uvtk[340]" -type "float2" -0.77981085 -3.6218925 ;
	setAttr ".uvtk[341]" -type "float2" -0.81644899 -2.3020635 ;
	setAttr ".uvtk[342]" -type "float2" -0.99379802 -2.2531531 ;
	setAttr ".uvtk[343]" -type "float2" -0.99180496 -2.48419 ;
	setAttr ".uvtk[344]" -type "float2" -0.89573032 -3.6896553 ;
	setAttr ".uvtk[345]" -type "float2" -0.68344557 -3.6885977 ;
	setAttr ".uvtk[346]" -type "float2" -0.75918657 -2.1510587 ;
	setAttr ".uvtk[347]" -type "float2" -0.86201566 -2.0945761 ;
	setAttr ".uvtk[348]" -type "float2" -1.0103455 -2.251405 ;
	setAttr ".uvtk[349]" -type "float2" -0.76101047 -3.7123575 ;
	setAttr ".uvtk[350]" -type "float2" -0.68567669 -3.7021668 ;
	setAttr ".uvtk[351]" -type "float2" -0.67081243 -3.6996534 ;
	setAttr ".uvtk[352]" -type "float2" -0.66098869 -2.0870814 ;
	setAttr ".uvtk[353]" -type "float2" -0.87316042 -2.0800722 ;
	setAttr ".uvtk[354]" -type "float2" -0.61988324 -3.6783397 ;
	setAttr ".uvtk[355]" -type "float2" -0.64805055 -2.0763836 ;
	setAttr ".uvtk[356]" -type "float2" -0.68535972 -2.1758273 ;
	setAttr ".uvtk[357]" -type "float2" -0.60476822 -2.1199651 ;
	setAttr ".uvtk[358]" -type "float2" 0.15897959 -3.1000085 ;
	setAttr ".uvtk[359]" -type "float2" 0.15682593 -3.0800827 ;
	setAttr ".uvtk[360]" -type "float2" 0.28743035 -3.0505655 ;
	setAttr ".uvtk[361]" -type "float2" 0.28817537 -3.0365598 ;
	setAttr ".uvtk[362]" -type "float2" 0.18200803 -2.9878387 ;
	setAttr ".uvtk[363]" -type "float2" -0.0086153001 -2.948704 ;
	setAttr ".uvtk[364]" -type "float2" 0.30301845 -3.0376258 ;
	setAttr ".uvtk[365]" -type "float2" 0.30900717 -3.0295303 ;
	setAttr ".uvtk[366]" -type "float2" 0.29288191 -3.0106006 ;
	setAttr ".uvtk[367]" -type "float2" 0.19520643 -2.8931344 ;
	setAttr ".uvtk[368]" -type "float2" -0.2145938 -2.9064167 ;
	setAttr ".uvtk[369]" -type "float2" 0.31613767 -3.0153754 ;
	setAttr ".uvtk[370]" -type "float2" 0.29878378 -2.9848874 ;
	setAttr ".uvtk[371]" -type "float2" 0.04665181 -2.8196747 ;
	setAttr ".uvtk[372]" -type "float2" 0.20503655 -2.8756683 ;
	setAttr ".uvtk[373]" -type "float2" 0.31516105 -2.9995556 ;
	setAttr ".uvtk[374]" -type "float2" 0.3036173 -2.9717205 ;
	setAttr ".uvtk[375]" -type "float2" 0.31284621 -2.9897556 ;
	setAttr ".uvtk[376]" -type "float2" -0.24210224 -3.0248208 ;
	setAttr ".uvtk[377]" -type "float2" -0.21908373 -3.0323334 ;
	setAttr ".uvtk[378]" -type "float2" -0.16911042 -2.7889152 ;
	setAttr ".uvtk[379]" -type "float2" -0.19322675 -2.7867513 ;
	setAttr ".uvtk[380]" -type "float2" -0.12701556 -3.2216043 ;
	setAttr ".uvtk[381]" -type "float2" -0.054492235 -3.0945652 ;
	setAttr ".uvtk[382]" -type "float2" 0.0066866875 -2.7965655 ;
	setAttr ".uvtk[383]" -type "float2" -0.0099179447 -2.6512282 ;
	setAttr ".uvtk[384]" -type "float2" -0.14713135 -3.2236381 ;
	setAttr ".uvtk[385]" -type "float2" -0.14826769 -3.3654852 ;
	setAttr ".uvtk[386]" -type "float2" 0.0042393059 -3.2598021 ;
	setAttr ".uvtk[387]" -type "float2" -0.029999107 -3.1067774 ;
	setAttr ".uvtk[388]" -type "float2" 0.034009993 -2.7949917 ;
	setAttr ".uvtk[389]" -type "float2" 0.02287218 -2.8082826 ;
	setAttr ".uvtk[390]" -type "float2" 0.12576973 -2.6678336 ;
	setAttr ".uvtk[391]" -type "float2" 0.027237236 -2.5106113 ;
	setAttr ".uvtk[392]" -type "float2" -0.02760464 -2.6414335 ;
	setAttr ".uvtk[393]" -type "float2" -0.24044472 -3.4456813 ;
	setAttr ".uvtk[394]" -type "float2" -0.028164983 -3.3748736 ;
	setAttr ".uvtk[395]" -type "float2" 0.024851084 -3.258929 ;
	setAttr ".uvtk[396]" -type "float2" 0.14437065 -2.6767569 ;
	setAttr ".uvtk[397]" -type "float2" 0.14132395 -2.5493028 ;
	setAttr ".uvtk[398]" -type "float2" -0.025887698 -2.4005852 ;
	setAttr ".uvtk[399]" -type "float2" -0.26416931 -3.4671898 ;
	setAttr ".uvtk[400]" -type "float2" -0.13362199 -3.4435961 ;
	setAttr ".uvtk[401]" -type "float2" -0.011191159 -3.386914 ;
	setAttr ".uvtk[402]" -type "float2" 0.1616686 -2.5449238 ;
	setAttr ".uvtk[403]" -type "float2" 0.071473241 -2.4445896 ;
	setAttr ".uvtk[404]" -type "float2" -0.039219171 -2.3714702 ;
	setAttr ".uvtk[405]" -type "float2" -0.13434586 -3.4568388 ;
	setAttr ".uvtk[406]" -type "float2" 0.076025188 -2.432133 ;
	setAttr ".uvtk[407]" -type "float2" -0.50973266 -3.0644059 ;
	setAttr ".uvtk[408]" -type "float2" -0.37387189 -3.0923085 ;
	setAttr ".uvtk[409]" -type "float2" -0.46545601 -2.8548949 ;
	setAttr ".uvtk[410]" -type "float2" -0.32850254 -2.8830223 ;
	setAttr ".uvtk[411]" -type "float2" -0.42357418 -2.6448934 ;
	setAttr ".uvtk[412]" -type "float2" -0.28771353 -2.6727962 ;
	setAttr ".uvtk[413]" -type "float2" -0.42433187 -2.6239505 ;
	setAttr ".uvtk[414]" -type "float2" -0.27527612 -2.654563 ;
	setAttr ".uvtk[415]" -type "float2" -0.58038276 -2.8404956 ;
	setAttr ".uvtk[416]" -type "float2" -0.58097142 -2.8219731 ;
	setAttr ".uvtk[417]" -type "float2" -0.56395793 -2.8248224 ;
	setAttr ".uvtk[420]" -type "float2" -1.7160202 -2.9144161 ;
	setAttr ".uvtk[425]" -type "float2" -0.42081133 -2.02685 ;
	setAttr ".uvtk[426]" -type "float2" -0.49438831 -2.3865116 ;
	setAttr ".uvtk[432]" -type "float2" -0.05506745 -2.3784785 ;
	setAttr ".uvtk[434]" -type "float2" 0.0038990676 -2.6810226 ;
	setAttr ".uvtk[435]" -type "float2" -0.27548563 -3.4548364 ;
	setAttr ".uvtk[436]" -type "float2" -0.19800144 -3.3402314 ;
	setAttr ".uvtk[437]" -type "float2" -0.10257432 -3.1996641 ;
	setAttr ".uvtk[443]" -type "float2" -0.065385789 -2.3517466 ;
	setAttr ".uvtk[444]" -type "float2" -0.061677188 -2.3448019 ;
	setAttr ".uvtk[446]" -type "float2" -0.29471707 -3.483269 ;
	setAttr ".uvtk[447]" -type "float2" -0.25206017 -3.0108259 ;
	setAttr ".uvtk[448]" -type "float2" -0.20799026 -2.7965746 ;
	setAttr ".uvtk[461]" -type "float2" 0.92569995 4.0206242 ;
	setAttr ".uvtk[462]" -type "float2" 0.88373971 3.8321929 ;
	setAttr ".uvtk[465]" -type "float2" 0.85819924 3.7165618 ;
	setAttr ".uvtk[466]" -type "float2" 0.84063834 3.7060721 ;
	setAttr ".uvtk[467]" -type "float2" 0.9502185 4.1275625 ;
	setAttr ".uvtk[469]" -type "float2" -0.68284094 -3.7239599 ;
	setAttr ".uvtk[470]" -type "float2" -0.68370265 -3.739311 ;
	setAttr ".uvtk[471]" -type "float2" -0.68839306 -3.8128912 ;
	setAttr ".uvtk[472]" -type "float2" -0.67965215 -3.6731212 ;
	setAttr ".uvtk[473]" -type "float2" -0.67910439 -3.6625278 ;
	setAttr ".uvtk[474]" -type "float2" -0.67823958 -3.6502612 ;
	setAttr ".uvtk[475]" -type "float2" -0.69742757 -3.9472101 ;
	setAttr ".uvtk[476]" -type "float2" -0.70617032 -4.0809097 ;
	setAttr ".uvtk[477]" -type "float2" -0.67449528 -3.5940661 ;
	setAttr ".uvtk[478]" -type "float2" -0.66605681 -3.4652357 ;
	setAttr ".uvtk[479]" -type "float2" -0.72124738 -4.3115654 ;
	setAttr ".uvtk[480]" -type "float2" -0.65373045 -3.2770433 ;
	setAttr ".uvtk[481]" -type "float2" -0.62172979 -3.6722851 ;
	setAttr ".uvtk[482]" -type "float2" -0.53485918 -3.2708406 ;
	setAttr ".uvtk[483]" -type "float2" -0.73149014 -4.4631095 ;
	setAttr ".uvtk[484]" -type "float2" -0.63825196 -3.0327394 ;
	setAttr ".uvtk[485]" -type "float2" -0.54402524 -3.0094361 ;
	setAttr ".uvtk[486]" -type "float2" -0.62029409 -3.0207524 ;
	setAttr ".uvtk[487]" -type "float2" -0.66966712 -3.9007404 ;
	setAttr ".uvtk[489]" -type "float2" 0.84667921 3.7047055 ;
	setAttr ".uvtk[490]" -type "float2" -0.60341078 -2.9843836 ;
	setAttr ".uvtk[491]" -type "float2" -0.72761065 -4.4806557 ;
	setAttr ".uvtk[492]" -type "float2" -0.72465998 -4.4806509 ;
createNode polyMapSewMove -n "polyMapSewMove53";
	rename -uid "65899EF5-4D90-7A86-5D94-629E5FAA4512";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[393]" "e[454]" "e[508]" "e[513]" "e[515:517]" "e[532]" "e[535]" "e[546]" "e[686]";
createNode polyDelEdge -n "polyDelEdge14";
	rename -uid "363C1905-4403-FBE1-8A92-BA9D4D2C11D9";
	setAttr ".ics" -type "componentList" 2 "e[214]" "e[537]";
	setAttr ".cv" yes;
createNode polyMapCut -n "polyMapCut27";
	rename -uid "B1D9B3B8-4CC5-6DE5-C938-D78AA0E9F588";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[74]" "e[83]" "e[288]";
createNode polyMapSewMove -n "polyMapSewMove54";
	rename -uid "9F2A4AA1-4417-C520-A07D-C1B016CFEC17";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[136]" "e[149]" "e[191]" "e[195:196]" "e[220]" "e[223]" "e[227]";
createNode polyMapCut -n "polyMapCut28";
	rename -uid "A4047B1E-4574-BB40-7807-0AA43DAAFA92";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[408]";
createNode polyNormal -n "polyNormal1";
	rename -uid "AEEDFD2A-4A47-3785-B173-A79FADACFCAB";
	setAttr ".ics" -type "componentList" 1 "f[0:323]";
	setAttr ".nm" 2;
createNode polySplitEdge -n "polySplitEdge1";
	rename -uid "1685E1E1-4F40-57CE-4B21-36B298FDC01A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[76:77]" "e[407]";
createNode polySplitVert -n "polySplitVert1";
	rename -uid "73CE7A4E-4C68-0E5C-58BF-F1A690E96818";
	setAttr ".ics" -type "componentList" 3 "vtx[40]" "vtx[47:48]" "vtx[218]";
createNode polyMapCut -n "polyMapCut29";
	rename -uid "F7520D6F-4530-CD8B-444F-D495A60DB1C2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[209:213]" "e[412:413]" "e[531:535]" "e[607]" "e[686]" "e[702]";
createNode polyTweakUV -n "polyTweakUV60";
	rename -uid "E00FB71C-48A0-9D58-0E9A-89A7AC7607B1";
	setAttr ".uopa" yes;
	setAttr -s 506 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -0.43651593 0.60501319 -0.29610777
		 0.80809557 -0.30106354 0.81076312 -1.99482393 0.45524883 -0.073869228 0.4101392 0.066345215
		 0.6132471 -0.21901345 0.95926803 -0.22577214 0.95999587 -2.0081734657 0.42892289
		 -0.44199705 0.56939262 -0.079329252 0.37452519 0.14263272 0.76402128 -0.21743345
		 0.97612679 -0.22261739 0.97890413 -2.044490337 0.41342497 -2.035724163 0.45713711
		 0.14424372 0.78100586 -2.26057696 0.34699368 -2.24051356 0.32015538 -0.0023213625
		 -4.2676926e-05 -0.0021840334 -0.00093722343 -0.0022939444 -0.00094419718 -0.0026383996
		 -7.5817108e-05 0.0051417649 0.00028550625 0.0052689612 -0.00052243471 -0.0014621615
		 -0.01331073 -0.0013605952 -0.01330483 -0.0023829341 -0.00079882145 -0.0022758245
		 -0.00078880787 0.0051827431 -0.00047105551 0.0060947537 -0.012865245 0.0036598444
		 -0.026867509 0.0068181753 -0.02421236 1.4834286 -0.23246187 1.46367669 -0.48958486
		 -0.21863079 -0.42343614 -0.27007329 -0.47710684 -2.35746813 -0.0033013821 -2.52414203
		 -0.094561815 -0.28126734 -0.17066211 -0.44224983 -0.23049721 -2.16857243 0.24585485
		 -0.37679327 -0.57930714 -0.54897201 -0.33269164 -2.12609482 0.4868238 -0.46610755
		 -0.62070233 -0.63829041 -0.37407586 -0.79694468 -0.67094666 -0.69916022 -0.6474514
		 0.67632324 -0.21505332 0.6600585 -0.17134523 0.59192079 -0.45704949 0.53989232 -0.45880657
		 0.65767378 -0.16043735 0.60694772 -0.45596963 0.59740061 -0.20235607 0.54388499 -0.20405394
		 0.64074367 -0.11232162 0.67631483 -0.44980434 0.61107016 -0.20114663 0.66384941 -0.16291261
		 0.80348992 -0.43938363 0.6222772 -0.021431208 0.68054008 -0.19504359 0.80771518 -0.18462287
		 1.025667429 -0.48529372 0.63850069 0.10875845 1.029893398 -0.23053458 -0.71022296
		 0.06912446 -0.81170559 -0.085138798 -0.80681437 -0.086372435 -0.70213175 0.069161713
		 -1.64259613 -0.052171707 -0.43840635 -0.28442413 -0.33699667 -0.12963909 -0.65487993
		 0.21289456 -0.64999163 0.21026796 -0.46873856 -0.32721382 -0.28617477 0.014859915
		 -0.009557128 0.0052106678 -0.0095248818 0.0051961541 0.019946933 -0.0084618926 0.019936323
		 -0.0084580779 -0.013445199 0.0069887936 -0.013416171 0.0069767535 0.015491843 -0.0053989291
		 0.015810966 -0.0055936575 0.017309427 -0.0074843168 -0.012032032 0.0060062259 -0.015900731
		 0.0077516586 -2.26351142 0.76595855 -2.22302294 0.75694168 0.0089751482 -0.0016288161
		 0.0090681314 -0.0016618371 0.013056636 -0.0035478175 -2.46106029 0.62020028 -2.45103002
		 0.58703446 0.0061278343 0.00030782819 -0.0022277236 -0.0025266409 -0.0025487542 -0.0025184155
		 -0.0027900338 -0.0016390085 -0.0026798844 -0.0016379356 -0.0022664666 -0.0017801523
		 -0.0023741126 -0.001778245 -0.0039691925 0.010720015 -0.0038675666 0.010721326 0.0047785342
		 -0.0014930964 0.005230099 -0.0022946596 0.0051914155 -0.0015404224 -0.0009354949
		 0.024806976 0.0035964549 0.010841966 0.0025630593 0.022181869 0.056315362 0.49002105
		 0.1348958 0.46857709 -0.078687012 0.034577847 -3.32094073 -1.65337229 -3.31009555
		 -1.51533914 -0.085583985 0.01506424 -3.16805887 -1.38086653 -0.096026659 0.00054645538
		 -0.024141133 -0.0071210861 -0.016846806 0.012407303 -2.96814585 -1.38402629 -0.10354656
		 -0.03118515 -0.034583747 -0.02163887 -2.59783053 -1.70515501 -0.04210338 -0.053370714
		 -0.041771442 -1.1845752 0.9961139 0.098173022 -0.015940547 0.24542332 -0.011537611
		 0.25569403 1.025269389 0.089529037 0.046465486 0.22435248 0.0511989 0.23500133 -0.010194838
		 0.2582202 1.00075125694 0.14175177 0.052435815 0.23708844 -0.0040323138 0.26819885
		 1.0007545948 0.13436615 0.058602512 0.24715114 0.0071163177 0.28688955 1.12319398
		 0.081347108 1.0024805069 0.13637996 0.069751203 0.26584184 0.023132026 0.3830533
		 1.21856105 0.14012051 0.085764766 0.36200523 0.0078603923 -0.009250164 0.0089279115
		 -0.010256648 -0.0071120858 0.0031284243 -0.007396102 0.0033041686 -0.011920214 0.0032529831
		 -0.012016237 0.0032973886 0.012058318 -0.009455815 -0.0026060641 0.00015637279 -0.012750089
		 0.0069541633 -0.013161063 0.0032271668 -0.0080909729 0.0038017631 -0.0022762418 -0.0012842417
		 -0.0022334456 -0.0012699366 -0.0023518801 -0.0012414455 -0.0023779869 -0.0012879372
		 0.0051843822 -0.0010070801 0.0052004755 -0.00097095966 0.65505016 0.091933489 -0.0022633076
		 -0.0012955666 0.0051735044 -0.001042366 0.65591681 0.081414223 0.65824914 0.080657005
		 0.65611839 0.089150429 0.65913731 0.092959642 0.65746152 0.092674255 -0.21235347
		 0.98654044 0.0095850229 -0.0014532208 0.0096657276 -0.0014898777 -0.21658921 0.99051869
		 0.14931655 0.79144371 0.16087627 0.81183183 0.0087023973 -0.0013362169 0.0089238882
		 -0.001418829 0.0058407784 0.00059506297 0.72099632 -0.097900867 0.71804994 -0.1603508
		 0.41823599 -0.43730354 0.27353778 -0.43412507 0.74350476 -0.095635414 0.72634065
		 -0.15215826 0.69289482 -0.21505237 0.51417679 -0.458177 0.4219982 -0.18256125 0.27729997
		 -0.17938246 0.72368175 -0.043633461 0.077620327 -0.43862492 0.74455845 -0.043303728
		 0.68469501 -0.21893668 0.5289979 -0.45916361 0.51793903 -0.20343396 0.081382275 -0.1838824
		 0.76633286 -0.037768126 -0.20913386 -0.42584702 0.75083506 -0.037832022 0.6684643
		 -0.20783615 0.53276008 -0.20442089 -0.20537192 -0.17110288 -0.2293945 -0.41528863
		 -2.50515652 -0.1100843 0.75141841 -0.039520979 -0.20508385 -0.1688018 0.7596339 -0.0558846
		 0.75316077 -0.039881945 0.76309967 -0.0718472 -2.66627526 0.049382925 0.80453259
		 -0.082497358 0.80036509 -0.10235524 0.80684495 -0.081140757 0.50943196 -1.3293252
		 0.78974092 -0.084199667 0.81023777 -0.073101521 0.79790765 -0.025382996 0.8503865
		 -0.0086574554 -0.075499952 0.035737276 -0.07678479 0.040598869 -0.053408682 0.11951351
		 0.8483333 0.0090920925 0.90311497 0.034879923 -0.015801102 0.014985323 -0.014607757
		 0.01950717 0.0087681413 0.098421335 -0.036776423 0.16005886 0.94424069 0.068132401
		 0.94052958 0.075850725 0.025400281 0.13896704 -0.024853766 0.19640088 0.037322998
		 0.17530906 -0.018004179 0.23889518 1.0053231716 0.12512326 0.044172734 0.2178036
		 -0.01683259 0.24320722 1.0047240257 0.13161635 0.045344472 0.22211528 1.0097489357
		 0.16462171 1.013646841 0.15720391 -0.55645096 0.37636444 -0.55152142 0.37371179 -0.18845356
		 0.17874986 -0.47131765 0.55614728 -0.46858907 0.55105132 -0.1058985 0.35618192 -0.44630682
		 0.57171077 -1.009449482 -0.48144671;
	setAttr ".uvtk[250:499]" -0.90965921 -0.31514388 -1.87689412 0.2149291 -1.9430747
		 0.13051367 -0.64063311 -0.68006492 -0.54089141 -0.51363546 -1.71221781 -0.030097246
		 -0.48340487 -0.36388874 1.26614034 -0.48000392 0.675951 0.23945403 1.27036762 -0.22524679
		 1.44338346 -0.48827863 0.69192988 0.33728242 1.44993877 -0.23484844 -2.51050067 -0.078337431
		 -2.65638924 0.069201231 -2.87132573 0.17720008 -2.29175878 0.34536338 -2.37590718
		 0.45172119 -2.38327527 0.44629979 -2.28267026 0.32047057 -2.471596 0.55982256 -2.49910879
		 0.57341814 -2.39645505 0.43785596 -2.2784183 0.29392242 -2.24882627 0.2810843 -2.26743579
		 0.31508327 -2.50078321 0.63948703 -2.49785352 0.59436119 -2.53525233 0.58060038 -0.0016405582
		 -0.00080227852 -0.0016830564 -0.00078216195 -0.011988163 0.0060507655 -0.011848867
		 0.0059532225 -0.0037717223 -0.00036802515 -0.014275074 0.0066867098 0.0046282783
		 0.004584983 -2.39953518 1.011175513 -2.36876512 0.94108057 -2.37249088 0.93655837
		 -2.39925075 1.18288767 0.018015802 -0.0057088137 0.0098254085 0.0015748739 -0.0029014051
		 0.001930505 0.043514609 0.43304348 1.33302605 0.14467669 0.057792127 0.48153019 0.65937221
		 -0.84239936 0.10614523 0.41199577 0.12056994 0.46235156 0.65459073 0.086446047 0.65465873
		 0.081192255 0.65826356 0.086728811 0.69393969 0.046162605 0.70326895 0.078550577
		 0.69214493 0.042396307 0.71660024 0.014796019 0.74468565 0.052234411 0.71497881 0.11669064
		 0.71387804 0.011980534 0.73829418 -0.025530338 0.76978654 0.016317844 0.77912635
		 0.10629869 0.71481121 0.12224865 0.73607987 -0.031131029 0.75128531 -0.02652216 0.7735135
		 -0.030963421 0.82702148 0.06188941 0.78137374 0.11307454 0.75099123 -0.029000521
		 0.82620174 -0.007655859 0.8325516 0.070634365 0.73919749 -0.04199934 0.75087434 -0.03244853
		 0.83382511 -0.0033988953 0.75388944 -0.060575008 0.75640047 -0.050181627 0.84191877
		 0.0047149658 0.84126508 0.06791091 0.73018956 -0.0602386 0.74474865 -0.08977747 0.91365653
		 0.051003456 0.91425937 0.11792207 0.84150672 0.07957983 0.68318522 -0.099617004 0.70098501
		 -0.13182402 0.97914052 0.089105606 0.98015422 0.12964225 0.91144633 0.1234386 0.66511548
		 -0.17312551 1.0047867298 0.11838746 1.0014312267 0.12568438 0.97990102 0.13324761
		 1.004336834 0.14014256 1.0018526316 0.12747169 1.000426054 0.14572716 -1.84694362
		 0.58554077 -1.87169123 0.60639167 -1.77092946 0.73228383 -1.78565693 0.75172353 -1.92766523
		 0.72204578 -2.14658618 0.60464406 -1.76519334 0.76068246 -1.76629651 0.77501762 -1.80103993
		 0.78395212 -1.99046469 0.83426034 -2.35587621 0.46568632 -1.77115369 0.79718852 -1.81609738
		 0.81676853 -2.19795942 0.79275477 -1.98979545 0.86509919 -1.78436625 0.81528795 -1.81956422
		 0.83988678 -1.79417562 0.82539976 -2.24362397 0.2917254 -2.21162009 0.30339193 -2.45628715
		 0.66069245 -2.48918653 0.63817263 -1.93667006 0.18214083 -2.020289421 0.39266133
		 -2.26710963 0.79567516 -2.370924 0.97185373 -1.93908787 0.12868142 -1.84760666 0.20755863
		 -1.83158946 0.35892844 -1.98515058 0.41355944 -2.23387885 0.80840635 -2.23167133
		 0.78696525 -2.21525264 0.94160688 -2.35682416 0.97862971 -2.40009403 1.0063420534
		 -1.71388113 0.033196449 -1.72910619 0.30443287 -1.81139731 0.38104272 -2.18690705
		 0.94384694 -2.21462727 1.018154383 -2.36784911 1.14484394 -1.67012215 0.0018866062
		 -1.71612585 0.18899918 -1.69894922 0.31893754 -2.18818331 1.02927196 -2.2941103 1.065878272
		 -2.35635185 1.18060517 -1.6905427 0.18696046 -2.27997398 1.079629421 -2.37802362
		 -0.026942968 -2.26792192 0.10673428 -2.58268452 0.19828463 -2.47130227 0.35250854
		 -2.87287593 0.34495521 -2.76635098 0.52671063 -0.014391601 0.016851425 -0.44119906
		 0.6075592 -0.0023749471 -0.0013331175 0.65557683 0.084483385 1.032806396 0.087302208
		 1.065911055 0.083238244 -0.012671813 0.0069090724 -0.007618621 0.0049960837 -0.85057002
		 -0.16671365 -0.83589858 -0.12991977 -2.40390277 1.18048632 0.012906902 -0.0026814081
		 -2.40735102 1.027987719 -1.71409988 -0.029805183 -1.88229394 0.20635128 -1.95621526
		 0.12150311 -0.20081234 1.0068747997 0.0066813231 0.00042489171 -0.20571303 1.0095082521
		 -0.12634337 -2.85196376 0.16994983 -2.50221539 -2.36297941 1.22383237 -2.36961627
		 1.22247505 -0.83844799 -0.12855205 -1.64520717 -0.050870657 -2.26279473 0.30253553
		 -2.50836086 0.60335064 1.024907708 0.08797586 1.03298533 0.085731506 1.066178083
		 0.081557274 0.99597323 0.096701026 0.99220562 0.098490119 0.9914487 0.092170596 1.12346745
		 0.079789519 1.21879101 0.13854706 0.94462645 0.056362152 0.87626779 0.022006512 1.33312678
		 0.14294434 0.80931485 -0.010512829 -3.53994632 -3.31201077 -3.71474242 -3.38491821
		 1.42700756 0.14671266 0.77966338 -0.067186832 -3.74902296 -3.50038528 -3.76784897
		 -3.50869083 -3.32725525 -3.37458134 -0.22074988 -1.34848535 0.66129208 -0.16598725
		 0.65488994 -0.15148544 0.61736465 -0.084518433 0.69143897 -0.21841073 0.7016803 -0.22327209
		 0.7100035 -0.21775579 0.56245273 0.053281307 0.54360354 0.27466965 0.70489252 -0.16712284
		 0.70758176 -0.10457349 0.59428173 0.54007483 0.71867967 -0.042843819 -2.085802078
		 0.40046358 -2.34771466 0.042694092 0.64191091 0.72099996 0.77245164 -0.027793407
		 -2.51672363 -0.10434937 0.75084203 -0.066152573 -1.99807262 0.72500968 0.78321725
		 -0.066767931 -2.50588059 -0.14164352 0.63664073 0.74103093 0.63105047 0.74331188
		 1.47084415 -0.49749333 1.51039577 -0.23576123 0.6394434 0.72174692 0.65581 -0.83712435
		 1.42846036 0.15092635 0.76146764 -0.071161032 -0.0091034174 0.058961153 0.031715214
		 0.04223156 -2.54457545 -0.087347507 -2.53080153 -0.1253736 -2.66717148 0.051232576
		 0.76085454 -0.093985081 0.75900215 -0.088406086 0.0039414167 -0.03096962 -0.015159786
		 0.018844604 -0.016467929 -0.022604942 0.020576119 -0.00033164024 0.018056452 -0.0016832352
		 0.0051620007 -0.027991533 -0.015247285 -0.019626617 0.79595912 -0.10783482 0.029551387
		 -0.03569603 -0.038632572 0.002014637 -2.90042973 0.17037821 -2.87381101 0.14898491
		 0.50809181 -1.32938576 0.51698577 -1.32719159 -3.20533848 -1.033324242 -3.19467282
		 -1.039396763 -2.50459838 -0.11997747;
	setAttr ".uvtk[500:505]" 0.75407237 -0.079156876 0.75923187 -0.039768219 0.7572068
		 -0.049209356 0.64514887 -0.86089742 -0.00084394217 0.021349311 -0.009367466 0.021974802;
createNode polyMapCut -n "polyMapCut30";
	rename -uid "48B09D85-4FED-5203-E190-BEB871D9F99A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[543]";
createNode polyTweakUV -n "polyTweakUV61";
	rename -uid "EE7B7E7E-43B3-14A0-2725-EF9E53CB19BF";
	setAttr ".uopa" yes;
	setAttr -s 18 ".uvtk";
	setAttr ".uvtk[128]" -type "float2" -0.050516427 0.07662493 ;
	setAttr ".uvtk[131]" -type "float2" -0.1048373 0.037377417 ;
	setAttr ".uvtk[141]" -type "float2" -0.29638284 -0.19259244 ;
	setAttr ".uvtk[145]" -type "float2" -0.49009696 -0.43433458 ;
	setAttr ".uvtk[294]" -type "float2" -0.71434295 -0.69189864 ;
	setAttr ".uvtk[404]" -type "float2" -0.11905104 0.02468878 ;
	setAttr ".uvtk[405]" -type "float2" -0.18333796 -0.048500359 ;
	setAttr ".uvtk[427]" -type "float2" -0.1075587 0.040323436 ;
	setAttr ".uvtk[428]" -type "float2" -0.12289038 0.0278005 ;
	setAttr ".uvtk[429]" -type "float2" -0.18745175 -0.04516381 ;
	setAttr ".uvtk[430]" -type "float2" -0.053620487 0.079087436 ;
	setAttr ".uvtk[433]" -type "float2" -0.30023769 -0.18949598 ;
	setAttr ".uvtk[434]" -type "float2" -0.49387139 -0.43121988 ;
	setAttr ".uvtk[437]" -type "float2" -0.7178539 -0.68853194 ;
	setAttr ".uvtk[441]" -type "float2" -0.89788568 -0.85499483 ;
	setAttr ".uvtk[474]" -type "float2" -0.89830858 -0.86543304 ;
	setAttr ".uvtk[506]" -type "float2" -0.041471124 0.086850584 ;
	setAttr ".uvtk[507]" -type "float2" -0.046223998 0.08770293 ;
createNode polyMapSewMove -n "polyMapSewMove55";
	rename -uid "116BBF71-407C-2734-C476-E8B0667F0AC9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[464]" "e[477]" "e[513]" "e[517:518]" "e[542]" "e[545]" "e[549]";
createNode polyTweakUV -n "polyTweakUV62";
	rename -uid "FA6E675A-49A6-3B35-B8A7-129A44844E5D";
	setAttr ".uopa" yes;
	setAttr -s 499 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 2.34364605 -0.29548183 2.44720244 -0.35356635
		 2.4487648 -0.35080495 2.65275931 2.70064116 2.22980237 -0.49888581 2.33350396 -0.55698949
		 2.53320503 -0.40223569 2.53383422 -0.39870098 2.67749047 2.72376609 2.32483006 -0.29503658
		 2.21096587 -0.49843651 2.42011261 -0.60535985 2.54533434 -0.40884128 2.5469439 -0.40588769
		 2.72033501 2.73005581 2.69838333 2.68992496 2.43221688 -0.61205184 2.99565268 2.74560285
		 2.97766399 2.77646089 0.75908053 1.7646594 0.76888466 1.70163393 0.77079391 1.70175612
		 0.76465273 1.76434922 0.70747828 1.75651515 0.71729249 1.69340336 0.7797271 1.64506078
		 0.77796257 1.64494824 0.76035571 1.76642895 0.75880229 1.76630831 0.70720112 1.75817251
		 0.72636801 1.636693 0.78543162 1.60158932 0.7331931 1.5977658 -0.098201513 0.86665326
		 0.020164251 1.067224145 1.35734069 0.27860069 1.42102182 0.2406078 3.22318792 3.13825321
		 3.37352395 3.13431883 1.23875856 0.078156441 1.3024404 0.040162146 3.045769215 3.031859398
		 1.50909495 0.18850502 1.39051521 -0.011945039 2.99561405 2.86603808 1.58112955 0.14639419
		 1.46255279 -0.054064125 1.60041308 0.1353178 1.48217344 -0.065714926 0.68651706 3.37887526
		 0.70524609 3.36303878 0.7087968 0.66197681 0.74996251 0.63768291 0.709355 3.35933757
		 0.6968292 0.66889286 0.58922827 0.46211737 0.63137972 0.43724024 0.73229164 3.34013104
		 0.64174265 0.70133948 0.57832164 0.46840489 0.71090227 3.35207272 0.54100859 0.76123089
		 0.76704806 3.29888606 0.52315801 0.50089824 0.42242402 0.56078959 0.3688432 0.86308694
		 0.76390767 3.20570946 0.25025874 0.66264564 2.035618067 -0.1203351 1.95898628 -0.073748797
		 1.95810938 -0.076254159 2.034818888 -0.12400538 2.49502206 3.33037925 1.83991778
		 -0.27722251 1.91908395 -0.32441059 2.12427235 -0.16928595 2.12276053 -0.17207786
		 1.81873548 -0.26402971 2.0080220699 -0.37504667 1.0023405552 -0.29339626 1.00057816505
		 -0.29678535 1.13367426 -0.36671197 1.13552475 -0.36314619 0.98886728 -0.27373821
		 0.98723692 -0.27684301 1.24327397 -0.42388535 1.24659002 -0.42077559 1.27281773 -0.10194887
		 1.13951337 -0.032057747 1.12602198 -0.01238098 2.88067961 2.29369545 2.83365154 2.30962086
		 1.26197982 -0.42908192 1.25995779 -0.43283051 1.38398409 -0.16042775 3.14550662 2.41401172
		 3.14065909 2.44935846 1.39944839 -0.168648 0.75631905 1.7822051 0.76180923 1.7832067
		 0.74855793 1.84557724 0.74664843 1.84546065 0.75656235 1.78078508 0.75811654 1.78089368
		 0.73996723 1.90227604 0.73820233 1.9021678 0.69503963 1.8374958 0.70471102 1.77415323
		 0.70495647 1.77272511 0.73266149 1.94565487 0.68658811 1.8942275 0.68096024 1.93323684
		 1.4246614 1.72723722 1.28515708 1.73611307 1.48408532 2.65712261 3.69120502 2.70786071
		 3.64768744 2.54954958 1.48669863 2.70155454 3.4768858 2.39447856 1.49053228 2.76285648
		 1.3471849 2.71027923 1.34457135 2.6658473 3.28037024 2.39536762 1.49391711 2.8128264
		 1.35101855 2.77158141 3.22492146 2.40172434 1.35440326 2.82155132 1.35523653 2.83522129
		 0.46607155 2.55405092 1.45752573 2.23445225 1.45570421 2.20581985 0.48349988 2.56454277
		 1.31801188 2.24317718 1.31614733 2.2138586 1.45510173 2.19754529 0.48762748 2.56791139
		 1.31559074 2.20632291 1.45263302 2.15925169 0.50798982 2.5874455 1.31311929 2.16797519
		 1.44839549 2.089037895 0.54422253 2.62352347 0.4925732 2.58377743 1.30888176 2.097761393
		 1.44090271 1.96920514 0.59435642 2.68873501 1.30138886 1.97792864 0.60365969 -0.087565236
		 0.60700238 -0.084181622 0.50332975 -0.035770655 0.50127649 -0.038431801 0.71685874
		 -0.14781405 0.71877664 -0.14428073 0.73969889 0.17709501 0.63493681 0.22849631 0.47443646
		 -0.02333042 0.85487521 0.11770778 0.60631943 0.24184905 0.75774992 1.7735498 0.75830936
		 1.7691952 0.75980526 1.77013218 0.7592358 1.77365994 0.7061435 1.76545167 0.70673001
		 1.76107562 1.038198948 2.93180227 0.75703835 1.77768612 0.70545608 1.76961279 1.030255437
		 2.95892763 1.029877305 2.9508431 1.036289454 2.92382097 1.03059268 2.92840433 1.031247735
		 2.92007399 2.55133748 -0.41206032 1.28375447 -0.44097024 1.28184855 -0.44450998 2.55357933
		 -0.4095538 2.43822289 -0.61528069 2.44945765 -0.62138951 1.26954329 -0.43322763 1.2684325
		 -0.43733817 1.40702426 -0.17278998 0.65815264 3.24072862 0.67427856 3.32330513 0.8444742
		 0.58169478 0.95826399 0.51437628 0.6639117 3.2389245 0.68160784 3.32451487 0.67669755
		 3.37249517 0.77022594 0.62569493 0.72589153 0.38125235 0.83968139 0.31393382 0.63120145
		 3.13798952 1.11288214 0.42290348 0.63437968 3.13533854 0.68189949 3.37837934 0.7586385
		 0.63262242 0.65164316 0.42525232 0.99429953 0.2224609 0.59819424 3.04131937 1.3382057
		 0.29021561 0.60238785 3.045541286 0.68347079 3.37260413 0.64005578 0.43217981 1.21962333
		 0.089771807 1.34532332 0.27499887 3.39442873 3.14209414 0.59964621 3.040453434 1.2315222
		 0.08263652 0.59870452 3.036912441 0.60031486 3.038961411 0.59248871 3.026979923 3.51560187
		 2.91888523 0.54995739 2.99393106 0.54556525 3.0015444756 0.54589224 2.99142218 0.54147524
		 2.99718952 0.54140306 2.98621774 0.54510838 2.97821927 0.52372277 2.8172667 0.52958649
		 2.81923127 1.47824407 2.65237045 1.48339868 2.64370465 1.47328591 2.48707986 0.5077042
		 2.70696163 0.5136078 2.7051959 1.34435523 2.66074371 1.34388494 2.65242934 1.33377218
		 2.49580479 1.4665556 2.37946129 0.47869438 2.62832808 0.48384112 2.62319827 1.32704186
		 2.38818598 1.46160269 2.30026031 1.32208896 2.30898499 1.45840764 2.24855614 0.45578122
		 2.58189464 1.31889391 2.25728106 1.45793927 2.24046659 0.46314305 2.55247021 1.31842542
		 2.24919152 0.4610315 2.57843328 2.2124083 -0.21860719 2.2108655 -0.22137955 2.096696377
		 -0.42469972 2.31711984 -0.27719477 2.31415105 -0.27855065 2.20026493 -0.48193991
		 2.32618237 -0.29261953 1.74037123 0.051759154 1.82747221 0.00030082464;
	setAttr ".uvtk[250:498]" 2.63839674 3.037416697 2.67008042 3.044045925 1.62187362
		 -0.14878458 1.70901155 -0.20033744 2.54348373 3.29884338 1.79715824 -0.25119379 0.1790601
		 0.97536594 0.7244972 3.14024949 0.060475528 0.7749247 0.040085316 1.05798173 0.6955213
		 3.098164558 -0.080247283 0.8585369 3.39141726 3.10637617 3.50086117 2.90313482 3.66980815
		 2.73400688 3.026465893 2.74093866 3.090490103 2.6095922 3.099469185 2.61370134 3.022804737
		 2.76902795 3.16446757 2.47674227 3.18926811 2.45726275 3.11315751 2.62048292 3.023091078
		 2.7998724 2.99339032 2.81825781 3.0078332424 2.77812052 3.18392491 2.38654566 3.18473768
		 2.43588614 3.22076368 2.44219756 0.83009142 -0.20725651 0.831967 -0.20370379 0.96460819
		 -0.2778846 0.96399558 -0.27379751 0.96882814 0.057922624 1.10112381 -0.012424335
		 0.21701738 0.10686523 3.0030331612 2.0030965805 2.97233391 1.99279141 2.98549509
		 1.99712992 3.020886421 1.71916723 0.46294942 0.31429687 0.34807691 0.37116274 0.57779479
		 0.25588137 1.43264294 1.8371098 0.65617263 2.74638271 1.42679119 1.7402451 0.63548726
		 2.86527205 1.29312921 1.8458333 1.28718257 1.74776471 1.038712859 2.94053984 1.037388325
		 2.94889426 1.030032396 2.93969226 0.90022093 3.011224747 0.89875609 2.93846297 0.90248144
		 3.018568516 0.78791398 3.046276331 0.77238691 2.95364499 0.89195883 2.85996699 0.78945082
		 3.053271532 0.69561386 3.073222876 0.68334323 2.9789598 0.74780518 2.84493184 0.89384389
		 2.85119128 0.69793922 3.08190155 0.63157892 3.056945324 0.60789734 3.0097005367 0.63736659
		 2.88104725 0.74602199 2.83444524 0.6314491 3.063596725 0.57936335 2.94023275 0.63490099
		 2.87072229 0.68851447 3.092046976 0.63550788 3.07200861 0.57433105 2.93145466 0.72101331
		 3.14335275 0.66162741 3.1415019 0.57307035 2.91812658 0.62249869 2.87136555 0.75878328
		 3.20922303 0.69301438 3.22844982 0.56121677 2.79801846 0.64024013 2.74248123 0.67924464
		 2.84155655 0.76012397 3.29701018 0.71313518 3.30690503 0.54481858 2.69636178 0.60711986
		 2.66294193 0.70385104 3.35670447 0.5171181 2.62067938 0.55663419 2.60329628 0.48505372
		 2.5830934 2.44120002 2.59460068 2.46352363 2.5666132 2.31096911 2.44389153 2.32322669
		 2.42010498 2.49870396 2.42072988 2.78292108 2.50139523 2.29813886 2.41298771 2.29604578
		 2.39659429 2.33400249 2.37931323 2.54576874 2.27651834 3.066516399 2.5989933 2.29658747
		 2.37040949 2.34389925 2.33814406 2.79885912 2.27472305 2.54017329 2.24121046 2.30839276
		 2.34686017 2.34439874 2.31162596 2.31796432 2.33321261 2.98409629 2.80827355 2.9449873
		 2.80209517 3.12971377 2.37210608 3.17017269 2.39032173 2.66263294 3.0020599365 2.69773865
		 2.75714517 2.87842607 2.26041245 2.97555566 2.035489321 2.67481661 3.049085617 2.59720373
		 3.05218792 2.50637007 2.86940479 2.65521026 2.74536967 2.83605433 2.2487812 2.83980346
		 2.27388263 2.77332711 2.070591211 2.9440496 1.95931172 3.0095348358 2.00074195862
		 2.52850199 3.24232483 2.43481398 2.97395229 2.47861338 2.84999824 2.73959613 2.073228359
		 2.76438713 1.9452045 2.97964406 1.76050687 2.5027678 3.27999568 2.47685432 3.10744286
		 2.40035248 2.96756625 2.73135448 1.9335227 2.86744094 1.85117483 2.97626662 1.71499467
		 2.45521951 3.115839 2.85290265 1.83309078 3.23443365 3.10161686 3.071478605 3.0010037422
		 3.38768411 2.8025136 3.22307754 2.68429804 3.61590171 2.57090664 3.45603132 2.42882752
		 0.66616386 2.48781252 2.34512973 -0.29285491 0.75863713 1.77697754 1.034920096 2.95619082
		 0.47557199 -0.020974509 0.44702303 -0.0068647116 1.91492319 -0.051121861 1.93660975
		 -0.063856632 3.025261879 1.71853161 0.33181989 0.049985703 3.00019478798 2.0020871162
		 2.54631042 3.30107665 2.6519351 3.04031229 2.66712165 3.043479204 2.56258583 -0.41812882
		 1.42126882 -0.18049045 2.56410718 -0.41534206 1.5006274 2.9238205 1.36111629 2.93258667
		 2.99500585 1.66768789 2.99883485 1.66539454 1.93743634 -0.062492281 2.49668026 3.33340716
		 3.0055539608 2.79188061 3.1941638 2.4252069 0.48464835 2.55236173 0.48880655 2.55707955
		 0.5035212 2.59126806 0.47352713 2.53738332 0.45431066 2.57390499 0.45406592 2.58238554
		 0.52921438 2.64793706 0.57673752 2.71526527 0.47678423 2.62912369 0.50572562 2.70777154
		 0.66523486 2.75184774 0.52187043 2.81765079 3.48082137 2.38896084 3.65357733 2.54613042
		 0.7305299 2.81125116 0.53965843 2.99151731 3.69653749 2.7035799 3.72163486 2.71591496
		 3.28454185 2.38942003 1.49492061 2.8261373 0.70600945 3.36394739 0.7101444 3.36026454
		 0.73317146 3.34091306 0.68730986 3.3798275 0.68088883 3.38013601 0.6747936 3.3729651
		 0.76817799 3.29909587 0.76493591 3.20520616 0.67215306 3.32377076 0.65609515 3.24115086
		 0.72528428 3.13971901 0.62990254 3.13808775 3.041642904 3.037278652 3.22193885 3.14495826
		 0.69650531 3.10155702 0.59654737 3.040628195 3.37097979 3.14063644 0.60949886 3.067791224
		 2.99117565 2.87169409 0.53359526 3.0086259842 3.39128375 3.15993738 0.69725323 3.094088793
		 0.69983351 3.090999126 0.016255558 1.073378563 -0.10881126 0.87598497 0.69827592
		 3.099512815 0.6363067 2.86132765 0.59495115 3.0483675 0.61902237 2.54349351 0.63227761
		 2.54326797 3.42574596 3.10344434 3.41386604 3.1362412 3.51637506 2.91728997 0.59030181
		 3.033140659 0.59188175 3.026591063 0.62390965 2.54146647 0.6655578 2.48938489 0.62737149
		 2.54525471 0.57692879 2.50857306 0.57397926 2.50699067 0.62378186 2.54115462 0.62724364
		 2.54494286 0.54885739 3.0031714439 0.64082825 2.47997212 0.60196763 2.46236324 3.70385385
		 2.74759555 3.68089604 2.76604676 0.54294902 2.99733019 0.53731161 3.010954857 3.71392918
		 2.72177029 3.70473003 2.72700763 3.3939476 3.15062666 0.60197961 3.071303129 0.5977903
		 3.038151264 0.59973884 3.047738075 0.6395427 2.87230134 0.74753344 1.94583821 0.67899597
		 1.93755198 0.47277653 2.53153229;
createNode objectSet -n "set3";
	rename -uid "372ABE13-45DE-2A63-F15F-9FB58A5A5FA4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "CD4E981B-46A1-FAF2-43A5-8CBA6606DE61";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "38426815-428E-0141-3EA6-A1965B55E080";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 5 "e[75]" "e[406]" "e[689]" "e[693]" "e[698:699]";
createNode deleteComponent -n "deleteComponent14";
	rename -uid "1F1B98EA-4722-40D3-DC9C-E4AE0D241FB4";
	setAttr ".dc" -type "componentList" 2 "f[17]" "f[172]";
createNode objectSet -n "set4";
	rename -uid "095FC782-4ED7-B375-E834-C9922DCEC4E0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "8E528E1D-40DB-EACC-96E5-0BB7D0589D8B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "94403A13-421E-FA9A-A83D-C7B7CBE4B13C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 5 "e[75]" "e[685]" "e[687]" "e[691]" "e[693:694]";
createNode deleteComponent -n "deleteComponent15";
	rename -uid "F289837C-4598-AD84-AFD2-C5AA30B08E1C";
	setAttr ".dc" -type "componentList" 2 "f[17]" "f[171]";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "C905CA51-4CA0-D9F9-2961-95B3B3C7F6A1";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -44.444442678380966 -71.428568590255125 ;
	setAttr ".tgi[0].vh" -type "double2" 461.11109278820254 44.444442678380966 ;
createNode polyTweakUV -n "polyTweakUV63";
	rename -uid "A5A6C66D-4B90-4356-F48B-03B5A7C50DC2";
	setAttr ".uopa" yes;
	setAttr -s 487 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0.27325103 0.51182681 0.30348828 0.49572739
		 0.3039262 0.49653569 0.030848026 0.38582429 0.24136576 0.45228535 0.27164611 0.43618
		 0.32867542 0.48206502 0.32883957 0.4830904 0.034380674 0.38912782 0.26780739 0.51183271
		 0.23591575 0.45229217 0.29701325 0.4226065 0.33224502 0.48017642 0.33269516 0.48104113
		 0.040500879 0.3900263 0.037365198 0.38429353 0.30057535 0.42069221 0.079828858 0.39224741
		 0.077259302 0.39665505 0.2975778 0.13609073 0.29757798 0.053065173 0.29757792 0.053065114
		 0.2975778 0.13609073 0.2278024 0.13609073 0.22780231 0.053065233 0.29757792 -0.021653008
		 0.29757786 -0.021653067 0.29782772 0.13834855 0.29757792 0.13834879 0.2278024 0.13834861
		 0.22780228 -0.021653127 0.29757786 -0.075941868 0.22780222 -0.075941809 -0.45034435
		 0.85313845 -0.41518286 0.91271955 -0.017964616 0.67845279 0.00095228851 0.66716665
		 0.11233091 0.44833574 0.13380575 0.44777349 -0.053190365 0.61890948 -0.034273222
		 0.60762292 0.086987257 0.43313774 0.027115077 0.65168911 -0.0081100017 0.59214413
		 0.079823017 0.40945098 0.048513323 0.63917977 0.013289317 0.57963228 0.054919153
		 0.63440651 0.020763084 0.57512885 -0.38676244 0.70470589 -0.37466598 0.69847059 -0.21061942
		 0.79233772 -0.19839081 0.7851209 -0.37203079 0.69662338 -0.21417448 0.79439217 -0.24613819
		 0.73296791 -0.23361674 0.72557801 -0.35631633 0.69233078 -0.23053834 0.80403066 -0.249378
		 0.73483568 -0.37050068 0.69250017 -0.26046214 0.82182187 -0.33323854 0.67655385 -0.26576486
		 0.74448812 -0.2956886 0.76227927 -0.31160519 0.85207891 -0.30832398 0.63223654 -0.34683165
		 0.79253638 0.18301263 0.56101733 0.16063359 0.5740115 0.16039202 0.57328284 0.18279538
		 0.55995631 0.0083121061 0.47577897 0.12721476 0.51446462 0.1503478 0.50130534 0.20901999
		 0.54719579 0.20859709 0.54637843 0.1210278 0.51814067 0.17644581 0.48696595 0.25560054
		 0.67220563 0.25503242 0.67132735 0.28950334 0.64880866 0.29009995 0.64973313 0.25243601
		 0.67791516 0.25191131 0.67711103 0.31790996 0.63039368 0.31889409 0.63115555 0.33427593
		 0.71740079 0.29974717 0.73991501 0.29657829 0.7456302 0.063405395 0.32769415 0.056687593
		 0.32996926 0.32287803 0.62849766 0.32223007 0.62752789 0.36307648 0.69858932 0.10123467
		 0.34488115 0.10054231 0.34993002 0.36708286 0.69595289 0.2975778 0.15993175 0.29757798
		 0.15993187 0.29757816 0.24275872 0.2975778 0.24275884 0.2975778 0.1574671 0.29782772
		 0.15746698 0.2975781 0.31749305 0.2975781 0.31749317 0.22780243 0.24275884 0.22780237
		 0.15993175 0.22780237 0.1574671 0.29757816 0.37177512 0.22780249 0.31749317 0.22780257
		 0.37177512 -0.52701712 0.19406238 -0.59611821 0.19845894 -0.49758238 0.65466499 0.17918634
		 0.38685581 0.17297006 0.36424181 -0.49628788 0.67667347 0.14857185 0.3420907 -0.49438894
		 0.70703858 -0.56539345 0.68099523 -0.56668794 0.65898693 0.12050056 0.34221742 -0.49271214
		 0.73179018 -0.56349456 0.71136034 0.11264992 0.34326062 -0.56181794 0.73611182 -0.56127632
		 0.74301028 -0.51838648 0.26786444 -0.51073825 0.44530258 -0.51164061 0.43112001 -0.50508052
		 0.26747271 -0.579844 0.44962439 -0.58076751 0.43510184 -0.51193887 0.42702135 -0.50196451
		 0.26764223 -0.58104324 0.43136927 -0.51316166 0.40805313 -0.48625559 0.26318333 -0.58226752
		 0.41237435 -0.51526064 0.37327406 -0.45816952 0.26468214 -0.49859303 0.27036974 -0.58436644
		 0.37759528 -0.51897228 0.31391701 -0.41306478 0.291738 -0.58807796 0.31823823 0.15221122
		 0.73860681 0.15320933 0.73944229 0.12620369 0.75532693 0.12557542 0.75465465 0.18152636
		 0.71926945 0.18214056 0.72018343 0.19613367 0.80724704 0.16890603 0.82397437 0.11865076
		 0.75946772 0.2260105 0.78809685 0.16145119 0.82835686 0.2975778 0.1479089 0.2975778
		 0.14270136 0.29782772 0.14270136 0.29782772 0.14790902 0.2278024 0.14790902 0.2278024
		 0.14270148 -0.13079882 0.39901766 0.2975778 0.15330312 0.2278024 0.153303 -0.13209802
		 0.41532132 -0.13342756 0.4108586 -0.13239533 0.39486727 -0.13539422 0.3979542 -0.13549799
		 0.39332375 0.33400276 0.47927445 0.32850793 0.62469393 0.32789686 0.6237781 0.33463833
		 0.48001373 0.30233392 0.41978726 0.3056201 0.41808265 0.32483348 0.62717235 0.32442433
		 0.62608027 0.36904168 0.69462836 -0.37995338 0.61954612 -0.38685709 0.66859543 -0.17031536
		 0.7684893 -0.13651332 0.74849176 -0.37714583 0.61934328 -0.3839528 0.67046571 -0.39160603
		 0.69910073 -0.1923714 0.78155982 -0.20554128 0.70894635 -0.17173931 0.68894881 -0.3738572
		 0.55848867 -0.090582818 0.72131908 -0.37130362 0.55683815 -0.38943982 0.70329791
		 -0.19581357 0.78361773 -0.22759733 0.72201687 -0.12580869 0.66177613 -0.40273684
		 0.50465655 -0.023648754 0.68190312 -0.39857811 0.505656 -0.38820797 0.70003074 -0.23103949
		 0.72407472 -0.058874518 0.62235981 -0.021534517 0.67738289 0.13679218 0.44888422
		 -0.40461791 0.50181437 -0.055339962 0.62024015 -0.41017252 0.49866995 -0.40516722
		 0.49955019 -0.41612184 0.46660349 0.15410113 0.41700009 -0.4242 0.43343458 -0.42898184
		 0.43324956 -0.42512476 0.43133149 -0.42737246 0.43117055 -0.42536557 0.42793331 -0.42209995
		 0.42503318 -0.42870593 0.38143966 -0.42581767 0.38134953 -0.50047576 0.65231115 -0.49792224
		 0.64801872 -0.50293165 0.57043719 -0.46869993 0.33718011 -0.46627891 0.33579251 -0.56679517
		 0.6564588 -0.56702822 0.65234041 -0.57203734 0.57475924 -0.50626546 0.51713037 -0.50018513
		 0.29928908 -0.49870276 0.29615328 -0.57537091 0.52145207 -0.50871891 0.47789952 -0.57782447
		 0.4822211 -0.51030135 0.45228896 -0.52011609 0.27525982 -0.57940704 0.45661005 -0.51053339
		 0.44828185 -0.52046889 0.27011392 -0.5796392 0.45260331 -0.51775259 0.27264652 0.23479322
		 0.53343999 0.23436126 0.53262854 0.20237914 0.47311172 0.26547369 0.51696813 0.26462045
		 0.51656032 0.23272243 0.45702302 0.26818624 0.51254022 0.096648842 0.60926598 0.12211475
		 0.59487534;
	setAttr ".uvtk[250:486]" 0.028796196 0.43393138 0.033322096 0.43487832 0.063380808
		 0.55057377 0.088857561 0.53615516 0.015238404 0.47127488 0.11469969 0.52177352 -0.36798164
		 0.8854323 -0.30917192 0.57342309 -0.40320817 0.82588971 -0.40926513 0.9099741 -0.31938624
		 0.52372187 -0.44501087 0.85072744 0.1363616 0.443782 0.15199542 0.41475007 0.17612898
		 0.39059076 0.084229946 0.39158091 0.093375683 0.37281868 0.094658375 0.37340567 0.083707333
		 0.39559349 0.10394323 0.35384151 0.10748577 0.35105929 0.096613646 0.37437424 0.083748102
		 0.39999947 0.079505444 0.40262565 0.081568599 0.3968921 0.10672259 0.34095761 0.1068387
		 0.34800562 0.11198497 0.34890732 0.21085939 0.70013362 0.21146259 0.70105362 0.24571452
		 0.67741048 0.24565288 0.67854065 0.25553143 0.76884949 0.28978834 0.7462604 0.051828444
		 0.80160004 0.080883384 0.28618333 0.076498032 0.28471157 0.078378081 0.28533134 0.083433509
		 0.2456257 0.12422693 0.85179484 0.094382167 0.87026942 0.15403524 0.83291602 -0.52306348
		 0.24848577 -0.39156014 0.35143301 -0.52596229 0.20050558 -0.36026567 0.36830136 -0.59216928
		 0.25280687 -0.59511495 0.2042304 -0.12935287 0.40369388 -0.12935472 0.40856716 -0.13455665
		 0.40443382 -0.20312983 0.45948908 -0.20977879 0.41654012 -0.20106012 0.4638221 -0.27086413
		 0.48927599 -0.28386563 0.43013749 -0.21839035 0.37368593 -0.26873302 0.4938609 -0.32410508
		 0.50846303 -0.3378886 0.4423202 -0.2961843 0.37055346 -0.2178086 0.3689197 -0.32070518
		 0.51350725 -0.37029159 0.5025301 -0.39424169 0.46004745 -0.3568604 0.37640795 -0.2959339
		 0.36550626 -0.36817253 0.50779486 -0.39493871 0.41149971 -0.35648638 0.36986169 -0.3249734
		 0.51884341 -0.36533052 0.51301885 -0.39595675 0.40647313 -0.3136999 0.57387829 -0.3548159
		 0.5612129 -0.3961786 0.40117738 -0.36331505 0.36752114 -0.31298441 0.63160414 -0.360403
		 0.61890298 -0.41556656 0.37098309 -0.39518213 0.35134086 -0.36330575 0.35728654 -0.33752918
		 0.67340589 -0.36603248 0.6660338 -0.45228904 0.32734165 -0.41688144 0.29455408 -0.37542933
		 0.69367641 -0.48107278 0.29054829 -0.46019799 0.26961967 -0.50343508 0.2719191 0.0006275177
		 0.37067696 0.0038163662 0.36667916 -0.017974973 0.34914884 -0.016224146 0.34575102
		 0.0088419914 0.34584031 0.049440861 0.35736308 -0.019807935 0.3447344 -0.020106792
		 0.34239265 -0.014684677 0.33992419 0.015564919 0.32524046 0.089951396 0.37130472
		 -0.020029545 0.33865234 -0.013271093 0.33404347 0.051717877 0.32498392 0.014765739
		 0.32019696 -0.01834321 0.33528849 -0.013199925 0.33025548 -0.01697576 0.33333883
		 0.078177691 0.40119931 0.072591305 0.40031686 0.098978758 0.3388947 0.10475802 0.3414968
		 0.032258153 0.42888072 0.03727293 0.39389601 0.063083529 0.32294008 0.076958299 0.29081056
		 0.033998728 0.43559805 0.022912025 0.43604127 0.0099369287 0.40993169 0.031197786
		 0.39221367 0.057030916 0.32127818 0.057566404 0.32486412 0.048070669 0.29582486 0.072457433
		 0.27992925 0.081812143 0.2858474 0.013098001 0.46320155 -0.00028467178 0.42486557
		 0.0059719086 0.40715948 0.043252468 0.29620156 0.046793818 0.27791378 0.077542305
		 0.25153074 0.0094219446 0.46858266 0.0057207346 0.44393435 -0.0052074194 0.42395374
		 0.042075276 0.27624509 0.061514378 0.26448235 0.077059507 0.24502966 0.0026302338
		 0.44513366 0.059437871 0.26189896 0.11393762 0.44310221 0.090659857 0.4287301 0.13582873
		 0.40037677 0.11231542 0.38349006 0.16842842 0.36729273 0.14559174 0.34699723 0.27366653
		 0.51259601 0.29782772 0.153303 -0.12964737 0.4129729 0.11902043 0.76008105 0.11160013
		 0.76466185 0.14775893 0.58032835 0.15411612 0.57672507 0.084058523 0.24553487 0.081653833
		 0.78312361 0.080477715 0.2860392 0.015641928 0.47159401 0.030730009 0.43434533 0.03289938
		 0.43479738 0.33729318 0.47758174 0.37272629 0.69216061 0.33771852 0.47839761 -0.48934877
		 0.78694946 -0.55825996 0.79124898 0.079736114 0.23827228 0.080283999 0.23794422 0.15435383
		 0.5771268 0.0085575581 0.47621301 0.0812428 0.39885804 0.10818529 0.34648022 -0.50490272
		 0.26653275 -0.5019592 0.26660594 -0.48615223 0.26214299 -0.51816297 0.2669315 -0.52140874
		 0.27027997 -0.52090925 0.27573523 -0.45777023 0.26380321 -0.41237259 0.29107901 -0.50100648
		 0.2998049 -0.46948355 0.33775148 -0.39089543 0.35074696 -0.42950243 0.38184234 0.14913559
		 0.34130237 0.17381251 0.36375329 -0.3702141 0.35575494 -0.42574722 0.42405137 0.17994928
		 0.38624415 0.18353415 0.38800606 0.12109756 0.34136769 -0.49218291 0.73853189 -0.37406689
		 0.69921714 -0.37150615 0.69751686 -0.35568696 0.69316489 -0.38625085 0.70548898 -0.39001435
		 0.70405233 -0.39252549 0.69914877 -0.33244127 0.67709941 -0.30739254 0.632478 -0.38782698
		 0.66860551 -0.38092339 0.61951798 -0.30820435 0.5734517 -0.37481099 0.55857646 0.086397767
		 0.43391171 0.11215258 0.44929346 -0.3189593 0.52444279 -0.40244997 0.50521994 0.1334424
		 0.44867614 -0.40411127 0.50910044 0.079188466 0.41025892 -0.42951947 0.41604921 0.13634276
		 0.45143309 -0.3168366 0.52234912 -0.31567705 0.52308649 -0.41634396 0.9145478 -0.45349601
		 0.85591048 -0.31833488 0.52560496 -0.3595621 0.36858162 -0.40845269 0.50209355 0.14126575
		 0.44336316 0.13956904 0.44804797 0.15421128 0.41677216 -0.41943145 0.46697053 -0.41918737
		 0.46783993 -0.427984 0.43670103 0.18099236 0.39253184 0.17771256 0.39516774 -0.42749214
		 0.43109879 -0.43040967 0.41692671 0.1824317 0.38884291 0.18111801 0.38959107 0.13672328
		 0.45010325 -0.40533954 0.50848651 -0.40620863 0.50289905 -0.40614009 0.50276852 -0.35719603
		 0.36955842 0.28240705 0.3747389 0.22780252 0.37473902 -0.52082324 0.26907381;
createNode polyTweakUV -n "polyTweakUV64";
	rename -uid "EDA14E84-4667-77F6-805A-8F9FEDE606EA";
	setAttr ".uopa" yes;
	setAttr -s 52 ".uvtk[0:51]" -type "float2" 0.81764537 -0.50932074 0.81956631
		 -0.50932074 0.81956631 -0.0039128796 0.81764537 -0.0039128754 0.81956631 -0.51124161
		 0.81764537 -0.51124161 0.82853186 -0.50932074 0.82853186 -0.0039128796 0.81956631
		 -0.0019919702 0.81764537 -0.0019919705 0.82853186 -0.51124161 0.83045268 -0.50932074
		 0.83045268 -0.0039128796 0.82853186 -0.0019919702 0.83045268 -0.51124161 0.83045268
		 -0.0019919702 0.75772184 -0.50932074 0.75964272 -0.50932074 0.75964272 -0.0039128801
		 0.75772184 -0.0039128796 0.75772184 -0.51124161 0.75964272 -0.51124161 0.75772184
		 -0.0019919702 0.75964272 -0.0019919705 0.74875629 -0.0039128796 0.74875629 -0.50932074
		 0.74875629 -0.51124161 0.74875629 -0.0019919702 0.74683547 -0.50932074 0.74683547
		 -0.0039128792 0.74683547 -0.51124161 0.74683547 -0.0019919698 0.81572455 -0.50932074
		 0.81572455 -0.0039128489 0.81572455 -0.51124161 0.81572455 -0.0019919393 0.7615636
		 -0.0039128489 0.7615636 -0.50932074 0.7615636 -0.51124161 0.7615636 -0.0019919393
		 0.68883264 -0.50932074 0.69075358 -0.50932074 0.69075358 -0.0039128787 0.68883264
		 -0.0039128787 0.68883264 -0.5112415 0.69075358 -0.5112415 0.74491459 -0.50932074
		 0.74491459 -0.0039128787 0.69075358 -0.0019919695 0.68883264 -0.0019919695 0.74491459
		 -0.5112415 0.74491459 -0.0019919695;
createNode polyTweakUV -n "polyTweakUV65";
	rename -uid "2A1FE2A6-4A01-D4B6-3292-C8BBD9153CCB";
	setAttr ".uopa" yes;
	setAttr -s 84 ".uvtk[0:83]" -type "float2" 0.8521058 -0.2320599 0.8521058
		 -0.22806776 0.785869 -0.22806776 0.785869 -0.2320599 0.85615736 -0.22806776 0.85615736
		 -0.2320599 0.8521058 0.48277405 0.785869 0.48277405 0.78181738 -0.22806776 0.78181738
		 -0.2320599 0.85615736 0.48277405 0.8521058 0.48676628 0.785869 0.48676628 0.78181738
		 0.48277405 0.85615736 0.48676628 0.78181738 0.48676628 0.67100936 -0.22806776 0.67100936
		 -0.2320599 0.73724627 -0.2320599 0.73724627 -0.22806776 0.66695786 -0.22806776 0.66695786
		 -0.2320599 0.74129784 -0.22806776 0.74129784 -0.2320599 0.73724627 0.48277405 0.67100936
		 0.48277405 0.66695786 0.48277405 0.74129784 0.48277405 0.67100936 0.48676628 0.73724627
		 0.48676628 0.66695786 0.48676628 0.74129784 0.48676628 -0.2833268 -0.070288315 -0.27927521
		 -0.070288315 -0.27927521 -0.0040514693 -0.2833268 -0.0040514693 -0.2833268 -0.074339852
		 -0.27927521 -0.074339852 -0.2833268 9.5510948e-08 -0.27927521 9.5510948e-08 -0.31574324
		 -0.0040514693 -0.31574324 -0.070288315 -0.31574324 -0.074339852 -0.31574324 9.5510948e-08
		 -0.31979477 -0.070288315 -0.31979477 -0.0040514693 -0.31979477 -0.074339852 -0.31979477
		 9.5510948e-08 0.10743728 -0.070288368 0.11148879 -0.070288368 0.11148879 -0.0040514907
		 0.10743728 -0.0040514907 0.11148879 -0.074339911 0.10743728 -0.074339911 0.14390531
		 -0.070288368 0.14390531 -0.0040514907 0.11148879 9.5510948e-08 0.10743728 9.5510948e-08
		 0.14390531 -0.074339911 0.14795682 -0.070288368 0.14795682 -0.0040514907 0.14390531
		 9.5510948e-08 0.14795682 -0.074339911 0.14795682 9.5510948e-08 0.86020899 -0.22806776
		 0.86020899 -0.2320599 0.89262539 -0.2320599 0.89262539 -0.22806776 0.89667696 -0.22806776
		 0.89667696 -0.2320599 0.89262545 0.48277405 0.86020899 0.48277405 0.89667696 0.48277405
		 0.86020899 0.48676628 0.89262545 0.48676628 0.89667696 0.48676628 0.77776587 0.48277405
		 0.77776587 -0.22806776 0.77776587 0.48676628 0.74534941 0.48277405 0.74534941 -0.22806776
		 0.77776587 -0.2320599 0.74534941 0.48676628 0.74534941 -0.2320599;
createNode polyMapSewMove -n "polyMapSewMove56";
	rename -uid "1D8F0115-4C77-911C-6AD2-458129D35CA1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[366]" "e[578]";
createNode polyTweakUV -n "polyTweakUV66";
	rename -uid "6A1BED67-4C90-C1E3-4A7C-CBAFA482A38A";
	setAttr ".uopa" yes;
	setAttr -s 484 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0.48953837 -0.10638955 0.49308074 -0.11172929
		 0.4931885 -0.11163464 0.42113411 -0.02626577 0.47941661 -0.11336508 0.48296404 -0.1187056
		 0.49603152 -0.11620572 0.49610716 -0.11608556 0.42137694 -0.025421888 0.48890066
		 -0.10591033 0.47877812 -0.11288562 0.48593581 -0.12317148 0.49644971 -0.11685076
		 0.49655885 -0.11674944 0.4222368 -0.024725467 0.42227173 -0.0259085 0.48635316 -0.1238195
		 0.42804933 -0.020779043 0.42726505 -0.020313889 0.37683296 -0.026412636 0.37683272
		 -0.027844101 0.37683272 -0.027844101 0.37683296 -0.026412517 0.37564227 -0.026412517
		 0.37564227 -0.027844101 0.37683272 -0.029132277 0.37683272 -0.029132277 0.37686205
		 -0.026373655 0.37683272 -0.026373655 0.37564227 -0.026373655 0.37564227 -0.029132277
		 0.37683272 -0.030068308 0.37564227 -0.030068308 0.39838082 0.014021724 0.40965286
		 0.021001726 0.4554216 -0.05290845 0.45763776 -0.056450158 0.42870378 -0.0085432827
		 0.4323535 -0.0067688525 0.44490847 -0.059884161 0.44712469 -0.063425809 0.42571354
		 -0.013286024 0.46070281 -0.061326414 0.45018974 -0.068302244 0.42653739 -0.017930061
		 0.46320978 -0.065289825 0.45269677 -0.072265953 0.4639602 -0.066687912 0.4535723
		 -0.073632509 0.4294275 -0.021861762 0.43105996 -0.021229059 0.4336181 -0.01699844
		 0.43505064 -0.019291788 0.43146551 -0.021136016 0.43320158 -0.016362458 0.42230415
		 -0.023953706 0.42377102 -0.026267439 0.43315923 -0.020027727 0.43128446 -0.013318151
		 0.42192459 -0.023339659 0.43198693 -0.021338433 0.42777887 -0.0077261627 0.43652511
		 -0.019232303 0.42000484 -0.020293742 0.41649914 -0.014701694 0.42178729 0.0018063486
		 0.44248497 -0.020427316 0.41050762 -0.0051693618 0.47902313 -0.09024033 0.47640136
		 -0.086074382 0.4763166 -0.086159855 0.47894123 -0.090364665 0.40966371 -0.013966113
		 0.46604347 -0.093050569 0.46875358 -0.097235709 0.48207 -0.094843775 0.48196402 -0.0949395
		 0.46531868 -0.091903001 0.47181106 -0.10189995 0.4759734 0.0086413324 0.47599462
		 0.0086326301 0.47634658 0.0092151463 0.47632006 0.0092239082 0.47562245 0.0085822046
		 0.47563508 0.0085738599 0.47662422 0.0096735656 0.47659376 0.0097239912 0.47496453
		 0.0099125803 0.47461551 0.0093253553 0.47426414 0.0092655122 0.43143257 -0.032432586
		 0.43018857 -0.032666832 0.4766393 0.0097743571 0.4766722 0.0097631514 0.47525826
		 0.010423928 0.43550143 -0.026482254 0.43496236 -0.025772482 0.47530106 0.010475606
		 0.37683296 -0.026001483 0.37683272 -0.026001483 0.37683272 -0.024573475 0.37683296
		 -0.024573475 0.37683296 -0.026044041 0.37686205 -0.026044041 0.37683272 -0.023284942
		 0.37683272 -0.023284942 0.37564227 -0.024573475 0.37564227 -0.026001483 0.37564227
		 -0.026044041 0.37683272 -0.022496969 0.37564227 -0.023284942 0.37564227 -0.022496849
		 0.46943021 -0.0047142804 0.46845174 -0.0042033494 0.47326133 0.0030419528 0.44535664
		 -0.013500363 0.44596037 -0.017860085 0.47341165 0.0034132302 0.44348475 -0.02341786
		 0.47363243 0.003924638 0.47202194 0.0039153993 0.4718715 0.0035441816 0.43882975
		 -0.02551201 0.47382721 0.0043404996 0.47224259 0.0044268072 0.43746415 -0.025932938
		 0.4724375 0.004842788 0.47250044 0.0049658716 0.45594704 -0.071440011 0.47132194
		 -0.00048342347 0.47121704 -0.00070527196 0.45718515 -0.07014969 0.47034276 1.8805265e-05
		 0.47023547 -0.00024262071 0.47118235 -0.00079229474 0.45743334 -0.069827586 0.4702034
		 -0.00028696656 0.47104025 -0.0011099875 0.45927644 -0.068708688 0.47006118 -0.00060787797
		 0.47079635 -0.0016962588 0.46165979 -0.06588915 0.45746374 -0.069251209 0.46981728
		 -0.0011941493 0.47036505 -0.0026959479 0.46328151 -0.058942348 0.46938598 -0.0021938384
		 0.47493479 0.0069181621 0.47489712 0.0069663227 0.47477522 0.0065220296 0.47478107
		 0.006498605 0.47525051 0.0074002445 0.47522405 0.0074109733 0.47358841 0.0075387061
		 0.47340301 0.0070355833 0.47468504 0.0063972175 0.4738614 0.0080612004 0.4733696
		 0.0069080889 0.37683296 -0.026208788 0.37683296 -0.026298553 0.37686205 -0.026298553
		 0.37686205 -0.026208788 0.37564227 -0.026208788 0.37564227 -0.026298553 0.4822433
		 -0.022345096 0.37683296 -0.026115805 0.37564227 -0.026115805 0.48046499 -0.020736843
		 0.48076898 -0.021349818 0.48248571 -0.022936136 0.48186439 -0.022917658 0.48231858
		 -0.023407966 0.49665558 -0.11715719 0.47670773 0.0098698437 0.47673419 0.0098592937
		 0.49678648 -0.11707047 0.48655915 -0.12412611 0.48694414 -0.12469718 0.47666338 0.0098063648
		 0.47669592 0.009821862 0.47532487 0.010507673 0.43821061 -0.028099388 0.43291676
		 -0.02490595 0.43833974 -0.024528414 0.44229975 -0.030828685 0.43845308 -0.027849823
		 0.43297577 -0.024479538 0.42955279 -0.022801906 0.43575588 -0.020413786 0.42706019
		 -0.031504005 0.4310202 -0.037804157 0.44482183 -0.031996578 0.44768062 -0.039389342
		 0.44505012 -0.03189078 0.42933667 -0.022236556 0.43535259 -0.019768685 0.42447627
		 -0.027389437 0.43640107 -0.046364933 0.44380116 -0.038922936 0.45552215 -0.051834434
		 0.44435954 -0.038464218 0.42975652 -0.022394031 0.42407298 -0.026744276 0.4442426
		 -0.058810085 0.45497844 -0.052776307 0.43275666 -0.006329447 0.44412792 -0.039478689
		 0.44465667 -0.059470743 0.44408309 -0.040371448 0.44439435 -0.039788991 0.44741476
		 -0.044044644 0.43856481 -0.010174543 0.45072651 -0.04748109 0.45045066 -0.047797233
		 0.45094573 -0.047672421 0.45073426 -0.047834426 0.4514761 -0.047843009 0.45172918
		 -0.047834307 0.45240176 -0.05265215 0.45281434 -0.05238238 0.47300902 0.0030205548
		 0.47281113 0.0029295385 0.472229 0.0016241968 0.45356798 -0.060375959 0.45391822
		 -0.060256511 0.4718591 0.0035025179 0.47183204 0.0034317672 0.47124994 0.0021263063
		 0.47184169 0.00072643161 0.45450771 -0.066860348 0.45494401 -0.06699422 0.47086251
		 0.0012285411 0.47155654 6.5654516e-05 0.47057748 0.00056788325 0.47137272 -0.0003657639
		 0.4550736 -0.070917279 0.47039354 0.00013646483 0.47134566 -0.00043347478 0.4555155
		 -0.071421057 0.4703666 6.8753958e-05 0.45552731 -0.070913225 0.48508936 -0.099408418
		 0.48498231 -0.099503547 0.47484916 -0.1064761 0.4886837 -0.10477772 0.4885273 -0.10484317
		 0.47840399 -0.1118181 0.48900145 -0.10582742 0.46884897 -0.074565738 0.47183231 -0.079200894;
	setAttr ".uvtk[250:483]" 0.4165796 -0.018757373 0.41714013 -0.018447906 0.45856512
		 -0.081441671 0.46154982 -0.086080104 0.41114226 -0.014053494 0.46457732 -0.090740949
		 0.41518262 0.012314171 0.44769359 -0.025005192 0.40390295 0.0053385794 0.41034618
		 0.020058602 0.4508462 -0.029521316 0.39900565 0.013117582 0.43312132 -0.007201463
		 0.4383463 -0.010728627 0.44452849 -0.013075143 0.42879033 -0.020481616 0.43187895
		 -0.022702724 0.43202993 -0.0224953 0.42835343 -0.019875318 0.43516651 -0.024885088
		 0.43596736 -0.02505067 0.43225643 -0.022168428 0.42797279 -0.019147426 0.42707264
		 -0.019156843 0.42790389 -0.019870669 0.43665478 -0.026597291 0.43611732 -0.025578052
		 0.43686852 -0.025047809 0.47554782 0.0078882277 0.47552136 0.0078980029 0.47589973
		 0.0085204542 0.47588614 0.0084728897 0.47415966 0.0085709989 0.47452787 0.0091556013
		 0.47421798 0.0052705109 0.43751207 -0.036351711 0.44026646 -0.037888199 0.44052592
		 -0.037729532 0.44434091 -0.043550402 0.47306639 0.0062752664 0.47285092 0.005770117
		 0.47331887 0.0067841709 0.46988964 -0.0037979186 0.45931804 -0.050691277 0.46955287
		 -0.0045770705 0.46135128 -0.04786399 0.46891057 -0.003295809 0.46856833 -0.0041442215
		 0.48193222 -0.021699578 0.48144466 -0.021185428 0.48129916 -0.02214554 0.46852231
		 -0.02336362 0.47217631 -0.028452545 0.46830118 -0.02270332 0.45865762 -0.027381867
		 0.4632268 -0.034490854 0.47558689 -0.033745319 0.45840979 -0.026716799 0.451841 -0.031116754
		 0.45670879 -0.038637787 0.46789789 -0.041681319 0.47612894 -0.034187466 0.45169103
		 -0.030352265 0.44809628 -0.036139518 0.44993424 -0.042556077 0.4611305 -0.046970397
		 0.46843064 -0.042171866 0.44784749 -0.035419017 0.45437646 -0.046743542 0.46177113
		 -0.047644287 0.4508636 -0.030356437 0.44773972 -0.034690171 0.45472729 -0.047200352
		 0.44732571 -0.025385886 0.4455651 -0.03009823 0.45511031 -0.047677547 0.46101177
		 -0.047984272 0.44219196 -0.020906657 0.43977809 -0.026300758 0.45524001 -0.052366287
		 0.45911777 -0.051180154 0.46197093 -0.049204379 0.43647754 -0.019888669 0.43488634
		 -0.023139447 0.45603454 -0.059560806 0.46263707 -0.059055001 0.43145907 -0.021707982
		 0.45704186 -0.065771133 0.46099102 -0.065636426 0.45688677 -0.069582611 0.41785491
		 -0.031349927 0.41869926 -0.031669885 0.41693914 -0.036321551 0.41751075 -0.036684066
		 0.42134142 -0.034425408 0.42654553 -0.029026061 0.41705334 -0.037161738 0.4172169
		 -0.037547261 0.41826773 -0.037439615 0.42421821 -0.036998063 0.43147328 -0.023247153
		 0.41756356 -0.038113624 0.41901019 -0.038214117 0.42985955 -0.033880025 0.42454281
		 -0.037850171 0.41812277 -0.038477927 0.41935989 -0.038789302 0.41850647 -0.038654
		 0.42699981 -0.019503266 0.42622542 -0.020162493 0.43568149 -0.027573258 0.43631926
		 -0.026699692 0.41751111 -0.01947549 0.42139721 -0.024419576 0.43185166 -0.033182055
		 0.43681303 -0.036211401 0.41717756 -0.018277675 0.41545844 -0.018934399 0.41564369
		 -0.024384767 0.42060018 -0.025222212 0.43123123 -0.033989698 0.43083504 -0.03340593
		 0.4330568 -0.038475424 0.43965545 -0.038895994 0.43759301 -0.036298424 0.41151884
		 -0.01551047 0.41267002 -0.022797853 0.41526055 -0.025172025 0.43234113 -0.038872033
		 0.43527317 -0.041066557 0.44293669 -0.043135434 0.41047272 -0.014990479 0.41203892
		 -0.019182593 0.41194963 -0.023366123 0.43471217 -0.041735679 0.43913063 -0.042378217
		 0.44347706 -0.044214994 0.41144514 -0.019258171 0.43903527 -0.042985111 0.42940843
		 -0.0092670023 0.42668831 -0.013702065 0.43657795 -0.014508277 0.43401417 -0.019323498
		 0.44493029 -0.017672807 0.44258323 -0.022823125 0.48964351 -0.10629955 0.37686205
		 -0.026115805 0.48097312 -0.020737439 0.47469637 0.0064007938 0.47464398 0.006275326
		 0.47483662 -0.08388117 0.47558132 -0.085039645 0.44444451 -0.043508083 0.47443447
		 0.0057738125 0.41117743 -0.013967901 0.41683877 -0.018510967 0.41709101 -0.018457562
		 0.49704111 -0.11772671 0.47536781 0.01057151 0.49714738 -0.11763111 0.44449779 -0.044929296
		 0.44460621 -0.044924885 0.47566572 -0.084992558 0.40966311 -0.013877779 0.42767537
		 -0.019590765 0.43645808 -0.025713831 0.45729411 -0.070218712 0.45753527 -0.069919974
		 0.45938659 -0.06878987 0.45606208 -0.071512252 0.45542467 -0.071495205 0.45496202
		 -0.070953637 0.46178377 -0.065931469 0.46341407 -0.058936507 0.45438707 -0.066895515
		 0.453444 -0.060403973 0.45942509 -0.050677329 0.45223165 -0.052685171 0.44364443
		 -0.02350679 0.44614586 -0.017881066 0.46122575 -0.049454719 0.4521569 -0.048010737
		 0.44553843 -0.013550907 0.44603035 -0.012965351 0.43899891 -0.025607497 0.47388873
		 0.0044455826 0.43103898 -0.021107405 0.43142366 -0.021010011 0.43313229 -0.019896477
		 0.42939556 -0.021744698 0.42921364 -0.022227198 0.42946827 -0.022886902 0.43653619
		 -0.019114345 0.44253123 -0.020323783 0.43283451 -0.024998397 0.43813527 -0.028193921
		 0.4477607 -0.024914712 0.44478512 -0.032071382 0.42555141 -0.013206035 0.42859232
		 -0.0084052384 0.45080113 -0.029426008 0.44296646 -0.038636833 0.432217 -0.0066511929
		 0.44113588 -0.037958771 0.42636573 -0.017850071 0.45251238 -0.048847109 0.43245804
		 -0.0059547722 0.45114875 -0.029382735 0.45116723 -0.029216379 0.40951684 0.021519154
		 0.39801157 0.01464954 0.45072711 -0.029282242 0.46142948 -0.047824293 0.44377184
		 -0.039790541 0.43389297 -0.0068363249 0.43328738 -0.0062334836 0.43860182 -0.010199934
		 0.44712019 -0.044329673 0.44698048 -0.044229895 0.45003724 -0.047578841 0.44526681
		 -0.01236096 0.44447228 -0.012149602 0.45072615 -0.047853857 0.45237792 -0.048795015
		 0.44577339 -0.012904912 0.44550088 -0.012880713 0.4326328 -0.0061400235 0.44097173
		 -0.038156062 0.44389856 -0.039528757 0.44391203 -0.039523154 0.46169233 -0.047686726
		 0.37658378 -0.022150069 0.37564227 -0.022150069 0.45557773 -0.071577102;
createNode polyTweakUV -n "polyTweakUV67";
	rename -uid "22933B94-4903-8471-56F2-34BA6B4D45DB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk[0:7]" -type "float2" 0.21053112 -0.25 0.18050116
		 -0.24947733 0.2093513 -0.0034855902 0.17932141 -0.0029628873 -0.37382022 -0.25463104
		 -0.40385017 -0.25410834 -0.375 -0.0081164837 -0.40502995 -0.0075938702;
createNode polyAutoProj -n "polyAutoProj11";
	rename -uid "BF753AB0-4161-F4CE-362F-32B06D217E59";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:571]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.0672064754216544 0 0 0 0 0.98692533935277804 0
		 0 -1.65922442784799 -0.28623479486675407 1;
	setAttr ".s" -type "double3" 61.735813140869141 61.735813140869141 61.735813140869141 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak71";
	rename -uid "AB7EAB90-4C3E-51E4-778C-6BB586A20CF5";
	setAttr ".uopa" yes;
	setAttr -s 600 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[1]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[2]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[5]" -type "float3" 0 0.40308022 0 ;
	setAttr ".tk[6]" -type "float3" 0 0.56633782 0 ;
	setAttr ".tk[9]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[11]" -type "float3" 0 0.40308022 0 ;
	setAttr ".tk[12]" -type "float3" 0 0.40308022 0 ;
	setAttr ".tk[13]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[17]" -type "float3" 0 0.40308022 0 ;
	setAttr ".tk[18]" -type "float3" 0 0.40308022 0 ;
	setAttr ".tk[19]" -type "float3" -4.7683716e-07 0.23883381 0 ;
	setAttr ".tk[22]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[23]" -type "float3" 0 0.23883384 0 ;
	setAttr ".tk[24]" -type "float3" 0 0.40308022 0 ;
	setAttr ".tk[25]" -type "float3" 0 0.40308022 0 ;
	setAttr ".tk[26]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[29]" -type "float3" 0 0 -0.18826847 ;
	setAttr ".tk[30]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[31]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[38]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[39]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[40]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[43]" -type "float3" -4.7683716e-07 0.23883381 0 ;
	setAttr ".tk[44]" -type "float3" 0 0.99074239 0 ;
	setAttr ".tk[45]" -type "float3" 0 0.82208878 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.48152536 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.48152548 0 ;
	setAttr ".tk[48]" -type "float3" 0 0.82208878 0 ;
	setAttr ".tk[49]" -type "float3" 0 0.82208878 0 ;
	setAttr ".tk[50]" -type "float3" 0 0.48152536 0 ;
	setAttr ".tk[51]" -type "float3" 0 0.48152536 0 ;
	setAttr ".tk[52]" -type "float3" 0 0.82208878 0 ;
	setAttr ".tk[53]" -type "float3" 0 0.82208878 0 ;
	setAttr ".tk[54]" -type "float3" 0 0.48152536 0 ;
	setAttr ".tk[55]" -type "float3" 0 0.48152536 0 ;
	setAttr ".tk[56]" -type "float3" 0 0.82208878 0 ;
	setAttr ".tk[57]" -type "float3" 0 0.82208902 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.48152536 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.48152536 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[61]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[62]" -type "float3" 0 0.23883381 -0.18826847 ;
	setAttr ".tk[63]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[64]" -type "float3" 0 0.77611446 0 ;
	setAttr ".tk[65]" -type "float3" 0 0.65787393 0 ;
	setAttr ".tk[66]" -type "float3" 0 0.65787393 0 ;
	setAttr ".tk[67]" -type "float3" 0 0.65787393 0 ;
	setAttr ".tk[68]" -type "float3" 0 0.65787393 0 ;
	setAttr ".tk[69]" -type "float3" 0 0.65787393 0 ;
	setAttr ".tk[70]" -type "float3" 0 0.65787393 0 ;
	setAttr ".tk[71]" -type "float3" 0 0.77611446 0 ;
	setAttr ".tk[72]" -type "float3" 0 0.93097508 0 ;
	setAttr ".tk[73]" -type "float3" 0 0.93097508 0 ;
	setAttr ".tk[74]" -type "float3" 0 0.93097508 0 ;
	setAttr ".tk[75]" -type "float3" 0 0.93097508 0 ;
	setAttr ".tk[76]" -type "float3" 0 0.93097508 0 ;
	setAttr ".tk[77]" -type "float3" 0 0.93097508 0 ;
	setAttr ".tk[78]" -type "float3" 0 0.93097508 0 ;
	setAttr ".tk[79]" -type "float3" 0 0.76241821 0 ;
	setAttr ".tk[80]" -type "float3" 0 0.41563812 0 ;
	setAttr ".tk[81]" -type "float3" 0 0.58429122 0 ;
	setAttr ".tk[82]" -type "float3" 0 0.58429122 0 ;
	setAttr ".tk[83]" -type "float3" 0 0.58429122 0 ;
	setAttr ".tk[84]" -type "float3" 0 0.58429122 0 ;
	setAttr ".tk[85]" -type "float3" 0 0.58429122 0 ;
	setAttr ".tk[86]" -type "float3" 0 0.58429122 0 ;
	setAttr ".tk[87]" -type "float3" 0 0.58429122 0 ;
	setAttr ".tk[88]" -type "float3" 0 0.23883381 0 ;
	setAttr ".tk[95]" -type "float3" 0 0.075576335 0 ;
	setAttr ".tk[96]" -type "float3" 0.024914289 0.82208878 0 ;
	setAttr ".tk[97]" -type "float3" -0.024915965 0.82208878 0 ;
	setAttr ".tk[98]" -type "float3" -0.024915965 0.58429122 0 ;
	setAttr ".tk[99]" -type "float3" 0.024914289 0.58429122 0 ;
	setAttr ".tk[100]" -type "float3" 2.1387949 0.82208878 0 ;
	setAttr ".tk[101]" -type "float3" 2.1886241 0.82208878 0 ;
	setAttr ".tk[102]" -type "float3" 2.1886241 0.93097508 0 ;
	setAttr ".tk[103]" -type "float3" 2.1387949 0.93097508 0 ;
	setAttr ".tk[104]" -type "float3" -2.1387963 0.82208878 0 ;
	setAttr ".tk[105]" -type "float3" -2.1886265 0.82208878 0 ;
	setAttr ".tk[106]" -type "float3" -2.1886241 0.58429122 0 ;
	setAttr ".tk[107]" -type "float3" -2.1387951 0.58429122 0 ;
	setAttr ".tk[108]" -type "float3" -0.024914999 0.48152536 0 ;
	setAttr ".tk[109]" -type "float3" 0.024914639 0.48152536 0 ;
	setAttr ".tk[110]" -type "float3" 0.024914645 0.93097508 0 ;
	setAttr ".tk[111]" -type "float3" -0.024914987 0.93097508 0 ;
	setAttr ".tk[112]" -type "float3" -2.1886196 0.48152536 0 ;
	setAttr ".tk[113]" -type "float3" -2.138797 0.48152536 0 ;
	setAttr ".tk[114]" -type "float3" -2.1387975 0.93097508 0 ;
	setAttr ".tk[115]" -type "float3" -2.1886241 0.93097508 0 ;
	setAttr ".tk[116]" -type "float3" 2.1886237 0.40308022 0 ;
	setAttr ".tk[117]" -type "float3" 2.1387944 0.40308022 0 ;
	setAttr ".tk[118]" -type "float3" 2.1387949 0.58429122 0 ;
	setAttr ".tk[119]" -type "float3" 2.1886249 0.58429122 0 ;
	setAttr ".tk[120]" -type "float3" -2.1886265 0 0 ;
	setAttr ".tk[121]" -type "float3" -2.1387963 0 0 ;
	setAttr ".tk[122]" -type "float3" -2.1387963 0.23883381 0 ;
	setAttr ".tk[123]" -type "float3" -2.1886265 0.23883381 0 ;
	setAttr ".tk[124]" -type "float3" 2.1886244 -7.4505806e-09 0 ;
	setAttr ".tk[125]" -type "float3" 2.1387949 -7.4505806e-09 0 ;
	setAttr ".tk[126]" -type "float3" 2.1387949 0.23883381 0 ;
	setAttr ".tk[127]" -type "float3" 2.1886244 0.23883381 0 ;
	setAttr ".tk[128]" -type "float3" -2.1886265 -7.4505806e-09 0 ;
	setAttr ".tk[129]" -type "float3" -2.1387963 -7.4505806e-09 0 ;
	setAttr ".tk[130]" -type "float3" -2.1387951 0.23883381 0 ;
	setAttr ".tk[131]" -type "float3" -2.1886244 0.23883381 0 ;
	setAttr ".tk[132]" -type "float3" -0.024915965 0 0 ;
	setAttr ".tk[133]" -type "float3" 0.024914289 0 0 ;
	setAttr ".tk[134]" -type "float3" 0.024914289 0.23883381 0 ;
	setAttr ".tk[135]" -type "float3" -0.024915965 0.23883381 0 ;
	setAttr ".tk[136]" -type "float3" -2.1387963 0 0 ;
	setAttr ".tk[137]" -type "float3" -2.1886265 0 0 ;
	setAttr ".tk[138]" -type "float3" -2.1886249 0.23883381 0 ;
	setAttr ".tk[139]" -type "float3" -2.1387951 0.23883381 0 ;
	setAttr ".tk[140]" -type "float3" 2.1886249 0 0 ;
	setAttr ".tk[141]" -type "float3" 2.1387949 0 0 ;
	setAttr ".tk[142]" -type "float3" 2.138793 0.23883381 0 ;
	setAttr ".tk[143]" -type "float3" 2.1886237 0.23883381 0 ;
	setAttr ".tk[144]" -type "float3" 2.1387949 -7.4505806e-09 0 ;
	setAttr ".tk[145]" -type "float3" 2.1886244 -7.4505806e-09 0 ;
	setAttr ".tk[146]" -type "float3" 2.1886222 0.23883381 0 ;
	setAttr ".tk[147]" -type "float3" 2.1387942 0.23883381 0 ;
	setAttr ".tk[148]" -type "float3" 2.1387949 0 0 ;
	setAttr ".tk[149]" -type "float3" 2.1886244 0 0 ;
	setAttr ".tk[150]" -type "float3" 2.1886244 0.23883381 0 ;
	setAttr ".tk[151]" -type "float3" 2.1387949 0.23883381 0 ;
	setAttr ".tk[152]" -type "float3" -0.024915965 0.23883381 0 ;
	setAttr ".tk[153]" -type "float3" 0.024914289 0.23883381 0 ;
	setAttr ".tk[154]" -type "float3" 0.024914289 -7.4505806e-09 0 ;
	setAttr ".tk[155]" -type "float3" -0.024915965 -7.4505806e-09 0 ;
	setAttr ".tk[156]" -type "float3" -2.1387963 -7.4505806e-09 0 ;
	setAttr ".tk[157]" -type "float3" -2.1886265 -7.4505806e-09 0 ;
	setAttr ".tk[158]" -type "float3" -2.1886265 0.23883381 0 ;
	setAttr ".tk[159]" -type "float3" -2.1387963 0.23883381 0 ;
	setAttr ".tk[160]" -type "float3" -0.024915965 -7.4505806e-09 0 ;
	setAttr ".tk[161]" -type "float3" 0.024914289 -7.4505806e-09 0 ;
	setAttr ".tk[162]" -type "float3" 0.024914289 0.23883381 0 ;
	setAttr ".tk[163]" -type "float3" -0.024915965 0.23883381 0 ;
	setAttr ".tk[164]" -type "float3" 0.024914289 0 0 ;
	setAttr ".tk[165]" -type "float3" -0.024915965 0 0 ;
	setAttr ".tk[166]" -type "float3" -0.024915965 0.23883381 0 ;
	setAttr ".tk[167]" -type "float3" 0.024914289 0.23883381 0 ;
	setAttr ".tk[168]" -type "float3" -2.1387949 0.40308022 0 ;
	setAttr ".tk[169]" -type "float3" -2.1886206 0.40308022 0 ;
	setAttr ".tk[170]" -type "float3" -2.1387951 0 0 ;
	setAttr ".tk[171]" -type "float3" -2.1886249 0 0 ;
	setAttr ".tk[172]" -type "float3" 2.1886241 0 0 ;
	setAttr ".tk[173]" -type "float3" 2.1387949 0 0 ;
	setAttr ".tk[174]" -type "float3" -2.1886251 0.65787393 0 ;
	setAttr ".tk[175]" -type "float3" -2.1387951 0.65787393 0 ;
	setAttr ".tk[176]" -type "float3" 2.138793 0.48152536 0 ;
	setAttr ".tk[177]" -type "float3" 2.1886196 0.48152536 0 ;
	setAttr ".tk[178]" -type "float3" 2.1387937 0.65787393 0 ;
	setAttr ".tk[179]" -type "float3" 2.1886225 0.65787393 0 ;
	setAttr ".tk[180]" -type "float3" 0.024914289 0 0 ;
	setAttr ".tk[181]" -type "float3" -0.024915965 0 0 ;
	setAttr ".tk[182]" -type "float3" -0.024915965 0.65787393 0 ;
	setAttr ".tk[183]" -type "float3" 0.024914289 0.65787393 0 ;
	setAttr ".tk[184]" -type "float3" 0.024914289 0.40308022 0 ;
	setAttr ".tk[185]" -type "float3" -0.024915965 0.40308022 0 ;
	setAttr ".tk[186]" -type "float3" -1.0569407 0.23883381 0 ;
	setAttr ".tk[187]" -type "float3" -1.1067712 0.23883381 0 ;
	setAttr ".tk[188]" -type "float3" -1.1067712 -7.4505806e-09 0 ;
	setAttr ".tk[189]" -type "float3" -1.0569407 -7.4505806e-09 0 ;
	setAttr ".tk[190]" -type "float3" -1.1067712 0 0 ;
	setAttr ".tk[191]" -type "float3" -1.0569407 0 0 ;
	setAttr ".tk[192]" -type "float3" -1.1067712 0.23883381 0 ;
	setAttr ".tk[193]" -type "float3" -1.0569407 0.23883381 0 ;
	setAttr ".tk[194]" -type "float3" -1.1067712 0.65787393 0 ;
	setAttr ".tk[195]" -type "float3" -1.0569407 0.65787393 0 ;
	setAttr ".tk[196]" -type "float3" -1.10677 0.48152536 0 ;
	setAttr ".tk[197]" -type "float3" -1.0569423 0.48152536 0 ;
	setAttr ".tk[198]" -type "float3" -1.10677 0.93097508 0 ;
	setAttr ".tk[199]" -type "float3" -1.0569423 0.93097508 0 ;
	setAttr ".tk[200]" -type "float3" -1.1067712 0.82208878 0 ;
	setAttr ".tk[201]" -type "float3" -1.0569407 0.82208878 0 ;
	setAttr ".tk[202]" -type "float3" -1.1067712 0.58429122 0 ;
	setAttr ".tk[203]" -type "float3" -1.0569407 0.58429122 0 ;
	setAttr ".tk[204]" -type "float3" -1.1067712 0.40308022 0 ;
	setAttr ".tk[205]" -type "float3" -1.0569407 0.40308022 0 ;
	setAttr ".tk[206]" -type "float3" -1.1067712 0 0 ;
	setAttr ".tk[207]" -type "float3" -1.0569407 0 0 ;
	setAttr ".tk[208]" -type "float3" -1.1067712 0.23883381 0 ;
	setAttr ".tk[209]" -type "float3" -1.0569407 0.23883381 0 ;
	setAttr ".tk[210]" -type "float3" -1.1067712 0 0 ;
	setAttr ".tk[211]" -type "float3" -1.0569407 0 0 ;
	setAttr ".tk[212]" -type "float3" -1.1067712 -7.4505806e-09 0 ;
	setAttr ".tk[213]" -type "float3" -1.0569407 -7.4505806e-09 0 ;
	setAttr ".tk[214]" -type "float3" -1.1067712 0.23883381 0 ;
	setAttr ".tk[215]" -type "float3" -1.0569407 0.23883381 0 ;
	setAttr ".tk[216]" -type "float3" 1.1067696 0.23883381 0 ;
	setAttr ".tk[217]" -type "float3" 1.0569391 0.23883381 0 ;
	setAttr ".tk[218]" -type "float3" 1.0569391 -7.4505806e-09 0 ;
	setAttr ".tk[219]" -type "float3" 1.1067696 -7.4505806e-09 0 ;
	setAttr ".tk[220]" -type "float3" 1.0569391 0 0 ;
	setAttr ".tk[221]" -type "float3" 1.1067696 0 0 ;
	setAttr ".tk[222]" -type "float3" 1.0569391 0.23883381 0 ;
	setAttr ".tk[223]" -type "float3" 1.1067696 0.23883381 0 ;
	setAttr ".tk[224]" -type "float3" 1.0569391 0.65787393 0 ;
	setAttr ".tk[225]" -type "float3" 1.1067696 0.65787393 0 ;
	setAttr ".tk[226]" -type "float3" 1.0569391 0.48152536 0 ;
	setAttr ".tk[227]" -type "float3" 1.1067696 0.48152536 0 ;
	setAttr ".tk[228]" -type "float3" 1.0569391 0.93097508 0 ;
	setAttr ".tk[229]" -type "float3" 1.1067696 0.93097508 0 ;
	setAttr ".tk[230]" -type "float3" 1.0569391 0.82208878 0 ;
	setAttr ".tk[231]" -type "float3" 1.1067696 0.82208878 0 ;
	setAttr ".tk[232]" -type "float3" 1.0569391 0.58429122 0 ;
	setAttr ".tk[233]" -type "float3" 1.1067696 0.58429122 0 ;
	setAttr ".tk[234]" -type "float3" 1.0569391 0.40308022 0 ;
	setAttr ".tk[235]" -type "float3" 1.1067696 0.40308022 0 ;
	setAttr ".tk[236]" -type "float3" 1.0569391 0 0 ;
	setAttr ".tk[237]" -type "float3" 1.1067696 0 0 ;
	setAttr ".tk[238]" -type "float3" 1.0569391 0.23883381 0 ;
	setAttr ".tk[239]" -type "float3" 1.1067696 0.23883381 0 ;
	setAttr ".tk[240]" -type "float3" 1.0569391 0 0 ;
	setAttr ".tk[241]" -type "float3" 1.1067696 0 0 ;
	setAttr ".tk[242]" -type "float3" 1.0569391 -7.4505806e-09 0 ;
	setAttr ".tk[243]" -type "float3" 1.1067696 -7.4505806e-09 0 ;
	setAttr ".tk[244]" -type "float3" 1.0569391 0.23883381 0 ;
	setAttr ".tk[245]" -type "float3" 1.1067696 0.23883381 0 ;
	setAttr ".tk[246]" -type "float3" 3.1853259 0.23883381 0 ;
	setAttr ".tk[247]" -type "float3" 3.1355035 0.23883381 0 ;
	setAttr ".tk[248]" -type "float3" 3.1361403 -7.4505806e-09 0 ;
	setAttr ".tk[249]" -type "float3" 3.1859713 -7.4505806e-09 0 ;
	setAttr ".tk[250]" -type "float3" 3.1387436 0 0 ;
	setAttr ".tk[251]" -type "float3" 3.1885777 0 0 ;
	setAttr ".tk[252]" -type "float3" 3.1361406 0.23883381 0 ;
	setAttr ".tk[253]" -type "float3" 3.185971 0.23883381 0 ;
	setAttr ".tk[254]" -type "float3" 3.1364064 0.65787393 0 ;
	setAttr ".tk[255]" -type "float3" 3.1862335 0.65787393 0 ;
	setAttr ".tk[256]" -type "float3" 3.1366765 0.48152536 0 ;
	setAttr ".tk[257]" -type "float3" 3.1864913 0.48152536 0 ;
	setAttr ".tk[258]" -type "float3" 3.1366696 0.93097508 0 ;
	setAttr ".tk[259]" -type "float3" 3.1864989 0.93097508 0 ;
	setAttr ".tk[260]" -type "float3" 3.1366699 0.82208878 0 ;
	setAttr ".tk[261]" -type "float3" 3.1864994 0.82208878 0 ;
	setAttr ".tk[262]" -type "float3" 3.1366689 0.58429122 0 ;
	setAttr ".tk[263]" -type "float3" 3.1864994 0.58429122 0 ;
	setAttr ".tk[264]" -type "float3" 3.1366696 0.40308022 0 ;
	setAttr ".tk[265]" -type "float3" 3.1864958 0.40308022 0 ;
	setAttr ".tk[266]" -type "float3" 3.1377065 0 0 ;
	setAttr ".tk[267]" -type "float3" 3.1875372 0 0 ;
	setAttr ".tk[268]" -type "float3" 3.1387453 0.23883381 0 ;
	setAttr ".tk[269]" -type "float3" 3.1885765 0.23883381 0 ;
	setAttr ".tk[270]" -type "float3" 3.1387451 0 0 ;
	setAttr ".tk[271]" -type "float3" 3.1885767 0 0 ;
	setAttr ".tk[272]" -type "float3" 3.1361406 -7.4505806e-09 0 ;
	setAttr ".tk[273]" -type "float3" 3.1859715 -7.4505806e-09 0 ;
	setAttr ".tk[274]" -type "float3" 3.1354988 0.23883381 0 ;
	setAttr ".tk[275]" -type "float3" 3.1853304 0.23883381 0 ;
	setAttr ".tk[276]" -type "float3" -3.1361449 0.23883381 0 ;
	setAttr ".tk[277]" -type "float3" -3.1859682 0.23883381 0 ;
	setAttr ".tk[278]" -type "float3" -3.1859725 -7.4505806e-09 0 ;
	setAttr ".tk[279]" -type "float3" -3.1361418 -7.4505806e-09 0 ;
	setAttr ".tk[280]" -type "float3" -3.1859725 0 0 ;
	setAttr ".tk[281]" -type "float3" -3.1361418 0 0 ;
	setAttr ".tk[282]" -type "float3" -3.1859725 0.23883381 0 ;
	setAttr ".tk[283]" -type "float3" -3.1361418 0.23883381 0 ;
	setAttr ".tk[284]" -type "float3" -3.1862345 0.65787393 0 ;
	setAttr ".tk[285]" -type "float3" -3.1364076 0.65787393 0 ;
	setAttr ".tk[286]" -type "float3" -3.186492 0.48152536 0 ;
	setAttr ".tk[287]" -type "float3" -3.1366775 0.48152536 0 ;
	setAttr ".tk[288]" -type "float3" -3.1864989 0.93097508 0 ;
	setAttr ".tk[289]" -type "float3" -3.1366727 0.93097508 0 ;
	setAttr ".tk[290]" -type "float3" -3.1865005 0.82208878 0 ;
	setAttr ".tk[291]" -type "float3" -3.1366718 0.82208878 0 ;
	setAttr ".tk[292]" -type "float3" -3.1864989 0.58429122 0 ;
	setAttr ".tk[293]" -type "float3" -3.1366727 0.58429122 0 ;
	setAttr ".tk[294]" -type "float3" -3.1864922 0.40308022 0 ;
	setAttr ".tk[295]" -type "float3" -3.136677 0.40308022 0 ;
	setAttr ".tk[296]" -type "float3" -3.1875374 0 0 ;
	setAttr ".tk[297]" -type "float3" -3.1377108 0 0 ;
	setAttr ".tk[298]" -type "float3" -3.1885769 0.23883381 0 ;
	setAttr ".tk[299]" -type "float3" -3.138747 0.23883381 0 ;
	setAttr ".tk[300]" -type "float3" -3.1885772 0 0 ;
	setAttr ".tk[301]" -type "float3" -3.1387467 0 0 ;
	setAttr ".tk[302]" -type "float3" -3.1859722 -7.4505806e-09 0 ;
	setAttr ".tk[303]" -type "float3" -3.1361415 -7.4505806e-09 0 ;
	setAttr ".tk[304]" -type "float3" -3.1853309 0.23883381 0 ;
	setAttr ".tk[305]" -type "float3" -3.1354995 0.23883381 0 ;
	setAttr ".tk[306]" -type "float3" 0.0024598348 0.83573294 2.3283064e-09 ;
	setAttr ".tk[307]" -type "float3" -9.0412391e-07 0.83573294 2.3283064e-09 ;
	setAttr ".tk[308]" -type "float3" -0.0024612697 0.83573294 0.0080175251 ;
	setAttr ".tk[309]" -type "float3" -0.0024612697 0.59793538 0.079817049 ;
	setAttr ".tk[310]" -type "float3" -9.0412391e-07 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[311]" -type "float3" 0.0024598348 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[312]" -type "float3" 2.1612449 0.83573294 2.3283064e-09 ;
	setAttr ".tk[313]" -type "float3" 2.1637096 0.83573294 2.3283064e-09 ;
	setAttr ".tk[314]" -type "float3" 2.1661654 0.83573294 2.3283064e-09 ;
	setAttr ".tk[315]" -type "float3" 2.1661756 0.94461918 -7.4505806e-09 ;
	setAttr ".tk[316]" -type "float3" 2.1637096 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[317]" -type "float3" 2.1612544 0.94461918 3.7252903e-09 ;
	setAttr ".tk[318]" -type "float3" -2.1612527 0.83573294 2.3283064e-09 ;
	setAttr ".tk[319]" -type "float3" -2.1637113 0.83573294 2.3283064e-09 ;
	setAttr ".tk[320]" -type "float3" -2.1661739 0.83573294 2.3283064e-09 ;
	setAttr ".tk[321]" -type "float3" -2.1661808 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[322]" -type "float3" -2.1637113 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[323]" -type "float3" -2.1612594 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[324]" -type "float3" -2.166172 0.013644145 0.05339662 ;
	setAttr ".tk[325]" -type "float3" -2.1637113 0.013644152 0.05339662 ;
	setAttr ".tk[326]" -type "float3" -2.1612511 0.013644152 0.05339662 ;
	setAttr ".tk[327]" -type "float3" -2.1612525 0.25247794 2.9802322e-08 ;
	setAttr ".tk[328]" -type "float3" -2.1637113 0.25247797 2.9802322e-08 ;
	setAttr ".tk[329]" -type "float3" -2.1661737 0.25247797 -4.4703484e-08 ;
	setAttr ".tk[330]" -type "float3" 2.1661704 0.23945785 0.05339662 ;
	setAttr ".tk[331]" -type "float3" 2.1637096 0.23945785 0.05339662 ;
	setAttr ".tk[332]" -type "float3" 2.1612494 0.23945785 0.05339662 ;
	setAttr ".tk[333]" -type "float3" 2.1612494 0.47829166 0.05339662 ;
	setAttr ".tk[334]" -type "float3" 2.1637096 0.47829166 0.05339662 ;
	setAttr ".tk[335]" -type "float3" 2.1661704 0.47829166 0.05339662 ;
	setAttr ".tk[336]" -type "float3" -2.166172 0.23945785 -0.051327288 ;
	setAttr ".tk[337]" -type "float3" -2.1637113 0.23945785 -0.051327176 ;
	setAttr ".tk[338]" -type "float3" -2.1612515 0.23945785 -0.051327176 ;
	setAttr ".tk[339]" -type "float3" -2.1612563 0.47829166 -0.049491867 ;
	setAttr ".tk[340]" -type "float3" -2.1637113 0.47829166 -0.049491867 ;
	setAttr ".tk[341]" -type "float3" -2.1661777 0.47829166 -0.049493097 ;
	setAttr ".tk[342]" -type "float3" -0.0024612697 0.013644137 0.05339662 ;
	setAttr ".tk[343]" -type "float3" -9.0412391e-07 0.013644152 0.05339662 ;
	setAttr ".tk[344]" -type "float3" 0.0024598348 0.013644137 0.05339662 ;
	setAttr ".tk[345]" -type "float3" 0.0024598348 0.25247797 2.9802322e-08 ;
	setAttr ".tk[346]" -type "float3" -9.0412391e-07 0.25247797 2.9802322e-08 ;
	setAttr ".tk[347]" -type "float3" -0.0024612697 0.25247797 0.27041718 ;
	setAttr ".tk[348]" -type "float3" -2.1612515 0.013644152 -0.05332046 ;
	setAttr ".tk[349]" -type "float3" -2.1637113 0.013644152 -0.05332046 ;
	setAttr ".tk[350]" -type "float3" -2.166172 0.013644145 -0.053320661 ;
	setAttr ".tk[351]" -type "float3" -2.1661716 0.25247794 2.9802322e-08 ;
	setAttr ".tk[352]" -type "float3" -2.1637113 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[353]" -type "float3" -2.1612506 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[354]" -type "float3" 2.1661694 0.013644145 -0.053320661 ;
	setAttr ".tk[355]" -type "float3" 2.1637096 0.013644152 -0.05332046 ;
	setAttr ".tk[356]" -type "float3" 2.1612482 0.013644145 -0.05332046 ;
	setAttr ".tk[357]" -type "float3" 2.1612494 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[358]" -type "float3" 2.1637096 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[359]" -type "float3" 2.1661704 0.25247794 2.9802322e-08 ;
	setAttr ".tk[360]" -type "float3" 2.1612501 0.23945785 -0.051327176 ;
	setAttr ".tk[361]" -type "float3" 2.1637096 0.23945785 -0.051327176 ;
	setAttr ".tk[362]" -type "float3" 2.1661708 0.23945785 -0.051327288 ;
	setAttr ".tk[363]" -type "float3" 2.1661754 0.47829166 -0.049493097 ;
	setAttr ".tk[364]" -type "float3" 2.1637096 0.47829166 -0.049491867 ;
	setAttr ".tk[365]" -type "float3" 2.1612549 0.47829166 -0.049491867 ;
	setAttr ".tk[366]" -type "float3" 2.1612501 0.013644152 0.05339662 ;
	setAttr ".tk[367]" -type "float3" 2.1637096 0.013644152 0.05339662 ;
	setAttr ".tk[368]" -type "float3" 2.1661711 0.013644145 0.05339662 ;
	setAttr ".tk[369]" -type "float3" 2.1661718 0.25247797 -4.4703484e-08 ;
	setAttr ".tk[370]" -type "float3" 2.1637096 0.25247797 2.9802322e-08 ;
	setAttr ".tk[371]" -type "float3" 2.1612508 0.25247794 2.9802322e-08 ;
	setAttr ".tk[372]" -type "float3" -0.0024612697 0.47829166 0.05339662 ;
	setAttr ".tk[373]" -type "float3" -9.0412391e-07 0.47829166 0.05339662 ;
	setAttr ".tk[374]" -type "float3" 0.0024598348 0.47829166 0.05339662 ;
	setAttr ".tk[375]" -type "float3" 0.0024598348 0.23945785 0.05339662 ;
	setAttr ".tk[376]" -type "float3" -9.0412391e-07 0.23945785 0.05339662 ;
	setAttr ".tk[377]" -type "float3" -0.0024612697 0.23945785 0.05339662 ;
	setAttr ".tk[378]" -type "float3" -2.1612506 0.23945785 0.05339662 ;
	setAttr ".tk[379]" -type "float3" -2.1637113 0.23945785 0.05339662 ;
	setAttr ".tk[380]" -type "float3" -2.1661716 0.23945785 0.05339662 ;
	setAttr ".tk[381]" -type "float3" -2.1661716 0.47829166 0.05339662 ;
	setAttr ".tk[382]" -type "float3" -2.1637113 0.47829166 0.05339662 ;
	setAttr ".tk[383]" -type "float3" -2.1612506 0.47829166 0.05339662 ;
	setAttr ".tk[384]" -type "float3" -0.0024612697 0.23945785 -0.051327176 ;
	setAttr ".tk[385]" -type "float3" -9.0412391e-07 0.23945785 -0.051327176 ;
	setAttr ".tk[386]" -type "float3" 0.0024598348 0.23945785 -0.051327176 ;
	setAttr ".tk[387]" -type "float3" 0.0024598348 0.47829166 -0.049491867 ;
	setAttr ".tk[388]" -type "float3" -9.0412391e-07 0.47829166 -0.049491867 ;
	setAttr ".tk[389]" -type "float3" -0.0024612697 0.47829166 -0.049491867 ;
	setAttr ".tk[390]" -type "float3" 0.0024598348 0.013644137 -0.05332046 ;
	setAttr ".tk[391]" -type "float3" -9.0412391e-07 0.013644152 -0.05332046 ;
	setAttr ".tk[392]" -type "float3" -0.0024612697 0.013644137 -0.05332046 ;
	setAttr ".tk[393]" -type "float3" -0.0024612697 0.25247797 -0.26528269 ;
	setAttr ".tk[394]" -type "float3" -9.0412391e-07 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[395]" -type "float3" 0.0024598348 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[396]" -type "float3" -1.0793957 0.47829166 -0.049491867 ;
	setAttr ".tk[397]" -type "float3" -1.0818563 0.47829166 -0.049491867 ;
	setAttr ".tk[398]" -type "float3" -1.0843168 0.47829166 -0.049491867 ;
	setAttr ".tk[399]" -type "float3" -1.0843168 0.23945785 -0.051327176 ;
	setAttr ".tk[400]" -type "float3" -1.0818563 0.23945785 -0.051327176 ;
	setAttr ".tk[401]" -type "float3" -1.0793957 0.23945785 -0.051327176 ;
	setAttr ".tk[402]" -type "float3" 1.0843149 0.47829166 -0.049491867 ;
	setAttr ".tk[403]" -type "float3" 1.0818543 0.47829166 -0.049491867 ;
	setAttr ".tk[404]" -type "float3" 1.0793939 0.47829166 -0.049491867 ;
	setAttr ".tk[405]" -type "float3" 1.0793939 0.23945785 -0.051327176 ;
	setAttr ".tk[406]" -type "float3" 1.0818543 0.23945785 -0.051327176 ;
	setAttr ".tk[407]" -type "float3" 1.0843149 0.23945785 -0.051327176 ;
	setAttr ".tk[408]" -type "float3" 3.1628907 0.47829166 -0.049995311 ;
	setAttr ".tk[409]" -type "float3" 3.1604137 0.47829166 -0.049994074 ;
	setAttr ".tk[410]" -type "float3" 3.1579695 0.47829166 -0.049992852 ;
	setAttr ".tk[411]" -type "float3" 3.1586027 0.23945785 -0.051380813 ;
	setAttr ".tk[412]" -type "float3" 3.1610551 0.23945785 -0.051380947 ;
	setAttr ".tk[413]" -type "float3" 3.1635234 0.23945785 -0.051381126 ;
	setAttr ".tk[414]" -type "float3" -3.1586058 0.47829166 -0.049992852 ;
	setAttr ".tk[415]" -type "float3" -3.1610577 0.47829166 -0.049994074 ;
	setAttr ".tk[416]" -type "float3" -3.1635265 0.47829166 -0.049995363 ;
	setAttr ".tk[417]" -type "float3" -3.1635196 0.23945785 -0.051381126 ;
	setAttr ".tk[418]" -type "float3" -3.1610577 0.23945785 -0.051380947 ;
	setAttr ".tk[419]" -type "float3" -3.1585975 0.23945785 -0.051380783 ;
	setAttr ".tk[420]" -type "float3" 0.0024536825 0.94461918 0 ;
	setAttr ".tk[421]" -type "float3" -9.0412391e-07 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[422]" -type "float3" -0.0024674349 0.94461918 -0.063759327 ;
	setAttr ".tk[423]" -type "float3" 2.1612506 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[424]" -type "float3" 2.1637096 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[425]" -type "float3" 2.1661718 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[426]" -type "float3" -2.1612663 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[427]" -type "float3" -2.1637113 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[428]" -type "float3" -2.1661873 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[429]" -type "float3" 0.0024536618 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[430]" -type "float3" -9.0412391e-07 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[431]" -type "float3" -0.0024674558 0.49516952 -0.13558152 ;
	setAttr ".tk[432]" -type "float3" -2.1612718 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[433]" -type "float3" -2.1637113 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[434]" -type "float3" -2.166193 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[435]" -type "float3" 2.1612568 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[436]" -type "float3" 2.1637096 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[437]" -type "float3" 2.166178 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[438]" -type "float3" -2.1612592 0.013644145 1.4901161e-08 ;
	setAttr ".tk[439]" -type "float3" -2.1637113 0.013644145 1.4901161e-08 ;
	setAttr ".tk[440]" -type "float3" -2.1661801 0.013644145 1.4901161e-08 ;
	setAttr ".tk[441]" -type "float3" 0.0024598348 0.013644145 1.4901161e-08 ;
	setAttr ".tk[442]" -type "float3" -9.0412391e-07 0.013644145 1.4901161e-08 ;
	setAttr ".tk[443]" -type "float3" -0.0024612697 0.013644145 0.21101685 ;
	setAttr ".tk[444]" -type "float3" -2.1661789 0.67151809 4.4703484e-08 ;
	setAttr ".tk[445]" -type "float3" -2.1637113 0.67151809 0 ;
	setAttr ".tk[446]" -type "float3" -2.1612582 0.67151809 -2.9802322e-08 ;
	setAttr ".tk[447]" -type "float3" 2.161257 0.67151809 0 ;
	setAttr ".tk[448]" -type "float3" 2.1637096 0.67151809 0 ;
	setAttr ".tk[449]" -type "float3" 2.166178 0.67151809 -7.4505806e-08 ;
	setAttr ".tk[450]" -type "float3" 2.1661754 0.013644145 1.4901161e-08 ;
	setAttr ".tk[451]" -type "float3" 2.1637096 0.013644145 1.4901161e-08 ;
	setAttr ".tk[452]" -type "float3" 2.1612542 0.013644145 1.4901161e-08 ;
	setAttr ".tk[453]" -type "float3" -0.0024612697 0.67151809 -0.20043209 ;
	setAttr ".tk[454]" -type "float3" -9.0412391e-07 0.67151809 0 ;
	setAttr ".tk[455]" -type "float3" 0.0024598348 0.67151809 0 ;
	setAttr ".tk[456]" -type "float3" -2.1661873 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[457]" -type "float3" -2.1637113 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[458]" -type "float3" -2.1612663 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[459]" -type "float3" 2.1661849 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[460]" -type "float3" 2.1637096 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[461]" -type "float3" 2.1612637 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[462]" -type "float3" 0.0024598348 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[463]" -type "float3" -9.0412391e-07 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[464]" -type "float3" -0.0024612697 0.41672435 0.15161656 ;
	setAttr ".tk[465]" -type "float3" -1.0843168 0.013644137 -0.05332046 ;
	setAttr ".tk[466]" -type "float3" -1.0818563 0.013644152 -0.05332046 ;
	setAttr ".tk[467]" -type "float3" -1.0793957 0.013644137 -0.05332046 ;
	setAttr ".tk[468]" -type "float3" -1.0843168 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[469]" -type "float3" -1.0818563 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[470]" -type "float3" -1.0793957 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[471]" -type "float3" -1.0843168 0.67151809 2.9802322e-08 ;
	setAttr ".tk[472]" -type "float3" -1.0818563 0.67151809 0 ;
	setAttr ".tk[473]" -type "float3" -1.0793957 0.67151809 2.9802322e-08 ;
	setAttr ".tk[474]" -type "float3" -1.0843287 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[475]" -type "float3" -1.0818563 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[476]" -type "float3" -1.0794078 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[477]" -type "float3" -1.0843287 0.94461918 -1.8626451e-08 ;
	setAttr ".tk[478]" -type "float3" -1.0818563 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[479]" -type "float3" -1.0794079 0.94461918 -1.4901161e-08 ;
	setAttr ".tk[480]" -type "float3" -1.0843168 0.83573294 2.3283064e-09 ;
	setAttr ".tk[481]" -type "float3" -1.0818563 0.83573294 2.3283064e-09 ;
	setAttr ".tk[482]" -type "float3" -1.0793957 0.83573294 2.3283064e-09 ;
	setAttr ".tk[483]" -type "float3" -1.0843168 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[484]" -type "float3" -1.0818563 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[485]" -type "float3" -1.0793957 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[486]" -type "float3" -1.0843168 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[487]" -type "float3" -1.0818563 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[488]" -type "float3" -1.0793957 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[489]" -type "float3" -1.0843168 0.013644145 1.4901161e-08 ;
	setAttr ".tk[490]" -type "float3" -1.0818563 0.013644145 1.4901161e-08 ;
	setAttr ".tk[491]" -type "float3" -1.0793957 0.013644145 1.4901161e-08 ;
	setAttr ".tk[492]" -type "float3" -1.0843168 0.25247797 2.9802322e-08 ;
	setAttr ".tk[493]" -type "float3" -1.0818563 0.25247797 2.9802322e-08 ;
	setAttr ".tk[494]" -type "float3" -1.0793957 0.25247797 2.9802322e-08 ;
	setAttr ".tk[495]" -type "float3" -1.0843168 0.013644137 0.05339662 ;
	setAttr ".tk[496]" -type "float3" -1.0818563 0.013644152 0.05339662 ;
	setAttr ".tk[497]" -type "float3" -1.0793957 0.013644137 0.05339662 ;
	setAttr ".tk[498]" -type "float3" -1.0843168 0.23945785 0.05339662 ;
	setAttr ".tk[499]" -type "float3" -1.0818563 0.23945785 0.05339662 ;
	setAttr ".tk[500]" -type "float3" -1.0793957 0.23945785 0.05339662 ;
	setAttr ".tk[501]" -type "float3" -1.0843168 0.47829166 0.05339662 ;
	setAttr ".tk[502]" -type "float3" -1.0818563 0.47829166 0.05339662 ;
	setAttr ".tk[503]" -type "float3" -1.0793957 0.47829166 0.05339662 ;
	setAttr ".tk[504]" -type "float3" 1.0793939 0.013644137 -0.05332046 ;
	setAttr ".tk[505]" -type "float3" 1.0818543 0.013644152 -0.05332046 ;
	setAttr ".tk[506]" -type "float3" 1.0843149 0.013644137 -0.05332046 ;
	setAttr ".tk[507]" -type "float3" 1.0793939 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[508]" -type "float3" 1.0818543 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[509]" -type "float3" 1.0843149 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[510]" -type "float3" 1.0793939 0.67151809 0 ;
	setAttr ".tk[511]" -type "float3" 1.0818543 0.67151809 0 ;
	setAttr ".tk[512]" -type "float3" 1.0843149 0.67151809 0 ;
	setAttr ".tk[513]" -type "float3" 1.0793939 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[514]" -type "float3" 1.0818543 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[515]" -type "float3" 1.0843149 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[516]" -type "float3" 1.0793939 0.94461918 0 ;
	setAttr ".tk[517]" -type "float3" 1.0818543 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[518]" -type "float3" 1.0843149 0.94461918 0 ;
	setAttr ".tk[519]" -type "float3" 1.0793939 0.83573294 2.3283064e-09 ;
	setAttr ".tk[520]" -type "float3" 1.0818543 0.83573294 2.3283064e-09 ;
	setAttr ".tk[521]" -type "float3" 1.0843149 0.83573294 2.3283064e-09 ;
	setAttr ".tk[522]" -type "float3" 1.0793939 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[523]" -type "float3" 1.0818543 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[524]" -type "float3" 1.0843149 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[525]" -type "float3" 1.0793939 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[526]" -type "float3" 1.0818543 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[527]" -type "float3" 1.0843149 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[528]" -type "float3" 1.0793939 0.013644145 1.4901161e-08 ;
	setAttr ".tk[529]" -type "float3" 1.0818543 0.013644145 1.4901161e-08 ;
	setAttr ".tk[530]" -type "float3" 1.0843149 0.013644145 1.4901161e-08 ;
	setAttr ".tk[531]" -type "float3" 1.0793939 0.25247797 2.9802322e-08 ;
	setAttr ".tk[532]" -type "float3" 1.0818543 0.25247797 2.9802322e-08 ;
	setAttr ".tk[533]" -type "float3" 1.0843149 0.25247797 2.9802322e-08 ;
	setAttr ".tk[534]" -type "float3" 1.0793939 0.013644137 0.05339662 ;
	setAttr ".tk[535]" -type "float3" 1.0818543 0.013644152 0.05339662 ;
	setAttr ".tk[536]" -type "float3" 1.0843149 0.013644137 0.05339662 ;
	setAttr ".tk[537]" -type "float3" 1.0793939 0.23945785 0.05339662 ;
	setAttr ".tk[538]" -type "float3" 1.0818543 0.23945785 0.05339662 ;
	setAttr ".tk[539]" -type "float3" 1.0843149 0.23945785 0.05339662 ;
	setAttr ".tk[540]" -type "float3" 1.0793939 0.47829166 0.05339662 ;
	setAttr ".tk[541]" -type "float3" 1.0818543 0.47829166 0.05339662 ;
	setAttr ".tk[542]" -type "float3" 1.0843149 0.47829166 0.05339662 ;
	setAttr ".tk[543]" -type "float3" 3.1611876 0.013644137 -0.053396188 ;
	setAttr ".tk[544]" -type "float3" 3.1636605 0.013644137 -0.053396426 ;
	setAttr ".tk[545]" -type "float3" 3.1661091 0.013644145 -0.05339662 ;
	setAttr ".tk[546]" -type "float3" 3.1585968 0.25247797 1.4901161e-08 ;
	setAttr ".tk[547]" -type "float3" 3.1610551 0.25247794 -4.4703484e-08 ;
	setAttr ".tk[548]" -type "float3" 3.163518 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[549]" -type "float3" 3.158874 0.67151809 4.4703484e-08 ;
	setAttr ".tk[550]" -type "float3" 3.1613197 0.67151809 4.4703484e-08 ;
	setAttr ".tk[551]" -type "float3" 3.1637945 0.67151809 2.9802322e-08 ;
	setAttr ".tk[552]" -type "float3" 3.1591532 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[553]" -type "float3" 3.1615829 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[554]" -type "float3" 3.1640744 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[555]" -type "float3" 3.1591339 0.94461918 -1.1175871e-08 ;
	setAttr ".tk[556]" -type "float3" 3.1615829 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[557]" -type "float3" 3.1640553 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[558]" -type "float3" 3.1591153 0.83573294 2.3283064e-09 ;
	setAttr ".tk[559]" -type "float3" 3.1615846 0.83573294 2.3283064e-09 ;
	setAttr ".tk[560]" -type "float3" 3.164036 0.83573294 2.3283064e-09 ;
	setAttr ".tk[561]" -type "float3" 3.1591265 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[562]" -type "float3" 3.1615829 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[563]" -type "float3" 3.164047 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[564]" -type "float3" 3.1591392 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[565]" -type "float3" 3.1615829 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[566]" -type "float3" 3.1640599 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[567]" -type "float3" 3.1601706 0.013644145 5.9604645e-08 ;
	setAttr ".tk[568]" -type "float3" 3.1626213 0.013644145 1.4901161e-08 ;
	setAttr ".tk[569]" -type "float3" 3.1650918 0.013644145 1.4901161e-08 ;
	setAttr ".tk[570]" -type "float3" 3.1612029 0.25247794 -1.4901161e-08 ;
	setAttr ".tk[571]" -type "float3" 3.1636605 0.25247794 2.9802322e-08 ;
	setAttr ".tk[572]" -type "float3" 3.1661236 0.25247797 -4.4703484e-08 ;
	setAttr ".tk[573]" -type "float3" 3.1612012 0.013644152 0.05339662 ;
	setAttr ".tk[574]" -type "float3" 3.1636605 0.013644137 0.05339662 ;
	setAttr ".tk[575]" -type "float3" 3.1661224 0.013644145 0.05339662 ;
	setAttr ".tk[576]" -type "float3" 3.1585946 0.23945785 0.05339662 ;
	setAttr ".tk[577]" -type "float3" 3.1610551 0.23945785 0.05339662 ;
	setAttr ".tk[578]" -type "float3" 3.1635158 0.23945785 0.05339662 ;
	setAttr ".tk[579]" -type "float3" 3.1579609 0.47829166 0.05339662 ;
	setAttr ".tk[580]" -type "float3" 3.1604137 0.47829166 0.05339662 ;
	setAttr ".tk[581]" -type "float3" 3.1628826 0.47829166 0.05339662 ;
	setAttr ".tk[582]" -type "float3" -3.1635187 0.013644145 -0.05339662 ;
	setAttr ".tk[583]" -type "float3" -3.1610577 0.013644152 -0.053396426 ;
	setAttr ".tk[584]" -type "float3" -3.1585972 0.013644145 -0.053396188 ;
	setAttr ".tk[585]" -type "float3" -3.1635187 0.25247797 -2.9802322e-08 ;
	setAttr ".tk[586]" -type "float3" -3.1610577 0.25247794 -4.4703484e-08 ;
	setAttr ".tk[587]" -type "float3" -3.1585972 0.25247797 1.4901161e-08 ;
	setAttr ".tk[588]" -type "float3" -3.1637971 0.67151809 -1.0430813e-07 ;
	setAttr ".tk[589]" -type "float3" -3.1613207 0.67151809 4.4703484e-08 ;
	setAttr ".tk[590]" -type "float3" -3.1588769 0.67151809 -2.9802322e-08 ;
	setAttr ".tk[591]" -type "float3" -3.1640751 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[592]" -type "float3" -3.1615853 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[593]" -type "float3" -3.1591554 0.49516952 -2.9802322e-08 ;
	setAttr ".tk[594]" -type "float3" -3.1640635 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[595]" -type "float3" -3.1615858 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[596]" -type "float3" -3.1591432 0.94461918 -3.7252903e-09 ;
	setAttr ".tk[597]" -type "float3" -3.1640518 0.83573294 2.3283064e-09 ;
	setAttr ".tk[598]" -type "float3" -3.1615856 0.83573294 2.3283064e-09 ;
	setAttr ".tk[599]" -type "float3" -3.1591299 0.83573294 2.3283064e-09 ;
	setAttr ".tk[600]" -type "float3" -3.1640635 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[601]" -type "float3" -3.1615858 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[602]" -type "float3" -3.1591432 0.59793538 -7.4505806e-09 ;
	setAttr ".tk[603]" -type "float3" -3.1640751 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[604]" -type "float3" -3.1615853 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[605]" -type "float3" -3.1591554 0.41672435 -7.4505806e-09 ;
	setAttr ".tk[606]" -type "float3" -3.1650994 0.013644152 1.4901161e-08 ;
	setAttr ".tk[607]" -type "float3" -3.1626236 0.013644145 1.4901161e-08 ;
	setAttr ".tk[608]" -type "float3" -3.1601789 0.013644145 5.9604645e-08 ;
	setAttr ".tk[609]" -type "float3" -3.1661251 0.25247797 -4.4703484e-08 ;
	setAttr ".tk[610]" -type "float3" -3.1636627 0.25247794 2.9802322e-08 ;
	setAttr ".tk[611]" -type "float3" -3.1612046 0.25247794 -1.4901161e-08 ;
	setAttr ".tk[612]" -type "float3" -3.1661241 0.013644145 0.05339662 ;
	setAttr ".tk[613]" -type "float3" -3.1636627 0.013644137 0.05339662 ;
	setAttr ".tk[614]" -type "float3" -3.1612024 0.013644152 0.05339662 ;
	setAttr ".tk[615]" -type "float3" -3.1635182 0.23945785 0.05339662 ;
	setAttr ".tk[616]" -type "float3" -3.1610577 0.23945785 0.05339662 ;
	setAttr ".tk[617]" -type "float3" -3.1585972 0.23945785 0.05339662 ;
	setAttr ".tk[618]" -type "float3" -3.162883 0.47829166 0.05339662 ;
	setAttr ".tk[619]" -type "float3" -3.1604159 0.47829166 0.05339662 ;
	setAttr ".tk[620]" -type "float3" -3.1579623 0.47829166 0.05339662 ;
createNode polyMapCut -n "polyMapCut31";
	rename -uid "055854DD-4E93-8472-B557-AD9413E71B14";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1129]";
createNode polyMapCut -n "polyMapCut32";
	rename -uid "4E9612F9-400C-D43E-ECE3-5089A449E053";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[590]" "e[734]";
createNode polyMapCut -n "polyMapCut33";
	rename -uid "6E130CB8-4126-415D-3EC4-8AA863616038";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[68]";
createNode polyMapCut -n "polyMapCut34";
	rename -uid "B91B7FBD-40F7-28F7-34CF-25BFAA51D7C0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20]" "e[27]" "e[34]";
createNode polyMapCut -n "polyMapCut35";
	rename -uid "EEE303B2-4E96-0655-5D4A-A5A00BF55501";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[40]" "e[47]" "e[51]";
createNode polyMapCut -n "polyMapCut36";
	rename -uid "89B66DBF-47DA-DDC6-7472-4C84E8B62380";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[1]" "e[10]" "e[40]" "e[47]" "e[51]";
createNode polyMapCut -n "polyMapCut37";
	rename -uid "D36CB530-430C-D186-5425-3C8E4FF024B9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 38 "e[1]" "e[10]" "e[14]" "e[21]" "e[29]" "e[36]" "e[395]" "e[407]" "e[414]" "e[425]" "e[450]" "e[463]" "e[476]" "e[489]" "e[553]" "e[557]" "e[584]" "e[588]" "e[626]" "e[630]" "e[771]" "e[774]" "e[819]" "e[822]" "e[867]" "e[870]" "e[915]" "e[918]" "e[944]" "e[946]" "e[952]" "e[954]" "e[966]" "e[968]" "e[1035:1036]" "e[1059:1060]" "e[1083:1084]" "e[1107:1108]";
createNode polyMapCut -n "polyMapCut38";
	rename -uid "CE4993F9-48D0-B4AD-AF40-7F930371E556";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 38 "e[40]" "e[47]" "e[51]" "e[55]" "e[62]" "e[66]" "e[399]" "e[409]" "e[416]" "e[423]" "e[442]" "e[455]" "e[468]" "e[481]" "e[595]" "e[599]" "e[608]" "e[612]" "e[660]" "e[664]" "e[739]" "e[742]" "e[787]" "e[790]" "e[835]" "e[838]" "e[883]" "e[886]" "e[956]" "e[958]" "e[960]" "e[962]" "e[976]" "e[978]" "e[1019:1020]" "e[1043:1044]" "e[1067:1068]" "e[1091:1092]";
createNode polyTweakUV -n "polyTweakUV68";
	rename -uid "E6C7EC07-48D0-6086-003C-18BA9C43AA8C";
	setAttr ".uopa" yes;
	setAttr -s 167 ".uvtk";
	setAttr ".uvtk[88]" -type "float2" -0.68608934 -0.40313947 ;
	setAttr ".uvtk[89]" -type "float2" -0.68593687 -0.40313947 ;
	setAttr ".uvtk[90]" -type "float2" -0.66954541 -0.41822729 ;
	setAttr ".uvtk[122]" -type "float2" -0.57505113 -0.00088886917 ;
	setAttr ".uvtk[123]" -type "float2" -0.57520348 -0.00088886917 ;
	setAttr ".uvtk[124]" -type "float2" -0.62811214 -0.40255195 ;
	setAttr ".uvtk[125]" -type "float2" -0.62929505 -0.40310663 ;
	setAttr ".uvtk[127]" -type "float2" -0.61251193 -0.41695386 ;
	setAttr ".uvtk[145]" -type "float2" -0.72645402 0.014826566 ;
	setAttr ".uvtk[147]" -type "float2" -0.7420429 0.00047755241 ;
	setAttr ".uvtk[148]" -type "float2" -0.74086082 -7.7515841e-05 ;
	setAttr ".uvtk[187]" -type "float2" -0.41857207 0.45334882 ;
	setAttr ".uvtk[197]" -type "float2" -0.99016964 0.47604087 ;
	setAttr ".uvtk[206]" -type "float2" -0.41857207 0.45334995 ;
	setAttr ".uvtk[207]" -type "float2" -0.41857225 0.45334882 ;
	setAttr ".uvtk[208]" -type "float2" -0.41857225 0.45334995 ;
	setAttr ".uvtk[209]" -type "float2" -0.41857225 0.45334882 ;
	setAttr ".uvtk[218]" -type "float2" -1.0287859 0.48820493 ;
	setAttr ".uvtk[220]" -type "float2" -1.0296134 0.48270836 ;
	setAttr ".uvtk[221]" -type "float2" -0.9918195 0.47080329 ;
	setAttr ".uvtk[230]" -type "float2" -0.41857225 0.45334995 ;
	setAttr ".uvtk[231]" -type "float2" -0.41857225 0.45334882 ;
	setAttr ".uvtk[232]" -type "float2" -0.41857207 0.45334882 ;
	setAttr ".uvtk[238]" -type "float2" -1.0288759 0.48823324 ;
	setAttr ".uvtk[239]" -type "float2" -1.03036 0.48320779 ;
	setAttr ".uvtk[240]" -type "float2" -0.9909215 0.47078469 ;
	setAttr ".uvtk[250]" -type "float2" -0.41857225 0.45334995 ;
	setAttr ".uvtk[251]" -type "float2" -0.41857225 0.45334882 ;
	setAttr ".uvtk[257]" -type "float2" -1.0304501 0.48323616 ;
	setAttr ".uvtk[258]" -type "float2" -0.99083138 0.47075632 ;
	setAttr ".uvtk[259]" -type "float2" -0.98916709 0.47572514 ;
	setAttr ".uvtk[267]" -type "float2" -0.41857207 0.45335001 ;
	setAttr ".uvtk[268]" -type "float2" -0.41857225 0.45334995 ;
	setAttr ".uvtk[269]" -type "float2" -0.41857225 0.45334876 ;
	setAttr ".uvtk[270]" -type "float2" -0.41857207 0.45334762 ;
	setAttr ".uvtk[272]" -type "float2" -1.0297885 0.48852071 ;
	setAttr ".uvtk[273]" -type "float2" -1.0305402 0.4832646 ;
	setAttr ".uvtk[274]" -type "float2" -0.99074125 0.47072789 ;
	setAttr ".uvtk[284]" -type "float2" -0.41857225 0.45335001 ;
	setAttr ".uvtk[285]" -type "float2" -0.41857225 0.45334762 ;
	setAttr ".uvtk[287]" -type "float2" -1.0314382 0.48328313 ;
	setAttr ".uvtk[288]" -type "float2" -0.98999465 0.47022846 ;
	setAttr ".uvtk[289]" -type "float2" -0.9535377 0.46454594 ;
	setAttr ".uvtk[293]" -type "float2" -0.41857207 0.45335102 ;
	setAttr ".uvtk[298]" -type "float2" -0.41857225 0.45335102 ;
	setAttr ".uvtk[299]" -type "float2" -1.0692322 0.49518821 ;
	setAttr ".uvtk[300]" -type "float2" -0.9552002 0.45926824 ;
	setAttr ".uvtk[305]" -type "float2" -0.41857225 0.45335102 ;
	setAttr ".uvtk[311]" -type "float2" -0.95430213 0.45924965 ;
	setAttr ".uvtk[312]" -type "float2" -0.95262504 0.46425954 ;
	setAttr ".uvtk[316]" -type "float2" -0.41857225 0.45335102 ;
	setAttr ".uvtk[320]" -type "float2" -0.95421207 0.45922127 ;
	setAttr ".uvtk[321]" -type "float2" -0.95253474 0.46423122 ;
	setAttr ".uvtk[324]" -type "float2" -0.41857225 0.45335102 ;
	setAttr ".uvtk[325]" -type "float2" -0.41857207 0.45335108 ;
	setAttr ".uvtk[327]" -type "float2" -0.95412195 0.4591929 ;
	setAttr ".uvtk[330]" -type "float2" -0.41857225 0.45335108 ;
	setAttr ".uvtk[331]" -type "float2" -0.41857207 0.45335156 ;
	setAttr ".uvtk[332]" -type "float2" -0.95337534 0.45869341 ;
	setAttr ".uvtk[334]" -type "float2" -0.41857225 0.45335156 ;
	setAttr ".uvtk[335]" -type "float2" -0.93641609 0.45335129 ;
	setAttr ".uvtk[336]" -type "float2" -0.92942214 0.45719448 ;
	setAttr ".uvtk[337]" -type "float2" -0.93206304 0.45335129 ;
	setAttr ".uvtk[356]" -type "float2" -0.0073909648 2.9325485e-05 ;
	setAttr ".uvtk[367]" -type "float2" -0.0073908977 -4.4703484e-07 ;
	setAttr ".uvtk[368]" -type "float2" -0.0075712763 -8.6426735e-07 ;
	setAttr ".uvtk[376]" -type "float2" -0.52565354 -1.6123056e-05 ;
	setAttr ".uvtk[378]" -type "float2" -0.0073869713 7.1525574e-07 ;
	setAttr ".uvtk[380]" -type "float2" -0.0075752027 2.9802322e-07 ;
	setAttr ".uvtk[381]" -type "float2" -0.0075752623 2.8848648e-05 ;
	setAttr ".uvtk[389]" -type "float2" -0.52553463 -1.6123056e-05 ;
	setAttr ".uvtk[390]" -type "float2" -0.52553719 0 ;
	setAttr ".uvtk[391]" -type "float2" -0.5256536 0 ;
	setAttr ".uvtk[392]" -type "float2" -0.0073865391 6.8545341e-07 ;
	setAttr ".uvtk[393]" -type "float2" -0.0073861815 2.9325485e-05 ;
	setAttr ".uvtk[394]" -type "float2" -0.0075756349 3.2782555e-07 ;
	setAttr ".uvtk[395]" -type "float2" -0.0075756945 2.887845e-05 ;
	setAttr ".uvtk[403]" -type "float2" -0.52553433 -1.6152859e-05 ;
	setAttr ".uvtk[404]" -type "float2" -0.52553463 -7.4505806e-07 ;
	setAttr ".uvtk[405]" -type "float2" -0.5256561 -7.4505806e-07 ;
	setAttr ".uvtk[406]" -type "float2" -0.5256561 -1.6123056e-05 ;
	setAttr ".uvtk[407]" -type "float2" -0.0073861144 7.1525574e-07 ;
	setAttr ".uvtk[417]" -type "float2" -0.52553433 -7.4505806e-07 ;
	setAttr ".uvtk[418]" -type "float2" -0.5256564 -7.4505806e-07 ;
	setAttr ".uvtk[419]" -type "float2" -0.5256564 -1.6123056e-05 ;
	setAttr ".uvtk[420]" -type "float2" -0.007382188 -4.1723251e-07 ;
	setAttr ".uvtk[421]" -type "float2" -0.007201869 2.977252e-05 ;
	setAttr ".uvtk[429]" -type "float2" -0.52553153 -1.6123056e-05 ;
	setAttr ".uvtk[430]" -type "float2" -0.52553409 -7.4505806e-07 ;
	setAttr ".uvtk[431]" -type "float2" -0.007201802 1.4901161e-08 ;
	setAttr ".uvtk[441]" -type "float2" -0.52553153 0 ;
	setAttr ".uvtk[442]" -type "float2" -0.007197883 1.1771917e-06 ;
	setAttr ".uvtk[449]" -type "float2" -0.52541262 -1.6123056e-05 ;
	setAttr ".uvtk[452]" -type "float2" -0.52541512 0 ;
	setAttr ".uvtk[453]" -type "float2" -0.0071974508 1.1771917e-06 ;
	setAttr ".uvtk[454]" -type "float2" -0.0071970858 2.977252e-05 ;
	setAttr ".uvtk[461]" -type "float2" -0.52541262 -7.4505806e-07 ;
	setAttr ".uvtk[464]" -type "float2" -0.0071970187 1.1771917e-06 ;
	setAttr ".uvtk[472]" -type "float2" -0.52541232 -7.4505806e-07 ;
	setAttr ".uvtk[475]" -type "float2" -0.0071930923 2.9802322e-08 ;
	setAttr ".uvtk[476]" -type "float2" -0.0070275553 2.8051436e-05 ;
	setAttr ".uvtk[482]" -type "float2" -0.52540952 -1.6123056e-05 ;
	setAttr ".uvtk[483]" -type "float2" -0.52541202 -7.4505806e-07 ;
	setAttr ".uvtk[486]" -type "float2" -0.0070270337 2.9802322e-08 ;
	setAttr ".uvtk[494]" -type "float2" -0.52540952 0 ;
	setAttr ".uvtk[497]" -type "float2" -0.0070231073 1.1771917e-06 ;
	setAttr ".uvtk[504]" -type "float2" -0.52529979 -1.6257167e-05 ;
	setAttr ".uvtk[505]" -type "float2" -0.52530235 0 ;
	setAttr ".uvtk[508]" -type "float2" -0.0070226751 1.1771917e-06 ;
	setAttr ".uvtk[509]" -type "float2" -0.007022772 2.7999282e-05 ;
	setAttr ".uvtk[515]" -type "float2" -0.52529955 -1.6257167e-05 ;
	setAttr ".uvtk[516]" -type "float2" -0.52529985 -7.4505806e-07 ;
	setAttr ".uvtk[517]" -type "float2" -0.0070222504 1.1771917e-06 ;
	setAttr ".uvtk[523]" -type "float2" -0.52529955 -7.3760748e-07 ;
	setAttr ".uvtk[524]" -type "float2" -0.0070183165 2.9802322e-08 ;
	setAttr ".uvtk[527]" -type "float2" -0.52529675 -1.6264617e-05 ;
	setAttr ".uvtk[528]" -type "float2" -0.52529925 -7.4505806e-07 ;
	setAttr ".uvtk[529]" -type "float2" -0.0069373809 -1.8440187e-07 ;
	setAttr ".uvtk[530]" -type "float2" -0.52524447 -1.6381964e-05 ;
	setAttr ".uvtk[531]" -type "float2" -0.52529675 0 ;
	setAttr ".uvtk[532]" -type "float2" -0.52518708 -7.4505806e-09 ;
	setAttr ".uvtk[533]" -type "float2" -0.52083403 -6.0535967e-09 ;
	setAttr ".uvtk[534]" -type "float2" -0.51819313 -0.0038432414 ;
	setAttr ".uvtk[779]" -type "float2" -0.55882597 0.013648257 ;
	setAttr ".uvtk[789]" -type "float2" -0.52524447 1.8626451e-09 ;
	setAttr ".uvtk[790]" -type "float2" -0.52351135 -0.0053198403 ;
	setAttr ".uvtk[799]" -type "float2" -0.95271504 0.46428779 ;
	setAttr ".uvtk[801]" -type "float2" -0.95171225 0.46397319 ;
	setAttr ".uvtk[803]" -type "float2" -0.52529925 -1.6257167e-05 ;
	setAttr ".uvtk[805]" -type "float2" -0.52530235 -1.6257167e-05 ;
	setAttr ".uvtk[807]" -type "float2" -0.52553409 -1.6123056e-05 ;
	setAttr ".uvtk[809]" -type "float2" -0.52553713 -1.6123056e-05 ;
	setAttr ".uvtk[811]" -type "float2" -1.0289662 0.48826167 ;
	setAttr ".uvtk[813]" -type "float2" -1.0279636 0.48794588 ;
	setAttr ".uvtk[814]" -type "float2" -0.52541512 -1.6137958e-05 ;
	setAttr ".uvtk[816]" -type "float2" -0.52541202 -1.6123056e-05 ;
	setAttr ".uvtk[818]" -type "float2" -1.0675824 0.5004257 ;
	setAttr ".uvtk[819]" -type "float2" -0.72527212 0.012015253 ;
	setAttr ".uvtk[823]" -type "float2" -0.98834455 0.47546706 ;
	setAttr ".uvtk[825]" -type "float2" -0.98934734 0.47578183 ;
	setAttr ".uvtk[831]" -type "float2" -0.52541232 -1.6137958e-05 ;
	setAttr ".uvtk[832]" -type "float2" -0.669698 -0.41527122 ;
	setAttr ".uvtk[833]" -type "float2" -0.98925728 0.47575346 ;
	setAttr ".uvtk[835]" -type "float2" -0.93474036 0.45867112 ;
	setAttr ".uvtk[839]" -type "float2" -0.41857207 0.45335102 ;
	setAttr ".uvtk[841]" -type "float2" -0.41857207 0.45335102 ;
	setAttr ".uvtk[843]" -type "float2" -0.0070188381 2.7962029e-05 ;
	setAttr ".uvtk[845]" -type "float2" -0.0070236214 2.8014183e-05 ;
	setAttr ".uvtk[847]" -type "float2" -0.0073822476 2.9325485e-05 ;
	setAttr ".uvtk[849]" -type "float2" -0.0073870309 2.9325485e-05 ;
	setAttr ".uvtk[851]" -type "float2" -0.41857207 0.45334876 ;
	setAttr ".uvtk[853]" -type "float2" -0.41857207 0.45334882 ;
	setAttr ".uvtk[855]" -type "float2" -0.0075713433 2.887845e-05 ;
	setAttr ".uvtk[856]" -type "float2" -0.55867356 0.016545758 ;
	setAttr ".uvtk[857]" -type "float2" -0.61369473 -0.41971385 ;
	setAttr ".uvtk[860]" -type "float2" -0.0071931668 2.9727817e-05 ;
	setAttr ".uvtk[862]" -type "float2" -0.00719795 2.977252e-05 ;
	setAttr ".uvtk[864]" -type "float2" -0.41857207 0.45334995 ;
	setAttr ".uvtk[866]" -type "float2" -0.41857207 0.45334995 ;
	setAttr ".uvtk[868]" -type "float2" -0.41857207 0.45335102 ;
	setAttr ".uvtk[869]" -type "float2" -0.0070231892 2.7999282e-05 ;
	setAttr ".uvtk[870]" -type "float2" -0.0073866136 2.9295683e-05 ;
	setAttr ".uvtk[871]" -type "float2" -0.41857207 0.45334882 ;
	setAttr ".uvtk[873]" -type "float2" -0.0071975179 2.9787421e-05 ;
	setAttr ".uvtk[874]" -type "float2" -0.41857207 0.45334995 ;
	setAttr ".uvtk[876]" -type "float2" -0.0069382377 2.6013702e-05 ;
createNode polyMapSewMove -n "polyMapSewMove57";
	rename -uid "F90CA757-4222-EA25-0233-EDA37AF4607D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 65 "e[7]" "e[24]" "e[394]" "e[398]" "e[402]" "e[404]" "e[412]" "e[418:419]" "e[421]" "e[441]" "e[451]" "e[454]" "e[464]" "e[467]" "e[477]" "e[480]" "e[490]" "e[551]" "e[555]" "e[582]" "e[586]" "e[593]" "e[597]" "e[606]" "e[610]" "e[624]" "e[628]" "e[658]" "e[662]" "e[676]" "e[679]" "e[687]" "e[690]" "e[698]" "e[701]" "e[709]" "e[712]" "e[775]" "e[778]" "e[823]" "e[826]" "e[871]" "e[874]" "e[919]" "e[922]" "e[943]" "e[945]" "e[951]" "e[953]" "e[955]" "e[957]" "e[959]" "e[961]" "e[965]" "e[967]" "e[975]" "e[977]" "e[1017:1018]" "e[1037:1038]" "e[1041:1042]" "e[1061:1062]" "e[1065:1066]" "e[1085:1086]" "e[1089:1090]" "e[1109:1110]";
createNode polyTweakUV -n "polyTweakUV69";
	rename -uid "E6342B38-42AB-C1CE-7BF3-D9A4B7546BA3";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" -0.85327965 0.085143417 ;
	setAttr ".uvtk[4]" -type "float2" -0.8758269 0.044977576 ;
	setAttr ".uvtk[5]" -type "float2" -0.84759772 0.049474448 ;
	setAttr ".uvtk[10]" -type "float2" -0.9554646 0.040585965 ;
	setAttr ".uvtk[712]" -type "float2" -0.88848251 0.079535693 ;
	setAttr ".uvtk[713]" -type "float2" -0.96857429 0.066777378 ;
createNode polyMapSewMove -n "polyMapSewMove58";
	rename -uid "D4370C8D-4867-0393-0CD9-93BFAE426B49";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6]" "e[11]" "e[99]";
createNode polyMapCut -n "polyMapCut39";
	rename -uid "57C295FE-4259-B0C2-D6CE-6F89384A40E9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[42]";
createNode polyMapSew -n "polyMapSew1";
	rename -uid "7E5E2320-4248-9F09-83D9-C9BA539DABE9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[48]" "e[105]";
createNode polyTweakUV -n "polyTweakUV70";
	rename -uid "1980BF6F-4E0C-2988-208C-56B0E7FCF5DD";
	setAttr ".uopa" yes;
	setAttr -s 11 ".uvtk";
	setAttr ".uvtk[33]" -type "float2" -0.87194186 0.40225843 ;
	setAttr ".uvtk[36]" -type "float2" -0.43435171 0.19977131 ;
	setAttr ".uvtk[37]" -type "float2" -0.41987526 0.1999518 ;
	setAttr ".uvtk[40]" -type "float2" -0.9084186 0.4159492 ;
	setAttr ".uvtk[41]" -type "float2" -0.93124694 0.41311529 ;
	setAttr ".uvtk[42]" -type "float2" -0.43450454 0.21202901 ;
	setAttr ".uvtk[43]" -type "float2" -0.90867901 0.43165317 ;
	setAttr ".uvtk[617]" -type "float2" 0.014106527 2.9802322e-08 ;
	setAttr ".uvtk[703]" -type "float2" -0.91301429 0.40174624 ;
	setAttr ".uvtk[706]" -type "float2" -0.85388923 0.40248343 ;
createNode polyMapSewMove -n "polyMapSewMove59";
	rename -uid "F2E071AF-4F97-F648-0AB3-53B1FD46596B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[42]" "e[49]" "e[54]";
createNode polyTweakUV -n "polyTweakUV71";
	rename -uid "84990771-4827-3122-407B-0CB04F0A85F2";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[44]" -type "float2" -0.85010815 -0.45125541 ;
	setAttr ".uvtk[47]" -type "float2" -0.77180392 -0.45142961 ;
	setAttr ".uvtk[51]" -type "float2" -0.74389488 -0.45437384 ;
	setAttr ".uvtk[744]" -type "float2" -0.74761516 -0.48963854 ;
	setAttr ".uvtk[745]" -type "float2" -0.78241873 -0.48596695 ;
	setAttr ".uvtk[746]" -type "float2" -0.86160213 -0.47761351 ;
createNode polyMapSewMove -n "polyMapSewMove60";
	rename -uid "32D245D7-4693-CF05-889E-A88B4535B9EA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[25]" "e[28]" "e[103]";
createNode polyTweakUV -n "polyTweakUV72";
	rename -uid "8D9955A1-481B-267A-C332-5AAFB0233B19";
	setAttr ".uopa" yes;
	setAttr -s 11 ".uvtk";
	setAttr ".uvtk[44]" -type "float2" -0.0033169729 0.00051349821 ;
	setAttr ".uvtk[47]" -type "float2" -0.0065237274 -0.012076205 ;
	setAttr ".uvtk[51]" -type "float2" -0.0076556122 -0.016560754 ;
	setAttr ".uvtk[523]" -type "float2" 0.00061940495 -7.8696758e-08 ;
	setAttr ".uvtk[524]" -type "float2" -0.0062643215 0.0039878599 ;
	setAttr ".uvtk[611]" -type "float2" -0.0015114353 -0.017546793 ;
	setAttr ".uvtk[694]" -type "float2" -0.0009357438 -0.012928116 ;
	setAttr ".uvtk[696]" -type "float2" -0.00043691718 0.0053225677 ;
	setAttr ".uvtk[741]" -type "float2" -0.013508248 -0.014274938 ;
	setAttr ".uvtk[742]" -type "float2" -0.011457521 -0.0088425186 ;
createNode polyMapSewMove -n "polyMapSewMove61";
	rename -uid "5395998B-4C76-F0E1-35F0-969A9D6F1C88";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[20]" "e[27]" "e[34]";
createNode polyTweakUV -n "polyTweakUV73";
	rename -uid "727D0584-4440-798F-6A07-4A827768ADF5";
	setAttr ".uopa" yes;
	setAttr -s 14 ".uvtk";
	setAttr ".uvtk[78]" -type "float2" -0.72808194 -0.82133764 ;
	setAttr ".uvtk[82]" -type "float2" -0.73123723 -0.83057183 ;
	setAttr ".uvtk[83]" -type "float2" -0.71158212 -0.84845561 ;
	setAttr ".uvtk[84]" -type "float2" -0.69677502 -0.84442204 ;
	setAttr ".uvtk[85]" -type "float2" -0.7375378 -0.83876306 ;
	setAttr ".uvtk[86]" -type "float2" -0.71300477 -0.85038501 ;
	setAttr ".uvtk[87]" -type "float2" -0.70391679 -0.85410768 ;
	setAttr ".uvtk[684]" -type "float2" 0 2.3283064e-10 ;
	setAttr ".uvtk[685]" -type "float2" 0 2.3283064e-10 ;
	setAttr ".uvtk[775]" -type "float2" -0.74218655 -0.82041484 ;
	setAttr ".uvtk[778]" -type "float2" -0.70633394 -0.83737373 ;
createNode polyMapSewMove -n "polyMapSewMove62";
	rename -uid "53115D78-4C9D-3616-6FED-79AA0A395A2F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[58]" "e[64]" "e[68]";
createNode polyMapSewMove -n "polyMapSewMove63";
	rename -uid "91CDF651-47A1-CD06-E4B9-F69C2D6DA1EB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[65]" "e[109]";
createNode polyMapCut -n "polyMapCut40";
	rename -uid "FA4C91EC-4FBD-188B-C4F9-9F97F84D9568";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[0]" "e[8]" "e[13]" "e[20]" "e[27]" "e[34]" "e[42]" "e[49]" "e[54]" "e[58]" "e[64]" "e[68]";
createNode polyTweakUV -n "polyTweakUV74";
	rename -uid "3434522A-4A86-124D-E36D-588F728D84D2";
	setAttr ".uopa" yes;
	setAttr -s 41 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" 0.85585487 -0.084069699 ;
	setAttr ".uvtk[4]" -type "float2" 0.8756144 -0.04469803 ;
	setAttr ".uvtk[5]" -type "float2" 0.84836018 -0.044952959 ;
	setAttr ".uvtk[10]" -type "float2" 0.95475662 -0.040267318 ;
	setAttr ".uvtk[33]" -type "float2" 0.81543362 -0.39905971 ;
	setAttr ".uvtk[36]" -type "float2" 0.3593632 -0.39258027 ;
	setAttr ".uvtk[37]" -type "float2" 0.34692392 -0.39831004 ;
	setAttr ".uvtk[40]" -type "float2" 0.85222387 -0.39684436 ;
	setAttr ".uvtk[44]" -type "float2" 0.86095446 0.45134345 ;
	setAttr ".uvtk[47]" -type "float2" 0.80502486 0.4263185 ;
	setAttr ".uvtk[51]" -type "float2" 0.78513271 0.41736603 ;
	setAttr ".uvtk[78]" -type "float2" 0.74079692 0.8227607 ;
	setAttr ".uvtk[82]" -type "float2" 0.74114889 0.82505357 ;
	setAttr ".uvtk[83]" -type "float2" 0.73354596 0.82779884 ;
	setAttr ".uvtk[84]" -type "float2" 0.73250502 0.8268528 ;
	setAttr ".uvtk[87]" -type "float2" 0.73092818 0.82925051 ;
	setAttr ".uvtk[330]" -type "float2" 0.97539198 -0.05541274 ;
	setAttr ".uvtk[523]" -type "float2" 0.86516351 0.47241521 ;
	setAttr ".uvtk[609]" -type "float2" 0.80013514 0.39150015 ;
	setAttr ".uvtk[610]" -type "float2" -0.34599644 -0.39678091 ;
	setAttr ".uvtk[687]" -type "float2" 0.85072649 -0.38280261 ;
	setAttr ".uvtk[690]" -type "float2" 0.79992217 -0.40620357 ;
	setAttr ".uvtk[691]" -type "float2" 0.89065158 -0.077317625 ;
	setAttr ".uvtk[730]" -type "float2" 0.98560947 -0.018003851 ;
	setAttr ".uvtk[731]" -type "float2" 0.77643812 0.44569528 ;
	setAttr ".uvtk[732]" -type "float2" 0.80252546 0.45402789 ;
	setAttr ".uvtk[769]" -type "float2" 0.74523067 0.82340372 ;
	setAttr ".uvtk[771]" -type "float2" 0.73503679 0.82560337 ;
	setAttr ".uvtk[774]" -type "float2" 0.73047441 0.83311039 ;
	setAttr ".uvtk[775]" -type "float2" 0.73445898 0.83042824 ;
	setAttr ".uvtk[776]" -type "float2" 0.74233121 0.82653677 ;
	setAttr ".uvtk[778]" -type "float2" 1.2973404 -0.39305624 ;
	setAttr ".uvtk[779]" -type "float2" 1.0066121 -0.40561056 ;
	setAttr ".uvtk[780]" -type "float2" 0.12332833 -0.40201682 ;
	setAttr ".uvtk[782]" -type "float2" 0.81851512 0.40328133 ;
	setAttr ".uvtk[784]" -type "float2" 0.86939585 0.43619746 ;
	setAttr ".uvtk[785]" -type "float2" 0.88835794 0.45300639 ;
	setAttr ".uvtk[786]" -type "float2" 0.95463204 -0.01990357 ;
	setAttr ".uvtk[788]" -type "float2" 0.88042814 -0.01282087 ;
	setAttr ".uvtk[789]" -type "float2" 0.85386956 -0.0076647103 ;
createNode polyMapSewMove -n "polyMapSewMove64";
	rename -uid "013F1EE2-49A9-4482-8BC2-CF923D34615B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[1]" "e[10]" "e[14]" "e[21]" "e[29]" "e[36]" "e[40]" "e[47]" "e[51]" "e[55]" "e[62]" "e[66]";
createNode polyTweakUV -n "polyTweakUV75";
	rename -uid "EFAADB1C-43C7-BD10-B690-AF83215AA6C7";
	setAttr ".uopa" yes;
	setAttr -s 117 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.041948259 0.053868771 ;
	setAttr ".uvtk[1]" -type "float2" -0.0463714 0.056733549 ;
	setAttr ".uvtk[2]" -type "float2" -0.052994072 0.050572097 ;
	setAttr ".uvtk[3]" -type "float2" -0.049408376 0.047931492 ;
	setAttr ".uvtk[4]" -type "float2" -0.040731311 0.057820857 ;
	setAttr ".uvtk[5]" -type "float2" -0.04495579 0.057590365 ;
	setAttr ".uvtk[6]" -type "float2" -0.058178067 0.04305619 ;
	setAttr ".uvtk[7]" -type "float2" -0.05422169 0.040550053 ;
	setAttr ".uvtk[8]" -type "float2" -0.038032413 0.040388644 ;
	setAttr ".uvtk[9]" -type "float2" -0.034175813 0.048889816 ;
	setAttr ".uvtk[10]" -type "float2" -0.032270193 0.057516396 ;
	setAttr ".uvtk[11]" -type "float2" -0.064402461 0.03434819 ;
	setAttr ".uvtk[12]" -type "float2" -0.059836626 0.031624556 ;
	setAttr ".uvtk[13]" -type "float2" -0.042288065 0.033208549 ;
	setAttr ".uvtk[14]" -type "float2" -0.024913311 0.038137197 ;
	setAttr ".uvtk[15]" -type "float2" -0.019756496 0.04223299 ;
	setAttr ".uvtk[16]" -type "float2" -0.070322275 0.025613964 ;
	setAttr ".uvtk[17]" -type "float2" -0.066391528 0.02334249 ;
	setAttr ".uvtk[18]" -type "float2" -0.048054993 0.024631977 ;
	setAttr ".uvtk[19]" -type "float2" -0.029473722 0.029769361 ;
	setAttr ".uvtk[20]" -type "float2" -0.076793075 0.017177045 ;
	setAttr ".uvtk[21]" -type "float2" -0.072213411 0.014639378 ;
	setAttr ".uvtk[22]" -type "float2" -0.054485023 0.016554475 ;
	setAttr ".uvtk[23]" -type "float2" -0.035396695 0.020781696 ;
	setAttr ".uvtk[24]" -type "float2" -0.085052907 0.0097801089 ;
	setAttr ".uvtk[25]" -type "float2" -0.080446839 0.0074241161 ;
	setAttr ".uvtk[26]" -type "float2" -0.060450912 0.0080862641 ;
	setAttr ".uvtk[27]" -type "float2" -0.041786194 0.012479603 ;
	setAttr ".uvtk[28]" -type "float2" -0.089751899 0.0023810267 ;
	setAttr ".uvtk[29]" -type "float2" -0.08554095 0.00057476759 ;
	setAttr ".uvtk[30]" -type "float2" -0.068195462 0.0010722876 ;
	setAttr ".uvtk[31]" -type "float2" -0.048076928 0.0038814545 ;
	setAttr ".uvtk[32]" -type "float2" -0.060563445 -0.012068361 ;
	setAttr ".uvtk[33]" -type "float2" -0.055799305 -0.014896184 ;
	setAttr ".uvtk[34]" -type "float2" -0.07415992 -0.0049788356 ;
	setAttr ".uvtk[35]" -type "float2" -0.055600166 -0.0037375093 ;
	setAttr ".uvtk[36]" -type "float2" 0.42473421 -0.17298308 ;
	setAttr ".uvtk[37]" -type "float2" 0.42099547 -0.17006695 ;
	setAttr ".uvtk[38]" -type "float2" -0.045853078 -0.021649301 ;
	setAttr ".uvtk[39]" -type "float2" -0.062667668 -0.011366189 ;
	setAttr ".uvtk[40]" -type "float2" -0.014401138 -0.035547763 ;
	setAttr ".uvtk[44]" -type "float2" 0.0050246119 0.010009348 ;
	setAttr ".uvtk[45]" -type "float2" 0.004113555 0.013339788 ;
	setAttr ".uvtk[46]" -type "float2" 0.0043017864 0.012795359 ;
	setAttr ".uvtk[47]" -type "float2" 0.0031585693 0.012147367 ;
	setAttr ".uvtk[48]" -type "float2" 0.0029707551 0.011525452 ;
	setAttr ".uvtk[49]" -type "float2" 0.0032610893 0.0090036392 ;
	setAttr ".uvtk[50]" -type "float2" 0.0037555099 0.011875302 ;
	setAttr ".uvtk[51]" -type "float2" 0.0020654202 0.012905926 ;
	setAttr ".uvtk[52]" -type "float2" 0.008045435 0.01131773 ;
	setAttr ".uvtk[53]" -type "float2" 0.012384415 0.017962635 ;
	setAttr ".uvtk[54]" -type "float2" 0.0011857152 0.007825017 ;
	setAttr ".uvtk[55]" -type "float2" 0.0016496181 0.0058036447 ;
	setAttr ".uvtk[56]" -type "float2" 0.0035335422 0.0079177022 ;
	setAttr ".uvtk[57]" -type "float2" 0.0055376291 0.0084192157 ;
	setAttr ".uvtk[58]" -type "float2" -0.00077527761 0.0035123229 ;
	setAttr ".uvtk[59]" -type "float2" -0.00018078089 0.0017464757 ;
	setAttr ".uvtk[60]" -type "float2" 0.001878798 0.0049487948 ;
	setAttr ".uvtk[61]" -type "float2" 0.0035248399 0.0043391585 ;
	setAttr ".uvtk[62]" -type "float2" -0.002381146 -0.00087451935 ;
	setAttr ".uvtk[63]" -type "float2" -0.0018473268 -0.0025215149 ;
	setAttr ".uvtk[64]" -type "float2" 9.6797943e-05 0.00093597174 ;
	setAttr ".uvtk[65]" -type "float2" 0.0019773245 4.4941902e-05 ;
	setAttr ".uvtk[66]" -type "float2" -0.0042409301 -0.005164206 ;
	setAttr ".uvtk[67]" -type "float2" -0.0035851598 -0.006714046 ;
	setAttr ".uvtk[68]" -type "float2" -0.0016028285 -0.0031642318 ;
	setAttr ".uvtk[69]" -type "float2" 0.00014960766 -0.0040440559 ;
	setAttr ".uvtk[70]" -type "float2" -0.005314827 -0.0098050833 ;
	setAttr ".uvtk[71]" -type "float2" -0.0049613118 -0.011245966 ;
	setAttr ".uvtk[72]" -type "float2" -0.0033074021 -0.007363379 ;
	setAttr ".uvtk[73]" -type "float2" -0.00051289797 -0.0083229542 ;
	setAttr ".uvtk[74]" -type "float2" -0.006593883 -0.013711691 ;
	setAttr ".uvtk[75]" -type "float2" -0.0061378479 -0.014722407 ;
	setAttr ".uvtk[76]" -type "float2" -0.0047219396 -0.011685848 ;
	setAttr ".uvtk[77]" -type "float2" -0.0015788078 -0.012178957 ;
	setAttr ".uvtk[78]" -type "float2" -0.0047590137 -0.01797986 ;
	setAttr ".uvtk[79]" -type "float2" -0.008556962 -0.018184066 ;
	setAttr ".uvtk[80]" -type "float2" -0.0059549212 -0.014877737 ;
	setAttr ".uvtk[81]" -type "float2" -0.00063413382 -0.015774429 ;
	setAttr ".uvtk[82]" -type "float2" -0.0024672151 -0.015104771 ;
	setAttr ".uvtk[83]" -type "float2" -0.0032339692 -0.0085089207 ;
	setAttr ".uvtk[84]" -type "float2" -0.010530472 -0.018036187 ;
	setAttr ".uvtk[87]" -type "float2" -0.0029367208 -0.0087142587 ;
	setAttr ".uvtk[125]" -type "float2" -0.20132658 -0.87258679 ;
	setAttr ".uvtk[127]" -type "float2" -0.17233714 -0.81175619 ;
	setAttr ".uvtk[128]" -type "float2" -0.17707798 -0.81340152 ;
	setAttr ".uvtk[129]" -type "float2" -0.1374732 -0.75608605 ;
	setAttr ".uvtk[130]" -type "float2" -0.14125648 -0.75669259 ;
	setAttr ".uvtk[131]" -type "float2" -0.09515968 -0.69768268 ;
	setAttr ".uvtk[132]" -type "float2" -0.097929507 -0.69713223 ;
	setAttr ".uvtk[133]" -type "float2" -0.040989846 -0.64975548 ;
	setAttr ".uvtk[134]" -type "float2" -0.042734832 -0.64799017 ;
	setAttr ".uvtk[135]" -type "float2" 0.0052399933 -0.59482288 ;
	setAttr ".uvtk[136]" -type "float2" 0.0045271814 -0.59188294 ;
	setAttr ".uvtk[137]" -type "float2" -0.040646464 -0.64982468 ;
	setAttr ".uvtk[138]" -type "float2" -0.09586671 -0.69894069 ;
	setAttr ".uvtk[139]" -type "float2" 0.0066154897 -0.59371758 ;
	setAttr ".uvtk[140]" -type "float2" 0.055974513 -0.54384762 ;
	setAttr ".uvtk[141]" -type "float2" 0.056293696 -0.53973287 ;
	setAttr ".uvtk[142]" -type "float2" 0.10796914 -0.51047891 ;
	setAttr ".uvtk[143]" -type "float2" 0.10914204 -0.50539231 ;
	setAttr ".uvtk[145]" -type "float2" 0.15415868 -0.46417183 ;
	setAttr ".uvtk[607]" -type "float2" 3.8444996e-05 0.012716889 ;
	setAttr ".uvtk[608]" -type "float2" 1.1081002 -0.38377604 ;
	setAttr ".uvtk[685]" -type "float2" -0.24866879 0.030644685 ;
	setAttr ".uvtk[709]" -type "float2" 0.152132 -0.47023025 ;
	setAttr ".uvtk[743]" -type "float2" -0.20699963 -0.87529325 ;
	setAttr ".uvtk[760]" -type "float2" 0.0081058741 -0.0075675249 ;
	setAttr ".uvtk[761]" -type "float2" 0.00041538477 -0.0050634742 ;
	setAttr ".uvtk[762]" -type "float2" -0.00070357323 -0.0059074759 ;
	setAttr ".uvtk[764]" -type "float2" -0.16286248 -0.0080088079 ;
	setAttr ".uvtk[765]" -type "float2" 0.66221845 -0.25082248 ;
	setAttr ".uvtk[767]" -type "float2" 0.0021980405 0.010643154 ;
	setAttr ".uvtk[769]" -type "float2" 0.0080771446 0.0093357265 ;
	setAttr ".uvtk[770]" -type "float2" -0.026697397 0.060544789 ;
	setAttr ".uvtk[772]" -type "float2" -0.041227579 0.064011335 ;
	setAttr ".uvtk[773]" -type "float2" -0.047288656 0.062024891 ;
createNode polyMapSewMove -n "polyMapSewMove65";
	rename -uid "2A533007-4680-9158-D5F8-DF901C2B26A9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[179:180]" "e[182]" "e[196]" "e[199]" "e[227]" "e[229]" "e[258]" "e[260]" "e[267]";
createNode polyTweakUV -n "polyTweakUV76";
	rename -uid "5925E65E-4C61-A14F-F640-51ACCB3661C3";
	setAttr ".uopa" yes;
	setAttr -s 26 ".uvtk";
	setAttr ".uvtk[91]" -type "float2" 0.2089594 -0.85076147 ;
	setAttr ".uvtk[92]" -type "float2" 0.16089141 -0.80268908 ;
	setAttr ".uvtk[93]" -type "float2" 0.16412783 -0.80619287 ;
	setAttr ".uvtk[94]" -type "float2" 0.10850555 -0.76594877 ;
	setAttr ".uvtk[95]" -type "float2" 0.11089998 -0.76841062 ;
	setAttr ".uvtk[120]" -type "float2" -0.15119922 -0.4933399 ;
	setAttr ".uvtk[121]" -type "float2" -0.12060332 -0.52384603 ;
	setAttr ".uvtk[513]" -type "float2" 0.0096700191 -0.53801358 ;
	setAttr ".uvtk[514]" -type "float2" 0.010867834 -0.5385654 ;
	setAttr ".uvtk[515]" -type "float2" -0.036797523 -0.49574637 ;
	setAttr ".uvtk[516]" -type "float2" -0.039018154 -0.49427572 ;
	setAttr ".uvtk[517]" -type "float2" -0.079836726 -0.45708445 ;
	setAttr ".uvtk[518]" -type "float2" -0.082994223 -0.45477086 ;
	setAttr ".uvtk[519]" -type "float2" 0.11310604 -0.44751695 ;
	setAttr ".uvtk[520]" -type "float2" 0.11395791 -0.44902435 ;
	setAttr ".uvtk[521]" -type "float2" 0.1627734 -0.50038928 ;
	setAttr ".uvtk[522]" -type "float2" 0.16088095 -0.49778682 ;
	setAttr ".uvtk[523]" -type "float2" 0.11365274 -0.44931439 ;
	setAttr ".uvtk[524]" -type "float2" 0.16246822 -0.50067931 ;
	setAttr ".uvtk[672]" -type "float2" -0.12295407 -0.52231473 ;
	setAttr ".uvtk[673]" -type "float2" 0.11065525 -0.76860839 ;
	setAttr ".uvtk[674]" -type "float2" 0.16388309 -0.80639064 ;
	setAttr ".uvtk[675]" -type "float2" 0.2087146 -0.8509593 ;
	setAttr ".uvtk[698]" -type "float2" 0.20488071 -0.84621572 ;
	setAttr ".uvtk[745]" -type "float2" -0.15434122 -0.49136826 ;
createNode polyMapSewMove -n "polyMapSewMove66";
	rename -uid "3976B771-4FDE-F409-21D6-A79492908963";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[517]" "e[587]" "e[591]" "e[661]" "e[666]" "e[715]" "e[726]" "e[735]";
createNode polyTweakUV -n "polyTweakUV77";
	rename -uid "C4E27D03-4284-07A9-DBBD-209ABC2C8C51";
	setAttr ".uopa" yes;
	setAttr -s 25 ".uvtk";
	setAttr ".uvtk[96]" -type "float2" 0.20720983 -0.80775446 ;
	setAttr ".uvtk[97]" -type "float2" 0.16275251 -0.77383929 ;
	setAttr ".uvtk[98]" -type "float2" 0.1117143 -0.74684417 ;
	setAttr ".uvtk[99]" -type "float2" 0.20564675 -0.80939811 ;
	setAttr ".uvtk[100]" -type "float2" 0.16118944 -0.77548295 ;
	setAttr ".uvtk[101]" -type "float2" 0.11015123 -0.74848783 ;
	setAttr ".uvtk[102]" -type "float2" 0.060201168 -0.70351076 ;
	setAttr ".uvtk[103]" -type "float2" 0.058638096 -0.70515442 ;
	setAttr ".uvtk[104]" -type "float2" 0.012473464 -0.65619701 ;
	setAttr ".uvtk[105]" -type "float2" 0.010910392 -0.65784061 ;
	setAttr ".uvtk[106]" -type "float2" -0.041924655 -0.61592811 ;
	setAttr ".uvtk[107]" -type "float2" -0.043505013 -0.61755902 ;
	setAttr ".uvtk[108]" -type "float2" 0.012665212 -0.65599531 ;
	setAttr ".uvtk[109]" -type "float2" 0.060393095 -0.70330906 ;
	setAttr ".uvtk[110]" -type "float2" -0.041750073 -0.61571366 ;
	setAttr ".uvtk[111]" -type "float2" -0.086364388 -0.56512582 ;
	setAttr ".uvtk[112]" -type "float2" -0.087927997 -0.56676996 ;
	setAttr ".uvtk[113]" -type "float2" -0.086172938 -0.56492454 ;
	setAttr ".uvtk[114]" -type "float2" -0.12368029 -0.51630008 ;
	setAttr ".uvtk[115]" -type "float2" -0.12524343 -0.51794374 ;
	setAttr ".uvtk[116]" -type "float2" -0.12348843 -0.51609832 ;
	setAttr ".uvtk[117]" -type "float2" -0.15606135 -0.46228501 ;
	setAttr ".uvtk[118]" -type "float2" -0.15762454 -0.4639287 ;
	setAttr ".uvtk[119]" -type "float2" -0.15586948 -0.46208325 ;
createNode polyMapSewMove -n "polyMapSewMove67";
	rename -uid "2A03EE9D-4D07-D526-364C-FD87A5AFA0C7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[511]" "e[583]" "e[659]" "e[1113]" "e[1129]" "e[1134]" "e[1140]" "e[1143]";
createNode polyMapSewMove -n "polyMapSewMove68";
	rename -uid "E4807551-456F-082E-51C1-6492AD3B7F1C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[518]" "e[590]" "e[734]";
createNode polyTweakUV -n "polyTweakUV78";
	rename -uid "87C50E5E-4B1E-3474-10BF-44BB0E7FF877";
	setAttr ".uopa" yes;
	setAttr -s 302 ".uvtk";
	setAttr ".uvtk[88]" -type "float2" 0.93496037 -0.46600381 ;
	setAttr ".uvtk[89]" -type "float2" 0.93496794 -0.46599004 ;
	setAttr ".uvtk[90]" -type "float2" 0.93383223 -0.46494624 ;
	setAttr ".uvtk[138]" -type "float2" 0.9349066 -0.4656705 ;
	setAttr ".uvtk[187]" -type "float2" 0.93417978 -0.4537676 ;
	setAttr ".uvtk[206]" -type "float2" 0.9341718 -0.45951396 ;
	setAttr ".uvtk[208]" -type "float2" 0.93491983 -0.45953751 ;
	setAttr ".uvtk[209]" -type "float2" 0.93492371 -0.45377815 ;
	setAttr ".uvtk[224]" -type "float2" 0.9341718 -0.45952737 ;
	setAttr ".uvtk[225]" -type "float2" 0.93494177 -0.4596628 ;
	setAttr ".uvtk[226]" -type "float2" 0.93494582 -0.45365283 ;
	setAttr ".uvtk[241]" -type "float2" 0.93494666 -0.4596765 ;
	setAttr ".uvtk[242]" -type "float2" 0.93495077 -0.45363909 ;
	setAttr ".uvtk[243]" -type "float2" 0.93417996 -0.45361841 ;
	setAttr ".uvtk[254]" -type "float2" 0.93417162 -0.45966315 ;
	setAttr ".uvtk[255]" -type "float2" 0.93494177 -0.45969024 ;
	setAttr ".uvtk[256]" -type "float2" 0.93494588 -0.45362538 ;
	setAttr ".uvtk[267]" -type "float2" 0.93491966 -0.45981559 ;
	setAttr ".uvtk[268]" -type "float2" 0.93492395 -0.45350009 ;
	setAttr ".uvtk[269]" -type "float2" 0.93418133 -0.44831839 ;
	setAttr ".uvtk[279]" -type "float2" 0.93493015 -0.44819781 ;
	setAttr ".uvtk[290]" -type "float2" 0.93495232 -0.44807246 ;
	setAttr ".uvtk[291]" -type "float2" 0.93418133 -0.44818261 ;
	setAttr ".uvtk[299]" -type "float2" 0.93495721 -0.44805875 ;
	setAttr ".uvtk[300]" -type "float2" 0.93418133 -0.4481692 ;
	setAttr ".uvtk[306]" -type "float2" 0.93495232 -0.44804502 ;
	setAttr ".uvtk[310]" -type "float2" 0.93493044 -0.44791973 ;
	setAttr ".uvtk[312]" -type "float2" 0.93493485 -0.44533533 ;
	setAttr ".uvtk[313]" -type "float2" 0.39920515 -0.46614316 ;
	setAttr ".uvtk[314]" -type "float2" 0.39920515 -0.47203791 ;
	setAttr ".uvtk[315]" -type "float2" 0.40157032 -0.47203791 ;
	setAttr ".uvtk[316]" -type "float2" 0.40157032 -0.46614316 ;
	setAttr ".uvtk[317]" -type "float2" 0.39683992 -0.46614316 ;
	setAttr ".uvtk[318]" -type "float2" 0.39683992 -0.47203791 ;
	setAttr ".uvtk[319]" -type "float2" 0.39920592 -0.47216618 ;
	setAttr ".uvtk[320]" -type "float2" 0.40157032 -0.47216618 ;
	setAttr ".uvtk[321]" -type "float2" 0.40393555 -0.47203791 ;
	setAttr ".uvtk[322]" -type "float2" 0.40393555 -0.46614316 ;
	setAttr ".uvtk[323]" -type "float2" 0.39470363 -0.46614316 ;
	setAttr ".uvtk[324]" -type "float2" 0.39470363 -0.47203791 ;
	setAttr ".uvtk[325]" -type "float2" 0.39683992 -0.47216618 ;
	setAttr ".uvtk[326]" -type "float2" 0.39920515 -0.47218025 ;
	setAttr ".uvtk[327]" -type "float2" 0.40157032 -0.47218025 ;
	setAttr ".uvtk[328]" -type "float2" 0.40393555 -0.47216618 ;
	setAttr ".uvtk[329]" -type "float2" 0.40630078 -0.47203791 ;
	setAttr ".uvtk[330]" -type "float2" 0.40630078 -0.46614316 ;
	setAttr ".uvtk[332]" -type "float2" 0.39256734 -0.46614316 ;
	setAttr ".uvtk[333]" -type "float2" 0.39470363 -0.47216618 ;
	setAttr ".uvtk[334]" -type "float2" 0.39683992 -0.47218025 ;
	setAttr ".uvtk[335]" -type "float2" 0.39920592 -0.47219428 ;
	setAttr ".uvtk[336]" -type "float2" 0.40157032 -0.47219428 ;
	setAttr ".uvtk[337]" -type "float2" 0.40393555 -0.47218025 ;
	setAttr ".uvtk[338]" -type "float2" 0.40630078 -0.47216618 ;
	setAttr ".uvtk[339]" -type "float2" 0.40825754 -0.47203791 ;
	setAttr ".uvtk[340]" -type "float2" 0.40825754 -0.46614316 ;
	setAttr ".uvtk[341]" -type "float2" 0.39256734 -0.47216618 ;
	setAttr ".uvtk[344]" -type "float2" 0.39470363 -0.47218025 ;
	setAttr ".uvtk[345]" -type "float2" 0.39683992 -0.47219428 ;
	setAttr ".uvtk[346]" -type "float2" 0.39920515 -0.47232255 ;
	setAttr ".uvtk[347]" -type "float2" 0.40157032 -0.47232255 ;
	setAttr ".uvtk[348]" -type "float2" 0.40393555 -0.47219428 ;
	setAttr ".uvtk[349]" -type "float2" 0.40630078 -0.47218025 ;
	setAttr ".uvtk[350]" -type "float2" 0.40825754 -0.47216618 ;
	setAttr ".uvtk[351]" -type "float2" 0.93414533 -0.46614611 ;
	setAttr ".uvtk[352]" -type "float2" 0.4102143 -0.47203791 ;
	setAttr ".uvtk[354]" -type "float2" 0.39256734 -0.47218025 ;
	setAttr ".uvtk[357]" -type "float2" 0.39470363 -0.47219428 ;
	setAttr ".uvtk[358]" -type "float2" 0.39683992 -0.47232255 ;
	setAttr ".uvtk[359]" -type "float2" 0.39920515 -0.4782173 ;
	setAttr ".uvtk[360]" -type "float2" 0.40157032 -0.4782173 ;
	setAttr ".uvtk[361]" -type "float2" 0.40393555 -0.47232255 ;
	setAttr ".uvtk[362]" -type "float2" 0.40630078 -0.47219428 ;
	setAttr ".uvtk[363]" -type "float2" 0.40825754 -0.47218025 ;
	setAttr ".uvtk[364]" -type "float2" 0.93414432 -0.47216904 ;
	setAttr ".uvtk[365]" -type "float2" 0.93492478 -0.47204098 ;
	setAttr ".uvtk[366]" -type "float2" 0.93492526 -0.46614626 ;
	setAttr ".uvtk[370]" -type "float2" 0.39470363 -0.47232255 ;
	setAttr ".uvtk[371]" -type "float2" 0.39683992 -0.47821727 ;
	setAttr ".uvtk[372]" -type "float2" 0.40157032 -0.47834554 ;
	setAttr ".uvtk[373]" -type "float2" 0.39920592 -0.4783456 ;
	setAttr ".uvtk[374]" -type "float2" 0.40393555 -0.4782173 ;
	setAttr ".uvtk[375]" -type "float2" 0.40630078 -0.47232255 ;
	setAttr ".uvtk[376]" -type "float2" 0.40825754 -0.47219428 ;
	setAttr ".uvtk[377]" -type "float2" 0.93414432 -0.47218317 ;
	setAttr ".uvtk[378]" -type "float2" 0.93494594 -0.47216919 ;
	setAttr ".uvtk[379]" -type "float2" 0.93494648 -0.46601799 ;
	setAttr ".uvtk[380]" -type "float2" 0.93414539 -0.46601787 ;
	setAttr ".uvtk[382]" -type "float2" 0.39256734 -0.47232255 ;
	setAttr ".uvtk[383]" -type "float2" 0.39470363 -0.4782173 ;
	setAttr ".uvtk[384]" -type "float2" 0.39683992 -0.47834563 ;
	setAttr ".uvtk[385]" -type "float2" 0.40393555 -0.47834557 ;
	setAttr ".uvtk[386]" -type "float2" 0.40157032 -0.47835961 ;
	setAttr ".uvtk[387]" -type "float2" 0.39920515 -0.47835961 ;
	setAttr ".uvtk[388]" -type "float2" 0.40630078 -0.4782173 ;
	setAttr ".uvtk[389]" -type "float2" 0.40825754 -0.47232255 ;
	setAttr ".uvtk[390]" -type "float2" 0.4102143 -0.47219428 ;
	setAttr ".uvtk[391]" -type "float2" 0.93495083 -0.47218326 ;
	setAttr ".uvtk[392]" -type "float2" 0.93450594 -0.46485925 ;
	setAttr ".uvtk[395]" -type "float2" 0.39470363 -0.4783456 ;
	setAttr ".uvtk[396]" -type "float2" 0.39683992 -0.47835961 ;
	setAttr ".uvtk[397]" -type "float2" 0.40393555 -0.47835961 ;
	setAttr ".uvtk[398]" -type "float2" 0.40630078 -0.4783456 ;
	setAttr ".uvtk[399]" -type "float2" 0.40157032 -0.47837365 ;
	setAttr ".uvtk[400]" -type "float2" 0.39920592 -0.47837371 ;
	setAttr ".uvtk[401]" -type "float2" 0.40825754 -0.4782173 ;
	setAttr ".uvtk[402]" -type "float2" 0.93414432 -0.47232544 ;
	setAttr ".uvtk[403]" -type "float2" 0.93494594 -0.47219732 ;
	setAttr ".uvtk[405]" -type "float2" 0.39256734 -0.47834554 ;
	setAttr ".uvtk[406]" -type "float2" 0.39470363 -0.47835961 ;
	setAttr ".uvtk[407]" -type "float2" 0.39683992 -0.47837377 ;
	setAttr ".uvtk[408]" -type "float2" 0.40393555 -0.47837368 ;
	setAttr ".uvtk[409]" -type "float2" 0.40630078 -0.47835961 ;
	setAttr ".uvtk[410]" -type "float2" 0.40825754 -0.47834557 ;
	setAttr ".uvtk[411]" -type "float2" 0.40157032 -0.47850192 ;
	setAttr ".uvtk[412]" -type "float2" 0.39920515 -0.47850192 ;
	setAttr ".uvtk[413]" -type "float2" 0.4102143 -0.4782173 ;
	setAttr ".uvtk[414]" -type "float2" 0.93492472 -0.47232559 ;
	setAttr ".uvtk[416]" -type "float2" 0.39256734 -0.47835961 ;
	setAttr ".uvtk[417]" -type "float2" 0.39470351 -0.47837371 ;
	setAttr ".uvtk[418]" -type "float2" 0.39683992 -0.47850189 ;
	setAttr ".uvtk[419]" -type "float2" 0.40393555 -0.47850192 ;
	setAttr ".uvtk[420]" -type "float2" 0.40630078 -0.47837371 ;
	setAttr ".uvtk[421]" -type "float2" 0.40825754 -0.47835961 ;
	setAttr ".uvtk[422]" -type "float2" 0.9341433 -0.47834843 ;
	setAttr ".uvtk[423]" -type "float2" 0.39920515 -0.483917 ;
	setAttr ".uvtk[424]" -type "float2" 0.40157032 -0.483917 ;
	setAttr ".uvtk[425]" -type "float2" 0.93492424 -0.47822025 ;
	setAttr ".uvtk[428]" -type "float2" 0.39470273 -0.47850192 ;
	setAttr ".uvtk[429]" -type "float2" 0.39683992 -0.48391703 ;
	setAttr ".uvtk[430]" -type "float2" 0.40393555 -0.483917 ;
	setAttr ".uvtk[431]" -type "float2" 0.40630078 -0.47850192 ;
	setAttr ".uvtk[432]" -type "float2" 0.40825754 -0.47837371 ;
	setAttr ".uvtk[433]" -type "float2" 0.4102143 -0.47835961 ;
	setAttr ".uvtk[434]" -type "float2" 0.9349454 -0.47834852 ;
	setAttr ".uvtk[435]" -type "float2" 0.39920551 -0.4840453 ;
	setAttr ".uvtk[436]" -type "float2" 0.40157032 -0.48404521 ;
	setAttr ".uvtk[438]" -type "float2" 0.39256549 -0.47850192 ;
	setAttr ".uvtk[439]" -type "float2" 0.39466864 -0.48391551 ;
	setAttr ".uvtk[440]" -type "float2" 0.39683992 -0.48404542 ;
	setAttr ".uvtk[441]" -type "float2" 0.40393555 -0.48404527 ;
	setAttr ".uvtk[442]" -type "float2" 0.40630078 -0.483917 ;
	setAttr ".uvtk[443]" -type "float2" 0.40825748 -0.47850192 ;
	setAttr ".uvtk[444]" -type "float2" 0.4102143 -0.47837368 ;
	setAttr ".uvtk[445]" -type "float2" 0.93495029 -0.47836256 ;
	setAttr ".uvtk[446]" -type "float2" 0.39920515 -0.4840593 ;
	setAttr ".uvtk[447]" -type "float2" 0.40157032 -0.4840593 ;
	setAttr ".uvtk[450]" -type "float2" 0.3946678 -0.48404384 ;
	setAttr ".uvtk[451]" -type "float2" 0.39683992 -0.4840593 ;
	setAttr ".uvtk[452]" -type "float2" 0.40393555 -0.4840593 ;
	setAttr ".uvtk[453]" -type "float2" 0.40630078 -0.48404533 ;
	setAttr ".uvtk[454]" -type "float2" 0.40825438 -0.48392293 ;
	setAttr ".uvtk[455]" -type "float2" 0.93414313 -0.47850472 ;
	setAttr ".uvtk[456]" -type "float2" 0.9349454 -0.47837666 ;
	setAttr ".uvtk[457]" -type "float2" 0.39920557 -0.4840734 ;
	setAttr ".uvtk[458]" -type "float2" 0.40157032 -0.48407331 ;
	setAttr ".uvtk[460]" -type "float2" 0.39249569 -0.48404223 ;
	setAttr ".uvtk[461]" -type "float2" 0.39466774 -0.48405778 ;
	setAttr ".uvtk[462]" -type "float2" 0.39683992 -0.48407352 ;
	setAttr ".uvtk[463]" -type "float2" 0.40393555 -0.48407337 ;
	setAttr ".uvtk[464]" -type "float2" 0.40630078 -0.4840593 ;
	setAttr ".uvtk[465]" -type "float2" 0.40825433 -0.48405123 ;
	setAttr ".uvtk[466]" -type "float2" 0.41020805 -0.48392886 ;
	setAttr ".uvtk[467]" -type "float2" 0.9349243 -0.47850484 ;
	setAttr ".uvtk[468]" -type "float2" 0.39920515 -0.48420161 ;
	setAttr ".uvtk[469]" -type "float2" 0.40157032 -0.48420161 ;
	setAttr ".uvtk[471]" -type "float2" 0.39249551 -0.48405629 ;
	setAttr ".uvtk[472]" -type "float2" 0.39466757 -0.48407194 ;
	setAttr ".uvtk[473]" -type "float2" 0.39683992 -0.48420155 ;
	setAttr ".uvtk[474]" -type "float2" 0.40393555 -0.48420161 ;
	setAttr ".uvtk[475]" -type "float2" 0.40630078 -0.48407343 ;
	setAttr ".uvtk[476]" -type "float2" 0.40825433 -0.48406523 ;
	setAttr ".uvtk[477]" -type "float2" 0.93413591 -0.48405987 ;
	setAttr ".uvtk[478]" -type "float2" 0.93492639 -0.48393175 ;
	setAttr ".uvtk[479]" -type "float2" 0.40157032 -0.48683783 ;
	setAttr ".uvtk[480]" -type "float2" 0.39920515 -0.48683783 ;
	setAttr ".uvtk[483]" -type "float2" 0.39466679 -0.48420009 ;
	setAttr ".uvtk[484]" -type "float2" 0.39683992 -0.48683783 ;
	setAttr ".uvtk[485]" -type "float2" 0.40393555 -0.48683783 ;
	setAttr ".uvtk[486]" -type "float2" 0.40630078 -0.48420161 ;
	setAttr ".uvtk[487]" -type "float2" 0.40825433 -0.48407933 ;
	setAttr ".uvtk[488]" -type "float2" 0.93413591 -0.484074 ;
	setAttr ".uvtk[489]" -type "float2" 0.93494761 -0.48405999 ;
	setAttr ".uvtk[491]" -type "float2" 0.39249367 -0.4841986 ;
	setAttr ".uvtk[492]" -type "float2" 0.3946318 -0.48683518 ;
	setAttr ".uvtk[493]" -type "float2" 0.40630078 -0.48683783 ;
	setAttr ".uvtk[494]" -type "float2" 0.40825427 -0.48420754 ;
	setAttr ".uvtk[495]" -type "float2" 0.41020787 -0.48408523 ;
	setAttr ".uvtk[496]" -type "float2" 0.93495256 -0.48407412 ;
	setAttr ".uvtk[498]" -type "float2" 0.39242363 -0.48683253 ;
	setAttr ".uvtk[499]" -type "float2" 0.40825111 -0.48684824 ;
	setAttr ".uvtk[500]" -type "float2" 0.93413574 -0.48421624 ;
	setAttr ".uvtk[501]" -type "float2" 0.93494767 -0.48408818 ;
	setAttr ".uvtk[502]" -type "float2" 0.93412936 -0.48692822 ;
	setAttr ".uvtk[503]" -type "float2" 0.93492651 -0.48421636 ;
	setAttr ".uvtk[504]" -type "float2" 0.93491358 -0.48681724 ;
	setAttr ".uvtk[507]" -type "float2" 0.93679851 -0.44517249 ;
	setAttr ".uvtk[508]" -type "float2" 0.93747753 -0.44517893 ;
	setAttr ".uvtk[509]" -type "float2" 0.93679851 -0.44780639 ;
	setAttr ".uvtk[510]" -type "float2" 0.93747753 -0.44781005 ;
	setAttr ".uvtk[511]" -type "float2" 0.93671978 -0.44793466 ;
	setAttr ".uvtk[512]" -type "float2" 0.93739873 -0.44793826 ;
	setAttr ".uvtk[513]" -type "float2" 0.93672961 -0.44794869 ;
	setAttr ".uvtk[514]" -type "float2" 0.93740857 -0.44795236 ;
	setAttr ".uvtk[515]" -type "float2" 0.93671978 -0.44796276 ;
	setAttr ".uvtk[516]" -type "float2" 0.93739879 -0.4479664 ;
	setAttr ".uvtk[517]" -type "float2" 0.93679851 -0.44809103 ;
	setAttr ".uvtk[518]" -type "float2" 0.93747753 -0.44809467 ;
	setAttr ".uvtk[519]" -type "float2" 0.93679851 -0.45350301 ;
	setAttr ".uvtk[520]" -type "float2" 0.93747753 -0.45350301 ;
	setAttr ".uvtk[521]" -type "float2" 0.93671978 -0.45363128 ;
	setAttr ".uvtk[522]" -type "float2" 0.93739873 -0.45363128 ;
	setAttr ".uvtk[523]" -type "float2" 0.93672961 -0.45364532 ;
	setAttr ".uvtk[524]" -type "float2" 0.93740857 -0.45364532 ;
	setAttr ".uvtk[525]" -type "float2" 0.93671978 -0.45365939 ;
	setAttr ".uvtk[526]" -type "float2" 0.93739873 -0.45365939 ;
	setAttr ".uvtk[527]" -type "float2" 0.93679851 -0.45378762 ;
	setAttr ".uvtk[528]" -type "float2" 0.93747753 -0.45378762 ;
	setAttr ".uvtk[529]" -type "float2" 0.93679851 -0.45968232 ;
	setAttr ".uvtk[530]" -type "float2" 0.93747753 -0.45968232 ;
	setAttr ".uvtk[531]" -type "float2" 0.93671978 -0.45981058 ;
	setAttr ".uvtk[532]" -type "float2" 0.93739873 -0.45981058 ;
	setAttr ".uvtk[533]" -type "float2" 0.93672961 -0.45982465 ;
	setAttr ".uvtk[534]" -type "float2" 0.93740857 -0.45982465 ;
	setAttr ".uvtk[535]" -type "float2" 0.93671978 -0.45983869 ;
	setAttr ".uvtk[536]" -type "float2" 0.93739873 -0.45983869 ;
	setAttr ".uvtk[537]" -type "float2" 0.93679851 -0.45996696 ;
	setAttr ".uvtk[538]" -type "float2" 0.93747753 -0.45996696 ;
	setAttr ".uvtk[539]" -type "float2" 0.93679851 -0.46586165 ;
	setAttr ".uvtk[540]" -type "float2" 0.93747753 -0.46586165 ;
	setAttr ".uvtk[541]" -type "float2" 0.93671978 -0.46598989 ;
	setAttr ".uvtk[542]" -type "float2" 0.93739873 -0.46598989 ;
	setAttr ".uvtk[543]" -type "float2" 0.93672961 -0.46600395 ;
	setAttr ".uvtk[544]" -type "float2" 0.93740857 -0.46600395 ;
	setAttr ".uvtk[545]" -type "float2" 0.93671978 -0.46601799 ;
	setAttr ".uvtk[546]" -type "float2" 0.93739873 -0.46601799 ;
	setAttr ".uvtk[547]" -type "float2" 0.93679851 -0.46614626 ;
	setAttr ".uvtk[548]" -type "float2" 0.93747753 -0.46614626 ;
	setAttr ".uvtk[549]" -type "float2" 0.93679851 -0.47204092 ;
	setAttr ".uvtk[550]" -type "float2" 0.93747753 -0.47204092 ;
	setAttr ".uvtk[551]" -type "float2" 0.93671978 -0.47216919 ;
	setAttr ".uvtk[552]" -type "float2" 0.93739873 -0.47216919 ;
	setAttr ".uvtk[553]" -type "float2" 0.93672961 -0.47218323 ;
	setAttr ".uvtk[554]" -type "float2" 0.93740857 -0.47218323 ;
	setAttr ".uvtk[555]" -type "float2" 0.93671978 -0.47219732 ;
	setAttr ".uvtk[556]" -type "float2" 0.93739873 -0.47219732 ;
	setAttr ".uvtk[557]" -type "float2" 0.93679851 -0.47232559 ;
	setAttr ".uvtk[558]" -type "float2" 0.93747753 -0.47232559 ;
	setAttr ".uvtk[559]" -type "float2" 0.93679851 -0.47822022 ;
	setAttr ".uvtk[560]" -type "float2" 0.93747753 -0.47822022 ;
	setAttr ".uvtk[561]" -type "float2" 0.93671978 -0.47834849 ;
	setAttr ".uvtk[562]" -type "float2" 0.93739873 -0.47834849 ;
	setAttr ".uvtk[563]" -type "float2" 0.93672961 -0.47836256 ;
	setAttr ".uvtk[564]" -type "float2" 0.93740857 -0.47836256 ;
	setAttr ".uvtk[565]" -type "float2" 0.93671978 -0.47837669 ;
	setAttr ".uvtk[566]" -type "float2" 0.93739873 -0.47837669 ;
	setAttr ".uvtk[567]" -type "float2" 0.93679851 -0.4785049 ;
	setAttr ".uvtk[568]" -type "float2" 0.93747753 -0.4785049 ;
	setAttr ".uvtk[569]" -type "float2" 0.93679851 -0.48391694 ;
	setAttr ".uvtk[570]" -type "float2" 0.93747753 -0.48391318 ;
	setAttr ".uvtk[571]" -type "float2" 0.93671978 -0.48404512 ;
	setAttr ".uvtk[572]" -type "float2" 0.93739879 -0.48404154 ;
	setAttr ".uvtk[573]" -type "float2" 0.93672961 -0.48405915 ;
	setAttr ".uvtk[574]" -type "float2" 0.93740857 -0.48405555 ;
	setAttr ".uvtk[575]" -type "float2" 0.93671978 -0.48407325 ;
	setAttr ".uvtk[576]" -type "float2" 0.93739873 -0.48406962 ;
	setAttr ".uvtk[577]" -type "float2" 0.93679851 -0.48420152 ;
	setAttr ".uvtk[578]" -type "float2" 0.93747753 -0.48419785 ;
	setAttr ".uvtk[579]" -type "float2" 0.93680757 -0.48683712 ;
	setAttr ".uvtk[655]" -type "float2" 0.93418133 -0.44819599 ;
	setAttr ".uvtk[657]" -type "float2" 0.93418139 -0.44804683 ;
	setAttr ".uvtk[658]" -type "float2" 0.41020769 -0.48421347 ;
	setAttr ".uvtk[659]" -type "float2" 0.93413591 -0.48408803 ;
	setAttr ".uvtk[660]" -type "float2" 0.41020787 -0.48405713 ;
	setAttr ".uvtk[661]" -type "float2" 0.93413609 -0.4839316 ;
	setAttr ".uvtk[662]" -type "float2" 0.4102143 -0.47232255 ;
	setAttr ".uvtk[663]" -type "float2" 0.93414432 -0.47219723 ;
	setAttr ".uvtk[664]" -type "float2" 0.4102143 -0.47216618 ;
	setAttr ".uvtk[665]" -type "float2" 0.93414438 -0.47204077 ;
	setAttr ".uvtk[667]" -type "float2" 0.9341718 -0.45954078 ;
	setAttr ".uvtk[669]" -type "float2" 0.93417197 -0.45939159 ;
	setAttr ".uvtk[670]" -type "float2" 0.9341433 -0.47822016 ;
	setAttr ".uvtk[671]" -type "float2" 0.4102143 -0.47834557 ;
	setAttr ".uvtk[672]" -type "float2" 0.93414325 -0.47837651 ;
	setAttr ".uvtk[673]" -type "float2" 0.41021413 -0.47850192 ;
	setAttr ".uvtk[674]" -type "float2" 0.93338895 -0.4656707 ;
	setAttr ".uvtk[676]" -type "float2" 0.4102143 -0.46614316 ;
	setAttr ".uvtk[677]" -type "float2" 0.93417996 -0.45349604 ;
	setAttr ".uvtk[679]" -type "float2" 0.9341799 -0.45364523 ;
	setAttr ".uvtk[682]" -type "float2" 0.41020787 -0.48407117 ;
	setAttr ".uvtk[683]" -type "float2" 0.4102143 -0.47218025 ;
	setAttr ".uvtk[685]" -type "float2" 0.9341433 -0.47836244 ;
	setAttr ".uvtk[686]" -type "float2" 0.93417996 -0.45363182 ;
	setAttr ".uvtk[687]" -type "float2" 0.41020143 -0.48685864 ;
	setAttr ".uvtk[693]" -type "float2" 0.39249533 -0.48407033 ;
	setAttr ".uvtk[695]" -type "float2" 0.39249724 -0.48391399 ;
	setAttr ".uvtk[697]" -type "float2" 0.39256734 -0.47219428 ;
	setAttr ".uvtk[699]" -type "float2" 0.39256734 -0.47203791 ;
	setAttr ".uvtk[708]" -type "float2" 0.39256716 -0.47837368 ;
	setAttr ".uvtk[710]" -type "float2" 0.39256734 -0.4782173 ;
	setAttr ".uvtk[730]" -type "float2" 0.93748981 -0.48684713 ;
	setAttr ".uvtk[733]" -type "float2" 0.93417889 -0.44552317 ;
createNode polyMapSewMove -n "polyMapSewMove69";
	rename -uid "DD9205F1-408C-BA2D-C5D9-8AAE00610CB3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 32 "e[395]" "e[407]" "e[414]" "e[425]" "e[450]" "e[463]" "e[476]" "e[489]" "e[553]" "e[557]" "e[584]" "e[588]" "e[626]" "e[630]" "e[771]" "e[774]" "e[819]" "e[822]" "e[867]" "e[870]" "e[915]" "e[918]" "e[944]" "e[946]" "e[952]" "e[954]" "e[966]" "e[968]" "e[1035:1036]" "e[1059:1060]" "e[1083:1084]" "e[1107:1108]";
createNode polyMapSewMove -n "polyMapSewMove70";
	rename -uid "9715961C-4DFE-F2F4-2A57-D9A391C9D7B0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[178]" "e[181]" "e[183]" "e[197:198]" "e[228]" "e[261]" "e[266]";
createNode polyTweakUV -n "polyTweakUV79";
	rename -uid "939B6353-4354-FA20-745C-6DA1D77F1EC2";
	setAttr ".uopa" yes;
	setAttr -s 149 ".uvtk";
	setAttr ".uvtk[41]" -type "float2" 0.40106541 -0.44501457 ;
	setAttr ".uvtk[42]" -type "float2" 0.39936748 -0.44507208 ;
	setAttr ".uvtk[43]" -type "float2" 0.40064538 -0.44504377 ;
	setAttr ".uvtk[85]" -type "float2" 0.40189976 -0.48661894 ;
	setAttr ".uvtk[86]" -type "float2" 0.40030774 -0.4867827 ;
	setAttr ".uvtk[122]" -type "float2" 0.40139538 -0.46584985 ;
	setAttr ".uvtk[123]" -type "float2" 0.40140235 -0.46586394 ;
	setAttr ".uvtk[124]" -type "float2" 0.40145141 -0.46572018 ;
	setAttr ".uvtk[125]" -type "float2" 0.40314251 -0.46588352 ;
	setAttr ".uvtk[177]" -type "float2" 0.40221626 -0.45951566 ;
	setAttr ".uvtk[196]" -type "float2" 0.40205443 -0.45349249 ;
	setAttr ".uvtk[197]" -type "float2" 0.40127701 -0.4595409 ;
	setAttr ".uvtk[198]" -type "float2" 0.40112054 -0.45364591 ;
	setAttr ".uvtk[199]" -type "float2" 0.40125924 -0.45966971 ;
	setAttr ".uvtk[218]" -type "float2" 0.40109593 -0.45351824 ;
	setAttr ".uvtk[219]" -type "float2" 0.40125465 -0.4596839 ;
	setAttr ".uvtk[220]" -type "float2" 0.40222049 -0.45967203 ;
	setAttr ".uvtk[236]" -type "float2" 0.40109062 -0.45350429 ;
	setAttr ".uvtk[237]" -type "float2" 0.40125996 -0.45969781 ;
	setAttr ".uvtk[251]" -type "float2" 0.40204847 -0.45333621 ;
	setAttr ".uvtk[252]" -type "float2" 0.40109509 -0.45349011 ;
	setAttr ".uvtk[253]" -type "float2" 0.40128458 -0.45982555 ;
	setAttr ".uvtk[266]" -type "float2" 0.4011125 -0.45336136 ;
	setAttr ".uvtk[273]" -type "float2" 0.40182966 -0.44779757 ;
	setAttr ".uvtk[278]" -type "float2" 0.40095347 -0.44794962 ;
	setAttr ".uvtk[284]" -type "float2" 0.40092856 -0.44782189 ;
	setAttr ".uvtk[295]" -type "float2" 0.40092325 -0.44780788 ;
	setAttr ".uvtk[303]" -type "float2" 0.40092772 -0.44779369 ;
	setAttr ".uvtk[304]" -type "float2" 0.40182352 -0.44764122 ;
	setAttr ".uvtk[309]" -type "float2" 0.40094513 -0.44766489 ;
	setAttr ".uvtk[325]" -type "float2" 0.40253383 -0.47187454 ;
	setAttr ".uvtk[334]" -type "float2" 0.40159786 -0.47190011 ;
	setAttr ".uvtk[335]" -type "float2" 0.40144014 -0.4660053 ;
	setAttr ".uvtk[344]" -type "float2" 0.4015801 -0.47202891 ;
	setAttr ".uvtk[346]" -type "float2" 0.40141547 -0.46587753 ;
	setAttr ".uvtk[347]" -type "float2" 0.40236938 -0.46585152 ;
	setAttr ".uvtk[358]" -type "float2" 0.40157562 -0.4720431 ;
	setAttr ".uvtk[359]" -type "float2" 0.40253806 -0.47203088 ;
	setAttr ".uvtk[360]" -type "float2" 0.40160805 -0.46464944 ;
	setAttr ".uvtk[371]" -type "float2" 0.40158087 -0.47205698 ;
	setAttr ".uvtk[382]" -type "float2" 0.40160555 -0.47218472 ;
	setAttr ".uvtk[383]" -type "float2" 0.40270245 -0.47805393 ;
	setAttr ".uvtk[393]" -type "float2" 0.40176326 -0.47807959 ;
	setAttr ".uvtk[404]" -type "float2" 0.4017455 -0.47820836 ;
	setAttr ".uvtk[415]" -type "float2" 0.40174103 -0.47822255 ;
	setAttr ".uvtk[416]" -type "float2" 0.40270656 -0.4782103 ;
	setAttr ".uvtk[426]" -type "float2" 0.40174627 -0.47823647 ;
	setAttr ".uvtk[437]" -type "float2" 0.40177065 -0.47836423 ;
	setAttr ".uvtk[438]" -type "float2" 0.40278786 -0.48375252 ;
	setAttr ".uvtk[448]" -type "float2" 0.40190673 -0.48379144 ;
	setAttr ".uvtk[459]" -type "float2" 0.40188879 -0.48392019 ;
	setAttr ".uvtk[470]" -type "float2" 0.40188426 -0.48393446 ;
	setAttr ".uvtk[471]" -type "float2" 0.40279019 -0.48390892 ;
	setAttr ".uvtk[479]" -type "float2" 0.4018895 -0.48394832 ;
	setAttr ".uvtk[486]" -type "float2" 0.40191388 -0.48407611 ;
	setAttr ".uvtk[571]" -type "float2" 0.39840907 -0.44773152 ;
	setAttr ".uvtk[572]" -type "float2" 0.39908808 -0.4477137 ;
	setAttr ".uvtk[573]" -type "float2" 0.39849123 -0.44785759 ;
	setAttr ".uvtk[574]" -type "float2" 0.39917022 -0.44783983 ;
	setAttr ".uvtk[575]" -type "float2" 0.39848176 -0.44787195 ;
	setAttr ".uvtk[576]" -type "float2" 0.39916077 -0.44785413 ;
	setAttr ".uvtk[577]" -type "float2" 0.39849189 -0.44788578 ;
	setAttr ".uvtk[578]" -type "float2" 0.39917091 -0.44786802 ;
	setAttr ".uvtk[579]" -type "float2" 0.39841655 -0.44801614 ;
	setAttr ".uvtk[580]" -type "float2" 0.39909557 -0.44799832 ;
	setAttr ".uvtk[581]" -type "float2" 0.39855853 -0.45342836 ;
	setAttr ".uvtk[582]" -type "float2" 0.39923751 -0.4534106 ;
	setAttr ".uvtk[583]" -type "float2" 0.39931965 -0.45353678 ;
	setAttr ".uvtk[584]" -type "float2" 0.39864066 -0.45355448 ;
	setAttr ".uvtk[585]" -type "float2" 0.3993102 -0.45355108 ;
	setAttr ".uvtk[586]" -type "float2" 0.39863122 -0.45356891 ;
	setAttr ".uvtk[587]" -type "float2" 0.39932036 -0.45356485 ;
	setAttr ".uvtk[588]" -type "float2" 0.39864138 -0.45358267 ;
	setAttr ".uvtk[589]" -type "float2" 0.39924499 -0.45369515 ;
	setAttr ".uvtk[590]" -type "float2" 0.39856601 -0.45371297 ;
	setAttr ".uvtk[591]" -type "float2" 0.39872065 -0.45960793 ;
	setAttr ".uvtk[592]" -type "float2" 0.39939961 -0.45959014 ;
	setAttr ".uvtk[593]" -type "float2" 0.39880276 -0.45973414 ;
	setAttr ".uvtk[594]" -type "float2" 0.39948174 -0.45971632 ;
	setAttr ".uvtk[595]" -type "float2" 0.39879331 -0.45974845 ;
	setAttr ".uvtk[596]" -type "float2" 0.3994723 -0.45973063 ;
	setAttr ".uvtk[597]" -type "float2" 0.39880347 -0.45976225 ;
	setAttr ".uvtk[598]" -type "float2" 0.39948249 -0.45974442 ;
	setAttr ".uvtk[599]" -type "float2" 0.3987281 -0.45989257 ;
	setAttr ".uvtk[600]" -type "float2" 0.39940706 -0.45987475 ;
	setAttr ".uvtk[601]" -type "float2" 0.39888272 -0.4657875 ;
	setAttr ".uvtk[602]" -type "float2" 0.39956173 -0.46576971 ;
	setAttr ".uvtk[603]" -type "float2" 0.39964381 -0.46589589 ;
	setAttr ".uvtk[604]" -type "float2" 0.39896485 -0.46591371 ;
	setAttr ".uvtk[605]" -type "float2" 0.39963439 -0.4659102 ;
	setAttr ".uvtk[606]" -type "float2" 0.3989554 -0.46592802 ;
	setAttr ".uvtk[607]" -type "float2" 0.39964458 -0.46592402 ;
	setAttr ".uvtk[608]" -type "float2" 0.39896557 -0.46594182 ;
	setAttr ".uvtk[609]" -type "float2" 0.39956918 -0.46605432 ;
	setAttr ".uvtk[610]" -type "float2" 0.3988902 -0.46607214 ;
	setAttr ".uvtk[611]" -type "float2" 0.39904484 -0.47196704 ;
	setAttr ".uvtk[612]" -type "float2" 0.39972383 -0.47194922 ;
	setAttr ".uvtk[613]" -type "float2" 0.39912692 -0.47209322 ;
	setAttr ".uvtk[614]" -type "float2" 0.39980593 -0.47207543 ;
	setAttr ".uvtk[615]" -type "float2" 0.3991175 -0.47210753 ;
	setAttr ".uvtk[616]" -type "float2" 0.39979652 -0.47208974 ;
	setAttr ".uvtk[617]" -type "float2" 0.39912769 -0.47212136 ;
	setAttr ".uvtk[618]" -type "float2" 0.39980668 -0.47210354 ;
	setAttr ".uvtk[619]" -type "float2" 0.39905229 -0.47225168 ;
	setAttr ".uvtk[620]" -type "float2" 0.39973128 -0.47223389 ;
	setAttr ".uvtk[621]" -type "float2" 0.39920694 -0.47814664 ;
	setAttr ".uvtk[622]" -type "float2" 0.39988592 -0.47812882 ;
	setAttr ".uvtk[623]" -type "float2" 0.39996803 -0.478255 ;
	setAttr ".uvtk[624]" -type "float2" 0.39928904 -0.47827286 ;
	setAttr ".uvtk[625]" -type "float2" 0.39995861 -0.47826931 ;
	setAttr ".uvtk[626]" -type "float2" 0.39927962 -0.47828713 ;
	setAttr ".uvtk[627]" -type "float2" 0.39996877 -0.47828314 ;
	setAttr ".uvtk[628]" -type "float2" 0.39928982 -0.47830096 ;
	setAttr ".uvtk[629]" -type "float2" 0.3998934 -0.47841343 ;
	setAttr ".uvtk[630]" -type "float2" 0.39921439 -0.47843122 ;
	setAttr ".uvtk[631]" -type "float2" 0.39935628 -0.48383984 ;
	setAttr ".uvtk[632]" -type "float2" 0.40003538 -0.48382565 ;
	setAttr ".uvtk[633]" -type "float2" 0.39943832 -0.48396608 ;
	setAttr ".uvtk[634]" -type "float2" 0.40011746 -0.4839519 ;
	setAttr ".uvtk[635]" -type "float2" 0.39942893 -0.48398033 ;
	setAttr ".uvtk[636]" -type "float2" 0.40010804 -0.48396617 ;
	setAttr ".uvtk[637]" -type "float2" 0.39943922 -0.48399422 ;
	setAttr ".uvtk[638]" -type "float2" 0.40011823 -0.48398 ;
	setAttr ".uvtk[639]" -type "float2" 0.39936373 -0.48412442 ;
	setAttr ".uvtk[640]" -type "float2" 0.40004283 -0.4841103 ;
	setAttr ".uvtk[641]" -type "float2" 0.39943278 -0.4867557 ;
	setAttr ".uvtk[643]" -type "float2" 0.40183473 -0.4479259 ;
	setAttr ".uvtk[645]" -type "float2" 0.40182853 -0.44776949 ;
	setAttr ".uvtk[647]" -type "float2" 0.4027921 -0.48403719 ;
	setAttr ".uvtk[649]" -type "float2" 0.40278977 -0.48388082 ;
	setAttr ".uvtk[651]" -type "float2" 0.40254158 -0.47215915 ;
	setAttr ".uvtk[653]" -type "float2" 0.40253729 -0.47200277 ;
	setAttr ".uvtk[655]" -type "float2" 0.40222394 -0.4598003 ;
	setAttr ".uvtk[657]" -type "float2" 0.40221971 -0.45964393 ;
	setAttr ".uvtk[659]" -type "float2" 0.4023729 -0.46597978 ;
	setAttr ".uvtk[660]" -type "float2" 0.40227038 -0.4648506 ;
	setAttr ".uvtk[662]" -type "float2" 0.40270847 -0.47833857 ;
	setAttr ".uvtk[664]" -type "float2" 0.40270597 -0.4781822 ;
	setAttr ".uvtk[666]" -type "float2" 0.40205789 -0.45362076 ;
	setAttr ".uvtk[668]" -type "float2" 0.40205348 -0.45346448 ;
	setAttr ".uvtk[670]" -type "float2" 0.40182906 -0.44778362 ;
	setAttr ".uvtk[671]" -type "float2" 0.40279001 -0.48389485 ;
	setAttr ".uvtk[672]" -type "float2" 0.40253764 -0.47201684 ;
	setAttr ".uvtk[673]" -type "float2" 0.40222007 -0.45965797 ;
	setAttr ".uvtk[674]" -type "float2" 0.40270633 -0.47819623 ;
	setAttr ".uvtk[675]" -type "float2" 0.40205401 -0.45347849 ;
	setAttr ".uvtk[680]" -type "float2" 0.40265113 -0.48682076 ;
	setAttr ".uvtk[683]" -type "float2" 0.39868838 -0.44508234 ;
createNode polyMapSewMove -n "polyMapSewMove71";
	rename -uid "9A05B73A-404E-5F1C-12AF-2D97DE088529";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 32 "e[399]" "e[409]" "e[416]" "e[423]" "e[442]" "e[455]" "e[468]" "e[481]" "e[595]" "e[599]" "e[608]" "e[612]" "e[660]" "e[664]" "e[739]" "e[742]" "e[787]" "e[790]" "e[835]" "e[838]" "e[883]" "e[886]" "e[956]" "e[958]" "e[960]" "e[962]" "e[976]" "e[978]" "e[1019:1020]" "e[1043:1044]" "e[1067:1068]" "e[1091:1092]";
createNode polyTweakUV -n "polyTweakUV80";
	rename -uid "857AC856-4BB0-661E-99FB-009043E1ABA4";
	setAttr ".uopa" yes;
	setAttr -s 653 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -0.10995507 -0.461768 -0.10891914 -0.46424598
		 -0.1037181 -0.46163285 -0.10465807 -0.45969644 -0.11210579 -0.46323621 -0.1114279
		 -0.46522552 -0.098787427 -0.45866522 -0.099840939 -0.45659164 -0.10755306 -0.4537977
		 -0.11253595 -0.4563424 -0.11393398 -0.4578189 -0.092799544 -0.45508966 -0.094034791
		 -0.45270428 -0.10294503 -0.45036924 -0.10968363 -0.45032799 -0.11463815 -0.45354906
		 -0.08688581 -0.45140013 -0.087955594 -0.44935289 -0.097191572 -0.44654009 -0.10514605
		 -0.44654217 -0.080836296 -0.44797242 -0.082090914 -0.44559827 -0.091169655 -0.44320971
		 -0.099312663 -0.44268116 -0.074214995 -0.44560176 -0.075489461 -0.44323733 -0.085346937
		 -0.4395048 -0.093242705 -0.43931198 -0.069340944 -0.44257632 -0.070525944 -0.44045985
		 -0.078827202 -0.43712848 -0.087210238 -0.43582499 -0.063831449 -0.44054711 -0.065202117
		 -0.43824214 -0.073675692 -0.43473476 -0.08061707 -0.43336606 -0.063102841 -0.43760931
		 -0.061967254 -0.43940765 -0.068228662 -0.43306506 -0.074639201 -0.43070114 -0.066185057
		 -0.43258715 -0.42646828 0.54967999 -0.45894626 0.55629951 -0.44286832 0.54931021
		 -0.17828171 -0.44581199 -0.17668419 -0.44350666 -0.18474595 -0.44148231 -0.18614624
		 -0.44480935 -0.17456453 -0.43603316 -0.183367 -0.43370846 -0.18839096 -0.44074324
		 -0.18903379 -0.44442952 -0.16915701 -0.43802944 -0.17219345 -0.44553372 -0.17116366
		 -0.42885041 -0.18045352 -0.42631346 -0.18673621 -0.43282112 -0.16499208 -0.4309209
		 -0.16749756 -0.42000943 -0.17672388 -0.41737115 -0.18403982 -0.42530102 -0.161301
		 -0.42197078 -0.16458075 -0.41093326 -0.17379163 -0.40820849 -0.18079726 -0.41618478
		 -0.15834634 -0.41282165 -0.16109125 -0.40202764 -0.17025118 -0.39923322 -0.17736034
		 -0.40715387 -0.15504585 -0.40374222 -0.15962087 -0.39252248 -0.16881512 -0.38962713
		 -0.17431329 -0.39801422 -0.15367632 -0.39405656 -0.15774445 -0.38487384 -0.16637091
		 -0.38213694 -0.17237107 -0.38853097 -0.15201013 -0.38511291 -0.15692137 -0.37700394
		 -0.1647964 -0.37431741 -0.16956823 -0.38109696 -0.15158324 -0.37572983 -0.15675609
		 -0.37410474 -0.16433208 -0.37080774 -0.1683033 -0.37306753 -0.42502326 0.47104448
		 -0.43600944 0.46880078 -0.16705094 -0.3697097 -0.38510823 0.51075649 -0.38555294
		 0.51079547 -0.39875585 0.4985359 -0.40333521 0.49836797 -0.40626639 0.50309968 -0.40599772
		 0.50187194 -0.41154522 0.49551046 -0.41237062 0.49738687 -0.3984496 0.49337214 -0.40380281
		 0.47751874 -0.41323254 0.48692721 -0.39966375 0.48631451 -0.40395501 0.48034769 -0.41179964
		 0.48877963 -0.41670099 0.48766828 -0.41440201 0.49545145 -0.42400163 0.48791939 -0.42016533
		 0.49837434 -0.43328297 0.50098205 -0.42731982 0.50488269 -0.42423621 0.49131596 -0.41721064
		 0.48759264 -0.43250811 0.49304351 -0.44108617 0.50366157 -0.43318266 0.50623626 -0.44073385
		 0.49972069 -0.44773123 0.50305051 -0.43849999 0.50592411 -0.44787157 0.50270796 -0.45111167
		 0.50376356 -0.44523302 0.50650513 -0.44416115 0.49526191 -0.44919077 0.50306696 -0.44724932
		 0.50210273 -0.46147442 0.5121454 -0.46185702 0.51210034 -0.46136379 0.51278418 -0.45878336
		 0.5143562 -0.4465811 0.50271744 -0.43994647 0.49495849 -0.43973184 0.49844322 -0.43195277
		 0.48906171 -0.43230373 0.49571311 -0.42453298 0.49470854 -0.42463937 0.49839681 -0.41719928
		 0.49316365 -0.41731408 0.49771899 -0.41199347 0.49820244 -0.40601075 0.50327879 -0.39301562
		 0.50653189 -0.38576102 0.50712442 -0.43388245 0.51322699 -0.42747974 0.51278138 -0.424431
		 0.52448088 -0.42950606 0.52485311 -0.43398866 0.51239908 -0.42740434 0.51196623 -0.42145422
		 0.51280642 -0.41960606 0.52446526 -0.42437318 0.52531505 -0.42935038 0.52574724 -0.44021291
		 0.5146876 -0.43444198 0.5275135 -0.44092739 0.5139215 -0.43399298 0.51229537 -0.42734203
		 0.51186645 -0.42132592 0.5119822 -0.41533759 0.51339453 -0.41481227 0.5256263 -0.41966048
		 0.52532858 -0.42436618 0.52542031 -0.4293386 0.52585888 -0.43487966 0.52836096 -0.4460099
		 0.51638329 -0.43926829 0.5296908 -0.44691873 0.51558363 -0.44096673 0.5138185 -0.43399617
		 0.51218885 -0.42734963 0.51176357 -0.42126876 0.51188236 -0.41453877 0.51264393 -0.40853709
		 0.51449263 -0.40895358 0.52734202 -0.41431302 0.52645731 -0.4196659 0.52543664 -0.42441505
		 0.52552307 -0.42932457 0.52596724 -0.43492979 0.5284673 -0.43983436 0.53055871 -0.45167923
		 0.52186561 -0.44619387 0.53370273 -0.45301992 0.52123666 -0.44698006 0.51547456 -0.44090617
		 0.51371163 -0.4340854 0.51133442 -0.42748979 0.51095098 -0.42127401 0.51177883 -0.41445091
		 0.5125463 -0.40764725 0.51374185 -0.40481561 0.52929574 -0.40370882 0.51547587 -0.40841624
		 0.52819133 -0.41425374 0.52656257 -0.41972795 0.52553856 -0.42446068 0.526353 -0.42922357
		 0.5268327 -0.4348833 0.52856648 -0.43989944 0.53066647 -0.44728547 0.53444827 -0.46419388
		 0.52631235 -0.46026155 0.5368154 -0.46564502 0.52590281 -0.45318422 0.52115899 -0.44690043
		 0.51536506 -0.44041729 0.51286072 -0.42135441 0.51095355 -0.4145354 0.51244593 -0.40754703
		 0.5136472 -0.40236962 0.51484925 -0.40391904 0.53006536 -0.38778633 0.51901972 -0.3891955
		 0.53110021 -0.40835294 0.52829683 -0.41435507 0.52665687 -0.41985124 0.52638876 -0.42371583
		 0.53910732 -0.42624915 0.53873086 -0.43410718 0.5292896 -0.43982527 0.53077006 -0.44741783
		 0.53454083 -0.46155426 0.53744328 -0.46578082 0.52586538 -0.453035 0.52106535 -0.44626519
		 0.51448965 -0.41523236 0.5116657 -0.40763244 0.51354653 -0.40220839 0.51477081 -0.38622469
		 0.51858509 -0.38807803 0.53175873 -0.40380704 0.53016067 -0.40846837 0.52838922 -0.41520846
		 0.52738369 -0.42124662 0.53814572 -0.4262577 0.53968257 -0.42365929 0.53999996 -0.42922935
		 0.54121417 -0.43882561 0.53150845 -0.44723529 0.53460693 -0.46166992 0.53750587 -0.46563929
		 0.52580899 -0.45185658 0.52030027 -0.40827858 0.51273179 -0.40234917 0.51468009 -0.38606685
		 0.51855552 -0.38796443 0.53181648 -0.40397644 0.53023541 -0.40946001 0.52911937 -0.41843998
		 0.53950024 -0.42124331 0.53908885 -0.42941219 0.54214644 -0.42629287 0.53980011 -0.42369738
		 0.54010952;
	setAttr ".uvtk[250:499]" -0.43204784 0.54360515 -0.4457044 0.53513288 -0.46151313
		 0.53752315 -0.46411574 0.52522922 -0.40351188 0.51395547 -0.38619739 0.51850361 -0.38812912
		 0.53183508 -0.40535623 0.53083777 -0.41440043 0.54128885 -0.41813818 0.54041195 -0.42124397
		 0.53920591 -0.43224227 0.54450595 -0.42943466 0.5422622 -0.42627957 0.53991473 -0.42374784
		 0.54021823 -0.43861064 0.54482782 -0.45984909 0.53775775 -0.387474 0.51788503 -0.38972294
		 0.53218508 -0.41153824 0.5433979 -0.4141452 0.54217982 -0.41809973 0.54052591 -0.42129898
		 0.53931683 -0.43926597 0.54551101 -0.43226838 0.54461759 -0.42937452 0.54236811 -0.42615753
		 0.54084313 -0.42417553 0.54108042 -0.45408255 0.54540676 -0.39651334 0.54230905 -0.41077161
		 0.54413027 -0.41410893 0.54228902 -0.41820505 0.54062557 -0.42177477 0.54018939 -0.45501477
		 0.54598725 -0.43935263 0.54559642 -0.43218035 0.54472262 -0.42860264 0.5431627 -0.42447314
		 0.54770666 -0.42251077 0.54717904 -0.3956989 0.5429101 -0.41068113 0.54422212 -0.41422087
		 0.54238439 -0.41905114 0.54139972 -0.42094547 0.54660422 -0.45509559 0.54604459 -0.4391526
		 0.54565322 -0.43118602 0.54548824 -0.42663565 0.54946494 -0.39558613 0.54296607 -0.41085958
		 0.54429233 -0.41513169 0.54315078 -0.41905725 0.54762161 -0.45492563 0.54604304 -0.43751797
		 0.5461117 -0.42816651 0.55153668 -0.39574766 0.5429793 -0.41230685 0.54485565 -0.41640943
		 0.54926711 -0.45317799 0.54612243 -0.39735132 0.54327464 -0.41466403 0.55044001 -0.4011392
		 0.54861009 -0.42170212 0.50444591 -0.41764832 0.50433522 -0.4256649 0.50390017 -0.42180049
		 0.50371236 -0.41757524 0.50363761 -0.41412094 0.50416863 -0.42978793 0.5031389 -0.42633486
		 0.50320953 -0.42180043 0.50361931 -0.4175081 0.50355124 -0.41399965 0.50343645 -0.41065276
		 0.503232 -0.43837026 0.50166792 -0.43049413 0.50240833 -0.42640799 0.50311863 -0.42179483
		 0.50352663 -0.41751954 0.503461 -0.41395181 0.50334531 -0.40988976 0.50259668 -0.40566656
		 0.50262552 -0.43945277 0.5010137 -0.45577985 0.49988434 -0.46031961 0.51144129 -0.43057853
		 0.50231433 -0.42632246 0.5030303 -0.42169124 0.50279576 -0.41762671 0.50276446 -0.41396791
		 0.50325203 -0.40980673 0.50251162 -0.4048489 0.50195628 -0.40250617 0.50173432 -0.45697671
		 0.49928927 -0.43958792 0.50093174 -0.46188441 0.51204354 -0.43046719 0.50222665 -0.42557833
		 0.50235486 -0.42026269 0.49643666 -0.41701639 0.49686605 -0.41415325 0.50252271 -0.40989614
		 0.50242913 -0.4047581 0.50187033 -0.40128797 0.5011512 -0.38702703 0.50065011 -0.38628495
		 0.51010841 -0.45708665 0.49922913 -0.43940461 0.50086296 -0.42950517 0.50155115 -0.42321181
		 0.49530417 -0.41688094 0.496149 -0.42004994 0.49571851 -0.41423914 0.49642524 -0.41066709
		 0.5018127 -0.40485525 0.50178701 -0.40113944 0.5010767 -0.38557708 0.50014055 -0.38503027
		 0.51070994 -0.45692262 0.49919701 -0.43787977 0.50032085 -0.42630613 0.49358851 -0.42357713
		 0.49457958 -0.41431966 0.49570674 -0.41682056 0.49605897 -0.42002574 0.49562645 -0.4113988
		 0.4956997 -0.40564895 0.50113273 -0.40129358 0.50100863 -0.38543516 0.50010037 -0.45520681
		 0.4988164 -0.43201822 0.49094054 -0.42680126 0.49283794 -0.42362225 0.49448642 -0.41428208
		 0.4956148 -0.41089156 0.49499303 -0.41682649 0.4959653 -0.42000762 0.49553621 -0.40726247
		 0.49411774 -0.40256804 0.50048012 -0.38558561 0.50007403 -0.44696099 0.48784027 -0.4329313
		 0.49025527 -0.4268617 0.49274206 -0.42352432 0.49440405 -0.41425329 0.49552369 -0.41082582
		 0.49490151 -0.40655968 0.49344143 -0.41685641 0.49523818 -0.41984904 0.49482393 -0.4047274
		 0.49283212 -0.38708568 0.49966943 -0.44797295 0.48720127 -0.4330453 0.49016941 -0.42673612
		 0.49265996 -0.42271656 0.49377596 -0.41432181 0.49479574 -0.41087693 0.494813 -0.40648663
		 0.49335384 -0.40378731 0.49218041 -0.41747668 0.48776078 -0.41660386 0.48829052 -0.3898434
		 0.49079788 -0.4480612 0.48713505 -0.43285134 0.4901064 -0.42568427 0.49202907 -0.4184629
		 0.4861542 -0.41583991 0.48829377 -0.41168457 0.49417353 -0.40660343 0.4932763 -0.40366817
		 0.49209911 -0.38850027 0.49027181 -0.41727287 0.48695743 -0.41614777 0.48754382 -0.44788724
		 0.48711759 -0.43124175 0.48960876 -0.41962767 0.48432142 -0.41867822 0.48532894 -0.41544971
		 0.48753265 -0.41456267 0.48804837 -0.40761265 0.49268165 -0.40383077 0.49203318 -0.38837087
		 0.49022847 -0.41724986 0.48685652 -0.41609755 0.4874469 -0.44610336 0.48685396 -0.42165232
		 0.48143595 -0.41988552 0.48352316 -0.41870442 0.48522487 -0.41541117 0.48743361 -0.41409975
		 0.48727685 -0.41168118 0.48661742 -0.40515661 0.49151409 -0.38853276 0.49021572 -0.4172219
		 0.48675805 -0.41610813 0.48734617 -0.43429744 0.47805148 -0.42230421 0.48076677 -0.41991881
		 0.48342201 -0.41860408 0.48513517 -0.41543394 0.48733407 -0.4140422 0.48717749 -0.41130847
		 0.48588273 -0.41029865 0.48478031 -0.3901335 0.48992339 -0.41700217 0.48597649 -0.41622347
		 0.48655513 -0.43532142 0.47748581 -0.4223941 0.4806838 -0.41979712 0.48333529 -0.41777825
		 0.4844476 -0.41563201 0.48655191 -0.4141297 0.48708537 -0.41125795 0.48579064 -0.40938979
		 0.48420191 -0.39602005 0.48338869 -0.41427204 0.48274434 -0.41464111 0.48197365 -0.43540466
		 0.47742736 -0.4221971 0.48062402 -0.41879582 0.48266393 -0.41568989 0.48062396 -0.41392115
		 0.48287666 -0.41483554 0.48637894 -0.41136417 0.48570836 -0.40928072 0.4841283 -0.3948341
		 0.4828862 -0.43522674 0.47742379 -0.42058739 0.48014748 -0.4160893 0.4789907 -0.41345361
		 0.48273635 -0.41221613 0.48505902 -0.40945023 0.48406881 -0.39472073 0.48284587 -0.43338108
		 0.47728547 -0.41406524 0.47838899 -0.41173652 0.4816834 -0.41081947 0.48360544 -0.39488286
		 0.48283631 -0.41131854 0.48148307 -0.39647961 0.48257709 -0.3981126 0.47765452 -0.41689441
		 0.49322435 -0.41406956 0.49365437 -0.39673215 0.54796815 -0.39508384 0.54977906 -0.39172691
		 0.54492331 -0.38987744 0.54727876 -0.39146662 0.54446757 -0.38954878 0.54674292;
	setAttr ".uvtk[500:652]" -0.39139622 0.54447889 -0.38951701 0.54674774 -0.39141929
		 0.54443324 -0.38948208 0.54669607 -0.39086002 0.54436165 -0.38905555 0.54658473 -0.38356143
		 0.53614461 -0.38167167 0.5381012 -0.3834036 0.53562564 -0.38146871 0.53734899 -0.38333899
		 0.53562188 -0.38145661 0.53731757 -0.38336599 0.5355711 -0.38144046 0.5372436 -0.38314658
		 0.53534859 -0.3812449 0.53681099 -0.3812573 0.5242902 -0.3792485 0.52528191 -0.38117945
		 0.52386159 -0.37917233 0.52481544 -0.3811205 0.52384996 -0.37917924 0.52479398 -0.38116574
		 0.5238111 -0.37917078 0.52474928 -0.38109714 0.52353901 -0.37913406 0.52444971 -0.37950844
		 0.51202357 -0.37748265 0.51255256 -0.37952369 0.51159 -0.37747097 0.51195025 -0.37946171
		 0.51156539 -0.3774662 0.51190209 -0.37950617 0.51152897 -0.37745291 0.5118472 -0.37948531
		 0.51115429 -0.37747079 0.51129609 -0.38024867 0.49922842 -0.37821972 0.49879301 -0.38031501
		 0.49890369 -0.37828189 0.49835432 -0.38026744 0.49886817 -0.3782922 0.49830279 -0.38032603
		 0.49884796 -0.37829208 0.49826556 -0.38040799 0.49842444 -0.37840658 0.49771148 -0.38336426
		 0.48784226 -0.38138437 0.48665974 -0.38349819 0.48760307 -0.38152939 0.48626941 -0.38346344
		 0.4875603 -0.38154602 0.48620534 -0.3835296 0.487553 -0.38155848 0.48617554 -0.38368905
		 0.48709548 -0.3817457 0.48550957 -0.39008683 0.47977263 -0.38819546 0.47801852 -0.39030963
		 0.4796589 -0.38841099 0.47792628 -0.39028847 0.47962248 -0.38843316 0.47788385 -0.39036036
		 0.47962993 -0.38845223 0.47788572 -0.3905881 0.47924221 -0.38865727 0.47743696 -0.39409119
		 0.47638884 -0.18928264 -0.44805571 -0.05981338 -0.4379738 -0.46173772 0.55505371
		 -0.45909557 0.55222577 -0.46254623 0.55418193 -0.45975488 0.55165589 -0.46257982
		 0.5541594 -0.45983359 0.55166852 -0.46263283 0.55408013 -0.45983747 0.5516122 -0.46272618
		 0.55354565 -0.4599812 0.55138606 -0.47043908 0.54402721 -0.46695989 0.54194999 -0.46743324
		 0.54132497 -0.47116065 0.54323983 -0.46749648 0.54131424 -0.47116166 0.5432111 -0.46747422
		 0.54125607 -0.47120246 0.54314518 -0.46740872 0.5408507 -0.47096726 0.54256582 -0.4750334
		 0.52882409 -0.47141734 0.52772236 -0.47557586 0.52779084 -0.47168463 0.52705628 -0.47556657
		 0.52771604 -0.47173083 0.52701688 -0.47558728 0.52762973 -0.47169137 0.52696246 -0.47517028
		 0.52665281 -0.47147846 0.52635419 -0.4719533 0.51120675 -0.46828705 0.51151592 -0.46837977
		 0.51097894 -0.47232136 0.51048988 -0.46841452 0.51093042 -0.47229597 0.51042622 -0.46835846
		 0.51089573 -0.47230783 0.51037681 -0.46799639 0.51026314 -0.47171614 0.50957412 -0.46669185
		 0.49542072 -0.46304587 0.49660715 -0.46697661 0.49471712 -0.46307939 0.49614033 -0.46693948
		 0.49463984 -0.46310091 0.49608034 -0.46693736 0.49459562 -0.46303701 0.49605799 -0.46627852
		 0.49368697 -0.46260518 0.49536198 -0.45725146 0.48012948 -0.45370534 0.48235369 -0.45356569
		 0.48200092 -0.45735106 0.47953233 -0.45356652 0.48193291 -0.45729747 0.47944915 -0.4534907
		 0.48192751 -0.45727825 0.47942233 -0.45294073 0.48119473 -0.4564501 0.47846448 -0.44341782
		 0.46923661 -0.44062459 0.47200054 -0.44331089 0.4689258 -0.44042596 0.47178441 -0.44325703
		 0.46885303 -0.44041836 0.471724 -0.44322312 0.46885076 -0.44033486 0.47173429 -0.44246885
		 0.46802789 -0.43969941 0.47108665 -0.43388623 0.46525389 -0.06803602 -0.4291068 -0.16560178
		 -0.36643696 -0.16273277 -0.36763889 -0.15475379 -0.37075043 -0.063997865 -0.43074429
		 -0.060933828 -0.43603808 -0.46094945 0.5585652 -0.18627326 -0.44837555 -0.39243466
		 0.47544241 -0.17764245 -0.44831973 -0.11579889 -0.4577423 -0.11460745 -0.46383837
		 -0.11395496 -0.46592045;
createNode polyDelEdge -n "polyDelEdge15";
	rename -uid "83C7140F-430B-0DFD-25D0-D48D5AB1420E";
	setAttr ".ics" -type "componentList" 58 "e[2]" "e[35]" "e[43]" "e[70]" "e[81]" "e[91:98]" "e[100:102]" "e[104]" "e[106:108]" "e[110]" "e[117]" "e[124]" "e[131]" "e[138]" "e[145]" "e[152]" "e[159]" "e[166:177]" "e[218]" "e[223]" "e[242]" "e[247]" "e[252]" "e[257]" "e[268]" "e[297:298]" "e[327:328]" "e[357:358]" "e[387]" "e[492:505]" "e[566]" "e[570]" "e[575]" "e[579]" "e[619]" "e[623]" "e[635]" "e[639]" "e[644]" "e[648]" "e[653]" "e[657]" "e[669]" "e[673]" "e[680]" "e[684]" "e[691]" "e[695]" "e[702]" "e[706]" "e[783]" "e[786]" "e[831]" "e[834]" "e[879]" "e[882]" "e[927]" "e[930]";
	setAttr ".cv" yes;
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "9DC4C3F0-4480-4ACD-33B7-CD8EBAC5011E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 58 "e[2]" "e[35]" "e[43]" "e[70]" "e[81]" "e[91:98]" "e[100:102]" "e[104]" "e[106:108]" "e[110]" "e[117]" "e[124]" "e[131]" "e[138]" "e[145]" "e[152]" "e[159]" "e[166:177]" "e[218]" "e[223]" "e[242]" "e[247]" "e[252]" "e[257]" "e[268]" "e[297:298]" "e[327:328]" "e[357:358]" "e[387]" "e[492:505]" "e[566]" "e[570]" "e[575]" "e[579]" "e[619]" "e[623]" "e[635]" "e[639]" "e[644]" "e[648]" "e[653]" "e[657]" "e[669]" "e[673]" "e[680]" "e[684]" "e[691]" "e[695]" "e[702]" "e[706]" "e[783]" "e[786]" "e[831]" "e[834]" "e[879]" "e[882]" "e[927]" "e[930]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.0672064754216544 0 0 0 0 0.98692533935277804 0
		 0 -1.65922442784799 -0.28623479486675407 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 11.304267 0.14078818 ;
	setAttr ".rs" 57627;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -30.86790657043457 10.847879030297639 -13.1544084356055 ;
	setAttr ".cbx" -type "double3" 30.86790657043457 11.760655586150632 13.435984814144538 ;
createNode polyTweak -n "polyTweak72";
	rename -uid "E96AB0A7-4E91-8E0A-A47C-AF97CDACEAEC";
	setAttr ".uopa" yes;
	setAttr -s 427 ".tk";
	setAttr ".tk[0]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[1]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[6]" -type "float3" 0 9.5367432e-07 0 ;
	setAttr ".tk[19]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[23]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[25]" -type "float3" 0 -7.4803829e-06 0 ;
	setAttr ".tk[26]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[27]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[39]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[43]" -type "float3" 0 -1.0952353e-05 0 ;
	setAttr ".tk[44]" -type "float3" 0 -6.5416098e-06 0 ;
	setAttr ".tk[47]" -type "float3" 0 -4.6472996e-06 0 ;
	setAttr ".tk[57]" -type "float3" 0 -5.1259995e-06 0 ;
	setAttr ".tk[58]" -type "float3" 0 -4.6472996e-06 0 ;
	setAttr ".tk[60]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[61]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[62]" -type "float3" 0 -1.0237098e-05 0 ;
	setAttr ".tk[63]" -type "float3" 0 -7.5399876e-06 0 ;
	setAttr ".tk[64]" -type "float3" 0 1.065433e-06 0 ;
	setAttr ".tk[71]" -type "float3" 0 1.065433e-06 0 ;
	setAttr ".tk[72]" -type "float3" 0 -1.0728836e-05 0 ;
	setAttr ".tk[79]" -type "float3" 0 -2.771616e-06 0 ;
	setAttr ".tk[80]" -type "float3" 0 -4.0531158e-06 0 ;
	setAttr ".tk[87]" -type "float3" 0 -6.4261258e-06 0 ;
	setAttr ".tk[88]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[95]" -type "float3" 0 -3.9041042e-06 0 ;
	setAttr ".tk[126]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[127]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[130]" -type "float3" 0 -3.1143427e-06 0 ;
	setAttr ".tk[131]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[146]" -type "float3" 0 -8.1956387e-07 0 ;
	setAttr ".tk[147]" -type "float3" 0 -4.4107437e-06 0 ;
	setAttr ".tk[152]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[153]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[158]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[159]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[162]" -type "float3" 0 -3.5762787e-06 0 ;
	setAttr ".tk[163]" -type "float3" 0 -3.5762787e-06 0 ;
	setAttr ".tk[186]" -type "float3" 0 -3.1143427e-06 0 ;
	setAttr ".tk[187]" -type "float3" 0 -3.1143427e-06 0 ;
	setAttr ".tk[214]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[215]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[216]" -type "float3" 0 -3.1143427e-06 0 ;
	setAttr ".tk[217]" -type "float3" 0 -3.1143427e-06 0 ;
	setAttr ".tk[244]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[245]" -type "float3" 0 -3.7997961e-06 0 ;
	setAttr ".tk[246]" -type "float3" 0 -2.7269125e-06 0 ;
	setAttr ".tk[247]" -type "float3" 0 -3.5762787e-06 0 ;
	setAttr ".tk[274]" -type "float3" 0 -6.3180923e-06 0 ;
	setAttr ".tk[275]" -type "float3" 0 -4.4107437e-06 0 ;
	setAttr ".tk[276]" -type "float3" 0 -4.8726797e-06 0 ;
	setAttr ".tk[277]" -type "float3" 0 -2.7269125e-06 0 ;
	setAttr ".tk[304]" -type "float3" 0 -4.4107437e-06 0 ;
	setAttr ".tk[305]" -type "float3" 0 -6.3180923e-06 0 ;
	setAttr ".tk[333]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[334]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[335]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[339]" -type "float3" 0 -4.8950315e-06 0 ;
	setAttr ".tk[340]" -type "float3" 0 -2.0265579e-06 0 ;
	setAttr ".tk[341]" -type "float3" 0 2.3841858e-07 0 ;
	setAttr ".tk[363]" -type "float3" 0 -1.8924475e-06 0 ;
	setAttr ".tk[364]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".tk[365]" -type "float3" 0 -5.2377582e-06 0 ;
	setAttr ".tk[372]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[373]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[374]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[381]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[382]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[383]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[387]" -type "float3" 0 -1.7955899e-06 0 ;
	setAttr ".tk[388]" -type "float3" 0 -6.4224005e-06 0 ;
	setAttr ".tk[389]" -type "float3" 0 -1.7955899e-06 0 ;
	setAttr ".tk[396]" -type "float3" 0 -3.8146973e-06 0 ;
	setAttr ".tk[397]" -type "float3" 0 -1.6540289e-06 0 ;
	setAttr ".tk[398]" -type "float3" 0 -2.9653311e-06 0 ;
	setAttr ".tk[402]" -type "float3" 0 -3.8146973e-06 0 ;
	setAttr ".tk[403]" -type "float3" 0 -1.6540289e-06 0 ;
	setAttr ".tk[404]" -type "float3" 0 -2.9653311e-06 0 ;
	setAttr ".tk[408]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".tk[409]" -type "float3" 0 -6.1988831e-06 0 ;
	setAttr ".tk[410]" -type "float3" 0 -7.4580312e-06 0 ;
	setAttr ".tk[414]" -type "float3" 0 -6.6533685e-06 0 ;
	setAttr ".tk[415]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[416]" -type "float3" 0 -1.5497208e-06 0 ;
	setAttr ".tk[501]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[502]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[503]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[540]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[541]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[542]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[579]" -type "float3" 0 -7.9870224e-06 0 ;
	setAttr ".tk[580]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[581]" -type "float3" 0 -3.8295984e-06 0 ;
	setAttr ".tk[618]" -type "float3" 0 -3.8295984e-06 0 ;
	setAttr ".tk[619]" -type "float3" 0 -4.6342611e-06 0 ;
	setAttr ".tk[620]" -type "float3" 0 -7.9870224e-06 0 ;
	setAttr ".tk[621]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[622]" -type "float3" 0 -3.4570694e-06 0 ;
	setAttr ".tk[623]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[624]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[625]" -type "float3" 0 -4.6491623e-06 0 ;
	setAttr ".tk[626]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[627]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[628]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[629]" -type "float3" 0 -3.4570694e-06 0 ;
	setAttr ".tk[630]" -type "float3" 0 5.9604645e-07 0 ;
	setAttr ".tk[631]" -type "float3" 0 -3.4570694e-06 0 ;
	setAttr ".tk[632]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[633]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[634]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[635]" -type "float3" 0 -3.4570694e-06 0 ;
	setAttr ".tk[636]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[637]" -type "float3" 0 2.1457672e-06 0 ;
	setAttr ".tk[638]" -type "float3" 0 -2.5033951e-06 0 ;
	setAttr ".tk[639]" -type "float3" 0 -2.5033951e-06 0 ;
	setAttr ".tk[640]" -type "float3" 0 1.0967255e-05 0 ;
	setAttr ".tk[641]" -type "float3" 0 -3.695488e-06 0 ;
	setAttr ".tk[642]" -type "float3" 0 -3.695488e-06 0 ;
	setAttr ".tk[643]" -type "float3" 0 3.695488e-06 0 ;
	setAttr ".tk[644]" -type "float3" 0 1.0967255e-05 0 ;
	setAttr ".tk[645]" -type "float3" 0 -3.5762787e-06 0 ;
	setAttr ".tk[646]" -type "float3" 0 -3.5762787e-06 0 ;
	setAttr ".tk[647]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[648]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[649]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[650]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[651]" -type "float3" 0 -5.7220459e-06 0 ;
	setAttr ".tk[652]" -type "float3" 0 -5.7220459e-06 0 ;
	setAttr ".tk[653]" -type "float3" 0 -6.5565109e-06 0 ;
	setAttr ".tk[654]" -type "float3" 0 -6.5565109e-06 0 ;
	setAttr ".tk[655]" -type "float3" 0 1.5497208e-06 0 ;
	setAttr ".tk[656]" -type "float3" 0 1.5497208e-06 0 ;
	setAttr ".tk[657]" -type "float3" 0 -6.5565109e-06 0 ;
	setAttr ".tk[658]" -type "float3" 0 -6.5565109e-06 0 ;
	setAttr ".tk[659]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[660]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[661]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[662]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[663]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[664]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[665]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[666]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[667]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[668]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[669]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[670]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[671]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[672]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[673]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[674]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[675]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[676]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[677]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[678]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[683]" -type "float3" 0 1.5497208e-06 0 ;
	setAttr ".tk[684]" -type "float3" 0 1.5497208e-06 0 ;
	setAttr ".tk[685]" -type "float3" 0 4.4107437e-06 0 ;
	setAttr ".tk[686]" -type "float3" 0 4.4107437e-06 0 ;
	setAttr ".tk[687]" -type "float3" 0 -1.1920929e-07 0 ;
	setAttr ".tk[688]" -type "float3" 0 -1.1920929e-07 0 ;
	setAttr ".tk[689]" -type "float3" 0 8.3446503e-07 0 ;
	setAttr ".tk[690]" -type "float3" 0 -3.2186508e-06 0 ;
	setAttr ".tk[691]" -type "float3" 0 -2.3841858e-06 0 ;
	setAttr ".tk[692]" -type "float3" 0 -2.3841858e-06 0 ;
	setAttr ".tk[693]" -type "float3" 0 3.5762787e-06 0 ;
	setAttr ".tk[694]" -type "float3" 0 3.5762787e-06 0 ;
	setAttr ".tk[695]" -type "float3" 0 -4.529953e-06 0 ;
	setAttr ".tk[696]" -type "float3" 0 1.0371208e-05 0 ;
	setAttr ".tk[697]" -type "float3" 0 -1.6689301e-06 0 ;
	setAttr ".tk[698]" -type "float3" 0 -1.6689301e-06 0 ;
	setAttr ".tk[699]" -type "float3" 0 -7.1525574e-07 0 ;
	setAttr ".tk[700]" -type "float3" 0 -7.1525574e-07 0 ;
	setAttr ".tk[701]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[702]" -type "float3" 0 5.2452087e-06 0 ;
	setAttr ".tk[703]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[704]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[705]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[706]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[707]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[708]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[709]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[710]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[711]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[712]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[713]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[714]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[715]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[716]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[717]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[718]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[719]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[720]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[721]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[722]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[723]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[724]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[725]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[726]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[727]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[728]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[729]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[730]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[731]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[732]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[733]" -type "float3" 0 -1.9073486e-06 0 ;
	setAttr ".tk[734]" -type "float3" 0 -1.9073486e-06 0 ;
	setAttr ".tk[735]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[736]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[737]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[738]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[739]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[740]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[741]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[742]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[743]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[744]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[745]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[746]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[747]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[748]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[749]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[750]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[751]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[752]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[753]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[754]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[755]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[756]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[757]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[758]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[759]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[760]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[761]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[762]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[763]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[764]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[765]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[766]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[767]" -type "float3" 0 1.7881393e-06 0 ;
	setAttr ".tk[768]" -type "float3" 0 1.7881393e-06 0 ;
	setAttr ".tk[769]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[770]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[771]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[772]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[773]" -type "float3" 0 2.7418137e-06 0 ;
	setAttr ".tk[774]" -type "float3" 0 2.7418137e-06 0 ;
	setAttr ".tk[775]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[776]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[777]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[778]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[779]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[780]" -type "float3" 0 -5.9604645e-07 0 ;
	setAttr ".tk[781]" -type "float3" 0 1.7881393e-06 0 ;
	setAttr ".tk[782]" -type "float3" 0 1.7881393e-06 0 ;
	setAttr ".tk[783]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[784]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[785]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[786]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[787]" -type "float3" 0 -1.9073486e-06 0 ;
	setAttr ".tk[788]" -type "float3" 0 -1.9073486e-06 0 ;
	setAttr ".tk[789]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[790]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[791]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[792]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[793]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[794]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[795]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[796]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[797]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[798]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[799]" -type "float3" 0 -2.7418137e-06 0 ;
	setAttr ".tk[800]" -type "float3" 0 -2.7418137e-06 0 ;
	setAttr ".tk[801]" -type "float3" 0 4.1723251e-06 0 ;
	setAttr ".tk[802]" -type "float3" 0 4.1723251e-06 0 ;
	setAttr ".tk[803]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[804]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[805]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[806]" -type "float3" 0 -1.3113022e-06 0 ;
	setAttr ".tk[807]" -type "float3" 0 1.7881393e-06 0 ;
	setAttr ".tk[808]" -type "float3" 0 1.7881393e-06 0 ;
	setAttr ".tk[809]" -type "float3" 0 2.7418137e-06 0 ;
	setAttr ".tk[810]" -type "float3" 0 2.7418137e-06 0 ;
	setAttr ".tk[811]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[812]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[813]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[814]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[815]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[816]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[817]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[818]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[819]" -type "float3" 0 1.1920929e-06 0 ;
	setAttr ".tk[820]" -type "float3" 0 1.1920929e-06 0 ;
	setAttr ".tk[821]" -type "float3" 0 -2.3841858e-06 0 ;
	setAttr ".tk[822]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[823]" -type "float3" 0 8.3446503e-07 0 ;
	setAttr ".tk[824]" -type "float3" 0 8.3446503e-07 0 ;
	setAttr ".tk[825]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[826]" -type "float3" 0 8.3446503e-07 0 ;
	setAttr ".tk[827]" -type "float3" 0 8.3446503e-07 0 ;
	setAttr ".tk[828]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[829]" -type "float3" 0 -2.3841858e-06 0 ;
	setAttr ".tk[830]" -type "float3" 0 1.1920929e-06 0 ;
	setAttr ".tk[831]" -type "float3" 0 1.1920929e-06 0 ;
	setAttr ".tk[832]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[833]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[834]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[835]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[836]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[837]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[838]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[839]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[840]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[841]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[842]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[843]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[844]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[845]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[846]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[847]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[848]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[849]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[850]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[851]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[852]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[853]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[854]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[855]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[856]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[857]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[858]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[859]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[860]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[861]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[862]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[863]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[864]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[865]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[866]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[867]" -type "float3" 0 -8.3446503e-07 0 ;
	setAttr ".tk[868]" -type "float3" 0 -8.3446503e-07 0 ;
	setAttr ".tk[869]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[870]" -type "float3" 0 1.1920929e-07 0 ;
	setAttr ".tk[871]" -type "float3" 0 1.5497208e-06 0 ;
	setAttr ".tk[872]" -type "float3" 0 1.5497208e-06 0 ;
	setAttr ".tk[873]" -type "float3" 0 -3.5762787e-07 0 ;
	setAttr ".tk[874]" -type "float3" 0 3.5762787e-06 0 ;
	setAttr ".tk[875]" -type "float3" 0 3.5762787e-06 0 ;
	setAttr ".tk[876]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[877]" -type "float3" 0 -4.8875809e-06 0 ;
	setAttr ".tk[878]" -type "float3" 0 1.3113022e-06 0 ;
	setAttr ".tk[879]" -type "float3" 0 1.3113022e-06 0 ;
	setAttr ".tk[880]" -type "float3" 0 2.9802322e-06 0 ;
	setAttr ".tk[881]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[882]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[883]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[884]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[885]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[886]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[887]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[888]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[889]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[890]" -type "float3" 0 -2.3841858e-07 0 ;
	setAttr ".tk[891]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[892]" -type "float3" 0 7.1525574e-07 0 ;
	setAttr ".tk[893]" -type "float3" 0 4.8875809e-06 0 ;
	setAttr ".tk[894]" -type "float3" 0 4.8875809e-06 0 ;
	setAttr ".tk[895]" -type "float3" 0 -2.1457672e-06 0 ;
	setAttr ".tk[896]" -type "float3" 0 -4.7683716e-07 0 ;
	setAttr ".tk[897]" -type "float3" 0 -8.3446503e-07 0 ;
	setAttr ".tk[898]" -type "float3" 0 -8.3446503e-07 0 ;
	setAttr ".tk[899]" -type "float3" 0 -8.3446503e-07 0 ;
	setAttr ".tk[900]" -type "float3" 0 -8.3446503e-07 0 ;
	setAttr ".tk[901]" -type "float3" 0 -4.7683716e-07 0 ;
	setAttr ".tk[902]" -type "float3" 0 -2.1457672e-06 0 ;
	setAttr ".tk[903]" -type "float3" 0 4.8875809e-06 0 ;
	setAttr ".tk[904]" -type "float3" 0 4.8875809e-06 0 ;
	setAttr ".tk[905]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[906]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[907]" -type "float3" 0 -1.1920929e-06 0 ;
	setAttr ".tk[908]" -type "float3" 0 -1.1920929e-06 0 ;
createNode polyTweakUV -n "polyTweakUV81";
	rename -uid "4D281B61-4DF6-DD8A-CE90-F1A998077D54";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[20]" -type "float2" -0.0055442918 1.622029e-05 ;
	setAttr ".uvtk[665]" -type "float2" 8.808961e-06 0.9994483 ;
	setAttr ".uvtk[668]" -type "float2" 8.808961e-06 -0.00055172248 ;
	setAttr ".uvtk[742]" -type "float2" -8.8078787e-06 0.99944937 ;
	setAttr ".uvtk[743]" -type "float2" -8.8078787e-06 -0.00055060897 ;
createNode polyMergeVert -n "polyMergeVert44";
	rename -uid "8A7C3A6E-443E-7B14-585D-8C9BCE38110F";
	setAttr ".ics" -type "componentList" 2 "vtx[72]" "vtx[628]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.0672064754216544 0 0 0 0 0.98692533935277804 0
		 0 -1.65922442784799 -0.28623479486675407 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak73";
	rename -uid "83809045-499B-AC44-2D71-7D9B9DF218B9";
	setAttr ".uopa" yes;
	setAttr -s 192 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[1]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[6]" -type "float3" 0 0.1001384 0 ;
	setAttr ".tk[19]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[23]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[25]" -type "float3" 0 0.26340318 0 ;
	setAttr ".tk[26]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[27]" -type "float3" 0 0.2643894 9.5367432e-07 ;
	setAttr ".tk[39]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[43]" -type "float3" 0 0.26439512 9.5367432e-07 ;
	setAttr ".tk[44]" -type "float3" 0 -0.31885925 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.021699417 0 ;
	setAttr ".tk[57]" -type "float3" 0 -0.15020907 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.021699417 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[61]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[62]" -type "float3" 0 0.26439512 9.5367432e-07 ;
	setAttr ".tk[63]" -type "float3" 0 0.26439229 9.5367432e-07 ;
	setAttr ".tk[64]" -type "float3" 0 -0.15465258 0 ;
	setAttr ".tk[71]" -type "float3" 0 -0.15465258 0 ;
	setAttr ".tk[72]" -type "float3" 0 -0.42764366 0 ;
	setAttr ".tk[79]" -type "float3" 0 -0.25909647 0 ;
	setAttr ".tk[80]" -type "float3" 0 0.087585621 0 ;
	setAttr ".tk[87]" -type "float3" 0 -0.081063665 0 ;
	setAttr ".tk[88]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[95]" -type "float3" 0 0.42764366 9.5367432e-07 ;
	setAttr ".tk[126]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[127]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[130]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[131]" -type "float3" 0 0.26438582 9.5367432e-07 ;
	setAttr ".tk[146]" -type "float3" 0 0.26438582 9.5367432e-07 ;
	setAttr ".tk[147]" -type "float3" 0 0.2643894 9.5367432e-07 ;
	setAttr ".tk[152]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[153]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[158]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[159]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[162]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[163]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[186]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[187]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[214]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[215]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[216]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[217]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[244]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[245]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[246]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[247]" -type "float3" 0 0.2643885 0 ;
	setAttr ".tk[274]" -type "float3" 0 0.26439035 1.9073486e-06 ;
	setAttr ".tk[275]" -type "float3" 0 0.2643894 1.9073486e-06 ;
	setAttr ".tk[276]" -type "float3" 0 0.2643894 9.5367432e-07 ;
	setAttr ".tk[277]" -type "float3" 0 0.26438758 0 ;
	setAttr ".tk[304]" -type "float3" 0 0.2643894 1.9073486e-06 ;
	setAttr ".tk[305]" -type "float3" 0 0.26439035 1.9073486e-06 ;
	setAttr ".tk[333]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[334]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[335]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[339]" -type "float3" 0 0.15229997 9.5367432e-07 ;
	setAttr ".tk[340]" -type "float3" 0 0.16621932 9.5367432e-07 ;
	setAttr ".tk[341]" -type "float3" 0 0.15223205 0 ;
	setAttr ".tk[363]" -type "float3" 0 0.15223405 0 ;
	setAttr ".tk[364]" -type "float3" 0 0.16622058 9.5367432e-07 ;
	setAttr ".tk[365]" -type "float3" 0 0.15229997 9.5367432e-07 ;
	setAttr ".tk[372]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[373]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[374]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[381]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[382]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[383]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[387]" -type "float3" 0 0.15226483 9.5367432e-07 ;
	setAttr ".tk[388]" -type "float3" 0 0.16622242 9.5367432e-07 ;
	setAttr ".tk[389]" -type "float3" 0 0.15226483 9.5367432e-07 ;
	setAttr ".tk[396]" -type "float3" 0 0.15226667 9.5367432e-07 ;
	setAttr ".tk[397]" -type "float3" 0 0.16621932 9.5367432e-07 ;
	setAttr ".tk[398]" -type "float3" 0 0.15226577 9.5367432e-07 ;
	setAttr ".tk[402]" -type "float3" 0 0.15226667 9.5367432e-07 ;
	setAttr ".tk[403]" -type "float3" 0 0.16621932 9.5367432e-07 ;
	setAttr ".tk[404]" -type "float3" 0 0.15226577 9.5367432e-07 ;
	setAttr ".tk[408]" -type "float3" 0 0.15217853 9.5367432e-07 ;
	setAttr ".tk[409]" -type "float3" 0 0.16622242 0 ;
	setAttr ".tk[410]" -type "float3" 0 0.15235931 0 ;
	setAttr ".tk[414]" -type "float3" 0 0.1523222 0 ;
	setAttr ".tk[415]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[416]" -type "float3" 0 0.15221393 9.5367432e-07 ;
	setAttr ".tk[501]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[502]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[503]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[540]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[541]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[542]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[579]" -type "float3" 0 0.1523132 0 ;
	setAttr ".tk[580]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[581]" -type "float3" 0 0.15222539 0 ;
	setAttr ".tk[618]" -type "float3" 0 0.15222539 0 ;
	setAttr ".tk[619]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[620]" -type "float3" 0 0.1523132 0 ;
	setAttr ".tk[621]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[622]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[623]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[624]" -type "float3" 0 0.2643894 1.9073486e-06 ;
	setAttr ".tk[625]" -type "float3" 0 0.2643894 9.5367432e-07 ;
	setAttr ".tk[626]" -type "float3" 0 0.26438758 0 ;
	setAttr ".tk[627]" -type "float3" 0 -0.31885833 0 ;
	setAttr ".tk[628]" -type "float3" 0 -0.4276436 0 ;
	setAttr ".tk[629]" -type "float3" 0 -0.15020907 0 ;
	setAttr ".tk[630]" -type "float3" 0 -0.25909647 0 ;
	setAttr ".tk[631]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[632]" -type "float3" 0 0.42764366 9.5367432e-07 ;
	setAttr ".tk[633]" -type "float3" 0 0.26439512 9.5367432e-07 ;
	setAttr ".tk[634]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[635]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[636]" -type "float3" 0 0.2643894 1.9073486e-06 ;
	setAttr ".tk[637]" -type "float3" 0 0.1001384 0 ;
	setAttr ".tk[638]" -type "float3" 0 -0.081063665 0 ;
	setAttr ".tk[639]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[640]" -type "float3" 0 -0.15465258 0 ;
	setAttr ".tk[641]" -type "float3" 0 0.26340318 0 ;
	setAttr ".tk[642]" -type "float3" 0 0.087585621 0 ;
	setAttr ".tk[643]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[644]" -type "float3" 0 -0.15465258 0 ;
	setAttr ".tk[645]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[646]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[647]" -type "float3" 0 0.26439512 9.5367432e-07 ;
	setAttr ".tk[648]" -type "float3" 0 0.26439229 9.5367432e-07 ;
	setAttr ".tk[649]" -type "float3" 0 0.021699417 0 ;
	setAttr ".tk[650]" -type "float3" 0 0.021699417 0 ;
	setAttr ".tk[651]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[652]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[653]" -type "float3" 0 0.2643894 9.5367432e-07 ;
	setAttr ".tk[654]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[655]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[656]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[657]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[658]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[659]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[660]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[661]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[662]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[663]" -type "float3" 0 0.26438758 9.5367432e-07 ;
	setAttr ".tk[664]" -type "float3" 0 0.2643885 9.5367432e-07 ;
	setAttr ".tk[665]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[666]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[667]" -type "float3" 0 0.2643885 0 ;
	setAttr ".tk[668]" -type "float3" 0 0.26438582 9.5367432e-07 ;
	setAttr ".tk[669]" -type "float3" 0 0.26439035 1.9073486e-06 ;
	setAttr ".tk[670]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[671]" -type "float3" 0 0.2643894 9.5367432e-07 ;
	setAttr ".tk[672]" -type "float3" 0 0.26438582 9.5367432e-07 ;
	setAttr ".tk[673]" -type "float3" 0 0.26439035 1.9073486e-06 ;
	setAttr ".tk[674]" -type "float3" 0 0.2643885 1.9073486e-06 ;
	setAttr ".tk[675]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[676]" -type "float3" 0 0.15223205 0 ;
	setAttr ".tk[677]" -type "float3" 0 0.15229997 9.5367432e-07 ;
	setAttr ".tk[678]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[679]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[680]" -type "float3" 0 0.15226483 9.5367432e-07 ;
	setAttr ".tk[681]" -type "float3" 0 0.15226577 9.5367432e-07 ;
	setAttr ".tk[682]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[683]" -type "float3" 0 0.15226577 9.5367432e-07 ;
	setAttr ".tk[684]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[685]" -type "float3" 0 0.15235931 0 ;
	setAttr ".tk[686]" -type "float3" 0 0.15222539 0 ;
	setAttr ".tk[687]" -type "float3" 0 0.15221393 9.5367432e-07 ;
	setAttr ".tk[688]" -type "float3" 0 0.1523132 0 ;
	setAttr ".tk[689]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[690]" -type "float3" 0 0.15229997 9.5367432e-07 ;
	setAttr ".tk[691]" -type "float3" 0 0.15223405 0 ;
	setAttr ".tk[692]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[693]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[694]" -type "float3" 0 0.15226483 9.5367432e-07 ;
	setAttr ".tk[695]" -type "float3" 0 0.15226667 9.5367432e-07 ;
	setAttr ".tk[696]" -type "float3" 0 0.15226667 9.5367432e-07 ;
	setAttr ".tk[697]" -type "float3" 0 0.15217853 9.5367432e-07 ;
	setAttr ".tk[698]" -type "float3" 0 0.1523222 0 ;
	setAttr ".tk[699]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[700]" -type "float3" 0 0.15226196 0 ;
	setAttr ".tk[701]" -type "float3" 0 0.1523132 0 ;
	setAttr ".tk[702]" -type "float3" 0 0.15222539 0 ;
	setAttr ".tk[703]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[704]" -type "float3" 0 0.16621932 9.5367432e-07 ;
	setAttr ".tk[705]" -type "float3" 0 0.16622058 9.5367432e-07 ;
	setAttr ".tk[706]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[707]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[708]" -type "float3" 0 0.16622242 9.5367432e-07 ;
	setAttr ".tk[709]" -type "float3" 0 0.16621932 9.5367432e-07 ;
	setAttr ".tk[710]" -type "float3" 0 0.16621932 9.5367432e-07 ;
	setAttr ".tk[711]" -type "float3" 0 0.16622242 0 ;
	setAttr ".tk[712]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[713]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[714]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[715]" -type "float3" 0 0.16622153 0 ;
	setAttr ".tk[716]" -type "float3" 0 0.16622153 0 ;
createNode polyMergeVert -n "polyMergeVert45";
	rename -uid "E2BDFAB5-4EA7-6546-E902-3CADEB393D36";
	setAttr ".ics" -type "componentList" 39 "vtx[0:1]" "vtx[6]" "vtx[19]" "vtx[23]" "vtx[25:27]" "vtx[39]" "vtx[43:44]" "vtx[47]" "vtx[57:58]" "vtx[60:64]" "vtx[71:72]" "vtx[79:80]" "vtx[87:88]" "vtx[95]" "vtx[126:127]" "vtx[130:131]" "vtx[146:147]" "vtx[152:153]" "vtx[158:159]" "vtx[162:163]" "vtx[186:187]" "vtx[214:217]" "vtx[244:247]" "vtx[274:277]" "vtx[304:305]" "vtx[333:335]" "vtx[339:341]" "vtx[363:365]" "vtx[372:374]" "vtx[381:383]" "vtx[387:389]" "vtx[396:398]" "vtx[402:404]" "vtx[408:410]" "vtx[414:416]" "vtx[501:503]" "vtx[540:542]" "vtx[579:581]" "vtx[618:715]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.0672064754216544 0 0 0 0 0.98692533935277804 0
		 0 -1.65922442784799 -0.28623479486675407 1;
	setAttr ".am" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :standardSurface1;
	setAttr ".bc" -type "float3" 0.40000001 0.40000001 0.40000001 ;
	setAttr ".sr" 0.5;
select -ne :initialShadingGroup;
	setAttr -s 20 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :initialMaterialInfo;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "standardSurface1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV2.out" "backboardShape.i";
connectAttr "polyTweakUV2.uvtk[0]" "backboardShape.uvst[0].uvtw";
connectAttr "polyTweakUV64.out" "baseboard_frontShape.i";
connectAttr "polyTweakUV64.uvtk[0]" "baseboard_frontShape.uvst[0].uvtw";
connectAttr "polyMergeVert45.out" "mattressShape.i";
connectAttr "polyTweakUV81.uvtk[0]" "mattressShape.uvst[0].uvtw";
connectAttr "polyTweakUV65.out" "back_leg_RShape.i";
connectAttr "polyTweakUV65.uvtk[0]" "back_leg_RShape.uvst[0].uvtw";
connectAttr "polyTweakUV6.out" "sideboard_RShape.i";
connectAttr "polyTweakUV6.uvtk[0]" "sideboard_RShape.uvst[0].uvtw";
connectAttr "polyTweakUV14.out" "front_leg_RShape.i";
connectAttr "polyTweakUV14.uvtk[0]" "front_leg_RShape.uvst[0].uvtw";
connectAttr "polyTweakUV23.out" "back_leg_LShape.i";
connectAttr "polyTweakUV23.uvtk[0]" "back_leg_LShape.uvst[0].uvtw";
connectAttr "polyTweakUV4.out" "sideboard_LShape.i";
connectAttr "polyTweakUV4.uvtk[0]" "sideboard_LShape.uvst[0].uvtw";
connectAttr "polyTweakUV10.out" "front_leg_LShape.i";
connectAttr "polyTweakUV10.uvtk[0]" "front_leg_LShape.uvst[0].uvtw";
connectAttr "polyBevel9.out" "pillowShape3.i";
connectAttr "deleteComponent4.og" "round_pillow_largeShape.i";
connectAttr "polyBevel7.out" "pillowShape1.i";
connectAttr "polyTweakUV67.out" "baseboardShape.i";
connectAttr "polyTweakUV67.uvtk[0]" "baseboardShape.uvst[0].uvtw";
connectAttr "groupId3.id" "pCubeShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupId4.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pCubeShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupId2.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "polyTweakUV66.out" "pCube3Shape.i";
connectAttr "groupId5.id" "pCube3Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCube3Shape.iog.og[0].gco";
connectAttr "groupId6.id" "pCube3Shape.iog.og[2].gid";
connectAttr "set1.mwc" "pCube3Shape.iog.og[2].gco";
connectAttr "groupId7.id" "pCube3Shape.iog.og[3].gid";
connectAttr "set2.mwc" "pCube3Shape.iog.og[3].gco";
connectAttr "groupId8.id" "pCube3Shape.iog.og[4].gid";
connectAttr "set3.mwc" "pCube3Shape.iog.og[4].gco";
connectAttr "groupId9.id" "pCube3Shape.iog.og[5].gid";
connectAttr "set4.mwc" "pCube3Shape.iog.og[5].gco";
connectAttr "polyTweakUV66.uvtk[0]" "pCube3Shape.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "polySurfaceShape1.o" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "pasted__polyCube3.out" "transformGeometry2.ig";
connectAttr "polyCube3.out" "transformGeometry3.ig";
connectAttr "polySplit3.out" "transformGeometry4.ig";
connectAttr "transformGeometry4.og" "polyTweak1.ip";
connectAttr "polyTweak1.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polySplit9.ip";
connectAttr "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_L_grp|sideboard_L|polySurfaceShape2.o" "polySplit10.ip"
		;
connectAttr "polySplit10.out" "polySplit11.ip";
connectAttr "polySplit11.out" "polySplit12.ip";
connectAttr "polySplit12.out" "polySplit13.ip";
connectAttr "polySplit13.out" "polySplit14.ip";
connectAttr "polySplit14.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "polySplit15.ip";
connectAttr "polySplit15.out" "polyTweak5.ip";
connectAttr "polyTweak5.out" "polySplit16.ip";
connectAttr "transformGeometry2.og" "polySplit17.ip";
connectAttr "polySplit17.out" "polySplit18.ip";
connectAttr "polySplit18.out" "polySplit19.ip";
connectAttr "polySplit19.out" "polySplit20.ip";
connectAttr "polySplit20.out" "polySplit21.ip";
connectAttr "polySplit21.out" "polySplit22.ip";
connectAttr "polyTweak6.out" "polyBevel1.ip";
connectAttr "mattressShape.wm" "polyBevel1.mp";
connectAttr "polySplit22.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polyDelEdge1.ip";
connectAttr "polyBevel1.out" "polyTweak7.ip";
connectAttr "polyDelEdge1.out" "polyDelEdge2.ip";
connectAttr "polyDelEdge2.out" "polyDelEdge3.ip";
connectAttr "polyDelEdge3.out" "polyDelEdge4.ip";
connectAttr "polyDelEdge4.out" "polyDelEdge5.ip";
connectAttr "polySurfaceShape3.o" "polyBevel2.ip";
connectAttr "baseboard_frontShape.wm" "polyBevel2.mp";
connectAttr "polyCube4.out" "polyBevel3.ip";
connectAttr "front_leg_LShape.wm" "polyBevel3.mp";
connectAttr "|daybed_003_daybed_grp|POS|MOV|ADJ|daybed_geo_grp|board_L_grp|back_leg_L|polySurfaceShape4.o" "polyBevel4.ip"
		;
connectAttr "back_leg_LShape.wm" "polyBevel4.mp";
connectAttr "polyTweak8.out" "polyBevel5.ip";
connectAttr "sideboard_LShape.wm" "polyBevel5.mp";
connectAttr "polySplit16.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polyBevel6.ip";
connectAttr "backboardShape.wm" "polyBevel6.mp";
connectAttr "polySplit9.out" "polyTweak9.ip";
connectAttr "transformGeometry3.og" "deleteComponent1.ig";
connectAttr "polyBevel5.out" "deleteComponent2.ig";
connectAttr "polyBevel6.out" "deleteComponent3.ig";
connectAttr "polyCube5.out" "polySplit23.ip";
connectAttr "polySplit23.out" "polySplit24.ip";
connectAttr "polySplit24.out" "polyTweak10.ip";
connectAttr "polyTweak10.out" "polySplit25.ip";
connectAttr "polySplit25.out" "polySplit26.ip";
connectAttr "polySplit26.out" "polySplit27.ip";
connectAttr "polySplit27.out" "polySplit28.ip";
connectAttr "polyTweak11.out" "polyBevel7.ip";
connectAttr "pillowShape1.wm" "polyBevel7.mp";
connectAttr "polySplit28.out" "polyTweak11.ip";
connectAttr "polyCylinder1.out" "polyBevel8.ip";
connectAttr "round_pillow_largeShape.wm" "polyBevel8.mp";
connectAttr "polyBevel8.out" "polySplit29.ip";
connectAttr "polySplit29.out" "polySplit30.ip";
connectAttr "polySplit30.out" "polySplit31.ip";
connectAttr "polyTweak12.out" "polyDelEdge6.ip";
connectAttr "polySplit31.out" "polyTweak12.ip";
connectAttr "polyDelEdge6.out" "deleteComponent4.ig";
connectAttr "polySurfaceShape5.o" "polyMergeVert1.ip";
connectAttr "pillowShape3.wm" "polyMergeVert1.mp";
connectAttr "polyTweak13.out" "polyMergeVert2.ip";
connectAttr "pillowShape3.wm" "polyMergeVert2.mp";
connectAttr "polyMergeVert1.out" "polyTweak13.ip";
connectAttr "polyTweak14.out" "polyMergeVert3.ip";
connectAttr "pillowShape3.wm" "polyMergeVert3.mp";
connectAttr "polyMergeVert2.out" "polyTweak14.ip";
connectAttr "polyTweak15.out" "polyMergeVert4.ip";
connectAttr "pillowShape3.wm" "polyMergeVert4.mp";
connectAttr "polyMergeVert3.out" "polyTweak15.ip";
connectAttr "polyTweak16.out" "polyMergeVert5.ip";
connectAttr "pillowShape3.wm" "polyMergeVert5.mp";
connectAttr "polyMergeVert4.out" "polyTweak16.ip";
connectAttr "polyTweak17.out" "polyMergeVert6.ip";
connectAttr "pillowShape3.wm" "polyMergeVert6.mp";
connectAttr "polyMergeVert5.out" "polyTweak17.ip";
connectAttr "polyTweak18.out" "polyMergeVert7.ip";
connectAttr "pillowShape3.wm" "polyMergeVert7.mp";
connectAttr "polyMergeVert6.out" "polyTweak18.ip";
connectAttr "polyTweak19.out" "polyMergeVert8.ip";
connectAttr "pillowShape3.wm" "polyMergeVert8.mp";
connectAttr "polyMergeVert7.out" "polyTweak19.ip";
connectAttr "polyTweak20.out" "polyMergeVert9.ip";
connectAttr "pillowShape3.wm" "polyMergeVert9.mp";
connectAttr "polyMergeVert8.out" "polyTweak20.ip";
connectAttr "polyTweak21.out" "polyMergeVert10.ip";
connectAttr "pillowShape3.wm" "polyMergeVert10.mp";
connectAttr "polyMergeVert9.out" "polyTweak21.ip";
connectAttr "polyTweak22.out" "polyMergeVert11.ip";
connectAttr "pillowShape3.wm" "polyMergeVert11.mp";
connectAttr "polyMergeVert10.out" "polyTweak22.ip";
connectAttr "polyTweak23.out" "polyMergeVert12.ip";
connectAttr "pillowShape3.wm" "polyMergeVert12.mp";
connectAttr "polyMergeVert11.out" "polyTweak23.ip";
connectAttr "polyTweak24.out" "polyMergeVert13.ip";
connectAttr "pillowShape3.wm" "polyMergeVert13.mp";
connectAttr "polyMergeVert12.out" "polyTweak24.ip";
connectAttr "polyTweak25.out" "polyMergeVert14.ip";
connectAttr "pillowShape3.wm" "polyMergeVert14.mp";
connectAttr "polyMergeVert13.out" "polyTweak25.ip";
connectAttr "polyTweak26.out" "polyMergeVert15.ip";
connectAttr "pillowShape3.wm" "polyMergeVert15.mp";
connectAttr "polyMergeVert14.out" "polyTweak26.ip";
connectAttr "polyTweak27.out" "polyMergeVert16.ip";
connectAttr "pillowShape3.wm" "polyMergeVert16.mp";
connectAttr "polyMergeVert15.out" "polyTweak27.ip";
connectAttr "polyTweak28.out" "polyMergeVert17.ip";
connectAttr "pillowShape3.wm" "polyMergeVert17.mp";
connectAttr "polyMergeVert16.out" "polyTweak28.ip";
connectAttr "polyTweak29.out" "polyMergeVert18.ip";
connectAttr "pillowShape3.wm" "polyMergeVert18.mp";
connectAttr "polyMergeVert17.out" "polyTweak29.ip";
connectAttr "polyTweak30.out" "polyMergeVert19.ip";
connectAttr "pillowShape3.wm" "polyMergeVert19.mp";
connectAttr "polyMergeVert18.out" "polyTweak30.ip";
connectAttr "polyTweak31.out" "polyMergeVert20.ip";
connectAttr "pillowShape3.wm" "polyMergeVert20.mp";
connectAttr "polyMergeVert19.out" "polyTweak31.ip";
connectAttr "polyTweak32.out" "polyMergeVert21.ip";
connectAttr "pillowShape3.wm" "polyMergeVert21.mp";
connectAttr "polyMergeVert20.out" "polyTweak32.ip";
connectAttr "polyTweak33.out" "polyMergeVert22.ip";
connectAttr "pillowShape3.wm" "polyMergeVert22.mp";
connectAttr "polyMergeVert21.out" "polyTweak33.ip";
connectAttr "polyTweak34.out" "polyMergeVert23.ip";
connectAttr "pillowShape3.wm" "polyMergeVert23.mp";
connectAttr "polyMergeVert22.out" "polyTweak34.ip";
connectAttr "polyTweak35.out" "polyMergeVert24.ip";
connectAttr "pillowShape3.wm" "polyMergeVert24.mp";
connectAttr "polyMergeVert23.out" "polyTweak35.ip";
connectAttr "polyTweak36.out" "polyMergeVert25.ip";
connectAttr "pillowShape3.wm" "polyMergeVert25.mp";
connectAttr "polyMergeVert24.out" "polyTweak36.ip";
connectAttr "polyTweak37.out" "polyMergeVert26.ip";
connectAttr "pillowShape3.wm" "polyMergeVert26.mp";
connectAttr "polyMergeVert25.out" "polyTweak37.ip";
connectAttr "polyTweak38.out" "polyMergeVert27.ip";
connectAttr "pillowShape3.wm" "polyMergeVert27.mp";
connectAttr "polyMergeVert26.out" "polyTweak38.ip";
connectAttr "polyTweak39.out" "polyMergeVert28.ip";
connectAttr "pillowShape3.wm" "polyMergeVert28.mp";
connectAttr "polyMergeVert27.out" "polyTweak39.ip";
connectAttr "polyTweak40.out" "polyMergeVert29.ip";
connectAttr "pillowShape3.wm" "polyMergeVert29.mp";
connectAttr "polyMergeVert28.out" "polyTweak40.ip";
connectAttr "polyTweak41.out" "polyMergeVert30.ip";
connectAttr "pillowShape3.wm" "polyMergeVert30.mp";
connectAttr "polyMergeVert29.out" "polyTweak41.ip";
connectAttr "polyTweak42.out" "polyMergeVert31.ip";
connectAttr "pillowShape3.wm" "polyMergeVert31.mp";
connectAttr "polyMergeVert30.out" "polyTweak42.ip";
connectAttr "polyTweak43.out" "polyMergeVert32.ip";
connectAttr "pillowShape3.wm" "polyMergeVert32.mp";
connectAttr "polyMergeVert31.out" "polyTweak43.ip";
connectAttr "polyMergeVert32.out" "polyBevel9.ip";
connectAttr "pillowShape3.wm" "polyBevel9.mp";
connectAttr "deleteComponent3.og" "polyAutoProj1.ip";
connectAttr "backboardShape.wm" "polyAutoProj1.mp";
connectAttr "polyAutoProj1.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV2.ip";
connectAttr "deleteComponent2.og" "polyAutoProj2.ip";
connectAttr "sideboard_LShape.wm" "polyAutoProj2.mp";
connectAttr "polyAutoProj2.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyTweakUV4.ip";
connectAttr "polySurfaceShape6.o" "polyAutoProj3.ip";
connectAttr "sideboard_RShape.wm" "polyAutoProj3.mp";
connectAttr "polyAutoProj3.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyTweakUV6.ip";
connectAttr "polyBevel2.out" "polyAutoProj4.ip";
connectAttr "baseboard_frontShape.wm" "polyAutoProj4.mp";
connectAttr "polyAutoProj4.out" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "polyCloseBorder1.ip";
connectAttr "polyCloseBorder1.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "deleteComponent6.ig";
connectAttr "deleteComponent6.og" "deleteComponent7.ig";
connectAttr "polyDelEdge5.out" "polyTweak44.ip";
connectAttr "polyTweak44.out" "deleteComponent8.ig";
connectAttr "polyBevel3.out" "polyAutoProj5.ip";
connectAttr "front_leg_LShape.wm" "polyAutoProj5.mp";
connectAttr "polyAutoProj5.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyTweakUV10.ip";
connectAttr "polySurfaceShape7.o" "polyAutoProj6.ip";
connectAttr "front_leg_RShape.wm" "polyAutoProj6.mp";
connectAttr "polyAutoProj6.out" "polyTweakUV11.ip";
connectAttr "polyTweakUV11.out" "polyMapSewMove7.ip";
connectAttr "polyMapSewMove7.out" "polyTweakUV12.ip";
connectAttr "polyTweakUV12.out" "polyMapSewMove8.ip";
connectAttr "polyMapSewMove8.out" "polyTweakUV13.ip";
connectAttr "polyTweakUV13.out" "polyMapSewMove9.ip";
connectAttr "polyMapSewMove9.out" "polyTweakUV14.ip";
connectAttr "polySurfaceShape8.o" "polyAutoProj7.ip";
connectAttr "back_leg_RShape.wm" "polyAutoProj7.mp";
connectAttr "polyAutoProj7.out" "polyTweakUV15.ip";
connectAttr "polyTweakUV15.out" "polyMapSewMove10.ip";
connectAttr "polyMapSewMove10.out" "polyTweakUV16.ip";
connectAttr "polyTweakUV16.out" "polyMapSewMove11.ip";
connectAttr "polyMapSewMove11.out" "polyTweakUV17.ip";
connectAttr "polyTweakUV17.out" "polyMapSewMove12.ip";
connectAttr "polyMapSewMove12.out" "polyTweakUV18.ip";
connectAttr "polyBevel4.out" "polyAutoProj8.ip";
connectAttr "back_leg_LShape.wm" "polyAutoProj8.mp";
connectAttr "polyAutoProj8.out" "polyTweakUV19.ip";
connectAttr "polyTweakUV19.out" "polyMapSewMove13.ip";
connectAttr "polyMapSewMove13.out" "polyTweakUV20.ip";
connectAttr "polyTweakUV20.out" "polyMapSewMove14.ip";
connectAttr "polyMapSewMove14.out" "polyTweakUV21.ip";
connectAttr "polyTweakUV21.out" "polyMapSewMove15.ip";
connectAttr "polyMapSewMove15.out" "polyTweakUV22.ip";
connectAttr "polyTweakUV22.out" "polyLayoutUV1.ip";
connectAttr "polyLayoutUV1.out" "polyLayoutUV2.ip";
connectAttr "polyLayoutUV2.out" "polyTweakUV23.ip";
connectAttr "polyTweakUV18.out" "polyLayoutUV3.ip";
connectAttr "polyLayoutUV3.out" "polyLayoutUV4.ip";
connectAttr "deleteComponent7.og" "polyAutoProj9.ip";
connectAttr "baseboard_frontShape.wm" "polyAutoProj9.mp";
connectAttr "polyAutoProj9.out" "polyTweakUV24.ip";
connectAttr "polyTweakUV24.out" "polyMapSewMove16.ip";
connectAttr "polyMapSewMove16.out" "polyTweakUV25.ip";
connectAttr "polyTweakUV25.out" "polyMapSewMove17.ip";
connectAttr "polyMapSewMove17.out" "polyTweakUV26.ip";
connectAttr "polyTweakUV26.out" "polyMapSewMove18.ip";
connectAttr "deleteComponent8.og" "polyTweak57.ip";
connectAttr "polyTweak57.out" "polySplit46.ip";
connectAttr "polySplit46.out" "polySplit47.ip";
connectAttr "polySplit47.out" "polySplit48.ip";
connectAttr "polySplit48.out" "polySplit49.ip";
connectAttr "polySplit49.out" "polyTweak58.ip";
connectAttr "polyTweak58.out" "polySplit50.ip";
connectAttr "polySplit50.out" "polySplit51.ip";
connectAttr "polySplit51.out" "polySplit52.ip";
connectAttr "polySplit52.out" "polySplit53.ip";
connectAttr "polySplit53.out" "polyBevel11.ip";
connectAttr "mattressShape.wm" "polyBevel11.mp";
connectAttr "polyTweak59.out" "polyBevel12.ip";
connectAttr "mattressShape.wm" "polyBevel12.mp";
connectAttr "polyBevel11.out" "polyTweak59.ip";
connectAttr "pCubeShape2.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape1.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape2.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[1]";
connectAttr "polyUnite1.out" "groupParts1.ig";
connectAttr "groupId5.id" "groupParts1.gi";
connectAttr "groupParts1.og" "polyMergeVert33.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert33.mp";
connectAttr "polyMergeVert33.out" "polyTweak60.ip";
connectAttr "polyTweak60.out" "polySplit54.ip";
connectAttr "polySplit54.out" "polySplit55.ip";
connectAttr "polySplit55.out" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "deleteComponent10.ig";
connectAttr "polyTweak61.out" "polyMergeVert34.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert34.mp";
connectAttr "deleteComponent10.og" "polyTweak61.ip";
connectAttr "polyTweak62.out" "polyMergeVert35.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert35.mp";
connectAttr "polyMergeVert34.out" "polyTweak62.ip";
connectAttr "polyTweak63.out" "polyMergeVert36.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert36.mp";
connectAttr "polyMergeVert35.out" "polyTweak63.ip";
connectAttr "polyTweak64.out" "polyMergeVert37.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert37.mp";
connectAttr "polyMergeVert36.out" "polyTweak64.ip";
connectAttr "polyTweak65.out" "polyMergeVert38.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert38.mp";
connectAttr "polyMergeVert37.out" "polyTweak65.ip";
connectAttr "polyTweak66.out" "polyMergeVert39.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert39.mp";
connectAttr "polyMergeVert38.out" "polyTweak66.ip";
connectAttr "polyTweak67.out" "polyMergeVert40.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert40.mp";
connectAttr "polyMergeVert39.out" "polyTweak67.ip";
connectAttr "polyTweak68.out" "polyMergeVert41.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert41.mp";
connectAttr "polyMergeVert40.out" "polyTweak68.ip";
connectAttr "polyTweak69.out" "polyMergeVert42.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert42.mp";
connectAttr "polyMergeVert41.out" "polyTweak69.ip";
connectAttr "polyTweak70.out" "polyMergeVert43.ip";
connectAttr "pCube3Shape.wm" "polyMergeVert43.mp";
connectAttr "polyMergeVert42.out" "polyTweak70.ip";
connectAttr "polyMergeVert43.out" "polyAutoProj10.ip";
connectAttr "pCube3Shape.wm" "polyAutoProj10.mp";
connectAttr "polyAutoProj10.out" "polyMapCut2.ip";
connectAttr "polyMapCut2.out" "polyMapCut3.ip";
connectAttr "polyMapCut3.out" "polyMapCut4.ip";
connectAttr "polyMapCut4.out" "polyMapCut5.ip";
connectAttr "polyMapCut5.out" "polyMapCut6.ip";
connectAttr "polyMapCut6.out" "polyMapCut7.ip";
connectAttr "polyMapCut7.out" "polyMapCut8.ip";
connectAttr "polyMapCut8.out" "polyMapCut9.ip";
connectAttr "polyMapCut9.out" "polyMapCut10.ip";
connectAttr "polyMapCut10.out" "polyMapCut11.ip";
connectAttr "polyMapCut11.out" "polyMapCut12.ip";
connectAttr "polyMapCut12.out" "polyDelEdge7.ip";
connectAttr "polyDelEdge7.out" "polySplit56.ip";
connectAttr "polySplit56.out" "polyDelEdge8.ip";
connectAttr "polyDelEdge8.out" "polySplit57.ip";
connectAttr "polySplit57.out" "polyTweakUV27.ip";
connectAttr "polyTweakUV27.out" "polyMapSewMove19.ip";
connectAttr "polyMapSewMove19.out" "polyTweakUV28.ip";
connectAttr "polyTweakUV28.out" "polyMapSewMove20.ip";
connectAttr "polyMapSewMove20.out" "polyTweakUV29.ip";
connectAttr "polyTweakUV29.out" "polyMapSewMove21.ip";
connectAttr "polyMapSewMove21.out" "polyTweakUV30.ip";
connectAttr "polyTweakUV30.out" "polyMapSewMove22.ip";
connectAttr "polyMapSewMove22.out" "polyMapCut13.ip";
connectAttr "polyMapCut13.out" "polyTweakUV31.ip";
connectAttr "polyTweakUV31.out" "polyMapSewMove23.ip";
connectAttr "polyMapSewMove23.out" "polyMapCut14.ip";
connectAttr "polyMapCut14.out" "polyMapCut15.ip";
connectAttr "polyMapCut15.out" "polyTweakUV32.ip";
connectAttr "polyTweakUV32.out" "polyMapSewMove24.ip";
connectAttr "polyMapSewMove24.out" "polyTweakUV33.ip";
connectAttr "polyTweakUV33.out" "polyMapSewMove25.ip";
connectAttr "polyMapSewMove25.out" "polyMapCut16.ip";
connectAttr "polyMapCut16.out" "polyTweakUV34.ip";
connectAttr "polyTweakUV34.out" "polyMapSewMove26.ip";
connectAttr "polyMapSewMove26.out" "polyTweakUV35.ip";
connectAttr "polyTweakUV35.out" "polyMapSewMove27.ip";
connectAttr "polyMapSewMove27.out" "polyTweakUV36.ip";
connectAttr "polyTweakUV36.out" "polyMapSewMove28.ip";
connectAttr "polyMapSewMove28.out" "polyMapCut17.ip";
connectAttr "polyMapCut17.out" "polyTweakUV37.ip";
connectAttr "polyTweakUV37.out" "polyMapSewMove29.ip";
connectAttr "polyMapSewMove29.out" "polyMapSewMove30.ip";
connectAttr "polyMapSewMove30.out" "polyMapCut18.ip";
connectAttr "polyMapCut18.out" "polyTweakUV38.ip";
connectAttr "polyTweakUV38.out" "polyMapSewMove31.ip";
connectAttr "polyMapSewMove31.out" "polyTweakUV39.ip";
connectAttr "polyTweakUV39.out" "polyMapSewMove32.ip";
connectAttr "polyMapSewMove32.out" "polyTweakUV40.ip";
connectAttr "polyTweakUV40.out" "polyMapSewMove33.ip";
connectAttr "polyMapSewMove33.out" "polyTweakUV41.ip";
connectAttr "polyTweakUV41.out" "polyMapSewMove34.ip";
connectAttr "polyMapSewMove34.out" "polyTweakUV42.ip";
connectAttr "polyTweakUV42.out" "polyMapSewMove35.ip";
connectAttr "polyMapSewMove35.out" "polyTweakUV43.ip";
connectAttr "polyTweakUV43.out" "polyMapSewMove36.ip";
connectAttr "polyMapSewMove36.out" "polyTweakUV44.ip";
connectAttr "polyTweakUV44.out" "polyMapSewMove37.ip";
connectAttr "polyMapSewMove37.out" "polyTweakUV45.ip";
connectAttr "polyTweakUV45.out" "polyMapSewMove38.ip";
connectAttr "polyMapSewMove38.out" "polyTweakUV46.ip";
connectAttr "polyTweakUV46.out" "polyMapSewMove39.ip";
connectAttr "polyMapSewMove39.out" "polyTweakUV47.ip";
connectAttr "polyTweakUV47.out" "polyMapSewMove40.ip";
connectAttr "polyMapSewMove40.out" "polyTweakUV48.ip";
connectAttr "polyTweakUV48.out" "polyMapSewMove41.ip";
connectAttr "polyMapSewMove41.out" "polyTweakUV49.ip";
connectAttr "polyTweakUV49.out" "polyMapSewMove42.ip";
connectAttr "polyMapSewMove42.out" "polyTweakUV50.ip";
connectAttr "polyTweakUV50.out" "polyMapSewMove43.ip";
connectAttr "polyMapSewMove43.out" "polyTweakUV51.ip";
connectAttr "polyTweakUV51.out" "polyMapSewMove44.ip";
connectAttr "polyMapSewMove44.out" "polyTweakUV52.ip";
connectAttr "polyTweakUV52.out" "polyMapSewMove45.ip";
connectAttr "polyMapSewMove45.out" "polyTweakUV53.ip";
connectAttr "polyTweakUV53.out" "polyMapSewMove46.ip";
connectAttr "polyMapSewMove46.out" "polyTweakUV54.ip";
connectAttr "polyTweakUV54.out" "polyMapSewMove47.ip";
connectAttr "polyMapSewMove47.out" "polyTweakUV55.ip";
connectAttr "polyTweakUV55.out" "polyMapSewMove48.ip";
connectAttr "polyMapSewMove48.out" "polyTweakUV56.ip";
connectAttr "polyTweakUV56.out" "polyMapSewMove49.ip";
connectAttr "polyMapSewMove49.out" "polyTweakUV57.ip";
connectAttr "polyTweakUV57.out" "polyMapSewMove50.ip";
connectAttr "polyMapSewMove50.out" "polyDelEdge9.ip";
connectAttr "polyDelEdge9.out" "deleteComponent11.ig";
connectAttr "groupId6.msg" "set1.gn" -na;
connectAttr "pCube3Shape.iog.og[2]" "set1.dsm" -na;
connectAttr "deleteComponent11.og" "groupParts2.ig";
connectAttr "groupId6.id" "groupParts2.gi";
connectAttr "groupParts2.og" "deleteComponent12.ig";
connectAttr "groupId7.msg" "set2.gn" -na;
connectAttr "pCube3Shape.iog.og[3]" "set2.dsm" -na;
connectAttr "deleteComponent12.og" "groupParts3.ig";
connectAttr "groupId7.id" "groupParts3.gi";
connectAttr "groupParts3.og" "deleteComponent13.ig";
connectAttr "deleteComponent13.og" "polyMapSewMove51.ip";
connectAttr "polyMapSewMove51.out" "polyMapCut19.ip";
connectAttr "polyMapCut19.out" "polyMapCut20.ip";
connectAttr "polyMapCut20.out" "polyMapCut21.ip";
connectAttr "polyMapCut21.out" "polyMapCut22.ip";
connectAttr "polyMapCut22.out" "polySplit58.ip";
connectAttr "polySplit58.out" "polySplit59.ip";
connectAttr "polySplit59.out" "polyMapCut23.ip";
connectAttr "polyMapCut23.out" "polyDelEdge10.ip";
connectAttr "polyDelEdge10.out" "polyDelEdge11.ip";
connectAttr "polyDelEdge11.out" "polyDelEdge12.ip";
connectAttr "polyDelEdge12.out" "polyDelEdge13.ip";
connectAttr "polyDelEdge13.out" "polyMapCut24.ip";
connectAttr "polyMapCut24.out" "polyMapCut25.ip";
connectAttr "polyMapCut25.out" "polyMapCut26.ip";
connectAttr "polyMapCut26.out" "polyTweakUV58.ip";
connectAttr "polyTweakUV58.out" "polyMapSewMove52.ip";
connectAttr "polyMapSewMove52.out" "polyTweakUV59.ip";
connectAttr "polyTweakUV59.out" "polyMapSewMove53.ip";
connectAttr "polyMapSewMove53.out" "polyDelEdge14.ip";
connectAttr "polyDelEdge14.out" "polyMapCut27.ip";
connectAttr "polyMapCut27.out" "polyMapSewMove54.ip";
connectAttr "polyMapSewMove54.out" "polyMapCut28.ip";
connectAttr "polyMapCut28.out" "polyNormal1.ip";
connectAttr "polyNormal1.out" "polySplitEdge1.ip";
connectAttr "polySplitEdge1.out" "polySplitVert1.ip";
connectAttr "polySplitVert1.out" "polyMapCut29.ip";
connectAttr "polyMapCut29.out" "polyTweakUV60.ip";
connectAttr "polyTweakUV60.out" "polyMapCut30.ip";
connectAttr "polyMapCut30.out" "polyTweakUV61.ip";
connectAttr "polyTweakUV61.out" "polyMapSewMove55.ip";
connectAttr "polyMapSewMove55.out" "polyTweakUV62.ip";
connectAttr "groupId8.msg" "set3.gn" -na;
connectAttr "pCube3Shape.iog.og[4]" "set3.dsm" -na;
connectAttr "polyTweakUV62.out" "groupParts4.ig";
connectAttr "groupId8.id" "groupParts4.gi";
connectAttr "groupParts4.og" "deleteComponent14.ig";
connectAttr "groupId9.msg" "set4.gn" -na;
connectAttr "pCube3Shape.iog.og[5]" "set4.dsm" -na;
connectAttr "deleteComponent14.og" "groupParts5.ig";
connectAttr "groupId9.id" "groupParts5.gi";
connectAttr "groupParts5.og" "deleteComponent15.ig";
connectAttr "deleteComponent15.og" "polyTweakUV63.ip";
connectAttr "polyMapSewMove18.out" "polyTweakUV64.ip";
connectAttr "polyLayoutUV4.out" "polyTweakUV65.ip";
connectAttr "polyTweakUV63.out" "polyMapSewMove56.ip";
connectAttr "polyMapSewMove56.out" "polyTweakUV66.ip";
connectAttr "deleteComponent1.og" "polyTweakUV67.ip";
connectAttr "polyTweak71.out" "polyAutoProj11.ip";
connectAttr "mattressShape.wm" "polyAutoProj11.mp";
connectAttr "polyBevel12.out" "polyTweak71.ip";
connectAttr "polyAutoProj11.out" "polyMapCut31.ip";
connectAttr "polyMapCut31.out" "polyMapCut32.ip";
connectAttr "polyMapCut32.out" "polyMapCut33.ip";
connectAttr "polyMapCut33.out" "polyMapCut34.ip";
connectAttr "polyMapCut34.out" "polyMapCut35.ip";
connectAttr "polyMapCut35.out" "polyMapCut36.ip";
connectAttr "polyMapCut36.out" "polyMapCut37.ip";
connectAttr "polyMapCut37.out" "polyMapCut38.ip";
connectAttr "polyMapCut38.out" "polyTweakUV68.ip";
connectAttr "polyTweakUV68.out" "polyMapSewMove57.ip";
connectAttr "polyMapSewMove57.out" "polyTweakUV69.ip";
connectAttr "polyTweakUV69.out" "polyMapSewMove58.ip";
connectAttr "polyMapSewMove58.out" "polyMapCut39.ip";
connectAttr "polyMapCut39.out" "polyMapSew1.ip";
connectAttr "polyMapSew1.out" "polyTweakUV70.ip";
connectAttr "polyTweakUV70.out" "polyMapSewMove59.ip";
connectAttr "polyMapSewMove59.out" "polyTweakUV71.ip";
connectAttr "polyTweakUV71.out" "polyMapSewMove60.ip";
connectAttr "polyMapSewMove60.out" "polyTweakUV72.ip";
connectAttr "polyTweakUV72.out" "polyMapSewMove61.ip";
connectAttr "polyMapSewMove61.out" "polyTweakUV73.ip";
connectAttr "polyTweakUV73.out" "polyMapSewMove62.ip";
connectAttr "polyMapSewMove62.out" "polyMapSewMove63.ip";
connectAttr "polyMapSewMove63.out" "polyMapCut40.ip";
connectAttr "polyMapCut40.out" "polyTweakUV74.ip";
connectAttr "polyTweakUV74.out" "polyMapSewMove64.ip";
connectAttr "polyMapSewMove64.out" "polyTweakUV75.ip";
connectAttr "polyTweakUV75.out" "polyMapSewMove65.ip";
connectAttr "polyMapSewMove65.out" "polyTweakUV76.ip";
connectAttr "polyTweakUV76.out" "polyMapSewMove66.ip";
connectAttr "polyMapSewMove66.out" "polyTweakUV77.ip";
connectAttr "polyTweakUV77.out" "polyMapSewMove67.ip";
connectAttr "polyMapSewMove67.out" "polyMapSewMove68.ip";
connectAttr "polyMapSewMove68.out" "polyTweakUV78.ip";
connectAttr "polyTweakUV78.out" "polyMapSewMove69.ip";
connectAttr "polyMapSewMove69.out" "polyMapSewMove70.ip";
connectAttr "polyMapSewMove70.out" "polyTweakUV79.ip";
connectAttr "polyTweakUV79.out" "polyMapSewMove71.ip";
connectAttr "polyMapSewMove71.out" "polyTweakUV80.ip";
connectAttr "polyTweakUV80.out" "polyDelEdge15.ip";
connectAttr "polyTweak72.out" "polyExtrudeEdge1.ip";
connectAttr "mattressShape.wm" "polyExtrudeEdge1.mp";
connectAttr "polyDelEdge15.out" "polyTweak72.ip";
connectAttr "polyExtrudeEdge1.out" "polyTweakUV81.ip";
connectAttr "polyTweak73.out" "polyMergeVert44.ip";
connectAttr "mattressShape.wm" "polyMergeVert44.mp";
connectAttr "polyTweakUV81.out" "polyTweak73.ip";
connectAttr "polyMergeVert44.out" "polyMergeVert45.ip";
connectAttr "mattressShape.wm" "polyMergeVert45.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "baseboardShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "sideboard_LShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "baseboard_frontShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "backboardShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "mattressShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "front_leg_LShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "back_leg_LShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "back_leg_RShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "sideboard_RShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "front_leg_RShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pillowShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "round_pillow_largeShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pillowShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pillowShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "round_pillow_smallShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCube3Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
// End of daybed_005.ma
