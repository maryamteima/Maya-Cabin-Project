//Maya ASCII 2024 scene
//Name: cultery base.ma
//Last modified: Tue, Oct 17, 2023 09:32:04 AM
//Codeset: 1252
requires maya "2024";
requires "stereoCamera" "10.0";
requires "mtoa" "5.3.1.1";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2024";
fileInfo "version" "2024";
fileInfo "cutIdentifier" "202304191415-7fa20164c6";
fileInfo "osv" "Windows 11 Enterprise v2009 (Build: 22000)";
fileInfo "UUID" "336DD2B7-4C66-396F-F51E-79A44D30B468";
fileInfo "license" "education";
createNode transform -n "cutlery";
	rename -uid "84013F20-4B23-B97B-8C2B-EC8C881FD814";
	setAttr ".s" -type "double3" 1 1 6.8448421293756363 ;
createNode transform -n "pCube2" -p "cutlery";
	rename -uid "9EB1B951-44A6-4DF6-C8CF-4A84E6E3EF02";
	setAttr ".t" -type "double3" 0 0 -1.0546788290054838 ;
	setAttr ".s" -type "double3" 1 1 0.14609540747599634 ;
createNode mesh -n "pCubeShape1" -p "pCube2";
	rename -uid "94D86107-4E18-8D26-234A-DD855B44D2AE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[12:15]" -type "float3"  0 0 -1.5025433 0 0 -1.5025433 
		0 0 -1.5025433 0 0 -1.5025433;
createNode transform -n "polySurface1" -p "cutlery";
	rename -uid "FD1104CD-40A0-6F0E-0B96-A08C26798F4B";
createNode mesh -n "polySurfaceShape4" -p "polySurface1";
	rename -uid "2EA09533-4143-C31E-AA47-1EAB26B3231D";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000002980232239 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape7" -p "polySurface1";
	rename -uid "743C60E2-4EF7-07C5-89AF-D1AB7D3FBBFC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 20 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 4 "f[4]" "f[5]" "f[10]" "f[13]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 8 "f[0]" "f[3]" "f[8]" "f[11]" "f[16]" "f[17]" "f[18]" "f[19]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 2 "f[6]" "f[14]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[2]" "f[15]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 4 "f[1]" "f[7]" "f[9]" "f[12]";
	setAttr ".pv" -type "double2" 0.50000002980232239 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.375 0 0.4375 0
		 0.4375 0.125 0.375 0.125 0.375 0.25 0.4375 0.25 0.4375 0.45871216 0.375 0.45793957
		 0.625 0 0.83293957 0 0.83293957 0.125 0.625 0.125 0.5625 0.125 0.5 0.125 0.5 0 0.5625
		 0 0.5 1 0.5 0.7905153 0.5625 0.7912879 0.5625 1 0.375 0.79206043 0.4375 0.7912879
		 0.4375 1 0.375 1 0.16706043 0.125 0.16706043 0 0.5625 0.25 0.5625 0.45871216 0.5
		 0.45948476 0.5 0.25 0.625 0.45793957 0.625 0.25 0.62500006 0.79206043 0.625 1 0.16706045
		 0.25 0.83293957 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 27 ".pt[0:26]" -type "float3"  -0.064954311 0 0 0.064954311 
		0 0 -0.064954311 0 0 0.064954311 0 0 0 0 0 0 0 0 0.038013984 -0.008096315 -6.3195067e-09 
		-0.038013984 -0.008096315 -6.3195067e-09 0.038013984 0.0079592718 6.3195067e-09 -0.038013984 
		0.0079592718 6.3195067e-09 0 -0.014811424 -2.1065025e-09 0 0.014811424 6.3195067e-09 
		-0.038125359 0 0.0069304733 -0.038125359 0 0.0069304733 0.022312552 -0.014811424 
		-6.3195067e-09 0.022312554 0.014811424 6.3195067e-09 0.038125359 0 0.0069304733 0.038125359 
		0 0.0069304733 -0.022312552 -0.014811424 -6.3195067e-09 -0.022312554 0.014811424 
		6.3195067e-09 -0.076250717 0 0 0.044625107 -6.8521447e-05 -2.1065025e-09 -0.044625107 
		-6.8521447e-05 -2.1065025e-09 0.076250717 0 0 0.038125359 0 0.0069304733 0 0 0 -0.038125359 
		0 0.0069304733;
	setAttr -s 27 ".vt[0:26]"  -0.28710565 -0.060630977 0.5 0.28710565 -0.060630977 0.5
		 -0.28710565 0.060630977 0.5 0.28710565 0.060630977 0.5 0 -0.11238277 0.53813833 0 0.11134773 0.53813833
		 -0.28710565 0.060630977 -0.3317582 0.28710565 0.060630977 -0.3317582 -0.28710568 -0.060630977 -0.33175829
		 0.28710568 -0.060630977 -0.33175829 0 0.11134773 -0.33175823 0 -0.11238277 -0.33175829
		 -0.16851853 -0.11238277 0.51906919 -0.16851853 0.11134773 0.51906919 -0.16851853 0.11134773 -0.3317582
		 -0.16851854 -0.11238277 -0.33175829 0.16851853 -0.11238277 0.51906919 0.16851853 0.11134773 0.51906919
		 0.16851853 0.11134773 -0.3317582 0.16851854 -0.11238277 -0.33175829 -0.33703706 1.2878262e-17 0.5
		 -0.33703709 -3.2278705e-17 -0.33175823 0.33703709 -1.8400917e-17 -0.33175823 0.33703706 1.7412863e-17 0.5
		 0.16851853 3.1382405e-17 0.51906919 0 6.3125139e-18 0.53813833 -0.16851853 3.2353737e-17 0.51906919;
	setAttr -s 46 ".ed[0:45]"  0 12 0 2 13 0 0 20 0 1 23 0 2 6 0 3 7 0 4 16 0
		 5 17 0 4 25 1 5 10 1 8 0 0 9 1 0 11 4 1 10 14 0 11 15 0 9 22 0 8 21 0 10 18 0 11 19 0
		 12 4 0 13 5 0 14 6 0 15 8 0 12 26 1 13 14 1 15 12 1 16 1 0 17 3 0 18 7 0 19 9 0 16 24 1
		 17 18 1 19 16 1 20 2 0 21 6 0 22 7 0 23 3 0 24 17 1 25 5 1 26 13 1 20 21 1 22 23 1
		 23 24 1 24 25 1 25 26 1 26 20 1;
	setAttr -s 20 -ch 80 ".fc[0:19]" -type "polyFaces" 
		f 4 0 23 45 -3
		mu 0 4 0 1 2 3
		f 4 1 24 21 -5
		mu 0 4 4 5 6 7
		f 4 -12 15 41 -4
		mu 0 4 8 9 10 11
		f 4 43 -9 6 30
		mu 0 4 12 13 14 15
		f 4 -13 18 32 -7
		mu 0 4 16 17 18 19
		f 4 -23 25 -1 -11
		mu 0 4 20 21 22 23
		f 4 40 -17 10 2
		mu 0 4 3 24 25 0
		f 4 31 -18 -10 7
		mu 0 4 26 27 28 29
		f 4 44 -24 19 8
		mu 0 4 13 2 1 14
		f 4 -25 20 9 13
		mu 0 4 6 5 29 28
		f 4 -26 -15 12 -20
		mu 0 4 22 21 17 16
		f 4 42 -31 26 3
		mu 0 4 11 12 15 8
		f 4 -29 -32 27 5
		mu 0 4 30 27 26 31
		f 4 -33 29 11 -27
		mu 0 4 19 18 32 33
		f 4 -35 -41 33 4
		mu 0 4 34 24 3 4
		f 4 -42 35 -6 -37
		mu 0 4 11 10 35 31
		f 4 -38 -43 36 -28
		mu 0 4 26 12 11 31
		f 4 -39 -44 37 -8
		mu 0 4 29 13 12 26
		f 4 -40 -45 38 -21
		mu 0 4 5 2 13 29
		f 4 -46 39 -2 -34
		mu 0 4 3 2 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface2" -p "cutlery";
	rename -uid "25EE304C-4F52-896E-F424-D787815403E8";
	setAttr ".t" -type "double3" 0 0 -0.050288719306455065 ;
	setAttr ".rp" -type "double3" -5.9604644775390625e-08 -0.0005175173282623291 -0.46332553029060364 ;
	setAttr ".sp" -type "double3" -5.9604644775390625e-08 -0.0005175173282623291 -0.46332553029060364 ;
createNode mesh -n "polySurfaceShape5" -p "polySurface2";
	rename -uid "B7021122-477F-7AB8-34FD-81B37E3BC608";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape6" -p "polySurface2";
	rename -uid "7F3FA6E2-49CB-9D9F-D68D-47B505CE9592";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:113]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[0:77]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 9 "f[78:79]" "f[88:90]" "f[95]" "f[97]" "f[99]" "f[102]" "f[107]" "f[109]" "f[111]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 5 "f[80:81]" "f[91]" "f[100]" "f[103]" "f[112]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 5 "f[86:87]" "f[94]" "f[101]" "f[106]" "f[113]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 7 "f[82:85]" "f[92:93]" "f[96]" "f[98]" "f[104:105]" "f[108]" "f[110]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 143 ".uvst[0].uvsp[0:142]" -type "float2" 0.375 0.625 0.4375
		 0.625 0.4375 0.75 0.375 0.75 0.5 0.75 0.5 0.625 0.5625 0.625 0.5625 0.75 0.375 0.5
		 0.4375 0.5 0.4375 0.5 0.375 0.5 0.4375 0.75 0.4375 0.75 0.375 0.75 0.375 0.75 0.375
		 0.625 0.375 0.625 0.5 0.5 0.5625 0.5 0.5625 0.5 0.5 0.5 0.625 0.625 0.625 0.625 0.625
		 0.75 0.625 0.75 0.5625 0.75 0.5625 0.75 0.5 0.75 0.5 0.75 0.625 0.5 0.625 0.5 0.625
		 0.625 0.625 0.75 0.375 0.5 0.4375 0.5 0.4375 0.625 0.375 0.625 0.5 0.625 0.5 0.5
		 0.5625 0.625 0.5625 0.5 0.625 0.625 0.625 0.5 0.375 0.5 0.4375 0.5 0.5 0.5 0.5625
		 0.5 0.625 0.5 0.4375 0.75 0.375 0.75 0.375 0.75 0.4375 0.75 0.375 0.625 0.375 0.625
		 0.5625 0.75 0.5 0.75 0.5 0.75 0.5625 0.75 0.625 0.625 0.625 0.75 0.625 0.75 0.625
		 0.625 0.375 0.5 0.4375 0.5 0.4375 0.5 0.375 0.5 0.5 0.5 0.5625 0.5 0.5625 0.5 0.5
		 0.5 0.625 0.5 0.625 0.5 0.4375 0.75 0.375 0.75 0.375 0.625 0.5625 0.75 0.5 0.75 0.625
		 0.625 0.625 0.75 0.375 0.5 0.4375 0.5 0.5 0.5 0.5625 0.5 0.625 0.5 0.375 0.77103019
		 0.4375 0.77064395 0.14603022 2.5437812e-09 0.14603022 0.125 0.14603022 0.24999993
		 0.375 0.47896978 0.4375 0.47935608 0.5 0.47974238 0.5625 0.47935608 0.625 0.47896978
		 0.85396981 0.25 0.85396981 0.125 0.85396981 1.8626451e-09 0.625 0.77103019 0.5625
		 0.77064395 0.5 0.77025765 0.43749997 0.77064395 0.4375 0.77036506 0.5 0.76997721
		 0.5 0.77025765 0.37577045 0.77101082 0.375 0.77075243 0.14575244 -1.7462298e-09 0.14603022
		 0 0.14603022 0.125 0.14580317 0.125 0.14603022 0.25 0.14575222 0.25 0.5 0.47974238
		 0.5 0.48002294 0.4375 0.47963485 0.43749997 0.47935608 0.5625 0.47935608 0.5625 0.47963485
		 0.62421441 0.47898951 0.625 0.47924778 0.375 0.47924778 0.37578556 0.47898948 0.85396981
		 0.125 0.85419685 0.125 0.85424781 0.25 0.85396981 0.25 0.85396981 1.8626451e-09 0.85424757
		 6.2717048e-10 0.625 0.77075243 0.62422955 0.77101082 0.5625 0.77064395 0.5625 0.77036506
		 0.125 2.2351742e-08 0.125 0.125 0.875 0.125 0.875 0 0.125 0.25 0.875 0.25 0.37499997
		 0.77103019 0.375 0.47896978 0.625 0.47896978 0.625 0.77103019;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 121 ".vt[0:120]"  -0.30827028 -9.310944e-05 -0.52694309 -0.14333388 -0.11685899 -0.52694309
		 -0.25069454 -0.070006482 -0.52694309 -0.3056936 -0.085251451 -0.5 -0.17477946 -0.14238277 -0.5
		 -0.37590069 5.5511151e-17 -0.5 0 0.14134774 -0.5 0.17466442 0.14134774 -0.5 -9.7376066e-18 -0.11685899 -0.52694309
		 0.14333388 -0.11685899 -0.52694309 -0.17466442 0.14134774 -0.5 -0.30561936 0.085341558 -0.5
		 0.37590069 -1.4438791e-08 -0.5 0.3056936 -0.085251451 -0.5 0.17477946 -0.14238277 -0.5
		 0 -0.14238277 -0.5 -0.14323956 0.11582395 -0.52694309 -0.25063369 0.06989418 -0.52694309
		 0.14323956 0.11582395 -0.52694309 -9.7376066e-18 0.11582395 -0.52694309 0.30827028 -9.310944e-05 -0.52694309
		 0.25069454 -0.070006482 -0.52694309 0.30561936 0.085341558 -0.5 0.25063369 0.06989418 -0.52694309
		 -0.30827075 -9.310944e-05 -0.54938459 -0.1381996 -9.310944e-05 -0.54938459 -0.14333406 -0.11685899 -0.54938459
		 -0.2506949 -0.070006482 -0.54938459 -9.7376066e-18 -9.310944e-05 -0.54938459 -9.7376066e-18 -0.11685899 -0.54938459
		 0.13819951 -9.310944e-05 -0.54938459 0.14333397 -0.11685899 -0.54938459 0.30827036 -9.310944e-05 -0.54938459
		 0.25069472 -0.070006482 -0.54938459 -0.23545103 0.049629413 -0.54938459 -0.13819942 0.091221437 -0.54938459
		 -0.13819942 -9.310944e-05 -0.54938459 -0.27639925 -9.310944e-05 -0.54938459 -9.7376066e-18 -9.310944e-05 -0.54938459
		 -9.7376066e-18 0.091221437 -0.54938459 0.13819942 -9.310944e-05 -0.54938459 0.13819954 0.091221437 -0.54938459
		 0.27639884 -9.310944e-05 -0.54938459 0.23545097 0.049629413 -0.54938459 -0.25063398 0.06989418 -0.54938459
		 -0.14323956 0.11582395 -0.54938459 -9.7376066e-18 0.11582395 -0.54938459 0.14323977 0.11582395 -0.54938459
		 0.2506339 0.06989418 -0.54938459 -0.28002408 -0.10885461 -0.52694309 -0.15334414 -0.16482437 -0.52694309
		 -0.28002444 -0.10885461 -0.54938459 -0.15334432 -0.16482437 -0.54938459 -0.34690881 -9.3109906e-05 -0.52694309
		 -0.34690928 -9.3109906e-05 -0.54938459 -9.926121e-17 -0.16691348 -0.52694309 0.15334414 -0.16482435 -0.52694309
		 -9.9527675e-17 -0.16691348 -0.54938459 0.15334423 -0.16482435 -0.54938459 0.34690887 -9.3113631e-05 -0.52694309
		 0.28002411 -0.1088546 -0.52694309 0.28002429 -0.1088546 -0.54938459 0.34690896 -9.3113631e-05 -0.54938459
		 -0.27979422 0.10881532 -0.52694309 -0.15308084 0.16386233 -0.52694309 -0.15308084 0.16386233 -0.54938459
		 -0.27979451 0.10881532 -0.54938459 -9.2322316e-17 0.16587844 -0.52694309 0.15308085 0.16386233 -0.52694309
		 0.15308106 0.16386233 -0.54938459 -9.2588781e-17 0.16587844 -0.54938459 0.27979428 0.1088153 -0.52694309
		 0.27979448 0.1088153 -0.54938459 -0.31376296 -0.12190761 -0.53816384 -0.17181991 -0.18462087 -0.53816384
		 7.1814665e-09 -0.1869617 -0.53816384 0.17181987 -0.18462086 -0.53816384 0.31376293 -0.12190759 -0.53816384
		 0.38870624 -4.197935e-05 -0.53816384 0.31350544 0.12198827 -0.53816384 0.17152493 0.18366763 -0.53816384
		 7.1814665e-09 0.18592666 -0.53816384 -0.17152478 0.18366763 -0.53816384 -0.31350538 0.12198829 -0.53816384
		 -0.38870636 -4.1975185e-05 -0.53816384 -0.29174942 -0.066781782 -0.41587913 -0.34436581 1.4492139e-09 -0.40988648
		 -0.29173362 0.066808008 -0.4158791 -0.17014903 0.11930671 -0.41076374 0 0.11839849 -0.37726647
		 0.17014903 0.11930671 -0.41076374 0.29173362 0.066808008 -0.4158791 0.34436581 1.4492141e-09 -0.40988648
		 0.29174942 -0.066781782 -0.41587913 0.17018025 -0.12034499 -0.41076377 0 -0.11943352 -0.37726647
		 -0.17018025 -0.12034499 -0.41076377 -0.17312075 -0.13443479 -0.41076377 -0.17477947 -0.14238277 -0.41196933
		 -0.30569363 -0.085251421 -0.41699025 -0.30114084 -0.079221092 -0.41587913 -0.37590069 7.6020834e-09 -0.41085941
		 -0.3681258 6.1476122e-09 -0.40988648 0 0.13429698 -0.37726647 0 0.14134774 -0.37896633
		 0.17466442 0.14134774 -0.41196874 0.17303674 0.13340253 -0.41076374 -0.30108273 0.079286456 -0.4158791
		 -0.30561936 0.085341558 -0.41699106 -0.17466442 0.14134774 -0.41196874 -0.17303674 0.13340253 -0.41076374
		 0.3681258 6.1476122e-09 -0.40988648 0.37590069 7.4461957e-09 -0.41085941 0.30569363 -0.085251421 -0.41699025
		 0.30114084 -0.079221092 -0.41587913 0.17312075 -0.13443479 -0.41076377 0.17477947 -0.14238277 -0.41196933
		 0 -0.14238277 -0.37896559 0 -0.13533202 -0.37726647 0.30561936 0.085341558 -0.41699106
		 0.30108273 0.079286456 -0.4158791;
	setAttr -s 234 ".ed";
	setAttr ".ed[0:165]"  2 1 0 0 2 0 3 4 1 5 3 1 6 7 1 8 9 0 11 10 1 12 13 1
		 15 14 1 10 16 1 17 16 0 11 17 0 1 4 1 3 2 0 5 0 1 7 18 1 19 18 0 6 19 1 20 12 1 13 21 0
		 20 21 0 9 14 1 15 8 1 10 6 1 16 19 0 1 8 0 4 15 1 7 22 1 22 23 0 18 23 0 9 21 0 14 13 1
		 11 5 1 17 0 0 22 12 1 23 20 0 24 25 0 25 26 1 27 26 1 24 27 1 28 29 1 28 30 0 30 31 1
		 29 31 1 25 28 0 26 29 1 30 32 0 32 33 1 31 33 1 34 35 1 35 36 1 37 36 0 34 37 1 36 38 0
		 35 39 1 39 38 1 38 40 0 39 41 1 41 40 1 40 42 0 41 43 1 43 42 1 36 25 1 37 24 0 40 30 1
		 38 28 1 34 44 1 44 45 1 35 45 1 39 46 1 46 47 1 41 47 1 45 46 1 47 48 1 43 48 1 42 32 0
		 44 24 1 48 32 1 2 49 1 1 50 1 49 50 0 27 51 1 49 73 1 26 52 1 51 52 0 50 74 1 0 53 1
		 53 49 0 24 54 1 53 84 0 54 51 0 8 55 1 9 56 1 55 56 0 29 57 1 55 75 1 31 58 1 57 58 0
		 56 76 1 50 55 0 52 57 0 20 59 1 21 60 1 59 60 0 33 61 1 60 77 1 32 62 1 62 61 0 59 78 0
		 56 60 0 58 61 0 17 63 1 16 64 1 63 64 0 45 65 1 64 82 1 44 66 1 66 65 0 63 83 1 19 67 1
		 18 68 1 67 68 0 47 69 1 68 80 1 46 70 1 70 69 0 67 81 1 64 67 0 65 70 0 23 71 1 68 71 0
		 48 72 1 71 79 1 69 72 0 63 53 0 66 54 0 71 59 0 72 62 0 73 51 1 74 52 1 75 57 1 76 58 1
		 77 61 1 78 62 0 79 72 1 80 69 1 81 70 1 82 65 1 83 66 1 84 54 0 73 74 1 74 75 1 75 76 1
		 76 77 1 77 78 1 78 79 1 79 80 1 80 81 1 81 82 1 82 83 1 83 84 1 84 73 1 85 96 0 86 85 0
		 87 86 0 88 87 0;
	setAttr ".ed[166:233]" 89 88 0 90 89 0 91 90 0 92 91 0 93 92 0 94 93 0 95 94 0
		 96 95 0 97 98 1 98 117 0 117 118 0 118 97 0 97 100 0 100 99 1 99 98 0 100 102 0 102 101 0
		 101 99 0 102 107 0 107 108 1 108 101 0 103 104 0 104 109 0 109 110 1 110 103 0 103 106 0
		 106 105 1 105 104 0 106 120 0 120 119 1 119 105 0 107 110 0 109 108 0 111 112 0 112 119 0
		 120 111 0 111 114 0 114 113 1 113 112 0 114 115 0 115 116 1 116 113 0 115 118 0 117 116 0
		 4 98 1 99 3 1 101 5 0 6 104 1 105 7 1 109 10 1 11 108 1 12 112 0 113 13 1 14 116 1
		 117 15 1 119 22 1 85 100 0 97 96 0 86 102 0 90 106 0 103 89 0 88 110 0 107 87 0 93 114 0
		 111 92 0 95 118 0 115 94 0 91 120 0;
	setAttr -s 114 -ch 456 ".fc[0:113]" -type "polyFaces" 
		f 4 36 37 -39 -40
		mu 0 4 0 1 2 3
		f 4 -41 41 42 -44
		mu 0 4 4 5 6 7
		f 4 6 9 -11 -12
		mu 0 4 8 9 10 11
		f 4 12 -3 13 0
		mu 0 4 12 13 14 15
		f 4 -4 14 1 -14
		mu 0 4 14 16 17 15
		f 4 4 15 -17 -18
		mu 0 4 18 19 20 21
		f 4 18 7 19 -21
		mu 0 4 22 23 24 25
		f 4 21 -9 22 5
		mu 0 4 26 27 28 29
		f 4 -10 23 17 -25
		mu 0 4 10 9 18 21
		f 4 -38 44 40 -46
		mu 0 4 2 1 5 4
		f 4 -27 -13 25 -23
		mu 0 4 28 13 12 29
		f 4 -16 27 28 -30
		mu 0 4 20 19 30 31
		f 4 -43 46 47 -49
		mu 0 4 7 6 32 33
		f 4 -32 -22 30 -20
		mu 0 4 24 27 26 25
		f 4 -15 -33 11 33
		mu 0 4 17 16 8 11
		f 4 49 50 -52 -53
		mu 0 4 34 35 36 37
		f 4 -54 -51 54 55
		mu 0 4 38 36 35 39
		f 4 -57 -56 57 58
		mu 0 4 40 38 39 41
		f 4 -60 -59 60 61
		mu 0 4 42 40 41 43
		f 4 34 -19 -36 -29
		mu 0 4 30 23 22 31
		f 4 51 62 -37 -64
		mu 0 4 37 36 1 0
		f 4 56 64 -42 -66
		mu 0 4 38 40 6 5
		f 4 -50 66 67 -69
		mu 0 4 35 34 44 45
		f 4 -58 69 70 -72
		mu 0 4 41 39 46 47
		f 4 -55 68 72 -70
		mu 0 4 39 35 45 46
		f 4 53 65 -45 -63
		mu 0 4 36 38 5 1
		f 4 -61 71 73 -75
		mu 0 4 43 41 47 48
		f 4 59 75 -47 -65
		mu 0 4 40 42 32 6
		f 4 52 63 -77 -67
		mu 0 4 34 37 0 44
		f 4 -62 74 77 -76
		mu 0 4 42 43 48 32
		f 4 -81 82 150 -86
		mu 0 4 49 50 51 52
		f 4 -88 89 161 -83
		mu 0 4 50 53 54 51
		f 4 -94 95 152 -99
		mu 0 4 55 56 57 58
		f 4 -100 85 151 -96
		mu 0 4 56 49 52 57
		f 4 103 105 154 -109
		mu 0 4 59 60 61 62
		f 4 -110 98 153 -106
		mu 0 4 60 55 58 61
		f 4 113 115 159 -119
		mu 0 4 63 64 65 66
		f 4 121 123 157 -127
		mu 0 4 67 68 69 70
		f 4 127 126 158 -116
		mu 0 4 64 67 70 65
		f 4 130 132 156 -124
		mu 0 4 68 71 72 69
		f 4 -135 118 160 -90
		mu 0 4 53 63 66 54
		f 4 136 108 155 -133
		mu 0 4 71 59 62 72
		f 4 -1 78 80 -80
		mu 0 4 12 15 50 49
		f 4 38 83 -85 -82
		mu 0 4 3 2 73 74
		f 4 -2 86 87 -79
		mu 0 4 15 17 53 50
		f 4 39 81 -91 -89
		mu 0 4 0 3 74 75
		f 4 -6 91 93 -93
		mu 0 4 26 29 56 55
		f 4 43 96 -98 -95
		mu 0 4 4 7 76 77
		f 4 -26 79 99 -92
		mu 0 4 29 12 49 56
		f 4 45 94 -101 -84
		mu 0 4 2 4 77 73
		f 4 20 102 -104 -102
		mu 0 4 22 25 60 59
		f 4 -48 106 107 -105
		mu 0 4 33 32 78 79
		f 4 -31 92 109 -103
		mu 0 4 25 26 55 60
		f 4 48 104 -111 -97
		mu 0 4 7 33 79 76
		f 4 10 112 -114 -112
		mu 0 4 11 10 64 63
		f 4 -68 116 117 -115
		mu 0 4 45 44 80 81
		f 4 16 120 -122 -120
		mu 0 4 21 20 68 67
		f 4 -71 124 125 -123
		mu 0 4 47 46 82 83
		f 4 24 119 -128 -113
		mu 0 4 10 21 67 64
		f 4 -73 114 128 -125
		mu 0 4 46 45 81 82
		f 4 29 129 -131 -121
		mu 0 4 20 31 71 68
		f 4 -74 122 133 -132
		mu 0 4 48 47 83 84
		f 4 -34 111 134 -87
		mu 0 4 17 11 63 53
		f 4 76 88 -136 -117
		mu 0 4 44 0 75 80
		f 4 35 101 -137 -130
		mu 0 4 31 22 59 71
		f 4 -78 131 137 -107
		mu 0 4 32 48 84 78
		f 4 -151 138 84 -140
		mu 0 4 52 51 74 73
		f 4 -152 139 100 -141
		mu 0 4 57 52 73 77
		f 4 -153 140 97 -142
		mu 0 4 58 57 77 76
		f 4 -154 141 110 -143
		mu 0 4 61 58 76 79
		f 4 -155 142 -108 -144
		mu 0 4 62 61 79 78
		f 4 -156 143 -138 -145
		mu 0 4 72 62 78 84
		f 4 -157 144 -134 -146
		mu 0 4 69 72 84 83
		f 4 -158 145 -126 -147
		mu 0 4 70 69 83 82
		f 4 -159 146 -129 -148
		mu 0 4 65 70 82 81
		f 4 -160 147 -118 -149
		mu 0 4 66 65 81 80
		f 4 -161 148 135 -150
		mu 0 4 54 66 80 75
		f 4 -162 149 90 -139
		mu 0 4 51 54 75 74
		f 4 174 175 176 177
		mu 0 4 101 102 103 104
		f 4 -175 178 179 180
		mu 0 4 102 101 105 106
		f 4 -180 181 182 183
		mu 0 4 107 108 109 110
		f 4 -183 184 185 186
		mu 0 4 110 109 111 112
		f 4 187 188 189 190
		mu 0 4 113 114 115 116
		f 4 -188 191 192 193
		mu 0 4 114 113 117 118
		f 4 -193 194 195 196
		mu 0 4 118 117 119 120
		f 4 -186 197 -190 198
		mu 0 4 121 122 116 115
		f 4 199 200 -196 201
		mu 0 4 123 124 125 126
		f 4 -200 202 203 204
		mu 0 4 124 123 127 128
		f 4 -204 205 206 207
		mu 0 4 129 130 131 132
		f 4 -207 208 -177 209
		mu 0 4 132 131 104 103
		f 4 2 210 -181 211
		mu 0 4 14 13 102 106
		f 4 -212 -184 212 3
		mu 0 4 133 107 110 134
		f 4 213 -194 214 -5
		mu 0 4 18 114 118 19
		f 4 -199 215 -7 216
		mu 0 4 121 115 9 8
		f 4 217 -205 218 -8
		mu 0 4 135 124 128 136
		f 4 219 -210 220 8
		mu 0 4 27 132 103 28
		f 4 -216 -189 -214 -24
		mu 0 4 9 115 114 18
		f 4 -211 26 -221 -176
		mu 0 4 102 13 28 103
		f 4 -215 -197 221 -28
		mu 0 4 19 118 120 30
		f 4 -208 -220 31 -219
		mu 0 4 129 132 27 24
		f 4 -213 -187 -217 32
		mu 0 4 134 110 112 137
		f 4 -201 -218 -35 -222
		mu 0 4 125 124 135 138
		f 4 -163 222 -179 223
		mu 0 4 86 85 139 101
		f 4 -164 224 -182 -223
		mu 0 4 87 88 109 108
		f 4 -168 225 -192 226
		mu 0 4 92 93 117 113
		f 4 -166 227 -198 228
		mu 0 4 90 91 116 140
		f 4 -171 229 -203 230
		mu 0 4 96 97 127 123
		f 4 -173 231 -209 232
		mu 0 4 99 100 104 131
		f 4 -167 -227 -191 -228
		mu 0 4 91 92 113 116
		f 4 -174 -224 -178 -232
		mu 0 4 100 86 101 104
		f 4 -169 233 -195 -226
		mu 0 4 93 94 141 117
		f 4 -172 -233 -206 -230
		mu 0 4 98 99 131 142
		f 4 -165 -229 -185 -225
		mu 0 4 88 89 111 109
		f 4 -170 -231 -202 -234
		mu 0 4 95 96 123 126;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode deleteComponent -n "deleteComponent35";
	rename -uid "1FAE449A-470B-8C83-42CD-A88F5D5AEFBC";
	setAttr ".dc" -type "componentList" 1 "f[0]";
createNode polyTweak -n "polyTweak38";
	rename -uid "1CB05E2E-4333-A0CC-93EC-828E19240C36";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[0:15]" -type "float3"  0 0.15151234 1.067580342 0
		 0.15151234 1.067580342 0 -0.15151234 1.067580342 0 -0.15151234 1.067580342 0 -0.15151234
		 0 0 -0.15151234 0 0 0.15151234 0 0 0.15151234 0 0 0.033332709 1.067580342 0 0.033332709
		 1.067580342 0 -0.033332709 1.067580342 0 -0.033332709 1.067580342 0 0.033332709 0
		 0 0.033332709 0 0 -0.033332709 0 0 -0.033332709 0;
createNode polyExtrudeFace -n "polyExtrudeFace34";
	rename -uid "1FD845B1-4106-44EA-0B5B-E796A5CC5539";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -4.6335652358266577 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0 -4.1335654 ;
	setAttr ".rs" 33075;
	setAttr ".lt" -type "double3" 0 0 3.1565253807662574 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.11000001430511475 -0.11000001430511475 -4.1335652358266577 ;
	setAttr ".cbx" -type "double3" 0.11000001430511475 0.11000001430511475 -4.1335652358266577 ;
createNode polyExtrudeFace -n "polyExtrudeFace33";
	rename -uid "3CA54C06-4475-96FE-71DE-0BBC7FE3110D";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -4.6335652358266577 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0 -4.1335654 ;
	setAttr ".rs" 46481;
	setAttr ".off" 0.38999998569488525;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.5 -0.5 -4.1335652358266577 ;
	setAttr ".cbx" -type "double3" 0.5 0.5 -4.1335652358266577 ;
createNode polyCube -n "polyCube2";
	rename -uid "615C21C8-4690-87DC-E195-8EA9A32ABDF8";
	setAttr ".cuv" 4;
createNode deleteComponent -n "deleteComponent34";
	rename -uid "B99441F2-463E-ED39-66C8-71A0126EEDAF";
	setAttr ".dc" -type "componentList" 1 "vtx[0:90]";
createNode polyMirror -n "polyMirror2";
	rename -uid "33C051DB-4118-277D-AE22-F8954B970521";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 6.8448421293756363 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".a" 1;
	setAttr ".mtt" 1;
	setAttr ".cm" yes;
	setAttr ".fnf" 40;
	setAttr ".lnf" 79;
createNode deleteComponent -n "deleteComponent33";
	rename -uid "DE82B331-49BD-8FA9-5B57-318D534E4413";
	setAttr ".dc" -type "componentList" 7 "f[1:2]" "f[5]" "f[7]" "f[20:25]" "f[27:32]" "f[41:46]" "f[62:67]";
createNode deleteComponent -n "deleteComponent32";
	rename -uid "C7C1BADC-484F-FB4B-4EC7-7592253C6A1F";
	setAttr ".dc" -type "componentList" 11 "f[0]" "f[2:3]" "f[6]" "f[8]" "f[11]" "f[33]" "f[40]" "f[49]" "f[56]" "f[64]" "f[73]";
createNode polyTweak -n "polyTweak37";
	rename -uid "D9AD3934-46BA-7DBE-CB6C-ED9DF42EC1E5";
	setAttr ".uopa" yes;
	setAttr -s 34 ".tk";
	setAttr ".tk[2]" -type "float3" 0 -1.8626451e-09 0 ;
	setAttr ".tk[3]" -type "float3" 0 -1.8626451e-09 0 ;
	setAttr ".tk[5]" -type "float3" 0 -0.0097456165 0 ;
	setAttr ".tk[6]" -type "float3" 0 -3.7252903e-09 0 ;
	setAttr ".tk[11]" -type "float3" 0 -0.0097456165 0 ;
	setAttr ".tk[13]" -type "float3" 0 -0.0097456165 0 ;
	setAttr ".tk[21]" -type "float3" 0 -2.3283064e-10 0 ;
	setAttr ".tk[22]" -type "float3" 0 -0.020843782 0 ;
	setAttr ".tk[23]" -type "float3" 0 0.0047141551 0 ;
	setAttr ".tk[24]" -type "float3" 0 0.020843796 0 ;
	setAttr ".tk[25]" -type "float3" 0 -0.020843789 0 ;
	setAttr ".tk[26]" -type "float3" 0 0.0047141551 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.0048655039 0 ;
	setAttr ".tk[28]" -type "float3" 0 -0.0095094675 0 ;
	setAttr ".tk[29]" -type "float3" 0 0.020843789 0 ;
	setAttr ".tk[30]" -type "float3" 0 0.012813969 0 ;
	setAttr ".tk[52]" -type "float3" 0 0.016828878 0 ;
	setAttr ".tk[53]" -type "float3" 0 -0.0080648148 0 ;
	setAttr ".tk[54]" -type "float3" 0 -0.0023219802 0 ;
	setAttr ".tk[55]" -type "float3" 0 -0.0080648148 0 ;
	setAttr ".tk[56]" -type "float3" 0 0.016828885 0 ;
	setAttr ".tk[68]" -type "float3" 0 0.014821426 0 ;
	setAttr ".tk[69]" -type "float3" 0 -0.0144543 0 ;
	setAttr ".tk[70]" -type "float3" 0 -0.0059157214 0 ;
	setAttr ".tk[71]" -type "float3" 0 -0.014454292 0 ;
	setAttr ".tk[72]" -type "float3" 0 0.014821426 0 ;
	setAttr ".tk[76]" -type "float3" 0 0.01883634 0 ;
	setAttr ".tk[77]" -type "float3" 0 -0.001675325 0 ;
	setAttr ".tk[78]" -type "float3" 0 0.0012717544 0 ;
	setAttr ".tk[79]" -type "float3" 0 -0.001675325 0 ;
	setAttr ".tk[80]" -type "float3" 0 0.018836336 0 ;
createNode polySplit -n "polySplit25";
	rename -uid "C17CCB3C-456A-6408-BAB4-A7A71EC66BD2";
	setAttr -s 17 ".e[0:16]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 17 ".d[0:16]"  -2147483639 -2147483575 -2147483576 -2147483561 -2147483562 -2147483563 
		-2147483564 -2147483565 -2147483566 -2147483621 -2147483638 -2147483578 -2147483592 -2147483595 -2147483587 -2147483584 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit24";
	rename -uid "FFF09F91-4708-42BB-3579-C9889E9FC350";
	setAttr -s 17 ".e[0:16]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 17 ".d[0:16]"  -2147483644 -2147483622 -2147483574 -2147483573 -2147483572 -2147483571 
		-2147483570 -2147483569 -2147483568 -2147483567 -2147483643 -2147483597 -2147483608 -2147483605 -2147483613 -2147483610 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit23";
	rename -uid "256AAE70-43C4-79AB-A229-11ADE5149CDD";
	setAttr -s 17 ".e[0:16]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 17 ".d[0:16]"  -2147483644 -2147483622 -2147483639 -2147483584 -2147483587 -2147483595 
		-2147483592 -2147483578 -2147483638 -2147483621 -2147483643 -2147483597 -2147483608 -2147483605 -2147483613 -2147483610 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak36";
	rename -uid "03E1C7E9-4431-4804-AFA7-5D947A853C94";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[6]" -type "float3" 0 0 0.0058453218 ;
	setAttr ".tk[7]" -type "float3" 0 0 0.0058453218 ;
	setAttr ".tk[8]" -type "float3" 0 0 0.0058454089 ;
	setAttr ".tk[9]" -type "float3" 0 0 0.0058454089 ;
	setAttr ".tk[15]" -type "float3" 0 0 0.0058453567 ;
	setAttr ".tk[16]" -type "float3" 0 0 0.0058453567 ;
	setAttr ".tk[23]" -type "float3" 0 0 -0.005842112 ;
	setAttr ".tk[24]" -type "float3" 0 0 -0.0058453847 ;
	setAttr ".tk[26]" -type "float3" 0 0 -0.005842112 ;
	setAttr ".tk[27]" -type "float3" 0 0 -0.005842112 ;
	setAttr ".tk[29]" -type "float3" 0 0 -0.0058453847 ;
	setAttr ".tk[31]" -type "float3" 0 0 -0.0058420785 ;
	setAttr ".tk[33]" -type "float3" 0 0 -0.0058420557 ;
	setAttr ".tk[35]" -type "float3" 0 0 -0.0058420557 ;
	setAttr ".tk[36]" -type "float3" 0 0 -0.0058454089 ;
	setAttr ".tk[39]" -type "float3" 0 0 -0.0058454089 ;
createNode deleteComponent -n "deleteComponent31";
	rename -uid "9FE2016F-426C-4417-18D3-0CA315648DBA";
	setAttr ".dc" -type "componentList" 7 "f[21]" "f[23]" "f[26:27]" "f[30]" "f[32]" "f[35]" "f[37]";
createNode polyExtrudeFace -n "polyExtrudeFace30";
	rename -uid "3E8057AB-4635-CDBE-F0CE-F3B87C3A5E79";
	setAttr ".ics" -type "componentList" 5 "f[1]" "f[4:5]" "f[7]" "f[9:10]" "f[12:13]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 6.8448421293756363 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -0.00051751733 0.70631939 ;
	setAttr ".rs" 53960;
	setAttr ".off" 0.079999998211860657;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.35205996036529541 -0.11238276958465576 -2.2708331237842603 ;
	setAttr ".cbx" -type "double3" 0.35205996036529541 0.1113477349281311 3.683471912497827 ;
createNode groupParts -n "groupParts2";
	rename -uid "4B91DEE5-4A6F-64A8-1B03-C9BA0C6D6CD7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 20 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]";
createNode groupId -n "groupId7";
	rename -uid "296E4842-4F00-FC17-4B6B-5B9FF292D76C";
	setAttr ".ihi" 0;
createNode polyExtrudeFace -n "polyExtrudeFace32";
	rename -uid "1B6F3D51-4931-D864-EE31-5999100B4F03";
	setAttr ".ics" -type "componentList" 4 "f[0:1]" "f[9]" "f[12]" "f[16:19]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 6.8448421293756363 0 0 0 0.75829700484714224 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.3411045e-07 -0.00051751733 -3.0021539 ;
	setAttr ".rs" 35619;
	setAttr ".lt" -type "double3" 1.5567406779349972e-17 3.8261226877671723e-19 -0.11644500346156804 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.2564525306224823 -0.076858997344970703 -3.0021538091449926 ;
	setAttr ".cbx" -type "double3" 0.25645226240158081 0.075823962688446045 -3.0021538091449926 ;
createNode polyExtrudeFace -n "polyExtrudeFace31";
	rename -uid "FF2DD5C3-4314-407F-1CCA-558723820408";
	setAttr ".ics" -type "componentList" 4 "f[0:1]" "f[9]" "f[12]" "f[16:19]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 6.8448421293756363 0 0 0 0.75829700484714224 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.937151e-07 -0.00051751733 -3.0021539 ;
	setAttr ".rs" 60466;
	setAttr ".off" 0.039999999105930328;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.30827075242996216 -0.11685898900032043 -3.0021538091449926 ;
	setAttr ".cbx" -type "double3" 0.30827036499977112 0.11582395434379578 -3.0021538091449926 ;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "1C69DBB5-40F3-849B-38E8-769C6D899B33";
	setAttr ".ics" -type "componentList" 2 "e[49:50]" "e[58:59]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "6C560BD1-4475-2592-6906-37854CEA6146";
	setAttr ".ics" -type "componentList" 5 "e[49]" "e[51]" "e[53:54]" "e[56:57]" "e[59:61]";
	setAttr ".cv" yes;
createNode polyExtrudeEdge -n "polyExtrudeEdge3";
	rename -uid "29425835-48E4-2CF4-5D83-F08AB8814044";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[162:173]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 6.8448421293756363 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -0.00051913783 -2.7144783 ;
	setAttr ".rs" 52542;
	setAttr ".lt" -type "double3" 1.8735013540549517e-16 -3.7404974950749903e-18 -0.07093076821235593 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.34436580538749695 -0.12034498900175095 -2.8466269922399432 ;
	setAttr ".cbx" -type "double3" 0.34436580538749695 0.11930671334266663 -2.5823294047047276 ;
createNode groupParts -n "groupParts1";
	rename -uid "A4853C46-45CB-877A-D704-C9AE7E00A71A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:113]";
createNode groupId -n "groupId6";
	rename -uid "9465670A-471A-4F96-E761-8289D5BB9BFA";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :sequenceManager1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
	setAttr ".rtfm" 1;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :standardSurface1;
	setAttr ".bc" -type "float3" 0.40000001 0.40000001 0.40000001 ;
	setAttr ".sr" 0.5;
select -ne :initialShadingGroup;
	setAttr -s 11 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "standardSurface1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "deleteComponent35.og" "pCubeShape1.i";
connectAttr "deleteComponent34.og" "polySurfaceShape4.i";
connectAttr "groupId7.id" "polySurfaceShape4.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape4.iog.og[0].gco";
connectAttr "polyExtrudeFace32.out" "polySurfaceShape5.i";
connectAttr "groupId6.id" "polySurfaceShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape5.iog.og[0].gco";
connectAttr "polyTweak38.out" "deleteComponent35.ig";
connectAttr "polyExtrudeFace34.out" "polyTweak38.ip";
connectAttr "polyExtrudeFace33.out" "polyExtrudeFace34.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace34.mp";
connectAttr "polyCube2.out" "polyExtrudeFace33.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace33.mp";
connectAttr "polyMirror2.out" "deleteComponent34.ig";
connectAttr "deleteComponent33.og" "polyMirror2.ip";
connectAttr "polySurfaceShape4.wm" "polyMirror2.mp";
connectAttr "deleteComponent32.og" "deleteComponent33.ig";
connectAttr "polyTweak37.out" "deleteComponent32.ig";
connectAttr "polySplit25.out" "polyTweak37.ip";
connectAttr "polySplit24.out" "polySplit25.ip";
connectAttr "polySplit23.out" "polySplit24.ip";
connectAttr "polyTweak36.out" "polySplit23.ip";
connectAttr "deleteComponent31.og" "polyTweak36.ip";
connectAttr "polyExtrudeFace30.out" "deleteComponent31.ig";
connectAttr "groupParts2.og" "polyExtrudeFace30.ip";
connectAttr "polySurfaceShape4.wm" "polyExtrudeFace30.mp";
connectAttr "polySurfaceShape7.o" "groupParts2.ig";
connectAttr "groupId7.id" "groupParts2.gi";
connectAttr "polyExtrudeFace31.out" "polyExtrudeFace32.ip";
connectAttr "polySurfaceShape5.wm" "polyExtrudeFace32.mp";
connectAttr "polyDelEdge2.out" "polyExtrudeFace31.ip";
connectAttr "polySurfaceShape5.wm" "polyExtrudeFace31.mp";
connectAttr "polyDelEdge1.out" "polyDelEdge2.ip";
connectAttr "polyExtrudeEdge3.out" "polyDelEdge1.ip";
connectAttr "groupParts1.og" "polyExtrudeEdge3.ip";
connectAttr "polySurfaceShape5.wm" "polyExtrudeEdge3.mp";
connectAttr "polySurfaceShape6.o" "groupParts1.ig";
connectAttr "groupId6.id" "groupParts1.gi";
connectAttr "polySurfaceShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape4.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
// End of cultery base.ma
