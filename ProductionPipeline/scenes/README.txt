The Scenes folder is where you store all models that are brought into maya.

This ENVIRONMENT file should exist at the root of this folder.  This is the final version that references the prop and texture database.

The Prop_Database is the FINAL version of each texture.  This is also saved to the GOOGLE Drive.
NOTE there should only be one version of each prop.
This is NOT the backup file, that would be on the local drive of each person working on the project.  
NOTE you can always create a mirrored backup.
THIS FOLDER NEEDS A GATEKEEPER.
There must be some kind of system setup that will allow artist to "check out" and lock the model from other departments, such as texturing