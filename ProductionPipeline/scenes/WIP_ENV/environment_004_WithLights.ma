//Maya ASCII 2024 scene
//Name: environment_004_WithLights.ma
//Last modified: Sun, Nov 05, 2023 09:32:25 PM
//Codeset: 1252
file -rdi 1 -ns "chair" -rfn "chairRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/chair.ma";
file -rdi 1 -ns "corner_cabinet" -rfn "corner_cabinetRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/corner cabinet.ma";
file -rdi 1 -ns "daybed" -rfn "daybedRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/daybed.ma";
file -rdi 1 -ns "fireplace" -rfn "fireplaceRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/fireplace.ma";
file -rdi 1 -ns "stove" -rfn "stoveRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/stove.ma";
file -rdi 1 -ns "table" -rfn "tableRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/table.ma";
file -rdi 1 -ns "walls" -rfn "wallsRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/walls.ma";
file -rdi 1 -ns "rug" -rfn "rugRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/rug.ma";
file -rdi 1 -ns "coffeepot" -rfn "coffeepotRN" -op "v=0;" -typ "mayaAscii" "C:/Users/miffy/Pictures/IMD/3 d/ProductionPipeline//scenes/Prop_Database/coffeepot.ma";
file -rdi 1 -ns "hoosier_cabinet" -rfn "hoosier_cabinetRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/hoosier cabinet.ma";
file -rdi 1 -ns "wall_shelf" -rfn "wall_shelfRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/wall shelf.ma";
file -rdi 1 -ns "curtains" -rfn "curtainsRN" -op "v=0;" -typ "mayaAscii" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/curtains.ma";
file -rdi 1 -ns "bucket" -rfn "bucketRN" -op "v=0;" -typ "mayaAscii" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/bucket.ma";
file -rdi 1 -ns "second_chair" -rfn "second_chairRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/second_chair.ma";
file -r -ns "chair" -dr 1 -rfn "chairRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/chair.ma";
file -r -ns "corner_cabinet" -dr 1 -rfn "corner_cabinetRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/corner cabinet.ma";
file -r -ns "daybed" -dr 1 -rfn "daybedRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/daybed.ma";
file -r -ns "fireplace" -dr 1 -rfn "fireplaceRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/fireplace.ma";
file -r -ns "stove" -dr 1 -rfn "stoveRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/stove.ma";
file -r -ns "table" -dr 1 -rfn "tableRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/table.ma";
file -r -ns "walls" -dr 1 -rfn "wallsRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/walls.ma";
file -r -ns "rug" -dr 1 -rfn "rugRN" -op "v=0;" -typ "mayaAscii" "C:/Users/zhan0743/Downloads/ProductionPipeline//scenes/Prop_Database/rug.ma";
file -r -ns "coffeepot" -dr 1 -rfn "coffeepotRN" -op "v=0;" -typ "mayaAscii" "C:/Users/miffy/Pictures/IMD/3 d/ProductionPipeline//scenes/Prop_Database/coffeepot.ma";
file -r -ns "hoosier_cabinet" -dr 1 -rfn "hoosier_cabinetRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/hoosier cabinet.ma";
file -r -ns "wall_shelf" -dr 1 -rfn "wall_shelfRN" -op "v=0;" -typ "mayaAscii" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/wall shelf.ma";
file -r -ns "curtains" -dr 1 -rfn "curtainsRN" -op "v=0;" -typ "mayaAscii" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/curtains.ma";
file -r -ns "bucket" -dr 1 -rfn "bucketRN" -op "v=0;" -typ "mayaAscii" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/bucket.ma";
file -r -ns "second_chair" -dr 1 -rfn "second_chairRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/second_chair.ma";
requires maya "2024";
requires "stereoCamera" "10.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiSkyDomeLight"
		 "mtoa" "5.3.1.1";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2024";
fileInfo "version" "2024";
fileInfo "cutIdentifier" "202304191415-7fa20164c6";
fileInfo "osv" "Windows 11 Pro v2009 (Build: 22621)";
fileInfo "UUID" "07A7DE83-4E3A-679A-02AE-628EB278D4DD";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "84AA9275-4757-C6BE-F014-CE817F4EC95A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -45.698169996259217 54.07302348887287 -34.624763280443105 ;
	setAttr ".r" -type "double3" -381.93835272906199 -1221.7999999994761 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "527A49C1-4A5D-81BA-106A-24A28F219092";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 63.304742792429252;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "7D0CB692-4195-DA4A-DC20-E39C1DA05EE3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 5.8218352352539782 1000.1 13.181609162813352 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "0D586436-434A-2E21-5A4A-19A65982AC15";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 262.82462868583042;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "970D3172-4ED8-C940-74C3-878E770279C2";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "81A5C08B-41FA-8D71-9046-11859C8DEF44";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "653D18D4-4A88-202E-DC8B-EC9993152FA6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "36DFA9A4-4406-476E-71FA-F58A8294395B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "Skydome_sunset";
	rename -uid "0F1DD9BB-44F9-0F12-CCD7-C5A249E23F10";
createNode aiSkyDomeLight -n "Skydome_sunset" -p "|Skydome_sunset";
	rename -uid "C0AB2610-4C2F-BFEF-5F08-6F8DF7824DD4";
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr -k off ".v";
	setAttr ".csh" no;
	setAttr ".rcsh" no;
	setAttr ".camera" 0;
	setAttr ".ai_samples" 3;
	setAttr ".aal" -type "attributeAlias" {"exposure","aiExposure"} ;
createNode transform -n "Window_Light";
	rename -uid "BB570F99-4572-9875-E21B-15AD1CBDE8B7";
	setAttr ".t" -type "double3" -7.5257164602653486 71.082746278858068 -212.16286463243526 ;
	setAttr ".r" -type "double3" -10.830038695263955 180 0 ;
	setAttr ".s" -type "double3" 125.26680245788464 125.26680245788464 125.26680245788464 ;
createNode areaLight -n "Window_LightShape" -p "Window_Light";
	rename -uid "09607559-41EB-775D-2A19-E5B7A1D3E43E";
	setAttr -k off ".v";
	setAttr ".cl" -type "float3" 1 0.59799999 0 ;
	setAttr ".in" 10000;
	setAttr ".ai_exposure" 5;
	setAttr ".ai_sss" 100;
	setAttr ".ai_volume_samples" 3;
createNode transform -n "Door_Render_Camera";
	rename -uid "C75492E2-4BB7-4804-25B1-88B9B86775DD";
	setAttr ".t" -type "double3" -149.39751804444097 72.033354456070171 51.2751032662349 ;
	setAttr ".r" -type "double3" -19.199999999961648 -1148.3999999999089 0 ;
createNode camera -n "Door_Render_CameraShape" -p "Door_Render_Camera";
	rename -uid "FA3112F7-4084-2A7E-F4CB-4588A4BAC735";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".cap" -type "double2" 1.41732 0.94488 ;
	setAttr ".ff" 0;
	setAttr ".coi" 160.50827124134253;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
createNode transform -n "Fireplace_Light";
	rename -uid "EABB603C-44B7-5C78-02E1-83ADE7D391BA";
	setAttr ".t" -type "double3" 88.955410743377755 32.693284186241968 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 82.504084208127097 82.504084208127097 82.504084208127097 ;
createNode areaLight -n "Fireplace_LightShape" -p "Fireplace_Light";
	rename -uid "633553E2-414B-C48E-0911-EFBBFDFC2611";
	setAttr -k off ".v";
	setAttr ".cl" -type "float3" 0.6631 0.2491 0 ;
	setAttr ".in" 150000;
	setAttr ".ai_volume_samples" 3;
createNode transform -n "Fireplace_Render_Camera";
	rename -uid "9A4A14FC-4D30-A1D5-23B0-25A24755F30C";
	setAttr ".t" -type "double3" 118.54944380959914 65.63891684928079 62.599779009155704 ;
	setAttr ".r" -type "double3" -19.799999999990316 -303.59999999999195 2.8736928543749735e-15 ;
createNode camera -n "Fireplace_Render_CameraShape" -p "Fireplace_Render_Camera";
	rename -uid "60F4BED2-4038-ADE5-36B2-0C827C44A779";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".cap" -type "double2" 1.41732 0.94488 ;
	setAttr ".ff" 0;
	setAttr ".coi" 128.14153451322773;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "653B0BC1-4826-2818-7FC8-4B90AFE271DE";
	setAttr -s 139 ".lnk";
	setAttr -s 139 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "E10AD863-4D2A-5A36-95CE-1ABE72B80D50";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "926F58E4-4D0C-018D-B482-4A91FCB3E37D";
createNode displayLayerManager -n "layerManager";
	rename -uid "0A42A7DC-475F-7D44-0D88-4EBBF1A99643";
createNode displayLayer -n "defaultLayer";
	rename -uid "BA3A702C-4B16-B9EE-84D3-688A0FA644C3";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "687016AD-4600-9F43-D1EB-7184808024F8";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C4B00706-4C2C-4BA9-34AA-EFAE8C1C60C9";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "78D0DA01-4A44-E677-7085-57B8E73DE1B2";
	addAttr -ci true -sn "ARV_options" -ln "ARV_options" -dt "string";
	setAttr ".AA_samples" 4;
	setAttr ".indirect_sample_clamp" 5;
	setAttr ".version" -type "string" "5.3.1.1";
	setAttr ".ARV_options" -type "string" "Test Resolution=100%;Camera=DELETECAM_LATERShape;Color Management.Gamma=1;Color Management.Exposure=0;Background.BG=BG Color;Background.Color=0 0 0;Background.Image=;Background.Scale=1   1;Background.Offset=0   0;Background.Apply Color Management=1;Foreground.Enable FG=0;Foreground.Image=;Foreground.Scale=1   1;Foreground.Offset=0   0;Foreground.Apply Color Management=1;";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "CBFB8BE7-4615-4671-5366-8DBF3FD55436";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "CDFBA084-4CDA-A771-A530-49A40F6914CE";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "BAB9022F-403A-9FCB-28BB-99BAD59B7CFC";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode reference -n "chairRN";
	rename -uid "7065B015-4FEA-A4F6-1146-639289253683";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/chair.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"chairRN"
		"chairRN" 0
		"chairRN" 4
		2 "|chair:POS" "translate" " -type \"double3\" -11.27367041054777275 0 -56.11984885750084118"
		
		2 "|chair:POS" "rotate" " -type \"double3\" 0 -89.99999999999992895 0"
		2 "|chair:POS|chair:MOV" "translate" " -type \"double3\" 4.05092914524879255 0 37.34473647202227653"
		
		2 "|chair:POS|chair:MOV" "scale" " -type \"double3\" 1 1 1";
lockNode -l 1 ;
createNode reference -n "corner_cabinetRN";
	rename -uid "77D2CF5F-4F39-2496-65D5-F58327F517A3";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/corner cabinet.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"corner_cabinetRN"
		"corner_cabinetRN" 3
		2 "|corner_cabinet:corner_cabinet_grp|corner_cabinet:POS|corner_cabinet:MOV|corner_cabinet:ADJ|corner_cabinet:corner_cabinet_geo_grp|corner_cabinet:corner_cabinet_geo" 
		"translate" " -type \"double3\" 5.85200363592911454 0 7.52683543272902966"
		2 "|corner_cabinet:corner_cabinet_grp|corner_cabinet:POS|corner_cabinet:MOV|corner_cabinet:ADJ|corner_cabinet:corner_cabinet_geo_grp|corner_cabinet:corner_cabinet_geo|corner_cabinet:corner_cabinet_geoShape" 
		"uvPivot" " -type \"double2\" 0.25 0.375"
		2 "|corner_cabinet:corner_cabinet_grp|corner_cabinet:POS|corner_cabinet:MOV|corner_cabinet:ADJ|corner_cabinet:corner_cabinet_geo_grp|corner_cabinet:corner_cabinet_geo|corner_cabinet:corner_cabinet_geoShape" 
		"pt[0:7]" " -s 8 -type \"float3\" -0.050853006999999999 0 0 0.050853006999999999 0 0 -0.050853006999999999 0 0 0.050853006999999999 0 0 0.30297118000000001 0 0 -0.30297118000000001 0 0 0.28293264000000001 0 9.536743200000001e-07 -0.30297118000000001 0 0"
		
		"corner_cabinetRN" 3
		2 "|corner_cabinet:corner_cabinet_grp|corner_cabinet:POS" "translate" " -type \"double3\" -94.07704418493908349 0 -61.63814459672359192"
		
		2 "|corner_cabinet:corner_cabinet_grp|corner_cabinet:POS" "rotate" " -type \"double3\" 0 45.00000000000001421 0"
		
		2 "|corner_cabinet:corner_cabinet_grp|corner_cabinet:POS|corner_cabinet:MOV" 
		"translate" " -type \"double3\" 0 0.59732348432063809 3.11142262774265976";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "daybedRN";
	rename -uid "D0B694BA-40F9-6336-3E0E-4A8798B1763A";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/daybed.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"daybedRN"
		"daybedRN" 0
		"daybedRN" 11
		2 "|daybed:daybed_grp" "rotate" " -type \"double3\" 0 0 0"
		2 "|daybed:daybed_grp|daybed:POS" "translate" " -type \"double3\" 14.36487464370305744 0 46.46582215641737434"
		
		2 "|daybed:daybed_grp|daybed:POS" "rotate" " -type \"double3\" 0 -180 0"
		2 "daybed:mattres_pattern" "fileTextureName" " -type \"string\" \"C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Texture_Database/daybed textures/mattress_pattern2.png\""
		
		2 "daybed:mattres_pattern" "colorSpace" " -type \"string\" \"sRGB\""
		2 "daybed:mattres_pattern" "viewNameUsed" " 0"
		2 "daybed:mattres_pattern" "viewNameStr" " -type \"string\" \"<N/A>\""
		2 "daybed:file2" "fileTextureName" " -type \"string\" \"C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Texture_Database/Open Book/BookOpenUV_DefaultMaterial_BaseColor.png\""
		
		2 "daybed:file2" "colorSpace" " -type \"string\" \"sRGB\""
		2 "daybed:file2" "viewNameUsed" " 0"
		2 "daybed:file2" "viewNameStr" " -type \"string\" \"<N/A>\"";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "fireplaceRN";
	rename -uid "DEC36F7C-4352-12FD-DC22-659FE03C988D";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/fireplace.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"fireplaceRN"
		"fireplaceRN" 0
		"fireplaceRN" 2
		2 "|fireplace:fireplace_grp|fireplace:POS" "translate" " -type \"double3\" 87.01009784185866636 0 34.78351788725245086"
		
		2 "|fireplace:fireplace_grp|fireplace:POS" "rotate" " -type \"double3\" 0 -89.99999999999997158 0";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "stoveRN";
	rename -uid "B5D94C54-4E27-53CC-78C9-158EB6432E55";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/stove.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"stoveRN"
		"stoveRN" 0
		"stoveRN" 5
		2 "|stove:stove_grp|stove:POS" "translate" " -type \"double3\" 81.77717922165251707 0 -56.74125484262714281"
		
		2 "|stove:stove_grp|stove:POS" "rotate" " -type \"double3\" 0 -45.00000000000001421 0"
		
		2 "|stove:stove_grp|stove:POS|stove:MOV" "translate" " -type \"double3\" -7.1202636300214861 7.03724735833624671 17.95742247749738851"
		
		2 "|stove:stove_grp|stove:POS|stove:MOV" "rotate" " -type \"double3\" 0 -45 0"
		
		2 "|stove:stove_grp|stove:POS|stove:MOV" "scale" " -type \"double3\" 4.96595091199414185 4.96595091199414185 4.96595091199414185";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "tableRN";
	rename -uid "60192D7A-4031-63E0-176C-D8B6DB210142";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/table.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"tableRN"
		"tableRN" 0
		"tableRN" 121
		2 "|table:table_grp|table:POS" "translate" " -type \"double3\" -33.89397869284520226 0 -51.96847135723912459"
		
		2 "|table:table_grp|table:POS" "rotate" " -type \"double3\" 0 -89.99999999999997158 0"
		
		2 "|table:table_grp|table:POS" "scale" " -type \"double3\" 1 1 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_top_geo|table:table_top_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_top_geo|table:table_top_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_base_geo|table:table_base_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_base_geo|table:table_base_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_front_L|table:table_leg_front_LShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_front_L|table:table_leg_front_LShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_front_R|table:table_leg_front_RShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_front_R|table:table_leg_front_RShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_back_L|table:table_leg_back_LShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_back_L|table:table_leg_back_LShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_back_R|table:table_leg_back_RShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:TableLeg_grp|table:table_leg_back_R|table:table_leg_back_RShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp|table:dessertfork_geo|table:dessertfork_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp|table:dessertfork_geo|table:dessertfork_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp|table:CutleryHandle_geo4|table:CutleryHandle_geo4Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp|table:CutleryHandle_geo4|table:CutleryHandle_geo4Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp|table:CutleryDeco_geo4|table:CutleryDeco_geo4Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp|table:CutleryDeco_geo4|table:CutleryDeco_geo4Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp|table:knife_geo|table:knife_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp|table:knife_geo|table:knife_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp|table:CutleryHandle_geo5|table:CutleryHandle_geo5Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp|table:CutleryHandle_geo5|table:CutleryHandle_geo5Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp|table:CutleryDeco_geo5|table:CutleryDeco_geo5Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp|table:CutleryDeco_geo5|table:CutleryDeco_geo5Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp|table:TableSpoon_geo|table:TableSpoon_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp|table:TableSpoon_geo|table:TableSpoon_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp|table:CutleryHandle_geo7|table:CutleryHandle_geo7Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp|table:CutleryHandle_geo7|table:CutleryHandle_geo7Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp|table:CutleryDeco_geo7|table:CutleryDeco_geo7Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp|table:CutleryDeco_geo7|table:CutleryDeco_geo7Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp2|table:dessertfork_geo|table:dessertfork_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp2|table:dessertfork_geo|table:dessertfork_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp2|table:CutleryHandle_geo4|table:CutleryHandle_geo4Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp2|table:CutleryHandle_geo4|table:CutleryHandle_geo4Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp2|table:CutleryDeco_geo4|table:CutleryDeco_geo4Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:DessertFork_grp2|table:CutleryDeco_geo4|table:CutleryDeco_geo4Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp2|table:knife_geo|table:knife_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp2|table:knife_geo|table:knife_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp2|table:CutleryHandle_geo5|table:CutleryHandle_geo5Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp2|table:CutleryHandle_geo5|table:CutleryHandle_geo5Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp2|table:CutleryDeco_geo5|table:CutleryDeco_geo5Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Knife_grp2|table:CutleryDeco_geo5|table:CutleryDeco_geo5Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp2|table:TableSpoon_geo|table:TableSpoon_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp2|table:TableSpoon_geo|table:TableSpoon_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp2|table:CutleryHandle_geo7|table:CutleryHandle_geo7Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp2|table:CutleryHandle_geo7|table:CutleryHandle_geo7Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp2|table:CutleryDeco_geo7|table:CutleryDeco_geo7Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:Cutlery_grp|table:Spoon_grp2|table:CutleryDeco_geo7|table:CutleryDeco_geo7Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:circle_geo1|table:circle_geoShape1" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:circle_geo1|table:circle_geoShape1" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:circle_geo|table:circle_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:circle_geo|table:circle_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:milkhandle_geo|table:milkhandle_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:milkhandle_geo|table:milkhandle_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:milkjugbody_geo|table:milkjugbody_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:MilkJug_grp|table:milkjugbody_geo|table:milkjugbody_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:sugarbody_geo|table:sugarbody_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:sugarbody_geo|table:sugarbody_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:sugarhandle_geo|table:sugarhandle_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:sugarhandle_geo|table:sugarhandle_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle|table:handle_circleShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle|table:handle_circleShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle1|table:handle_circleShape1" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle1|table:handle_circleShape1" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle2|table:handle_circleShape2" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle2|table:handle_circleShape2" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle3|table:handle_circle3Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:handle_circle3|table:handle_circle3Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:sugarhandle_geo1|table:sugarhandle_geo1Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarcontainerbody_grp|table:sugarhandle_geo1|table:sugarhandle_geo1Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarlidtop_geo|table:sugarlidtop_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarlidtop_geo|table:sugarlidtop_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarlidbase_geo1|table:sugarlidbase_geo1Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:SugarContainer_grp|table:sugarlidbase_geo1|table:sugarlidbase_geo1Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:circle_geo1|table:circle_geoShape1" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:circle_geo1|table:circle_geoShape1" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:circle_geo|table:circle_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:circle_geo|table:circle_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:teapothandle_geo|table:teapothandle_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:teapothandle_geo|table:teapothandle_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:sugarlidbase_geo2|table:sugarlidbase_geo2Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:sugarlidbase_geo2|table:sugarlidbase_geo2Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:sugarlidtop_geo1|table:sugarlidtop_geo1Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:sugarlidtop_geo1|table:sugarlidtop_geo1Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:teapotbase_geo|table:teapotbase_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaPot_grp|table:teapotbase_geo|table:teapotbase_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:circle_geo2|table:circle_geo2Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:circle_geo2|table:circle_geo2Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:originalteacup|table:originalteacupShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:originalteacup|table:originalteacupShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:original_handle|table:original_handleShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:original_handle|table:original_handleShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:originalteacup1|table:originalteacup1Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp|table:originalteacup1|table:originalteacup1Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:teaplate_geo|table:teaplate_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:teaplate_geo|table:teaplate_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:Plate_geo|table:Plate_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:Plate_geo|table:Plate_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:bowl_geo|table:bowl_geoShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:bowl_geo|table:bowl_geoShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:bowl_geo2|table:bowl_geo2Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:bowl_geo2|table:bowl_geo2Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:Plate_geo2|table:Plate_geo2Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:Plate_geo2|table:Plate_geo2Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:teaplate_geo1|table:teaplate_geo1Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:teaplate_geo1|table:teaplate_geo1Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:circle_geo2|table:circle_geo2Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:circle_geo2|table:circle_geo2Shape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:originalteacup|table:originalteacupShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:originalteacup|table:originalteacupShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:original_handle|table:original_handleShape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:original_handle|table:original_handleShape" 
		"displaySmoothMesh" " 0"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:originalteacup1|table:originalteacup1Shape" 
		"dispResolution" " 1"
		2 "|table:table_grp|table:POS|table:MOV|table:ADJ|table:table_grp_geo|table:table_props_grp|table:TeaCup_grp1|table:originalteacup1|table:originalteacup1Shape" 
		"displaySmoothMesh" " 0"
		2 "table:file17" "fileTextureName" " -type \"string\" \"C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Texture_Database/table textures/table props/TableProps_BaseColor.png\""
		
		2 "table:file17" "colorSpace" " -type \"string\" \"sRGB\""
		2 "table:file17" "viewNameUsed" " 0"
		2 "table:file17" "viewNameStr" " -type \"string\" \"<N/A>\"";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "wallsRN";
	rename -uid "BB18349A-4666-E456-EF77-A7AB26036250";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/walls.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"wallsRN"
		"wallsRN" 0
		"wallsRN" 6
		2 "|walls:walls_grp|walls:POS|walls:MOV|walls:ADJ|walls:walls_geo_grp|walls:roof_geo" 
		"visibility" " 1"
		2 "|walls:walls_grp|walls:POS|walls:MOV|walls:ADJ|walls:walls_geo_grp|walls:east_wall_geo_grp" 
		"visibility" " 0"
		2 "|walls:walls_grp|walls:POS|walls:MOV|walls:ADJ|walls:walls_geo_grp|walls:north_wall_geo_grp" 
		"visibility" " 1"
		2 "|walls:walls_grp|walls:POS|walls:MOV|walls:ADJ|walls:walls_geo_grp|walls:west_wall_geo_grp" 
		"visibility" " 0"
		2 "|walls:walls_grp|walls:POS|walls:MOV|walls:ADJ|walls:walls_geo_grp|walls:south_wall_geo_grp" 
		"visibility" " 1"
		2 "|walls:humanBody" "visibility" " 0";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "rugRN";
	rename -uid "8FB6C321-4AB8-C3D6-BD15-14AAF1689B30";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/rug.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"rugRN"
		"rugRN" 0
		"rugRN" 6
		2 "|rug:rug_grp|rug:POS" "translate" " -type \"double3\" 33.47133118449676203 0 35.35547896640550647"
		
		2 "|rug:rug_grp|rug:POS" "rotate" " -type \"double3\" 0 0 0"
		2 "rug:mattres_pattern" "fileTextureName" " -type \"string\" \"C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Texture_Database/rug textures/rug_basecolour.png\""
		
		2 "rug:mattres_pattern" "colorSpace" " -type \"string\" \"sRGB\""
		2 "rug:mattres_pattern" "viewNameUsed" " 0"
		2 "rug:mattres_pattern" "viewNameStr" " -type \"string\" \"<N/A>\"";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "5FA1AA9B-44D3-B0DE-45DF-D0AD361C951C";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n"
		+ "            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n"
		+ "            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n"
		+ "            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"|Fireplace_Render_Camera\" \n            -useInteractiveMode 0\n            -displayLights \"all\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n"
		+ "            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -excludeObjectPreset \"All\" \n            -shadows 1\n            -captureSequenceNumber -1\n            -width 990\n            -height 688\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n"
		+ "            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n"
		+ "            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n"
		+ "                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n"
		+ "                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 0\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n"
		+ "                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n"
		+ "                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n"
		+ "                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n"
		+ "                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"|persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n"
		+ "                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n"
		+ "                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -bluePencil 1\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"|Fireplace_Render_Camera\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -excludeObjectPreset \\\"All\\\" \\n    -shadows 1\\n    -captureSequenceNumber -1\\n    -width 990\\n    -height 688\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"|Fireplace_Render_Camera\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -excludeObjectPreset \\\"All\\\" \\n    -shadows 1\\n    -captureSequenceNumber -1\\n    -width 990\\n    -height 688\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "5ABC04A1-4FA5-ED96-A41A-ADAABCD03589";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode reference -n "sharedReferenceNode";
	rename -uid "46245237-48F3-5531-738E-D6A70FFCA033";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sharedReferenceNode";
createNode reference -n "coffeepotRN";
	rename -uid "80FFFE01-4B9F-B195-4C8D-B7A47EFD3ECF";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/coffeepot.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"coffeepotRN"
		"coffeepotRN" 1
		2 "|coffeepot:coffeepot_grp|coffeepot:POS|coffeepot:MOV|coffeepot:ADJ|coffeepot:coffepot_geo_grp|coffeepot:coffeepot_geo" 
		"translate" " -type \"double3\" 47.37398416604102636 41.02118338680999443 -7.90702499932800684"
		
		"coffeepotRN" 18
		2 "|coffeepot:coffeepot_grp" "translate" " -type \"double3\" -26.33237502119056472 0 -50.00735439804078908"
		
		2 "|coffeepot:coffeepot_grp|coffeepot:POS|coffeepot:MOV" "translate" " -type \"double3\" 42.63463924313715836 47.34087443045596899 -1.28826441149251569"
		
		2 "|coffeepot:coffeepot_grp|coffeepot:POS|coffeepot:MOV" "rotate" " -type \"double3\" 0 148.43568804273689921 0"
		
		2 "|coffeepot:coffeepot_grp|coffeepot:POS|coffeepot:MOV" "scale" " -type \"double3\" 1.8187372036092142 1.8187372036092142 1.41404137548171982"
		
		2 "|coffeepot:back" "translate" " -type \"double3\" 0.058355704842013134 3.08131725017047842 -1000.10000000000002274"
		
		2 "|coffeepot:back" "rotate" " -type \"double3\" 0 180 0"
		2 "|coffeepot:back" "scale" " -type \"double3\" 1 1 1"
		2 "|coffeepot:back" "shear" " -type \"double3\" 0 0 0"
		2 "|coffeepot:back" "rotatePivot" " -type \"double3\" 0 0 0"
		2 "|coffeepot:back" "rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|coffeepot:back" "scalePivot" " -type \"double3\" 0 0 0"
		2 "|coffeepot:back" "scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|coffeepot:back" "rotateAxis" " -type \"double3\" 0 0 0"
		2 "|coffeepot:back|coffeepot:backShape" "orthographicWidth" " 419.35636869777937363"
		
		2 "coffeepot:file1" "fileTextureName" " -type \"string\" \"C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Texture_Database/coffeepot textures/CoffeepotUV_standardSurface1_BaseColor.png\""
		
		2 "coffeepot:file1" "colorSpace" " -type \"string\" \"sRGB\""
		2 "coffeepot:file1" "viewNameUsed" " 0"
		2 "coffeepot:file1" "viewNameStr" " -type \"string\" \"<N/A>\"";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "hoosier_cabinetRN";
	rename -uid "5C769EE8-4274-DDA0-169F-6588FEDB8DAD";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/hoosier cabinet.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"hoosier_cabinetRN"
		"hoosier_cabinetRN" 0
		"hoosier_cabinetRN" 1
		2 "|hoosier_cabinet:hoosier_cabinet_grp|hoosier_cabinet:POS|hoosier_cabinet:MOV" 
		"translate" " -type \"double3\" 28.55723312985846718 0 -57.93360742594704504";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "wall_shelfRN";
	rename -uid "B7285794-42BD-0863-80A4-2EBADDF58011";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/wall shelf.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"wall_shelfRN"
		"wall_shelfRN" 0
		"wall_shelfRN" 6
		2 "|wall_shelf:wall_shelf_grp|wall_shelf:POS" "rotate" " -type \"double3\" 0 -7 0"
		
		2 "|wall_shelf:wall_shelf_grp|wall_shelf:POS" "rotatePivot" " -type \"double3\" 0 0 0"
		
		2 "|wall_shelf:wall_shelf_grp|wall_shelf:POS" "rotatePivotTranslate" " -type \"double3\" 0 0 0"
		
		2 "|wall_shelf:wall_shelf_grp|wall_shelf:POS" "scalePivot" " -type \"double3\" 0 0 0"
		
		2 "|wall_shelf:wall_shelf_grp|wall_shelf:POS|wall_shelf:MOV" "translate" 
		" -type \"double3\" 89.07648809950165969 30.18882766720885513 -17.52461013105850185"
		
		2 "|wall_shelf:wall_shelf_grp|wall_shelf:POS|wall_shelf:MOV" "rotate" " -type \"double3\" 0 -90 0";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode file -n "file1";
	rename -uid "93259A99-4DA9-177C-C916-B9B64A9B6F70";
	setAttr ".ftn" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Light_Texture/Skydome_sunset_4k.hdr";
	setAttr ".cs" -type "string" "Raw";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "64EAD696-4901-A288-3463-10BB6679A144";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "F7EB3FCE-4ECE-F9DC-4423-CC83153AE7D9";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -44.047617297323995 -635.71426045327064 ;
	setAttr ".tgi[0].vh" -type "double2" 119.04761431709188 44.047617297323995 ;
createNode reference -n "curtainsRN";
	rename -uid "467EA99D-48DB-40DE-C435-AB938ABEF663";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/curtains.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"curtainsRN"
		"curtainsRN" 0;
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "bucketRN";
	rename -uid "BC36F2C5-4F62-1CF7-CED9-2FA6414A2A31";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/bucket.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"bucketRN"
		"bucketRN" 0
		"bucketRN" 2
		2 "|bucket:bucket_grp|bucket:POS|bucket:MOV" "translate" " -type \"double3\" -82.95550070240756213 0 55.69639880775689278"
		
		2 "|bucket:bucket_grp|bucket:POS|bucket:MOV" "scale" " -type \"double3\" 2.02481109301722073 2.02481109301722073 1.57426078785811696";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "second_chairRN";
	rename -uid "0DC10E8A-4FAF-C48F-2122-DCB067EFF423";
	setAttr ".fn[0]" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//scenes/Prop_Database/second_chair.ma";
	setAttr ".ed" -type "dataReferenceEdits" 
		"second_chairRN"
		"second_chairRN" 0
		"second_chairRN" 2
		2 "|second_chair:POS|second_chair:MOV" "translate" " -type \"double3\" -11.95932659870186576 0 -50.05067753010074227"
		
		2 "|second_chair:POS|second_chair:MOV" "rotate" " -type \"double3\" 0 121.21745353552559266 0";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode file -n "file2";
	rename -uid "E940D146-4942-4E8B-2882-94B6E154273C";
	setAttr ".ftn" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Texture_Database/rug textures/rug_basecolour.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture2";
	rename -uid "75794262-4807-9EF5-9021-05B4A425D334";
createNode file -n "file3";
	rename -uid "228216F3-4F17-4FB1-CCA5-9DB54096C087";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "C:/Users/marya/AppData/Local/GitHubDesktop/MayaCabinProject - Use this one/Maya-Cabin-Project/ProductionPipeline//sourceimages/Texture_Database/rug textures/fabric_bump_map.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture3";
	rename -uid "22027698-4FAD-1D2C-4D33-BE867A3FE12A";
createNode bump2d -n "bump2d1";
	rename -uid "4B861850-4407-5C4A-9E12-8BB975E30D73";
	setAttr ".vc1" -type "float3" 0 2.9999999e-05 0 ;
	setAttr ".vc2" -type "float3" 9.9999997e-06 9.9999997e-06 0 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".aoon" yes;
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
	setAttr ".rtfm" 1;
select -ne :renderPartition;
	setAttr -s 60 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 59 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 190 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 11 ".r";
select -ne :lightList1;
	setAttr -s 3 ".l";
select -ne :defaultTextureList1;
	setAttr -s 149 ".tx";
select -ne :standardSurface1;
	setAttr ".sr" 0.5;
select -ne :initialShadingGroup;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :initialMaterialInfo;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".outf" 51;
	setAttr ".imfkey" -type "string" "exr";
	setAttr ".dss" -type "string" "standardSurface1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultLightSet;
	setAttr -s 3 ".dsm";
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHideFaceDataSet;
	setAttr -s 3 ".dnsm";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "file1.oc" "|Skydome_sunset|Skydome_sunset.sc";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "sharedReferenceNode.sr" "chairRN.sr";
connectAttr "sharedReferenceNode.sr" "corner_cabinetRN.sr";
connectAttr "sharedReferenceNode.sr" "daybedRN.sr";
connectAttr "sharedReferenceNode.sr" "fireplaceRN.sr";
connectAttr "sharedReferenceNode.sr" "stoveRN.sr";
connectAttr "sharedReferenceNode.sr" "tableRN.sr";
connectAttr "sharedReferenceNode.sr" "wallsRN.sr";
connectAttr "sharedReferenceNode.sr" "rugRN.sr";
connectAttr "sharedReferenceNode.sr" "coffeepotRN.sr";
connectAttr "sharedReferenceNode.sr" "hoosier_cabinetRN.sr";
connectAttr "sharedReferenceNode.sr" "wall_shelfRN.sr";
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "sharedReferenceNode.sr" "curtainsRN.sr";
connectAttr "sharedReferenceNode.sr" "bucketRN.sr";
connectAttr "sharedReferenceNode.sr" "second_chairRN.sr";
connectAttr ":defaultColorMgtGlobals.cme" "file2.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file2.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file2.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file2.ws";
connectAttr "place2dTexture2.c" "file2.c";
connectAttr "place2dTexture2.tf" "file2.tf";
connectAttr "place2dTexture2.rf" "file2.rf";
connectAttr "place2dTexture2.mu" "file2.mu";
connectAttr "place2dTexture2.mv" "file2.mv";
connectAttr "place2dTexture2.s" "file2.s";
connectAttr "place2dTexture2.wu" "file2.wu";
connectAttr "place2dTexture2.wv" "file2.wv";
connectAttr "place2dTexture2.re" "file2.re";
connectAttr "place2dTexture2.of" "file2.of";
connectAttr "place2dTexture2.r" "file2.ro";
connectAttr "place2dTexture2.n" "file2.n";
connectAttr "place2dTexture2.vt1" "file2.vt1";
connectAttr "place2dTexture2.vt2" "file2.vt2";
connectAttr "place2dTexture2.vt3" "file2.vt3";
connectAttr "place2dTexture2.vc1" "file2.vc1";
connectAttr "place2dTexture2.o" "file2.uv";
connectAttr "place2dTexture2.ofs" "file2.fs";
connectAttr ":defaultColorMgtGlobals.cme" "file3.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file3.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file3.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file3.ws";
connectAttr "place2dTexture3.c" "file3.c";
connectAttr "place2dTexture3.tf" "file3.tf";
connectAttr "place2dTexture3.rf" "file3.rf";
connectAttr "place2dTexture3.mu" "file3.mu";
connectAttr "place2dTexture3.mv" "file3.mv";
connectAttr "place2dTexture3.s" "file3.s";
connectAttr "place2dTexture3.wu" "file3.wu";
connectAttr "place2dTexture3.wv" "file3.wv";
connectAttr "place2dTexture3.re" "file3.re";
connectAttr "place2dTexture3.of" "file3.of";
connectAttr "place2dTexture3.r" "file3.ro";
connectAttr "place2dTexture3.n" "file3.n";
connectAttr "place2dTexture3.vt1" "file3.vt1";
connectAttr "place2dTexture3.vt2" "file3.vt2";
connectAttr "place2dTexture3.vt3" "file3.vt3";
connectAttr "place2dTexture3.vc1" "file3.vc1";
connectAttr "place2dTexture3.o" "file3.uv";
connectAttr "place2dTexture3.ofs" "file3.fs";
connectAttr "file3.oa" "bump2d1.bv";
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "bump2d1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "|Skydome_sunset|Skydome_sunset.ltd" ":lightList1.l" -na;
connectAttr "Window_LightShape.ltd" ":lightList1.l" -na;
connectAttr "Fireplace_LightShape.ltd" ":lightList1.l" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "file3.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.oc" ":standardSurface1.bc";
connectAttr "bump2d1.o" ":standardSurface1.n";
connectAttr "|Skydome_sunset.iog" ":defaultLightSet.dsm" -na;
connectAttr "Window_Light.iog" ":defaultLightSet.dsm" -na;
connectAttr "Fireplace_Light.iog" ":defaultLightSet.dsm" -na;
// End of environment_004_WithLights.ma
