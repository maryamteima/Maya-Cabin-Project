This is the basic folders that can be setup to work on the Environment.
JPG is from the notes that visually shows the breakdown.
When you submit the File do not keep any images or textfiles that have been provided to your for pedagogical purposes.

For simplicity, I would suggest you may not want to Reference Lights.  
Have the Environment file have the lights in it rather than referencing them in.

However if you wish to work this way, you may create a light folder.